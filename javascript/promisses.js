function demora1(){  // função que demora com promisses
return new Promise((resolve)=>{ // retorna a promisse
    setTimeout(() => { // define um intervalo
        console.log("resolvido 1 - 3s");
        resolve (100) // resolve (then)
    }, 3000);
})
}

function demora2(){
return new Promise((resolve)=>{
    setTimeout(() => {
        console.log("resolvido 2 - 1s");
        resolve (100)
    }, 1000);
})
}

function demora3(){
return new Promise((resolve)=>{
    setTimeout(() => {
        console.log("resolvido 3 - 2s");
        resolve (100)
    }, 2000);
})
}



function normal1(){
setTimeout(() => {
    console.log("resolvido 1 - 3s");
}, 3000);
}

function normal2(){
setTimeout(() => {
    console.log("resolvido 2 - 1s");
}, 1000);
}

function normal3(){
setTimeout(() => {
    console.log("resolvido 3 - 2s");
}, 2000);
}


async function promessa(x){  // esperando um resultado de uma promisse
    await new Promise((res)=>{
        setTimeout(res, x);
    })
    console.log(`promessa resolvida em ${x} ms`);
}

async function teste(){ // quando chamado é executado na ordem da chamada (assicrono) 
   await demora1();
   await demora2();
   await demora3();
}

function normal(){ // quando chamado é executado por tempo (mais rápida primeiro)
   normal1();
   normal2();
   normal3();
}

//teste();
promessa(2000);