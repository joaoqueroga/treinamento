let a = [1,2,3,4,'5',"seis",7,"oito",'0009','ZERO']

//map
a.map((x)=>{
    console.log(x);
})

//forEach
a.forEach(x => {
    console.log(x);
});

//for
for (let index = 0; index < a.length; index++) {
    console.log(a[index]);
}

let i = a.length - 1;
while (i >= 0) {
    console.log(a[i]);
    i--;
}