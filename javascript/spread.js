let array = [1,2,3,4,5,6];

console.log(array);
// [ 1, 2, 3, 4, 5, 6]

console.log(...array);
// 1 2 3 4 5 6

console.log(...array, 7);
// 1 2 3 4 5 6 7

function teste(...parametros){ // multiplos parametros (REST)
    console.log(parametros);
}
teste(1,6,7,2,8,9);
// [ 1, 6, 7, 2, 8, 9 ]

let arr1 = [0, 1, 2];
let arr2 = [3, 4, 5];
let arr3 = [...arr2, ...arr1];
console.log(arr3);
// [ 3, 4, 5, 0, 1, 2 ]