import React, {useState} from "react";
import Speech from 'react-speech';
import { BsSpotify } from "react-icons/bs";

const App = () => {

  const [fala1, setFala1] = useState('Sua demanda envolve ação contra pessoas físicas? Exemplos: Vizinhança (infiltrações, poda de árvores, aberturas irregulares de janelas, passagem forçada, muro divisório, dentre outras), acidente de trânsito, despejo, cobranças de valores, anulação de contratos e negociações particulares, danos morais, invesão de terreno com até um metro, queda de água da chuva no seu terreno, construção irregular de fossas, acidentes causados por animais, entre outras?')


  const icone = <BsSpotify size={30}/>

  return (
    <div>
      <p>{fala1}</p>
      <Speech 
      text={fala1}
      lang="pt-BR"
      rate={1.3} 
      voice="Google português do Brasil"
      textAsButton
      displayText={icone}
      />
    </div>
  );
};

export default App;
