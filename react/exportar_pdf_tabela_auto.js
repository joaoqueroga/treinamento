import { jsPDF } from "jspdf";   /// npm i jspdf
import React, {useState} from "react";
import autoTable from 'jspdf-autotable'; /// npm i jspdf-autotable

const App = () => {


  const [texto1] = useState([
    {"linha": "Mussum Ipsum, cacilds vidis litro abertis. Manduma pindureta quium dia nois paga.Mé faiz elementum girarzis, nisi eros vermeio.Per aumento de cachacis, eu reclamis.Mais vale um bebadis conhecidiss, que um alcoolatra anonimis, Mussum Ipsum, cacilds vidis litro abertis. Manduma"},
    {"linha": "Mussum Ipsum, cacilds vidis litro abertis. Manduma pindureta quium dia nois paga.Mé faiz elementum girarzis, nisi eros vermeio.Per aumento de cachacis, eu reclamis.Mais vale um bebadis conhecidiss, que um alcoolatra anonimis, Mussum Ipsum, cacilds vidis litro abertis. Manduma pindureta"},
    {"linha": "Mussum Ipsum, cacilds vidis litro abertis. Manduma pindureta quium dia nois paga.Mé faiz elementum girarzis, nisi eros vermeio.Per aumento de cachacis, eu reclamis.Mais vale um bebadis conhecidiss, que um alcoolatra anonimis, Mussum Ipsum, cacilds vidis litro abertis. Manduma pindureta quium dia nois paga.Mé faiz elementum girarzis, nisi eros vermeio.Per aumento de cachacis, eu reclamis.Mais vale um bebadis conhecidiss, que um alcoolatra anonimis."},
    {"linha": "Mussum Ipsum que um alcoolatra anonimis, Mussum Ipsum,"},
  ]);

  const [tabela] = useState([
    {"nome": "Joao" , "idade": 28},
    {"nome": "Maria" , "idade": 22},
    {"nome": "Pedro" , "idade": 45},
    {"nome": "José" , "idade": 16},
  ])


  function generatePDF (){

    let data = new Date();
    let doc = new jsPDF('p', 'pt');

    doc.setFont('helvetica', 'normal')
    doc.setFontSize(14)
    doc.text(228, 20, `${data.getDate()}/${data.getMonth()+1}/${data.getFullYear()} - ${data.toLocaleTimeString()}`)
    
    doc.setFontSize(11.5)     

    autoTable(doc, ({
        startY:35,
        body: texto1,
        theme: 'plain',
        styles: { textColor: [100, 100, 100], cellPadding: 1 },
        columns: [
            { header: '', dataKey: 'linha' }
        ],
    }))

    autoTable(doc, ({
      body: tabela,
      theme: 'grid',
      columns: [
          { header: 'Nome', dataKey: 'nome' },
          { header: 'Idade', dataKey: 'idade' }
      ],
    }))

    //doc.save(`Resultado${data}.pdf`)
    window.open(doc.output('bloburl', { filename: 'download.pdf' }), '_blank');  
  } 

  return (
     <div>
         <button  onClick={generatePDF}>imprimir</button>
     </div>
  );
};
export default App;
