import React, { useEffect, useState, useContext } from 'react';
import Swal from 'sweetalert2';

import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { Checkbox } from 'primereact/checkbox';
import { BreadCrumb } from 'primereact/breadcrumb';
import { Dropdown } from 'primereact/dropdown';
import { Tooltip } from 'primereact/tooltip';
import { Message } from 'primereact/message';
import { Accordion, AccordionTab } from 'primereact/accordion';
import { FiRefreshCcw, FiTrash2 } from 'react-icons/fi';

import { ApiContext } from '../../../context/store';
import api from "../../../config/api";
import "./distribuir-processos.scss";
import SpinnerCarregamento from '../spinner-carregamento';
import { getToken } from '../../../config/auth';


function DistribuirProcessos() {
  const headers = { "Authorization": `Bearer ${getToken()}` };
  const items = [
    { label: 'Distribuição de Processos' },
    { label: 'Distribuir Processos' }
  ];
  const home = { icon: 'pi pi-home', url: '/inicio' };

  const [loading, setLoading] = useState(false);
  const [valorCheckbox, setValorCheckbox] = useState(true);
  const [iconeCheckbox, setIconeCheckbox] = useState("pi pi-check");
  const [mascara, setMascara] = useState("23000000");

  const [processoInput, setProcessoInput] = useState(mascara);
  const [tituloReuniaoInput, setTituloReuniaoInput] = useState("");
  const [sorteiosEmAndamento, setSorteiosEmAndamento] = useState([]);
  const [cicloSelecionado, setCicloSelecionado] = useState();
  const [defensoresRejeitados, setDefensoresRejeitados] = useState([]);
  const [defensoresSorteados, setDefensoresSorteados] = useState([]);
  const [defensores, setDefensores] = useState([]);
  const { showError } = useContext(ApiContext);

  useEffect(() => {
    // são feitas duas requisições, uma para obter os sorteios e outra para obter os dados do sorteio em si
    // a variável loading é colocada como true para haver apenas um feedback de loading
    setLoading(true);
    fetchSorteios(false);
  }, []);

  useEffect(() => {
    if (cicloSelecionado) {
      fetchDefensores();
    }
  }, [cicloSelecionado]);

  async function fetchSorteios(loadingSpinner = true) {
    try {
      setLoading(true);
      // retorna uma lista com os ids dos sorteios em aberto
      let response = await api.get("distribuicao-processos/listar-sorteios/", { headers });
      let sorteiosEmAndamento = response.data;
      for (let sorteio of sorteiosEmAndamento) {
        sorteio.label = `Ciclo ${sorteio.id_sorteio_processo} - ${sorteio.data_inicio}`;
      }
      setSorteiosEmAndamento(sorteiosEmAndamento);
      // para definir ordem decrescente no dropdown
      sorteiosEmAndamento.reverse();
      const sorteioAtual = sorteiosEmAndamento[0];
      setCicloSelecionado(sorteioAtual);
      if (sorteioAtual === undefined) {
        setDefensores([]);
        setLoading(false);
      }
    }
    catch {
      showError("Erro", "Erro ao carregar dados de sorteios");
      return;
    }
    finally {
      if (loadingSpinner)
        setLoading(false);
    }
  }

  async function fetchDefensores() {
    setLoading(true);
    const params = { id_sorteio_processo: cicloSelecionado.id_sorteio_processo };
    let response = await api.get("distribuicao-processos/listar/", { params, headers });
    let defensoresDisponiveis = [];
    // popular com defensores
    for (const defensor of response.data.defensores_status) {
      defensor.processo = "";
      if (!defensor.eh_suplente)
        defensoresDisponiveis.push(defensor);
    }
    // popular com suplentes em seguida
    for (const defensor of response.data.defensores_status) {
      defensor.processo = "";
      if (defensor.eh_suplente)
        defensoresDisponiveis.push(defensor);
    }
    atualizarEstadoCheckboxes(estadoCheckboxes(defensoresDisponiveis));
    setDefensores(defensoresDisponiveis);
    setDefensoresSorteados(valorAntigo => {
      return response.data.defensores_sorteados;
    });

    for (const sorteio of response.data.sorteio_atual) {
      if (sorteio.id_sorteio_processo === cicloSelecionado.id_sorteio_processo) {
        const reuniao_atual = sorteio.reunioes[sorteio.reunioes.length - 1];
        if (reuniao_atual)
          setTituloReuniaoInput(reuniao_atual.descricao);
        else
          setTituloReuniaoInput("");
      }
    }

    setLoading(false);
  }

  async function iniciarNovoCiclo(titulo, texto) {
    Swal.fire({
      title: titulo,
      text: texto,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: "Iniciar ciclo",
      cancelButtonText: "Cancelar",
    }).then(async (result) => {
      if (result.isConfirmed) {
        await api.post("/distribuicao-processos/iniciar/", {}, { headers });
        await fetchSorteios();
        setDefensoresRejeitados([]);
        setDefensoresSorteados([]);
        if (cicloSelecionado)
          fetchDefensores();
      }
    });
  }

  async function encerrarCicloAtual() {
    if (existeCicloEmAndamento())
      Swal.fire({
        title: 'Encerrar o ciclo',
        text: `Encerrar o ciclo de sorteio atual?`,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: "Encerrar ciclo",
        cancelButtonText: "Cancelar"
      }).then(async (result) => {
        if (result.isConfirmed) {
          const params = { id_sorteio_processo: cicloSelecionado.id_sorteio_processo };
          await api.post("/distribuicao-processos/encerrar/", params, { headers });
          await fetchSorteios();
          if (!existeCicloEmAndamento()) {
            fetchDefensores();
          }
          else {
            setDefensores([]);
            setDefensoresSorteados([]);
            setDefensoresRejeitados([]);
          }
        }
      });
    else
      iniciarNovoCiclo("Não existem ciclos em andamento", "Deseja iniciar um novo ciclo de sorteio?");
  }

  function existeCicloEmAndamento() {
    return cicloSelecionado !== [] && cicloSelecionado !== undefined && cicloSelecionado !== null;
  }

  async function distribuir() {
    if (processoInput === "")
      return showError("Erro", "Por favor, insira o número do processo");
    // verificar não existe ciclo em andamento 
    if (!cicloSelecionado) {
      iniciarNovoCiclo("Não existem ciclos em andamento", "Deseja iniciar um novo ciclo de sorteio?");
      return;
    }

    let response;
    const processo = processoInput;

    Swal.fire({
      icon: 'info',
      title: 'Sorteando...',
      html: '<b></b>',
      allowOutsideClick: false,

      didOpen: async () => {
        Swal.showLoading();
        let defensoresSorteaveis = [];
        for (const defensor of defensores) // enviar para o sorteio apenas os defensores selecionados
          if (defensor.selecionado)
            defensoresSorteaveis.push(defensor);

        try {
          const params = { defensores: defensoresSorteaveis, processo, id_sorteio_processo: cicloSelecionado.id_sorteio_processo, id_usuario: 5 };
          response = await api.post("/distribuicao-processos/distribuir/", params, { headers });
        }
        catch {
          Swal.close();
          showError("Erro ao distribuir processo", response.data.motivo);
        }

        //delay de 2 segundos para sorteio
        await new Promise(r => setTimeout(r, 2000));
        Swal.close();

        if (response.data.sucesso === 'N')
          showError("Erro", response.data.motivo);

        else {
          let defensorSorteado = response.data;
          defensorSorteado.processo = processo;

          Swal.fire({
            title: 'Resultado do sorteio',
            text: `${defensorSorteado.funcionario_nome}: Processo Nº ${processo}`,
            icon: 'success',
            showDenyButton: true,
            confirmButtonText: "Aceitar",
            denyButtonText: "Rejeitar"
          }).then(async (result) => {
            if (result.isConfirmed) {
              Swal.fire({
                title: "Confirmação em andamento",
                text: "Por favor aguarde",
                allowOutsideClick: false,
                didOpen: () => Swal.showLoading()
              });

              let responseConfirmar;
              try {
                const params = {
                  defensor_sorteado: defensorSorteado,
                  id_sorteio_processo: cicloSelecionado.id_sorteio_processo,
                  processo: processo,
                  descricao: tituloReuniaoInput,
                };

                responseConfirmar = await api.post("/distribuicao-processos/confirmar/", params, { headers });
                if (responseConfirmar.data.sucesso === 'N') {
                  showError("Erro", responseConfirmar.data.motivo);
                  Swal.close();
                  return;
                }
              }
              catch {
                showError("Erro", "Falha na comunicação com o servidor");
                Swal.close();
              }
              setDefensoresSorteados(responseConfirmar.data.defensores_sorteados);
              // desmarcar quem foi sorteado
              marcarDesmarcarCheckbox(defensorSorteado.id_funcionario);
              // remarcar os que foram anteriormente rejeitados
              for (const defensor of defensoresRejeitados) {
                marcarDesmarcarCheckbox(defensor.id_funcionario);
              }
              setDefensoresRejeitados([]);

              setProcessoInput(mascara);
              document.getElementById("processo-input").value = "";
              Swal.close();
            }
            else if (result.isDenied) {
              marcarDesmarcarCheckbox(defensorSorteado.id_funcionario);
              // lógica para guardar defensores rejeitados e marcá-los depois
              // inserir apenas se o defensor ainda não estiver na lista
              if (!defensoresRejeitados.find((defensor) => defensor.id_funcionario === defensorSorteado.id_funcionario)) {
                defensoresRejeitados.push(defensorSorteado);
              }
              defensoresRejeitados.sort((a, b) => {
                if (a.funcionario_nome > b.funcionario_nome)
                  return 1;
                else if (a.funcionario_nome < b.funcionario_nome)
                  return -1;
                else
                  return 0;
              });
              setDefensoresRejeitados(defensoresRejeitados);
            }
          });
        }
      }
    });
  }

  async function redistribuirProcesso(defensor) {
    defensor.id_funcionario = Number(defensor.id_funcionario);
    Swal.fire({
      title: 'Redistribuir Processo',
      text: `Redistribuir Processo ${defensor.processo} recebido por ${defensor.funcionario_nome}?`,
      icon: 'info',
      showCancelButton: true,
      confirmButtonText: "Redistribuir",
      cancelButtonText: "Cancelar"
    }).then(async (result) => {
      if (result.isConfirmed) {
        Swal.fire({
          title: "Redistribuição em andamento",
          text: "Por favor aguarde",
          allowOutsideClick: false,
          didOpen: () => Swal.showLoading()
        });

        let defensoresSelecionados = [];
        for (const defensor of defensores) // enviar para o sorteio apenas os defensores selecionados
          if (defensor.selecionado)
            defensoresSelecionados.push(defensor);
        let response, responseConfirmar;
        try {
          const params = { defensores: defensoresSelecionados, processo: defensor.processo, id_sorteio_processo: cicloSelecionado.id_sorteio_processo, id_usuario: 5 };
          response = await api.post("/distribuicao-processos/distribuir/", params, { headers });
          if (response.data.sucesso === "N") {
            showError("Erro", response.data.motivo);
            Swal.close();
            return;
          }

          let defensorSorteado = response.data;
          Swal.close();
          Swal.fire({
            title: 'Resultado do sorteio',
            text: `${defensorSorteado.funcionario_nome}: Processo Nº ${defensor.processo}`,
            icon: 'success',
            showCancelButton: true,
            confirmButtonText: "Aceitar",
            cancelButtonText: "Rejeitar"
          }).then(async (result) => {
            if (result.isConfirmed) {
              Swal.fire({
                title: "Confirmação em andamento",
                text: "Por favor aguarde",
                allowOutsideClick: false,
                didOpen: () => Swal.showLoading()
              });

              const params = {
                defensor_sorteado: defensorSorteado,
                defensor_excluir: defensor,
                processo: defensor.processo,
                id_sorteio_processo: cicloSelecionado.id_sorteio_processo,
                descricao: tituloReuniaoInput,
              };
              responseConfirmar = await api.post("/distribuicao-processos/confirmar/", params, { headers });
              if (responseConfirmar.data.sucesso === "N") {
                showError("Erro", responseConfirmar.data.motivo);
                Swal.close();
                return;
              }
              marcarDesmarcarCheckbox(defensor.id_funcionario);
              marcarDesmarcarCheckbox(defensorSorteado.id_funcionario);
              let defensoresSorteados = [];
              for (const defensor of responseConfirmar.data.defensores_sorteados) {
              }

              // TODO ver fetch/carregamento de defensores
              await fetchDefensores()
              Swal.close();
            }
          });

        }
        catch {
          showError("Erro", "Falha na comunicação com o servidor");
          Swal.close();
        }
      }
      else if (result.isDenied || result.isDismissed) { }
    });
  }

  async function excluirProcesso(defensor) {
    Swal.fire({
      title: 'Excluir processo',
      text: `Excluir Processo ${defensor.processo} recebido por ${defensor.funcionario_nome}?`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: "Excluir",
      cancelButtonText: "Cancelar"
    }).then(async (result) => {
      if (result.isConfirmed) {
        let defensoresDisponiveis = [];

        for (const defensorAux of defensores)
          if (defensorAux.selecionado)
            defensoresDisponiveis.push(defensorAux);

        Swal.fire({
          title: "Exclusão em andamento",
          text: "Por favor aguarde",
          allowOutsideClick: false,
          didOpen: () => Swal.showLoading()
        });

        let response;
        try {
          const params = { defensor_sorteado: defensor, id_usuario: 5, id_sorteio_processo: cicloSelecionado.id_sorteio_processo };
          response = await api.delete("/distribuicao-processos/excluir/", { data: params, headers });
        }
        catch {
          showError("Erro", "Falha na comunicação com o servidor");
          Swal.close();
        }

        if (response.data.sucesso === "N") {
          showError("Erro", response.data.motivo);
          Swal.close();
          return;
        }
        fetchDefensores();
        Swal.close();
      }
      else if (result.isDenied || result.isDismissed) { }
    });
  }

  async function marcarDesmarcarCheckbox(id) {
    let defensoresAux = [...defensores];
    let defensorSelecionado;

    // Desmarcar seleção
    for (let defensor of defensoresAux)
      if (defensor.id_funcionario === id) {
        defensor.selecionado = !defensor.selecionado;
        defensorSelecionado = defensor;
      }
    const params = { id_funcionario: defensorSelecionado.id_funcionario, selecionado: defensorSelecionado.selecionado, id_sorteio_processo: cicloSelecionado.id_sorteio_processo };
    const response = await api.post("/distribuicao-processos/guardar-status-selecionados/", params, { headers });
    // mesma função usada no fetch defensores 
    // TO DO: fazer uma função separada para lidar com as listas
    let listaOrganizada = [];
    // popular com defensores
    for (const defensor of response.data.lista) {
      defensor.processo = "";
      if (!defensor.eh_suplente)
        listaOrganizada.push(defensor);
    }
    // popular com suplentes em seguida
    for (const defensor of response.data.lista) {
      defensor.processo = "";
      if (defensor.eh_suplente)
        listaOrganizada.push(defensor);
    }

    let aux_listaResposta = [...listaOrganizada];
    setDefensores(aux_listaResposta);
    atualizarEstadoCheckboxes(estadoCheckboxes(aux_listaResposta));
  }

  function estadoCheckboxes(defensores) {
    let existemMarcados = false, existemDesmarcados = false;
    for (const defensor of defensores) {
      if (defensor.eh_suplente)
        continue;
      if (defensor.selecionado)
        existemMarcados = true;
      if (!defensor.selecionado)
        existemDesmarcados = true;
    }

    if (existemDesmarcados && !existemMarcados)
      return "todos-desmarcados";
    else if (existemMarcados && !existemDesmarcados)
      return "todos-marcados";
    else if (existemMarcados && existemDesmarcados)
      return "ambos-marcados";
  }

  function atualizarEstadoCheckboxes(estado) {
    switch (estado) {
      case "todos-desmarcados":
        setValorCheckbox(false);
        setIconeCheckbox(null);
        break;
      case "todos-marcados":
        setValorCheckbox(true);
        setIconeCheckbox("pi pi-check");
        break;
      case "ambos-marcados":
        setValorCheckbox(true);
        setIconeCheckbox("pi pi-minus");
        break;
      default:
        setValorCheckbox(true);
        setIconeCheckbox("pi pi-check");
    }
  }

  async function marcarDesmarcarTodos() {
    setValorCheckbox(!valorCheckbox);
    let listaDefensores = [...defensores];
    const estado = (estadoCheckboxes(listaDefensores));

    if (estado === "todos-desmarcados" || estado === "ambos-marcados")
      for (let defensor of listaDefensores) {
        if (!defensor.eh_suplente) {
          defensor.selecionado = true;
          const params = { id_funcionario: defensor.id_funcionario, selecionado: defensor.selecionado, id_sorteio_processo: cicloSelecionado.id_sorteio_processo };
          api.post("/distribuicao-processos/guardar-status-selecionados/", params, {headers});
        }
      }
    else if (estado === "todos-marcados")
      for (let defensor of listaDefensores) {
        if (!defensor.eh_suplente) {
          defensor.selecionado = false;
          const params = { id_funcionario: defensor.id_funcionario, selecionado: defensor.selecionado, id_sorteio_processo: cicloSelecionado.id_sorteio_processo };
          api.post("/distribuicao-processos/guardar-status-selecionados/", params, {headers});
        }
      }
    atualizarEstadoCheckboxes(estadoCheckboxes(listaDefensores));
    setDefensores(listaDefensores);
  }

  function goToInputEnd(event) {
    const elemento = event.target;
    const tamanhoInput = elemento.value.length;
    elemento.setSelectionRange(tamanhoInput, tamanhoInput);
  }

  function selecionarCiclo(event) {
    setCicloSelecionado(event.value);
  }

  function tooltipRemarcarDefensores() {
    const quantidade = defensoresRejeitados.length;
    let defs = "\n";
    defensoresRejeitados.forEach((defensor) => { defs += `${defensor.funcionario_nome}\n` });
    if (defensoresRejeitados.length <= 1)
      return `${quantidade} defensor anteriormente rejeitado será remarcado automaticamente após a próxima distribuição: ${defs}`
    else
      return `${quantidade} defensores anteriormente rejeitados serão remarcados automaticamente após a próxima distribuição: ${defs}`
  }


  return (
    <div className="view">
      <Tooltip target="#defensores-remarcar" />
      <div className="view-body">
        <div className="header">
          <BreadCrumb model={items} home={home} />
        </div>
        <div className="view-flex distribuicao-processos">
          <div className='row-dropdown'>
            <Dropdown
              value={cicloSelecionado}
              onChange={(e) => selecionarCiclo(e)}
              options={sorteiosEmAndamento.length > 0 ? sorteiosEmAndamento : [{ label: "Sem ciclos em andamento", value: "No results found", disabled: true }]}
              optionLabel="label"
              placeholder="Selecione um ciclo de sorteio"
            />
            <InputText
              id='titulo-reuniao'
              placeholder='Título da reunião'
              disabled={defensores.length > 0 ? false : true}
              value={tituloReuniaoInput}
              onChange={(e) => setTituloReuniaoInput(e.target.value)}
            />
          </div>

          {
            loading
              ? <SpinnerCarregamento />
              : <>
                <div className='label-marcar'>
                  <div className='checkbox-geral' onClick={marcarDesmarcarTodos}>
                    <Checkbox icon={iconeCheckbox} checked={valorCheckbox} />
                    <span id='texto-checkbox'>Marcar/Desmarcar todos</span>
                  </div>

                  {
                    /*
                      <Message
                        id='defensores-remarcar'
                        severity="info"
                        text={`${defensoresRejeitados.length}` + (defensoresRejeitados.length > 1 ? " defensores a remarcar" : " defensor a remarcar")}
                        data-pr-position="bottom"
                        data-pr-tooltip={tooltipRemarcarDefensores()}
                        style={defensoresRejeitados.length <= 0 ? { visibility: 'hidden' } : { visibility: 'visible' }}
                      />
                    */
                  }
                </div>
                <div className='selecao-defensores'>
                  <div className="metade">
                    <b className='titulo-lista'>Defensores disponíveis</b>
                    <ul className='lista-defensores'>
                      {
                        defensores?.length > 0 ?
                          defensores.map((defensor) =>
                            defensor.eh_suplente ?
                              <div className='defensor-item defensores-livres suplente-item' onClick={(e) => marcarDesmarcarCheckbox(defensor.id_funcionario, e)} key={defensor.id_funcionario}>
                                <Checkbox checked={defensor.selecionado} />
                                <li>
                                  {defensor.funcionario_nome}
                                  <br />
                                  <span className='span-info'>Defensor suplente</span>
                                </li>
                              </div>
                              :
                              <div className='defensor-item defensores-livres' onClick={(e) => marcarDesmarcarCheckbox(defensor.id_funcionario, e)} key={defensor.id_funcionario}>
                                <Checkbox checked={defensor.selecionado} />
                                <li>
                                  {defensor.funcionario_nome}
                                  <br />
                                  <span className='span-info'>Defensor</span>
                                </li>
                              </div>
                          ) :
                          <li>Nenhum sorteio em andamento</li>
                      }
                    </ul>
                  </div>
                  <div className="metade">
                    <b className='titulo-lista'>Processos distribuídos</b>
                    <ul className='lista-defensores'>
                      {
                        defensoresSorteados.length > 0 ?
                          defensoresSorteados.map((defensor) =>
                            <div className={`defensor-item ${defensor.eh_suplente ? "suplente-item" : ""}`} key={defensor.processo}>
                              <li>
                                <b>{defensor.funcionario_nome} </b>
                                <br />
                                <div className='processo' key={`${defensor.processo}`}>
                                  <div>
                                    <span>Processo Nº {defensor.processo}</span>
                                    <br />
                                    <span className='span-info'>{defensor.data}</span>

                                  </div>
                                  <div className='botoes'>
                                    <Button
                                      tooltip='Redistribuir processo'
                                      icon="pi pi-refresh"
                                      className='btn-green'
                                      onClick={() => redistribuirProcesso({ id_funcionario: defensor.id_funcionario, funcionario_nome: defensor.funcionario_nome, processo: defensor.processo })}
                                    >
                                    </Button>
                                  </div>
                                </div>

                                {
                                  defensor.redistribuicao.length > 0
                                    ? <span className='span-info'>Histórico de redistribuições:</span>
                                    : null
                                }

                                {
                                  defensor.redistribuicao.length > 0
                                    ? defensor.redistribuicao.map((def) =>
                                      <Accordion key={def.data} >
                                        <AccordionTab header={def.data}>
                                          <span className='span-info'>
                                            Processo redistribuído de {def.funcionario_nome}, originalmente recebido em {def.data}
                                          </span>
                                        </AccordionTab>
                                      </Accordion>
                                    )
                                    : null
                                }
                              </li>
                            </div>
                          ) :
                          <li>Nenhum processo sorteado</li>
                      }
                    </ul>
                  </div>
                </div>
                <div className="processos">
                  <div className="input-grupo">
                    <div className="p-inputgroup">
                      <span className="p-inputgroup-addon">Processo Nº</span>
                      <InputText
                        id='processo-input'
                        value={processoInput}
                        disabled={defensores.length > 0 ? false : true}
                        placeholder="Insira o número do Processo"
                        onClick={goToInputEnd}
                        onChange={(e) => setProcessoInput(e.target.value)}
                      />
                    </div>
                  </div>

                  <div className="botoes-distribuir-processos">
                    <Button
                      type="button"
                      onClick={distribuir}
                      className="btn-1"
                      icon="pi pi-check"
                      label='Distribuir'
                    />
                    <Button
                      type="button"
                      onClick={encerrarCicloAtual}
                      className="btn-3"
                      icon="pi pi-times"
                      label='Encerrar ciclo'
                    />
                    <Button
                      type="button"
                      onClick={() => iniciarNovoCiclo('Iniciar novo ciclo', `Iniciar um novo ciclo de sorteio?`)}
                      className="btn-2"
                      icon="pi pi-sync"
                      label='Novo ciclo'
                    />
                  </div>
                </div>
              </>
          }
        </div>
      </div>
    </div>
  );
}

export default DistribuirProcessos;