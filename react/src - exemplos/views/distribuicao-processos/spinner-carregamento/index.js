import React from "react";
import { ProgressSpinner } from 'primereact/progressspinner';
import './spinner-carregamento.scss'

function SpinnerCarregamento() {
  return (
    <span className="spinner-carregamento">
      <ProgressSpinner />
    </span>
  );
}

export default SpinnerCarregamento;