// Importações
import jsPDF from 'jspdf';
import autoTable from 'jspdf-autotable';
import { Button } from 'primereact/button';
import logo from "./logo_black.png"
import './gerar-relatorio.scss'

function ComprovantePdf(props) {
  const X_INICIAL_ASSINATURA = 50;
  const X_FINAL_ASSINATURA = X_INICIAL_ASSINATURA + 300;
  const Y_MARGEM_LINHA_ASSINATURA = 40;
  const Y_MARGEM_TEXTO_ASSINATURA = Y_MARGEM_LINHA_ASSINATURA + 15;
  const Y_MARGEM_LINHA_ASSINATURA_NOVA_PAGINA = 50;
  const Y_MARGEM_TEXTO_ASSINATURA_NOVA_PAGINA = Y_MARGEM_LINHA_ASSINATURA_NOVA_PAGINA + 15;

  const Y_TITULO1 = 90;
  const Y_TITULO2 = 105;
  const Y_TITULO3 = 125;

  const X_LOGO = 198;
  const Y_LOGO = 20;
  const WIDTH_LOGO = 200;
  const HEIGHT_LOGO = 40;


  function generatePDF() {
    function assinatura() {
      doc.setFontSize(10.5);
      // verificação necessária para a assinatura não passar do fundo da página
      if (doc.lastAutoTable.finalY > 750) {
        doc.addPage();
        doc.line(
          X_INICIAL_ASSINATURA,
          Y_MARGEM_LINHA_ASSINATURA_NOVA_PAGINA,
          X_FINAL_ASSINATURA,
          Y_MARGEM_LINHA_ASSINATURA_NOVA_PAGINA,
          "f" // modo fill, desenha uma linha cheia
        );
        doc.text(
          X_INICIAL_ASSINATURA,
          Y_MARGEM_TEXTO_ASSINATURA_NOVA_PAGINA,
          "Assinatura do(a) responsável"
        );
      }
      else {
        doc.line(
          X_INICIAL_ASSINATURA,
          doc.lastAutoTable.finalY + Y_MARGEM_LINHA_ASSINATURA,
          X_FINAL_ASSINATURA,
          doc.lastAutoTable.finalY + Y_MARGEM_LINHA_ASSINATURA,
          "f" // modo fill, desenha uma linha cheia
        );
        doc.text(
          X_INICIAL_ASSINATURA,
          doc.lastAutoTable.finalY + Y_MARGEM_TEXTO_ASSINATURA,
          "Assinatura do(a) responsável"
        );
      }
    }


    function historicoDistribuicao() {
      let y = doc.lastAutoTable.finalY;
      // incluir nova página se a tabela ficar muito próxima ao final da página
      if (y > 650) {
        doc.addPage();
        y = 50;
      }

      doc.setFont('helvetica', 'bold');
      doc.setFontSize(15);
      doc.text(
        40,
        y + 35,
        "Histórico de redistribuição de processos"
      );

      autoTable(doc, {
        startY: y + 45,
        body: redistribuicao,
        columns: [
          { header: 'Nome', dataKey: 'funcionario_nome' },
          { header: 'Sorteado em', dataKey: 'data' },
          { header: 'Reunião', dataKey: 'reuniao_descricao' },
          { header: 'Processo', dataKey: 'processo' },
        ],
      });
    }

    let data = new Date();
    const dataFormatada = {
      dia: String(data.getDate()).padStart(2, '0'),
      mes: String(data.getMonth() + 1).padStart(2, '0'),
      ano: String(data.getFullYear())
    };

    let doc = new jsPDF('p', 'pt');
    doc.setProperties({
      title: `Resultado${data}.pdf`
    });

    doc.centeredText = function (y, text) {
      var textWidth = doc.getStringUnitWidth(text) * doc.internal.getFontSize() / doc.internal.scaleFactor;
      var textOffset = (doc.internal.pageSize.width - textWidth) / 2;
      doc.text(textOffset, y, text);
    }

    doc.addImage(
      logo,
      'PNG',
      X_LOGO,
      Y_LOGO,
      WIDTH_LOGO,
      HEIGHT_LOGO
    );
    doc.setFont('helvetica', 'bold');
    doc.setFontSize(18);
    doc.centeredText(
      Y_TITULO1,
      `Resultado do sorteio - Ciclo ${props.resultado.id_sorteio_processo}`
    );

    doc.setFont('helvetica', 'normal');
    doc.setFontSize(14);
    doc.centeredText(
      Y_TITULO2,
      `${props.resultado.data_inicio} - ${props.resultado.data_fim}`
    );
    doc.setFontSize(10);
    doc.centeredText(
      Y_TITULO3,
      `Relatório gerado em ${dataFormatada.dia}/${dataFormatada.mes}/${dataFormatada.ano} às ${data.toLocaleTimeString()}`
    );

    doc.setFontSize(11.5);
    autoTable(doc, {
      startY: 145,
      body: props.resultado.defensores_sorteados,
      columns: [
        { header: 'Nome', dataKey: 'funcionario_nome' },
        { header: 'Sorteado em', dataKey: 'data' },
        { header: 'Reunião', dataKey: 'reuniao_descricao' },
        { header: 'Processo', dataKey: 'processo' },
      ],
    });

    let redistribuicao = [];
    for (const defensor of props.resultado.defensores_sorteados)
      if (defensor.redistribuicao.length > 0)
        for (const redist of defensor.redistribuicao)
          redistribuicao.push({ ...redist, processo: defensor.processo });

    if (redistribuicao.length > 0)
      historicoDistribuicao();

    assinatura();
    window.open(doc.output('bloburl'));
  }

  // Botão para realizar download do PDF
  return (
    <div>
      <Button
        type="button"
        tooltip="Imprimir resultado em pdf"
        tooltipOptions={{ position: 'top' }}
        onClick={generatePDF}
        className="btn-2"
        label="Gerar PDF"
        icon="pi pi-file-pdf"
        disabled={!props.ativo}
      >
      </Button>
    </div>
  );
}

export default ComprovantePdf;