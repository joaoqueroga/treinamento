import React, { useEffect, useState, useContext } from "react";
import { BreadCrumb } from 'primereact/breadcrumb';
import { Dropdown } from 'primereact/dropdown';
import SpinnerCarregamento from "../spinner-carregamento";

import './relatorios.scss';
import api from "../../../config/api";
import { ApiContext } from "../../../context/store";
import ComprovantePdf from "../gerar-relatorio";
import { getToken } from "../../../config/auth";

function RelatoriosDistribuicaoProcessos() {
  const headers = { "Authorization": `Bearer ${getToken()}` };
  const items = [
    { label: 'Distribuição de Processos' },
    { label: 'Gerar Relatório' }
  ];
  const home = { icon: 'pi pi-home', url: '/inicio' }
  const { showError } = useContext(ApiContext);

  const [loading, setLoading] = useState(false);
  const [botaoAtivo, setBotaoAtivo] = useState(false);
  const [sorteados, setSorteados] = useState([]);
  const [listaSorteios, setListaSorteios] = useState([]);
  const [selecaoDropdown, setSelecaoDropdown] = useState({});


  useEffect(() => {
    fetchSorteados();
  }, []);


  async function fetchSorteados() {
    setLoading(true);
    let response;
    response = await api.get("distribuicao-processos/listar/", {headers});
    if (response.data.sucesso === 'S') {
      let sorteiosAnteriores = [];

      // ordenar a lista de sorteios pelo id
      response.data.sorteios_realizados.sort((a, b) =>
        (a.id_sorteio_processo - b.id_sorteio_processo) * -1
      );

      for (const sorteio of response.data.sorteios_realizados)
        sorteiosAnteriores.push({ ...sorteio, label: `Ciclo ${sorteio.id_sorteio_processo}: ${sorteio.data_inicio.slice(0, 10)} - ${sorteio.data_fim.slice(0, 10)}` });
      setListaSorteios(sorteiosAnteriores);

      // por padrão, mostrar o último sorteio ao abrir a página
      const ultimoSorteio = sorteiosAnteriores[0];
      if (ultimoSorteio) {
        setSorteados(ultimoSorteio.defensores_sorteados);
        setSelecaoDropdown(ultimoSorteio);
        if (ultimoSorteio.defensores_sorteados.length === 0)
          setBotaoAtivo(false);
        else
          setBotaoAtivo(true);
      }
    }
    else {
      showError("Erro", "Erro ao carregar dados de defensores");
      setBotaoAtivo(false);
    }
    setLoading(false);
  }

  function selecionarSorteio(event) {
    setSelecaoDropdown(event.value);
    const sorteadosSelecionados = event.value.defensores_sorteados;
    setSorteados(sorteadosSelecionados);

    if (sorteadosSelecionados.length === 0)
      setBotaoAtivo(false);
    else
      setBotaoAtivo(true);
  }

  return (
    <div className="view">
      <div className="view-body">
        <div className="header">
          <BreadCrumb model={items} home={home} />
        </div>

        {
          loading
            ?
            <SpinnerCarregamento />
            :
            <div className="view-flex distribuicao-relatorios">
              <Dropdown
                value={selecaoDropdown}
                onChange={(e) => selecionarSorteio(e)}
                options={listaSorteios}
                optionLabel="label"
                placeholder="Selecione um ciclo de sorteio"
              />
              <ul className='lista-sorteados'>
                {
                  sorteados?.length > 0
                    ? sorteados.map((defensor) =>
                      <li key={`${defensor.id_funcionario}-${defensor.processo}`} className={`${defensor.eh_suplente ? "suplente-item" : ""}`}>
                        {defensor.funcionario_nome} <br /> <b>Processo Nº {defensor.processo}</b>
                        <span className='span-info'>{defensor.data}</span>
                      </li>
                    )
                    : <li>Nenhum defensor sorteado</li>
                }
              </ul>
              <ComprovantePdf resultado={selecaoDropdown} ativo={botaoAtivo} />
            </div>
        }
      </div>
    </div>
  );
}

export default RelatoriosDistribuicaoProcessos;
