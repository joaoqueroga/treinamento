import React from "react";
import '../../components/navegacao_topo/navegacao_topo.scss';
import { Button } from 'primereact/button'

import './style.scss';
import 'primeicons/primeicons.css';

import usePersistedState from '../../utils/usePersistedState';
import { ThemeProvider } from 'styled-components';
import { light } from '../../styles/themes/light';
import GlobalStyle from '../../styles/global';

function PaginaNaoAutorizada() {
    const [theme] = usePersistedState('theme', light);
    return (
        <ThemeProvider theme={theme}>
        <GlobalStyle/>
        <div>
            <div className="pagina_404">
                <div>
                    <h5><b>Acesso negado:</b></h5>
                    <h6>Perfil não autorizado.</h6>
                </div>
                <div className='conteiner-buttons'>
                    <Button style={{ width: '120px', margin: '0px 5px 10px' }} label='Voltar' icon='pi pi-chevron-left' onClick={() => { window.history.back() }} className="btn-2" />
                    <Button style={{ margin: '0px 0px 10px' }} label='Ir para página inicial' icon='pi pi-chevron-right' iconPos='right' onClick={() => { window.location.replace('/') }} className="btn-1" />
                </div>
            </div>

        </div>
        </ThemeProvider> 
    );
}

export default PaginaNaoAutorizada;