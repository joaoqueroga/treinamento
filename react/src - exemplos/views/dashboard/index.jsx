import { React, useState, useEffect } from 'react';
import './dashboard.scss';
import logoColorida from '../../images/logo_h.png';
import logoBranca from '../../images/Logo Horizontal Branca.png';
import { Button } from 'primereact/button';

import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';

function Dashboard() {

    const lista = [{
        id: '1000',
        code: 'f230fh0g3',
        name: 'Bamboo Watch',
        description: 'Product Description',
        image: 'bamboo-watch.jpg',
        price: 65,
        category: 'Accessories',
        quantity: 24,
        inventoryStatus: 'INSTOCK',
        rating: 5
    },
    {
        id: '1000',
        code: 'f230fh0g3',
        name: 'Bamboo Watch',
        description: 'Product Description',
        image: 'bamboo-watch.jpg',
        price: 65,
        category: 'Accessories',
        quantity: 24,
        inventoryStatus: 'INSTOCK',
        rating: 5
    },
    {
        id: '1000',
        code: 'f230fh0g3',
        name: 'Bamboo Watch',
        description: 'Product Description',
        image: 'bamboo-watch.jpg',
        price: 65,
        category: 'Accessories',
        quantity: 24,
        inventoryStatus: 'INSTOCK',
        rating: 5
    }];

    return (
        <div className='view'>
            <div className='view-body'>
                <div className='dashboard'>
                    <h3 className="titulo-dash no-select">Sistema de Gerenciamento Integrado</h3>
                    <img className='image-logo-colorida no-select' src={logoColorida} draggable={false}/>
                </div>
            </div>
        </div>
    );
}

export default Dashboard;