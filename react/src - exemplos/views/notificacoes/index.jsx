import { React, useState, useEffect } from 'react';
import { FilterMatchMode, FilterOperator } from 'primereact/api';
import { ProgressSpinner } from 'primereact/progressspinner';
import { Tag } from 'primereact/tag';
import { classNames } from 'primereact/utils';
import { BreadCrumb } from 'primereact/breadcrumb';
import { Button } from 'primereact/button';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Dropdown } from 'primereact/dropdown';

import api from '../../config/api';
import { getToken, getUserId } from '../../config/auth';

function Notificacoes() {
    const [loading, setLoading] = useState(true);
    const items = [
        { label: 'Portal do Servidor' },
        { label: 'Minhas Notificações' }
    ];
    const home = { icon: 'pi pi-home', url: '/inicio' }

    const [selectedProducts, setSelectedProducts] = useState(null);
    const [notificacoes, setNotificacoes] = useState([]);

    const isSelectable = (data) => data.lido_em == null;
    const isRowSelectable = (event) => (event.data ? isSelectable(event.data) : true);

    function get_notificacoes() {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {"aplicacao": "SGI", "id_usuario": getUserId()}

        api.post('/core/listar_notificacoes/', data, config)
        .then((res)=>{
            try {
                let x = JSON.parse(res.data);
                setNotificacoes(x.notificacao);
                setLoading(false);
            } catch (error) {
                console.warn("Erro ao buscar as notificações para este usuário");
            }
        })
    }

   useEffect(() => {
        get_notificacoes();
    }, []);

    const [filters, setFilters] = useState({
        'data_fmtd': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'categoria': { value: null, matchMode: FilterMatchMode.EQUALS},
        'titulo': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'texto': { value: null, matchMode: FilterMatchMode.CONTAINS },
    });

    const statusTemplate = (rowData) => { 
        //return <i className={classNames('pi', { 'text-green-500 pi-check-circle': rowData.lido_em, 'text-red-500 pi-times-circle': !rowData.lido_em })}></i>;

        switch (rowData.lido_em) {
            case null:
                return <i className='pi pi-exclamation-circle' style={{color: '#eab308'}}></i>                                           
            default:
                return <i className='pi pi-check' style={{color: '#22c55e'}}></i>
        }
    }

    const [categorias] = useState(['ferias', 'pecunia', 'folgas', 'processo-seletivo', 'meritocracia']);

    const categoriaFilterTemplate = (options) => {
        return (
            <Dropdown value={options.value} options={categorias} onChange={(e) => options.filterApplyCallback(e.value)} itemTemplate={categoriaItemTemplate} placeholder="Selecione" className="p-column-filter" showClear style={{ minWidth: '12rem' }} />
        );
    };

    const categoriaItemTemplate = (categoria) => {
        return <Tag value={categoria} className={getSeverity(categoria)} />;
    };

    const categoriaTemplate = (rowData) => {
        return <Tag value={rowData.categoria} className={getSeverity(rowData.categoria)} />;
    };

    const getSeverity = (categoria) => {
        switch (categoria) {
            case 'ferias':
                return 'tag-';

            case 'pecunia':
                return 'tag-pecunia';

            case 'folgas':
                return 'tag-folgas';

            case 'processo-seletivo':
                return 'tag-processo-seletivo';

            case 'meritocracia':
                return 'tag-meritocracia';

            default:
                return 'tag-cancelado';
        }
    };

    function marcar_lidas() {

        let ids  = [];
        selectedProducts.forEach( n => {
            ids.push({"id_notificacao": n.id_notificacao })
        });
 
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI", 
            "id_usuario": getUserId(), 
            "notificacoes": ids
        }

        api.post('/core/notificacoes_marcar_lido/', data, config)
        .then((res)=>{
            try {
                let x = JSON.parse(res.data);
                if(x.sucesso === 'S'){
                    get_notificacoes();
                }else{
                    console.warn("Erro ao marcar como lida");
                }
            } catch (error) {
                console.warn("Erro ao marcar como lida");
            }
        })
    }

    const diasColumnTemplate = (rowData) => {
        let datas = rowData.data_fmtd.split(' ');
        return(
            <span>
                {
                    datas.map((d, index)=>{
                        return d !== '' ?<span key={index}><Tag value={d} className="tag-lista-dias"></Tag>{' '}</span>:null
                    })
                }
            </span>
        )    
    }  

    return (
        <div className='view'>
        <div className="view-body">
        {
        loading?
        <div className="loading-pagina" ><ProgressSpinner/>Carregando...</div>
        :
            <div>
                <div className="header">
                    <BreadCrumb model={items} home={home}/>
                    <h6 className="titulo-header">Minhas Notificações</h6>

                </div>
                <div className="top-box">
                    <h1>Minhas Notificações</h1>
                </div>
                <div>
                    <Button 
                        label="Marcar como Lida" 
                        icon="pi pi-check" 
                        className="btn-1" 
                        onClick={() => { marcar_lidas() }} 
                    />
                </div>
                <DataTable id="tabela-notificacoes" value={notificacoes} selectionMode={true} paginator rows={10} selection={selectedProducts} isDataSelectable={isRowSelectable} onSelectionChange={(e) => setSelectedProducts(e.value)} filterDisplay="row"
                        filters={filters} globalFilterFields={['', 'notificacoes.data_fmtd', '', 'notificacoes.categoria', 'notificacoes.titulo', 'notificacoes.texto']} emptyMessage="Você não possui notificações.">
                    <Column selectionMode="multiple" headerStyle={{ width: '3rem' }}></Column>
                    <Column filter showFilterMenu={false} header="Recebido em"  body={diasColumnTemplate}  field="data_fmtd" sortable></Column>
                    <Column filter showFilterMenu={false} header="Categoria"    field="categoria" body={categoriaTemplate} filterElement={categoriaFilterTemplate} />
                    <Column filter showFilterMenu={false} header="Título"   field="titulo"></Column>
                    <Column filter showFilterMenu={false} header="Texto"    field="texto"></Column>
                    <Column sortable header="Lida" body={statusTemplate} className='col-centralizado'></Column>
                </DataTable>
            </div>
            }
            </div>
        </div>

    );
}

export default Notificacoes;