import './style.scss';
import React,{ useState, useEffect} from 'react';


export default function CalendarioFolgas(props){
    
    const [mesAtual, setMesatual] = useState(0);
    const [mes, setMes] = useState(0); // controla o mes apresentado no caledario 
    const [texto_mes, setTexto_mes] = useState(''); // mostra o texto no topo do calendario
   
    function apresentacaoMes(mes, ano) {
        let texto = '';
        switch (mes) {
            case 1:
                texto = 'JANEIRO ' + ano;
                break;
            case 2:
                texto = 'FEVEREIRO ' + ano;
                break;
            case 3:
                texto = 'MARÇO ' + ano;
                break;
            case 4:
                texto = 'ABRIL ' + ano;
                break;
            case 5:
                texto = 'MAIO ' + ano;
                break;
            case 6:
                texto = 'JUNHO ' + ano;
                break;
            case 7:
                texto = 'JULHO ' + ano;
                break;
            case 8:
                texto = 'AGOSTO ' + ano;
                break;
            case 9:
                texto = 'SETEMBRO ' + ano;
                break;
            case 10:
                texto = 'OUTUBRO ' + ano;
                break;
            case 11:
                texto = 'NOVEMBRO ' + ano;
                break;
            case 12:
                texto = 'DEZEMBRO ' + ano;
                break;
            default:
                break;
        }

        setTexto_mes(texto);
        
    }

    useEffect(() => {
        setMesatual(Number(props.mes_atual));
        apresentacaoMes(Number(props.mes_atual), props.ano_atual);
    }, [props.mes_atual]);
    
    //usa o index 17 para pegar um dia no meio do mes, garantindo que está no mes correto
    function proxMes(){
        if(mes < props.calendario.length-1){
            if(mesAtual === 12){
                setMesatual(1);
                apresentacaoMes(1, props.calendario[mes+1][17].ano);
            }else{
                setMesatual(mesAtual+1);
                apresentacaoMes(mesAtual+1, props.calendario[mes+1][17].ano);
            }
            setMes(mes+1);
        }
    }
    
    function anteMes(){
        if(mes > 0){
            if(mesAtual === 1){
                setMesatual(12);
                apresentacaoMes(12, props.calendario[mes-1][17].ano);
            }else{
                setMesatual(mesAtual-1);
                apresentacaoMes(mesAtual-1, props.calendario[mes-1][17].ano);
            }
            
            setMes(mes-1);
        }
    }

    function marcar_dia(index, dia) {
        let aux = props.calendario;

        if(aux[mes][index].selecionado == false){
            aux[mes][index].selecionado = true;
            props.selecionaDia(dia);
        }else{
            aux[mes][index].selecionado = false;
            props.removeDia(dia);
        }

        props.setCalendario([...aux]);
    }

    return(
        <div className="calendario-folgas" id='calendarios-folgas'>
            <div className='calendario-folgas-nav'>
            <button onClick={anteMes}>{'<'}</button> {texto_mes} <button onClick={proxMes}>{'>'}</button>
            </div>
            <div className="calendario-folgas-main" id='calendarios-folgas-main'>
                <span className='calendario-folgas-dia'>D</span>
                <span className='calendario-folgas-dia'>S</span>
                <span className='calendario-folgas-dia'>T</span>
                <span className='calendario-folgas-dia'>Q</span>
                <span className='calendario-folgas-dia'>Q</span>
                <span className='calendario-folgas-dia'>S</span>
                <span className='calendario-folgas-dia'>S</span>
                {
                    props.calendario[mes].map((d, index)=>{
                        return(
                            <span 
                                className={`calendario-folgas-data`}  
                                key={index}
                            >
                                <button
                                    className={`
                                        ${d.eh_mes_atual?"":"mes-anterior-cal"} 
                                        ${d.selecionado?"cal-botao-clicado":""}
                                    `}
                                    disabled={!d.pode_selecionar || !d.eh_mes_atual}
                                    onClick={()=>{marcar_dia(index, d)}}
                                >
                                    {d.dia}
                                </button>
                            </span>
                        )
                    })
                }
            </div>
        </div>
    )
}