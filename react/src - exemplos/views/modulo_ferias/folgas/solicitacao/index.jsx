import React, {useState, useEffect} from "react";
import { useLocation } from "react-router-dom";
import { ProgressSpinner } from 'primereact/progressspinner';
import { BreadCrumb } from 'primereact/breadcrumb';
import { Button } from 'primereact/button';
import { Tag } from 'primereact/tag';
import { Dialog } from 'primereact/dialog';
import { Checkbox } from 'primereact/checkbox';
import '../style.scss';
import api from "../../../../config/api";
import { getToken, getUserId , getEhDefensor} from "../../../../config/auth";

import CalendarioFolgas from "./calendario";
import Alertas from "../../../../utils/alertas";

function FolgasSolicitacao() {
    const location = useLocation();
    const terceiro = location.state;

    const [loading, setLoading] = useState(true);
    const items = [
        { label: 'Portal do Servidor' },
        { label: 'Solicitação de Folgas' }
    ];
    const home = { icon: 'pi pi-home', url: '/inicio' }


    const [disponiveis, setDisponiveis] = useState([]);

    // para guardas os dias escolhidos no calendario
    const [escolhidos, setEscolhidos] = useState([]);

    
    const [calendario, setCalendario] = useState([[]]);
    const [mes_atual, setMes_atual] = useState(0);
    const [ano_atual, setAno_atual] = useState('');

    const [reload, setReload] = useState(false);

    const [display, setDisplay] = useState(false);

    const [habilita_cal, setHabilita_cal] = useState(false);

    const [selectedSaldo, setSelectedSaldo] = useState([]);

    useEffect(() => {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = null;

        if(terceiro){ // se a solicitacao for para outro ou para próprio
            data = {
                "aplicacao": "sgi",
                "id_usuario": getUserId(),
                "id_funcionario":  terceiro.usuario.id_funcionario,
                "eh_folga": true
            }
        }else{
            data = {
                "aplicacao": "sgi",
                "id_usuario": getUserId(),
                "id_funcionario": getUserId(),
                "eh_folga": true
            }
        }
        
        api.post('/folgas/listar_calendario_folgas/', data , config)
        .then((res)=>{
            try {
                let x = JSON.parse(res.data);
                if (x.sucesso === 'S') {
                    setCalendario(x.calendario);
                    setMes_atual(x.calendario[0][17].mes); /// pega o mes do meio do primeiro calendario
                    setAno_atual(x.calendario[0][17].ano);
                    setDisponiveis(x.saldo_folga);
                    setEscolhidos([]);
                }else{
                    Alertas.erro(x.motivo);
                }
            } catch (error) {
                Alertas.erro(error);
            }
            setLoading(false);
        }).catch((error)=>{
            Alertas.erro(error);
        })
    }, [reload]);

    function solicitar_folgas() {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let dias = [];
        let saldos = [];

        selectedSaldo.map((s)=>{
            saldos.push({"id_folgas":s.id_folgas});
        })

        escolhidos.map((e)=>{
            dias.push({"data": e})
        })

        let data = null;

        if(terceiro){ // se a solicitacao for para outro ou para próprio
            data = {
                "aplicacao": "SGI",
                "id_usuario": getUserId(),
                "id_funcionario":  terceiro.usuario.id_funcionario,
                "datas": dias,
                "folgas_solicitadas": saldos,
                "arquivo": terceiro.caminho_arquivo
            }
        }else{
            data = {
                "aplicacao": "SGI",
                "id_usuario": getUserId(),
                "id_funcionario": getUserId(),
                "datas": dias,
                "folgas_solicitadas": saldos
            }
        }

        api.post('/folgas/gerar_solicitacao/', data , config)
        .then((res)=>{
            try {
                let x = JSON.parse(res.data);
                if (x.sucesso === 'S') {
                    Alertas.sucesso("Solicitado com sucesso");
                    setReload(!reload);
                }else{
                    Alertas.erro(x.motivo);
                }
            } catch (error) {
                Alertas.erro(error);
            }
            setDisplay(false);
        }).catch((error)=>{
            Alertas.erro(error);
            setDisplay(false);
        })
    }

    //adiciona a selecao do calendario
    function selecionaDia(dia) {
        setEscolhidos([...escolhidos, dia.data]);
    }

    //remove da selecao do calandario
    function removeDia(dia) {
        let array = escolhidos;
        let index = array.indexOf(dia.data);
        if (index > -1) {
            array.splice(index, 1);
        }
        setEscolhidos([...array]);
    }


    const renderFooter = (name) => {
        return (
            <div>
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{setDisplay(false)}} 
                    className="btn-2" />
        
                <Button  
                    label="Solicitar" 
                    icon="pi pi-check"  
                    className="btn-1"  
                    onClick={()=>solicitar_folgas()}              
                />   
            </div>    
        );
    }

    const onSaldoChange = (e) => {
        let _selectedSaldo = [...selectedSaldo];

        if (e.checked) {
            _selectedSaldo.push(e.value);
            document.getElementById("parent-"+e.value.id_folgas).classList.add("saldo-selecionado");
        }
        else {
            for (let i = 0; i < _selectedSaldo.length; i++) {
                const selectedCategory = _selectedSaldo[i];
                if (selectedCategory.id_folgas === e.value.id_folgas) {
                    _selectedSaldo.splice(i, 1);
                    document.getElementById("parent-"+e.value.id_folgas).classList.remove("saldo-selecionado");
                    break;
                }
            }
        }

        setHabilita_cal(_selectedSaldo.length > 0);
        setSelectedSaldo(_selectedSaldo);
    }

    return ( 
        <div className='view'>
        <div className="view-body">
        {
        loading?
        <div className="loading-pagina" ><ProgressSpinner/>Carregando...</div>
        :
        <div style={{height:"100%"}}>
            <div className="header">
                <BreadCrumb model={items} home={home}/>
                <h6 className="titulo-header">Solicitação de Folgas</h6>
            </div>
            <div style={{height:"100%"}}>
                <div className="top-box">
                    {
                        terceiro?
                            <h3>Solicitação de Folgas para {terceiro.usuario.nome}</h3>
                        :
                            <h1>Solicitação de Folgas</h1>
                    }
                    
                </div>
                <div className="area-solicitar-folgas">
                    <div className=" caixa-folgas-esq">
                        <h5>Disponíveis</h5>
                        <div className="solicitar-folgas-conteudo">
                            {
                                disponiveis.map((item, index)=>{
                                    return(
                                        <div className="item-folgas-disponiveis">
                                            <p className="vencimento-folga">
                                            {item.vencimento?`Vencimento: ${item.vencimento}`: "Sem vencimento"}
                                            </p>
                                            <div 
                                                key={index} 
                                                className="saldos-disponiveis" 
                                                id={'parent-'+item.id_folgas}
                                            >
                                                <label> {item.saldo} dias</label>
                                                <label> {item.descricao}</label>
                                                <Checkbox 
                                                    inputId={index} 
                                                    name="saldo" 
                                                    value={item} 
                                                    onChange={onSaldoChange} 
                                                    checked={
                                                        selectedSaldo.some((i) => i.id_folgas === item.id_folgas)
                                                    } 
                                                />
                                            </div>
                                        </div>
                                    )
                                })
                            }
                        </div>
                        
                    </div>
                    <div className="caixa-folgas-dir"
                        style={ !habilita_cal ? {userSelect: 'none', pointerEvents:'none', opacity:'0.4'} : {}}
                    >
                        <div>
                            <h5>Seleção de dias</h5>
                                <CalendarioFolgas
                                    className="calendario-folgas"
                                    selecionaDia={selecionaDia}
                                    removeDia={removeDia}
                                    setCalendario={setCalendario}
                                    calendario={calendario}
                                    mes_atual={mes_atual}
                                    ano_atual={ano_atual}
                                />
                        </div>
                        <div>
                            <h5>Resumo da seleção: {escolhidos.length} dias</h5>
                            <div className="selecao-folgas">
                                {
                                    escolhidos.map((i, index)=>{
                                        return(<Tag key={index} value={i} className="tag-lista-dias"></Tag>)
                                    })
                                }
                            </div>
                        </div>
                    </div>
                    <div></div>
                    <div className="botao-solicitar-ferias">
                        <Button 
                            label="Solicitar Folgas" 
                            className="btn-1"
                            icon="pi pi-check"
                            onClick={()=>setDisplay(true)}
                            disabled={escolhidos.length > 0 && selectedSaldo.length > 0 ? false : true}
                        />
                    </div>
                </div>
                </div>
            <Dialog 
                header="Confirmar Solicitação de Folgas?"
                visible={display}
                style={{ width: '50%' }} 
                footer={renderFooter('displayBasic')}
                onHide={() =>{setDisplay(false);}}>
                <p className="label-descricao"><strong>Total de dias solicitados: {escolhidos.length}</strong></p>
                <div className="selecao-folgas">
                    {
                        escolhidos.map((i, index)=>{
                            return(<span key={index}> {i} </span>)
                        })
                    }
                </div>
            </Dialog>
        </div>
        }
        </div>
        </div>
    );
}

export default FolgasSolicitacao;