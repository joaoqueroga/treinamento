import React, { useState, useEffect } from 'react';
import { ProgressSpinner } from 'primereact/progressspinner';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import api from '../../../../config/api';
import { getToken, getUserId } from '../../../../config/auth';
import { BreadCrumb } from 'primereact/breadcrumb';
import { useParams } from 'react-router-dom';
import { Button } from 'primereact/button';
import {useNavigate} from 'react-router-dom';
import { Tag } from 'primereact/tag';

function CreditarFolgaSaldo() {

    const home = { icon: 'pi pi-home', url: '/inicio' }
    const navigate = useNavigate();

    const [ nome, setNome ] = useState('');
    const [ dados, setDados ] = useState([]);
    const { id_funcionario } = useParams();
    const [ loading, setLoading ] = useState(false);
    const queryParameters = new URLSearchParams(window.location.search);
    const _nome = queryParameters.get("nome").toLowerCase();

    useEffect(() => {
        setNome(_nome.replace(/(^\w{1})|(\s+\w{1})/g, letra => letra.toUpperCase()));

        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            "id_funcionario": id_funcionario
        }
        api.post('/folgas/folgas_listar_folgas/', data, config)     
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(Array.isArray(x)){
                setDados(x);
                setLoading(false);
            }
        });

    }, [id_funcionario]);

    const items = [
        { label: 'Recursos Humanos' },
        { label: 'Férias' },
        { label: 'Creditar Folgas', url: '/solicitacoes/creditar-folgas-listagem'},
        { label: nome },
    ];

    const diasColumnVencimento = (rowData) => {
        return(<Tag value={rowData.vencimento} className="tag-lista-dias"></Tag>)
    }

    return ( 
        <div className='view'>
            <div className="view-body">
            {
            loading?
            <div className="loading-pagina" ><ProgressSpinner/>Carregando...</div>
            :
            <div>
                <div className="header">
                    <BreadCrumb model={items} home={home}/>
                
                    <h6 className="titulo-header">Histórico de Saldos</h6>
                    <Button 
                        label="Cadastrar Saldo"
                        icon="pi pi-plus" 
                        className="btn-1"
                        onClick={()=>navigate(`/folgas/creditar/${id_funcionario}?nome=${queryParameters.get("nome")}`)}
                    />
                </div>
                <div>
                    {dados.length > 0 ?
                        <h4>{ dados[0].nome} - Matrícula {dados[0].matricula}</h4>
                    :
                        null}
                    <DataTable
                        value={dados}
                        size="small"
                        className="tabela-servidores"
                        dataKey="id"
                        emptyMessage="..."
                        scrollable
                        scrollHeight="69vh"
                        selectionMode="single"
                    >
                        <Column 
                            field="descricao"
                            header="Origem"
                            style={{ flexGrow: 0, flexBasis: '20%' }}
                        />
                        <Column
                            field="dias" 
                            header="Dias Solicitados" 
                            style={{ flexGrow: 0, flexBasis: '20%' }}
                        />
                        <Column
                            body={diasColumnVencimento} 
                            header="Data de Vencimento" 
                            style={{ flexGrow: 0, flexBasis: '20%' }}
                        />
                        <Column
                            field="saldo" 
                            header="Saldo" 
                            style={{ flexGrow: 0, flexBasis: '20%' }}
                        />

                        <Column
                            field="data_solicitacao" 
                            header="Data Solicitada" 
                            style={{ flexGrow: 0, flexBasis: '20%' }}
                        />
                        
                    </DataTable>
                </div>

            </div>
            }
            </div>
        </div>
    );
}

export default CreditarFolgaSaldo;