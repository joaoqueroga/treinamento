import React, { useRef, useState, useEffect } from "react";
import { ProgressSpinner } from 'primereact/progressspinner';
import { BreadCrumb } from 'primereact/breadcrumb';
import { InputText } from 'primereact/inputtext';
import { InputNumber } from 'primereact/inputnumber';
import Swal from 'sweetalert2';
import api from "../../../../config/api";
import { getToken, getUserId } from "../../../../config/auth";
import { Button } from 'primereact/button';
import { useParams } from 'react-router-dom';
import SeletorData from "../../../../components/SeletorData";
import AnexosCreditarFolgas from "../creditar_anexos";

function CreditarFolgas() {

    const [ nome, setNome ] = useState('');
    const [loading, setLoading] = useState(true);
    const queryParameters = new URLSearchParams(window.location.search);
    const _nome = queryParameters.get("nome").toLowerCase();

    const home = { icon: 'pi pi-home', url: '/inicio' }

    const [descricao, setDescricao] = useState('');
    const [diasCredito, setDiasCredito] = useState(0);
    const toast = useRef(null);
    const [dataExpiracao, setDataExpiracao] = useState(null);
    const { id_funcionario } = useParams();

    const [cpf, setcpf] = useState("");
    const [arquivos, setArquivos] = useState([]);

    useEffect(() => {
        setNome(_nome.replace(/(^\w{1})|(\s+\w{1})/g, letra => letra.toUpperCase()));
        setLoading(false);
    }, []);

    const items = [
        { label: 'Recursos Humanos' },
        { label: 'Férias' },
        { label: 'Creditar Folgas', url: '/solicitacoes/creditar-folgas-listagem'},
        { label: nome , url: `/folgas/creditos/${id_funcionario}?nome=${queryParameters.get("nome")}`},
        { label: 'Cadastrar Saldo' },
    ];

    function  reloadArquivos() {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            "id_funcionario": Number(id_funcionario),
            "tipo_listagem": "COMPLETA"
        }
        api.post('/rh/servidor/',data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === "S"){
                setArquivos(x.funcionarios[0].arquivos)
            }
        })
    }

    const onUpload = () => {
        toast.current.show({ severity: 'info', summary: 'Success', detail: 'Arquivo Carregado' });
    }

    const [loading2, setLoading2] = useState(false);

    const salvar = () => {
    
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            "id_funcionario": id_funcionario,
            "descricao": descricao,
            "dias": diasCredito,
            "vencimento": dataExpiracao
        }

        let config = { headers: { "Authorization": `Bearer ${getToken()}` } }

        api.post("folgas/folgas_gerar_saldo/", data, config).then((res) => {
            let x = JSON.parse(res.data);
            if (x.sucesso === "S") {
                Swal.fire({
                    icon: 'success',
                    title: "Sucesso",
                    text: `atualizado`,
                    confirmButtonText: 'fechar',
                })
                //reload();

            } else {
                Swal.fire({
                    icon: 'error',
                    title: "Erro",
                    text: `${x.motivo}`,
                    confirmButtonText: 'fechar',
                })
            }


        })

    }

    return (
        <div className='view'>
            <div className="view-body">
                {
                    loading ?
                        <div className="loading-pagina" ><ProgressSpinner />Carregando...</div>
                        :
                        <div>
                            <div className="header">
                                <BreadCrumb model={items} home={home} />
                                <h6 className="titulo-header">Creditar Folgas</h6>

                            </div>
                            <div>
                                <span className="field grupo-input-label">
                                    Descrição
                                    <InputText value={descricao} onChange={(e) => setDescricao(e.target.value)} />
                                </span>

                                <span className="field grupo-input-label">
                                    Dias de Folga a Creditar
                                    <InputNumber
                                        inputId="vertical"
                                        value={diasCredito}
                                        onValueChange={(e) => setDiasCredito(e.target.value)}
                                        mode="decimal"
                                        showButtons 
                                        buttonLayout="horizontal"
                                        decrementButtonClassName="p-button-danger" 
                                        incrementButtonClassName="p-button-success" 
                                        incrementButtonIcon="pi pi-plus" 
                                        decrementButtonIcon="pi pi-minus" 
                                        min={0}
                                    />
                    
                                </span>

                                <span className="field grupo-input-label">
                                    {/* Anexo (Opcional)
                    <FileUpload 
                        name="demo[]" 
                        url="https://primefaces.org/primereact/showcase/upload.php" 
                        onUpload={onUpload}  
                        accept="image/*" 
                        maxFileSize={5000000}
                        emptyTemplate={<p className="m-0">Arraste o arquivo aqui.</p>} 
                    /> */}
                                    <AnexosCreditarFolgas
                                        id_funcionario={Number(id_funcionario)}
                                        cpf={{ get: cpf, set: setcpf }}
                                        arquivos={{ get: arquivos, set: setArquivos }}
                                        reloadArquivos={reloadArquivos}
                                    />
                                </span>

                                <span className="field grupo-input-label">
                                    Data de Expiração (Opcional)
                                    <SeletorData
                                        get={dataExpiracao}
                                        set={setDataExpiracao}
                                    />
                                </span>

                                <Button label="Creditar" className="btn-1" loading={loading2} onClick={salvar} />
                            </div>
                        </div>
                }
            </div>
        </div>
    );
}

export default CreditarFolgas;