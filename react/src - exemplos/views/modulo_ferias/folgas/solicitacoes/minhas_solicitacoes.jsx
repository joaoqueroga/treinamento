import React, {useState} from "react";
import { DataTable } from 'primereact/datatable';
import { InputTextarea } from 'primereact/inputtextarea';
import { Dialog } from 'primereact/dialog';
import { Button } from 'primereact/button';
import { Column } from 'primereact/column';
import { Dropdown } from 'primereact/dropdown';
import { Checkbox } from 'primereact/checkbox';
import { Tag } from 'primereact/tag';
import { FaRegCircle, FaTimesCircle, FaArrowRight, FaCheckCircle } from "react-icons/fa";
import { FilterMatchMode } from 'primereact/api';
import Alertas from "../../../../utils/alertas";
import api from "../../../../config/api";
import { getToken, getUserId } from "../../../../config/auth";

import SeletorDataFuncoes from "../../../../components/SeletorDataFuncoes";
import ComprovanteSolicitacao from "../../componentes/relatorio/comprovante";

function FolgasMinhasSolicitacoes(props) {

    const FileDownload = require('js-file-download');

    const [filters] = useState({
        'datas_lista': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'status': { value: null, matchMode: FilterMatchMode.EQUALS }
    });

    const [dialog_ver, setDialog_ver] = useState(false);
    const [dialog_interromper, setDialog_interromper] = useState(false);
    const [dialog_editar, setDialog_editar] = useState(false);

    const [id_solicitacao, setId_solicitacao] = useState(null);
    const [dialog_cancelar, setDialog_cancelar] = useState(false);
    const [obrigatorio, setObrigatorio] = useState(false);
    const [descricao_cancelar, setDescricao_cancelar] = useState('');

    const [diasSelecionados, setdiasSelecionados] = useState([]);
    const [datas_modificar, setDatas_modificar] = useState([]);

    const [status, setStatus] = useState([]);

    const [status_final, setStatus_final] = useState('');

    const [ver_dias, setVer_dias] = useState([]);
    const [historico, setHistorico] = useState([]);
    const [nome, setNome] = useState('');
  

    const [texto_indeferimento, setTexto_indeferimento] = useState(null);
    const [arquivo_indeferimento, setArquivo_indeferimento] = useState(null);
    const [texto_cancelamanto, setTexto_cancelamento] = useState(null);

    function visualizar_reprovacao(d) {
        setId_solicitacao(d.id_folgas_solicitacao);
        setVer_dias(d.datas);
        setDialog_cancelar(true);
    }

    function visualizar_interrupcao(d) {
        setId_solicitacao(d.id_folgas_solicitacao);
        setVer_dias(d.datas);
        setDialog_interromper(true);
    }

    function visualizar_edicao(d) {
        setId_solicitacao(d.id_folgas_solicitacao);
        let aux = [];
        d.datas.map((d)=>{
            aux.push({"atual": d.data, "nova": null})
        })
        setDatas_modificar([...aux]);
        setDialog_editar(true);
    }

    function visualizar(d) {
        setDialog_ver(true);
        setVer_dias(d.datas);
        setStatus_final(d.status);
        setHistorico(d.historico);
        setNome(d.nome);

        setArquivo_indeferimento(d.indeferimento_arquivo);
        setTexto_indeferimento(d.indeferimento_observacao);
        setTexto_cancelamento(d.motivo_cancelamento);

        let tipo_nstatus = 0; /// 0: passa pela Corregedoria  1:nao passa pela corregedoria
        let ultimo_status = '';
        let ultima_descri = null;
        d.historico.map((h)=>{
            if(h.status !== 'DEFERIDO' && h.status !== 'INDEFERIDO'){
                ultimo_status = h.status;
                if(h.status === "ALTERADO"){
                    ultima_descri = h.observacao;
                }
            }
        })
        if(ultimo_status === "CANCELAMENTO"){ // cancelamento nao passa pela CG
            tipo_nstatus = 1;
        }else if(ultima_descri){
            if(ultima_descri.includes("EXCLUSÃO"))tipo_nstatus = 1;  
            // se a ultima descricao nao tiver excluir tem que passar pela CG
        }


        // status 1: aprovado, 2: sem avaliacao, 3: rejeitado, 4: setor a aprovar
        let nstatus = [];
        if(d.eh_defensor){
            nstatus.push({id: 1, aprovador: "Solicitação", status: 1});
            if(tipo_nstatus === 0){
                nstatus.push({id: 2, aprovador: "Aprovação da Corregedoria Geral", status: d.autorizado_cg?1:2})
            }
            nstatus.push({id: 3, aprovador: "Aprovação do Gabinete do Defensor Público Geral", status: d.autorizado_gdpg?1:2})
        }else{
            nstatus.push({id: 1, aprovador: "Solicitação", status: 1});
            nstatus.push({id: 2, aprovador: "Aprovação do Setor", status: d.autorizado_chefia?1:2});
            if(tipo_nstatus === 0){
                nstatus.push({id: 3, aprovador: "Aprovação da Corregedoria Geral", status: d.autorizado_cg?1:2})
            }
            nstatus.push({id: 4, aprovador: "Aprovação do Gabinete do Subdefensor Público Geral", status: d.autorizado_gspg?1:2});
        }

        if(d.status === 'INDEFERIDO'){ // encontra o que rejeitou
            for (let i = 0; i < nstatus.length; i++) {
                if(nstatus[i].status === 2){
                    nstatus[i].status = 3;
                    break;
                }
            }
        }else if(d.status === 'TRAMITANDO'){
            for (let i = 0; i < nstatus.length; i++) {
                if(nstatus[i].status === 2){
                    nstatus[i].status = 4;
                    break;
                }
            }
        }

        setStatus(nstatus);
    }

    const statusRowFilterTemplate = (options) => {
        return <Dropdown value={options.value} options={statuses} onChange={(e) => options.filterApplyCallback(e.value)} itemTemplate={statusItemTemplate} placeholder="Filtrar por status" className="p-column-filter" showClear />;
    }
    const statusItemTemplate = (option) => {
        return <span>{option}</span>;
    }
    const statuses = [
        'TRAMITANDO', 'APROVADO', 'INDEFERIDO', 'CANCELADO'
    ];
    const statusColumnTemplate = (rowData) => {
        switch (rowData.status) {
            case 'APROVADO':
                return <Tag value={rowData.status} className="tag-aprovado"></Tag>;

            case 'INDEFERIDO':
                return <Tag value={rowData.status} className="tag-indeferido"></Tag>;

            case 'CANCELADO':
                return <Tag value={rowData.status} className="tag-cancelado"></Tag>;

            case 'TRAMITANDO':
                return <Tag value={rowData.status} className="tag-tramitando"></Tag>;

            default:
                return <>{  rowData.status2 === 'ALTERAÇÃO'|| 
                            rowData.status2 === 'INTERRUPÇÃO' || 
                            rowData.status2 === 'CANCELAMENTO'?
                            <Tag 
                                value={<i className="pi pi-pencil"></i>} 
                                className="tag-tramitando">
                            </Tag>
                        : null }
                        <Tag value={rowData.status} className="tag-tramitando"></Tag>
                    </>;
               
        }
    }

    const diasColumnTemplate = (rowData) => {
        let datas = rowData.datas_lista.split(' ');       
        return(
            <span>
                {
                    datas.map((d, index)=>{
                        return d !== '' ?<span key={index}><Tag value={d} className="tag-lista-dias"></Tag>{' '}</span>:null
                    })
                }
            </span>
        )    
    }

    function limpar_dados() {
        setDescricao_cancelar('');
        setdiasSelecionados([]);
        setDatas_modificar([]);
    }

    function salvar_cancelamento() {
        if(descricao_cancelar){
            let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
            let data = {
                "aplicacao": "SGI",
                "id_usuario": getUserId(),
                "id_folgas_solicitacao": id_solicitacao,
                "observacao": descricao_cancelar,
            };
            api.post('/folgas/cancelar_solicitacao/', data, config)     
            .then((res)=>{
                try {
                    let x = JSON.parse(res.data);
                    if(x.sucesso === 'S'){
                        Alertas.sucesso("Solicitação enviada!");
                        props.reload.set(!props.reload.get);
                        limpar_dados();
                    }else{
                        Alertas.erro(x.motivo);
                    }
                } catch (error) {
                    Alertas.erro(error);
                }
                setDialog_cancelar(false);
            }).catch((erro)=>{
                setDialog_cancelar(false);
                Alertas.erro(erro);
            })
        }else{
            setObrigatorio(true);
        }
    }

    function baixarArquivo(nome_arquivo) {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}, responseType: 'blob'}
        let data = {
            nome: nome_arquivo
        }
        api.post('core/download_pdf', data, config)
        .then((res)=>{
            FileDownload(res.data, `${nome_arquivo}.pdf`);
        }).catch((err)=>{
            let msg = "Falha no download";
            if(err.response.status === 401){
                msg = "Não autorizado";
            }
            Alertas.erro(msg);
        })
    }

    function salvar_interromper() {
        if(diasSelecionados.length > 0 && descricao_cancelar){
            let aux = [];
            diasSelecionados.map((d)=>{
                aux.push({"excluir": true, "data": d})
            })
            let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
            let data = {
                "aplicacao": "SGI",
                "id_usuario": getUserId(),
                "id_folgas_solicitacao": id_solicitacao,
                "datas": aux,
                "observacao": descricao_cancelar,
            };
            api.post('/folgas/alterar_solicitacao/', data, config)     
            .then((res)=>{
                try {
                    let x = JSON.parse(res.data);
                    if(x.sucesso === 'S'){
                        Alertas.sucesso("Solicitação enviada!");
                        props.reload.set(!props.reload.get);
                        limpar_dados();
                    }else{
                        Alertas.erro(x.motivo);
                    }
                } catch (error) {
                    Alertas.erro(error);
                }
                setDialog_interromper(false);
            }).catch((erro)=>{
                setDialog_interromper(false);
                Alertas.erro(erro);
            })
        }else{
            setObrigatorio(true);
        }
    }

    function salvar_editar() {
        if(datas_modificar.length > 0 && descricao_cancelar){
            let aux = [];
            datas_modificar.map((d)=>{
                if(d.nova !== null){
                    aux.push({"excluir": false, "data": d.atual, "data_alteracao":d.nova});
                }
            })
            let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
            let data = {
                "aplicacao": "SGI",
                "id_usuario": getUserId(),
                "id_folgas_solicitacao": id_solicitacao,
                "datas": aux,
                "observacao": descricao_cancelar,
            };

            api.post('/folgas/alterar_solicitacao/', data, config)     
            .then((res)=>{
                try {
                    let x = JSON.parse(res.data);
                    if(x.sucesso === 'S'){
                        Alertas.sucesso("Solicitação enviada!");
                        props.reload.set(!props.reload.get);
                        limpar_dados();
                    }else{
                        Alertas.erro(x.motivo);
                    }
                } catch (error) {
                    Alertas.erro(error);
                }
                setDialog_editar(false);
            }).catch((erro)=>{
                setDialog_editar(false);
                Alertas.erro(erro);
            })
           
        }else{
            setObrigatorio(true);
        }
    }

    

    const renderFooterCancelar = () => {
        return (
            <div>
                {obrigatorio?<p className='mensagem-erro'>Informe os campos obrigatórios *</p>:null}
                <Button  
                    label="Cancelar Solicitação" 
                    icon="pi pi-check"  
                    className="btn-2"  
                    onClick={()=>{salvar_cancelamento()}}            
                />   
            </div>    
        );
    }

    const renderFooterInterromper = () => {
        return (
            <div>
                {obrigatorio?<p className='mensagem-erro'>Informe os campos obrigatórios *</p>:null}
                <Button  
                    label="Enviar Solicitação" 
                    icon="pi pi-check"  
                    className="btn-2"  
                    onClick={()=>{salvar_interromper()}}            
                />   
            </div>    
        );
    }

    const renderFooterEditar = () => {
        return (
            <div>
                {obrigatorio?<p className='mensagem-erro'>Informe os campos obrigatórios *</p>:null}
                <Button  
                    label="Enviar Solicitação" 
                    icon="pi pi-check"  
                    className="btn-2"  
                    onClick={()=>{salvar_editar()}}            
                />   
            </div>    
        );
    }

    const irBodyTemplate = (rowData) => {
        if( rowData.status == 'CANCELADO' ){
            return(
                <span className="botoes-acao-folgas">
                    <Button 
                        title="Visualizar"
                        icon="pi pi-eye"
                        className="btn-darkblue"
                        onClick={()=>visualizar(rowData)}
                    />
                    <ComprovanteSolicitacao dados={rowData}/>
                </span>
            )
        }else if( rowData.status == 'APROVADO'){
            return(
                <span className="botoes-acao-folgas">
                    <Button 
                        title="Visualizar"
                        icon="pi pi-eye"
                        className="btn-darkblue"
                        onClick={()=>visualizar(rowData)}
                    />
                    <ComprovanteSolicitacao dados={rowData}/>
                    <Button 
                        title="Interromper Dias"
                        icon="pi pi-calendar-times"
                        className="btn-darkblue"
                        onClick={()=>visualizar_interrupcao(rowData)}
                    />
                    <Button 
                        title="Alterar Folgas"
                        icon="pi pi-pencil"
                        className="btn-darkblue"
                        onClick={()=>visualizar_edicao(rowData)}
                    />
                    <Button 
                        title="Cancelar Solicitação"
                        icon="pi pi-times"
                        className="btn-red"
                        onClick={()=>visualizar_reprovacao(rowData)}
                    />
                </span>
            )
        }else if(rowData.status == 'INDEFERIDO'){
            return(
                <span className="botoes-acao-folgas">
                    <Button 
                        title="Visualizar"
                        icon="pi pi-eye"
                        className="btn-darkblue"
                        onClick={()=>visualizar(rowData)}
                    />
                    <ComprovanteSolicitacao dados={rowData}/>
                </span>
            )
        }else{
            return(
                <span className="botoes-acao-folgas">
                    <Button 
                        title="Visualizar"
                        icon="pi pi-eye"
                        className="btn-darkblue"
                        onClick={()=>visualizar(rowData)}
                    />
                    <ComprovanteSolicitacao dados={rowData}/>
                </span>
            )
        }
    }

    const onDiasChange = (e) => {
        let dias_aux = [...diasSelecionados];
        if (e.checked)
            dias_aux.push(e.value);
        else
            dias_aux.splice(dias_aux.indexOf(e.value), 1);

        setdiasSelecionados(dias_aux);
    }

    function seleciona_data(d, index) {
        let aux = datas_modificar;
        aux[index].nova = d;
        setDatas_modificar([...aux]);
    }

    return ( 
        <div>
            <DataTable 
                value={props.minhas_solicitacoes} 
                responsiveLayout="scroll"
                scrollable 
                scrollHeight="60vh"
                filters={filters}
                filterDisplay="row"
                paginator
                paginatorTemplate="CurrentPageReport FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink"
                currentPageReportTemplate="{last} de {totalRecords}" 
                rows={10}
            >
                <Column 
                    field="datas_lista" 
                    header="Datas Solicitadas"
                    body={diasColumnTemplate}
                    filter 
                    filterPlaceholder="Buscar" 
                    style={{ flexBasis: '50%' }}
                    showFilterMenu={false}
                />

                <Column 
                    field="dias" 
                    header="Total Dias"
                    className="col-centralizado"
                    style={{ flexBasis: '10%' }}
                />
                <Column 
                    field="status" 
                    header="Status"
                    className="col-centralizado"
                    body={statusColumnTemplate} 
                    filter 
                    filterElement={statusRowFilterTemplate} 
                    filterPlaceholder="Buscar"
                    style={{ flexBasis: '20%' }}
                    showFilterMenu={false}
                /> 
                <Column 
                    header="Ações" 
                    body={irBodyTemplate} 
                    className="col-centralizado"
                    style={{ flexBasis: '20%' }}
                />
            </DataTable>

            <Dialog 
                header="Solicitação de Folgas"
                visible={dialog_ver}
                style={{ width: '75%' }}
                onHide={() =>{setDialog_ver(false);}}
            >
                
                <div>
                    <p>Dias solicitados</p>
                    <ul>
                        {
                            ver_dias.map((i, index)=>{
                                return(<li key={index}> {i.data} </li>)
                            })
                        }
                    </ul>
                </div>

                <div>
                    <p>Status da tramitação</p>

                    <ul className="folgas-lista-status">
                        {
                            status.map((s)=>{
                                return(
                                    <li key={s.id}>
                                        {
                                            s.status === 1
                                            ?<FaCheckCircle className="icone-folgas-status"/>
                                            :s.status === 2?
                                            <FaRegCircle className="icone-folgas-status"/>
                                            :s.status === 3?
                                            <FaTimesCircle className="icone-folgas-status-red"/>
                                            :<FaRegCircle className="icone-folgas-status"/>
                                        }
                                        {s.aprovador}
                                    </li>
                                )
                            })
                        }
                    </ul>
                </div>

                <div>
                    <p>Histórico</p>
                    <ul className="folgas-lista-status">
                        <li id="folgas-listas-historico">
                            <b>1 - SOLICITAÇÃO DE {nome}</b>
                        </li>
                        {
                            historico.map((h, index)=>{
                                return(
                                    <div key={index}>
                                    <li id="folgas-listas-historico">
                                        <div>
                                        <div><b>{index+2} - {h.status} POR {h.nome} NA DATA {h.data}</b></div>
                                        {h.observacao?<p id="descricao-historico"><i>{h.observacao}</i></p>:null}
                                        </div>
                                    </li>
                                    </div>
                                )
                            })
                        }
                    </ul>
                </div>

                {
                    texto_indeferimento?
                        <div>
                            <p>Justificativa de indeferimento</p>
                            <ul className="folgas-lista-status">
                                <li><i>- {texto_indeferimento}</i></li>
                                {
                                    arquivo_indeferimento?
                                    <li>
                                        <a href="#" onClick={()=>baixarArquivo(arquivo_indeferimento)} >
                                            Arquivo em anexo 
                                        </a> 
                                    </li>
                                    :null
                                }
                            </ul>
                        </div>
                    :null
                }

                {texto_cancelamanto?
                    <div>
                        <p>Motivo do Cancelamento</p>
                        <ul className="folgas-lista-status"></ul>
                            <li><i>{texto_cancelamanto}</i></li>
                            <br />

                    </div> 
                :null

                }       

                <div>
                    <p>Resultado da Solicitação</p>
                    <ul className="folgas-lista-status">
                        <li><b>{status_final}</b></li>
                    </ul>
                </div>
            
            </Dialog>

            <Dialog 
                header={"Cancelamento da Solicitação Nº " + id_solicitacao}
                visible={dialog_cancelar}
                style={{ width: '50%' }}
                footer={renderFooterCancelar}
                onHide={() =>{setDialog_cancelar(false); setObrigatorio(false); limpar_dados()}}
            >
                <div className="folgas-painel-deferimento">
                    <div>
                        <p>Dias solicitados</p>
                        <ul>
                            {
                                ver_dias.map((i, index)=>{
                                    return(<li key={index}> {i.data} </li>)
                                })
                            }
                        </ul>
                    </div>
                    <div style={{width: "100%"}}>
                        <p>Motivo *</p>
                        <InputTextarea 
                            rows={5} cols={30} 
                            id="folgas-texto-deferimento"
                            style={{height: "40px"}}
                            value={descricao_cancelar}
                            onChange={e=>setDescricao_cancelar(e.target.value)}
                        />
                    </div>
                </div>
            </Dialog>

            <Dialog 
                header={"Interromper Dias de Folgas"}
                visible={dialog_interromper}
                style={{ width: '50%' }}
                footer={renderFooterInterromper}
                onHide={() =>{setDialog_interromper(false); setObrigatorio(false); limpar_dados()}}
            >
                <div className="folgas-painel-deferimento">
                    <div>
                        <p>Selecione os dias que deseja interromper</p>
                        <ul>
                            {
                                ver_dias.map((i, index)=>{
                                    return (
                                        <div key={index} className="field-checkbox">
                                            <Checkbox 
                                                inputId={i.data}
                                                name="dias"
                                                value={i.data}
                                                onChange={onDiasChange}
                                                checked={diasSelecionados.some((item) => item === i.data)} 
                                            />
                                            {' '}
                                            <label htmlFor={i.data}>{i.data}</label>
                                        </div>
                                    )
                                })
                            }
                        </ul>
                    </div>
                    <div style={{width: "100%"}}>
                        <p>Motivo *</p>
                        <InputTextarea 
                            rows={5} cols={30} 
                            id="folgas-texto-deferimento"
                            style={{height: "40px"}}
                            value={descricao_cancelar}
                            onChange={e=>setDescricao_cancelar(e.target.value)}
                        />
                    </div>
                </div>
            </Dialog>

            <Dialog 
                header={"Alterar Dias de Folgas"}
                visible={dialog_editar}
                style={{ width: '50%' }}
                footer={renderFooterEditar}
                onHide={() =>{setDialog_editar(false); setObrigatorio(false); limpar_dados()}}
            >
                <div className="folgas-painel-deferimento">
                    <div>
                        <p>Defina os novos dias de folgas</p>
                        <div>
                            <div id="folgas-troca-datas">
                                <span id="folgas-troca-datas-cal">Data atual</span>
                                <span id="folgas-troca-datas-ico"></span>
                                <span id="folgas-troca-datas-cal">Nova data</span>
                            </div>
                            {
                                datas_modificar.map((i, index)=>{
                                    return (
                                        <div key={index} id="folgas-troca-datas">
                                            <span id="folgas-troca-datas-rot">
                                                {i.atual} 
                                            </span>
                                            <span id="folgas-troca-datas-ico">
                                                <FaArrowRight size={20}/>
                                            </span>
                                            <span id="folgas-troca-datas-cal">
                                                <SeletorDataFuncoes 
                                                    seleciona={seleciona_data}
                                                    index={index}
                                                />
                                            </span>
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </div>
                    <br/>
                    <div style={{width: "100%"}}>
                        <p>Motivo *</p>
                        <InputTextarea 
                            rows={5} cols={30} 
                            id="folgas-texto-deferimento"
                            style={{height: "40px"}}
                            value={descricao_cancelar}
                            onChange={e=>setDescricao_cancelar(e.target.value)}
                        />
                    </div>
                </div>
            </Dialog>
        </div>
    );
}

export default FolgasMinhasSolicitacoes;