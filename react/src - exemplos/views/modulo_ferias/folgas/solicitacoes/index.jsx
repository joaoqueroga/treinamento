import React, {useState, useEffect} from "react";
import { ProgressSpinner } from 'primereact/progressspinner';
import { SelectButton } from 'primereact/selectbutton';
import '../../style.scss';
import api from "../../../../config/api";
import { getToken, getUserId } from "../../../../config/auth";
import Alertas from "../../../../utils/alertas";

import FolgasMinhasSolicitacoes from "./minhas_solicitacoes";
import FolgasAprovacao from "./aprovacoes";

function SolicitacoesFolgas() {
    const [loading, setLoading] = useState(true);
    const [reload, setReload] = useState(true);

    const [minhas_solicitacoes, setMinhas_solicitacoes] = useState([]);
    const [aprovar_solicitacoes, setAprovar_solicitacoes] = useState([]);

    const options = [
        {label: "Minhas solicitações", value: 1},
        {label: "Solicitações a aprovar", value: 2},
    ];

    const [tipo_solicitacao, setTipo_solicitacao] = useState(1);
   
    useEffect(() => {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            "id_funcionario": getUserId()
        };
        api.post('/folgas/listar_solicitacoes/', data, config)     
        .then((res)=>{
            try {
                let x = JSON.parse(res.data);
                if (Array.isArray(x)) {
                    let solicitacoes = [];
                    let aprovacoes = [];
                    x.map((i)=>{
                        i.ferias_ou_folgas = 2;
                        if(i.mostrar_deferir_indeferir){
                            aprovacoes.push(i);
                        }else{
                            solicitacoes.push(i);
                        }
                    })
                    setMinhas_solicitacoes([...solicitacoes]);
                    setAprovar_solicitacoes([...aprovacoes]);
                }else{
                    Alertas.erro("A resposta não é uma estrutura de dados válida");
                }
            } catch (error) {
                Alertas.erro(error);
            }
            setLoading(false);
        })
        
    }, [reload]);

    return ( 
        <div>
        <div className="body-solicitacao-folgas">
        {
        loading?
        <div className="loading-pagina" ><ProgressSpinner/>Carregando...</div>
        :
        <div>
            <div className="view-solicitacao-folgas">
                <div className="view-solicitacao-folgas-menus">
                    <SelectButton 
                        value={tipo_solicitacao}
                        options={options}
                        optionLabel="label"
                        onChange={(e) => {setTipo_solicitacao(e.value)}} 
                    />
                </div>
                {
                    tipo_solicitacao === 1?
                    <FolgasMinhasSolicitacoes
                        minhas_solicitacoes={minhas_solicitacoes}
                        reload={{get: reload, set: setReload}}
                    />
                    :tipo_solicitacao === 2?
                    <FolgasAprovacao
                        aprovacoes={aprovar_solicitacoes}
                        reload={{get: reload, set: setReload}}
                    />
                    :null
                }
            </div>
        </div>
        }
        </div>
        </div>
    );
}

export default SolicitacoesFolgas;