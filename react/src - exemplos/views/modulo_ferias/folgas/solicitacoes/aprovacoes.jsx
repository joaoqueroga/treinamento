import React, {useState} from "react";
import { DataTable } from 'primereact/datatable';
import { Dialog } from 'primereact/dialog';
import { Button } from 'primereact/button';
import { Tag } from 'primereact/tag';
import { Column } from 'primereact/column';
import { InputTextarea } from 'primereact/inputtextarea';
import { FaRegCircle, FaTimesCircle, FaCheckCircle } from "react-icons/fa";
import { Dropdown } from 'primereact/dropdown';
import { BiCloudUpload } from "react-icons/bi";
import { FilterMatchMode } from 'primereact/api';
import api from "../../../../config/api";
import { getToken, getUserId } from "../../../../config/auth";
import Alertas from "../../../../utils/alertas";



function FolgasAprovacao(props) {

    const [dialog_ver, setDialog_ver] = useState(false);
    const [dialog_aprovar, setDialog_aprovar] = useState(false);
    const [dialog_reprovar, setDialog_reprovar] = useState(false);

    const [id_solicitacao, setId_solicitacao] = useState(null);

    const [status, setStatus] = useState([]);

    const [ver_dias, setVer_dias] = useState([]);
    const [nome_solicitante, setNome_solicitante] = useState('');
    const [mat_solicitante, setMat_solicitante] = useState('');
    const [lotacao_solicitante, setLotacao_solicitante] = useState('');
    const [eh_defensor, setEh_defensor] = useState(false);
    const [historico, setHistorico] = useState([]);

    const [file, setFile] = useState(null);
    const [descricao_indeferir, setDescricao_indeferir] = useState('');
    const [obrigatorio, setObrigatorio] = useState(false);

    const [filters] = useState({
        'nome': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'datas_lista': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'status2': { value: null, matchMode: FilterMatchMode.EQUALS }
    });

    function visualizar_reprovacao(d) {
        setId_solicitacao(d.id_folgas_solicitacao);
        setLotacao_solicitante(d.descricao_lotacao);
        setNome_solicitante(d.nome);
        setMat_solicitante(d.matricula);
        setEh_defensor(d.eh_defensor);
        setVer_dias(d.datas);
        setDialog_reprovar(true);
    }

    function visualizar_aprovacao(d) {
        setId_solicitacao(d.id_folgas_solicitacao);
        setLotacao_solicitante(d.descricao_lotacao);
        setNome_solicitante(d.nome);
        setMat_solicitante(d.matricula);
        setEh_defensor(d.eh_defensor);
        setVer_dias(d.datas);
        setDialog_aprovar(true);
    }

    function visualizar(d) {
        setLotacao_solicitante(d.descricao_lotacao);
        setNome_solicitante(d.nome);
        setMat_solicitante(d.matricula);
        setEh_defensor(d.eh_defensor);
        setHistorico(d.historico);
        setDialog_ver(true);
        setVer_dias(d.datas);
        
        let tipo_nstatus = 0; /// 0: passa pela Corregedoria  1:nao passa pela corregedoria
        let ultimo_status = '';
        let ultima_descri = null;
        d.historico.map((h)=>{
            if(h.status !== 'DEFERIDO' && h.status !== 'INDEFERIDO'){
                ultimo_status = h.status;
                if(h.status === "ALTERADO"){
                    ultima_descri = h.observacao;
                }
            }
        })
        if(ultimo_status === "CANCELAMENTO"){ // cancelamento nao passa pela CG
            tipo_nstatus = 1;
        }else if(ultima_descri){
            if(ultima_descri.includes("EXCLUSÃO"))tipo_nstatus = 1;  
            // se a ultima descricao nao tiver excluir tem que passar pela CG
        }


        // status 1: aprovado, 2: sem avaliacao, 3: rejeitado, 4: setor a aprovar
        let nstatus = [];
        if(d.eh_defensor){
            nstatus.push({id: 1, aprovador: "Solicitação", status: 1});
            if(tipo_nstatus === 0){
                nstatus.push({id: 2, aprovador: "Aprovação da Corregedoria Geral", status: d.autorizado_cg?1:2})
            }
            nstatus.push({id: 3, aprovador: "Aprovação do Gabinete do Defensor Público Geral", status: d.autorizado_gdpg?1:2})
        }else{
            nstatus.push({id: 1, aprovador: "Solicitação", status: 1});
            nstatus.push({id: 2, aprovador: "Aprovação do Setor", status: d.autorizado_chefia?1:2});
            if(tipo_nstatus === 0){
                nstatus.push({id: 3, aprovador: "Aprovação da Corregedoria Geral", status: d.autorizado_cg?1:2})
            }
            nstatus.push({id: 4, aprovador: "Aprovação do Gabinete do Subdefensor Público Geral", status: d.autorizado_gspg?1:2});
        }

        setStatus(nstatus);
    }

    function deferir() {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            "id_folgas_solicitacao": id_solicitacao
        };
        api.post('/folgas/deferir_solicitacao/', data, config)     
        .then((res)=>{
            try {
                let x = JSON.parse(res.data);
                if(x.sucesso === 'S'){
                    Alertas.sucesso("Folgas deferidas com sucesso");
                    props.reload.set(!props.reload.get); // atualiza um estado que dispara o useEffect
                }else{
                    Alertas.erro(x.motivo);
                }
            } catch (error) {
                Alertas.erro(error);
            }
            setDialog_aprovar(false);
        }).catch((erro)=>{
            setDialog_aprovar(false);
            Alertas.erro(erro);
        })
    }

    function salvar_indeferimento(nome) {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            "id_folgas_solicitacao": id_solicitacao,
            "observacao": descricao_indeferir,
            "arquivo": nome
        };
        api.post('/folgas/indeferir_solicitacao/', data, config)     
        .then((res)=>{
            try {
                let x = JSON.parse(res.data);
                if(x.sucesso === 'S'){
                    Alertas.sucesso("Folgas indeferidas com sucesso");
                    props.reload.set(!props.reload.get);
                }else{
                    Alertas.erro(x.motivo);
                }
            } catch (error) {
                Alertas.erro(error);
            }
            setDialog_reprovar(false);
        }).catch((erro)=>{
            setDialog_reprovar(false);
            Alertas.erro(erro);
        })
    }

    function nomeArquivo(f) {
        let nome = f.name;
        let arr = nome.split('.');
        let tipo = arr[arr.length - 1];
        let d = new Date();
        let tempo = d.toISOString().split('.')[0];
        return `indeferimento_folgas_${id_solicitacao}_${tempo}.${tipo}`;
    }

    function indeferir() {
        if(descricao_indeferir){
            if(file){
                let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
                let nome = nomeArquivo(file);

                const formData = new FormData();
                formData.append("arquivo", file);
                formData.append("nome", nome);

                api.post('core/upload_pdf', formData, config)
                .then((res)=>{
                    if(res.data === 200){
                        salvar_indeferimento(nome);
                    }else{
                        Alertas.erro("Erro no upload do arquivo");
                    }
                }).catch((err)=>{
                    Alertas.erro(err);
                })
            }else{
                salvar_indeferimento(null);
            }
            
        }else{
            setObrigatorio(true);
        }
    }

    const irBodyTemplate = (rowData) => {
        return(
            <span>
                <Button 
                    title="Botão Visualizar"
                    icon="pi pi-eye"
                    className="btn-darkblue"
                    onClick={()=>visualizar(rowData)}
                />
                <Button 
                    title="Botão Deferir"
                    icon="pi pi-check"
                    className="btn-green"
                    onClick={()=>visualizar_aprovacao(rowData)}
                />
                <Button 
                    title="Botão Indeferir"
                    icon="pi pi-times" 
                    className="btn-red" 
                    onClick={()=>visualizar_reprovacao(rowData)}
                />
            </span>
        )
    }

    const diasColumnTemplate = (rowData) => {
        let datas = rowData.datas_lista.split(' ');        
        return(
            <span>
                {
                    datas.map((d)=>{
                        return d !== '' ?<><Tag value={d} className="tag-lista-dias"></Tag>{' '}</>:null
                    })
                }
            </span>
        )    
    }

    const renderFooterDeferir = () => {
        return (
            <div>
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{setDialog_aprovar(false)}} 
                    className="btn-2" />
        
                <Button  
                    label="Aprovar Solicitação" 
                    icon="pi pi-check"  
                    className="btn-1"  
                    onClick={deferir}            
                />   
            </div>    
        );
    }

    const renderFooterIndeferir = () => {
        return (
            <div>
                {obrigatorio?<p className='mensagem-erro'>Informe os campos obrigatórios *</p>:null}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{setDialog_reprovar(false); setObrigatorio(false)}} 
                    className="btn-2" />
        
                <Button  
                    label="Reprovar Solicitação" 
                    icon="pi pi-check"  
                    className="btn-2"  
                    onClick={indeferir}            
                />   
            </div>    
        );
    }

    const statusRowFilterTemplate = (options) => {
        return <Dropdown value={options.value} options={statuses} onChange={(e) => options.filterApplyCallback(e.value)} itemTemplate={statusItemTemplate} placeholder="Filtrar por status"  showClear />;
    }
    const statusItemTemplate = (option) => {
        return <span>{option}</span>;
    }
    const statuses = [
        'SOLICITAÇÃO','INTERRUPÇÃO', 'ALTERAÇÃO', 'ALTERADO', 'CANCELAMENTO'
    ];
    const statusColumnTemplate = (rowData) => {
        switch (rowData.status2) {
            case 'SOLICITAÇÃO':
                return <Tag value={rowData.status} className="tag-tramitando"></Tag>;

            case 'CANCELAMENTO':
                return <Tag value={rowData.status2} className="tag-indeferido"></Tag>;

            case 'INTERRUPÇÃO':
                return <Tag value={rowData.status2} className="tag-cancelado"></Tag>;

            case 'TRAMITANDO':
                return <Tag value={rowData.status2} className="tag-tramitando"></Tag>;

            case 'ALTERADO':
                return <Tag value={rowData.status2} className="tag-aprovado"></Tag>;

            case 'ALTERAÇÃO':
                return <Tag value={rowData.status2} className="tag-alteracao"></Tag>;

            default:
                return <Tag value={rowData.status2} className="tag-tramitando"></Tag>;
               
        }
        
    }

    return ( 
        <div>
            <DataTable 
                value={props.aprovacoes} 
                responsiveLayout="scroll"
                scrollable 
                scrollHeight="60vh"
                filters={filters}
                filterDisplay="row"
                paginator
                paginatorTemplate="CurrentPageReport FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink"
                currentPageReportTemplate="{last} de {totalRecords}" 
                rows={10}
            >
                <Column 
                    field="nome" 
                    header="Solicitante"
                    filter 
                    filterPlaceholder="Buscar"
                    style={{ flexBasis: '35%' }}
                    showFilterMenu={false}
                />
                <Column 
                    field="datas_lista" 
                    header="Datas Solicitadas"
                    filter 
                    body={diasColumnTemplate}
                    filterPlaceholder="Buscar"
                    style={{ flexBasis: '40%' }}
                    showFilterMenu={false}
                />
                <Column 
                    field="status2" 
                    header="Tipo"
                    body={statusColumnTemplate}
                    className="col-centralizado"
                    filter 
                    filterPlaceholder="Buscar"
                    filterElement={statusRowFilterTemplate} 
                    style={{ flexBasis: '10%' }}
                    showFilterMenu={false}
                />
                <Column 
                    header="Ação" 
                    body={irBodyTemplate} 
                    className="col-centralizado" 
                    style={{ flexBasis: '15%' }}
                />
            </DataTable>

            <Dialog 
                header="Solicitação de Folgas"
                visible={dialog_ver}
                style={{ width: '75%' }}
                onHide={() =>{setDialog_ver(false)}}
            >
                <p className="folgas-descricao-solicitantes">Solicitante: <b>{nome_solicitante}</b></p>
                <p className="folgas-descricao-solicitantes">Matrícula: <b>{mat_solicitante}</b></p>
                <p className="folgas-descricao-solicitantes">Lotação: <b>{lotacao_solicitante}</b></p>
                <p className="folgas-descricao-solicitantes">Tipo do solicitante: <b>{
                    eh_defensor? "DEFENSOR": "SERVIDOR"
                }</b></p><br />
                <div>
                    <p>Dias solicitados</p>
                    <ul>
                        {
                            ver_dias.map((i, index)=>{
                                return(<li key={index}> {i.data} </li>)
                            })
                        }
                    </ul>
                </div>
                <div>
                    <p>Status da tramitação</p>
                    <ul className="folgas-lista-status">
                        {
                            status.map((s)=>{
                                return(
                                    <li key={s.id}>
                                        {
                                            s.status === 1
                                            ?<FaCheckCircle className="icone-folgas-status"/>
                                            :s.status === 2?
                                            <FaRegCircle className="icone-folgas-status"/>
                                            :s.status === 3?
                                            <FaTimesCircle className="icone-folgas-status-red"/>
                                            :<FaRegCircle className="icone-folgas-status"/>
                                        }
                                        {s.aprovador}
                                    </li>
                                )
                            })
                        }
                    </ul>
                </div>
                <div>
                    <p>Histórico</p>
                    <ul className="folgas-lista-status">
                        <li id="folgas-listas-historico">
                            <b>1 - SOLICITAÇÃO DE {nome_solicitante}</b>
                        </li>
                        {
                            historico.map((h, index)=>{
                                return(
                                    <div key={index}>
                                    <li id="folgas-listas-historico">
                                        <div>
                                        <div><b>{index+2} - {h.status} POR {h.nome} NA DATA {h.data}</b></div>
                                        {h.observacao?<p id="descricao-historico"><i>{h.observacao}</i></p>:null}
                                        </div>
                                    </li>
                                    </div>
                                )
                            })
                        }
                    </ul>
                </div>
            
            </Dialog>

            {/* dialogo de deferimento */}
            <Dialog 
                header="Aprovar Solicitação de Folgas"
                visible={dialog_aprovar}
                style={{ width: '50%' }}
                footer={renderFooterDeferir}
                onHide={() =>{setDialog_aprovar(false);}}
            >
                <p className="folgas-descricao-solicitantes">Solicitante: <b>{nome_solicitante}</b></p>
                <p className="folgas-descricao-solicitantes">Matrícula: <b>{mat_solicitante}</b></p>
                <p className="folgas-descricao-solicitantes">Lotação: <b>{lotacao_solicitante}</b></p>
                <p className="folgas-descricao-solicitantes">Tipo do solicitante: <b>{
                    eh_defensor? "DEFENSOR": "SERVIDOR"
                }</b></p><br />
                <div className="folgas-corpo-modal-visualizacao">
                    <div>
                    <p>Dias solicitados</p>
                        <ul>
                            {
                                ver_dias.map((i, index)=>{
                                    return(<li key={index}> {i.data} </li>)
                                })
                            }
                        </ul>
                    </div>
                </div>
            </Dialog>

            {/* dialogo de inddeferimento */}
            <Dialog 
                header="Solicitação de Folgas"
                visible={dialog_reprovar}
                style={{ width: '50%' }}
                footer={renderFooterIndeferir}
                onHide={() =>{setDialog_reprovar(false); setObrigatorio(false)}}
            >
                <p className="folgas-descricao-solicitantes">Solicitante: <b>{nome_solicitante}</b></p>
                <p className="folgas-descricao-solicitantes">Matrícula: <b>{mat_solicitante}</b></p>
                <p className="folgas-descricao-solicitantes">Lotação: <b>{lotacao_solicitante}</b></p>
                <p className="folgas-descricao-solicitantes">Tipo do solicitante: <b>{
                    eh_defensor? "DEFENSOR": "SERVIDOR"
                }</b></p><br />
                <div className="folgas-painel-deferimento">
                    <div>
                        <p>Dias solicitados</p>
                        <ul>
                            {
                                ver_dias.map((i, index)=>{
                                    return(<li key={index}> {i.data} </li>)
                                })
                            }
                        </ul>
                    </div>
                    <div style={{width: "100%"}}>
                        <p>Justificativa *</p>
                        <InputTextarea 
                            rows={5} cols={30} 
                            id="folgas-texto-deferimento"
                            style={{height: "40px"}}
                            value={descricao_indeferir}
                            onChange={e=>setDescricao_indeferir(e.target.value)}
                        />
                    </div>
                    <div style={{width: "100%"}}>
                        <p>Anexo (opcional)</p>
                        <span className='input-arquivo-servidor'>
                        <label className='label-file-servidor'>
                            <BiCloudUpload size={20}/>
                            <input
                                value="" 
                                type="file" 
                                className='input-files' 
                                accept='application/pdf'
                                onChange={e=>setFile(e.target.files[0])}
                            />
                        </label>
                        <p>
                            {file?file.name:"Nenhum arquivo selecionado"}
                        </p>
                    </span>
                    </div>
                </div>
            </Dialog>
        </div>
    );
}

export default FolgasAprovacao;