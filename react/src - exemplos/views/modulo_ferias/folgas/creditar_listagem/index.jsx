import React, { useState, useEffect } from 'react';
import {useNavigate} from 'react-router-dom';
import { ProgressSpinner } from 'primereact/progressspinner';
import { DataTable } from 'primereact/datatable';
import { FilterMatchMode } from 'primereact/api';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import api from '../../../../config/api';
import { getToken, getUserId } from '../../../../config/auth';
import { BreadCrumb } from 'primereact/breadcrumb';
import { BiCloudUpload } from "react-icons/bi";
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import Alertas from '../../../../utils/alertas';
import SeletorData from '../../../../components/SeletorData';
import { InputNumber } from 'primereact/inputnumber';
import { Tag } from 'primereact/tag';

function CreditarFolgaListagem() {

    const FileDownload = require('js-file-download');

    const items = [
        { label: 'Recursos Humanos' },
        { label: 'Férias' },
        { label: 'Creditar Folgas' }
    ];
    const home = { icon: 'pi pi-home', url: '/inicio' }
    const navigate = useNavigate();

    const [dados, setDados] = useState(null);
    const [loading, setLoading] = useState(false);
    const [obrigatorio, setObrigatorio] = useState(false);

    const [selecao, setSelecao] = useState([]);

    const [modal_credito, setModal_credito] = useState(false);
    const [modal_ver, setModal_ver] = useState(false);

    const [nome, setNome] = useState('');
    const [ dados_folgas, setDados_folgas ] = useState([]);

    const [descricao, setDescricao] = useState('');
    const [diasCredito, setDiasCredito] = useState(1);
    const [dataExpiracao, setDataExpiracao] = useState(null);
    const [file, setFile] = useState(null);
    const [reload, setReload] = useState(false);

    const [modal_editar, setModal_editar] = useState(false);

    const [dados_editar, setDados_editar] = useState(null);
    const [dias_editar, setDias_editar] = useState(0);

    const [filters] = useState({
        'matricula': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'nome': { value: null, matchMode: FilterMatchMode.CONTAINS }
    });

    useEffect(() => {
        setSelecao([]);
        setLoading(true);

        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            "tipo_listagem": "PARCIAL",
            "eh_estagiario": false,
            "ativo": true
        }
        api.post('/rh/servidores/', data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            setDados(x.funcionarios);
            setLoading(false);
        })
    }, [reload]);

    function limpar_estados() {
        setDescricao('');
        setDiasCredito(1);
        setDataExpiracao(null);
        setFile(null); 
    }

    function abrir_painel_creditar() {
        if(selecao.length > 0){
            setModal_credito(true);
        }else{
            Alertas.atencao("Nenhum membro ou servidor selecionado")
        }
    }

    function baixarArquivo(nome_arquivo) {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}, responseType: 'blob'}
        let data = {
            nome: nome_arquivo
        }
        api.post('core/download_pdf', data, config)
        .then((res)=>{
            FileDownload(res.data, `${nome_arquivo}.pdf`);
        }).catch((err)=>{
            let msg = "Falha no download";
            if(err.response.status === 401){
                msg = "Não autorizado";
            }
            Alertas.erro(msg);
        })
    }

    function nomeArquivo(f) {
        let nome = f.name;
        let arr = nome.split('.');
        let tipo = arr[arr.length - 1];
        let d = new Date();
        let tempo = d.toISOString().split('.')[0];
        return `creditar_folgas_${tempo}.${tipo}`;
    }

    function upload_file() {
        if(descricao && file){
            let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
            let nome = nomeArquivo(file);

            const formData = new FormData();
            formData.append("arquivo", file);
            formData.append("nome", nome);

            api.post('core/upload_pdf', formData, config)
            .then((res)=>{
                if(res.data === 200){
                    creditar_folgas(nome);
                }else{
                    Alertas.erro("Erro no upload do arquivo");
                }
            }).catch((err)=>{
                Alertas.erro(err);
            })
        }else{
            setObrigatorio(true);
        }
    }

    function creditar_folgas(file) {
        let ids = [];
        selecao.map((s)=>{ //extrair ids dos selecionados
            ids.push({"id_funcionario": s.id_funcionario});
        })

        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            "descricao": descricao,
            "dias": diasCredito,
            "vencimento": dataExpiracao,
            "funcionarios": ids,
            "arquivo": file
        }
    
        let config = { headers: { "Authorization": `Bearer ${getToken()}` } }

        api.post("folgas/folgas_gerar_saldo/", data, config)
        .then((res) => {
            let x = JSON.parse(res.data);
            if (x.sucesso === "S") {
                Alertas.sucesso("Folgas creditadas");
                setModal_credito(false);
                limpar_estados();
                setReload(!reload);
            } else {
                Alertas.erro(x.motivo);
            }
        }).catch((erro)=>{
            Alertas.erro(erro);
        })

        setObrigatorio(false)
    }

    function ver_folgas(obj) {
        setModal_ver(true);
        setNome(obj.nome);
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            "id_funcionario": obj.id_funcionario
        }
        api.post('/folgas/folgas_listar_folgas/', data, config)     
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(Array.isArray(x)){
                setDados_folgas(x);
                setLoading(false);
            }
        });
    }

    const irBodyTemplate = (rowData) => {
        return(
            <span className='botoes-acao'>
            <Button 
                onClick={()=>ver_folgas(rowData)}
                icon="pi pi-eye"
                className="btn-darkblue"
                title="Ver Folgas"
            />
            </span>
        )                                    
    }

    const baixarFileTemplate = (rowData) => {
        return(
            <span className='botoes-acao'>
                {
                    rowData.arquivo?
                        <Button 
                            onClick={()=>baixarArquivo(rowData.arquivo)}
                            icon="pi pi-file-pdf"
                            className="btn-red"
                            title="Anexo"
                        />
                    :
                        null
                }
            
            </span>
        )                                    
    }

    const editarTemplate = (rowData) => {
        return(
            <span className='botoes-acao'>
               <Button 
                    onClick={()=>abrir_edicao(rowData)}
                    icon="pi pi-pencil"
                    className="btn-green"
                    title="Editar"
                />
            </span>
        )                                    
    }

    function abrir_edicao(rowData) {
        setDados_editar(rowData);
        setDias_editar(rowData.dias);
        setModal_editar(true);
    }

    const renderFooterEditar = () => {
        return (
            <div>
                {obrigatorio?<p className='mensagem-erro'>Informe os campos obrigatórios *</p>:null}
                <Button 
                    label="Salvar" 
                    icon="pi pi-save"
                    className="btn-1" 
                    onClick={()=>salvar_edicao_dias()} 
                />
            </div>
        );
    }

    function salvar_edicao_dias() {
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            "funcionarios": [{"id_funcionario": dados_editar.id_funcionario}],
            "id_folga": dados_editar.id_folgas,
            "dias": dias_editar
        }
    
        let config = { headers: { "Authorization": `Bearer ${getToken()}` } }

        api.post("folgas/folgas_gerar_saldo/", data, config)
        .then((res) => {
            let x = JSON.parse(res.data);
            if (x.sucesso === "S") {
                Alertas.sucesso("Folga atualizada");
                setModal_editar(false);
                setModal_ver(false);
                setReload(!reload);
            } else {
                Alertas.erro(x.motivo);
            }
        }).catch((erro)=>{
            Alertas.erro(erro);
        })
    }

    const diasColumnVencimento = (rowData) => {
        return(<Tag value={rowData.vencimento} className="tag-lista-dias"></Tag>)
    }
    const diasColumnSolicitada = (rowData) => {
        return(<Tag value={rowData.data_solicitacao} className="tag-lista-dias"></Tag>)
    }

    const renderFooterCreditos = () => {
        return (
            <div>
                {obrigatorio?<p className='mensagem-erro'>Informe os campos obrigatórios *</p>:null}
                <Button 
                    label="Creditar Saldo de Folgas" 
                    icon="pi pi-check"
                    className="btn-1" 
                    onClick={()=>upload_file()} 
                />
            </div>
        );
    }

    return ( 
        <div className='view'>
            <div className="view-body">
            {
            loading?
            <div className="loading-pagina" ><ProgressSpinner/>Carregando...</div>
            :
            <div>
                <div className="header">
                    <BreadCrumb model={items} home={home}/>
                    <Button 
                        label="Creditar Saldo"
                        icon="pi pi-plus" 
                        className="btn-1"
                        onClick={()=>abrir_painel_creditar()}
                    />
                </div>
                <h6 className="titulo-header">Creditar Folgas</h6>
                <div className="top-box">
                    <h1>Creditar Folgas</h1>
                </div>
                <div>
                    <DataTable
                        value={dados}
                        size="small"
                        className="tabela-servidores"
                        emptyMessage="..."
                        scrollable
                        scrollHeight="65vh"
                        filters={filters}
                        filterDisplay="row"
                        dataKey="matricula"
                        selectionMode="single"
                        selection={selecao}
                        onSelectionChange={e => setSelecao(e.value)}
                        paginator
                        paginatorTemplate="CurrentPageReport FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink"
                        currentPageReportTemplate="{last} de {totalRecords}" 
                        rows={10}
                    >
                        <Column 
                            selectionMode="multiple"
                            headerStyle={{ width: '5%' }}
                        />
                        <Column 
                            field="matricula"
                            header="Matrícula"
                            filter 
                            filterPlaceholder="Buscar" 
                            style={{ flexGrow: 0, flexBasis: '15%' }}
                            showFilterMenu={false}
                        />
                        <Column
                            field="nome" 
                            header="Nome" 
                            filter 
                            filterPlaceholder="Buscar" 
                            style={{ flexGrow: 0, flexBasis: '60%' }}
                            showFilterMenu={false}
                        />
                        <Column 
                            field="botao" 
                            header="Ação" 
                            body={irBodyTemplate} 
                            className="col-centralizado" 
                            style={{ flexGrow: 0, flexBasis: '20%' }}
                        />
                    </DataTable>
                </div>

                {/* modal de credito de folgas */}
                <Dialog 
                    header="Creditar Folgas" 
                    visible={modal_credito}
                    style={{ width: '70vw' }}
                    footer={renderFooterCreditos}
                    onHide={() => {setModal_credito(false); setObrigatorio(false)}}
                >
                    <div className='credito-folgas-formulario'>

                        <div className='grupo-input-cadastro-folgas'>

                            <span className="field grupo-input-label field-cadastro-folgas" style={{width:"60%"}}>
                                Descrição *
                                <InputText value={descricao} onChange={(e) => setDescricao(e.target.value)} />
                            </span>
                            <span className="field grupo-input-label field-cadastro-folgas" style={{width:"20%"}}>
                                Dias a Creditar
                                <InputNumber 
                                    inputId="minmax-buttons" 
                                    value={diasCredito} 
                                    onValueChange={(e) => setDiasCredito(e.value)}
                                    mode="decimal"
                                    showButtons
                                    min={1}
                                    max={999}
                                />
                            </span>

                        </div>
                        <div className='grupo-input-cadastro-folgas'>
                            <span className="field grupo-input-label" style={{width:"30%", marginLeft: '5px'}}>
                                Data de Expiração (Opcional)
                                <SeletorData
                                    get={dataExpiracao}
                                    set={setDataExpiracao}
                                />
                            </span>

                            <span className='field grupo-input-label' style={{width:"70%",  marginLeft: '5px'}}>
                                Arquivo PDF *
                                <span className='input-arquivo-servidor'>
                                    <label className='label-file-servidor'>
                                        <BiCloudUpload size={20}/>
                                        <input
                                            value="" 
                                            type="file" 
                                            className='input-files' 
                                            accept='application/pdf'
                                            onChange={e=>setFile(e.target.files[0])}
                                        />
                                    </label>
                                    <p>
                                        {file?file.name:"Nenhum arquivo selecionado"}
                                    </p>
                                </span>
                            </span> 
                        </div>
                    </div>
                    <div className='credito-folgas-selecao'>
                    <ol>
                    {
                        selecao.map((item, index)=>{
                            return(
                                <li key={index}>
                                    {item.nome}
                                </li>
                            )
                        })
                    }
                    </ol>
                   </div>
                </Dialog>
                {/* Ver folgas de servidor */}
                <Dialog 
                    header={`Folgas de ${nome}`} 
                    visible={modal_ver}
                    style={{ width: '90%' }}
                    onHide={() => {setModal_ver(false)}}
                >

                    <DataTable
                        value={dados_folgas}
                        size="small"
                        className="tabela-servidores"
                        dataKey="id"
                        emptyMessage="Sem registros de folgas"
                        scrollable
                        scrollHeight="76vh"
                        selectionMode="single"
                        showGridlines
                    >
                        <Column 
                            field="descricao"
                            header="Origem"
                            style={{ flexGrow: 0, flexBasis: '30%' }}
                        />
                        <Column
                            field="dias" 
                            header="Dias" 
                            style={{ flexGrow: 0, flexBasis: '10%' }}
                        />
                        <Column
                            field="saldo" 
                            header="Saldo" 
                            style={{ flexGrow: 0, flexBasis: '10%' }}
                        />
                        <Column
                            body={diasColumnVencimento} 
                            header="Vencimento" 
                            style={{ flexGrow: 0, flexBasis: '15%' }}
                        />
                        
                        <Column
                            body={diasColumnSolicitada} 
                            header="Data Solicitada" 
                            style={{ flexGrow: 0, flexBasis: '15%' }}
                        />

                        <Column
                            body={baixarFileTemplate} 
                            header="Anexo" 
                            style={{ flexGrow: 0, flexBasis: '10%' }}
                        />

                        <Column
                            body={editarTemplate} 
                            header="Editar" 
                            style={{ flexGrow: 0, flexBasis: '10%' }}
                        />
                        
                    </DataTable>

                </Dialog>

                <Dialog 
                    header={`Folgas de ${nome}`} 
                    visible={modal_editar}
                    style={{ width: '60%' }}
                    onHide={() => {setModal_editar(false)}}
                    footer={renderFooterEditar}
                >
                    <p style={{margin: "0px"}}><b>Descrição:</b> {dados_editar?dados_editar.descricao:""}</p>
                    <p style={{margin: "0px"}}><b>Data da solicitação:</b> {dados_editar?dados_editar.data_solicitacao:""}</p>
                    <p style={{margin: "0px"}}><b>Vencimento:</b> {dados_editar?dados_editar.vencimento:""}</p>

                    <br />

                    <span className="field grupo-input-label field-cadastro-folgas" style={{width:"20%"}}>
                        Quantidade de dias
                        <InputNumber 
                            inputId="minmax-buttons" 
                            value={dias_editar} 
                            onValueChange={(e) => setDias_editar(e.value)}
                            mode="decimal"
                            showButtons
                            min={0}
                            max={999}
                        />
                    </span>
                </Dialog>
            </div>
            }
            </div>
        </div>
    );
}

export default CreditarFolgaListagem;