import React, {useState, useEffect} from "react";
import { ProgressSpinner } from 'primereact/progressspinner';
import api from "../../../config/api";
import { getToken, getUserId } from "../../../config/auth";
import Alertas from "../../../utils/alertas";
import { TabPanel, TabView } from "primereact/tabview";
import { BreadCrumb } from 'primereact/breadcrumb';

import FolgasTodasSolicitacoes from "./folgas";
import FeriasTodasSolicitacoes from "./ferias";

import RelatoriosSolicitacaoFerias from "../componentes/relatorio/solicitacoes_gerais_ferias";
import RelatoriosSolicitacaoFolgas from "../componentes/relatorio/solicitacoes_gerais_folgas";

function VisualizarFeriasFolgasGeral() {

    const [loading, setLoading] = useState(true);
    const [activeIndex, setActiveIndex] = useState(0); 

    const [reload, setReload] = useState(true);

    const [folgas, setFolgas] = useState([]);
    const [ferias, setFerias] = useState([]);

    const items = [
        { label: 'Férias e Folgas' },
        { label: 'Acompanhar Solicitações' }
    ];
    const home = { icon: 'pi pi-home', url: '/inicio' }

    //ferias
    const [statusAprovador, setStatusAprovador] = useState([]);
    const [statusSolicitante, setStatusSolicitante] = useState([]);
    const [solicitacoesFeriasSolicitante,setSolicitacoesFeriasSolicitante] = useState([]);
    const [solicitacoesFeriasAprovador, setSolicitacoesFeriasAprovador] = useState([]);


    useEffect(() => {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
        };
        api.post('/folgas/listar_solicitacoes/', data, config)     
        .then((res)=>{
            try {
                let x = JSON.parse(res.data);
                if (Array.isArray(x)) {
                    let solicitacoes = [];
                    let aprovacoes = [];
                    x.map((i)=>{
                        i.ferias_ou_folgas = 2;
                        if(i.mostrar_deferir_indeferir){
                            aprovacoes.push(i);
                        }else{
                            solicitacoes.push(i);
                        }
                    })
                    setFolgas([...solicitacoes]);
                }else{
                    Alertas.erro("A resposta não é uma estrutura de dados válida");
                }
            } catch (error) {
                Alertas.erro(error);
            }
            setLoading(false);
        })

        api.post('/ferias/listar_minhas_solicitacoes_ferias/', { "aplicacao":'SGI', "id_usuario": getUserId() }, config)
        .then((res)=>{
            try {
                let solicitacoes_ferias = JSON.parse(res.data);
                if (Array.isArray(solicitacoes_ferias)) {
                    let auxArraySolicitacoesAprovador = [];
                    let auxArraySolicitacoesSolicitante = [];
                    let auxArrayStatusAprovador = [];
                    let auxArrayStatusSolicitante = []; 

                    solicitacoes_ferias.map((item)=>{
                        item.ferias_ou_folgas = 1;
                        item.periodo = item.data_inicio+' a '+item.data_fim+' — '+item.dias_solicitados+' dias';
                        if(item.mostrar_deferir_indeferir){
                            auxArraySolicitacoesAprovador.push(item);
                            auxArrayStatusAprovador.push({name:item.status});
                        }else{
                            auxArraySolicitacoesSolicitante.push(item);
                            auxArrayStatusSolicitante.push({name:item.status});
                        }
                    })

                    const uniqueStatusAprovador = auxArrayStatusAprovador.filter((obj, index) => {
                        return index === auxArrayStatusAprovador.findIndex(o => obj.name === o.name);
                    });

                    const uniqueStatusSolicitante = auxArrayStatusSolicitante.filter((obj, index) => {
                        return index === auxArrayStatusSolicitante.findIndex(o => obj.name === o.name);
                    });

                    uniqueStatusAprovador.push({name:'TODOS'});
                    uniqueStatusSolicitante.push({name:'TODOS'});
                
                    setStatusAprovador([...uniqueStatusAprovador]);
                    setStatusSolicitante([...uniqueStatusSolicitante]);
                    setSolicitacoesFeriasAprovador([...auxArraySolicitacoesAprovador]);
                    setSolicitacoesFeriasSolicitante([...auxArraySolicitacoesSolicitante]);
                }else{
                    Alertas.erro("A resposta não é uma estrutura de dados válida");
                }
            } catch (error) {
                Alertas.erro(error);
            }
            setLoading(false);
        })
        
    }, [reload]);

    return ( 
        <div className='view'>
        <div className="view-body">
        {
        loading?
        <div className="loading-pagina" ><ProgressSpinner/>Carregando...</div>
        :
        <div>
            <div className="header">
                <BreadCrumb model={items} home={home}/>
                <h6 className="titulo-header">Acompanhar Solicitações</h6>
            </div>
            <div className="ver-relatorios-gerais">
                <RelatoriosSolicitacaoFerias
                    dados={solicitacoesFeriasSolicitante}
                />
                <span style={{width: "10px"}}></span>
                <RelatoriosSolicitacaoFolgas
                    dados={folgas}
                />
            </div>
            <div className="">
                <TabView activeIndex={activeIndex} onTabChange={(e) => setActiveIndex(e.index)}>

                    <TabPanel className="tabPanel-solicitacoes" header="Férias">
                       <FeriasTodasSolicitacoes 
                            solicitacoesFeriasSolicitante={solicitacoesFeriasSolicitante}
                            statusSolicitante={statusSolicitante}
                            //setChamaBuscarSolicitacoes={setChamaBuscarSolicitacoes}
                            //chamaBuscarSolicitacoes={chamaBuscarSolicitacoes}
                        />
                    </TabPanel>
                        
                    <TabPanel className="tabPanel-solicitacoes" header="Folgas">
                        <FolgasTodasSolicitacoes minhas_solicitacoes={folgas}/>
                    </TabPanel>
                </TabView>
            </div>
        </div>
        }
        </div>
        </div>
    );
}

export default VisualizarFeriasFolgasGeral;