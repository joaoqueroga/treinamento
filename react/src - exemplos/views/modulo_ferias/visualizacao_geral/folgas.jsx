import React, {useState} from "react";
import { DataTable } from 'primereact/datatable';
import { Dialog } from 'primereact/dialog';
import { Button } from 'primereact/button';
import { Column } from 'primereact/column';
import { Dropdown } from 'primereact/dropdown';
import { Tag } from 'primereact/tag';
import { FaRegCircle, FaTimesCircle, FaCheckCircle } from "react-icons/fa";
import { Badge } from 'primereact/badge';
import { FilterMatchMode } from 'primereact/api';
import Alertas from "../../../utils/alertas";
import api from "../../../config/api";
import { getToken, getUserId } from "../../../config/auth";

import ComprovanteSolicitacao from "../componentes/relatorio/comprovante";

function FolgasTodasSolicitacoes(props) {

    const FileDownload = require('js-file-download');

    const [filters] = useState({
        'datas_lista': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'nome': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'descricao_lotacao': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'status': { value: null, matchMode: FilterMatchMode.EQUALS },
        'data_solicitacao':{value:null,matchMode:FilterMatchMode.CONTAINS}
    });

    const [dialog_ver, setDialog_ver] = useState(false);
    const [status, setStatus] = useState([]);

    const [status_final, setStatus_final] = useState('');

    const [ver_dias, setVer_dias] = useState([]);
    const [historico, setHistorico] = useState([]);
    const [nome, setNome] = useState('');
    const [arquivo_solicitacao, setArquivo_solicitacao] = useState(null);
  

    const [texto_indeferimento, setTexto_indeferimento] = useState(null);
    const [arquivo_indeferimento, setArquivo_indeferimento] = useState(null);
    const [texto_cancelamanto, setTexto_cancelamento] = useState(null);

    
    function visualizar(d) {
        setArquivo_solicitacao(d.arquivo);
        setDialog_ver(true);
        setVer_dias(d.datas);
        setStatus_final(d.status);
        setHistorico(d.historico);
        setNome(d.nome);

        setArquivo_indeferimento(d.indeferimento_arquivo);
        setTexto_indeferimento(d.indeferimento_observacao);
        setTexto_cancelamento(d.motivo_cancelamento);

        let tipo_nstatus = 0; /// 0: passa pela Corregedoria  1:nao passa pela corregedoria
        let ultimo_status = '';
        let ultima_descri = null;
        d.historico.map((h)=>{
            if(h.status !== 'DEFERIDO' && h.status !== 'INDEFERIDO'){
                ultimo_status = h.status;
                if(h.status === "ALTERADO"){
                    ultima_descri = h.observacao;
                }
            }
        })
        if(ultimo_status === "CANCELAMENTO"){ // cancelamento nao passa pela CG
            tipo_nstatus = 1;
        }else if(ultima_descri){
            if(ultima_descri.includes("EXCLUSÃO"))tipo_nstatus = 1;  
            // se a ultima descricao nao tiver excluir tem que passar pela CG
        }


        // status 1: aprovado, 2: sem avaliacao, 3: rejeitado, 4: setor a aprovar
        let nstatus = [];
        if(d.eh_defensor){
            nstatus.push({id: 1, aprovador: "Solicitação", status: 1});
            if(tipo_nstatus === 0){
                nstatus.push({id: 2, aprovador: "Aprovação da Corregedoria Geral", status: d.autorizado_cg?1:2})
            }
            nstatus.push({id: 3, aprovador: "Aprovação do Gabinete do Defensor Público Geral", status: d.autorizado_gdpg?1:2})
        }else{
            nstatus.push({id: 1, aprovador: "Solicitação", status: 1});
            nstatus.push({id: 2, aprovador: "Aprovação do Setor", status: d.autorizado_chefia?1:2});
            if(tipo_nstatus === 0){
                nstatus.push({id: 3, aprovador: "Aprovação da Corregedoria Geral", status: d.autorizado_cg?1:2})
            }
            nstatus.push({id: 4, aprovador: "Aprovação do Gabinete do Subdefensor Público Geral", status: d.autorizado_gspg?1:2});
        }

        if(d.status === 'INDEFERIDO'){ // encontra o que rejeitou
            for (let i = 0; i < nstatus.length; i++) {
                if(nstatus[i].status === 2){
                    nstatus[i].status = 3;
                    break;
                }
            }
        }else if(d.status === 'TRAMITANDO'){
            for (let i = 0; i < nstatus.length; i++) {
                if(nstatus[i].status === 2){
                    nstatus[i].status = 4;
                    break;
                }
            }
        }

        setStatus(nstatus);
    }

    const statusRowFilterTemplate = (options) => {
        return <Dropdown value={options.value} options={statuses} onChange={(e) => options.filterApplyCallback(e.value)} itemTemplate={statusItemTemplate} placeholder="Filtrar por status" className="p-column-filter" showClear />;
    }
    const statusItemTemplate = (option) => {
        return <span>{option}</span>;
    }
    const statuses = [
        'TRAMITANDO', 'APROVADO', 'INDEFERIDO', 'CANCELADO'
    ];

    const statusColumnTemplate = (rowData) => {
        switch (rowData.status) {
            case 'APROVADO':
                return <Tag value={rowData.status} className="tag-aprovado"></Tag>;

            case 'INDEFERIDO':
                return <Tag value={rowData.status} className="tag-indeferido"></Tag>;

            case 'CANCELADO':
                return <Tag value={rowData.status} className="tag-cancelado"></Tag>;

            case 'TRAMITANDO':
                return <Tag value={rowData.status} className="tag-tramitando"></Tag>;

            default:
                return(
                    <span>
                        {rowData.status2 === 'ALTERAÇÃO'|| rowData.status2 === 'INTERRUPÇÃO' || rowData.status2 === 'CANCELAMENTO'?
                        <Tag value={<i className="pi pi-pencil"></i>} severity="info" className="tag-tramitando"></Tag>
                        : null }
                        {' '}<Tag value={rowData.status} className="tag-tramitando"></Tag>
                    </span>
                )
        }
    }

    const diasColumnTemplate = (rowData) => {
        let datas = rowData.datas_lista.split(' ');
        //let datas = rowData.periodo.split(' — '); 
        return(
            <span>
                {
                    datas.map((d, index)=>{
                        return d !== '' ?<span key={index}><Tag value={d} className="tag-lista-dias"></Tag>{' '}</span>:null
                    })
                }
            </span>
        )    
    }

    function baixarArquivo(nome_arquivo) {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}, responseType: 'blob'}
        let data = {
            nome: nome_arquivo
        }
        api.post('core/download_pdf', data, config)
        .then((res)=>{
            FileDownload(res.data, `${nome_arquivo}.pdf`);
        }).catch((err)=>{
            let msg = "Falha no download";
            if(err.response.status === 401){
                msg = "Não autorizado";
            }
            Alertas.erro(msg);
        })
    }

    const dataColumnTemplate = (rowData) => { 
        return(
            <span>
                <Tag value={rowData.data_solicitacao} className="tag-lista-dias"></Tag>
            </span>
        )    
    }

    const irBodyTemplate = (rowData) => {
        return(
            <span className="botoes-acao-folgas">
                <Button 
                    title="Visualizar"
                    icon="pi pi-eye"
                    className="btn-darkblue"
                    onClick={()=>visualizar(rowData)}
                />
                <ComprovanteSolicitacao dados={rowData}/>
            </span>
        )
    }

    return ( 
        <div>
            <DataTable 
                value={props.minhas_solicitacoes} 
                responsiveLayout="scroll"
                scrollable 
                scrollHeight="65vh"
                filters={filters}
                filterDisplay="row"
                paginator
                paginatorTemplate="CurrentPageReport FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink"
                currentPageReportTemplate="{last} de {totalRecords}" 
                rows={10}
            >
                <Column 
                    field="nome" 
                    header="Solicitante"
                    filter 
                    filterPlaceholder="Buscar Nome"
                    showFilterMenu={false} 
                    style={{ flexBasis: '25%' }}
                />
                <Column 
                    field="descricao_lotacao" 
                    header="Lotação"
                    filter 
                    filterPlaceholder="Buscar Lotação" 
                    showFilterMenu={false}
                    style={{ flexBasis: '25%' }}
                />
                <Column 
                    field="data_solicitacao" 
                    header="Solicitado em"
                    filter 
                    filterPlaceholder="Buscar" 
                    showFilterMenu={false}
                    body={dataColumnTemplate}
                    style={{ flexBasis: '10%' }}
                />
                <Column 
                    field="datas_lista" 
                    header="Datas Solicitadas"
                    body={diasColumnTemplate}
                    filter 
                    filterPlaceholder="Datas" 
                    showFilterMenu={false}
                    style={{ flexBasis: '20%' }}
                />
                <Column 
                    field="status" 
                    header="Status"
                    body={statusColumnTemplate} 
                    filter 
                    filterElement={statusRowFilterTemplate} 
                    showFilterMenu={false}
                    style={{ flexBasis: '10%' }}
                /> 
                <Column 
                    header="Ações" 
                    body={irBodyTemplate} 
                    style={{ flexBasis: '10%' }}
                />
            </DataTable>

            <Dialog 
                header="Solicitação de Folgas"
                visible={dialog_ver}
                style={{ width: '75%' }}
                onHide={() =>{setDialog_ver(false);}}
            >
                
                <div>
                    <p>Dias solicitados</p>
                    <ul>
                        {
                            ver_dias.map((i, index)=>{
                                return(<li key={index}> {i.data} </li>)
                            })
                        }
                    </ul>
                </div>

                <div>
                    <p>Status da tramitação</p>

                    <ul className="folgas-lista-status">
                        {
                            status.map((s)=>{
                                return(
                                    <li key={s.id}>
                                        {
                                            s.status === 1
                                            ?<FaCheckCircle className="icone-folgas-status"/>
                                            :s.status === 2?
                                            <FaRegCircle className="icone-folgas-status"/>
                                            :s.status === 3?
                                            <FaTimesCircle className="icone-folgas-status-red"/>
                                            :<FaRegCircle className="icone-folgas-status"/>
                                        }
                                        {s.aprovador}
                                    </li>
                                )
                            })
                        }
                    </ul>
                </div>

                <div>
                    <p>Histórico</p>
                    <ul className="folgas-lista-status">
                        <li id="folgas-listas-historico">
                            <b>1 - SOLICITAÇÃO DE {nome}</b>
                        </li>
                        {
                            arquivo_solicitacao?
                            <div> <a href="#" onClick={()=>baixarArquivo(arquivo_solicitacao)}><i>{arquivo_solicitacao}</i></a> </div>
                            :null
                        }
                        {
                            historico.map((h, index)=>{
                                return(
                                    <div key={index}>
                                    <li id="folgas-listas-historico">
                                        <div>
                                        <div><b>{index+2} - {h.status} POR {h.nome} NA DATA {h.data}</b></div>
                                        {h.observacao?<p id="descricao-historico"><i>{h.observacao}</i></p>:null}
                                        </div>
                                    </li>
                                    </div>
                                )
                            })
                        }
                    </ul>
                </div>

                {
                    texto_indeferimento?
                        <div>
                            <p>Justificativa de indeferimento</p>
                            <ul className="folgas-lista-status">
                                <li><i>- {texto_indeferimento}</i></li>
                                {
                                    arquivo_indeferimento?
                                    <li>
                                        <a href="#" onClick={()=>baixarArquivo(arquivo_indeferimento)} >
                                            Arquivo em anexo 
                                        </a> 
                                    </li>
                                    :null
                                }
                            </ul>
                        </div>
                    :null
                }

                {texto_cancelamanto?
                    <div>
                        <p>Motivo do Cancelamento</p>
                        <ul className="folgas-lista-status"></ul>
                            <li><i>{texto_cancelamanto}</i></li>
                            <br />

                    </div> 
                :null

                }       

                <div>
                    <p>Resultado da Solicitação</p>
                    <ul className="folgas-lista-status">
                        <li><b>{status_final}</b></li>
                    </ul>
                </div>
            
            </Dialog>

        
        </div>
    );
}

export default FolgasTodasSolicitacoes;