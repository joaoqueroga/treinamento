import React, {useState, useEffect} from "react";
import { DataTable } from 'primereact/datatable';
import { Dialog } from 'primereact/dialog';
import { Button } from 'primereact/button';
import { Column } from 'primereact/column';
import { Dropdown } from 'primereact/dropdown';
import { Tag } from 'primereact/tag';
import { FaRegCircle, FaTimesCircle, FaCheckCircle, FaCircle } from "react-icons/fa";
import { FilterMatchMode } from 'primereact/api';
import Alertas from "../../../utils/alertas";
import api from "../../../config/api";
import { getToken, getUserId } from "../../../config/auth";

import ComprovanteSolicitacao from "../componentes/relatorio/comprovante";

function FeriasTodasSolicitacoes(props) {
    const FileDownload = require('js-file-download');
    const [statusNoModal,setStatusNoModal] = useState([]);
    const [nomeSolicitante,setNomeSolicitante] = useState("");
    const [selectedStatusItemFilter,setSelectedItem] = useState(null);
    const [solicitacoesFerias, setSolicitacoesFerias] = useState([]);
    const [mostrarModalVisualizar,setMostrarModalVisualizar] = useState(false);
    const [itemSelecionado,setItemSelecionado] = useState(null);
    const [rowsPaginator,setRowsPaginator] = useState(10);

    const [status_final, setStatus_final] = useState('');

    const [filters, statusFilters] = useState(
        {
            'status': { value: null, matchMode: FilterMatchMode.EQUALS },
            'nome': { value:null,matchMode:FilterMatchMode.CONTAINS},
            'periodo': {value:null,matchMode:FilterMatchMode.CONTAINS},
            'lotacao_solicitante':{value:null,matchMode:FilterMatchMode.CONTAINS},
            'data_solicitacao':{value:null,matchMode:FilterMatchMode.CONTAINS}
        }
    );

    function visualizar(d) {
        setNomeSolicitante(d.nome)
        setStatus_final(d.status);
        // status 1: aprovado, 2: sem avaliacao, 3: rejeitado, 4: setor a aprovar
        let nstatus = [];
        if(d.eh_defensor){
            nstatus = [
                {id: 1, aprovador: "Solicitação", status: 1},
                {id: 2, aprovador: "Aprovação da Corregedoria Geral", status: d.autorizado_cg?1:2},
                {id: 3, aprovador: "Aprovação do Gabinete do Defensor Público Geral", status: d.autorizado_gdpg?1:2}
            ]
        }else{
            nstatus = [
                {id: 1, aprovador: "Solicitação", status: 1},
                {id: 2, aprovador: "Aprovação do Setor", status: d.autorizado_chefia?1:2},
                {id: 3, aprovador: "Aprovação da Corregedoria Geral", status: d.autorizado_cg?1:2},
                {id: 4, aprovador: "Aprovação do Gabinete do Subdefensor Público Geral", status: d.autorizado_gspg?1:2}
            ]
        }
        if(d.status === 'INDEFERIDO'){ // encontra o que rejeitou
            for (let i = 0; i < nstatus.length; i++) {
                if(nstatus[i].status === 2){
                    nstatus[i].status = 3;
                    break;
                }
            }
        }else if(d.status === 'TRAMITANDO'){
            for (let i = 0; i < nstatus.length; i++) {
                if(nstatus[i].status === 2){
                    nstatus[i].status = 4;
                    break;
                }
            }
        }

        setStatusNoModal(nstatus);
    }


    function baixarArquivo(nome_arquivo) {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}, responseType: 'blob'}
        let data = {
            nome: nome_arquivo
        }
        api.post('core/download_pdf', data, config)
        .then((res)=>{
            FileDownload(res.data, `${nome_arquivo}.pdf`);
        }).catch((err)=>{
            let msg = "Falha no download";
            if(err.response.status === 401){
                msg = "Não autorizado";
            }
            Alertas.erro(msg);
        })
    }

    const modalVisualizarFooter = () => {
        return (
            <div>
                <Button 
                    label="Fechar"  
                    onClick={()=>setMostrarModalVisualizar(false)} 
                    className="btn-2"
                />
            </div>
            
        );
    }

    const statusColumnTemplate = (rowData) => {
        switch (rowData.status) {
            case 'APROVADO':
                return <Tag value={rowData.status} className="tag-aprovado"></Tag>;

            case 'INDEFERIDO':
                return <Tag value={rowData.status} className="tag-indeferido"></Tag>;

            case 'CANCELADO':
                return <Tag value={rowData.status} className="tag-cancelado"></Tag>;

            case 'TRAMITANDO':
                return <Tag value={rowData.status} className="tag-tramitando"></Tag>;

            default:
                return <Tag value={rowData.status} className="tag-tramitando"></Tag>;
               
        }
    }
    
    const diasColumnTemplate = (rowData) => {
        let datas = rowData.periodo.split(' — ');       
        return(
            <span>
                {
                    datas.map((d, index)=>{
                        return d !== '' ?<span key={index}><Tag value={d} className="tag-lista-dias"></Tag>{' '}</span>:null
                    })
                }
            </span>
        )    
    }

    const dataColumnTemplate = (rowData) => { 
        return(
            <span>
                <Tag value={rowData.data_solicitacao} className="tag-lista-dias"></Tag>
            </span>
        )    
    }

    const irBodyTemplate = (rowData) => {
        return(
            <span>  
                <div>

                    <Button 
                        title="Visualizar"
                        icon="pi pi-eye"
                        className="btn-darkblue"
                        onClick={()=>{
                                setMostrarModalVisualizar(true);
                                setItemSelecionado(rowData);
                            }
                        }/>   

                    <ComprovanteSolicitacao dados={rowData}/>
                </div> 
            </span>

        )
    }

    useEffect(()=>{
        setSolicitacoesFerias(props.solicitacoesFeriasSolicitante);
    },[props.solicitacoesFeriasSolicitante])


    const statusRowFilterTemplate = () => {
        return <Dropdown  optionLabel="name" options={props.statusSolicitante} onChange={onStatusItemChange} itemTemplate={statusItemTemplate} placeholder={selectedStatusItemFilter?selectedStatusItemFilter:"Filtrar por status"} className="p-column-filter" showClear emptyFilterMessage="Lista de status vazia."/>
    }

    const onStatusItemChange = (e) => {
        setSelectedItem(e.value.name);
        if(e.value.name==="TODOS"){
            setSolicitacoesFerias(props.solicitacoesFeriasSolicitante);
        }else{
            let result = props.solicitacoesFeriasSolicitante.filter(obj => {
                return obj.status === e.value.name;
            })
            setSolicitacoesFerias(result);
        }
        
    }

    const statusItemTemplate = (option) => {
        return (
            <span>{option.name}</span>
        );
    }

    useEffect(()=>{
        if(itemSelecionado) visualizar(itemSelecionado); // para mostrar o status da solictação no modal
    },[itemSelecionado]);

    return(

        <div>
            <div className="datatable-filter">
                <DataTable
                    value={solicitacoesFerias} 
                    responsiveLayout="scroll"
                    scrollable 
                    scrollHeight="65vh"
                    filters={filters}
                    filterDisplay="row"
                    paginator
                    paginatorTemplate="CurrentPageReport FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink"
                    currentPageReportTemplate="{last} de {totalRecords}" 
                    rows={rowsPaginator}
                    emptyMessage="Sem registro."
                >
                    
                    <Column 
                        field="nome" 
                        header="Solicitante"
                        filter 
                        filterPlaceholder="Buscar Nome" 
                        showFilterMenu={false}
                        style={{ flexBasis: '25%' }}
                    />
                    <Column 
                        field="lotacao_solicitante" 
                        header="Lotação"
                        filter 
                        filterPlaceholder="Buscar Lotação" 
                        showFilterMenu={false}
                        style={{ flexBasis: '25%' }}
                    />

                    <Column 
                        field="data_solicitacao" 
                        header="Solicitado em"
                        filter 
                        filterPlaceholder="Buscar" 
                        showFilterMenu={false}
                        body={dataColumnTemplate}
                        style={{ flexBasis: '10%' }}
                    />

                    <Column 
                        filterField="periodo"
                        header="Período"
                        filter
                        body={diasColumnTemplate}
                        filterPlaceholder="Buscar período"
                        showFilterMenu={false}
                        className="col-centralizado"
                        style={{ flexBasis: '20%' }}
                    />
                   
                    <Column 
                        filterField="status"
                        field="status" 
                        header="Status"
                        body={statusColumnTemplate}
                        showFilterMenu={false} 
                        filterMenuStyle={{ width: '10rem' }} 
                        style={{ flexBasis: '10%' }}
                        filter 
                        filterElement={statusRowFilterTemplate}
                        className="col-centralizado" >
                    </Column>               

                    <Column 
                        filterField="acao"
                        header="Ação" 
                        body={irBodyTemplate} 
                        style={{ flexBasis: '10%' }}
                        className="col-centralizado"/>
                    
                </DataTable>
                    
            </div>

            <Dialog 
                header="Visualizar solicitação"
                visible={mostrarModalVisualizar}
                style={{ width: '75%' }}
                onHide={() =>{setMostrarModalVisualizar(false)}
                }>
               
                    {
                        itemSelecionado?
                            <div className="field">
                                <div>Solicitante: <b>{itemSelecionado.nome}</b></div>
                                <div>Nº da solicitação: <b>{itemSelecionado?itemSelecionado.id_ferias_solicitacao:null}</b></div>
                                <div>Período de férias: <b>{itemSelecionado.data_inicio+' a '+itemSelecionado.data_fim+' — '+itemSelecionado.dias_solicitados+' dias'}</b></div>
                                <div>Status: <b>{itemSelecionado?itemSelecionado.status:null}</b></div>
                                {itemSelecionado.setor_atual_descricao?<div>Tramitando em: <b>{itemSelecionado.setor_atual_descricao}</b></div>:<div></div>}
                            </div>    
                        :<div></div>
                    }
                   
                <br />

                <div>
                    <p>Status da tramitação</p>

                    <ul className="folgas-lista-status">
                        {
                            statusNoModal.map((s)=>{
                                return(
                                    <li key={s.id}>
                                        {
                                            s.status === 1
                                            ?<FaCheckCircle className="icone-folgas-status"/>
                                            :s.status === 2?
                                            <FaRegCircle className="icone-folgas-status"/>
                                            :s.status === 3?
                                            <FaTimesCircle className="icone-folgas-status-red"/>
                                            :<FaCircle className="icone-folgas-status-yellow"/>
                                        }
                                        {s.aprovador}
                                    </li>
                                )
                            })
                        }
                    </ul>
                </div>

                <div>
                    <p>Histórico</p>
                    <ul className="folgas-lista-status">
                        <li id="folgas-listas-historico">
                            <b>1 - SOLICITAÇÃO DE {nomeSolicitante}</b>
                        </li>
                        {
                            itemSelecionado?
                                itemSelecionado.historico.map((h, index)=>{
                                    return(
                                        <div key={index}>
                                        <li id="folgas-listas-historico">
                                            <span><b>{index+2} - {h.status} POR {h.nome} NA DATA {h.data}</b></span>
                                        </li>
                                        </div>
                                    )
                                })
                            :null
                        }
                    </ul>
                </div>

                {
                    itemSelecionado&&itemSelecionado.indeferimento_observacao?
                        <div>
                            {
                                itemSelecionado.status==="CANCELADO"?
                                    <span>
                                        <p>Justificativa de cancelamento</p>
                                        <ul className="folgas-lista-status">
                                            <li><i>- {itemSelecionado.indeferimento_observacao}</i></li>
                                            {
                                                itemSelecionado.indeferimento_arquivo?
                                                <li>
                                                    <a href="#" onClick={()=>baixarArquivo(itemSelecionado.indeferimento_arquivo)} >
                                                        Arquivo em anexo 
                                                    </a> 
                                                </li>
                                                :null
                                            }
                                        </ul>
                                    </span>    
                                :
                                    <span>
                                        <p>Justificativa de indeferimento</p>
                                        <ul className="folgas-lista-status">
                                            <li><i>- {itemSelecionado.indeferimento_observacao}</i></li>
                                            {
                                                itemSelecionado.indeferimento_arquivo?
                                                <li>
                                                    <a href="#" onClick={()=>baixarArquivo(itemSelecionado.indeferimento_arquivo)} >
                                                        Arquivo em anexo 
                                                    </a> 
                                                </li>
                                                :null
                                            }
                                        </ul>
                                    </span> 
                            }
                           
                        </div>
                    :null
                }
                <div>
                    <p>Resultado da Solicitação</p>
                    <ul className="folgas-lista-status">
                        <li><b>{status_final}</b></li>
                    </ul>
                </div> 
            </Dialog>
        </div>
    )
}

export default FeriasTodasSolicitacoes;