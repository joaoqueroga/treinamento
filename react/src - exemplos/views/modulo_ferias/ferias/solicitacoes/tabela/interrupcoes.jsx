import React, {useState, useEffect} from "react";
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import { FilterMatchMode} from 'primereact/api';
import './style.scss'
import { Dialog } from 'primereact/dialog';
import { BiCloudUpload } from "react-icons/bi";
import { Dropdown } from 'primereact/dropdown';
import { InputTextarea } from "primereact/inputtextarea";
import api from "../../../../../config/api";
import Alertas from "../../../../../utils/alertas";
import { getToken, getUserId } from "../../../../../config/auth";
import { FaCheckCircle } from "react-icons/fa";
import { FaRegCircle } from "react-icons/fa";
import { FaTimesCircle } from "react-icons/fa";
import { FaCircle } from "react-icons/fa";

const TabelaInterrupcoes = (props) => {
    const FileDownload = require('js-file-download');
    const [obrigatorioJustificarInterromper, setObrigatorioJustificarInterromper] = useState(false);
    const [obrigatorioJustificarRecusar, setObrigatorioJustificarRecusar] = useState(false);
    const [selectedItem, setSelectedItem] = useState(props.statusAprovador.length?"Selecionado: TODOS":"Sem status");
    const [solicitacoesFerias, setSolicitacoesFerias] = useState([]);
    const [statusNoModal,setStatusNoModal] = useState([]);
    const [mostrarModalInterromper, setMostrarModalInterromper] = useState(false);
    const [mostrarModalVisualizar, setMostrarModalVisualizar] = useState(false);
    const [mostrarModalAprovar, setMostrarModalDeferir] = useState(false);
    const [mostrarModalReprovar, setMostrarModalIndeferir] = useState(false); 
    const [file,setFile] = useState(null); 
    const [itemSelecionado,setItemSelecionado] = useState(null);

    const [nomeSolicitante,setNomeSolicitante] = useState("");
    const [textoIndeferimento, setTextoIndeferimento] = useState(null);
    const [arquivoIndeferimento, setArquivoIndeferimento] = useState(null);

    const [descricaoMotivoInterromper,setDescricaoMotivoInterromper] = useState(null); // inserido pelo solicitante
    const [descricaoMotivoRecusar,setDescricaoMotivoRecusar] = useState(null); // inserido pelo aprovador

    const [filters, statusFilters] = useState(
        {
            'status': { value: null, matchMode: FilterMatchMode.EQUALS },
            'nome': { value:null,matchMode:FilterMatchMode.CONTAINS},
        }
    );
    const [currentPage, setCurrentPage] = useState(1);
    const [firstPaginator,setFirstPaginator] = useState(0);
    const [rowsPaginator,setRowsPaginator] = useState(10);

    const resetStatesAboutJustifyFromModals = ()=>{
        setObrigatorioJustificarInterromper(false);
        setDescricaoMotivoInterromper(null);
        setObrigatorioJustificarRecusar(false);
        setDescricaoMotivoRecusar(null);
    }

    function visualizar(d) {
        setNomeSolicitante(d.nome)
        // status 1: aprovado, 2: sem avaliacao, 3: rejeitado, 4: setor a aprovar
        let nstatus = [];
        if(d.eh_defensor){
            nstatus = [
                {id: 1, aprovador: "Solicitação", status: 1},
                {id: 2, aprovador: "Aprovação do Corregedoria Geral", status: d.autorizado_cg?1:2},
                {id: 3, aprovador: "Aprovação do Gabinete DPG", status: d.autorizado_gdpg?1:2}
            ]
        }else{
            nstatus = [
                {id: 1, aprovador: "Solicitação", status: 1},
                {id: 2, aprovador: "Aprovação do Setor", status: d.autorizado_chefia?1:2},
                {id: 3, aprovador: "Aprovação do Corregedoria Geral", status: d.autorizado_cg?1:2},
                {id: 4, aprovador: "Aprovação do Gabinete SDPG", status: d.autorizado_gspg?1:2}
            ]
        }
        if(d.status === 'INDEFERIDO'){ // encontra o que rejeitou
            for (let i = 0; i < nstatus.length; i++) {
                if(nstatus[i].status === 2){
                    nstatus[i].status = 3;
                    break;
                }
            }
        }else if(d.status === 'TRAMITANDO'){
            for (let i = 0; i < nstatus.length; i++) {
                if(nstatus[i].status === 2){
                    nstatus[i].status = 4;
                    break;
                }
            }
        }

        setStatusNoModal(nstatus);
    }

    const interromperSolicitacaoFerias = (item) => {
        setMostrarModalInterromper(false);
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}};
        let data = {
            "aplicacao":"SGI",
            "id_usuario":getUserId(),
            "id_funcionario": item.id_funcionario,
            "id_ferias_solicitacao": item.id_ferias_solicitacao,
            "data":"01/05/2023",
            "observacao": descricaoMotivoInterromper,
            "arquivo":item.nomeArquivo
        };
        api.post('/ferias/interromper_solicitacao_ferias/', data, config)     
        .then((res)=>{ 
            props.setChamaBuscarSolicitacoes(!props.chamaBuscarSolicitacoes); // o props.aprovador é usado aqui meramente para atualizar a lista de solicitações de férias. 
            item.interromper = null;
            setDescricaoMotivoInterromper(null);
            setFile(null);
            Alertas.sucesso("Solicitação de férias interrompida com sucesso!");
           
        }).catch((error)=>{
            console.log(error);
        })
    }

    const aprovarSolicitacaoFerias = (item) => {
        setMostrarModalDeferir(false);
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}};
        let data = {
            "aplicacao":'SGI',
            "id_usuario":getUserId(),
            "id_ferias_solicitacao": item.id_ferias_solicitacao,
        };
        api.post('/ferias/deferir_solicitacao_ferias/', data, config)     
        .then((res)=>{ 
            props.setChamaBuscarSolicitacoes(!props.chamaBuscarSolicitacoes); // o props.aprovador é usado aqui meramente para atualizar a lista de solicitações de férias. 
            
            Alertas.sucesso("Solicitação de férias aprovada com sucesso!");
           
        }).catch((error)=>{
            console.log(error);
        })
    }

    const indeferirSolicitacaoFerias = (item) => {
        setMostrarModalIndeferir(false);
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}};
        let data = {
            "aplicacao":'SGI',
            "id_usuario":getUserId(),
            "id_ferias_solicitacao": item.id_ferias_solicitacao,
            "observacao":descricaoMotivoRecusar,
            "arquivo":item.nomeArquivo
        };
        api.post('/ferias/indeferir_solicitacao_ferias/', data, config)     
        .then((res)=>{ 
            props.setChamaBuscarSolicitacoes(!props.chamaBuscarSolicitacoes); // o props.aprovador é usado aqui meramente para atualizar a lista de solicitações de férias. 
            item.indeferir = null;
            setDescricaoMotivoRecusar(null);
            setFile(null);
            Alertas.sucesso("Solicitação de férias cancelada com sucesso!");
           
        }).catch((error)=>{
            console.log(error);
        })
    }

    function nomeArquivo(item) {
        let nome = file.name;
        let arr = nome.split('.');
        let tipo = arr[arr.length - 1];
        let d = new Date();
        let tempo = d.toISOString().split('.')[0];

        let texto = ''; 
        if(item.indeferir) texto = 'indeferimento_ferias';
        else if(item.interromper) texto = 'deferimento_ferias';

        return `${texto}_${itemSelecionado.id_solicitacao}_${tempo}.${tipo}`;
    }

    function enviarArquivo(item) {
        if(descricaoMotivoInterromper||descricaoMotivoRecusar){
            if(file){
                let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
                let nome = nomeArquivo(file);

                const formData = new FormData();
                formData.append("arquivo", file);
                formData.append("nome", nome);

                api.post('core/upload_pdf', formData, config)
                .then((res)=>{
                    if(res.data === 200){
                        item.nomeArquivo = nome;
                        if(item.indeferir)indeferirSolicitacaoFerias(item);
                        else if(item.interromper)interromperSolicitacaoFerias(item);
                    }else{
                        Alertas.erro("Erro no upload do arquivo");
                    }
                }).catch((err)=>{
                    Alertas.erro(err);
                })
            }else{
                item.nomeArquivo = null;
                if(item.indeferir)indeferirSolicitacaoFerias(item);
                else if(item.interromper)interromperSolicitacaoFerias(item);
            }
            
        }else{
            setObrigatorioJustificarRecusar(true);
        }
    }

    function baixarArquivo(nome_arquivo) {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}, responseType: 'blob'}
        let data = {
            nome: nome_arquivo
        }
        api.post('core/download_pdf', data, config)
        .then((res)=>{
            FileDownload(res.data, `${nome_arquivo}.pdf`);
        }).catch((err)=>{
            let msg = "Falha no download";
            if(err.response.status === 401){
                msg = "Não autorizado";
            }
            Alertas.erro(msg);
        })
    }

    const modalInterromperFooter = (item) => {
        return (
            <div>
                {obrigatorioJustificarInterromper?<p><small className="p-error">Informe os capos obrigatórios (*).</small></p>:null}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setMostrarModalInterromper(false)
                        resetStatesAboutJustifyFromModals()
                    }} 
                    className="btn-2" />
                <Button 
                    label="Interromper solicitação"
                    className="btn-1"
                    onClick={()=>{
                        if(descricaoMotivoInterromper===null) setObrigatorioJustificarInterromper(true);
                        else{
                            item.interromper = true;
                            enviarArquivo(item)
                        }
                    }}
                />
            </div>
        );
    }

    const modalVisualizarFooter = () => {
        return (
            <div>
                <Button 
                    label="Fechar"  
                    onClick={()=>setMostrarModalVisualizar(false)} 
                    className="btn-2"
                />
            </div>
            
        );
    }

    const modalIndeferirFooter = (item) => {
        return (
            <div>
                {obrigatorioJustificarRecusar?<p><small className="p-error">Informe os capos obrigatórios (*).</small></p>:null}
                <Button 
                    label="Indeferir solicitação" 
                    className="btn-1"
                    onClick={()=>{
                        if(descricaoMotivoRecusar===null) setObrigatorioJustificarRecusar(true);
                        else{
                            item.indeferir = true;
                            enviarArquivo(item)
                        }
                        
                    }}
                />
            </div>
            
        );
    }

    const modalDeferirFooter = (item) => {
        return (
            <div>
                <Button 
                    label="Deferir solicitação"  
                    onClick={()=>{
                        aprovarSolicitacaoFerias(item);
                        setMostrarModalDeferir(false)
                    }} 
                    className="btn-1"
                />
            </div>
            
        );
    }

    const irBodyTemplate = (rowData) => {
        return(
            <span>   
                <div>


                {
                    !rowData.mostrar_deferir_indeferir?
                          null:
                        <div>
                            <Button 
                                title="Deferir"
                                icon="pi pi-check"
                                className="btn-green"
                                onClick={()=>{
                                    setMostrarModalDeferir(true);
                                    setItemSelecionado(rowData);
                                } 
                                }/>       
                            <Button 
                                title="Indeferir"
                                icon="pi pi-times"
                                className="btn-red"
                                onClick={()=>{
                                    setMostrarModalIndeferir(true);
                                    setItemSelecionado(rowData);
                                } 
                                }/> 
                        </div>

                }
                    <Button 
                        title="Visualizar"
                        icon="pi pi-eye"
                        visible={rowData.mostrar_visualizar?true:false}
                        className="btn-darkblue"
                        onClick={()=>{
                                setMostrarModalVisualizar(true);
                                setItemSelecionado(rowData);
                            }
                        }
                    />       

                    
                    {/* <Button 
                        title="Interromper"
                        icon="pi pi-pause"
                        className="btn-red"
                        tooltip='Interromper'
                        tooltipOptions={{ position: 'bottom' }}
                        
                        visible={rowData.mostrar_interromper}
                        onClick={()=>{
                                setMostrarModalInterromper(true);
                                setItemSelecionado(rowData);
                            } 
                        }
                        /> */}

                </div>
            </span>
        )
    }

    const renderErrorMessage = (message) =>(
      message!==undefined?<div style={{color:"red"}}>{message}</div>:<div></div>
    );

    
    useEffect(()=>{
        setSolicitacoesFerias(props.solicitacoesFeriasAprovador);
    },[props.solicitacoesFeriasAprovador])


    const statusRowFilterTemplate = () => {
        return <Dropdown  optionLabel="name" options={props.statusAprovador} onChange={onStatusItemChange} itemTemplate={statusItemTemplate} placeholder={selectedItem} className="p-column-filter" showClear emptyFilterMessage="Lista de status vazia."/>
    }

    const onStatusItemChange = (e) => {
        setSelectedItem("Selecionado: "+e.value.name);
        if(e.value.name==="TODOS"){
            setSolicitacoesFerias(props.solicitacoesFeriasAprovador);
        }else{
            let result = props.solicitacoesFeriasAprovador.filter(obj => {
                return obj.status === e.value.name;
            })
            setSolicitacoesFerias(result);
        }
        
    }

    const statusItemTemplate = (option) => {
        return (
            <span>{option.name}</span>
        );
    }

    const onCustomPage = (event) => {
        setFirstPaginator(event.first);
        setRowsPaginator(event.rows);
        setCurrentPage(event.page + 1);
    }

    useEffect(()=>{
        if(itemSelecionado) visualizar(itemSelecionado); // para mostrar o status da solictação no modal
    },[itemSelecionado]);

    return(

        <div>
            <div className="datatable-filter">
                <DataTable className="p-datatable-customers" 
                    value={solicitacoesFerias} 
                    filters={filters} 
                    filterDisplay="row"
                    responsiveLayout="scroll" 
                    scrollHeight="60vh"
                    paginator
                    first={firstPaginator}
                    rows={rowsPaginator}
                    paginatorTemplate="CurrentPageReport FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink"
                    currentPageReportTemplate="{last} de {totalRecords}" 
                    onPage={onCustomPage}
                    globalFilterFields={['status','nome']}
                    emptyMessage="Sem resultado.">
                        <Column 
                            filterField="id_ferias_solicitacao"
                            field="id_ferias_solicitacao" 
                            header="Número da Solicitação"/>

                        <Column 
                            filterField="periodo"
                            field="numero_solicitacao_ferias" 
                            header="Nº da solicitação de férias relacionada"
                            className="col-centralizado"/>

                        <Column 
                            filterField="data_solicitacao"
                            field="operacao_data" 
                            header="Data da solicitação"
                            className="col-centralizado"/>

                        <Column 
                            filterField="status"
                            field="status" 
                            header="Status"
                            showFilterMenu={false} 
                            filterMenuStyle={{ width: '10rem' }} 
                            style={{ minWidth: '10rem' }} 
                            filter 
                            filterElement={statusRowFilterTemplate}
                            className="col-centralizado" />

                        <Column 
                            filterField="nome"
                            field="nome" 
                            header="Solicitante"
                            filterMenuStyle={{ width: '10rem' }} 
                            style={{ minWidth: '10rem' }} 
                            filter 
                            filterPlaceholder="Pesquise por nome"
                            showFilterMenu={false}
                            className="col-centralizado"/>                   

                        <Column 
                            filterField="acao"
                            header="Ação" 
                            body={irBodyTemplate} 
                            style={{ minWidth: '6rem' }}
                            className="col-centralizado"/>
                    
                </DataTable>
                    
            </div>

          
           <Dialog 
                header="Visualizar solicitação"
                visible={mostrarModalVisualizar}
                style={{ width: '50%' }}
                footer={modalVisualizarFooter}
                onHide={() =>{setMostrarModalVisualizar(false)}
                }>
                
                {
                    itemSelecionado?
                        <div className="field">
                            <div>Solicitante: <b>{itemSelecionado.nome}</b></div>
                            <div>Nº da solicitação: <b>{itemSelecionado?itemSelecionado.id_ferias_solicitacao:null}</b></div>
                            <div>Período de férias: <b>{itemSelecionado.data_inicio+' a '+itemSelecionado.data_fim+' — '+itemSelecionado.dias_solicitados+' dias'}</b></div>
                            <div>Status: <b>{itemSelecionado.status}</b></div>
                            {itemSelecionado.setor_atual_descricao?<div>Onde está: <b>{itemSelecionado.setor_atual_descricao}</b></div>:<div></div>}
                        </div>    
                    :<div></div>
                }

                <br />

                <div>
                    <p>Status da tramitação</p>

                    <ul className="folgas-lista-status">
                        {
                            statusNoModal.map((s)=>{
                                return(
                                    <li key={s.id}>
                                        {
                                            s.status === 1
                                            ?<FaCheckCircle className="icone-folgas-status"/>
                                            :s.status === 2?
                                            <FaRegCircle className="icone-folgas-status"/>
                                            :s.status === 3?
                                            <FaTimesCircle className="icone-folgas-status-red"/>
                                            :<FaCircle className="icone-folgas-status-yellow"/>
                                        }
                                        {s.aprovador}
                                    </li>
                                )
                            })
                        }
                    </ul>
                </div>

                <div>
                    <p>Histórico</p>
                    <ul className="folgas-lista-status">
                        <li id="folgas-listas-historico">
                            <b>1 - SOLICITAÇÃO DE {nomeSolicitante}</b>
                        </li>
                        {
                            itemSelecionado?
                                itemSelecionado.historico.map((h, index)=>{
                                    return(
                                        <div key={index}>
                                        <li id="folgas-listas-historico">
                                            <span><b>{index+2} - {h.status} POR {h.nome} NA DATA {h.data}</b></span>
                                        </li>
                                        </div>
                                    )
                                })
                            :null
                        }
                    </ul>
                </div>

                {
                    itemSelecionado&&itemSelecionado.textoIndeferimento?
                        <div>
                            <p>Justificativa de indeferimento</p>
                            <ul className="folgas-lista-status">
                                <li><i>- {textoIndeferimento}</i></li>
                                {
                                    itemSelecionado.arquivoIndeferimento?
                                    <li>
                                        <a href="#" onClick={()=>baixarArquivo(itemSelecionado.arquivoIndeferimento)} >
                                            Arquivo em anexo 
                                        </a> 
                                    </li>
                                    :null
                                }
                            </ul>
                        </div>
                    :null
                }

                <hr />
                
            </Dialog>


            <Dialog 
                header="Interromper solicitação"
                visible={mostrarModalInterromper}
                style={{ width: '50%' }}
                footer={modalInterromperFooter(itemSelecionado)}
                onHide={()=>{
                    setMostrarModalInterromper(false)
                    resetStatesAboutJustifyFromModals()
                }}>
                <div className="field">
                    <div>Solicitante: <b>{itemSelecionado?itemSelecionado.nome:null}</b></div>
                    <div>Nº da solicitação: <b>{itemSelecionado?itemSelecionado.id_ferias_solicitacao:null}</b></div>
                    <div>Período de férias: <b>{itemSelecionado?itemSelecionado.periodo:null}</b></div>
                    
                    <div style={{width: "100%", marginTop:"10px"}}>
                        <p>Justificativa *</p>
                        <InputTextarea
                        id="ferias-texto-justificativa"
                            rows={5} cols={30}
                            name="motivo_interromper"
                            value={descricaoMotivoInterromper}
                            style={{height: "40px"}}
                            onChange={(e)=>setDescricaoMotivoInterromper(e.target.value)}/>
                            {renderErrorMessage}
                    </div>        

                    <div style={{width: "100%"}}>
                        <p>Anexo (opcional)</p>
                            <span className='input-arquivo-servidor'>
                            <label className='label-file-servidor'>
                                <BiCloudUpload size={20}/>
                                <input
                                    value="" 
                                    type="file" 
                                    className='input-files' 
                                    accept='application/pdf'
                                    onChange={e=>setFile(e.target.files[0])}
                                />
                            </label>
                            <p>
                                {file?file.name:"Nenhum arquivo selecionado"}
                            </p>
                        </span>
                    </div>
                    
                </div>

                <br />

                <hr />       

                <span><span style={{color:"red"}}>*</span> - campo obrigatório</span>     
                            
            </Dialog>

            <Dialog 
                header="Indeferir solicitação"
                visible={mostrarModalReprovar}
                style={{ width: '50%' }}
                footer={modalIndeferirFooter(itemSelecionado)}
                onHide={()=>{
                    setMostrarModalIndeferir(false)
                    resetStatesAboutJustifyFromModals()
                }}>
                <div className="field" style={{width:"100%"}}>
                
                    <div>Solicitante: <b>{itemSelecionado!==null?itemSelecionado.nome:null}</b></div>
                    {
                        itemSelecionado&&itemSelecionado.lotacao_solicitante?
                            <div>Lotação: <b>{itemSelecionado?itemSelecionado.lotacao_solicitante:null}</b></div>
                        :<div></div>
                    }
                    <div>Nº da solicitação: <b>{itemSelecionado?itemSelecionado.id_ferias_solicitacao:null}</b></div>
                    <div>Período de férias: <b>{itemSelecionado?itemSelecionado.periodo:null}</b></div>
                    
                    <div style={{width:"100%"}}>
                        <div style={{width: "100%"}}>
                            <p>Justificativa *</p>
                            <InputTextarea
                                id="ferias-texto-justificativa"
                                rows={5} cols={30}
                                name="motivo_recusar"
                                value={descricaoMotivoRecusar}
                                style={{height: "40px"}}
                                onChange={(e)=>setDescricaoMotivoRecusar(e.target.value)}/>
                                {renderErrorMessage}
                        </div>        

                        <div style={{width: "100%"}}>
                            <p>Anexo (opcional)</p>
                            <span className='input-arquivo-servidor'>
                                <label className='label-file-servidor'>
                                    <BiCloudUpload size={20}/>
                                    <input
                                        value="" 
                                        type="file" 
                                        className='input-files' 
                                        accept='application/pdf'
                                        onChange={e=>setFile(e.target.files[0])}
                                    />
                                </label>
                                <p>
                                    {file?file.name:"Nenhum arquivo selecionado"}
                                </p>
                            </span>
                        </div>
                    </div>    

                </div>

                <br />

                <hr />

                <span><span style={{color:"red"}}>*</span> - campo obrigatório</span>
                
            </Dialog> 

            <Dialog 
                header="Deferir solicitação"
                visible={mostrarModalAprovar}
                style={{ width: '50%' }}
                footer={modalDeferirFooter(itemSelecionado)}
                onHide={() =>{setMostrarModalDeferir(false)}
                }
                >
                <div className="field">
                    <div>Solicitante: <b>{itemSelecionado!==null?itemSelecionado.nome:null}</b></div>
                    {
                        itemSelecionado&&itemSelecionado.lotacao_solicitante?
                            <div>Lotação: <b>{itemSelecionado!==null?itemSelecionado.lotacao_solicitante:null}</b></div>
                        :<div></div>
                    }
                    <div>Nº da solicitação: <b>{itemSelecionado!==null?itemSelecionado.id_ferias_solicitacao:null}</b></div>
                    <div>Período de férias: <b>{itemSelecionado!==null?itemSelecionado.periodo:null}</b></div>
                    <div>Status: <b>{itemSelecionado!==null?itemSelecionado.status:null}</b></div>
                    <div>Onde está: <b>{itemSelecionado!==null?itemSelecionado.setor_atual_descricao:null}</b></div>
                </div>

                <br />

                <hr />
                
            </Dialog>

        </div>
    )


}
export default TabelaInterrupcoes;