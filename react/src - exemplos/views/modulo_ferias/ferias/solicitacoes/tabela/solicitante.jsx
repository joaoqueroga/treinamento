import React, {useState, useEffect} from "react";
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import { FilterMatchMode} from 'primereact/api';
import './style.scss'
import { Dialog } from 'primereact/dialog';
import { BiCloudUpload } from "react-icons/bi";
import { Dropdown } from 'primereact/dropdown';
import { InputTextarea } from "primereact/inputtextarea";
import api from "../../../../../config/api";
import { getToken, getUserId } from "../../../../../config/auth";
import Alertas from "../../../../../utils/alertas";
import { FaCheckCircle } from "react-icons/fa";
import { FaRegCircle } from "react-icons/fa";
import { FaTimesCircle } from "react-icons/fa";
import { FaCircle } from "react-icons/fa";
import { Badge } from 'primereact/badge';
import ComprovanteSolicitacao from "../../../componentes/relatorio/comprovante";
import { Calendar } from 'primereact/calendar';
import { Tag } from 'primereact/tag';

const TabelaSolicitante = (props) => {
    const FileDownload = require('js-file-download');
    const [file,setFile] = useState(null); 
    const [statusNoModal,setStatusNoModal] = useState([]);
    const [historico, setHistorico] = useState([]);
    const [nomeSolicitante,setNomeSolicitante] = useState("");
    const [selectedStatusItemFilter,setSelectedItem] = useState(null);
    const [solicitacoesFerias, setSolicitacoesFerias] = useState([]);
    const [dataInterrupcao,setDataInterrupcao] = useState(null);
    
    const [obrigatorioJustificarInterromper, setObrigatorioJustificarInterromper] = useState(false);
    const [obrigatorioJustificarExcluir, setObrigatorioJustificarExcluir] = useState(false);
    const [obrigatorioJustificarCancelar, setObrigatorioJustificarCancelar] = useState(false);
    
    const [mostrarModalExcluir,setMostrarModalExcluir] = useState(false);
    const [mostrarModalCancelar,setMostrarModalCancelar] = useState(false);
    const [mostrarModalVisualizar,setMostrarModalVisualizar] = useState(false);
    const [mostrarModalInterromper, setMostrarModalInterromper] = useState(false);
    const [itemSelecionado,setItemSelecionado] = useState(null);

    const [descricaoMotivoCancelar,setDescricaoMotivoCancelar] = useState(null); // inserido pelo solicitante
    const [descricaoMotivoInterromper,setDescricaoMotivoInterromper] = useState(null);

    const [rowsPaginator,setRowsPaginator] = useState(10);

    const [filters, statusFilters] = useState(
        {
            'status': { value: null, matchMode: FilterMatchMode.EQUALS },
            'nome': { value:null,matchMode:FilterMatchMode.CONTAINS},
            'periodo': {value:null,matchMode:FilterMatchMode.CONTAINS}
        }
    );

    function visualizar(d) {
        setNomeSolicitante(d.nome)
        // status 1: aprovado, 2: sem avaliacao, 3: rejeitado, 4: setor a aprovar
        let nstatus = [];
        if(d.eh_defensor){
            nstatus = [
                {id: 1, aprovador: "Solicitação", status: 1},
                {id: 2, aprovador: "Aprovação da Corregedoria Geral", status: d.autorizado_cg?1:2},
                {id: 3, aprovador: "Aprovação do Gabinete do Defensor Público Geral", status: d.autorizado_gdpg?1:2}
            ]
        }else{
            nstatus = [
                {id: 1, aprovador: "Solicitação", status: 1},
                {id: 2, aprovador: "Aprovação do Setor", status: d.autorizado_chefia?1:2},
                {id: 3, aprovador: "Aprovação da Corregedoria Geral", status: d.autorizado_cg?1:2},
                {id: 4, aprovador: "Aprovação do Gabinete do Subdefensor Público Geral", status: d.autorizado_gspg?1:2}
            ]
        }
        if(d.status === 'INDEFERIDO'){ // encontra o que rejeitou
            for (let i = 0; i < nstatus.length; i++) {
                if(nstatus[i].status === 2){
                    nstatus[i].status = 3;
                    break;
                }
            }
        }else if(d.status === 'TRAMITANDO'){
            for (let i = 0; i < nstatus.length; i++) {
                if(nstatus[i].status === 2){
                    nstatus[i].status = 4;
                    break;
                }
            }
        }

        setStatusNoModal(nstatus);
    }

    const resetStatesValues = ()=>{
        setObrigatorioJustificarInterromper(false);
        setObrigatorioJustificarExcluir(false);
        setObrigatorioJustificarCancelar(false);
        setDescricaoMotivoInterromper(null);
        setDescricaoMotivoCancelar(null);
        setFile(null);
    }

    const cancelarSolicitacaoFerias = (item) => {
        setMostrarModalCancelar(false);
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}};
        let data = {
            "aplicacao":'SGI',
            "id_usuario":getUserId(),
            "id_ferias_solicitacao": item.id_ferias_solicitacao,
            "observacao":descricaoMotivoCancelar,
            "arquivo":item.nomeArquivo
        };
        api.post('/ferias/cancelar_solicitacao_ferias/', data, config)     
        .then((res)=>{
            try {
                let x = JSON.parse(res.data);
                if(x.sucesso === 'S'){
                    item.cancelar = null;
                    resetStatesValues();
                    props.setChamaBuscarSolicitacoes(!props.chamaBuscarSolicitacoes);
                    Alertas.sucesso("Solicitação de cancelamento de férias foi registrada com sucesso!");
            
                }else{
                    Alertas.erro(x.motivo);
                }
            } catch (error) {
                Alertas.erro(error);
            }
        }).catch((erro)=>{
            Alertas.erro(erro);
        })
    }

    const excluirSolicitacaoFerias = (item) => {
        setMostrarModalExcluir(false);
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}};
        let data = {
            "aplicacao":'SGI',
            "id_usuario":getUserId(),
            "id_ferias_solicitacao": item.id_ferias_solicitacao,
            "arquivo":item.nomeArquivo
        };
        api.post('/ferias/excluir_solicitacao_ferias/', data, config)     
        .then((res)=>{
            try {
                let x = JSON.parse(res.data);
                if(x.sucesso === 'S'){
                    resetStatesValues();
                    item.excluir = null;
                    props.setChamaBuscarSolicitacoes(!props.chamaBuscarSolicitacoes);
                    Alertas.sucesso("Solicitação de férias foi excluída com sucesso!");
                }else{
                    Alertas.erro(x.motivo);
                }
            } catch (error) {
                Alertas.erro(error);
            }
        }).catch((erro)=>{
            Alertas.erro(erro);
        })
    }

    const interromperSolicitacaoFerias = (item) => {
        setMostrarModalInterromper(false);
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}};
        let data = {
            "aplicacao":"SGI",
            "id_usuario":getUserId(),
            "id_funcionario": item.id_funcionario,
            "id_ferias_solicitacao": item.id_ferias_solicitacao,
            "data_interrupcao": getFormattedDate(dataInterrupcao),
            "observacao": descricaoMotivoInterromper,
            "arquivo":item.nomeArquivo
        };
        api.post('/ferias/interromper_solicitacao_ferias/', data, config)     
        .then((res)=>{ 

            try{
                let x = JSON.parse(res.data);
                if (x.sucesso === "S") {
                    setDataInterrupcao(null);
                    props.setChamaBuscarSolicitacoes(!props.chamaBuscarSolicitacoes); 
                    item.interromper = null;
                    resetStatesValues();
                    Alertas.sucesso("Solicitação de interrupção de férias foi realizada com sucesso!");
                }else{
                    Alertas.erro(x.motivo);
                }
            }catch(error){
                Alertas.erro(error);
            }
           
        }).catch((error)=>{
            console.log(error);
        })
    }

    function nomeArquivo(item) {
        let nome = file.name;
        let arr = nome.split('.');
        let tipo = arr[arr.length - 1];
        let d = new Date();
        let tempo = d.toISOString().split('.')[0];

        let texto = ''; 
        if(item.cancelar) texto = item.cancelar;
        else if(item.excluir) texto = item.excluir; 
        
        return `${texto}_${item.id_ferias_solicitacao}_${tempo}.${tipo}`;
    }

    function enviarArquivo(item) {
        if(descricaoMotivoCancelar||descricaoMotivoInterromper){
            if(file){
                let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
                let nome = nomeArquivo(item);

                const formData = new FormData();
                formData.append("arquivo", file);
                formData.append("nome", nome);

                api.post('core/upload_pdf', formData, config)
                .then((res)=>{
                    if(res.data === 200){
                        item.nomeArquivo = nome;
                        if(item.cancelar)cancelarSolicitacaoFerias(item);
                        else if(item.interromper)interromperSolicitacaoFerias(item);
                    }else{
                        Alertas.erro("Erro no upload do arquivo");
                    }
                }).catch((err)=>{
                    Alertas.erro(err);
                })
            }else{
                item.nomeArquivo = null;
                if(item.cancelar)cancelarSolicitacaoFerias(item);
                else if(item.interromper)interromperSolicitacaoFerias(item);
            }
            
        }else{
            setObrigatorioJustificarCancelar(true);
        }
    }


    function baixarArquivo(nome_arquivo) {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}, responseType: 'blob'}
        let data = {
            nome: nome_arquivo
        }
        api.post('core/download_pdf', data, config)
        .then((res)=>{
            FileDownload(res.data, `${nome_arquivo}.pdf`);
        }).catch((err)=>{
            let msg = "Falha no download";
            if(err.response.status === 401){
                msg = "Não autorizado";
            }
            Alertas.erro(msg);
        })
    }

    const modalCancelarFooter = (item) => {
        return (
            <div>
                {obrigatorioJustificarCancelar?<p><small className="p-error">Informe os campos obrigatórios (*).</small></p>:null}
                <Button 
                    label="Cancelar solicitação" 
                    className="btn-1"
                    onClick={()=>{
                        if(descricaoMotivoCancelar===null) setObrigatorioJustificarCancelar(true);
                        else{
                            item.cancelar = "cancelamento_ferias";
                            enviarArquivo(item);
                        }
                        
                    }}
                />
            </div>
            
        );
    }

    const modalExcluirFooter = (item) => {
        return (
            <div>
                {obrigatorioJustificarExcluir?<p><small className="p-error">Informe os campos obrigatórios (*).</small></p>:null}
                <Button 
                    label="Não" 
                    className="btn-2"
                    onClick={()=>{
                        setMostrarModalExcluir(false)
                    }}
                />
                <Button 
                    label="Sim, excluir solicitação" 
                    className="btn-1"
                    onClick={()=>{
                        item.excluir = "exclusao_ferias";
                        excluirSolicitacaoFerias(item);
                    }}
                />
            </div>
            
        );
    }

    const modalVisualizarFooter = () => {
        return (
            <div>
                <Button 
                    label="Fechar"  
                    onClick={()=>setMostrarModalVisualizar(false)} 
                    className="btn-2"
                />
            </div>
            
        );
    }

    const modalInterromperFooter = (item) => {
        return (
            <div>
                {obrigatorioJustificarInterromper?<p><small className="p-error">Informe os campos obrigatórios (*).</small></p>:null}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setMostrarModalInterromper(false)
                        resetStatesValues()
                    }} 
                    className="btn-2" />
                <Button 
                    label="Interromper solicitação"
                    className="btn-1"
                    onClick={()=>{
                        if(descricaoMotivoInterromper===null||dataInterrupcao===null) setObrigatorioJustificarInterromper(true);
                        else{
                            item.interromper = "interrompimento_ferias";
                            enviarArquivo(item)
                        }
                    }}
                />
            </div>
        );
    }

    const statusColumnTemplate = (rowData) => {
        switch (rowData.status) {
            case 'APROVADO':
                return <Tag value={rowData.status} className="tag-aprovado"></Tag>;

            case 'INDEFERIDO':
                return <Tag value={rowData.status} className="tag-indeferido"></Tag>;

            case 'CANCELADO':
                return <Tag value={rowData.status} className="tag-cancelado"></Tag>;

            case 'TRAMITANDO':
                return <Tag value={rowData.status} className="tag-tramitando"></Tag>;

            default:
                return <Tag value={rowData.status} className="tag-tramitando"></Tag>;
               
        }
    }

    const diasColumnTemplate = (rowData) => {
        let datas = rowData.periodo.split(' — ');       
        return(
            <span>
                {
                    datas.map((d, index)=>{
                        return d !== '' ?<span key={index}><Tag value={d} className="tag-lista-dias"></Tag>{' '}</span>:null
                    })
                }
            </span>
        )    
    }

    const irBodyTemplate = (rowData) => {
        return(
            <span>  
                <div>

                    <Button 
                        title="Visualizar"
                        icon="pi pi-eye"
                        visible={rowData.mostrar_visualizar}
                        className="btn-darkblue"
                        onClick={()=>{
                                setMostrarModalVisualizar(true);
                                setItemSelecionado(rowData);
                            }
                        }/>   

                    <ComprovanteSolicitacao dados={rowData}/>
                    
                    {/* <Button 
                        title="Excluir"
                        icon="pi pi-trash" 
                        className="btn-red" 
                        visible={rowData.mostrar_excluir}
                        onClick={()=>{
                                setMostrarModalExcluir(true);
                                setItemSelecionado(rowData);
                            }
                        }
                    /> */}
                
                    <Button 
                        title="Cancelar"
                        icon="pi pi-times" 
                        className="btn-red" 
                        visible={rowData.mostrar_cancelar}
                        onClick={()=>{
                                setMostrarModalCancelar(true);
                                setItemSelecionado(rowData);
                            }
                        } />

                    <Button 
                        title="Interromper"
                        icon="pi pi-pause"
                        className="btn-red"
                        tooltip='Interromper'
                        tooltipOptions={{ position: 'bottom' }}
                        visible={rowData.mostrar_interromper}
                        onClick={()=>{
                                setMostrarModalInterromper(true);
                                setItemSelecionado(rowData);
                            } 
                        }/>

                  
                </div> 
            </span>

        )
    }

    const renderErrorMessage = (message) =>(
      message!==undefined?<div style={{color:"red"}}>{message}</div>:<div></div>
    );

    function getFormattedDate(data){
        if(data!==0&&data!==undefined){
            let dia = data.getDate();
            if(dia<10) dia='0'+dia;
            let mes = data.getMonth();
            let ano = data.getFullYear();
            if(mes<9) mes = '0'+(mes+1);
            else mes = (mes+1);
            return dia+"/"+mes+"/"+ano;
        }
        
    }

    
    useEffect(()=>{
        setSolicitacoesFerias(props.solicitacoesFeriasSolicitante);
    },[props.solicitacoesFeriasSolicitante])


    const statusRowFilterTemplate = () => {
        return <Dropdown  optionLabel="name" options={props.statusSolicitante} onChange={onStatusItemChange} itemTemplate={statusItemTemplate} placeholder={selectedStatusItemFilter?selectedStatusItemFilter:"Filtrar por status"} className="p-column-filter" showClear emptyFilterMessage="Lista de status vazia."/>
    }

    const onStatusItemChange = (e) => {
        setSelectedItem(e.value.name);
        if(e.value.name==="TODOS"){
            setSolicitacoesFerias(props.solicitacoesFeriasSolicitante);
        }else{
            let result = props.solicitacoesFeriasSolicitante.filter(obj => {
                return obj.status === e.value.name;
            })
            setSolicitacoesFerias(result);
        }
        
    }

    const statusItemTemplate = (option) => {
        return (
            <span>{option.name}</span>
        );
    }

    useEffect(()=>{
        if(itemSelecionado) visualizar(itemSelecionado); // para mostrar o status da solictação no modal
    },[itemSelecionado]);

    return(

        <div>
            <div className="datatable-filter">
                <DataTable
                    value={solicitacoesFerias} 
                    responsiveLayout="scroll"
                    scrollable 
                    scrollHeight="60vh"
                    filters={filters}
                    filterDisplay="row"
                    paginator
                    paginatorTemplate="CurrentPageReport FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink"
                    currentPageReportTemplate="{last} de {totalRecords}" 
                    rows={rowsPaginator}
                    emptyMessage="Sem registro.">
                    <Column 
                        filterField="id_ferias_solicitacao"
                        field="id_ferias_solicitacao" 
                        header="Solicitação"
                        style={{ flexBasis: '15%' }}
                    />

                    <Column 
                        filterField="data_solicitacao"
                        field="operacao_data" 
                        header="Data da solicitação"
                        className="col-centralizado"
                        style={{ flexBasis: '20%' }}
                        />

                    <Column 
                        filterField="periodo"
                        header="Período"
                        filter
                        body={diasColumnTemplate}
                        filterPlaceholder="Buscar período"
                        showFilterMenu={false}
                        className="col-centralizado"
                        style={{ flexBasis: '25%' }}
                    />
                   
                    <Column 
                        filterField="status"
                        field="status" 
                        header="Status"
                        body={statusColumnTemplate}
                        showFilterMenu={false} 
                        filterMenuStyle={{ width: '10rem' }} 
                        style={{ flexBasis: '20%' }}
                        filter 
                        filterElement={statusRowFilterTemplate}
                        className="col-centralizado" >
                    </Column>               

                    <Column 
                        filterField="acao"
                        header="Ação" 
                        body={irBodyTemplate} 
                        style={{ flexBasis: '20%' }}
                        className="col-centralizado"/>
                    
                </DataTable>
                    
            </div>


            <Dialog 
                header="Cancelar solicitação"
                visible={mostrarModalCancelar}
                style={{ width: '50%' }}
                footer={modalCancelarFooter(itemSelecionado)}
                onHide={()=>{
                    setMostrarModalCancelar(false)
                    resetStatesValues()
                }}>
                <div className="field">
                
                    <div>Solicitante: <b>{itemSelecionado?itemSelecionado.nome:null}</b></div>
                    <div>Nº da solicitação: <b>{itemSelecionado?itemSelecionado.id_ferias_solicitacao:null}</b></div>
                    <div>Período de férias: <b>{itemSelecionado?itemSelecionado.data_inicio+' a '+itemSelecionado.data_fim+' — '+itemSelecionado.dias_solicitados+' dias':null}</b></div>
                    <div>Status: <b>{itemSelecionado?itemSelecionado.status:null}</b></div>
                    {itemSelecionado&&itemSelecionado.setor_atual_descricao?<div>Tramitando em: <b>{itemSelecionado.setor_atual_descricao}</b></div>:<div></div>}

                    <div style={{width: "100%", marginTop:"10px"}}>
                        <span>Justificativa <span style={{color:"red"}}>*</span></span><br/>
                        <InputTextarea
                            id="ferias-texto-justificativa"
                            rows={5} cols={30}
                            name="motivo_cancelar"
                            value={descricaoMotivoCancelar}
                            style={{height: "40px"}}
                            onChange={(e)=>setDescricaoMotivoCancelar(e.target.value)}/>
                            {renderErrorMessage}
                    </div>      
                </div>

                <div style={{width: "100%"}}>
                        <p>Anexo (opcional)</p>
                            <span className='input-arquivo-servidor'>
                            <label className='label-file-servidor'>
                                <BiCloudUpload size={20}/>
                                <input
                                    value="" 
                                    type="file" 
                                    className='input-files' 
                                    accept='application/pdf'
                                    onChange={e=>setFile(e.target.files[0])}
                                />
                            </label>
                            <p>
                                {file?file.name:"Nenhum arquivo selecionado"}
                            </p>
                        </span>
                </div>

                <br />

                <hr />

                <span>Campos com <span style={{color:"red"}}>*</span> são obrigatórios.</span>
                
            </Dialog> 

            <Dialog 
                header="Visualizar solicitação"
                visible={mostrarModalVisualizar}
                style={{ width: '75%' }}
                footer={modalVisualizarFooter}
                onHide={() =>{setMostrarModalVisualizar(false)}
                }>
               
                    {
                        itemSelecionado?
                            <div className="field">
                                <div>Solicitante: <b>{itemSelecionado.nome}</b></div>
                                <div>Nº da solicitação: <b>{itemSelecionado?itemSelecionado.id_ferias_solicitacao:null}</b></div>
                                <div>Período de férias: <b>{itemSelecionado.data_inicio+' a '+itemSelecionado.data_fim+' — '+itemSelecionado.dias_solicitados+' dias'}</b></div>
                                <div>Status: <b>{itemSelecionado?itemSelecionado.status:null}</b></div>
                                {itemSelecionado.setor_atual_descricao?<div>Tramitando em: <b>{itemSelecionado.setor_atual_descricao}</b></div>:<div></div>}
                            </div>    
                        :<div></div>
                    }
                   
                <br />

                <div>
                    <p>Status da tramitação</p>

                    <ul className="folgas-lista-status">
                        {
                            statusNoModal.map((s)=>{
                                return(
                                    <li key={s.id}>
                                        {
                                            s.status === 1
                                            ?<FaCheckCircle className="icone-folgas-status"/>
                                            :s.status === 2?
                                            <FaRegCircle className="icone-folgas-status"/>
                                            :s.status === 3?
                                            <FaTimesCircle className="icone-folgas-status-red"/>
                                            :<FaCircle className="icone-folgas-status-yellow"/>
                                        }
                                        {s.aprovador}
                                    </li>
                                )
                            })
                        }
                    </ul>
                </div>

                <div>
                    <p>Histórico</p>
                    <ul className="folgas-lista-status">
                        <li id="folgas-listas-historico">
                            <b>1 - SOLICITAÇÃO DE {nomeSolicitante}</b>
                        </li>
                        {
                            itemSelecionado?
                                itemSelecionado.historico.map((h, index)=>{
                                    return(
                                        <div key={index}>
                                        <li id="folgas-listas-historico">
                                            <span><b>{index+2} - {h.status} POR {h.nome} NA DATA {h.data}</b></span>
                                        </li>
                                        </div>
                                    )
                                })
                            :null
                        }
                    </ul>
                </div>

                {
                    itemSelecionado&&itemSelecionado.indeferimento_observacao?
                        <div>
                            {
                                itemSelecionado.status==="CANCELADO"?
                                    <span>
                                        <p>Justificativa de cancelamento</p>
                                        <ul className="folgas-lista-status">
                                            <li><i>- {itemSelecionado.indeferimento_observacao}</i></li>
                                            {
                                                itemSelecionado.indeferimento_arquivo?
                                                <li>
                                                    <a href="#" onClick={()=>baixarArquivo(itemSelecionado.indeferimento_arquivo)} >
                                                        Arquivo em anexo 
                                                    </a> 
                                                </li>
                                                :null
                                            }
                                        </ul>
                                    </span>    
                                :
                                    <span>
                                        <p>Justificativa de indeferimento</p>
                                        <ul className="folgas-lista-status">
                                            <li><i>- {itemSelecionado.indeferimento_observacao}</i></li>
                                            {
                                                itemSelecionado.indeferimento_arquivo?
                                                <li>
                                                    <a href="#" onClick={()=>baixarArquivo(itemSelecionado.indeferimento_arquivo)} >
                                                        Arquivo em anexo 
                                                    </a> 
                                                </li>
                                                :null
                                            }
                                        </ul>
                                    </span> 
                            }
                           
                        </div>
                    :null
                } 
            
                <hr />
                
            </Dialog>

            <Dialog 
                header="Interromper solicitação"
                visible={mostrarModalInterromper}
                style={{ width: '50%' }}
                footer={modalInterromperFooter(itemSelecionado)}
                onHide={()=>{
                    setMostrarModalInterromper(false)
                    resetStatesValues()
                }}>
                <div className="field">
                    <div>Solicitante: <b>{itemSelecionado?itemSelecionado.nome:null}</b></div>
                    <div>Nº da solicitação: <b>{itemSelecionado?itemSelecionado.id_ferias_solicitacao:null}</b></div>
                    <div>Período de férias: <b>{itemSelecionado?itemSelecionado.periodo:null}</b></div>
                    <div>Status: <b>{itemSelecionado?itemSelecionado.status:null}</b></div>
                    {itemSelecionado&&itemSelecionado.setor_atual_descricao?<div>Tramitando em: <b>{itemSelecionado.setor_atual_descricao}</b></div>:<div></div>}

                    
                    <div style={{width: "100%", marginTop:"10px"}}>
                        <span>Justificativa <span style={{color:"red"}}>*</span></span><br/>
                        <InputTextarea
                        id="ferias-texto-justificativa"
                            rows={5} cols={30}
                            name="motivo_interromper"
                            value={descricaoMotivoInterromper}
                            style={{height: "40px"}}
                            onChange={(e)=>setDescricaoMotivoInterromper(e.target.value)}/>
                            {renderErrorMessage}
                    </div>   

                    <div style={{width: "100%", marginTop:"10px", marginBottom:"10px"}}>
                        <span>Data de Interrupção <span style={{color:"red"}}>*</span></span>
                        <br/>
                        <Calendar value={dataInterrupcao} onChange={(e) => setDataInterrupcao(e.value)} placeholder="Informe aqui a data"></Calendar>
                    </div>   

                    <div style={{width: "100%"}}>
                        <p>Anexo (opcional)</p>
                            <span className='input-arquivo-servidor'>
                            <label className='label-file-servidor'>
                                <BiCloudUpload size={20}/>
                                <input
                                    value="" 
                                    type="file" 
                                    className='input-files' 
                                    accept='application/pdf'
                                    onChange={e=>setFile(e.target.files[0])}
                                />
                            </label>
                            <p>
                                {file?file.name:"Nenhum arquivo selecionado"}
                            </p>
                        </span>
                    </div>
                    
                </div>

                <br />

                <hr />       

                <span>Campos com <span style={{color:"red"}}>*</span> são obrigatórios.</span>     
                            
            </Dialog>

            <Dialog 
                header="Excluir Solicitação"
                visible={mostrarModalExcluir}
                style={{ width: '50%' }}
                footer={modalExcluirFooter(itemSelecionado)}
                onHide={() =>{setMostrarModalExcluir(false)}
                }>
                <div className="field">
                    <div>Deseja realmente excluir a solicitação <b>{itemSelecionado?itemSelecionado.id_ferias_solicitacao:null}</b>?</div>
                </div>

                <br />

                <hr />
                
            </Dialog>

        </div>
    )


}
export default TabelaSolicitante;