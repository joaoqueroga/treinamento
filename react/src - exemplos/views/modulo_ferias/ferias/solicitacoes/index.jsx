import React, {useState, useEffect} from "react";
import { ProgressSpinner } from 'primereact/progressspinner';
import { BreadCrumb } from 'primereact/breadcrumb';
import './style.scss'
import TabelaSolicitante from "./tabela/solicitante";
import TabelaAprovador from "./tabela/aprovador";
import api from "../../../../config/api";
import { getToken, getUserId } from "../../../../config/auth";
import { TabPanel, TabView } from "primereact/tabview";
import { SelectButton } from "primereact/selectbutton";
import TabelaInterrupcoes from "./tabela/interrupcoes";



function SolicitacoesFerias() {

    const [solicitacoesFeriasSolicitante,setSolicitacoesFeriasSolicitante] = useState([]);
    const [solicitacoesFeriasAprovador, setSolicitacoesFeriasAprovador] = useState([]);
    const [solicitacoesInterrupcoes,setSolicitacoesInterrupcoes] = useState([]);
    const [statusAprovador, setStatusAprovador] = useState([]);
    const [statusSolicitante, setStatusSolicitante] = useState([]);
    const [loading, setLoading] = useState(true);
    const [listarInterromper,setListarInterromper] = useState(false);

    const optionsSelectButton = 
            solicitacoesFeriasSolicitante.length>0&&solicitacoesInterrupcoes.length>0&&solicitacoesFeriasAprovador.length>0?
                [{title_name:'Solicitações de férias',value:1},{title_name:'Solicitações de interrupção',value:2},{title_name:'Solicitações a aprovar',value:3}]
            :
            solicitacoesInterrupcoes.length>0&&solicitacoesFeriasAprovador.length===0?
                [{title_name:'Solicitações de férias',value:1},{title_name:'Solicitações de interrupção',value:2}]
                :
                [{title_name:'Solicitações de férias',value:1},{title_name:'Solicitações a aprovar',value:2}];
    
    
    const [optionSelected,setOptionSelected] = useState(1);
    const [chamaBuscarSolicitacoes,setChamaBuscarSolicitacoes] = useState(true);

    function buscarSolicitacoes(){
        
        // Abaixo, caso seja chamado mais de uma vez durante a utilização do fluxo.
        setSolicitacoesFeriasSolicitante([]);
        setSolicitacoesFeriasAprovador([]);
        setStatusAprovador([]);
        setStatusSolicitante([]);

        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
                    "aplicacao":'SGI',
                    "id_funcionario":getUserId(),
                    "id_usuario":getUserId()
                };
        api.post('/ferias/listar_minhas_solicitacoes_ferias/', data, config)     
        .then((res)=>{
            
            let solicitacoes_ferias = JSON.parse(res.data);

            if(solicitacoes_ferias.length>0){

                let auxArraySolicitacoesAprovador = [];
                let auxArraySolicitacoesSolicitante = [];
                let auxArrayStatusAprovador = [];
                let auxArrayStatusSolicitante = []; 

                solicitacoes_ferias.map((item)=>{
                    item.ferias_ou_folgas = 1;
                    item.periodo = item.data_inicio+' a '+item.data_fim+' — '+item.dias_solicitados+' dias';
                    if(item.id_funcionario!==getUserId()){
                        auxArraySolicitacoesAprovador.push(item);
                        auxArrayStatusAprovador.push({name:item.status});
                    }else{
                        auxArraySolicitacoesSolicitante.push(item);
                        auxArrayStatusSolicitante.push({name:item.status});
                    }
                })

                const uniqueStatusAprovador = auxArrayStatusAprovador.filter((obj, index) => {
                    return index === auxArrayStatusAprovador.findIndex(o => obj.name === o.name);
                });

                const uniqueStatusSolicitante = auxArrayStatusSolicitante.filter((obj, index) => {
                    return index === auxArrayStatusSolicitante.findIndex(o => obj.name === o.name);
                });

                uniqueStatusAprovador.push({name:'TODOS'});
                uniqueStatusSolicitante.push({name:'TODOS'});
            
                setStatusAprovador([...uniqueStatusAprovador]);
                setStatusSolicitante([...uniqueStatusSolicitante]);
                setSolicitacoesFeriasAprovador([...auxArraySolicitacoesAprovador]);
                setSolicitacoesFeriasSolicitante([...auxArraySolicitacoesSolicitante]);
            
                
            }

            setLoading(false);
           
        }).catch((error)=>{
            console.log(error);
        })
    }

    useEffect(() => {
       buscarSolicitacoes();

    }, [chamaBuscarSolicitacoes]);

    const renderTables = () => {
        const table = () => {
            if(optionSelected===optionsSelectButton[0].value&&(optionsSelectButton.length===2||optionsSelectButton.length===3))
                return <TabelaSolicitante solicitacoesFeriasSolicitante={solicitacoesFeriasSolicitante} statusSolicitante={statusSolicitante} setChamaBuscarSolicitacoes={setChamaBuscarSolicitacoes} chamaBuscarSolicitacoes={chamaBuscarSolicitacoes}/>
            else if(optionsSelectButton[1].value===optionSelected&&(optionsSelectButton.length===2||optionsSelectButton.length===3)){
                if(optionsSelectButton.length===2) return <TabelaAprovador solicitacoesFeriasAprovador={solicitacoesFeriasAprovador} statusAprovador={statusAprovador} setChamaBuscarSolicitacoes={setChamaBuscarSolicitacoes} chamaBuscarSolicitacoes={chamaBuscarSolicitacoes}/>
                else if(optionsSelectButton.length===3) return <TabelaInterrupcoes solicitacoesFeriasAprovador={solicitacoesFeriasAprovador} statusAprovador={statusAprovador} setChamaBuscarSolicitacoes={setChamaBuscarSolicitacoes} chamaBuscarSolicitacoes={chamaBuscarSolicitacoes}/>
            }else if(optionsSelectButton[2]!==undefined&&optionsSelectButton[2].value===optionSelected){
                return <TabelaAprovador solicitacoesFeriasAprovador={solicitacoesFeriasAprovador} statusAprovador={statusAprovador} setChamaBuscarSolicitacoes={setChamaBuscarSolicitacoes} chamaBuscarSolicitacoes={chamaBuscarSolicitacoes}/>
            } 
        }
        return (<div>{table()}</div>)
    }

    return ( 
        <div className="visualizar-solicitacoes-ferias">
        <div>
        {
        loading?
        <div className="loading-pagina" ><ProgressSpinner/>Carregando...</div>
        :
        <div>
            <div>
                {  
                solicitacoesFeriasAprovador.length>0||solicitacoesInterrupcoes.length>0?
                    <div className="selectbutton-options">
                        <SelectButton value={optionSelected!==null?optionSelected:optionsSelectButton[0].value} options={optionsSelectButton} onChange={(e) => setOptionSelected(e.value)} optionLabel="title_name"/>
                    </div>
                    :null     
                }
                <div>
                    {
                        //Tabelas para acompanhamento dos processos envolvendo a solicitação de férias.
                        renderTables()
                    }
                </div>

            </div>
        </div>
        }
        </div>
        </div>
    );
}

export default SolicitacoesFerias;