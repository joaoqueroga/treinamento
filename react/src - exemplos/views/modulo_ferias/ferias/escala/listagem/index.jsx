import { useState, useEffect } from "react";
import { ProgressSpinner } from 'primereact/progressspinner';
import '../../../style.scss';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import Swal from 'sweetalert2';
import api from "../../../../../config/api";
import { getToken, getUserId } from "../../../../../config/auth";

import { DataTable } from "primereact/datatable";
import { Column } from 'primereact/column';

function ListagemEscalaFerias() {

    const [loading, setLoading] = useState(true);


    const [mesSelecionado, setMesSelecionado] = useState(null);

    const onMesesChange = (e) => {
        setMesSelecionado(e.target.value);
    }
    const [erro, setErro] = useState(false);
    const [isConverterPecunia, setIsConverterPecunia] = useState(null);

    const [display1, setDisplay1] = useState(false);
    const [display2, setDisplay2] = useState(false);
    const [solicitado, setSolicitado] = useState(true)
    const [list, setList] = useState([]);

    const irBodyTemplate = (rowData) => {
        return (
            <span>
                {rowData.mostrar_excluir &&
                    <Button
                        icon="pi pi-trash"
                        className="btn-red"
                        onClick={() => { ExcluirRespostaPesquisa(rowData.id_pesquisa_escala_ferias) }}
                    />
                }
            </span>
        )
    }

    useEffect(() => {
        let config = { headers: { "Authorization": `Bearer ${getToken()}` } }
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(), 
            "id_funcionario": getUserId() 
        }
        api.post('/ferias/listar_pesquisa_escala_ferias_funcionario/', data, config)
            .then((res) => {
                let x = JSON.parse(res.data);
                if (Array.isArray(x)) {
                    setList(x);
                    setLoading(false);
                } else {
                    setErro(true);
                }
            })
    }, []);


    function ExcluirRespostaPesquisa(id) {
        setSolicitado(false);
        Swal.fire({
            icon: 'warning',
            text: 'Tem certeza que deseja excluir?',
            showCancelButton: true,
            confirmButtonText: 'sim',
            cancelButtonText: 'não',
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed == true) {
                let data = { "aplicacao": "SGI", "id_usuario": getUserId(), "funcionarios": [{ "id_funcionario": getUserId(), 'excluir': true, 'id_pesquisa_escala_ferias': id }] }
                let config = { headers: { "Authorization": `Bearer ${getToken()}` } }
                api.post("ferias/salvar_pesquisa_escala_ferias_funcionario/", data, config).then((res) => {
                    let x = JSON.parse(res.data);
                    if (x.sucesso === "S") {
                        setSolicitado(false);
                        Swal.fire({
                            icon: 'success',
                            title: "Sucesso",
                            text: `Resposta excluida`,
                            confirmButtonText: 'fechar',
                        })
                        reload()
                    }
                    else {
                        Swal.fire({
                            icon: 'error',
                            title: "Erro",
                            text: `${x.motivo}`,
                            confirmButtonText: 'fechar',
                        })
                    }
                })
            }
        })
    }

    const renderFooter1 = (name) => {
        return (
            <div>
                <Button label="Cancelar" icon="pi pi-times" autoFocus onClick={() => setDisplay1(false)} className="btn-2" />
            </div>
        );
    }

    const renderFooter2 = (name) => {
        return (
            <div>
                <Button label="Não" icon="pi pi-times" autoFocus onClick={() => setDisplay2(false)} className="btn-2" />
                <Button label="Sim" icon="pi pi-check" className="btn-1" />
            </div>
        );
    }

    function reload() {
        let config = { headers: { "Authorization": `Bearer ${getToken()}` } }
        let data = { 
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            "id_funcionario": getUserId() 
        }
        api.post('/ferias/listar_pesquisa_escala_ferias_funcionario/', data, config)
            .then((res) => {
                let x = JSON.parse(res.data);
                if (Array.isArray(x)) {
                    setList(x);
                    setLoading(false);
                } else {
                    setErro(true);
                }
            })
    }

    const pecuniaBodyTemplate = (rowData) => {
        if (rowData.vende_ferias) {
            return (
                <>SIM</>
            )
        } else {
            return (
                <>NÃO</>
            )
        }
    }

    return (
        <div>
            {
                loading ?
                    <div className="loading-pagina" ><ProgressSpinner />Carregando...</div>
                    :
                    <div>

                        <div>
                            <DataTable value={list}>

                                <Column
                                    field="descricao"
                                    header="Pesquisa"
                                />

                                <Column
                                    field="mes_descricao"
                                    header="Mês Selecionado"
                                    className="col-centralizado"
                                />

                                <Column
                                    header="Venda de 1/3 da Pecúnia?"
                                    body={pecuniaBodyTemplate}
                                />

                                <Column
                                    field="botao"
                                    header="Ação"
                                    body={irBodyTemplate}
                                    className="col-centralizado"
                                    style={{ flexBasis: '20%' }}
                                />

                            </DataTable>

                        </div>

                    </div>
            }

            <Dialog
                header="Confirmar Escala de Férias?"
                visible={display1}
                style={{ width: '50vw' }}
                footer={renderFooter1}
                onHide={() => setDisplay1(false)}>
                <p>Mês Selecionado para recebimento de pagamento de férias: {mesSelecionado?.nome}</p>
                <p>Opção para converter 1/3 das férias em pecúnia: {isConverterPecunia ? 'Sim' : 'Não'}</p>
            </Dialog>

            <Dialog
                header="Escala de Férias Confirmada"
                visible={display2}
                style={{ width: '50vw' }}
                footer={renderFooter2}
                onHide={() => setDisplay2(false)}>
                <p>Gostaria de realizar solicitação de férias para o mês marcado na escala?</p>
            </Dialog>
        </div>
    );
}

export default ListagemEscalaFerias;