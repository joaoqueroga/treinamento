import { useState, useEffect } from "react";
import { ProgressSpinner } from 'primereact/progressspinner';
import '../../style.scss';
import { Dropdown } from 'primereact/dropdown';
import { RadioButton } from 'primereact/radiobutton';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import './style.scss';
import Swal from 'sweetalert2';
import api from "../../../../config/api";
import { getToken, getUserId } from "../../../../config/auth";
import { BreadCrumb } from 'primereact/breadcrumb';
import { DataTable } from "primereact/datatable";
import { Column } from 'primereact/column';
import { Tag } from 'primereact/tag'; 

function EscalaFerias() {

    const [loading, setLoading] = useState(true);
    const [eh_defensor, setEhDefensor] = useState(false);
    const [showSaveButton, setShowSaveButton] = useState(false);

    const [mesSelecionado, setMesSelecionado] = useState(null);
    const [mesSelecionado2, setMesSelecionado2] = useState(null);

    const items = [
        { label: 'Portal do Servidor' },
        { label: 'Escala de Férias' }
    ];
    const home = { icon: 'pi pi-home', url: '/inicio' }

    const onMesesChange = (e) => {
        setMesSelecionado(e.target.value);
    }

    const onMeses2Change = (e) => {
        setMesSelecionado2(e.target.value);
    }

    const [meses, setMeses] = useState([]);
    const [meses2, setMeses2] = useState([]);

    const [id_pesquisa_escala_ferias, setIdPesquisaEscalaFerias] = useState();
    const [initials, setInitials] = useState([]);
    const [descricao, setDescricao] = useState("");
    const [isConverterPecunia, setIsConverterPecunia] = useState(null);
    const [isConverterPecunia2, setIsConverterPecunia2] = useState(null);
    const [calendario, setCalendario] = useState(null);

    const [display0, setDisplay0] = useState(false);
    const [display1, setDisplay1] = useState(false);
    const [display2, setDisplay2] = useState(true);
    const [display3, setDisplay3] = useState(false);
    const [solicitado, setSolicitado] = useState(true);

    useEffect(() => {
        let config = { headers: { "Authorization": `Bearer ${getToken()}` } }
        let data = { 
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            "id_funcionario": getUserId() 
        }
        api.post('/ferias/iniciar_tela_pesquisa_escala_ferias_funcionario/', data, config)
        .then((res) => {        
            try {
                let x = JSON.parse(res.data);
                setInitials(x);
                if(x.descricao_pesquisa_escala_ferias){
                    setDescricao(x.descricao_pesquisa_escala_ferias.toLowerCase());
                }
                
                setMeses(x.meses_disponiveis);
                setEhDefensor(x.eh_defensor);
                setLoading(false);
            } catch (error) {
                Swal.fire({
                    icon: 'error',
                    title: "Erro",
                    text: `${error}`,
                    confirmButtonText: 'fechar',
                })
            }
        })
    }, []);

    const irBodyTemplate = (rowData) => {
        return (
            <span>

                {rowData.mostrar_excluir &&
                    <Button
                        icon="pi pi-trash"
                        className="btn-red"
                        onClick={() => { ExcluirRespostaPesquisa(initials.id_pesquisa_escala_ferias) }}
                    />
                }

            </span>

        )
    }

    function reload() {
        setDisplay3(false);
        setMesSelecionado(null);
        setMesSelecionado2(null);
        setIsConverterPecunia(null);
        setIsConverterPecunia2(null);
        setDisplay0(false);
        setDisplay2(true);

        let config = { headers: { "Authorization": `Bearer ${getToken()}` } }
        let data = { 
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            "id_funcionario": getUserId() 
        }
        api.post('/ferias/iniciar_tela_pesquisa_escala_ferias_funcionario/', data, config)
            .then((res) => {
                let x = JSON.parse(res.data);
                setMeses(x.meses_disponiveis);
                setInitials(x);
                setEhDefensor(x.eh_defensor);
                setLoading(false);
            })
    }

    function reload_meses() {
        let config = { headers: { "Authorization": `Bearer ${getToken()}` } }
        let data = { 
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            "id_funcionario": getUserId(),
            "mes": mesSelecionado.mes 
        }
        api.post('/ferias/iniciar_tela_pesquisa_escala_ferias_funcionario/', data, config)
        .then((res) => {
            let x = JSON.parse(res.data);
            setMeses2(x.meses_disponiveis);
        })
    }

    function ExcluirRespostaPesquisa(id) {
        let texto = 'Tem certeza que deseja excluir?';
        if (eh_defensor) {
            texto += ' Ao excluir, ambos os períodos deverão ser selecionados novamente.'
        }

        setSolicitado(false);
        Swal.fire({
            icon: 'warning',
            text: texto,
            showCancelButton: true,
            confirmButtonText: 'sim',
            cancelButtonText: 'não',
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed == true) {
                let data = { "aplicacao": "SGI", "id_usuario": getUserId(), "funcionarios": [{ "id_funcionario": getUserId(), 'excluir': true, 'id_pesquisa_escala_ferias': id }] }

                let config = { headers: { "Authorization": `Bearer ${getToken()}` } }
                api.post("ferias/salvar_pesquisa_escala_ferias_funcionario/", data, config).then((res) => {
                    let x = JSON.parse(res.data);
                    if (x.sucesso === "S") {
                        setSolicitado(false);
                        Swal.fire({
                            icon: 'success',
                            title: "Sucesso",
                            text: `Resposta excluida`,
                            confirmButtonText: 'fechar',
                        })
                        reload();
                    }

                    else {
                        Swal.fire({
                            icon: 'error',
                            title: "Erro",
                            text: `${x.motivo}`,
                            confirmButtonText: 'fechar',
                        })
                    }
                })
            }
        })


    }

    const renderFooter3 = (name) => {
        return (
            <div>
                <Button label="Cancelar" icon="pi pi-times" autoFocus onClick={() => setDisplay3(false)} className="btn-2" />
                <Button label="Confirmar" icon="pi pi-check" onClick={salvarEscalas} className="btn-1" />
            </div>
        );
    }

    const salvarEscalas = () => {
        let diasVenda = 0;
        if (isConverterPecunia) diasVenda = 10;

        let diasVenda2 = 0;
        if (eh_defensor) {
            if (isConverterPecunia2) diasVenda = 10;
        }

        let data;
        if (eh_defensor) {
            data = {
                "aplicacao": "SGI",
                "id_usuario": getUserId(),
                "funcionarios": [{
                    "id_funcionario": getUserId(),
                    "dias": diasVenda,
                    "mes_recebimento": mesSelecionado.chave,
                    "id_pesquisa_escala_ferias": initials.id_pesquisa_escala_ferias
                },
                {
                    "id_funcionario": getUserId(),
                    "dias": diasVenda2,
                    "mes_recebimento": mesSelecionado2.chave,
                    "id_pesquisa_escala_ferias": initials.id_pesquisa_escala_ferias
                }
                ]
            }

        } else {
            data =
                { "aplicacao": "SGI", "id_usuario": getUserId(), "funcionarios": [{ "id_funcionario": getUserId(), "dias": diasVenda, "mes_recebimento": mesSelecionado.chave, "id_pesquisa_escala_ferias": initials.id_pesquisa_escala_ferias }] }

        }

        let config = { headers: { "Authorization": `Bearer ${getToken()}` } }
        api.post("ferias/salvar_pesquisa_escala_ferias_funcionario/", data, config).then((res) => {
            let x = JSON.parse(res.data);
            if (x.sucesso === "S") {
                Swal.fire({
                    icon: 'success',
                    title: "Sucesso",
                    text: `Atualizado`,
                    confirmButtonText: 'Fechar',
                })
                reload();

            } else {
                Swal.fire({
                    icon: 'error',
                    title: "Erro",
                    text: `${x.motivo}`,
                    confirmButtonText: 'Fechar',
                })
            }
        })
    }

    const pecuniaBodyTemplate = (rowData) => {
        if (rowData.vende_ferias) {
            return (
                <>SIM</>
            )
        } else {
            return (
                <>NÃO</>
            )
        }

    }

    const tagRespondida = <span>Respondida <i className="pi pi-check"></i></span>;
    
    return (
        <div className='view'>
            <div className="view-body">
                {
                    loading ?
                        <div className="loading-pagina" ><ProgressSpinner />Carregando...</div>
                        :
                        <div style={{ height: "100%" }}>
                            <div className="header" style={{ marginTop: "5px" }}>
                                <BreadCrumb model={items} home={home} />
                                <h6 className="titulo-header">Escala de Férias {initials.descricao_pesquisa_escala_ferias == null ? <></> : <span className="h1-titulo"> - {initials.descricao_pesquisa_escala_ferias}</span>}</h6>
                            </div>
                            {/* condicional de ter ou nao pesquisa ativa no dia */}
                            {initials.descricao_pesquisa_escala_ferias == null ?
                                <h6>Sem pesquisa vigente</h6>
                                :
                                // se ja tiver respondido a pesquisa corrente
                                initials.encontrou ?
                                    <div>
                                        <div className="top-box">
                                            <h1>Escala de Férias <span className="h1-titulo"> - {descricao}</span></h1>
                                            <div style={{display:'flex-inline'}}>
                                                <Tag severity="success" value={tagRespondida} className="tag-respondida"></Tag>
                                            </div>
                                        </div>

                                        <DataTable value={initials.pesquisas_respondidas}>

                                            <Column
                                                field="descricao"
                                                header="Pesquisa"
                                            />

                                            <Column
                                                field="mes_descricao"
                                                header="Mês Selecionado"
                                                className="col-centralizado"
                                            />

                                            <Column
                                                header="Venda de 1/3 da Pecúnia?"
                                                body={pecuniaBodyTemplate}
                                            />

                                            <Column
                                                field="botao"
                                                header="Ação"
                                                body={irBodyTemplate}
                                                className="col-centralizado"
                                                style={{ flexBasis: '20%' }}
                                            />

                                        </DataTable>

                                        <div>
                                            <br />
                                            <span className="label-descricao"> *Sua solicitação foi enviada, Acompanhe o status em </span>  
                                            <a className="link" href="/publico/solicitacoes">Acompanhar Solicitações</a>
                                        </div>
                                        <div>
                                            <span className="label-descricao"> *Para editar a escala de férias, é preciso excluir e efetuar novamente essa solicitação.  </span>  
                                        </div>
                                    </div>

                                :
                                    //se nao tiver respondido a pesquisa corrente
                                    <div style={{ height: "100%" }}>
                                        <div className="top-box">
                                        <h1>Escala de Férias <span className="h1-titulo"> - {descricao}</span></h1>
                                    </div>
                                        {/*view aqui*/}
                                        {
                                            eh_defensor ?
                                                <div className="area-escala-defensor-ferias">
                                                    {/* 'defensor' */}
                                                    <div className="caixa-escala-esq">
                                                        <div id="div-1-escala">
                                                            <h5 style={{ margin: '0' }}>Mês para recebimento:</h5>
                                                            <h5>1º Período</h5>
                                                            <div style={{ textAlign: 'center', margin: '20px 0' }}>
                                                                <Dropdown
                                                                    value={mesSelecionado}
                                                                    options={meses}
                                                                    onChange={(event) => onMesesChange(event)}
                                                                    optionLabel="mes"
                                                                    placeholder="Selecione o Mês"
                                                                    className="dropdown_darkblue"
                                                                />
                                                            </div>
                                                            <h5>Pecúnia</h5>
                                                            <div className="field-checkbox">
                                                                <label className="label-descricao" htmlFor="binary">Deseja converter 1/3 do saldo de férias em pecúnia?</label>
                                                                <div style={{ margin: '10px 0' }}>
                                                                    <RadioButton inputId="radio11" name="radioPeriodo" onChange={(e) => setIsConverterPecunia(true)} checked={isConverterPecunia === true} />
                                                                    <label htmlFor="radio11">Sim</label>

                                                                    <RadioButton inputId="radio12" name="radioPeriodo" style={{ marginLeft: '15px' }} onChange={(e) => setIsConverterPecunia(false)} checked={isConverterPecunia === false} />
                                                                    <label htmlFor="radio12">Não</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="container-row">
                                                            {
                                                                display0 == false ?
                                                                    <Button label="Confirmar" icon="pi pi-check" className="btn-1" disabled={display0 == false && mesSelecionado && isConverterPecunia != null ? false : true} 
                                                                    onClick={(e) => { 
                                                                        setDisplay0(true); 
                                                                        document.getElementById('div-1-escala').classList.add("desabilitado"); 
                                                                        document.getElementById('div-2-escala').classList.remove("desabilitado");
                                                                        reload_meses();
                                                                    }
                                                                    } />
                                                                    :
                                                                    <Button label="Editar" icon="pi pi-undo" className="btn-2" disabled={display1 == false && mesSelecionado && isConverterPecunia != null ? false : true} onClick={(e) => { setDisplay0(false); document.getElementById('div-2-escala').classList.add("desabilitado"); document.getElementById('div-1-escala').classList.remove("desabilitado"); }} />
                                                            }
                                                        </div>
                                                    </div>
                                                    <div className="caixa-escala-esq">
                                                        <div className="desabilitado" id="div-2-escala">
                                                            <div className="field-checkbox">
                                                                <h5 style={{ margin: '0' }}>Mês para recebimento:</h5>
                                                                <h5>2º Período</h5>
                                                                <div style={{ textAlign: 'center', margin: '20px 0' }}>
                                                                    <Dropdown
                                                                        value={mesSelecionado2}
                                                                        options={meses2}
                                                                        onChange={(event) => onMeses2Change(event)}
                                                                        optionLabel="mes"
                                                                        placeholder="Selecione o Mês"
                                                                        className="dropdown_darkblue"
                                                                    />
                                                                </div>
                                                                <h5>Pecúnia</h5>
                                                                <div className="field-checkbox">
                                                                    <label className="label-descricao" htmlFor="radioPeriodo2">Deseja converter 1/3 do saldo de férias em pecúnia?</label>
                                                                    <div style={{ margin: '10px 0' }}>
                                                                        <RadioButton inputId="radio21" name="radioPeriodo2" onChange={(e) => setIsConverterPecunia2(true)} checked={isConverterPecunia2 === true} />
                                                                        <label htmlFor="radio21">Sim</label>

                                                                        <RadioButton inputId="radio22" name="radioPeriodo2" style={{ marginLeft: '15px' }} onChange={(e) => setIsConverterPecunia2(false)} checked={isConverterPecunia2 === false} />
                                                                        <label htmlFor="radio22">Não</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="container-row">
                                                            {
                                                                display1 == false ?
                                                                    <Button label="Confirmar" icon="pi pi-check" className="btn-1" disabled={display0 == true && display1 == false && mesSelecionado2 && isConverterPecunia2 != null ? false : true} onClick={(e) => { setDisplay1(true); setDisplay2(false); document.getElementById('div-2-escala').classList.add("desabilitado"); document.getElementById('div-3-escala').classList.remove("desabilitado"); }} />
                                                                    :
                                                                    <Button label="Editar" icon="pi pi-undo" className="btn-2" disabled={display2 == false && mesSelecionado2 && isConverterPecunia2 != null ? false : true} onClick={(e) => { setDisplay0(true); setDisplay1(false); setDisplay2(true); document.getElementById('div-3-escala').classList.add("desabilitado"); document.getElementById('div-2-escala').classList.remove("desabilitado"); }} />
                                                            }
                                                        </div>
                                                    </div>

                                                    <div className="caixa-escala-esq desabilitado" id="div-3-escala">
                                                        <div>
                                                            <h5 style={{ marginBottom: '25px' }}>Mês Selecionado para recebimento de férias</h5>
                                                            <div style={{ display: 'grid' }}>
                                                                <span><strong>1º Período:</strong> {mesSelecionado ? mesSelecionado.mes : "Não informado"} </span>
                                                                <span><strong>2º Período:</strong> {mesSelecionado2 ? mesSelecionado2.mes : "Não informado"} </span>
                                                            </div>

                                                            <h5 style={{ marginTop: '25px', marginBottom: '10px' }}>Será convertido 1/3 do saldo em pecúnia?</h5>
                                                            <label className="label-descricao">
                                                                {mesSelecionado ?
                                                                    'Saldo selecionado para ' + mesSelecionado.mes + ': ' : null
                                                                }
                                                                {mesSelecionado != null ? isConverterPecunia != null ? isConverterPecunia ? <strong>Sim</strong> : <strong>Não</strong> : <></> : <></>}
                                                            </label>
                                                            <label className="label-descricao">
                                                                {mesSelecionado2 ?
                                                                    'Saldo selecionado para ' + mesSelecionado2.mes + ': ' : null
                                                                }
                                                                {mesSelecionado2 != null ? isConverterPecunia2 != null ? isConverterPecunia2 ? <strong>Sim</strong> : <strong>Não</strong> : <></> : <></>}
                                                            </label>
                                                        </div>
                                                        <span className="label-descricao">Ao clicar em solicitar, você confirma as informações acima</span>

                                                    </div>
                                                    <div></div>
                                                    <div></div>
                                                    <div className="botao-solicitar-escala">
                                                        <Button label="Solicitar" icon="pi pi-check" disabled={display2} onClick={() => setDisplay3(true)} className="btn-1" />
                                                    </div>
                                                </div>

                                                :

                                                <div className="area-escala-ferias">
                                                    {/* 'servidor' */}
                                                    <div className="caixa-escala-esq">
                                                        <div id="div-1-escala">
                                                            <h5 style={{ margin: '0' }}>Mês para recebimento</h5>
                                                            <div style={{ textAlign: 'center', margin: '20px 0' }}>
                                                                <Dropdown
                                                                    value={mesSelecionado}
                                                                    options={meses}
                                                                    onChange={(event) => onMesesChange(event)}
                                                                    optionLabel="mes"
                                                                    placeholder="Selecione o Mês"
                                                                    className="dropdown_darkblue"
                                                                />
                                                            </div>
                                                            <h5>Pecúnia</h5>
                                                            <div className="field-checkbox">
                                                                <label className="label-descricao" htmlFor="binary">Deseja converter 1/3 do saldo de férias em pecúnia?</label>
                                                                <div style={{ margin: '10px 0' }}>
                                                                    <RadioButton inputId="radio11" name="radioPeriodo" onChange={(e) => setIsConverterPecunia(true)} checked={isConverterPecunia === true} />
                                                                    <label htmlFor="radio11">Sim</label>

                                                                    <RadioButton inputId="radio12" name="radioPeriodo" style={{ marginLeft: '15px' }} onChange={(e) => setIsConverterPecunia(false)} checked={isConverterPecunia === false} />
                                                                    <label htmlFor="radio12">Não</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="container-row">
                                                            {
                                                                display0 == false ?
                                                                    <Button label="Confirmar" icon="pi pi-check" className="btn-1" disabled={display0 == false && mesSelecionado && isConverterPecunia != null ? false : true} onClick={(e) => { setDisplay0(true); setDisplay2(false); document.getElementById('div-1-escala').classList.add("desabilitado"); document.getElementById('div-2-escala').classList.remove("desabilitado"); }} />
                                                                    :
                                                                    <Button label="Editar" icon="pi pi-undo" className="btn-2" disabled={display1 == false && mesSelecionado && isConverterPecunia != null ? false : true} onClick={(e) => { setDisplay0(false); setDisplay2(true); document.getElementById('div-2-escala').classList.add("desabilitado"); document.getElementById('div-1-escala').classList.remove("desabilitado"); }} />
                                                            }
                                                        </div>
                                                    </div>

                                                    <div className="caixa-escala-dir" style={{ justifyContent: 'space-between' }}>
                                                        <div className="desabilitado" id="div-2-escala">
                                                            <div>
                                                                <h5>Mês selecionado para recebimento de férias </h5>
                                                                <div style={{ margin: '20px 0', textAlign: 'center' }}>{mesSelecionado == null ?
                                                                    'Não informado'
                                                                    :
                                                                    <>
                                                                    <Tag severity="success" value={mesSelecionado.mes} className="tag-mes"></Tag>
                                                                        
                                                                    </>
                                                                }
                                                                </div>

                                                                <div className="field-checkbox">
                                                                    <h5>Será convertido 1/3 do saldo em pecúnia?</h5>
                                                                    <div>
                                                                        <label className="label-descricao">Saldo selecionado para <strong>{mesSelecionado == null ? 'Não informado' : mesSelecionado.mes}</strong>: {isConverterPecunia ? 'Sim' : 'Não'}</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <span className="label-descricao">Ao clicar em solicitar, você confirma as informações acima</span>
                                                    </div>
                                                    <div></div>
                                                    <div className="botao-solicitar-escala">
                                                        <Button label="Solicitar" icon="pi pi-check" disabled={display2} onClick={() => setDisplay3(true)} className="btn-1 botao-solicitar-ferias" />
                                                    </div>

                                                </div>
                                        }
                                        <Dialog
                                            header="Confirmar Escala de Férias?"
                                            visible={display3}
                                            style={{ width: '50vw' }}
                                            footer={renderFooter3}
                                            onHide={() => setDisplay3(false)}>
                                            <label className="label-descricao">Gostaria de realizar solicitação de férias para {eh_defensor ? 'os meses' : 'o mês'}:</label>
                                            <div>
                                                <div style={{ display: 'grid', marginTop: '25px' }}>
                                                    <label className="label-descricao"><strong>1º Período: </strong>{mesSelecionado?.mes} </label>
                                                    <label className="label-descricao">Pecúnia: {isConverterPecunia ? 'Sim' : 'Não'}</label>
                                                </div>
                                                {eh_defensor ?
                                                    <div style={{ display: 'grid', marginTop: '25px' }}>
                                                        <label className="label-descricao"><strong>2º Período:  </strong>{mesSelecionado2?.mes}</label>
                                                        <label className="label-descricao">Pecúnia: {isConverterPecunia2 ? 'Sim' : 'Não'}</label>
                                                    </div>
                                                    :
                                                    <div></div>
                                                }
                                            </div>
                                        </Dialog>

                                    </div>
                            }
                        </div>

                }

            </div >
        </div >

    );

}

export default EscalaFerias;