import '../new_version/calendario.scss';
import React,{ useState, useEffect, useContext } from 'react';
import api from "../../../../../../config/api";
import { getToken } from "../../../../../../config/auth";

//import Swal from 'sweetalert2';

export default function Calendario(props){

    const [mesAtual, setMesatual] = useState(0);
    const [mes, setMes] = useState(0); // controla o mes apresentado no caledario 
    const [textoMesAtual, setTextoMesAtual] = useState(''); // mostra o texto no topo do calendario
    const [textoMesPosterior,setTextoMesPosterior] = useState('');

    const [valor_slide, setValor_slide] = useState(10);

    const [selecao, setSelecao] = useState(null);

    
    function apresentacaoMes(mes, ano) {
        let textoMesAtual = '';
        let textoMesPosterior = '';
        switch (mes) {
            case 1:
                textoMesAtual = 'JANEIRO ' + ano;
                textoMesPosterior = 'FEVEREIRO ' + ano;
                break;
            case 2:
                textoMesAtual = 'FEVEREIRO ' + ano;
                textoMesPosterior = 'MARÇO ' + ano;
                break;
            case 3:
                textoMesAtual = 'MARÇO ' + ano;
                textoMesPosterior = 'ABRIL ' + ano;
                break;
            case 4:
                textoMesAtual = 'ABRIL ' + ano;
                textoMesPosterior = 'MAIO ' + ano;
                break;
            case 5:
                textoMesAtual = 'MAIO ' + ano;
                textoMesPosterior = 'JUNHO ' + ano;
                break;
            case 6:
                textoMesAtual = 'JUNHO ' + ano;
                textoMesPosterior = 'JULHO ' + ano;
                break;
            case 7:
                textoMesAtual = 'JULHO ' + ano;
                textoMesPosterior = 'AGOSTO ' + ano;
                break;
            case 8:
                textoMesAtual = 'AGOSTO ' + ano;
                textoMesPosterior = 'SETEMBRO ' + ano;
                break;
            case 9:
                textoMesAtual = 'SETEMBRO ' + ano;
                textoMesPosterior = 'OUTUBRO ' + ano;
                break;
            case 10:
                textoMesAtual = 'OUTUBRO ' + ano;
                textoMesPosterior = 'NOVEMBRO ' + ano;
                break;
            case 11:
                textoMesAtual = 'NOVEMBRO ' + ano;
                textoMesPosterior = 'DEZEMBRO ' + ano;
                break;
            case 12:
                textoMesAtual = 'DEZEMBRO ' + ano;
                textoMesPosterior = 'JANEIRO ' + (Number(ano)+1);
                break;
            default:
                break;
        }

        setTextoMesAtual(textoMesAtual);
        setTextoMesPosterior(textoMesPosterior);
        
    }

    useEffect(() => {
        setMesatual(Number(props.calendario[0][17].mes));
        apresentacaoMes(Number(props.calendario[0][17].mes), props.calendario[0][17].ano);
    },[]);
    
    //usa o index 17 para pegar um dia no meio do mes, garantindo que está no mes correto
    function proxMes(){
        if(mes < props.calendario.length-2){
            if(mesAtual === 12){
                setMesatual(1);
                apresentacaoMes(1, props.calendario[mes+1][17].ano);
            }else{
                setMesatual(mesAtual+1);
                apresentacaoMes(mesAtual+1, props.calendario[mes+1][17].ano);
            }
            setMes(mes+1);
        }
    }
    
    function anteMes(){
        if(mes > 0){
            if(mesAtual === 1){
                setMesatual(12);
                apresentacaoMes(12, props.calendario[mes-1][17].ano);
            }else{
                setMesatual(mesAtual-1);
                apresentacaoMes(mesAtual-1, props.calendario[mes-1][17].ano);
            }
            
            setMes(mes-1);
        }
    }

    function limpaCalendario() {
        let aux = props.calendario;
        aux.map((i)=>{
            i.map((j)=>j.selecionado = false);
        })
        props.setCalendario([...aux]);
    }

    function pintar_calendario(selecao, valor) {
        let aux = props.calendario;
        let cont_marcacao = 0;

        aux.map((mesx, i)=>{
            mesx.map((diax, j)=>{
                if(cont_marcacao < valor){ // se tem dia para marcar
                    if(i === selecao.index_mes){ // se estou no mes atual
                        if(j >= selecao.index_dia && diax.eh_mes_atual && diax.pode_selecionar){
                            diax.selecionado = true;
                            cont_marcacao++;
                            props.setDataFinalFeriasSelecionada(diax.data);
                        }
                    }else if(i > selecao.index_mes){
                        if(diax.eh_mes_atual && diax.pode_selecionar){
                            diax.selecionado = true;
                            cont_marcacao++;
                            props.setDataFinalFeriasSelecionada(diax.data);
                        }
                    }
                }
            });
        })

        props.setCalendario([...aux]);
    }

    function marcar_dia(index, dia, calendario) {
        
        limpaCalendario(); // atualiza o estado do calendario

        /*
            ATENÇÃO: variavel calendario deve ser 
            0: para indicar click no primeiro calendario
            1: para indicar click no segundo calendario
            pois, esse valor é utilizado na somo dos index que indentifica o calendario.

        */

        let selecao = {
            index_dia: index,
            index_mes: mes+calendario
        }
        setSelecao(selecao);
        props.setDataInicialFeriasSelecionada(dia.data);

        pintar_calendario(selecao, props.totalSelecao);
    }

    function definerValor_slider(valor) {
        limpaCalendario();
        setValor_slide(valor);

        if(selecao){
           pintar_calendario(selecao, valor);
        }
    }

    useEffect(()=>{
        if(selecao!==null)definerValor_slider(props.totalSelecao)
    },[props.totalSelecao])

    return(
        <div className='calendario-main'>
            
            <div className='backButton' onClick={anteMes}>
                <span className="pi pi-angle-left"style={{ fontSize: '2rem', fontWeight:'700' }}></span>
            </div>

            <div className="container-mes" style={{marginRight:"5px"}}>
                <div className='calendario-nav'>
                    {textoMesAtual}
                </div>
                <div className="calendario-mes">
                    <span className='calendario-dia'>D</span>
                    <span className='calendario-dia'>S</span>
                    <span className='calendario-dia'>T</span>
                    <span className='calendario-dia'>Q</span>
                    <span className='calendario-dia'>Q</span>
                    <span className='calendario-dia'>S</span>
                    <span className='calendario-dia'>S</span>
                    {
                        props.calendario[mes].map((d, index)=>{
                            return(
                                <span 
                                    className={`calendario-folgas-data`}  
                                    key={index}
                                >
                                    <button
                                        className={`
                                            ${d.eh_mes_atual?"":"mes-anterior-cal"} 
                                            ${d.selecionado?"cal-botao-clicado":""}
                                        `}
                                        disabled={!d.pode_selecionar || !d.eh_mes_atual}
                                        onClick={()=>{marcar_dia(index,d,0)}}
                                    >
                                        {d.dia}
                                    </button>
                                </span>
                            )
                        })
                    }
                </div>
            </div>

            <div className="container-mes">
                <div className='calendario-nav'>
                    {textoMesPosterior}
                </div>
                <div className="calendario-mes">
                    <span className='calendario-dia'>D</span>
                    <span className='calendario-dia'>S</span>
                    <span className='calendario-dia'>T</span>
                    <span className='calendario-dia'>Q</span>
                    <span className='calendario-dia'>Q</span>
                    <span className='calendario-dia'>S</span>
                    <span className='calendario-dia'>S</span>
                    {
                        props.calendario[mes+1].map((d, index)=>{
                            return(
                                <span 
                                    className={`calendario-folgas-data`}  
                                    key={index}
                                >
                                    <button
                                        className={`
                                            ${d.eh_mes_atual?"":"mes-anterior-cal"} 
                                            ${d.selecionado?"cal-botao-clicado":""}
                                        `}
                                        disabled={!d.pode_selecionar || !d.eh_mes_atual}
                                        onClick={()=>{marcar_dia(index,d,1)}}
                                    >
                                        {d.dia}
                                    </button>
                                </span>
                            )
                        })
                    }
                </div>
            </div>

            <div className='nextButton' onClick={proxMes}>
                <span className="pi pi-angle-right"style={{ fontSize: '2rem', fontWeight:'700' }}></span>
            </div>            

        </div>
    )
}
