import '../old_version/calendario.scss';
import React,{ useState, useEffect, useContext } from 'react';
import api from "../../../../../../config/api";
import { getToken } from "../../../../../../config/auth";

//import Swal from 'sweetalert2';

export default function Calendario(props){

    //Este calendário, para funcionar basta passar apenas a data de inicio do calenário em formato 'dd/mm/yyyy'.

    const [datasMesAtual, setDatasMesAtual] = useState([]);
    const [datasMesPosterior, setDatasMesPosterior] = useState([]);
    const [mesAtualTexto, setMesAtualTexto] = useState('');
    const [mesAtualNumero, setMesAtualNumero] = useState('');
    const [mesPosteriorTexto, setMesPosteriorTexto] = useState('');
    const [mesPosteriorNumero,setMesPosteriorNumero] = useState('');
    const [dataAtiva, setDataAtiva] = useState(new Date());
    const [mostrarMesAnterior, setMostrarMesAnterior] = useState(false);
    const [qualCalendario,setQualCalendario] = useState(1);
    const [ultimoDiaMesAtual, setUltimoDiaMesAtual] = useState('');
    const [ultimoDiaMesPosterior, setUltimoDiaMesPosterior] = useState('');
    const [indexDataClicadaCalendario,setIndexDataClicadaCalendario] = useState(0);
  

    function defineReferencialDay(){
        let mesAux = Number.parseInt(props.dataInicioCalendarioFuncionario.split('/')[1]);
        mesAux = mesAux -1;
        let anoAux = Number.parseInt(props.dataInicioCalendarioFuncionario.split('/')[2]);
        let referenceDate = new Date(anoAux,mesAux,1);
        return referenceDate;
    }        


    const  diaReferenciaCalendarioGlobal = defineReferencialDay();

    useEffect(()=>{

        iniciarCalendario();
    
        if(vaiPularParaProximoMes()){
            mesPosterior();
        }
       

    },[props.reloadComponent]);


    function defineMesAtual(data){
        let dataProximoMes = new Date(data.getFullYear(), data.getMonth()+1, 1);
        switch(data.getMonth()){
            case 0:
                setMesAtualNumero(`01/${data.getFullYear()}`)
                setMesAtualTexto(`Janeiro ${data.getFullYear()}`)
                setMesPosteriorNumero(`02/${dataProximoMes.getFullYear()}`);
                setMesPosteriorTexto(`Fevereiro ${dataProximoMes.getFullYear()}`)
                break;
            case 1:
                setMesAtualNumero(`02/${data.getFullYear()}`)
                setMesAtualTexto(`Fevereiro ${data.getFullYear()}`)
                setMesPosteriorNumero(`03/${dataProximoMes.getFullYear()}`)
                setMesPosteriorTexto(`Março ${dataProximoMes.getFullYear()}`)
                break;
            case 2:
                setMesAtualNumero(`03/${data.getFullYear()}`)
                setMesAtualTexto(`Março ${data.getFullYear()}`)
                setMesPosteriorNumero(`04/${dataProximoMes.getFullYear()}`)
                setMesPosteriorTexto(`Abril ${dataProximoMes.getFullYear()}`)
                break;
            case 3:
                setMesAtualNumero(`04/${data.getFullYear()}`)
                setMesAtualTexto(`Abril ${data.getFullYear()}`)
                setMesPosteriorNumero(`05/${dataProximoMes.getFullYear()}`)
                setMesPosteriorTexto(`Maio ${dataProximoMes.getFullYear()}`)
                break;
            case 4:
                setMesAtualNumero(`05/${data.getFullYear()}`)
                setMesAtualTexto(`Maio ${data.getFullYear()}`)
                setMesPosteriorNumero(`06/${dataProximoMes.getFullYear()}`)
                setMesPosteriorTexto(`Junho ${dataProximoMes.getFullYear()}`)
                break;
            case 5:
                setMesAtualNumero(`06/${data.getFullYear()}`)
                setMesAtualTexto(`Junho ${data.getFullYear()}`)
                setMesPosteriorNumero(`07/${dataProximoMes.getFullYear()}`)
                setMesPosteriorTexto(`Julho ${dataProximoMes.getFullYear()}`)
                break;
            case 6:
                setMesAtualNumero(`07/${data.getFullYear()}`)
                setMesAtualTexto(`Julho ${data.getFullYear()}`)
                setMesPosteriorNumero(`08/${dataProximoMes.getFullYear()}`)
                setMesPosteriorTexto(`Agosto ${dataProximoMes.getFullYear()}`)
                break;
            case 7:
                setMesAtualNumero(`08/${data.getFullYear()}`)
                setMesAtualTexto(`Agosto ${data.getFullYear()}`)
                setMesPosteriorNumero(`09/${dataProximoMes.getFullYear()}`)
                setMesPosteriorTexto(`Setembro ${dataProximoMes.getFullYear()}`)
                break;
            case 8:
                setMesAtualNumero(`09/${data.getFullYear()}`)
                setMesAtualTexto(`Setembro ${data.getFullYear()}`)
                setMesPosteriorNumero(`10/${dataProximoMes.getFullYear()}`)
                setMesPosteriorTexto(`Outubro ${dataProximoMes.getFullYear()}`)
                break;
            case 9:
                setMesAtualNumero(`10/${data.getFullYear()}`)
                setMesAtualTexto(`Outubro ${data.getFullYear()}`)
                setMesPosteriorNumero(`11/${dataProximoMes.getFullYear()}`)
                setMesPosteriorTexto(`Novembro ${dataProximoMes.getFullYear()}`)
                break;
            case 10:
                setMesAtualNumero(`11/${data.getFullYear()}`)
                setMesAtualTexto(`Novembro ${data.getFullYear()}`)
                setMesPosteriorNumero(`12/${dataProximoMes.getFullYear()}`)
                setMesPosteriorTexto(`Dezembro ${dataProximoMes.getFullYear()}`)
                break;
            case 11:
                setMesAtualNumero(`12/${data.getFullYear()}`)
                setMesAtualTexto(`Dezembro ${data.getFullYear()}`)
                setMesPosteriorNumero(`01/${dataProximoMes.getFullYear()}`)
                setMesPosteriorTexto(`Janeiro ${dataProximoMes.getFullYear()}`)
                break;
        
        }
    }

    function getDiasNoMes(ano, mes) {
        let dataMes = new Date(ano, mes, 1);

        setDataAtiva(new Date(ano, mes, 1));
        
        let diaInicioMes = dataMes.getDay();
        let diasDoMes = [];

        for(let i = 0; i < diaInicioMes; i++){
            diasDoMes.push({
                data: 0,
                selecionado: false
            });
        }

        while (dataMes.getMonth() === mes) {
            
            diasDoMes.push({
                data:new Date(dataMes),
                selecionado: false
            });

            dataMes.setDate(dataMes.getDate() + 1);
        }

        for(let i = diasDoMes.length; i < 42; i++){
            diasDoMes.push({
                data: 0,
                selecionado: false
            });
        }

        return diasDoMes;

    }
    

    function ativarDias(date) {
        
        let diaAtual = Number.parseInt(props.dataInicioCalendarioFuncionario.split('/')[0]);

        if(date.getMonth()===diaReferenciaCalendarioGlobal.getMonth() && diaReferenciaCalendarioGlobal.getFullYear()===date.getFullYear()){
            if(date.getUTCDate() < diaAtual) return false;
            else return true;
        
        }else return true;
    
        
    }
    
    function mesPosterior(){

        let newDataAtual = new Date(dataAtiva.getFullYear(), dataAtiva.getMonth()+1, 1);
        let newDataPosterior = new Date(dataAtiva.getFullYear(),dataAtiva.getMonth()+2, 1);
        setDatasMesAtual(getDiasNoMes(newDataAtual.getFullYear(), newDataAtual.getMonth()));
        setDatasMesPosterior(getDiasNoMes(newDataPosterior.getFullYear(), newDataPosterior.getMonth()));
        
        //Abaixo, setando o último dia de cada mês ao seu estado
        setUltimoDiaMesAtual(new Date(newDataAtual.getFullYear(), newDataAtual.getMonth() + 1, 0));
        setUltimoDiaMesPosterior(new Date(newDataPosterior.getFullYear(), newDataPosterior.getMonth() + 1, 0));

        defineMesAtual(newDataAtual);
        setDataAtiva(newDataAtual);

        if((newDataAtual.getMonth()===diaReferenciaCalendarioGlobal.getMonth() && newDataAtual.getFullYear()===diaReferenciaCalendarioGlobal.getFullYear())) {
                setMostrarMesAnterior(false);
        }else setMostrarMesAnterior(true);
        
        ativarDias(newDataAtual);
        
    }

    function mesAnterior(){

        let newDataAtual = new Date(dataAtiva.getFullYear(), dataAtiva.getMonth()-1, 1);
        let newDataPosterior = new Date(dataAtiva.getFullYear(), dataAtiva.getMonth(), 1);
        setDatasMesAtual(getDiasNoMes(newDataAtual.getFullYear(), newDataAtual.getMonth()));
        setDatasMesPosterior(getDiasNoMes(newDataPosterior.getFullYear(), newDataPosterior.getMonth()));

        //Abaixo, setando o último dia de cada mês ao seu estado
        setUltimoDiaMesAtual(new Date(newDataAtual.getFullYear(), newDataAtual.getMonth() + 1, 0));
        setUltimoDiaMesPosterior(new Date(newDataPosterior.getFullYear(), newDataPosterior.getMonth() + 1, 0));

        defineMesAtual(newDataAtual);
        setDataAtiva(newDataAtual);

        if((newDataAtual.getMonth()===diaReferenciaCalendarioGlobal.getMonth() && newDataAtual.getFullYear()===diaReferenciaCalendarioGlobal.getFullYear())){
                setMostrarMesAnterior(false);
        }else setMostrarMesAnterior(true);

        ativarDias(newDataAtual);

    }

    function iniciarCalendario(){

        let mesAuxFromDatabase = Number.parseInt(props.dataInicioCalendarioFuncionario.split('/')[1]);
        mesAuxFromDatabase = mesAuxFromDatabase -1;
        let anoAuxFromDatabase = Number.parseInt(props.dataInicioCalendarioFuncionario.split('/')[2]);
        setDatasMesAtual(getDiasNoMes(anoAuxFromDatabase, mesAuxFromDatabase));
        setDatasMesPosterior(getDiasNoMes(anoAuxFromDatabase, mesAuxFromDatabase+1));
        defineMesAtual(new Date(anoAuxFromDatabase, mesAuxFromDatabase, 1));

        //Abaixo, setando o último dia de cada mês ao seu estado
        setUltimoDiaMesAtual(new Date(anoAuxFromDatabase, mesAuxFromDatabase + 1, 0));
        let newDataPosterior = new Date(anoAuxFromDatabase, mesAuxFromDatabase+1, 1);
        setUltimoDiaMesPosterior(new Date(newDataPosterior.getFullYear(), newDataPosterior.getMonth() + 1, 0));

    }

    //vai pular para o próximo mês, caso o mês atual seja direfente do mês de referência que vem do banco de dados.
    function vaiPularParaProximoMes(){
        let monthOfDatabase = Number.parseInt(props.dataInicioCalendarioFuncionario.split('/')[1]);
        monthOfDatabase = monthOfDatabase-1; //menos 1, pois vamos comparar com o mês que é índice do array de meses do ano do javascript
        
        let currentMonth = new Date().getMonth();
        
        if(currentMonth!==monthOfDatabase)return true;
        return false;     
    }


    function recarregarDadosCalendario() {
        setIndexDataClicadaCalendario(0);  

        let tempDiasMesAtual = datasMesAtual, tempDiasMesPorterior = datasMesPosterior;

        tempDiasMesAtual.map((d)=>{
           d.selecionado = false;
        })

        tempDiasMesPorterior.map((d)=>{
          d.selecionado = false;
        })
      
    }

    useEffect(()=>{
        if(indexDataClicadaCalendario!==0){
            selecionaDatas(indexDataClicadaCalendario,qualCalendario);
        } 
    },[props.totalSelecao]);
    

    function getFormattedDate(data){
        if(data!==0&&data!==undefined){
            let dia = data.getDate();
            if(dia<10) dia='0'+dia;
            let mes = data.getMonth();
            let ano = data.getFullYear();
            if(mes<9) mes = '0'+(mes+1);
            else mes = (mes+1);
            return dia+"/"+mes+"/"+ano;
        }
        
    }
    

    function selecionaDatas(index, calendario){
      
      recarregarDadosCalendario();
      setIndexDataClicadaCalendario(index);  
      setQualCalendario(calendario);

      let diasParaMarcar = [], 
          tempDiasMesAtual = datasMesAtual, 
          tempDiasMesPosterior = datasMesPosterior;
      
      let diferencaEntreFimDoMesDiaSelecionado = 0;
     
      if(calendario===1){
        diasParaMarcar = [...datasMesAtual];
        diferencaEntreFimDoMesDiaSelecionado = ultimoDiaMesAtual.getDate() - props.totalSelecao;
      }else if(calendario===2){
        diasParaMarcar = [...datasMesPosterior];
        diferencaEntreFimDoMesDiaSelecionado = ultimoDiaMesPosterior.getDate() - props.totalSelecao;
      }

      

      props.setDataInicialFeriasSelecionada(getFormattedDate(diasParaMarcar[index].data));
    
      for(let i=index;i< (index+props.totalSelecao) ;i ++) {
          if(diasParaMarcar[i]!==undefined&&diasParaMarcar[i]!==0){// Essa condição foi adicionada porque o valor de 'aux[i]' pode ficar undefined durante a manipulação do ranger.
            diasParaMarcar[i].selecionado = true; 
            props.setDataFinalFeriasSelecionada(getFormattedDate(diasParaMarcar[i].data));
          }  
      }

      //Abaixo, bloco de código que faz a marcação no segundo calendário, caso a selação de dias ultrapasse o mês anterior
      if(calendario===1){
        if(diasParaMarcar[index].data!==0){
            let diferencaEntreMeses = (diasParaMarcar[index].data.getDate()-diferencaEntreFimDoMesDiaSelecionado);
            if(diferencaEntreMeses>1){
                let i = 0;
                let count = 0;
                while(count<diferencaEntreMeses-1 && i<tempDiasMesPosterior.length){
                if(tempDiasMesPosterior[i].data!==0) { 
                    tempDiasMesPosterior[i].selecionado = true;
                    props.setDataFinalFeriasSelecionada(getFormattedDate(tempDiasMesPosterior[i].data));
                    count++;
                }    
                i++;
                }      
            }
        }
      }
        
      //Abaixo, setando os valores das datas atualizados ao calendário selecionado.
      if(calendario===1){
        setDatasMesAtual([...diasParaMarcar]);
        setDatasMesPosterior([...tempDiasMesPosterior])
      }else if(calendario===2){
        setDatasMesAtual([...tempDiasMesAtual]);
        setDatasMesPosterior([...diasParaMarcar]);
      }
      

    };

    const calendarioView = ()=>{

        return (
            <div>

                <div className='calendario'>

                        <div className='calendario-main'> 

                                { mostrarMesAnterior ? 
                                    <div onClick={mesAnterior}>
                                        <img className='backButton' src={require('../assets/arrow_left.png')} />
                                    </div>: <div></div>
                                }                           

                               <div style={{marginRight:"5px"}} className='container-mes'>

                                    <div className='calendario-nav'>
                                        {mesAtualTexto} 
                                    </div>
                                    <div className='calendario-mes'>
                                        
                                        <span className='calendario-dia'>D</span>
                                        <span className='calendario-dia'>S</span>
                                        <span className='calendario-dia'>T</span>
                                        <span className='calendario-dia'>Q</span>
                                        <span className='calendario-dia'>Q</span>
                                        <span className='calendario-dia'>S</span>
                                        <span className='calendario-dia'>S</span>
                                        {
                                            props.calendario.map((objectDate, index)=>{
                                                return(
                                                    <span className={`calendario-data ${index===0?'q1':'padrao'} ${index===6?'q2':'padrao'} ${index===35?'q3':'padrao'} ${index===41?'q4':'padrao'}`}  key={index}>
                                                        {objectDate.data!==0?
                                                            
                                                            ativarDias(objectDate.data)?
                                                                
                                                                !objectDate.selecionado?
                                                                        <button 
                                                                            className='cal-botao-ativo'
                                                                            onClick={()=>{selecionaDatas(index,1)}}>
                                                                            {objectDate.data.getDate()}
                                                                        </button>  
                                                                    :
                                                                        <button 
                                                                            className='cal-botao-selecionado'
                                                                            onClick={()=>{selecionaDatas(index,1)}}>
                                                                            {objectDate.data.getDate()}
                                                                        </button>
                                                                    
                                                            :   
                                                            <button disabled>{objectDate.data.getDate()}</button>

                                                        :
                                                        <button disabled className='botao-complementar'>{'.'}</button>
                                                        } 
                                                    </span>
                                                )
                                            })
                                        }
                                    
                                    </div>

                                </div> 

                                <div style={{marginLeft:"5px"}} className='container-mes'>         

                                        <div className='calendario-nav'>
                                            {mesPosteriorTexto} 
                                        </div>  

                                        <div className='calendario-mes'>
                                        
                                            <span className='calendario-dia'>D</span>
                                            <span className='calendario-dia'>S</span>
                                            <span className='calendario-dia'>T</span>
                                            <span className='calendario-dia'>Q</span>
                                            <span className='calendario-dia'>Q</span>
                                            <span className='calendario-dia'>S</span>
                                            <span className='calendario-dia'>S</span>
                                            {
                                                datasMesPosterior.map((objectDate, index)=>{
                                                    return(
                                                        <span className={`calendario-data ${index===0?'q1':'padrao'} ${index===6?'q2':'padrao'} ${index===35?'q3':'padrao'} ${index===41?'q4':'padrao'}`}  key={index}>
                                                            {objectDate.data!==0?
                                                                
                                                                ativarDias(objectDate.data)?
                                                                    
                                                                    !objectDate.selecionado?
                                                                            <button 
                                                                                className='cal-botao-ativo'
                                                                                onClick={()=>{selecionaDatas(index,2)}}>
                                                                                {objectDate.data.getDate()}
                                                                            </button>  
                                                                        :
                                                                            <button 
                                                                                className='cal-botao-selecionado'
                                                                                onClick={()=>{selecionaDatas(index,2)}}>
                                                                                {objectDate.data.getDate()}
                                                                            </button>
                                                                        
                                                                :   
                                                                <button disabled>{objectDate.data.getDate()}</button>

                                                            :
                                                            <button disabled className='botao-complementar'>{'.'}</button>
                                                            } 
                                                        </span>
                                                    )
                                                })
                                            }
                                      
                                </div>

                            </div>   

                            <div onClick={mesPosterior}> 
                                <img className='nextButton' src={require('../assets/arrow_right.png')} />
                            </div>
                             

                            
                        </div>

                </div>    
        
            </div>
        )    
    }


    return(
        calendarioView()
    )
}