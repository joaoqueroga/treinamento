import React, {useState, useEffect} from "react";
import { ProgressSpinner } from 'primereact/progressspinner';
import { BreadCrumb } from 'primereact/breadcrumb';
import './style.scss'
import Calendario from "./calendario/new_version/calendario";
import CaixaVigencias from "../../componentes/selecionar_vigencia/caixa_vigencias";
import RangerDias from '../../componentes/ranger_dias/ranger_dias';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import api from "../../../../config/api";
import { getToken, getUserId, getEhDefensor } from "../../../../config/auth";
import Swal from 'sweetalert2';
import {useNavigate, useLocation} from "react-router-dom";
import { Tag } from 'primereact/tag';   
import { RadioButton } from 'primereact/radiobutton';
import RelatorioSolicitacaoFerias from "../relatorio";
import SeletorDefensor from "../../../../components/SeletorDefensor";
  

function FeriasSolicitacao() {

    const navigate = useNavigate();
    const location = useLocation();
    const usuario_terceiro = location.state&&location.state.usuario?location.state.usuario:null; // vem de outra tela
    const [ehDefensor,setEhDefensor] = useState(usuario_terceiro?usuario_terceiro.eh_defensor:getEhDefensor()!=='N'?true:false);
    
    //Estados para a tela principal de solicitação
    const [loading,setLoading] = useState(true);
    const [reloadComponent,setReloadComponent] = useState(false);
    const [obrigatorio,setObrigatorio] = useState(false);
    //Estados para guardar resultado de requisição do carregamento do calendário
    const [calendario, setCalendario] = useState([]);
    const [mes_atual, setMes_atual] = useState(0);
    const [ano_atual, setAno_atual] = useState('');
    const [minimoDiasFerias, setMinimoDiasFerias] = useState(0);
    const [maximoDiasFerias, setMaximoDiasFerias] = useState(0);
    const [nomeFuncionario,setNomeFuncionario] = useState('');
    const [vigencias,setVigencias] = useState([]);
    //Estados para interação com os componentes de calendário, ranger e checkboxes de vigências. 
    const [minValue,setMinValue] = useState(0);
    const [display, setDisplay] = useState(false);
    const [totalVigencias, setTotalVigencia] = useState(-1); // para que o valor do total de vigências(-1) seja menor que o valor mínimo(0) padrão para que o calendário não seja carregado fora da regra.
    const [totalSelecao, setTotalSelecao]= useState(0)
    const [dataInicialFeriasSelecionada, setDataInicialFeriasSelecionada] = useState(null);
    const [dataFinalFeriasSelecionada, setDataFinalFeriasSelecionada] = useState(null);
    const [selectedVigencias,setSelectedVigencias] = useState([]);
    const [totalDias,setTotalDias] = useState();
    const [replacementDefensorRadioButton,setReplacementDefensorRadioButton] = useState(null);
    const [defensorSubstituto,setDefensorSubstituto] = useState(null);
    const [idFeriasSolicitacaoRegistrada,setIdFeriasSolicitacaoRegistrada] = useState(null);
    

    const items = [
        { label: 'Portal do Servidor' },
        { label: 'Solicitação de Férias' }
    ];
    const home = { icon: 'pi pi-home', url: '/inicio' }

    function loadCalendar(){

        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao":"SGI",
            "id_usuario":getUserId(), 
            "eh_ferias":true, 
            "id_funcionario":usuario_terceiro?usuario_terceiro.id_funcionario:getUserId()
        };
        api.post('/ferias/solicitar_ferias_calendario/',data, config)     
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso==='S'){
                
                let auxVigencias = [];
                let _totalDias = 0;

                x.saldo_ferias_resumo = JSON.parse(x.saldo_ferias_resumo);//convertendo para JSON, pois está vindo como string

                x.saldo_ferias_resumo.forEach((element)=>{
                    let vigencia = {};
                    vigencia.key = element.id_ferias;
                    vigencia.ano_value = element.ano_referencia;
                    vigencia.resumo = element.saldo_resumo?element.saldo_resumo:[];
                    
                    if(String(element.ano_referencia).length===6) {
                        let ano = String(element.ano_referencia).substring(0,4)+'/'+String(element.ano_referencia).substring(4,6);
                        vigencia.ano = 'Saldo '+ano;
                    }else vigencia.ano = 'Saldo '+element.ano_referencia; 
                    
                    vigencia.saldo = element.saldo+' Dias disponíveis';
                    vigencia.valor = element.saldo;
                    _totalDias += element.saldo;
                    auxVigencias.push(vigencia);
                });
                
                setTotalDias(_totalDias);                
                setVigencias([...auxVigencias]);
                setMinimoDiasFerias(x.ferias_dias_minimo);
                setMaximoDiasFerias(x.ferias_dias_maximo);
                setCalendario([...x.calendario]);
                
                //Abaixo, usa-se a posição 21 para garantir que é o mês atual, pois é um dia que fica no meio do calendário. Do banco, estão vindo 42 dias para cada mês.
                setMes_atual(x.calendario[0][21].mes);
                setAno_atual(x.calendario[0][21].ano);
                setNomeFuncionario(x.nome);

            }else{
               
                Swal.fire({
                    icon: 'error',
                    title: "Erro",
                    text: `${x.motivo}`,
                    confirmButtonText: 'Fechar',
                })
            }

            setLoading(false);
           
        })
    }

    useEffect(() => {

        loadCalendar()

    }, []);


    useEffect(()=>{
        if(totalVigencias<minimoDiasFerias){
            setDataInicialFeriasSelecionada(null);
            setDataFinalFeriasSelecionada(null);
        }
    },[totalVigencias,minimoDiasFerias]);


    useEffect(()=>{
        if(dataInicialFeriasSelecionada===null||dataFinalFeriasSelecionada===null){
            setObrigatorio(true);
        }else setObrigatorio(false);
    },[dataInicialFeriasSelecionada,dataFinalFeriasSelecionada])

    useEffect(()=>{
        if(selectedVigencias.length){
            let aux = selectedVigencias;
            aux.sort((a,b)=> Number(a.ano_value)-Number(b.ano_value));
            setSelectedVigencias([...aux]);
        }
    },[selectedVigencias]);

    function registrarSolicitacaoFerias(){
        setObrigatorio(true);
        setDisplay(false);
        setLoading(true);

        let ferias_solicitadas = [];
        selectedVigencias.forEach((element)=>{
            let ferias = {};
            ferias.id_ferias=element.key;
            ferias_solicitadas.push(ferias);            
        })

        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
                    "aplicacao":"SGI",
                    "id_usuario": getUserId(),
                    "id_funcionario":usuario_terceiro?usuario_terceiro.id_funcionario:getUserId(), 
                    "data_inicio":dataInicialFeriasSelecionada,
                    "data_fim":dataFinalFeriasSelecionada,
                    "id_funcionario_substituto":defensorSubstituto&&replacementDefensorRadioButton==='yes'?defensorSubstituto.id_funcionario:null,
                    ferias_solicitadas};     

        api.post('/ferias/solicitar_ferias_registrar/', data, config)
        .then((res) => {
            let x = JSON.parse(res.data);
            if (x.sucesso === "S") {
                
                setIdFeriasSolicitacaoRegistrada(x.id_ferias_solicitacao); // para ser inserida no comprovante de solicitação de férias.

                Swal.fire({
                    icon: 'success',
                    title: "Sucesso!",
                    text: `Solicitação de férias registrada com sucesso!`,
                    cancelButtonText: 'fechar',
                    confirmButtonText: 'Acompanhar Solicitações',
                    showCancelButton: true
                }).then((result)=>{
                    if(result.isConfirmed){
                        navigate('/publico/solicitacoes', {state:{index:1}}) // vai para tela de solicitações gerais pública para iniciar na tela de solicitações de férias
                    }
                })

                loadCalendar();
                setReloadComponent(!reloadComponent);
                setTotalSelecao(minimoDiasFerias);
                setTotalVigencia(-1);
                setSelectedVigencias([]);
                
               
            } else {
               
                Swal.fire({
                    icon: 'error',
                    title: "Erro",
                    text: `${x.motivo}`,
                    confirmButtonText: 'Fechar',
                })
            }

            setLoading(false);
            setObrigatorio(false);
        })
       
    }

    const renderFooter = (name) => {
        return (
            <div>
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times"
                    style={{float:"left"}} 
                    onClick={() =>{setDisplay(false)}} 
                    className="btn-2" />
        
                <Button  
                    label="Confirmar" 
                    icon="pi pi-check"  
                    style={{float:"right"}} 
                    className="btn-1"  
                    onClick={registrarSolicitacaoFerias}
                    disabled={obrigatorio||
                        (ehDefensor&&replacementDefensorRadioButton===null)||
                        (replacementDefensorRadioButton==="yes"&&!defensorSubstituto)? true: false}                  
                />   
            </div>    
        );
    }

    function limpar(){
        setReloadComponent(!reloadComponent);
        setTotalSelecao(0);
        setTotalVigencia(-1);
        selectedVigencias.map((vigencia)=>{
            document.getElementById("parent-"+vigencia.key).classList.remove("saldo-selecionado");
        })
        setSelectedVigencias([]);

    }

    return ( 
        <div className='view'>
            <div className="view-body">
            {
                loading?
                <div className="loading-pagina" ><ProgressSpinner/>Carregando...</div>
                :
                <div style={{height:"100%"}}>
                    <div className="header" style={{marginTop:"5px"}}>
                        <BreadCrumb model={items} home={home}/>
                        <h6 className="titulo-header">Solicitar Férias</h6>
                    </div>
                    <div style={{height:"100%"}}>
                        {/*view aqui*/}
                        <div className="top-box">
                            {
                            usuario_terceiro?
                                <h3>Solicitação de Férias para {usuario_terceiro.nome}</h3>
                             :
                                <h1>Solicitação de Férias</h1>
                             }
                            {vigencias.length!==0?
                            <div style={{display:'flex-inline'}}>
                                <h2>Saldo Total de Férias <Tag severity="success" value={totalDias + " dias"} className="tag-dias"></Tag></h2>
                            </div>:<></>
                            }
                        </div>

                        <div className="area-solicitar-ferias">
                            <div className="caixa-ferias-esq">
                                {
                                 vigencias.length!==0?
                                    <div className="caixaVigencias">
                                        <div>
                                            <h5>Saldos disponíveis</h5>
                                            <CaixaVigencias minValueCalendar={minimoDiasFerias} maxValueCalendar={maximoDiasFerias} minValue={minValue} vigencias={vigencias} setMinValue={setMinValue} setTotalVigencia={setTotalVigencia} setSelectedVigencias={setSelectedVigencias} setTotalSelecao={setTotalSelecao} reloadComponent={reloadComponent}/>
                                        </div>
                                        <Button 
                                            label="Limpar" 
                                            icon="pi pi-refresh"
                                            className="btn-2"
                                            disabled={selectedVigencias.length!==0?false:true}
                                            onClick={()=>{limpar()}}
                                            />
                                    </div>
                                    :
                                        loading?<div></div>
                                        :
                                        <div style={{display:"flex", height: "100%",flexDirection: "column", justifyContent: "space-between"}}>
                                            <label>Você não tem saldo de férias a solicitar!</label>
                                            <Button 
                                                label="Acompanhar Solicitações" 
                                                className="btn-1"
                                                visible={loading?false:true}
                                                onClick={
                                                    ()=>{ 
                                                        navigate('/publico/solicitacoes', {state:{index:1}}) // vai para tela de solicitações gerais pública para iniciar na tela de solicitações de férias
                                                    } 
                                                }/>
                                        </div>
                                }
                                    
                            </div>
                            <div className="caixa-folgas-dir">
                                <div className = "seletor-ranger"> 
                                    {
                                        totalVigencias>=minimoDiasFerias?
                                            <div className="seletor-ranger-slider">
                                                <h5>Total de dias</h5>
                                                <label className="label-descricao">Ajuste o total de dias de férias abaixo.</label>
                                                <br />
                                                <RangerDias defensor={ehDefensor} minValueCalendar={minimoDiasFerias} maxValueCalendar={maximoDiasFerias} minValue={minValue} setTotalSelecao={setTotalSelecao} totalVigencias={totalVigencias} totalSelecao={totalSelecao} reloadComponent={reloadComponent}/>
                                            </div>
                                        :
                                        <div className="seletor-ranger-slider" style={{userSelect: 'none', pointerEvents:'none', opacity:'0.4'}}>
                                            <h5>Total de dias</h5>
                                            <label className="label-descricao">Ajuste o total de dias de férias abaixo.</label>
                                            <br />
                                            <RangerDias defensor={ehDefensor} minValueCalendar={minimoDiasFerias} maxValueCalendar={30} minValue={10} setTotalSelecao={10} totalVigencias={30} totalSelecao={10} />
                                        </div>
                                    } 
                                </div>

                                {
                                    totalVigencias>=minimoDiasFerias?
                                        <div className="calendario-ferias">
                                            <h5>Período</h5>
                                            <label className="label-descricao">Selecione a data inicial e visualize a data final do período.</label>
                                            <Calendario 
                                                mes_atual={mes_atual}
                                                ano_atual={ano_atual}
                                                calendario={calendario} 
                                                totalSelecao={totalSelecao} 
                                                totalVigencias={totalVigencias} 
                                                setDataInicialFeriasSelecionada={setDataInicialFeriasSelecionada} 
                                                setDataFinalFeriasSelecionada={setDataFinalFeriasSelecionada} 
                                                setCalendario={setCalendario}
                                                reloadComponent={reloadComponent}/>     
                                        </div>
                                    :
                                        <div className="calendario-ferias" style={{userSelect: 'none', pointerEvents:'none', opacity:'0.4'}}>
                                            <h5>Período</h5>
                                            <label className="label-descricao">Selecione a data inicial e visualize a data final do período.</label>
                                            <Calendario 
                                                mes_atual={mes_atual}
                                                ano_atual={ano_atual}
                                                calendario={calendario} 
                                                totalSelecao={totalSelecao} 
                                                totalVigencias={totalVigencias} 
                                                setDataInicialFeriasSelecionada={setDataInicialFeriasSelecionada} 
                                                setDataFinalFeriasSelecionada={setDataFinalFeriasSelecionada} 
                                                setCalendario={setCalendario}
                                                reloadComponent={reloadComponent}/>     
                                        </div>
                                }
                            </div>
                            <div></div>

                            <div className="botao-solicitar-ferias">
                                <Button 
                                    label="Solicitar Férias" 
                                    className="btn-1"
                                    icon="pi pi-check"
                                    disabled={obrigatorio?true:false&&(totalVigencias<=0||(vigencias.length===1&&vigencias[0].valor<minimoDiasFerias)?false:true)}
                                    onClick={()=>setDisplay(true)}
                                />
                            </div>

                            <Dialog
                                header="Confirmar Solicitação de Férias?"
                                visible={display}
                                style={{ width: '50%' }} 
                                footer={renderFooter('displayBasic')}
                                onHide={() =>{setDisplay(false);}}>
                                <div className="font-galano">
                                    <p>Período: {String(dataInicialFeriasSelecionada)} à {String(dataFinalFeriasSelecionada)} </p>
                                    <p>Total de dias solicitados: {totalSelecao} dias </p>
                                    <div>Ecxercícios utilizados para saldo: 
                                        <div>
                                            <ul>
                                                {
                                                    selectedVigencias.map((vigencia) => {
                                                        return (<li key={vigencia.key} style={{marginLeft:"10px"}} htmlFor={vigencia.key}> {vigencia.ano} - {vigencia.saldo}</li>)          
                                                    })
                                                }
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                {/*Abaixo, temos o trecho que solicitamos para o Defensor ou para o usuário que está realizando uma solicitação  para o Defensor se quer indicar um Defensor Substituto */}
                                {
                                    ehDefensor||usuario_terceiro?
                                    <div className="font-galano">
                                  
                                   <span>Deseja indicar um membro para a substituição durante o período de férias?</span>
                                    <div className="field-radiobutton">
                                        <RadioButton inputId="option1" name="radio1" value="not" onChange={(e) => setReplacementDefensorRadioButton(e.value)} checked={replacementDefensorRadioButton === 'not'} />
                                        <label htmlFor="option1" style={{marginLeft:"3px"}}>Não</label>
                                    </div>
                                    <div className="field-radiobutton">
                                        <RadioButton inputId="option2" name="radio2" value="yes" onChange={(e) => setReplacementDefensorRadioButton(e.value)} checked={replacementDefensorRadioButton === 'yes'} />
                                        <label htmlFor="option2" style={{marginLeft:"3px"}}>Sim</label>
                                    </div>

                                    <span className='field'>
                                        <label>Defensor Substituto *</label>
                                        <SeletorDefensor
                                            get={defensorSubstituto}
                                            set={setDefensorSubstituto}
                                            disabled={replacementDefensorRadioButton==='yes'?false:true}
                                            
                                        />
                                    </span>
                                                
                                </div>
                                :
                                <span></span>    
                                }
                                
                        
                            </Dialog>

                            {/* O dialog, abaixo, é para mostrar o progressBar durante a requisição  */}
                            <Dialog 
                                visible={loading}
                                closable={false}
                                style={{width:'20%'}}>
                                    <div style={{ width: '100%',display:'flex',flexDirection:'column', alignItems:'center',justifyContent:'center' }}>
                                        <ProgressSpinner></ProgressSpinner>Solicitando férias...
                                    </div>
                            </Dialog>                 

                        </div>
                    </div>
                </div>
            }
            </div>                     
      </div> 

    );
}

export default FeriasSolicitacao;
