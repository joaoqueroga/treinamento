import React, {useState, useEffect} from "react";
import { ProgressSpinner } from 'primereact/progressspinner';
import { BreadCrumb } from 'primereact/breadcrumb';
import '../../style.scss'

function FeriasAprovacao() {
    const [loading, setLoading] = useState(true);
    const items = [
        { label: 'Recursos Humanos' },
        { label: 'Férias' },
        { label: 'Aprovação de Férias' }
    ];
    const home = { icon: 'pi pi-home', url: '/inicio' }

    useEffect(() => {
        setLoading(false);
    }, []);

    return ( 
        <div className='view'>
        <div className="view-body">
        {
        loading?
        <div className="loading-pagina" ><ProgressSpinner/>Carregando...</div>
        :
        <div>
            <div className="header">
                <BreadCrumb model={items} home={home}/>
                <h6 className="titulo-header">Aprovação de Férias</h6>

            </div>
            <div className="top-box">
                <h1>Aprovação de Férias</h1>
            </div>
        </div>
        }
        </div>
        </div>
    );
}

export default FeriasAprovacao;