import React, { useState, useEffect, useRef } from "react";
import { ProgressSpinner } from 'primereact/progressspinner';
import { BreadCrumb } from 'primereact/breadcrumb';
import api from "../../../../../config/api";
import { getToken, getUserId } from "../../../../../config/auth";
import { useParams } from 'react-router-dom';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import { Tooltip } from 'primereact/tooltip';
import { Badge } from 'primereact/badge';
import { FilterMatchMode } from 'primereact/api';
import { Dropdown } from 'primereact/dropdown';
import jsPDF from 'jspdf';
import logo from '../../../../../images/logo_h.png'
import autoTable from 'jspdf-autotable';

function PesquisaEscalaFeriasDetalhe() {
    const { id } = useParams()
    const [loading, setLoading] = useState(true);
    const [list, setList] = useState([]);
    const [erro, setErro] = useState(false);
    const dt = useRef(null);
    const home = { icon: 'pi pi-home', url: '/inicio' }
    const [descricao, setDescricao] = useState('');

    const user = sessionStorage.getItem("nome");

    useEffect(() => {
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(), 
            "id_pesquisa_escala_ferias": id 
        }
        let config = { headers: { "Authorization": `Bearer ${getToken()}` } }
        api.post('/ferias/listar_pesquisa_escala_ferias_detalhe/', data, config)
            .then((res) => {
                let x = JSON.parse(res.data);
                if (Array.isArray(x)) {

                    setList(x[0].funcionarios);
                    setLoading(false);
                    setDescricao(x[0].descricao);
                } else {
                    setErro(true);
                }
            })
    }, []);

    const cols = [
        { field: 'nome', header: 'Servidor' },
        { field: 'mes_recebimento_descricao', header: 'Mês de Recebimento' },
        { field: 'dias_venda', header: 'Dias Vendidos' },
        { field: 'descricao_lotacao', header: 'Unidade' },
        { field: 'data_solicitacao', header: 'Data Solicitada' }

    ];

    const items = [
        { label: 'Recursos Humanos' },
        { label: 'Férias' },
        { label: 'Gestão de Escala de Férias', url: '/ferias/pesquisa_escala_ferias' },
        { label: descricao },
    ];

    const exportColumns = cols.map(col => ({ title: col.header, dataKey: col.field }));


    // const exportPdf = () => {
    //     const doc = new jsPDF()
    //     doc.autoTable({ html: dt.current.getTable() })
    //     window.open(doc.output('bloburl', { filename: `teste.pdf` }), '_blank');
    // }
    function coluna( x, y, tam, rotulo, texto, doc) {
        doc.setFont('helvetica', 'bold');
        doc.text(x, y, `${rotulo}:`);
        doc.setFont('helvetica', 'normal');
        doc.text(x+tam, y, `${texto?texto:""}`);
    }

    function generatePDF (d){

        let data = new Date();
        let dia = `${data.getDate()}/${data.getMonth()+1}/${data.getFullYear()}`;
        let hora = data.toLocaleTimeString();

       
        let doc = new jsPDF('l', 'pt');
        doc.addImage(logo, 'PNG', 40, 20, 155, 30 );
        
        doc.setFont('helvetica', 'bold');
        doc.setFontSize(18);
        doc.setTextColor(20,71,88);
        doc.text(545, 40, `${descricao}`);

        //coluna 1: x=50 , coluna 2 x=400

        let y = 60;
        let espaco = 18;

        doc.setFontSize(12);
        doc.setTextColor(0,0,0);

        if(d.length > 0){

           
            y+=espaco;
            autoTable(doc, ({
                startY: y,
                body: d,
                theme: 'striped',
                styles: {
                    fontSize: 9,
                    cellPadding: 2,
                    halign: 'left',
                    valign: 'middle',
                    textColor: "#000",
                    lineColor:"#000",
                    fillColor:"#fff"
                },
                columns: [
                   
                    { header: 'Servidor', dataKey: 'nome' },
                    { header: 'Mês de Recebimento', dataKey: 'mes_recebimento_descricao' },
                    { header: 'Dias Vendidos', dataKey: 'dias_venda' },
                    { header: 'Unidade', dataKey: 'descricao_lotacao' },
                    { header: 'Data Solicitada', dataKey: 'data_solicitacao' },
                    { header: 'Status', dataKey: 'respondeu' },
                    
                ],
            }))
        }else{
            y+=espaco;
            coluna(40, y,  0, "", "Sem dados", doc);
        }

        //rodapé da pagina
        let pageCount = doc.internal.getNumberOfPages()
        doc.setFontSize(8)
        for (var i = 1; i <= pageCount; i++) {
            doc.setPage(i)
            doc.text('DEFENSORIA PÚBLICA DO ESTADO DO AMAZONAS - AV. ANDRE ARAUJO, 679, ALEIXO  CNPJ: 19.421.427/0001-91', doc.internal.pageSize.width / 2, 570, {
            align: 'center'});
            doc.text(`Emitido por ${user} em ${dia}  ${hora}` + ' - Página ' + String(i) + ' de ' + String(pageCount), doc.internal.pageSize.width / 2, 580, {
            align: 'center'});
        }

        //doc.save(`${s.nome} ${new Date().toLocaleDateString()} ${new Date().toLocaleTimeString()}.pdf`)
        window.open(doc.output('bloburl', { filename: `pesquisa_escala_ferias.pdf` }), '_blank');
    } 
   

    const exportPdf = () => {
        generatePDF(list)
    }

    const exportCSV = () => {
        const filteredData = dt.filteredValue;
        dt.current.exportCSV({ filteredData });
    }

    const header = (
        <div className="flex align-items-center export-buttons">
            <Button type="button" icon="pi pi-print" label="Exportar" onClick={exportPdf} className="btn-2" data-pr-tooltip="PDF" />
            <Button type="button" icon="pi pi-file-excel" label="CSV" onClick={() => exportCSV(true)} className="btn-1" data-pr-tooltip="Selection Only" />
        </div>
    );

    const statusColumnTemplate = (rowData) => {
        if (rowData.respondeu == 'RESPONDIDO') {
            return (
                <Badge value="RESPONDIDO" severity="success"></Badge>
            )
        } else {
            return (
                <Badge value="PENDENTE" severity="danger"></Badge>
            )
        }
    }

    const [filters] = useState({
        'nome': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'respondeu': { value: null, matchMode: FilterMatchMode.EQUALS },
        'mes_recebimento_descricao': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'descricao_lotacao': { value: null, matchMode: FilterMatchMode.CONTAINS },
    });

    const statusRowFilterTemplate = (options) => {
        return <Dropdown value={options.value} options={statuses} onChange={(e) => options.filterApplyCallback(e.value)} itemTemplate={statusItemTemplate} placeholder="Filtrar por status" className="p-column-filter" showClear />;
    }

    const statusItemTemplate = (option) => {
        return <span>{option}</span>;
    }

    const statuses = [
        'RESPONDIDO', 'PENDENTE'
    ];

    return (
        <div className='view'>
            <div className="view-body">
                {
                    loading ?
                        <div className="loading-pagina" ><ProgressSpinner />Carregando...</div>
                        :
                        <div>
                            <div className="header">
                                <BreadCrumb model={items} home={home} />
                                <h6 className="titulo-header">Pesquisa de Escala de Férias</h6>

                            </div>
                            <div>
                                <div className="top-box">
                                    <h1>{descricao}</h1>
                                </div>
                                <Tooltip target=".export-buttons>button" position="bottom" />

                                <DataTable ref={dt} value={list} header={header} dataKey="id" responsiveLayout="scroll"
                                    selectionMode="multiple" selection={list} filters={filters} filterDisplay="row" scrollHeight="65vh" scrollable >
                                    <Column
                                        field="nome"
                                        header="Servidor"
                                        filter
                                        filterPlaceholder="Nome do Servidor"
                                        showFilterMenu={false}
                                    />

                                    <Column
                                        field="mes_recebimento_descricao"
                                        header="Mês de Recebimento"
                                        filter
                                        filterPlaceholder="Mês"
                                        showFilterMenu={false}
                                    />

                                    <Column
                                        field="dias_venda"
                                        header="Dias Vendidos"
                                    />

                                    <Column
                                        field="descricao_lotacao"
                                        header="Unidade"
                                        filter
                                        filterPlaceholder="Unidade"
                                        showFilterMenu={false}
                                    />

                                    <Column
                                        field="data_solicitacao"
                                        header="Data Solicitada"
                                    />

                                    <Column
                                        field="respondeu"
                                        header="Status"
                                        body={statusColumnTemplate}
                                        filter
                                        filterElement={statusRowFilterTemplate}
                                    />

                                </DataTable>
                            </div>
                        </div>
                }
            </div>
        </div>
    );
}

export default PesquisaEscalaFeriasDetalhe;