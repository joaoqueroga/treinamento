import {useState, useEffect } from "react";
import { ProgressSpinner } from 'primereact/progressspinner';
import TabelaCadastroPesquisaEscalaFerias from "../../../componentes/tabela_cadastro_pesquisa_escala_ferias";
import {useNavigate} from 'react-router-dom';
import api from "../../../../../config/api";
import { getToken, getUserId } from "../../../../../config/auth";
import Swal from 'sweetalert2';
import '../../../style.scss'

function PesquisaEscalaFeriasListagem() {

    const navigate = useNavigate();
    const items = [
        { label: 'Recursos Humanos' },
        { label: 'Férias' },
        { label: 'Gestão de Escala de Férias' }
    ];
    const home = { icon: 'pi pi-home', url: '/inicio' }
    const [erro, setErro] = useState(false);
    const [escalas, setEscalas] = useState([])
    const[nomepagina]= useState('Pesquisa de Escala de Férias')
    const [loading, setLoading] = useState(true);
    const[nomebotao]=useState('Nova Pesquisa')
    const[urldados]=useState('/ferias/gestao_escala/')
    const [data_inicial, setDataInicial] = useState('');
    const [data_final, setDataFinal] = useState('');
    const [id, setId] = useState(null);
    const [descricao, setdescricao] = useState('');
    const [modalCadastrar, setModalCadastrar] = useState(false);
    const [modalEditar, setModalEditar] = useState(false);

    function reload() {

        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId()
        }
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        api.post('/ferias/listar_pesquisa_escala_ferias/',data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.length > 0){
                setEscalas(x);
            }
        })
    }


    useEffect(() => {

        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId()
        }
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        api.post('/ferias/listar_pesquisa_escala_ferias/',data, config)     
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(Array.isArray(x)){
                setEscalas(x);
                setLoading(false);
            }else{
               setErro(true);
            }
        })

    }, []);


    function cadastrar_pesquisa(){
      
        let data = {};
        if (id) {   
            data = {
                "aplicacao": "SGI",
                "id_usuario": getUserId(),
                "id_pesquisa_escala_ferias": id,
                "descricao": descricao ,
                "data_inicio": data_inicial,
                'data_fim': data_final 
            }
        } else {
            data = {
                "aplicacao": "SGI",
                "id_usuario": getUserId(),
                "descricao": descricao,
                "data_inicio": data_inicial,
                'data_fim': data_final 
            }
        }
        
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        api.post("ferias/cadastrar_pesquisa_escala_ferias/" ,data,config).then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === "S"){
                setModalCadastrar(false);
                setModalEditar(false);
                Swal.fire({
                    icon: 'success',
                    title:"Sucesso",
                    text: `atualizado`,
                    confirmButtonText: 'fechar',
                })
                reload();
                
           }else{
                Swal.fire({
                    icon:'error',
                    title:"Erro",
                    text: `${x.motivo}`,
                    confirmButtonText: 'fechar',
                })
            }
            

        })

    }

    return ( 
        <div className='view'>
            <div className="view-body">
                {
                    loading?
                    <div className="loading-pagina" ><ProgressSpinner/>Carregando...</div>
                    :
                    <div className="tabela_cadastro">
                        <TabelaCadastroPesquisaEscalaFerias 
                            escalas={escalas} 
                            nomepagina={nomepagina}
                            nomebotao={nomebotao}
                            urldados={urldados}
                            navigate={navigate}
                            items={items}
                            cadastrar={cadastrar_pesquisa}
                            editar={cadastrar_pesquisa}
                            data_final={data_final}
                            data_inicial={data_inicial}
                            descricao={descricao}
                            id={id}
                            setDataFinal={setDataFinal}
                            setDataInicial={setDataInicial}
                            setdescricao={setdescricao}
                            setId={setId}
                            setModalCadastrar={setModalCadastrar}
                            modalCadastrar={modalCadastrar}
                            setModalEditar={setModalEditar}
                            modalEditar={modalEditar}
                            reload={reload}
                        />
                        
                    </div>
                }
            </div>
              
        </div>

    );
}

export default PesquisaEscalaFeriasListagem;