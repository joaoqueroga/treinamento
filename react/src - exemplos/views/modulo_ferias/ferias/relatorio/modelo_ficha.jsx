import React, {useState} from "react";
import './style.scss';
import { Dialog } from 'primereact/dialog';
import { Button } from 'primereact/button';
import jsPDF from 'jspdf';
import logo from '../../../../images/logo_h.png'
import TelaSpinner from "../../../../components/spinner";
import api from "../../../../config/api";
import { getToken, getUserId } from "../../../../config/auth";
import SeletorServidor from "../../../../components/SeletorServidor";

function RelatorioModeloFicha() {
    const [display_pdf, setdisplay_pdf] = useState(false);
    const [servidor, setServidor] = useState(null);
    const [display, setDisplay] = useState(false);
    const [obrigatorio, setobrigatorio] = useState(false);


    const user = sessionStorage.getItem("nome");

    function baixar_pdf(id) {
        setdisplay_pdf(true);
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            "id_funcionario": Number(id),
            "tipo_listagem": "COMPLETA"
        }
        api.post('/rh/servidor/',data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === "S"){
                let f = x.funcionarios[0];
                let a = x.funcionarios[0].anotacoes;
                let d = x.funcionarios[0].documentos_diversos;
                setdisplay_pdf(false);
                gerar_pdf(f,a,d);
            }
        })
    }

    function cabecalho(doc, x_titulo, titulo) {
        doc.addImage(logo, 'PNG', 40, 20, 155, 30 );
        doc.setFont('helvetica', 'bold');
        doc.setFontSize(18);
        doc.setTextColor(20,71,88);
        doc.text(x_titulo, 40, titulo);
    }

    function rodape(doc) {
        let data = new Date();
        let dia_x = data.getDate() < 10 ? `0${data.getDate()}` : data.getDate();
        let mes_x = (data.getMonth()+1) < 10 ? `0${data.getMonth()+1}` : data.getMonth()+1
        let dia = `${dia_x}/${mes_x}/${data.getFullYear()}`;
        let hora = data.toLocaleTimeString();

        let pageCount = doc.internal.getNumberOfPages();
        doc.setFontSize(7)
        doc.setFont('helvetica', 'bold');
        for (var i = 1; i <= pageCount; i++) {
            doc.setPage(i)
            doc.text('DEFENSORIA PÚBLICA DO ESTADO DO AMAZONAS - AV. ANDRE ARAUJO, 679, ALEIXO  CNPJ: 19.421.427/0001-91', doc.internal.pageSize.width / 2, 570, { align: 'center'});
            doc.text(`Emitido por ${user} em ${dia} ${hora} - Página  ${String(i)} de ${String(pageCount)}`, doc.internal.pageSize.width / 2, 580, { align: 'center'});
        }
    }

    function rotulo(doc, texto) {
        doc.autoTable({ startY: doc.autoTable.previous.finalY+5, styles: {fontStyle : 'bold'},  theme: 'striped' , body: [[texto]]})
    }

    function tabela_campos(doc,dados) {
        doc.autoTable({ startY: doc.autoTable.previous.finalY+5 , styles: { cellPadding: 2}, theme: 'plain', body: dados})
    }

    function gerar_pdf (s,a,d){
        let doc = new jsPDF('l', 'pt');
        cabecalho(doc, 635, "FICHA FUNCIONAL")

        //rotulo inicio do relatorio para referencia
        doc.autoTable({ startY: 60, styles: {fontStyle : 'bold'},  body:[['DADOS PESSOAIS']]})
        tabela_campos(doc, [
            [`NOME: ${s.nome}`, `MATRÍCULA: ${s.matricula}`],
            [`NOME SOCIAL: ${s.nome_social}`, `NASCIMENTO: ${s.nascimento}`],
            [`NOME DA MÃE: ${s.nome_mae}`, `NOME DO PAI: ${s.nome_pai}`],
            [`NATURALIDADE: ${s.naturalidade}-${s.uf}`, `SEXO: ${s.matricula}`],
            [`ESTADO CIVIL: ${s.nome}`, `CONJUGUE: ${s.matricula}`],
            [`POSSUÍ UNIÃO ESTÁVEL?: ${s.nome}`, `TIPO SANGUÍNEO: ${s.matricula}`],
        ])

        rotulo(doc, 'DADOS PESSOAIS');
        tabela_campos(doc, [
            ['NOME: joão', 'IDADE: 12'],
            ['DOCUMENTO: Francisco pereira da silva', `IDADE: ${32}`],
        ])

        rodape(doc);
        window.open(doc.output('bloburl', { filename: `ficha_funcional.pdf` }), '_blank');
        setdisplay_pdf(false);
    } 


    function gerar(){
        if(servidor !== null){
            setDisplay(false);
            setobrigatorio(false);
            baixar_pdf(servidor.id_funcionario);
            setServidor(null);
        }else{
            setobrigatorio(true);
        }
    }


    const modalFooter = () => {
        return (
            <div>
                {obrigatorio?<p><small className="p-error">Informe os capos obrigatórios (*).</small></p>:null}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setDisplay(false);
                        setobrigatorio(false);
                    }} 
                    className="btn-2" />
                <Button 
                    label="Gerar Relatório" 
                    icon="pi pi-check" 
                    onClick={()=>{
                        gerar();
                    }} 
                    className="btn-1"
                />
            </div>
        );
    }

    return (
        <div>
            <Button 
                label="Ficha Funcional Refatorada" 
                icon="pi pi-file-pdf" 
                className="btn-2"
                onClick={()=>setDisplay(true)}
            />

            <Dialog 
                header="Ficha funcional"
                visible={display}
                style={{ width: '50%' }}
                footer={modalFooter}
                onHide={
                    () =>{
                        setDisplay(false);
                        setobrigatorio(false);
                    }
                }
            >
                <span>
                    <label>Servidor *</label>
                    <SeletorServidor
                        get={servidor}
                        set={setServidor}
                    />
                </span>
            </Dialog>

            <Dialog
                visible={display_pdf} 
                style={{ width: '50vw' }}
                closable={false}
            >
                <TelaSpinner tamanho={100} texto={"Gerando Documento"}/>
            </Dialog>
        </div>
    );
}

export default RelatorioModeloFicha;