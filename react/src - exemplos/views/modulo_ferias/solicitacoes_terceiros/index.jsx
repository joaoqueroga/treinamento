import React, { useState, useEffect } from 'react';
import {useNavigate} from 'react-router-dom';
import { ProgressSpinner } from 'primereact/progressspinner';
import { DataTable } from 'primereact/datatable';
import { FilterMatchMode } from 'primereact/api';
import { BiCloudUpload } from "react-icons/bi";
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import api from '../../../config/api';
import { getToken, getUserId } from '../../../config/auth';
import { BreadCrumb } from 'primereact/breadcrumb';
import { Dialog } from 'primereact/dialog';
import "./style.scss";
import Alertas from '../../../utils/alertas';

function SolicitarParaTerceiros() {

    const items = [
        { label: 'Recursos Humanos' },
        { label: 'Férias' },
        { label: 'Solicitações para outros' }
    ];
    const home = { icon: 'pi pi-home', url: '/inicio' }
    const navigate = useNavigate();

    const [dados, setDados] = useState(null);
    const [loading, setLoading] = useState(false);


    const [selecionado, setSelecionado] = useState(null);
    const [file, setFile] = useState(null);
    const [modal, setModal] = useState(false);
    const [obrigatorio, setObrigatorio] = useState(false);
    const [tipo, setTipo] = useState(0);  //0 ferias , 1 folgas


    const [filters] = useState({
        'matricula': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'nome': { value: null, matchMode: FilterMatchMode.CONTAINS }
    });

    useEffect(() => {
        setLoading(true);

        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            "tipo_listagem": "PARCIAL",
            "eh_estagiario": false,
            "ativo": true
        }
        api.post('/rh/servidores/', data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            setDados(x.funcionarios);
            setLoading(false);
        })
    }, []);

    function lipar_dados() {
        setFile(null);
        setSelecionado(null);
        setObrigatorio(false);
    }

    const irBodyTemplate = (rowData) => {
        return(
            <span className='botoes-acao'>
            <Button 
                onClick={()=>{ setTipo(0) ; setModal(true) ; setSelecionado(rowData) }}
                icon="pi pi-sun"
                className="btn-darkblue"
                title="Solicitar Férias"
            />
            <Button 
                onClick={()=>{ setTipo(1) ; setModal(true) ; setSelecionado(rowData) }}
                icon="pi pi-calendar-plus"
                className="btn-darkblue"
                title="Solicitar Folgas"
            />
            </span>
        )                                    
    }

    function nomeArquivo(f) {
        let nome = f.name;
        let arr = nome.split('.');
        let t = arr[arr.length - 1];
        let d = new Date();
        let tempo = d.toISOString().split('.')[0];
        return `anexo_solicitacao_${tipo}_${tempo}.${t}`;
    }

    function ir_solicitacao(ferias) {
        if(file){

            let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
            let nome = nomeArquivo(file);

            const formData = new FormData();
            formData.append("arquivo", file);
            formData.append("nome", nome);

            api.post('core/upload_pdf', formData, config)
            .then((res)=>{
                if(res.data === 200){
                    if(ferias){
                        navigate('/solicitacao_ferias', {state:{usuario:selecionado, caminho_arquivo: nome}})
                    }else{ // folgas
                        navigate('/folgas/solicitacao', {state:{usuario:selecionado, caminho_arquivo: nome}})
                    }
                }else{
                    Alertas.erro("Erro ao salvar o arquivo");
                }
            }).catch((err)=>{
                Alertas.erro(err);
            })

        }else{
            setObrigatorio(true);
        }
    }

    const renderFooter = () => {
        return (
            <div>
                {obrigatorio ? <p style={{ color: "#f00" }}>Informe os campos obrigatórios</p> : null}

                {
                    tipo === 0?
                        <Button 
                            label="Solicitar Férias"
                            icon="pi pi-sun"
                            onClick={() => {ir_solicitacao(true)}}
                            className="p-button-success p-button-sm btn-2" 
                        />
                    :
                        <Button 
                            label="Solicitar Folgas" 
                            icon="pi pi-calendar-plus" 
                            onClick={() => {ir_solicitacao(false)}} 
                            className="p-button-success p-button-sm btn-2" 
                        />
                }
                
            </div>
        );
    }


    return ( 
        <div className='view'>
            <div className="view-body">
            {
            loading?
            <div className="loading-pagina" ><ProgressSpinner/>Carregando...</div>
            :
            <div>
                <div className="header">
                    <BreadCrumb model={items} home={home}/>
                </div>
                <h6 className="titulo-header">Solicitação para terceiros</h6>
                <div className="top-box">
                    <h1>Solicitação para terceiros</h1>
                </div>
                <div>
                    <DataTable
                        value={dados}
                        size="small"
                        className="tabela-servidores"
                        emptyMessage="..."
                        scrollable
                        scrollHeight="65vh"
                        filters={filters}
                        filterDisplay="row"
                        dataKey="matricula"
                        selectionMode="single"
                        paginator
                        paginatorTemplate="CurrentPageReport FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink"
                        currentPageReportTemplate="{last} de {totalRecords}" 
                        rows={10}
                    >
                        <Column 
                            field="matricula"
                            header="Matrícula"
                            filter 
                            filterPlaceholder="Buscar" 
                            style={{ flexGrow: 0, flexBasis: '15%' }}
                            showFilterMenu={false}
                        />
                        <Column
                            field="nome" 
                            header="Nome" 
                            filter 
                            filterPlaceholder="Buscar" 
                            style={{ flexGrow: 0, flexBasis: '60%' }}
                            showFilterMenu={false}
                        />
                        <Column 
                            field="botao" 
                            header="Ação" 
                            body={irBodyTemplate} 
                            className="col-centralizado" 
                            style={{ flexGrow: 0, flexBasis: '25%' }}
                            showFilterMenu={false}
                        />
                    </DataTable>

                    <Dialog 
                        header={`Solicitação de ${tipo === 0 ? "Férias" : "Folgas" }`} 
                        visible={modal} 
                        style={{ width: '50vw' }} 
                        footer={renderFooter} 
                        onHide={() => {setModal(false); lipar_dados() }}
                    >
                        <p className='texto-modal-arquivo-solicitacao'>Anexar arquivo para a solicitação de {tipo === 0 ? "férias" : "folgas" } de {selecionado ? selecionado.nome : "" }</p>
                        <br />
                        <div id='importar-arquivo-solicitacao'>
                            <label className='label-file-servidor ' style={{height: '40px'}}>
                            <BiCloudUpload size={20}/>
                            <input
                                type="file" 
                                className='input-files' 
                                accept='.pdf'
                                onChange={e=>setFile(e.target.files[0])}
                            />
                            </label>
                            <span>
                            {file?file.name:"Nenhum arquivo selecionado"}
                            </span>
                        </div>
                    </Dialog>
                </div>
            </div>
            }
            </div>
        </div>
    );
}

export default SolicitarParaTerceiros;