import React, {useState} from "react";
import { Dialog } from 'primereact/dialog';
import { Button } from 'primereact/button';
import jsPDF from 'jspdf';
import logo from '../../../../images/logo_h.png'
import TelaSpinner from "../../../../components/spinner";

function ComprovanteSolicitacao(props) {
    const [display_pdf, setdisplay_pdf] = useState(false);
    const user = sessionStorage.getItem("nome");

    function baixar_pdf() {
        setdisplay_pdf(true);
        //buscar os dados em uma procedure e passar para gerar_pdf();
        gerar_pdf();
    }

    function cabecalho(doc, x_titulo, titulo) {
        doc.addImage(logo, 'PNG', 40, 20, 155, 30 );
        doc.setFont('helvetica', 'bold');
        doc.setFontSize(18);
        doc.setTextColor(20,71,88);
        doc.text(x_titulo, 40, titulo);
    }

    function rodape(doc) {
        let data = new Date();
        let dia_x = data.getDate() < 10 ? `0${data.getDate()}` : data.getDate();
        let mes_x = (data.getMonth()+1) < 10 ? `0${data.getMonth()+1}` : data.getMonth()+1
        let dia = `${dia_x}/${mes_x}/${data.getFullYear()}`;
        let hora = data.toLocaleTimeString();

        let pageCount = doc.internal.getNumberOfPages();
        doc.setFontSize(7)
        doc.setFont('helvetica', 'bold');
        for (var i = 1; i <= pageCount; i++) {
            doc.setPage(i)
            doc.text('DEFENSORIA PÚBLICA DO ESTADO DO AMAZONAS - AV. ANDRE ARAUJO, 679, ALEIXO  CNPJ: 19.421.427/0001-91', doc.internal.pageSize.width / 2, 810, { align: 'center'});
            doc.text(`Emitido por ${user} em ${dia} ${hora} - Página  ${String(i)} de ${String(pageCount)}`, doc.internal.pageSize.width / 2, 820, { align: 'center'});
        }
    }

    function rotulo(doc, texto) {
        doc.autoTable({ startY: doc.autoTable.previous.finalY+5, styles: {fontStyle : 'bold'},  theme: 'striped' , body: [[texto]]})
    }

    function tabela_campos(doc,dados) {
        doc.autoTable({ startY: doc.autoTable.previous.finalY+5 , styles: { cellPadding: 2}, theme: 'plain', body: dados})
    }
    function linha(doc,dados) {
        doc.autoTable({ startY: doc.autoTable.previous.finalY , styles: { cellPadding: 2}, theme: 'plain', body: dados})
    }

    function gerar_pdf  (){
        let doc = new jsPDF('p', 'pt');
        cabecalho(doc, 322, props.dados.ferias_ou_folgas===1?"SOLICITAÇÃO DE FÉRIAS":"SOLICITAÇÃO DE FOLGAS");

        //rotulo inicio do relatorio para referencia
        doc.autoTable({ startY: 60, styles: {fontStyle : 'bold'},  body:[['DADOS DO SOLICITANTE']]})
        tabela_campos(doc, [
            [`NOME: ${props.dados.nome}`, `MATRÍCULA: ${props.dados.matricula}`],
            [`LOTAÇÃO: ${props.dados.ferias_ou_folgas===1?props.dados.lotacao_solicitante:props.dados.descricao_lotacao}`],
        ])

        if(props.dados.ferias_ou_folgas===1){
            rotulo(doc, 'PERÍODO');
            linha(doc,[[`${props.dados.periodo}`]])
        }else if(props.dados.ferias_ou_folgas===2){
            rotulo(doc, 'DIAS SELECIONADOS');
            doc.autoTable({
                startY: doc.autoTable.previous.finalY,
                body: props.dados.datas,
                theme: 'plain',
                styles: { cellPadding: 2},
                columns: [
                    { dataKey: 'data' }
                ],
            })
        }
        

        rotulo(doc, 'HISTÓRICO');
        linha(doc, [[`01 - SOLICITAÇÃO DE ${props.dados.nome}`]])
        props.dados.historico.map((h, index)=>{
            let i = '';
            if(index < 8){i = `0${index+2}`}else{i=`${index+2}`}
            linha(doc, [[`${i} - ${h.status} por ${h.nome} na data ${h.data}`]])
            if(h.observacao)linha(doc, [[`       ${h.observacao}`]])
        })

        rodape(doc);
        window.open(doc.output('bloburl', { filename: `ficha_funcional.pdf` }), '_blank');
        setdisplay_pdf(false);
    } 

    return (
        <span>
            <Button 
                title="Comprovante"
                icon="pi pi-file-pdf"
                className="btn-red"
                onClick={()=>baixar_pdf()}
            />
            <Dialog
                visible={display_pdf} 
                style={{ width: '50vw' }}
                closable={false}
            >
                <TelaSpinner tamanho={100} texto={"Gerando Documento"}/>
            </Dialog>
        </span>
    );
}

export default ComprovanteSolicitacao;