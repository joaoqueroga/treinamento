import React, {useState} from "react";
import { Dialog } from 'primereact/dialog';
import { Button } from 'primereact/button';
import jsPDF from 'jspdf';
import autoTable from 'jspdf-autotable';
import logo from '../../../../images/logo_h.png'
import TelaSpinner from "../../../../components/spinner";

function RelatoriosSolicitacaoFolgas(props) {
    const [display_pdf, setdisplay_pdf] = useState(false);
    const user = sessionStorage.getItem("nome");

    function baixar_pdf() {
        setdisplay_pdf(true);
        //buscar os dados em uma procedure e passar para gerar_pdf();
        gerar_pdf();
    }

    function cabecalho(doc, x_titulo, titulo) {
        doc.addImage(logo, 'PNG', 40, 20, 155, 30 );
        doc.setFont('helvetica', 'bold');
        doc.setFontSize(18);
        doc.setTextColor(20,71,88);
        doc.text(x_titulo, 40, titulo);
    }

    function rodape(doc) {
        let data = new Date();
        let dia_x = data.getDate() < 10 ? `0${data.getDate()}` : data.getDate();
        let mes_x = (data.getMonth()+1) < 10 ? `0${data.getMonth()+1}` : data.getMonth()+1
        let dia = `${dia_x}/${mes_x}/${data.getFullYear()}`;
        let hora = data.toLocaleTimeString();

        let pageCount = doc.internal.getNumberOfPages();
        doc.setFontSize(7)
        doc.setFont('helvetica', 'bold');
        for (var i = 1; i <= pageCount; i++) {
            doc.setPage(i)
            doc.text('DEFENSORIA PÚBLICA DO ESTADO DO AMAZONAS - AV. ANDRE ARAUJO, 679, ALEIXO  CNPJ: 19.421.427/0001-91', doc.internal.pageSize.width / 2, 565, { align: 'center'});
            doc.text(`Emitido por ${user} em ${dia} ${hora} - Página  ${String(i)} de ${String(pageCount)}`, doc.internal.pageSize.width / 2, 575, { align: 'center'});
        }
    }

    function gerar_pdf  (){
        let doc = new jsPDF('l', 'pt');
        cabecalho(doc, 570, "SOLICITAÇÕES DE FOLGAS");
        
        autoTable(doc, ({
            body: props.dados,
            startY:60,
            styles: {fontSize: 8, cellPadding: 2, halign: 'justify' , textColor: "#000", lineColor:"#000", fillColor:"#fff"},
            columnStyles: {
                4: { columnWidth: 200 },
            },
            columns: [
                { header: 'Nome', dataKey: 'nome' },
                { header: 'Lotação', dataKey: 'descricao_lotacao' },
                { header: 'Solicitação', dataKey: 'data_solicitacao' },
                { header: 'Qdt Dias', dataKey: 'dias' },
                { header: 'Dias Solicitados', dataKey: 'datas_lista' },
                { header: 'Status', dataKey: 'status' },
            ],
        }))

        rodape(doc);
        window.open(doc.output('bloburl', { filename: `ficha_funcional.pdf` }), '_blank');
        setdisplay_pdf(false);
    } 

    return (
        <span>
            <Button 
                title="Solicitações de Folgas"
                label="Solicitações de Folgas"
                icon="pi pi-file-pdf"
                className="btn-1"
                onClick={()=>baixar_pdf()}
            />
            <Dialog
                visible={display_pdf} 
                style={{ width: '50vw' }}
                closable={false}
            >
                <TelaSpinner tamanho={100} texto={"Gerando Documento"}/>
            </Dialog>
        </span>
    );
}

export default RelatoriosSolicitacaoFolgas;