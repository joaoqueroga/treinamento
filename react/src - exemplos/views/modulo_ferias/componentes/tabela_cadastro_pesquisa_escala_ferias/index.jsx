import React, { useState, useEffect } from "react";
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { BreadCrumb } from 'primereact/breadcrumb';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import SeletorData from "../../../../components/SeletorData";
import api from "../../../../config/api";
import { getToken, getUserId } from "../../../../config/auth";
import { Tag } from 'primereact/tag';

import Swal from 'sweetalert2';

const TabelaCadastroPesquisaEscalaFerias = (props) => {


    const home = { icon: 'pi pi-home', url: '/inicio' }
    const [obrigatorioeditar, setobrigatorioEditar] = useState(false);
    const [obrigatorio, setobrigatorio] = useState(false);




    const editarEscala = (rowData) => {
        props.setModalEditar(true)
        props.setId(rowData.id_pesquisa_escala_ferias)
        props.setdescricao(rowData.descricao)
        props.setDataInicial(rowData.data_inicio)
        props.setDataFinal(rowData.data_fim)

    }

    const limparDados = () => {
        props.setId(null)
        props.setdescricao('')
        props.setDataInicial('')
        props.setDataFinal('')
    }

    const modalCadastroFooter = () => {
        return (
            <div>
                {obrigatorio ? <p><small className="p-error">Informe os capos obrigatórios (*).</small></p> : null}
                <Button
                    label="Cancelar"
                    icon="pi pi-times"
                    onClick={() => {
                        props.setModalCadastrar(false);
                    }}
                    className="btn-2" />
                <Button
                    label="Cadastrar"
                    icon="pi pi-check"
                    onClick={() => {
                        props.cadastrar()
                    }}
                    className="btn-1"
                />
            </div>
        );
    }

    const modalEditarFooter = () => {
        return (
            <div>
                {obrigatorioeditar ? <p><small className="p-error">Informe os capos obrigatórios (*).</small></p> : null}
                <Button
                    label="Cancelar"
                    icon="pi pi-times"
                    onClick={() => {
                        props.setModalEditar(false);
                    }}
                    className="btn-2" />
                <Button
                    label="Salvar"
                    icon="pi pi-check"
                    onClick={() => {
                        props.editar();
                    }}
                    className="btn-1"
                />
            </div>

        );
    }


    const irBodyTemplate = (rowData) => {
        return (
            <span>

                {rowData.mostrar_editar &&
                    <Button
                        icon="pi pi-pencil"
                        className="btn-green"
                        onClick={() => { editarEscala(rowData) }}
                    />
                }

                {rowData.mostrar_excluir &&
                    <Button
                        icon="pi pi-trash"
                        className="btn-red"
                        onClick={() => { excluir_pesquisa(rowData) }}
                    />
                }
                {rowData.mostrar_visualizar &&
                    <Button
                        onClick={() => props.navigate(`${props.urldados}${rowData.id_pesquisa_escala_ferias}`)}
                        icon="pi pi-eye"
                        className="btn-darkblue"
                        title="Ir"

                    />
                }

            </span>

        )
    }

    const descricaoColumnTemplate = (rowData) => {
        let desc = rowData.descricao;
        if (rowData.estah_aberta) {
            return (
                <span >
                    {desc} <Tag value="Vigente" className="tag-aprovado"></Tag>
                </span>

            )
        } else {
            return (
                <span >
                    {desc}
                </span>

            )
        }

    }

    const excluir_pesquisa = (rowData) => {
        Swal.fire({
            icon: 'warning',
            text: 'Excluir a Pesquisa de Escala de Férias?',
            showCancelButton: true,
            confirmButtonText: 'Sim',
            cancelButtonText: 'Não',
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed) {
                let config = { headers: { "Authorization": `Bearer ${getToken()}` } }
                let data = {
                    "aplicacao": "SGI",
                    "id_usuario": getUserId(),
                    "id_pesquisa_escala_ferias": rowData.id_pesquisa_escala_ferias
                }
                api.post('ferias/excluir_pesquisa_escala_ferias/', data, config)
                    .then((res) => {
                        let x = JSON.parse(res.data);
                        if (x.sucesso === 'S') {
                            Swal.fire({
                                icon: 'success',
                                title: "Sucesso",
                                text: `Pesquisa excluída`,
                                confirmButtonText: 'fechar',
                            })
                            props.reload();
                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: "Erro",
                                text: `${x.motivo}`,
                                confirmButtonText: 'fechar',
                            })
                        }
                    }).catch((err) => {
                        Swal.fire({
                            icon: 'error',
                            title: "Erro",
                            text: `${err}`,
                            confirmButtonText: 'fechar',
                        })
                    })
            }
        })
    }

    const diasInicialTemplate = (rowData) => {
        return <span><Tag value={rowData.data_inicio} className="tag-lista-dias"></Tag></span>;
    }

    const diasFinalTemplate = (rowData) => {
        return <span><Tag value={rowData.data_fim} className="tag-lista-dias"></Tag></span>;
    }

    return (

        <div>
            <div className="header">
                <BreadCrumb model={props.items} home={home} />
                <h6 className="titulo-header">{props.nomepagina}</h6>
                <Button
                    label={props.nomebotao}
                    icon="pi pi-plus"
                    className="btn-1"
                    onClick={() => { props.setModalCadastrar(true); limparDados() }}
                />
            </div>
            <Dialog
                header="Nova Pesquisa de Pecunia"
                visible={props.modalCadastrar}
                style={{ width: '50%' }}
                footer={modalCadastroFooter}
                onHide={
                    () => {
                        props.setModalCadastrar(false);
                        setobrigatorio(false);
                    }
                }
            >
                <div className="field grupo-input-label">
                    <label>Título *</label>
                    <InputText
                        value={props.titulo}
                        onChange={(e) => props.setdescricao(e.target.value)}
                        className="texto-maiusculo"
                    />
                </div>

                <br />


                <hr />
                <span className="field grupo-input-label">
                    Período para definição da escala
                </span>

                <br />

                <span className="field grupo-input-label">
                    Data Inicial
                    <div className="seletor-de-data">
                        <SeletorData
                            get={props.data_inicial}
                            set={props.setDataInicial}
                        />
                    </div>
                </span>
                <span className="field grupo-input-label">
                    Data Final
                    <div className="seletor-de-data">
                        <SeletorData
                            get={props.data_final}
                            set={props.setDataFinal}
                        />
                    </div>
                </span>

            </Dialog>

            <div className="top-box">
                <h1>{props.nomepagina}</h1>
            </div>

            <div >
                <DataTable value={props.escalas} scrollHeight="70vh" scrollable>

                    <Column
                        field="botao"
                        header="Descrição"
                        body={descricaoColumnTemplate}
                    />

                    <Column
                        body={diasInicialTemplate}
                        field="data_inicio"
                        header="Data Inicial"
                        style={{ flexGrow: 0, flexBasis: '20%' }}
                    />

                    <Column
                        body={diasFinalTemplate}
                        field="data_fim"
                        header="Data Final"
                        style={{ flexGrow: 0, flexBasis: '20%' }}
                    />

                    <Column
                        field="botao"
                        header="Ação"
                        body={irBodyTemplate}
                        className="col-centralizado"
                        style={{ flexGrow: 0, flexBasis: '20%' }}
                    />

                </DataTable>

            </div>
            <Dialog
                header="Editar Escala de Férias"
                visible={props.modalEditar}
                style={{ width: '50%' }}
                footer={modalEditarFooter}
                onHide={
                    () => {
                        props.setModalEditar(false);
                        setobrigatorioEditar(false);
                    }
                }
            >
                <div className="field grupo-input-label">
                    <label>Título *</label>
                    <InputText
                        value={props.descricao}
                        onChange={(e) => props.setdescricao(e.target.value)}
                        className="texto-maiusculo"
                    />
                </div>

                <br />


                <hr />
                <span className="field grupo-input-label">
                    Período para definição da escala
                </span>

                <br />

                <span className="field grupo-input-label">
                    Data Inicial
                    <div className="seletor-de-data">
                        <SeletorData
                            get={props.data_inicial}
                            set={props.setDataInicial}
                        />
                    </div>
                </span>
                <span className="field grupo-input-label">
                    Data Final
                    <div className="seletor-de-data">
                        <SeletorData
                            get={props.data_final}
                            set={props.setDataFinal}
                        />
                    </div>
                </span>
            </Dialog>

        </div>
    )


}
export default TabelaCadastroPesquisaEscalaFerias;