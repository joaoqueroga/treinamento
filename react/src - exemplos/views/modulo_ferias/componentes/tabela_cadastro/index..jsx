import React, {useState, useEffect} from "react";
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Dropdown } from 'primereact/dropdown';
import { BreadCrumb } from 'primereact/breadcrumb';
import { Button } from 'primereact/button';
import { Tag } from 'primereact/tag';
import './style.scss'
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import SeletorData from "../../../../components/SeletorData";
import { getToken, getUserId } from "../../../../config/auth";
import api from "../../../../config/api";
import { FilterMatchMode } from 'primereact/api';

import Swal from 'sweetalert2';

const TabelaCadastro = (props) => {

    
    const home = { icon: 'pi pi-home', url: '/inicio' }
    const [obrigatorioeditar, setobrigatorioEditar] = useState(false);
    const [obrigatorio, setobrigatorio] = useState(false);

    const [filters] = useState({
        'descricao': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'data_inicio': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'data_fim': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'status': { value: null, matchMode: FilterMatchMode.EQUALS }
    });


    const editarEscala = (rowData) => {
        props.setModalEditar(true)
        props.setId(rowData.id_pesquisa_pecunia)
        props.setdescricao(rowData.descricao)
        props.setDataInicial(rowData.data_inicio)
        props.setDataFinal(rowData.data_fim)
        
    }

    const limparDados = () => {
        props.setId(null)
        props.setdescricao('')
        props.setDataInicial('')
        props.setDataFinal('')
    }

    const modalCadastroFooter = () => {
        return (
            <div>
                {obrigatorio?<p><small className="p-error">Informe os capos obrigatórios (*).</small></p>:null}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        props.setModalCadastrar(false);
                        //setobrigatorio(false);
                    }} 
                    className="btn-2" />
                <Button 
                    label="Cadastrar" 
                    icon="pi pi-check" 
                    onClick={()=>{
                        props.cadastrar()
                    }} 
                    className="btn-1"
                />
            </div>
        );
    }

    const modalEditarFooter = () => {
        return (
            <div>
                {obrigatorioeditar?<p><small className="p-error">Informe os capos obrigatórios (*).</small></p>:null}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        props.setModalEditar(false);
                        //setobrigatorioEditar(false);
                    }} 
                    className="btn-2" />
                <Button 
                    label="Salvar" 
                    icon="pi pi-check" 
                    onClick={()=>{
                        props.editar();
                    }} 
                    className="btn-1"
                />
            </div>
            
        );
    }

   


    const irBodyTemplate = (rowData) => {
        return(

            <span>
                  
                    { rowData.mostrar_editar && 
                        <Button 
                            icon="pi pi-pencil" 
                            className="btn-green" 
                            onClick={()=>{editarEscala(rowData)}}
                        />
                    }

                    { (rowData.mostrar_excluir) && 
                        <Button 
                            icon="pi pi-trash"
                            className="btn-red"
                            onClick={() => {excluir_pesquisa(rowData)}}
                        />
                    }
                   
                    { // rowData.mostrar_visualizar && 
                        
                        <Button 
                            onClick={()=>props.navigate(`${props.urldados}${rowData.id_pesquisa_pecunia}`)}
                            icon="pi pi-eye"
                            className="btn-darkblue"
                            title="Ir"
                            
                        />
                    }

            </span>

        )
    }
    const statusRowFilterTemplate = (options) => {
        return <Dropdown value={options.value} options={statuses} onChange={(e) => options.filterApplyCallback(e.value)} itemTemplate={statusItemTemplate} placeholder="Filtrar por status" className="p-column-filter" showClear />;
    }
    const statusItemTemplate = (option) => {
        return <span>{option}</span>;
    }
    const statuses = [
        'A INICIAR', 'EM ANDAMENTO', 'EM ANÁLISE', 'FINALIZADA'
    ];

    const statusBodyTemplate = (rowData) => {
        switch (rowData.status) {
            case 'EM ANDAMENTO':
                return <Tag value={rowData.status} className="tag-aprovado"></Tag>;

            case 'A INICIAR':
                return <Tag value={rowData.status} className="tag-tramitando"></Tag>;

            case 'EM ANÁLISE':
                return <Tag value={rowData.status} className="tag-alteracao"></Tag>;

            case 'FINALIZADA':
                return <Tag value={rowData.status} className="tag-cancelado"></Tag>;
        }        
    }
    

    const excluir_pesquisa = (rowData) => {
        Swal.fire({
            icon:'warning',
            text: 'Excluir a Pesquisa de Pecúnia?',
            showCancelButton: true,
            confirmButtonText: 'Sim',
            cancelButtonText: 'Não',
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed) {
                let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
                let data = {
                    "aplicacao": "SGI",
                    "id_usuario": getUserId(),
                    "id_pesquisa_pecunia": rowData.id_pesquisa_pecunia,
                    "excluir": true,
                    'id_funcionario': getUserId()
                }
                api.post('pecunia/cadastro_pesquisapecunia/', data, config)
                .then((res)=>{
                    let x = JSON.parse(res.data);
                    if(x.sucesso === 'S'){
                        Swal.fire({
                            icon: 'success',
                            title:"Sucesso",
                            text: `Pesquisa excluída`,
                            confirmButtonText: 'fechar',
                        })
                        //setPermissao(null);
                        props.reload();
                    }else{
                        //setmodalcadastro(false);
                        Swal.fire({
                            icon:'error',
                            title:"Erro",
                            text: `${x.motivo}`,
                            confirmButtonText: 'fechar',
                        })
                    }
                }).catch((err)=>{
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `${err}`,
                        confirmButtonText: 'fechar',
                    })
                })
            }
        })
    }

    const diasInicialTemplate = (rowData) => {
        return <span><Tag value={rowData.data_inicio} className="tag-lista-dias"></Tag></span>;
    }

    const diasFinalTemplate = (rowData) => {
        return <span><Tag value={rowData.data_fim} className="tag-lista-dias"></Tag></span>;
    }
    
    return(

        <div>
            <div className="header">
                <BreadCrumb model={props.items} home={home}/>
                <h6 className="titulo-header">{props.nomepagina}</h6>
                <Button 
                    label={props.nomebotao}
                    icon="pi pi-plus" 
                    className="btn-1"
                    onClick={()=>{props.setModalCadastrar(true); limparDados()}}
                />
            </div>
            <Dialog 
                header="Nova Pesquisa de Pecunia"
                visible={props.modalCadastrar}
                style={{ width: '50%' }}
                footer={modalCadastroFooter}
                onHide={
                    () =>{
                        props.setModalCadastrar(false);
                        setobrigatorio(false);
                    }
                }
                >
                <div className="field grupo-input-label">
                    <label>Título *</label>
                    <InputText 
                        value={props.titulo}
                        onChange={(e)=>props.setdescricao(e.target.value)}
                        className="texto-maiusculo"
                    />
                </div>

                <br />

                
                <hr />
                <span className="field grupo-input-label">
                    Período para definição da escala
                </span>

                <br />

                <span className="field grupo-input-label">
                    Data Inicial
                    <div className="seletor-de-data">
                    <SeletorData
                        get={props.data_inicial}
                        set={props.setDataInicial}
                    />
                    </div>
                </span>
                <span className="field grupo-input-label">
                    Data Final
                    <div className="seletor-de-data">
                    <SeletorData
                        get={props.data_final}
                        set={props.setDataFinal}
                    />
                    </div>
                </span> 
                            
            </Dialog>

            <div className="top-box">
                <h1>{props.nomepagina}</h1>
            </div>
            <div >
                <DataTable 
                    value={props.escalas}
                    responsiveLayout="scroll"
                    scrollable 
                    filters={filters}
                    filterDisplay="row" 
                    
                    paginator
                    paginatorTemplate="CurrentPageReport FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink"
                    currentPageReportTemplate="{last} de {totalRecords}" 
                    rows={6}
                    
                    >
                    <Column 
                        filter
                        filterPlaceholder="Buscar"
                        field="descricao" 
                        header="Descrição"
                        showFilterMenu={false}
                    />

                    <Column 
                        filter
                        filterPlaceholder="Buscar"
                        body={diasInicialTemplate}
                        field="data_inicio" 
                        header="Data Inicial"
                        showFilterMenu={false}
                    />

                    <Column 
                        filter
                        filterPlaceholder="Buscar"
                        body={diasFinalTemplate}
                        field="data_fim" 
                        header="Data Final"
                        showFilterMenu={false}
                    />

                    <Column
                        field="status" 
                        header="Status"
                        body={statusBodyTemplate}
                        filter 
                        filterElement={statusRowFilterTemplate} 
                        filterPlaceholder="Buscar"
                        showFilterMenu={false}
                    />
                    <Column 
                        field="botao" 
                        header="Ação" 
                        body={irBodyTemplate} 
                        className="col-centralizado" 
                        style={{ flexGrow: 0, flexBasis: '20%' }}
                    />
                    
                    
                </DataTable>
                    
            </div>
            <Dialog 
                header="Editar Escala de Férias"
                visible={props.modalEditar}
                style={{ width: '50%' }}
                footer={modalEditarFooter}
                onHide={
                    () =>{
                        props.setModalEditar(false);
                        setobrigatorioEditar(false);
                    }
                }
                >
                <div className="field grupo-input-label">
                    <label>Título *</label>
                    <InputText 
                        value={props.descricao}
                        onChange={(e)=>props.setdescricao(e.target.value)}
                        className="texto-maiusculo"
                    />
                </div>

                <br />

                
                <hr />
                <span className="field grupo-input-label">
                    Período para definição da escala
                </span>

                <br />

                <span className="field grupo-input-label">
                    Data Inicial
                    <div className="seletor-de-data">
                    <SeletorData
                        get={props.data_inicial}
                        set={props.setDataInicial}
                    />
                    </div>
                </span>
                <span className="field grupo-input-label">
                    Data Final
                    <div className="seletor-de-data">
                    <SeletorData
                        get={props.data_final}
                        set={props.setDataFinal}
                    />
                    </div>
                </span> 
            </Dialog> 

        </div>
    )


}
export default TabelaCadastro;