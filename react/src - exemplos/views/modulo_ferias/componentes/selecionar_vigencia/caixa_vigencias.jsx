import React,{ useEffect, useState, useRef } from 'react';
import { Checkbox } from 'primereact/checkbox';
import { Dialog } from 'primereact/dialog';

const CaixaVigencias = (props) => {
    const [visible, setVisible] = useState(false);

    // passar do pai para o filho: estado SetTotalVigencia SetTotalSelecao e o valor da vigencia 

    const vigencias = props.vigencias;  // vetor com as vigencias e valores
    const [selectedVigencias, setSelectedVigencias] = useState([]); // seletor de quais vigencias foram selecionados 
    const [emUso, setEmUso] = useState([]);
    const [resumo, setResumo] = useState([]);
    const op = useRef(null);
    
    useEffect(()=>{
        setSelectedVigencias([]);
    },[props.reloadComponent]);

    const onVigenciaChange = (e) => {
        let _selectedVigencias = [...selectedVigencias];

        if (e.checked) {
            _selectedVigencias.push(e.value);
            document.getElementById("parent-"+e.value.key).classList.add("saldo-selecionado");
        }
        else {
            document.getElementById("parent-"+e.value.key).classList.remove("saldo-selecionado");
            for (let i = 0; i < _selectedVigencias.length; i++) {
                const selectedVigencia = _selectedVigencias[i];

                if (selectedVigencia.key === e.value.key) {
                    _selectedVigencias.splice(i, 1);
                    break;
                }
            }
        }

        let count=0
        _selectedVigencias.map((v)=>{
            count+=v.valor // soma de todos os valores das vigencias 
            
        })

        props.setTotalVigencia(count)


        if (count>0){ // se o valor total do count for maior que zero o valor da seleção varia de 1 a count 
            props.setMinValue(1);
            if(props.maxValueCalendar!==undefined){ // condição para parar de contar quando o valor do count for igual ao  valor máximo do calendário de férias.
                if(count<=props.maxValueCalendar) props.setTotalSelecao(count);
            }else props.setTotalSelecao(count);
        }
        else{ // caso contrario a seleção fica com 0
            props.setMinValue(0)
            props.setTotalSelecao(0)
        }

        // Valor mínimo para solicitação de férias
        if(props.minValueCalendar!==undefined)props.setMinValue(props.minValueCalendar);
        
        if(props.setSelectedVigencias!==undefined) props.setSelectedVigencias(_selectedVigencias);

        setSelectedVigencias(_selectedVigencias);       
    }

    return (
        <div>
        {
            vigencias.map((vigencia) => {
                return (
                    <div style={{ marginBottom:'8px' }}>
                        {/* Abaixo, temos a label que indica que o saldo acima já está em uso */}
                        <>
                        {
                            (vigencia.resumo) ?
                            (vigencia.resumo).length>0?
                                <span className='resume-label' htmlFor={vigencia.key} style={{ marginTop:'3px', marginBottom:'0.5px' }}
                                        onClick={() => {setEmUso(vigencia); setResumo(vigencia.resumo); setVisible(true)}}>
                                    <i style={{fontSize: "0.8rem", marginLeft: "7px", marginRight: "3px"}} className="pi pi-exclamation-triangle"></i>
                                    <p style={{margin:"0px"}}>Em uso</p>
                                </span>
                            :<></>
                            :null
                        }
                        </>
                        <div 
                            key={vigencia.key} 
                            className="saldos-disponiveis" 
                            id={'parent-'+vigencia.key} 
                            style={
                                (vigencia.resumo) ?
                                    (vigencia.resumo).length<=0
                                    ?{marginTop:'8px'}
                                    :{}
                                :{}
                            }
                        >
                            <label htmlFor={vigencia.key}> {vigencia.ano}</label>
                            <label htmlFor={vigencia.key}> {vigencia.saldo}</label>
                            <Checkbox inputId={vigencia.key} name="vigencia" value={vigencia} onChange={onVigenciaChange} checked={selectedVigencias.some((item) => item.key === vigencia.key)} />
                        </div>
                    </div>
                 
                )   
            })
        }  

            <Dialog header={emUso.ano} visible={visible} style={{ width: '570px' }} onHide={() => setVisible(false)}>
                <div>
                    <span style={{fontWeight: 'bold'}}>Nº DA SOLICITAÇÃO  DIAS  STATUS</span><br />
                    {
                        resumo.map((resumo)=>{
                            return (
                                <div style={{textAlign: 'center', alignItems:'center'}}>
                                    <span key={resumo.id_ferias_solicitacao} >{resumo.id_ferias_solicitacao} - {resumo.dias<10?'0'+resumo.dias:resumo.dias} Dias - {resumo.status}</span> <br />
                                </div>
                            )
                        })
                    }
                </div>
            </Dialog>



        </div>
    );

}

export default CaixaVigencias;