import React, { useState, useEffect, useRef } from 'react';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import { FileUpload } from 'primereact/fileupload';
import { Tooltip } from 'primereact/tooltip';
import { Toast } from 'primereact/toast';

function TabelaDetalhe() {

    const [products, setProducts] = useState([]);
    const dt = useRef(null);
    const toast = useRef(null);

    const dados = [
        {id:1, servidor:'joaozinho', mes: 'Janeiro', unidade: 'DTI'},
        {id:2, servidor:'Mariteste', mes: 'Agosto', unidade: 'DTI'},
        {id:3, servidor:'Edi', mes: 'Outubro', unidade: 'DTI'},
        {id:4, servidor:'Bea', mes: 'Agosto', unidade: 'DTI'}
        ]

    const cols = [
        { field: 'servidor', header: 'Servidor' },
        { field: 'mes', header: 'Mês' },
        { field: 'unidade', header: 'Unidade' }
    ];

    const exportColumns = cols.map(col => ({ title: col.header, dataKey: col.field }));

    useEffect(() => {
        setProducts(dados)
    }, []); 
   

    const exportPdf = () => {
        import('jspdf').then(jsPDF => {
            import('jspdf-autotable').then(() => {
                const doc = new jsPDF.default(0, 0);
                doc.autoTable(exportColumns, products);
                doc.save('products.pdf');
            })
        })
    }

    const exportCSV = (selectionOnly) => {
        dt.current.exportCSV({ selectionOnly });
    }

    const header = (
        <div className="flex align-items-center export-buttons">
            <Button type="button" icon="pi pi-print" label= "Exportar" onClick={exportPdf} className="btn-2" data-pr-tooltip="PDF" />
            <Button type="button" icon="pi pi-file-excel" label="CSV" onClick={() => exportCSV(true)} className="btn-1" data-pr-tooltip="Selection Only" />
        </div>
    );

    return ( 
       <div>

            <Tooltip target=".export-buttons>button" position="bottom" />

            <DataTable ref={dt} value={dados} header={header} dataKey="id" responsiveLayout="scroll"
                selectionMode="multiple" selection={dados}>
                <Column 
                    field="servidor" 
                    header="Servidor"
                />

                <Column 
                    field="mes" 
                    header="Mês"
                />

                <Column 
                    field="unidade" 
                    header="Unidade"
                />
                
            </DataTable>

       </div>
    );
}

export default TabelaDetalhe;