import React,{ useState, useEffect, useContext } from 'react';

export default function CalendarioEstatico(state){

    const [mesAtual, setMesatual] = useState(0);
    const [mes, setMes] = useState(0); // controla o mes apresentado no caledario 
    const [textoMesAtual, setTextoMesAtual] = useState(''); // mostra o texto no topo do calendario
    
    function apresentacaoMes(mes) {
        let textoMesAtual = '';
        switch (mes) {
            case 1:
                textoMesAtual = 'JANEIRO ' + state.calendario[mes][0].ano;
                break;
            case 2:
                textoMesAtual = 'FEVEREIRO ' + state.calendario[mes][0].ano;
                break;
            case 3:
                textoMesAtual = 'MARÇO ' + state.calendario[mes][0].ano;
                break;
            case 4:
                textoMesAtual = 'ABRIL ' + state.calendario[mes][0].ano;
                break;
            case 5:
                textoMesAtual = 'MAIO ' + state.calendario[mes][0].ano;
                break;
            case 6:
                textoMesAtual = 'JUNHO ' + state.calendario[mes][0].ano;
                break;
            case 7:
                textoMesAtual = 'JULHO ' + state.calendario[mes][0].ano;
                break;
            case 8:
                textoMesAtual = 'AGOSTO ' + state.calendario[mes][0].ano;
                break;
            case 9:
                textoMesAtual = 'SETEMBRO ' + state.calendario[mes][0].ano;
                break;
            case 10:
                textoMesAtual = 'OUTUBRO ' + state.calendario[mes][0].ano;
                break;
            case 11:
                textoMesAtual = 'NOVEMBRO ' + state.calendario[mes][0].ano;
                break;
            case 12:
                textoMesAtual = 'DEZEMBRO ' + state.calendario[mes][0].ano;
                break;
            default:
                break;
        }

        setTextoMesAtual(textoMesAtual);
        
    }

    useEffect(() => {
        setMesatual(Number(state.mes_atual));
        apresentacaoMes(Number(state.mes_atual.chave));
    },[state]);
    
    //usa o index 17 para pegar um dia no meio do mes, garantindo que está no mes correto

    return(
        <div className='calendario-main'>
            <div className="container-mes" style={{marginRight:"5px"}}>
                <div className='calendario-nav'>
                    {textoMesAtual}
                </div>
                <div className="calendario-mes">
                    <span className='calendario-dia'>D</span>
                    <span className='calendario-dia'>S</span>
                    <span className='calendario-dia'>T</span>
                    <span className='calendario-dia'>Q</span>
                    <span className='calendario-dia'>Q</span>
                    <span className='calendario-dia'>S</span>
                    <span className='calendario-dia'>S</span>
                    {
                        state.calendario[mes].map((d, index)=>{
                            return(
                                <span 
                                    className={`calendario-folgas-data`}  
                                    key={index}
                                >
                                    <button
                                        className={`
                                            ${d.eh_mes_atual?"":"mes-anterior-cal"} 
                                        `}
                                    >
                                        {d.dia}
                                    </button>
                                </span>
                            )
                        })
                    }
                </div>
            </div>
        </div>
    )
}
