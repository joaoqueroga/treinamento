import React, {useState, useEffect} from "react";
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';

const TabelaHistorico = (props) => {

    
    return(

    <DataTable value={props.funcoes} header="Solicitações:" footer="" responsiveLayout="scroll">
        <Column field="numero" header="NUMERO"></Column>
        <Column field="dias" header="DIAS SOLICITADOS"></Column>
        <Column field="data" header="DATA SOLICITAÇÃO"></Column>
        <Column field="status" header="STATUS"></Column>
    </DataTable>
    )


}
export default TabelaHistorico;