import { Slider } from 'primereact/slider';
import './style.scss'

const RangerDias = (props) => {

    //passar do pai para filho: SetTotalVigencia, valor Totalvigencia, valor Totalselecao
   
    return(

        <div>
            {
                props.minValueCalendar!==undefined?//Vem da tela de solicitação de férias para o calendário.
                    <div className='container-dias'>
                        <h1 className='total-selected'>{props.totalSelecao}</h1>
                        <Slider step={props.defensor!=undefined&&props.defensor?1:5} value={props.totalSelecao} min={props.minValue} max={props.maxValueCalendar<=props.totalVigencias?props.maxValueCalendar:props.totalVigencias} onChange={(e) => props.setTotalSelecao(e.value)} />
                    </div>
                :
                    <div className='container-dias'>
                        <Slider value={props.totalSelecao} min={props.minValue} max={props.totalVigencias} onChange={(e) => props.setTotalSelecao(e.value)} />
                        <h1 className='total-selected'>{props.totalSelecao}</h1>
                    </div>
                
            }
            
        </div>     

    )

}
export default RangerDias;