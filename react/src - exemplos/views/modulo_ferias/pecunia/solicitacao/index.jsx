import React,{useState, useEffect} from "react";
import { ProgressSpinner } from 'primereact/progressspinner';
import { BreadCrumb } from 'primereact/breadcrumb';
import './style.scss'
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import CaixaVigencia from '../../componentes/selecionar_vigencia/caixa_vigencias';
import { getToken, getUserId } from "../../../../config/auth";
import api from "../../../../config/api";
import Swal from 'sweetalert2';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Tag } from 'primereact/tag';
import { InputNumber } from 'primereact/inputnumber';
import { NavLink } from "react-router-dom";
import {useNavigate, useLocation} from "react-router-dom";
import RangerDias from '../../componentes/ranger_dias/ranger_dias';



function PecuniaSolicitacao() {
    const [loading, setLoading] = useState(false);
    const items = [
        { label: 'Portal do Servidor' },
        { label: 'Solicitar Pecúnia' }
    ];
    const home = { icon: 'pi pi-home', url: '/inicio' }
    const [totalVigencias, setTotalVigencia] = useState(0);
    const [display, setDisplay] = useState(false);
    const [display0, setDisplay0] = useState(false);
    const [display1, setDisplay1] = useState(false);
    const[totalSelecao, setTotalSelecao]= useState(0)
    const [funcoes, setFuncoes] = useState([]);
    const [id_ferias, setIdferias]=([])
    const[minValue, setMinValue] = useState(0)
    const [erro, setErro] = useState(false);
    const [vigencias, setVigencias] = useState([])
    const[solicitado, setSolicitado] = useState(false)
    const[id, setID]= useState()
    const [id_pesquisa_pecunia_funcionario, SetIdPesquisaPecuniaFuncionario] = useState()
    const [saldototal, setSaldototal] = useState();
    const [selectedVigencias, setSelectedVigencias] = useState([])
    const navigate = useNavigate();


    useEffect(() => {

        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data={
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            'id_funcionario': getUserId()
        }
        api.post('/pecunia/listar_pesquisapecunia_funcionario/', data, config)     
        .then((res)=>{
            let x = JSON.parse(res.data);
            let dias=0
            x.saldo_ferias.map((e)=>{
                dias+=e.saldo
            })

            setSaldototal(dias);

            if(x.dias_solicitados!=null){
                setSolicitado(true)}
                setFuncoes([x]);
                setID(x.id_pesquisa_pecunia);
                SetIdPesquisaPecuniaFuncionario(x.id_pesquisa_pecunia_funcionario);
    
            if(x.saldo_ferias.length >0){
                let auxVigencias = [];

                x.saldo_ferias.forEach((element)=>{
                    let vigencia = {};
                    vigencia.key = element.id_ferias;
                    vigencia.ano = 'Saldo '+element.ano_referencia
                    vigencia.saldo = element.saldo+' Dias disponíveis';
                    vigencia.valor = element.saldo;
                    auxVigencias.push(vigencia);
                });
                
                auxVigencias.sort((a,b)=> a.key-b.key); // - garante a ordenação do menor para o maior ano das vigências(caso não venha ordenado do banco)
                
                setVigencias([...auxVigencias]);
                setLoading(false);
            }else{
               setErro(true);
            }
        })
    }, []);

    function reload() {

        setTotalVigencia(0);
        setMinValue(0);
        setTotalSelecao(0);
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data={
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            'id_funcionario': getUserId()
        }
        api.post('/pecunia/listar_pesquisapecunia_funcionario/',data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            setID(x.id_pesquisa_pecunia)

            if(x.dias_solicitados!=null){
                setSolicitado(true)
                setFuncoes([x]);
            
            }

            if(x.saldo_ferias.length >0){
                let auxVigencias = [];

                x.saldo_ferias.forEach((element)=>{
                    let vigencia = {};
                    vigencia.key = element.id_ferias;
                    vigencia.ano = 'Saldo '+element.ano_referencia
                    vigencia.saldo = element.saldo+' Dias disponíveis';
                    vigencia.valor = element.saldo;
                    auxVigencias.push(vigencia);
                });
                
                auxVigencias.sort((a,b)=> a.key-b.key); // - garante a ordenação do menor para o maior ano das vigências(caso não venha ordenado do banco)
                
                setVigencias([...auxVigencias]);
                setLoading(false);
            }
        })
    }


    const renderFooter = (name) => {
        return (
            <div>

                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setDisplay(false);
                    }} 
                    className="btn-2" />
        
             
                <Button  
                    label="Solicitar" 
                    icon="pi pi-check"  
                    className="btn-1" 
                    onClick={SolicitarPecunia}                
                />    
                
            </div>

        );
    }

    function SolicitarPecunia(){


        let vigencias=[]
        selectedVigencias.map((e)=>{
            let vigencia={}
            vigencia.id_ferias=e.key
            vigencias.push(vigencia)
            
        })

        
        console.log(selectedVigencias)
        let data={
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            'dias':totalSelecao,
            'id_pesquisa_pecunia': id,
            'id_funcionario': getUserId(),
            'ferias_solicitadas': vigencias
        } 
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        api.post("pecunia/cadastro_pesquisapecuniafuncionario/" ,data,config).then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === "S"){
                setDisplay(false);
                setSolicitado(true);
                Swal.fire({
                    icon: 'success',
                    title:"Sucesso",
                    text: `Atualizado`,
                    confirmButtonText: 'Fechar',
                })
                reload()
                
           }else{
                Swal.fire({
                    icon:'error',
                    title:"Erro",
                    text: `${x.motivo}`,
                    confirmButtonText: 'Fechar',
                })
            }

        })

    }

    function ExcluirPecunia(){

        Swal.fire({
            icon:'warning',
            text: 'Tem certeza que deseja excluir? Ao confirmar essa ação a sua resposta não será mais válida',
            showCancelButton: true,
            confirmButtonText: 'Sim',
            cancelButtonText: 'Não',
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed===true) {
                console.log("aqui")
                let data={
                    "aplicacao": "SGI",
                    "id_usuario": getUserId(),
                    'excluir': true ,
                    'id_pesquisa_pecunia': id,
                    'id_funcionario': getUserId(),
                    'id_pesquisa_pecunia_funcionario': id_pesquisa_pecunia_funcionario
                }
                let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
                api.post("pecunia/cadastro_pesquisapecuniafuncionario/" ,data,config).then((res)=>{
                    let x = JSON.parse(res.data);
                    if(x.sucesso === "S"){
                        setDisplay(false);
                        setSolicitado(false);
                        Swal.fire({
                            icon: 'success',
                            title:"Sucesso",
                            text: `Resposta excluida`,
                            confirmButtonText: 'Fechar',
                        })
                        reload()
                    }
            
                    else{
                        Swal.fire({
                            icon:'error',
                            title:"Erro",
                            text: `${x.motivo}`,
                            confirmButtonText: 'Fechar',
                        })
                    }
                })
            }
        })


    }

    const irBodyTemplate = () => {
        return(
            <span>
                
                                
                <Button 
                    icon="pi pi-trash"
                    className="btn-red"
                    onClick={() => {ExcluirPecunia()}}
                />
                    

            </span>

        )
    }
    const tagRespondida = <span>Respondida <i className="pi pi-check"></i></span>;

    return ( 
        <div className='view'>
        <div className="view-body">
        {
        loading?
        <div className="loading-pagina" ><ProgressSpinner/>Carregando...</div>
        :
        <div>
            <div className="header">
                <BreadCrumb model={items} home={home}/>
                <h6 className="titulo-header">Converter Férias em Pecúnia</h6>

            </div>
            {
            solicitado?
            <div>
                <div className="top-box">
                    <h1>Converter Férias em Pecúnia</h1>
                    <div style={{display:'flex-inline'}}>
                        <Tag severity="success" value={tagRespondida} className="tag-respondida"></Tag>
                    </div>
                </div>
                <div>
                <DataTable value={funcoes}>

                    <Column 
                        field="id_pesquisa_pecunia" 
                        header="Pesquisa Pecunia"
                    />

                    <Column 
                        field="dias_solicitados" 
                        header="Dias Solicitados"
                    />
                    
                    <Column 
                        field="botao" 
                        header="Excluir" 
                        body={irBodyTemplate} 
                        style={{ flexGrow: 0, flexBasis: '20%' }}
                    />

                </DataTable>
                    
                </div>
                <div>
                    <br />
                    <span className="label-descricao"> *Sua pesquisa está em analise, Acompanhe o status em </span>  
                    <NavLink className="link" to={{pathname:"/publico/solicitacoes", state: { index:3} }}>Acompanhar Solicitações</NavLink>
                </div>
                <div>
                    <span className="label-descricao"> *Para reenviar a resposta dessa pesquisa, é preciso excluir e efetuar novamente essa solicitação.  </span>  
                </div>
            </div>
            :
            <div>
                {/*view aqui*/}
                <div className="pecunia_selecao">

                    <div className="top-box">
                        <h1>Converter Férias em Pecúnia</h1>
                        {vigencias.length!==0?
                        <div style={{display:'flex-inline'}}>
                            <h2>Saldo Total de Férias <Tag severity="success" value={saldototal + " dias"} className="tag-dias"></Tag></h2>
                        </div>: <></>
                        }
                    </div>
                    {vigencias.length!==0?
                    <div className="area-soliciar-pecunia">
                        <div className="caixa-pecunia-esq">
                            <div id="div-1-pecunia">
                                <h5>Saldos disponíveis</h5>
                                <label className="label-descricao">Selecione os saldos que deseja utilizar.</label>
                                <div className="area-soliciar-pecunia_tabela1" style={{ textAlign: 'center', margin: '20px 0' }}>
                                    <div className="caixaVigenciasPecunia">
                                        <CaixaVigencia 
                                            vigencias={vigencias} 
                                            setMinValue={setMinValue} 
                                            setTotalVigencia={setTotalVigencia} 
                                            setTotalSelecao={setTotalSelecao}
                                            setSelectedVigencias={setSelectedVigencias}
                                            selectedVigencias={selectedVigencias}
                                        />
                                    </div>
                                </div>
                                <h5>Quanto dias deseja converter?</h5>
                                    <RangerDias  setTotalSelecao={setTotalSelecao} totalVigencias={totalVigencias} totalSelecao={totalSelecao} minValue={minValue}/>
                                    {/* <InputNumber
                                        disabled={totalSelecao >=0 ? false : true}
                                        value={totalSelecao}
                                        suffix=" dias" 
                                        min={0} max={totalVigencias}
                                        onValueChange={(e) => setTotalSelecao(e.value)}
                                        showButtons 
                                        placeholder="Selecione o saldo"
                                        className="inputnumber_darkblue"                                    
                                    /> */}
                            </div>
                            <div className="container-row">
                            {
                                display0 == false ?
                                    <Button label="Confirmar" icon="pi pi-check" className="btn-1" disabled={display0 == false && totalSelecao >=1 ? false : true} onClick={(e) => { setDisplay0(true); document.getElementById('div-1-pecunia').classList.add("desabilitado"); document.getElementById('div-2-pecunia').classList.remove("desabilitado"); }} />
                                    :
                                    <Button label="Editar" icon="pi pi-undo" className="btn-2" disabled={display1 == false && totalSelecao >=1 ? false : true} onClick={(e) => { setDisplay0(false); document.getElementById('div-2-pecunia').classList.add("desabilitado"); document.getElementById('div-1-pecunia').classList.remove("desabilitado"); }} />
                            }
                            </div>

                        </div>
                        {id!=null?
                        <div className="caixa-pecunia-dir">
                            <div id="div-2-pecunia" className="desabilitado">
                                <h5>Confirmar Solicitação de Pecúnia?</h5>
                                <label className="label-descricao">Números de dias que serão solicitados:</label>
                                <div style={{padding:'25px', textAlign:'center'}}>
                                    <Tag severity="success" value={totalSelecao+" dias"} className="tag-dias" style={{marginLeft: '0'}}></Tag>
                                </div>
                                <label className="label-descricao">Observação: Esta opção é apenas uma pesquisa em que sua solicitação será analisada, sem garantias que o total solicitado será integralmente convertido em pecúnia.</label>
                            </div>
                        </div>
                        :
                        < div className="caixa-pecunia-dir"  style={{userSelect: 'none', pointerEvents:'none', opacity:'0.4'}}>
                            <h5>Sem pesquisa disponível.</h5>
                            <label className="label-descricao">Aguarde a abertura de uma nova pesquisa.</label>
                        </div>
                        }
                        <div></div>
                        <div className="botao-solicitar-escala">
                            <Button 
                                label="Solicitar" 
                                className="btn-1"
                                icon="pi pi-check"
                                disabled={display1 == false && display0 ==true ? false : true}
                                onClick={()=> setDisplay(true)}
                            />
                            <Dialog 
                                header="Confirmar Solicitação de Pecúnia"
                                visible={display}
                                style={{ width: '50%' }} 
                                footer={renderFooter('displayBasic')}
                                onHide={
                                    () =>{
                                    setDisplay(false);
                                    }
                                }
                            >
                                <p>Números de dias que serão solicitados: {totalSelecao} dias </p>
                                <br />
                                <p>Observação: esta opção é apenas uma pesquisa em que sua solicitação será analisada, 
                                    sem garantias que o total solicitado será integralmente convertido em pecúnia</p>
                            </Dialog>
                        </div>
                    </div>
                    :
                    loading?<div></div>
                    :
                    <div className="area-soliciar-pecunia">
                        <div className="caixa-pecunia-esq" style={{display:"flex", flexDirection: "column", justifyContent: "space-between"}}>
                            <label>Você não tem saldo de férias a solicitar!</label>
                            <Button 
                                label="Acompanhar Solicitações" 
                                className="btn-1"
                                visible={loading?false:true}
                                onClick={
                                    ()=>{ 
                                        navigate('/publico/solicitacoes', {state:{index:3}}) // vai para tela de solicitações gerais pública para iniciar na tela de solicitações de PecuniaS
                                    } 
                                }/>
                        </div>
                        { id==null?
                        < div className="caixa-pecunia-dir"  style={{userSelect: 'none', pointerEvents:'none', opacity:'0.4'}}>
                            <div className = "seletor-ranger-slider"> 
                                <h5>Selecione o numero de dias </h5>
                            </div>

                        </div>
                        :
                        < div className="caixa-pecunia-dir calendario-ferias"  style={{userSelect: 'none', pointerEvents:'none', opacity:'0.4'}}>
                            <h5>Sem pesquisa disponível.</h5>
                            <label className="label-descricao">Aguarde a abertura de uma nova pesquisa.</label>
                        </div>
                        }
                    </div>
                    }
                </div>
            </div>
            }
        </div>
        }
        </div>
        </div>
    );
}

export default PecuniaSolicitacao;