import React, {useState, useEffect} from "react";
import { ProgressSpinner } from 'primereact/progressspinner';
import '../../style.scss'
import { DataTable } from 'primereact/datatable';
import { Tag } from 'primereact/tag';
import { Column } from 'primereact/column';
import { FilterMatchMode } from 'primereact/api';
import { Dropdown } from 'primereact/dropdown';

import { getToken, getUserId } from "../../../../config/auth";
import api from "../../../../config/api";



function SolicitacoesPecúnia() {

    const [loading, setLoading] = useState(true);
    const [historico, setHistorico] = useState([])
    const [erro, setErro] = useState(false);
    useEffect(() => {

        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data={
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            'id_funcionario': getUserId()
        }
        api.post('pecunia/listar_pesquisapecunia_funcionario_historico/', data, config)     
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.length > 0){
                setHistorico(x);
                setLoading(false);
            }else{
               setErro(true);
               setLoading(false);
            }
        })

    }, []);

    const [filters] = useState({
        'descricao': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'data_solicitacao': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'status': { value: null, matchMode: FilterMatchMode.EQUALS }
    });

    const statusBodyTemplate = (rowData) => {

        if (rowData.status == 'FINALIZADA'){
            return (<Tag value="FINALIZADA" className="tag-aprovado"></Tag>)
        }
        else if (rowData.status == 'EM ANÁLISE') {
            return (<Tag value="EM ANÁLISE" className="tag-tramitando"></Tag>)
        }
        
      
    }

    const diasConvertidosBodyTemplate = (rowData) => {


        if (rowData.status == 'FINALIZADA'){
             return <span>{rowData.dias_autorizado}</span>;
         }
         else {
             return <span>-</span>;
         }
         
       
     }

     const statusRowFilterTemplate = (options) => {
        return <Dropdown value={options.value} options={statuses} onChange={(e) => options.filterApplyCallback(e.value)} itemTemplate={statusItemTemplate} placeholder="Filtrar por status" className="p-column-filter" showClear />;
    }
    const statusItemTemplate = (option) => {
        return <span>{option}</span>;
    }
    const statuses = [
        'EM ANÁLISE', 'FINALIZADA'
    ];

    const diasColumnTemplate = (rowData) => {
        let datas = rowData.data_solicitacao.split(' ');
        return(
            <span>
                {
                    datas.map((d, index)=>{
                        return d !== '' ?<span key={index}><Tag value={d} className="tag-lista-dias"></Tag>{' '}</span>:null
                    })
                }
            </span>
        )    
            
    }


    return ( 
        <div>
        <div>
        {
        loading?
        <div className="loading-pagina" ><ProgressSpinner/>Carregando...</div>
        :
        <div>
            <div>
                {/*view aqui*/}
                <DataTable  
                    value={historico} 
                    scrollHeight="50vh"
                    filters={filters}
                    filterDisplay="row" 
                >

                    <Column 
                        field="descricao" 
                        filter
                        header="Pesquisa Pecunia"
                        style={{ flexGrow: 0, flexBasis: '25%' }}
                        showFilterMenu={false}
                    />

                    <Column 
                        field="dias" 
                        header="Dias Solicitados"
                        style={{ flexGrow: 0, flexBasis: '25%' }}
                    />

                    <Column 
                        body={diasConvertidosBodyTemplate}
                        header="Dias Convertidos"
                        style={{ flexGrow: 0, flexBasis: '25%' }}
                    />
                    <Column 
                        body={diasColumnTemplate}
                        field="data_solicitacao"
                        header="Data solicitação"
                        filter
                        style={{ flexGrow: 0, flexBasis: '25%' }}
                        showFilterMenu={false}
                    />

                    <Column 
                        field="status"
                        header="Status" 
                        body={statusBodyTemplate}
                        filter 
                        filterElement={statusRowFilterTemplate} 
                        filterPlaceholder="Buscar"
                        style={{ flexGrow: 0, flexBasis: '25%' }}
                        showFilterMenu={false}
                    />

                </DataTable>

            </div>
        </div>
        }
        </div>
        </div>
    );
}

export default SolicitacoesPecúnia;