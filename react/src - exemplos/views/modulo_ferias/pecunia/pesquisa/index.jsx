import {useState, useEffect } from "react";
import { ProgressSpinner } from 'primereact/progressspinner';
import TabelaCadastro from "../../componentes/tabela_cadastro/index.";
import {useNavigate} from 'react-router-dom';
import api from "../../../../config/api";
import { getToken, getUserId } from "../../../../config/auth";
import Swal from 'sweetalert2';
import '../../style.scss'

function PecuniaPesquisa() {

    const navigate = useNavigate();
    const items = [
        { label: 'Recursos Humanos' },
        { label: 'Férias' },
        { label: 'Pesquisa de Pecúnia' }
    ];
    const home = { icon: 'pi pi-home', url: '/inicio' }
    const [erro, setErro] = useState(false);
    const [escalas, setEscalas] = useState([])
    const[nomepagina]= useState('Pesquisa de Pecúnia')
    const [loading, setLoading] = useState(true);
    const[nomebotao]=useState('Nova Pesquisa')
    const[urldados]=useState('/pecunia/pesquisa/')
    const [data_inicial, setDataInicial] = useState('');
    const [data_final, setDataFinal] = useState('');
    const [id, setId] = useState(null);
    const [descricao, setdescricao] = useState('');
    const [modalCadastrar, setModalCadastrar] = useState(false);
    const [modalEditar, setModalEditar] = useState(false);

    function reload() {

        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data={
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            'id_funcionario': getUserId()
        }
        api.post('/pecunia/listar_pesquisapecunia/', data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.length > 0){
                setEscalas(x);
            }
            setLoading(false);
        })
    }


    useEffect(() => {

        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data={
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            'id_funcionario': getUserId()
        }
        api.post('/pecunia/listar_pesquisapecunia/', data, config)     
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.length > 0){
                setEscalas(x);
                setLoading(false);
            }else{
               setErro(true);
               setLoading(false);
            }
        })

    }, []);


    function cadastrar_pesquisa(){

        let data = {};
        if (id) {   
            data = {
                "aplicacao": "SGI",
                "id_usuario": getUserId(),
                "id_pesquisa_pecunia": id,
                "descricao": descricao ,
                "data_inicio": data_inicial,
                'data_fim': data_final,
                'id_funcionario': getUserId()
            }
        } else {
            data = {
                "aplicacao": "SGI",
                "id_usuario": getUserId(),
                "descricao": descricao ,
                "data_inicio": data_inicial,
                'data_fim': data_final,
                'id_funcionario': getUserId()
            }
        }
        
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        api.post("pecunia/cadastro_pesquisapecunia/" ,data,config).then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === "S"){
                setModalCadastrar(false);
                setModalEditar(false);
                Swal.fire({
                    icon: 'success',
                    title:"Sucesso",
                    text: `atualizado`,
                    confirmButtonText: 'fechar',
                })
                reload();
                
           }else{
                Swal.fire({
                    icon:'error',
                    title:"Erro",
                    text: `${x.motivo}`,
                    confirmButtonText: 'fechar',
                })
            }
            

        })

    }

    return ( 
        <div className='view'>
            <div className="view-body">
                {
                    loading?
                    <div className="loading-pagina" ><ProgressSpinner/>Carregando...</div>
                    :
                    <div className="tabela_cadastro">
                        <TabelaCadastro 
                            escalas={escalas} 
                            nomepagina={nomepagina}
                            nomebotao={nomebotao}
                            urldados={urldados}
                            navigate={navigate}
                            items={items}
                            editar={cadastrar_pesquisa}
                            cadastrar={cadastrar_pesquisa}
                            data_final={data_final}
                            data_inicial={data_inicial}
                            descricao={descricao}
                            id={id}
                            setDataFinal={setDataFinal}
                            setDataInicial={setDataInicial}
                            setdescricao={setdescricao}
                            setId={setId}
                            setModalCadastrar={setModalCadastrar}
                            setModalEditar={setModalEditar}
                            modalEditar={modalEditar}
                            modalCadastrar={modalCadastrar}
                            reload={reload}
                        />
                        
                    </div>
                }
            </div>
              
        </div>

    );
}

export default PecuniaPesquisa;