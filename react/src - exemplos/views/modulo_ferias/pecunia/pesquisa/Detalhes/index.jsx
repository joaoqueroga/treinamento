import React, { useState, useEffect, useRef } from 'react';
import { ProgressSpinner } from 'primereact/progressspinner';
import { BreadCrumb } from 'primereact/breadcrumb';
import {useLocation, useParams} from 'react-router-dom'
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { InputNumber } from 'primereact/inputnumber';
import { Checkbox } from 'primereact/checkbox';
import { Tooltip } from 'primereact/tooltip';
import jsPDF from 'jspdf';
import logo from '../../../../../images/logo_h.png'
import autoTable from 'jspdf-autotable';
import Swal from 'sweetalert2';
import { getToken, getUserId } from "../../../../../config/auth";
import { FilterMatchMode } from 'primereact/api';


import api from "../../../../../config/api";
import { FaSleigh } from 'react-icons/fa';

function PesquisaPecuniaDetalhe() {

    const [loading, setLoading] = useState(true);
    const items = [
        { label: 'Portal do Servidor' },
        { label: 'Pesquisa de pecúnia - Detalhe' }
    ];
    const home = { icon: 'pi pi-home', url: '/inicio' }

    const [nome, setNome]= useState();
    const [products, setProducts] = useState([]);
    const dt = useRef(null);
    const [modalCadastrar, setModalCadastrar] = useState(false);
    const [obrigatorio, setobrigatorio] = useState(false);

    const [valueDefensor, setValueDefensor] = useState();
    const [valueServidor, setValueServidor] = useState();
    const [checkedDefensor, setCheckedDefensor] = useState(false);
    const [checkedServidor, setCheckedServidor] = useState(false);

    const id_pesquisa_pecunia = useParams();
    const [erro, setErro] = useState(false);
    const [list, setList] = useState([]);
 
    const [modalSalvar, setModalSalvar] = useState(false);
    const [valormaximoservidor, setValormaximoservidor] = useState();  
    const [valormaximodefensor, setValormaximodefensor] = useState();  
    
    const user = sessionStorage.getItem("nome");

    const [filters] = useState({
        'nome': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'descricao_lotacao': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'descricao_cargo': { value: null, matchMode: FilterMatchMode.CONTAINS },
    });

    useEffect(() => {
       
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data={
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            'id_pesquisa_pecunia': id_pesquisa_pecunia.id 
        }
        api.post('pecunia/listar_pesquisapecunia/', data, config)     
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(Array.isArray(x)){
                setLoading(false);
                setList(x[0]);
                if(x[0].mostrar_encerrar){
                    setValormaximodefensor(x[0].max_defensor)
                    setValormaximoservidor(x[0].max_servidor)
                    setValueDefensor(x[0].teto_defensor)
                    setValueServidor(x[0].teto_servidor)
                    if( x[0].max_defensor == x[0].teto_defensor){
                        setCheckedDefensor(true);
                    }
                    if(x[0].max_servidor == x[0].teto_servidor){
                        setCheckedServidor(true);
                    }
                    
                }
            }else{
               setErro(true);
            }
        })
        
        
    }, []);

    function limparDados() {
        setCheckedDefensor(false);
        setCheckedServidor(false);
        setValueDefensor();
        setValueServidor();
    }

    function encerrar(){

        Swal.fire({
            icon:'warning',
            text: 'Tem certeza que deseja encerrar a Pesquisa?',
            showCancelButton: true,
            confirmButtonText: 'Sim',
            cancelButtonText: 'Não',
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed==true) {             
                let data = {}; 
                data = {
                    "aplicacao": "SGI",
                    "id_usuario": getUserId(),
                    "id_pesquisa_pecunia": list.id_pesquisa_pecunia,
                    'id_funcionario': getUserId()
                }
                let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
                api.post('pecunia/encerrar_pesquisapecunia/', data, config)
                .then((res)=>{
                    let x = JSON.parse(res.data);
                    if(x.sucesso === 'S'){
                        Swal.fire({
                            icon: 'success',
                            title:"Sucesso",
                            text: `Informações Cadastradas`,
                            confirmButtonText: 'fechar',
                        })
                        reload();
                        //setPermissao(null);
                    }else{
                        //setmodalcadastro(false);
                        Swal.fire({
                            icon:'error',
                            title:"Erro",
                            text: `${x.motivo}`,
                            confirmButtonText: 'fechar',
                        })
                    }
                })
            }

        })


    }
    
    
    function salvar(){
            
        let data = {}; 
        data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            "id_pesquisa_pecunia": list.id_pesquisa_pecunia,
            "teto_defensor": valueDefensor,
            "teto_servidor": valueServidor,
            'id_funcionario': getUserId()
        }
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        api.post('pecunia/cadastro_pesquisapecunia/', data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === 'S'){
                Swal.fire({
                    icon: 'success',
                    title:"Sucesso",
                    text: `Informações Cadastradas`,
                    confirmButtonText: 'fechar',
                })
                reload()
                //setPermissao(null);
            }else{
                //setmodalcadastro(false);
                Swal.fire({
                    icon:'error',
                    title:"Erro",
                    text: `${x.motivo}`,
                    confirmButtonText: 'fechar',
                })
            }
        })
                
            

    }


    const modalSalvarFooter= () =>{

        if(checkedDefensor == true){ setValueDefensor(valormaximodefensor)}
        if(checkedServidor == true){ setValueServidor(valormaximoservidor)}

        return(
            <div>

                <Button 
                    label="Não" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setModalSalvar(false);
                        setobrigatorio(false);
                        limparDados();
                    }} 
                    className="btn-cancelar" />
                <Button 
                    label="sim" 
                    icon="pi pi-check" 
                    onClick={()=>{
                        salvar();
                        setModalCadastrar(false);
                    
                    }}
                    className="btn-comum-1"
                />

            </div>


        )
    }

    function reload() {

        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data={
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            'id_pesquisa_pecunia': id_pesquisa_pecunia.id 
        }
        api.post('pecunia/listar_pesquisapecunia/', data, config)     
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(Array.isArray(x)){
                setLoading(false);
                setList(x[0]);
                if(x[0].mostrar_encerrar){
                    setValormaximodefensor(x[0].max_defensor)
                    setValormaximoservidor(x[0].max_servidor)
                    setValueDefensor(x[0].teto_defensor)
                    setValueServidor(x[0].teto_servidor)
                    if( (valueDefensor == valormaximodefensor) && valueDefensor){
                        setCheckedDefensor(true);
                    }
                    if( (valueServidor == valormaximoservidor) && valueServidor){
                        setCheckedDefensor(true);
                    }
                    
                }
            }else{
               setErro(true);
            }
        })
    }


    const modalCadastroFooter = () => {

        return (
            <div>
                {obrigatorio?<p><small className="p-error">Informe os capos obrigatórios (*).</small></p>:null}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setModalCadastrar(false);
                        setobrigatorio(false);
                        limparDados();
                    }} 
                    className="btn-cancelar" />
                <Button 
                    label="Salvar" 
                    icon="pi pi-check" 
                    onClick={()=>{     
                        if((valueDefensor || checkedDefensor) && (valueServidor || checkedServidor)){
                            setobrigatorio(false);               
                            setModalSalvar(true);
                        }else{ setobrigatorio(true)}
                    }}
                    className="btn-comum-1"
                />
                  <Dialog 
                        header="Confirme se as informações estão corretas: "
                        visible={modalSalvar}
                        style={{ width: '40%' }}
                        footer={modalSalvarFooter}
                        onClick={()=>{
                            setModalSalvar(false)
                        }}
                             
                    >
                        <div className="field grupo-input-label">
                            
                            <b>Saldo Liberado para Defensores: {valueDefensor} dias</b>
                            <b>Saldo Liberado para Servidores: {valueServidor} dias</b>
                            <br />
                            <span style={{color: "red" } }>Deseja continuar?</span>
    


                            
                        
                        </div>

                        <br />
                                    
                    </Dialog>
                
            </div>
        );
    }


    function coluna( x, y, tam, rotulo, texto, doc) {
        doc.setFont('helvetica', 'bold');
        doc.text(x, y, `${rotulo}:`);
        doc.setFont('helvetica', 'normal');
        doc.text(x+tam, y, `${texto?texto:""}`);
    }

    function generatePDF (d){

        let data = new Date();
        let dia = `${data.getDate()}/${data.getMonth()+1}/${data.getFullYear()}`;
        let hora = data.toLocaleTimeString();

       
        let doc = new jsPDF('l', 'pt');
        doc.addImage(logo, 'PNG', 40, 20, 155, 30 );
        
        doc.setFont('helvetica', 'bold');
        doc.setFontSize(18);
        doc.setTextColor(20,71,88);
        doc.text(545, 40, `${list.descricao}`);

        //coluna 1: x=50 , coluna 2 x=400

        let y = 60;
        let espaco = 18;

        doc.setFontSize(12);
        doc.setTextColor(0,0,0);

        if(d.length > 0){

           
            y+=espaco;
            autoTable(doc, ({
                startY: y,
                body: d,
                theme: 'striped',
                styles: {
                    fontSize: 9,
                    cellPadding: 2,
                    halign: 'left',
                    valign: 'middle',
                    textColor: "#000",
                    lineColor:"#000",
                    fillColor:"#fff"
                },
                columns: [
                   
                    { header: 'Nome', dataKey: 'nome' },
                    { header: 'Dias', dataKey: 'dias' },
                    { header: 'Data cadastro', dataKey:'data_solicitacao'},
                    { header: 'Cargo', dataKey: 'descricao_cargo' },
                    { header: 'Lotação', dataKey: 'descricao_lotacao' },

                ],
            }))
        }else{
            y+=espaco;
            coluna(40, y,  0, "", "Sem dados", doc);
        }

        //rodapé da pagina
        let pageCount = doc.internal.getNumberOfPages()
        doc.setFontSize(8)
        for (var i = 1; i <= pageCount; i++) {
            doc.setPage(i)
            doc.text('DEFENSORIA PÚBLICA DO ESTADO DO AMAZONAS - AV. ANDRE ARAUJO, 679, ALEIXO  CNPJ: 19.421.427/0001-91', doc.internal.pageSize.width / 2, 570, {
            align: 'center'});
            doc.text(`Emitido por ${user} em ${dia}  ${hora}` + ' - Página ' + String(i) + ' de ' + String(pageCount), doc.internal.pageSize.width / 2, 580, {
            align: 'center'});
        }

        //doc.save(`${s.nome} ${new Date().toLocaleDateString()} ${new Date().toLocaleTimeString()}.pdf`)
        window.open(doc.output('bloburl', { filename: `pesquisa_pecunia.pdf` }), '_blank');
    } 
   

    const exportPdf = () => {
        generatePDF(list.funcionarios)
    }

    const exportCSV = (selectionOnly) => {
        dt.current.exportCSV({ selectionOnly });
    }

    const header = (
        <div className="flex align-items-center export-buttons">
            <div className="header">
                <div> 
                <Button type="button" icon="pi pi-print" label= "Exportar" onClick={exportPdf} className="btn-2" data-pr-tooltip="PDF" />
                <Button type="button" icon="pi pi-file-excel" label="CSV" onClick={() => exportCSV(true)} className="btn-1 ml-auto" data-pr-tooltip="Selection Only" />
                </div>
                <div className='header'>
                    {list.status == "FINALIZADA"?
                    <div>
                        <Button 
                            label="vizualizar teto"
                            icon="pi pi-eye" 
                            className="btn-1"
                            onClick={()=>{setModalCadastrar(true)}}
                        />
                        <Dialog 
                            header="Informações do Cadastro do Teto de Saldo"
                            visible={modalCadastrar}
                            style={{ width: '40%' }}
                            onHide={
                                () =>{
                                    setModalCadastrar(false);
                                    setobrigatorio(false);
                                }
                                }
                        >
                            <div className="field grupo-input-label">
                                <b>Nome da Pesquisa: {list.descricao}</b>
                                <b>Finalizada por: {list.nome_funcionario_encerramento}</b>
                                <b>Data da Finalização: {list.data_encerramento}</b>
                                <b>Saldo Liberado para Defensores: {list.teto_defensor} DIAS</b>
                                <b>Saldo Liberado para Servidores: {list.teto_servidor} DIAS</b>                                                       
                            </div>

                            <br />
                                        
                        </Dialog>

                    </div>
                    :null}
                    {list.status == "EM ANÁLISE"?
                    <div>
                        <Button 
                            label="Definir Teto"
                            icon="pi pi-plus" 
                            className="btn-2"
                            onClick={()=>{setModalCadastrar(true)}}
                        />
                        <Dialog 
                            header="Definir Teto de saldo Pecúnia"
                            visible={modalCadastrar}
                            style={{ width: '50%' }}
                            footer={modalCadastroFooter}
                            onHide={() =>{
                                    setModalCadastrar(false);
                                    setobrigatorio(false);
                                }}>
                            <div className="field grupo-input-label"><h5>Selecione o número de saldo que será convertido:</h5></div>

                            <div className="field grupo-input-label">
                                <h5>Defensor</h5>
                                <br />
                                <div className="field-checkbox">
                                    <Checkbox inputId="binary" checked={checkedDefensor} onChange={e => setCheckedDefensor(e.checked)} />
                                    <label htmlFor="binary"> Converter todos os saldos solicitados</label>
                                </div>
                                <br />
                                {
                                checkedDefensor?
                                
                                <div className="field col-12 md:col-3">
                                    <label htmlFor="minmax-buttons">Numero de dias para converção</label>
                                    <InputNumber inputId="minmax-buttons" disabled value={valormaximodefensor} mode="decimal"  />
                                </div>
                                :
                                <div className="field col-12 md:col-3">
                                    <label htmlFor="minmax-buttons">Numero de dias</label>
                                    <InputNumber inputId="minmax-buttons" value={valueDefensor} onValueChange={(e) => setValueDefensor(e.value)} mode="decimal" showButtons min={0} max={100} />
                                </div>
                                }
                                
                            </div>

                            <br /><hr />

                            <div className="field grupo-input-label">
                                <h5>Servidor</h5>
                                <br />
                                <div className="field-checkbox">
                                    <Checkbox inputId="binary" checked={checkedServidor} onChange={e => setCheckedServidor(e.checked)} />
                                    <label htmlFor="binary"> Converter todos os saldos solicitados</label>
                                </div>
                                <br />
                                {
                                checkedServidor?
                            
                                <div className="field col-12 md:col-3">
                                    <label htmlFor="minmax-buttons">Numero de dias para converção</label>
                                    <InputNumber inputId="minmax-buttons" disabled value={valormaximoservidor}  mode="decimal"  />
                                </div>
                                
                                :
                                <div className="field col-12 md:col-3">
                                    <label htmlFor="minmax-buttons">Numero de dias para converção</label>
                                    <InputNumber inputId="minmax-buttons" value={valueServidor} onValueChange={(e) => setValueServidor(e.value)} mode="decimal" showButtons min={0} max={100} />
                                </div>
                                }
                            </div>
                            <br />              
                        </Dialog>  
                    </div>
                    :null}

                    {(list.teto_defensor>0 && list.teto_servidor>0 && list.status == "EM ANÁLISE")?
                    <div>
                        <Button 
                            label="Encerrar"
                            icon="pi pi-check-circle" 
                            className="btn-1"
                            onClick={()=>{ 
                                encerrar();
                            }}
                        />
                    </div>
                    :null}
            
                </div>
            </div>
        </div>
        
    );
   

    return ( 
        <div className='view'>
            <div className="view-body">
        {
        loading?
        <div className="loading-pagina" ><ProgressSpinner/>Carregando...</div>
        :
        <div>
            <div className="header">
                <BreadCrumb model={items} home={home}/>
                <h6 className="titulo-header">Pesquisa de pecúnia - Detalhe</h6>
            </div>
            <div>

                <Tooltip target=".export-buttons>button" position="bottom" />

                <DataTable 

                    filters={filters}
                    filterDisplay="row"
                    ref={dt} 
                    value={list.funcionarios} 
                    header={header} 
                    dataKey="id" 
                    responsiveLayout="scroll"
                    selectionMode="multiple" 
                    selection={list.funcionarios}
                    
                    >
                    <Column 
                        field="nome" 
                        header="Servidor"
                        filter
                        filterPlaceholder="Buscar"
                        showFilterMenu={false}
                    />

                    <Column 
                        field="dias" 
                        filter
                        header="Dias Solicitados"
                        showFilterMenu={false}
                    />

                    <Column 
                        field="data_solicitacao" 
                        header="Data Cadastro"
                        filter
                        showFilterMenu={false}
                    />

                    <Column 
                        field="descricao_cargo" 
                        header="Cargo"
                        filter
                        filterPlaceholder="Buscar"
                        showFilterMenu={false}
                    />

                    <Column 
                        field="descricao_lotacao" 
                        header="Unidade"
                        filter
                        filterPlaceholder="Buscar"
                        showFilterMenu={false}
                    />
                    
                </DataTable>

            </div>
            
        </div>
        }
        </div>
        </div>
    );
}

export default PesquisaPecuniaDetalhe;