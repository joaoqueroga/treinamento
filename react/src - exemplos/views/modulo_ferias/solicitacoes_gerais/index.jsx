import React, {useState, useEffect} from "react";
import { ProgressSpinner } from 'primereact/progressspinner';
import { BreadCrumb } from 'primereact/breadcrumb';
import '../style.scss'
import { TabPanel, TabView } from "primereact/tabview";
import SolicitacoesFerias from "../ferias/solicitacoes";
import SolicitacoesFolgas from "../folgas/solicitacoes";
import SolicitacoesPecunia from "../pecunia/solicitacoes";
import { useNavigate, useLocation } from "react-router-dom";
import ListagemEscalaFerias from "../ferias/escala/listagem";

function SolicitacoesTelaPrincipal() {
    const navigate = useNavigate();
    const location = useLocation();
    const [loading, setLoading] = useState(true);
    const [activeIndex, setActiveIndex] = useState(location.state?location.state.index:0); // caso o location.state.index venha com valor, então o TabView vai chamar o TabPanel na posição do valor vindo.

    const items = [
        { label: 'Portal do Servidor' },
        { label: 'Acompanhar Solicitações' }
    ];
    const home = { icon: 'pi pi-home', url: '/inicio' }


    useEffect(() => {
        setLoading(false);
    }, []);

    return ( 
        <div className='view'>
        <div className="view-body">
        {
        loading?
        <div className="loading-pagina" ><ProgressSpinner/>Carregando...</div>
        :
        <div>
            <div className="header">
                <BreadCrumb model={items} home={home}/>
                <h6 className="titulo-header">Acompanhar Solicitações</h6>
            </div>
            <div className="">
                <TabView activeIndex={activeIndex} onTabChange={(e) => setActiveIndex(e.index)}>

                    <TabPanel className="tabPanel-solicitacoes" header="Escala de Férias">
                        <ListagemEscalaFerias/>
                    </TabPanel>
                        
                    <TabPanel className="tabPanel-solicitacoes" header="Férias">
                        <SolicitacoesFerias/>
                    </TabPanel>

                    <TabPanel className="tabPanel-solicitacoes" header="Folgas">
                        <SolicitacoesFolgas/> 
                    </TabPanel>

                    <TabPanel className="tabPanel-solicitacoes" header="Pecúnia">
                        <SolicitacoesPecunia/>  
                    </TabPanel>

                </TabView>
            </div>
        </div>
        }
        </div>
        </div>
    );
}

export default SolicitacoesTelaPrincipal;