import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import './style.scss';
import { ProgressSpinner } from 'primereact/progressspinner';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import api from "../../../config/api";
import { getToken, getUserId } from '../../../config/auth';
import PdfFichaFuncional from '../relatorios/ficha_funcional/pdf';
import { BreadCrumb } from 'primereact/breadcrumb';
import { InputText } from 'primereact/inputtext';
import { InputMask } from 'primereact/inputmask';
import { RadioButton } from "primereact/radiobutton";

function ListaVerServidores() {

    const items = [
        { label: 'Recursos Humanos' },
        { label: 'Ver Servidor' }
    ];

    const home = { icon: 'pi pi-home', url: '/inicio' }
    const navigate = useNavigate();

    const [dados, setDados] = useState([]);
    const [nome_busca, setNome_busca] = useState('');
    const [matricula_busca, setMatricula_busca] = useState('');
    const [cpf_busca, setCpf_busca] = useState('');

    const [loading, setLoading] = useState(false);
    const [listar_ativos, setListar_ativos] = useState(true);
    const [cont_busca, setCont_busca] = useState(false);

    function buscarTodos() {
        setLoading(true);

        let config = { headers: { "Authorization": `Bearer ${getToken()}` } }
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(), 
            "tipo_listagem": "PARCIAL",
            "eh_estagiario":
            false, "ativo":
            listar_ativos
        }
        api.post('/rh/servidores/', data, config)
            .then((res) => {
                let x = JSON.parse(res.data);
                setDados(x.funcionarios);
                setLoading(false);
                setCont_busca(true);
            })
    }

    function buscar() {
        setLoading(true);

        let config = { headers: { "Authorization": `Bearer ${getToken()}` } }
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(), 
            "tipo_listagem": "PARCIAL",
            "nome": nome_busca.toUpperCase(),
            "matricula": matricula_busca.toUpperCase(),
            "eh_estagiario": false,
            "ativo": listar_ativos,
            "cpf": cpf_busca
        }
        api.post('/rh/servidores/', data, config)
            .then((res) => {
                let x = JSON.parse(res.data);
                setDados(x.funcionarios);
                setLoading(false);
                setNome_busca('');
                setMatricula_busca('');
                setCont_busca(true);
                setCpf_busca('');
            })
    }

    const irBodyTemplate = (rowData) => {
        return (
            <span className='botoes-acao'>
                <Button
                    onClick={() => navigate(
                        `/rh/servidor/${rowData.id_funcionario}`,
                        { state: { ativos: listar_ativos } })
                    }
                    icon="pi pi-eye"
                    className="btn-green"
                    title="Editar servidor"
                />
                <PdfFichaFuncional servidor={rowData} />
            </span>
        )
    }

    const bindBotaoEnter = (event) => {
        if (event.key === "Enter")
            buscar();
    }

    return (
        <div className='view'>
            <div className="view-body">
                {
                    loading ?
                        <div className="loading-pagina" ><ProgressSpinner />Carregando...</div>
                        :
                        <div>
                            <div className="header">
                                <BreadCrumb model={items} home={home} />
                                <h6 className="titulo-header">Membros e Servidores</h6>
                            </div>
                            <div className="top-box">
                                <h1>Membros e Servidores</h1>
                            </div>
                            <div>
                                {
                                    cont_busca ?
                                        <b style={{ fontSize: "12px" }}>Resultados encontrados: {dados.length}</b>
                                        : null
                                }

                                <div className='formulario-busca-servidores'>
                                    <InputMask
                                        className="p-inputtext-sm input-busca-servidores-mat texto-maiusculo"
                                        mask="999.999.999-99"
                                        type="text"
                                        value={cpf_busca}
                                        onChange={(e) => setCpf_busca(e.target.value)}
                                        onKeyDownCapture={bindBotaoEnter}
                                        placeholder="CPF"
                                    />
                                    <InputMask
                                        className="p-inputtext-sm input-busca-servidores-mat texto-maiusculo"
                                        mask="999.999-9 a"
                                        type="text"
                                        value={matricula_busca}
                                        onChange={(e) => setMatricula_busca(e.target.value)}
                                        onKeyDownCapture={bindBotaoEnter}
                                        placeholder="Matrícula"
                                    />
                                    <InputText
                                        className="p-inputtext-sm input-busca-servidores-nome"
                                        value={nome_busca}
                                        autoFocus
                                        onChange={(e) => setNome_busca(e.target.value)}
                                        onKeyDownCapture={bindBotaoEnter}
                                        placeholder="Nome"
                                    />
                                    <Button
                                        label="Buscar"
                                        icon="pi pi-search"
                                        className="btn-1 botao-busca-servidor"
                                        onClick={() => buscar()}
                                        style={{ marginLeft: '10px' }}
                                    />
                                    <Button
                                        label="Listar todos"
                                        icon="pi pi-list"
                                        onClick={() => buscarTodos()}
                                        className="btn-2 botao-busca-servidor"
                                        style={{ marginLeft: '10px' }}
                                    />
                                    <RadioButton
                                        inputId="seleção"
                                        value={true}
                                        onChange={(e) => setListar_ativos(e.value)}
                                        checked={listar_ativos === true}
                                        style={{ marginLeft: '15px' }}
                                    />
                                    <label htmlFor="seleção" style={{ marginLeft: '2px' }} className="ml-2">Ativo</label>

                                    <RadioButton
                                        inputId="seleção"
                                        value={false}
                                        onChange={(e) => setListar_ativos(e.value)}
                                        checked={listar_ativos === false}
                                        style={{ marginLeft: '15px' }}
                                    />
                                    <label htmlFor="seleção" style={{ marginLeft: '2px' }} className="ml-2">Inativo</label>
                                </div>
                                <DataTable
                                    value={dados}
                                    size="small"
                                    className="tabela-servidores"
                                    dataKey="id"
                                    emptyMessage="..."
                                    scrollable
                                    scrollHeight="69vh"
                                    selectionMode="single"
                                >
                                    <Column
                                        field="cpf"
                                        header="CPF"
                                        style={{ flexGrow: 0, flexBasis: '22%' }}
                                    />
                                    <Column
                                        field="matricula"
                                        header="Matrícula"
                                        style={{ flexGrow: 0, flexBasis: '22%' }}
                                    />
                                    <Column
                                        field="nome"
                                        header="Nome"
                                        style={{ flexGrow: 0, flexBasis: '70%' }}
                                    />
                                    <Column
                                        field="botao"
                                        header="Ação"
                                        body={irBodyTemplate}
                                        className="col-centralizado"
                                        style={{ flexGrow: 0, flexBasis: '20%' }}
                                    />
                                </DataTable>
                            </div>

                        </div>
                }
            </div>
        </div>
    );
}

export default ListaVerServidores;