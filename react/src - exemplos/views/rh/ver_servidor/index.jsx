import React, {useEffect, useState} from "react";
import {useParams} from 'react-router-dom';
import './style.scss';
import api from "../../../config/api";
import { getToken, getUserId } from "../../../config/auth";
import Swal from 'sweetalert2';

import { TabView, TabPanel } from 'primereact/tabview';

import { ProgressSpinner } from 'primereact/progressspinner';
import { BreadCrumb } from 'primereact/breadcrumb';
import VerDadosPessoaisServidor from "./dados_pessoais";
import VerDadosFuncionaisServidor from "./dados_funcionais";

function Servidor() {

    const items = [
        { label: 'Recursos Humanos' },
        { label: 'Membro ou Servidor' }
    ];
    const home = { icon: 'pi pi-home', url: '/inicio' }

    const { id } = useParams();

    const [servidor, setServidor] = useState(null);
    const [carregar, setCarregar] = useState(false);

    const [tipoanotacao, settipoanotacao] = useState([]);
    const [tipodocumento, settipodocumento] = useState([]);

    function reloadServidor() {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            "id_funcionario": Number(id),
            "tipo_listagem": "COMPLETA"
        }
        api.post('/rh/servidor/',data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            setServidor(x.funcionarios[0]);
        })
    }
    
    useEffect(() => {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data_aux = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
        }
        api.post('/rh/auxiliares/', data_aux, config)
        .then((res)=>{
            let tpanot = JSON.parse(res.data.tipo_anotacao);
            let tpdoc = JSON.parse(res.data.tipo_documento);
            settipoanotacao(tpanot.tipo_anotacao);
            settipodocumento(tpdoc.tipo_documento);
        })

        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            "id_funcionario": Number(id),
            "tipo_listagem": "COMPLETA"
        }
        api.post('/rh/servidor/',data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === "S"){
                setCarregar(true);
                setServidor(x.funcionarios[0]);
            }else{
                Swal.fire({
                    icon:'error',
                    title:"Erro",
                    text: `${x.motivo}`,
                    confirmButtonText: 'fechar',
                })
            }
        })
    }, []);

    return ( 
        <div className='view'>
            <div className='view-body'>
            {carregar?
            <>
            <div className="header">
                <BreadCrumb model={items} home={home}/>
                <h6 className="titulo-header">Editar Membro/Servidor</h6>
            </div>
            <TabView className="cadastro-painel">
            <TabPanel header="Dados Pessoais"> 
                <VerDadosPessoaisServidor
                    dados={servidor}
                />
            </TabPanel>
            <TabPanel header="Dados Funcionais">
                <VerDadosFuncionaisServidor
                    dados={servidor}
                    tipo_anotacao={tipoanotacao}
                    tipo_documento={tipodocumento}
                    reloadServidor={reloadServidor}
                />
            </TabPanel>
            <TabPanel header="Anotações">
                
            </TabPanel>
            <TabPanel header="Documentos Diversos">
               
            </TabPanel>
            <TabPanel header="Arquivos Anexos">
               
            </TabPanel>
            <TabPanel header="Funções">
                
            </TabPanel>
            
            </TabView>
            </>
            :
            <div className='loading-pagina'>
                <div className="loading-pagina" ><ProgressSpinner/>Carregando...</div>
            </div>
            }
            </div> 
        </div>
    );
    
    // return ( 
    //     <div className='view'>
    //     <div className='view-info'>
    //         <span>
    //             <Link className="bt-sair-pagina" to={`/rh/servidores`}>
    //                 <FaArrowLeft/> Voltar
    //             </Link>
    //             {servidor?servidor.nome:"Servidor"}
    //         </span>
    //     </div>
    //     <div className="view-body">
    //     {carregar?
    //     <TabView>
    //     <TabPanel header="Dados Pessoais"> 
    //         <VerDadosPessoaisServidor
    //             dados={servidor}
    //         />
    //     </TabPanel>
    //     <TabPanel header="Dados Funcionais">
    //         <VerDadosFuncionaisServidor
    //             dados={servidor}
    //             tipo_anotacao={tipoanotacao}
    //             tipo_documento={tipodocumento}
    //             reloadServidor={reloadServidor}
    //         />
    //     </TabPanel>
    //     </TabView>
    //     :
    //     <div className='loading-pagina'>
    //         <div className="loading-pagina" ><ProgressSpinner/>Carregando...</div>
    //     </div>
    //     }
    //     </div>
    //     </div>
    // );
}

export default Servidor;