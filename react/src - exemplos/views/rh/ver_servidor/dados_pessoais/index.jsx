import React from 'react';
import '../style.scss';
import foto from '../../../../images/default.jpg';
import assinatura from '../../../../images/default_assinatura.png';

function VerDadosPessoaisServidor(props) {
    return ( 

        <div className="card form-cadastro-servidor">
            <div className='painel-cadastro-servidor'>
                <span className='grupo-input-mat'>
                    Matrícula 
                    <p className='input-mat'>{props.dados.matricula}</p>
                </span>
                <span className='grupo-input-foto'>
                    <img src={assinatura} className="ass-servidor"/>
                </span>
                <span className='grupo-input-foto'>
                    <img src={foto} className="foto-servidor"/>
                </span>
                
            </div>
            <h4 className='rotulo-formulario'>Dados Pessoais</h4>
            <div className='painel-cadastro-servidor'>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Nome</p> 
                    <p className='item-ver-dados'>{props.dados.nome}</p>
                </span>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Nome Social</p>
                    <p className='item-ver-dados'>{props.dados.nome_social}</p>
                </span>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Nome da Mãe</p>
                    <p className='item-ver-dados'>{props.dados.nome_mae}</p>
                </span>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Nome do Pai</p>
                    <p className='item-ver-dados'>{props.dados.nome_pai}</p>
                </span>

                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Data de Nascimento</p>
                    <p className='item-ver-dados'>{props.dados.nascimento}</p> 
                </span>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Naturalidade</p>
                    <p className='item-ver-dados'>{props.dados.naturalidade} - {props.dados.uf}</p>
                </span>
                
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Sexo</p>
                    <p className='item-ver-dados'>{props.dados.sexo}</p> 
                </span>

                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Estado Civil</p>
                    <p className='item-ver-dados'>{props.dados.descricao_estado_civil}</p> 
                </span>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Nome Cônjuge</p>
                    <p className='item-ver-dados'>{props.dados.nome_conjuge}</p> 
                </span>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Grupo Sanguíneo</p>
                    <p className='item-ver-dados'>{props.dados.grupo_sanguineo}</p> 
                </span>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Fator RH</p>
                    <p className='item-ver-dados'>{props.dados.fator_rh}</p> 
                </span>

                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Possui União Estavel?</p>
                    <p className='item-ver-dados'>{props.dados.possui_uniao_estavel?"SIM":"NÃO"}</p>
                </span>
            </div>
            <h4 className='rotulo-formulario'>Endereço</h4>
            <div className='painel-cadastro-servidor'>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Tipo Logradouro</p>
                    <p className='item-ver-dados'>{props.dados.descricao_tipo_logradouro}</p> 
                </span>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Logradouro </p>
                    <p className='item-ver-dados'>{props.dados.logradouro}</p>
                </span>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Número</p>
                    <p className='item-ver-dados'>{props.dados.numero}</p>
                </span>

                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Bairro </p>
                    <p className='item-ver-dados'>{props.dados.bairro}</p>
                </span>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Complemento</p>
                    <p className='item-ver-dados'>{props.dados.complemento}</p>
                </span>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>CEP</p>
                    <p className='item-ver-dados'>{props.dados.cep}</p>
                </span>

                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Município </p>
                    <p className='item-ver-dados'>{props.dados.municipio}</p>
                </span>
            </div>
            <h4 className='rotulo-formulario'>Contatos</h4>
            <div className='painel-cadastro-servidor'>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Email Institucional </p>
                    <p className='item-ver-dados'>{props.dados.email_institucional}</p>
                </span>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Email Pessoal </p>
                    <p className='item-ver-dados'>{props.dados.email_pessoal}</p>
                </span>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Telefone Principal</p>
                    <p className='item-ver-dados'>{props.dados.telefone_principal}</p>
                </span>

                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Telefone Alternativo </p>
                    <p className='item-ver-dados'>{props.dados.telefone_elternativo}</p>
                </span>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Telefone de Emergência </p>
                    <p className='item-ver-dados'>{props.dados.telefone_emergencia}</p>
                </span>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Nome do Contato de Emergência</p>
                    <p className='item-ver-dados'>{props.dados.nome_contato_emergencia}</p>
                </span>
            </div>
            <h4 className='rotulo-formulario'>Documentos</h4>
            <div className='painel-cadastro-servidor'>

                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>CPF </p>
                    <p className='item-ver-dados'>{props.dados.cpf}</p>
                </span>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>RG </p>
                    <p className='item-ver-dados'>{props.dados.rg}</p>
                </span>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>RG Data Expedição</p>
                    <p className='item-ver-dados'>{props.dados.rg_expedicao}</p>
                </span>

                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>RG Orgão </p>
                    <p className='item-ver-dados'>{props.dados.rg_orgao}</p>
                </span>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>RG UF</p>
                    <p className='item-ver-dados'>{props.dados.rg_uf}</p>
                </span>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>RG Segunda Via?</p>
                    <p className='item-ver-dados'>{props.dados.rg_segunda_via?"SIM":"NÃO"}</p>
                </span>

                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Título de Eleitor </p>
                    <p className='item-ver-dados'>{props.dados.titulo_eleitor}</p>
                </span>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Zona do Título</p>
                    <p className='item-ver-dados'>{props.dados.titulo_eleitor_zona}</p>
                </span>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Seção do Título</p>
                    <p className='item-ver-dados'>{props.dados.titulo_eleitor_secao}</p>
                </span>

                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Município do Título </p>
                    <p className='item-ver-dados'>{props.dados.titulo_eleitor_municipio}</p>
                </span>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Reservista</p>
                    <p className='item-ver-dados'>{props.dados.reservista}</p>
                </span>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Classe Reservista</p>
                    <p className='item-ver-dados'>{props.dados.reservista_classe}</p>
                </span>

                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>CNH </p>
                    <p className='item-ver-dados'>{props.dados.cnh}</p>
                </span>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Categoria da CNH</p>
                    <p className='item-ver-dados'>{props.dados.cnh_categoria}</p>
                </span>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Data de Validade CNH</p>
                    <p className='item-ver-dados'>{props.dados.cnh_validade}</p>
                </span>

                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Data da Primeira CNH</p> 
                    <p className='item-ver-dados'>{props.dados.cnh_expedicao_primeira}</p>
                </span>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Data de Expedição CNH</p>
                    <p className='item-ver-dados'>{props.dados.cnh_expedicao}</p>
                </span>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>CNH UF</p>
                    <p className='item-ver-dados'>{props.dados.cnh_uf}</p>
                </span>

                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Registro Conselho Profissional</p> 
                    <p className='item-ver-dados'>{props.dados.conselho_profissional}</p>
                </span>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Órgão Emissor Conselho Profissional</p>
                    <p className='item-ver-dados'>{props.dados.conselho_profissional_orgao_emissor}</p>
                </span>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Data de Expedição Conselho Profissional</p>
                    <p className='item-ver-dados'>{props.dados.conselho_profissional_expedicao}</p>
                </span>

                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Data de Validade Conselho Profissional</p> 
                    <p className='item-ver-dados'>{props.dados.conselho_profissional_validade}</p>
                </span>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Conselho Profissional UF</p>
                    <p className='item-ver-dados'>{props.dados.conselho_profissional_uf}</p>
                </span>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>PIS/PASEP</p>
                    <p className='item-ver-dados'>{props.dados.pis}</p>
                </span>

            </div>
            <br/>
        </div>
    );
}

export default VerDadosPessoaisServidor;