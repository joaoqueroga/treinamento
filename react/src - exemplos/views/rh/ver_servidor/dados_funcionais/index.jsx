import React, {useState} from 'react';
import '../style.scss';

import ListarAnotacoes from '../../../../components/listarAnotacoes';

function VerDadosFuncionaisServidor(props) {

    return ( 
        
        <div className="card form-cadastro-servidor">
            <h4 className='rotulo-formulario'></h4>
            <div className='painel-cadastro-servidor'>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Grau de Instrução</p>
                    <p className='item-ver-dados'>{props.dados.descricao_grau_instrucao}</p> 
                </span>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Data de Ingresso no Serviço Público</p>
                    <p className='item-ver-dados'>{props.dados.ingresso}</p> 
                </span>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Data referência férias</p>
                    <p className='item-ver-dados'>{props.dados.ferias_referencia}</p>
                </span>

                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Tipo de Servidor</p>
                    <p className='item-ver-dados'>{props.dados.descricao_tipo_servidor}</p> 
                </span>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Lotação</p>
                    <p className='item-ver-dados'>{props.dados.descricao_lotacao}</p>
                </span>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Chefe imediato </p>
                    <p className='item-ver-dados'>{props.dados.nome_responsavel_lotacao}</p>
                </span>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Cargo </p>
                    <p className='item-ver-dados'>{props.dados.descricao_cargo}</p>
                </span>
                <span className='grupo-input-label-ver'>
                    <p className='rotulo-ver-dados'>Servidor Ativo? </p>
                    <p className='item-ver-dados'>{props.dados.ativo?"SIM":"NÃO"}</p>
                </span>
            </div>

            <h4 className='rotulo-formulario'>Anotações</h4>
            <div className='painel-anotacoes'>
                <ListarAnotacoes anotacoes={props.dados.anotacoes}/>
                {
                    // props.dados?
                    // props.dados.anotacoes.map((a)=>{
                    //     return(
                    //         <div className='card-anotacao' key={a.id_funcionario_anotacoes}>
                    //             <h4><b>{a.descricao_tipo_anotacao} Nº {a.documento_numero_ano} {a.data} Diário: {a.diario_oficial_numero_ano}</b></h4>
                    //             <h5>{a.resumo}</h5>
                    //             <p>{a.descricao}</p>
                    //         </div>
                    //     )
                    // })
                    // :
                    // null
                }

            </div>
            <br/>
        </div>
    );
}

export default VerDadosFuncionaisServidor;