import React, { useState, useEffect, useRef } from 'react';
import './style.scss';
import { TabView, TabPanel } from 'primereact/tabview';
import { Button } from 'primereact/button';
import Swal from 'sweetalert2';
import {useNavigate, useParams, useLocation} from 'react-router-dom';
import DadosPessoaisServidor from '../cadastro_servidor/dados_pessoais'
import DadosFuncionaisServidor from '../cadastro_servidor/dados_funcionais';
import AnotacoesServidor from './anotacoes';
import AnexosServidor from './anexos';
import FuncoesServidor from './funcoes';
import { ProgressSpinner } from 'primereact/progressspinner';
import { BreadCrumb } from 'primereact/breadcrumb';
import api from '../../../config/api';
import { getToken, getUserId } from '../../../config/auth';
import { Toast } from 'primereact/toast';
import DocumentosDiversos from './documentos_diversos';
import Alertas from '../../../utils/alertas';

function EditarServidor() {

    const items = [
        { label: 'Recursos Humanos' },
        { label: 'Membro ou Servidor' }
    ];
    const home = { icon: 'pi pi-home', url: '/inicio' }

    const {state} = useLocation();
    const {ativos} = state;

    const navigate = useNavigate();
    const { id } = useParams();
    const toast = useRef(null);
    
    const [submit, setSubmit] = useState(false);
    const [carregar, setCarregar] = useState(false);

    const [ativo, setAtivo] = useState(true);
    const [bairro, setbairro] = useState("");
    const [cep, setcep] = useState("");
    const [cnh, setcnh] = useState("");
    const [cnh_expedicao, setcnh_expedicao] = useState("");
    const [cnh_expedicao_primeira, setcnh_expedicao_primeira] = useState("");
    const [cnhuf, setCnhuf] = useState("");
    const [cnh_validade, setcnh_validade] = useState("");
    const [complemento, setcomplemento] = useState("");
    const [conselho_profissional, setconselho_profissional] = useState("");
    const [conselho_profissional_expedicao, setconselho_profissional_expedicao] = useState("");
    const [conselho_profissional_orgao_emissor, setconselho_profissional_orgao_emissor] = useState("");
    const [cpuf, setCpuf] = useState("");
    const [conselho_profissional_validade, setconselho_profissional_validade] = useState("");
    const [cpf, setcpf] = useState("");
    const [email_institucional, setemail_institucional] = useState("");
    const [email_pessoal, setemail_pessoal] = useState("");
    const [endereco, setendereco] = useState("");
    const [logradouro, setlogradouro] = useState("");
    const [matricula, setmatricula] = useState("");
    const [endmunicipio, setEndmunicipio] = useState("");
    const [nascimento, setnascimento] = useState("");
    const [natmunicipio, setNatmunicipio] = useState("");
    const [nome, setnome] = useState("");
    const [nome_conjuge, setConjugue] = useState('');
    const [nome_contato_emergencia, setnome_contato_emergencia] = useState("");
    const [nome_mae, setnome_mae] = useState("");
    const [nome_pai, setnome_pai] = useState("");
    const [nome_social, setnome_social] = useState("");
    const [numero, setnumero] = useState("");
    const [uniao, setUniao] = useState(false);
    const [reservista, setreservista] = useState("");
    const [reservista_classe, setreservista_classe] = useState("");
    const [rg, setrg] = useState("");
    const [rg_expedicao, setrg_expedicao] = useState("");
    const [rg_orgao, setrg_orgao] = useState("");
    const [rguf, setRguf] = useState("");
    const [rgsegvia, setRgsegvia] = useState(false);
    const [sexo, setSexo] = useState("");
    const [telefone_elternativo, settelefone_elternativo] = useState("");
    const [telefone_emergencia, settelefone_emergencia] = useState("");
    const [telefone_principal, settelefone_principal] = useState("");
    const [titulo_eleitor, settitulo_eleitor] = useState("");
    const [titulo_eleitor_secao, settitulo_eleitor_secao] = useState("");
    const [titulo_eleitor_zona, settitulo_eleitor_zona] = useState("");
    const [natuf, setNatuf] = useState("");
    const [ecivil, setEcivil] = useState("");
    const [tipologradouro , setTipologradouro] = useState(null);
    const [categoriacnh , setCategoriacnh] = useState("");
    const [titulomunicipio, setTitulomunicipio] = useState("");
    const [grauinstrucao, setGrauinstrucao] = useState("");
    const [tiposervidor, setTiposervidor] = useState(null);
    const [ingresso, setIngresso] = useState("");
    const [ferias_referencia, setFeriasreferencia] = useState("");
    const [cargo, setcargo] = useState(null);
    
    
    //novos
    const [chefe_imediato, setChefeimediato] = useState(null);
    const [lotacao, setLotacao] = useState(null);
    const [cargos, setCargos] = useState(null);
    const [obrigatorio, setObrigatorio] = useState(false);
    const [tipoanotacao, settipoanotacao] = useState([]);
    const [tipodocumento, settipodocumento] = useState([]);
    const [ecivils, setEcivils] = useState([]);
    const [grauInstrucoes, setGrauinstrucoes] = useState([]);
    const [tiposlogradouro, setTiposlogradouro] = useState([])
    const [tiposservidor, setTiposservidor] = useState([])
    const [anotacoes, setAnotacoes] = useState([]);
    const [arquivos, setArquivos] = useState([]);
    const [funcoes, setFuncoes] = useState([]);
    const [pis, setPis] = useState('');
    const [grupo_sanguineo, setGrupo_sanguineo] = useState(null);
    const [fator_rh, setFator_rh] = useState(null);
    const [foto, setFoto] = useState(null);
    const [caminho_foto, setCaminho_foto] = useState(null);
    const [assinatura, setAssinatura] = useState(null);
    const [caminho_assinatura, setCaminho_assinatura] = useState(null);
    const [inicio_atividades, setInicio_atividades] = useState('');
    
    const [ativo_historico, setAtivo_historico] = useState(false); 
    const [documentos_diversos, setDocumentosdiversos]=useState([]);
    const [tipos_documento_diversos, setTipo_documento_diversos]=useState([]);
    const [classes, setClasses] = useState([]);
    const [classe, setClasse] = useState(null);
    

    useEffect(() => {
        setSubmit(false);
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data_aux = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
        }

        api.post('/rh/cargos/',data_aux, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            setCargos(x.cargo);
        })
        
        api.post('/rh/auxiliares/',data_aux, config)
        .then((res)=>{
    
            
            let tpanot = JSON.parse(res.data.tipo_anotacao);
            let tpdoc = JSON.parse(res.data.tipo_documento);
            let ginst = JSON.parse(res.data.grau_instrucao);
            let eciv = JSON.parse(res.data.estado_civil);
            let tl = JSON.parse(res.data.tipo_logradouro);
            let ts = JSON.parse(res.data.tipo_servidor);
            let tpdd = JSON.parse(res.data.tipo_documento_diversos);

            settipoanotacao(tpanot.tipo_anotacao);
            settipodocumento(tpdoc);
            setGrauinstrucoes(ginst.grau_instrucao);
            setEcivils(eciv.estado_civil);
            setTiposlogradouro(tl.tipo_logradouro);
            setTiposservidor(ts.tipo_servidor);
            setTipo_documento_diversos(tpdd);
            
        })

        api.post('/rh/listar_classe/',data_aux, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            setClasses(x);

        })

        
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            "id_funcionario": Number(id),
            "tipo_listagem": "COMPLETA",
            "ativo": ativos
        }
        api.post('/rh/servidor/',data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === "S"){
                setCarregar(true);
                if(x.funcionarios.length > 0){
                    setDocumentosdiversos(x.funcionarios[0].documentos_diversos);
                    setbairro(x.funcionarios[0].bairro ?? '');
                    setcep(x.funcionarios[0].cep ?? '');
                    setcnh(x.funcionarios[0].cnh ?? '');
                    setcnh_expedicao(x.funcionarios[0].cnh_expedicao ?? '');
                    setcnh_expedicao_primeira(x.funcionarios[0].cnh_expedicao_primeira ?? '');
                    setcnh_validade(x.funcionarios[0].cnh_validade ?? '');
                    setcomplemento(x.funcionarios[0].complemento ?? '');
                    setconselho_profissional(x.funcionarios[0].conselho_profissional ?? '');
                    setconselho_profissional_expedicao(x.funcionarios[0].conselho_profissional_expedicao ?? '');
                    setconselho_profissional_orgao_emissor(x.funcionarios[0].conselho_profissional_orgao_emissor ?? '');
                    setconselho_profissional_validade(x.funcionarios[0].conselho_profissional_validade ?? '');
                    setcpf(x.funcionarios[0].cpf ?? '');
                    setemail_institucional(x.funcionarios[0].email_institucional ?? '');
                    setemail_pessoal(x.funcionarios[0].email_pessoal ?? '');
                    setendereco(x.funcionarios[0].endereco ?? '');
                    setlogradouro(x.funcionarios[0].logradouro ?? '');
                    setnascimento(x.funcionarios[0].nascimento ?? '');
                    setmatricula(x.funcionarios[0].matricula ?? '');
                    setnome(x.funcionarios[0].nome ?? '');
                    setnome_contato_emergencia(x.funcionarios[0].nome_contato_emergencia ?? '');
                    setnome_mae(x.funcionarios[0].nome_mae ?? '');
                    setnome_pai(x.funcionarios[0].nome_pai ?? '');
                    setnome_social(x.funcionarios[0].nome_social ?? '');
                    setnumero(x.funcionarios[0].numero ?? '');
                    setreservista(x.funcionarios[0].reservista ?? '');
                    setreservista_classe(x.funcionarios[0].reservista_classe ?? '');
                    setrg(x.funcionarios[0].rg ?? '');
                    setrg_expedicao(x.funcionarios[0].rg_expedicao ?? '');
                    setrg_orgao(x.funcionarios[0].rg_orgao ?? '');
                    settelefone_elternativo(x.funcionarios[0].telefone_elternativo ?? '');
                    settelefone_emergencia(x.funcionarios[0].telefone_emergencia ?? '');
                    settelefone_principal(x.funcionarios[0].telefone_principal ?? '');
                    settitulo_eleitor(x.funcionarios[0].titulo_eleitor ?? '');
                    settitulo_eleitor_secao(x.funcionarios[0].titulo_eleitor_secao ?? '');
                    settitulo_eleitor_zona(x.funcionarios[0].titulo_eleitor_zona ?? '');
                    setSexo({"code": x.funcionarios[0].sexo ?? ''});
                    setUniao(x.funcionarios[0].possui_uniao_estavel);
                    setEcivil({
                        "id_estado_civil":x.funcionarios[0].id_estado_civil ,
                        "descricao":x.funcionarios[0].descricao_estado_civil ?? ''
                    });
                    setRguf({"sigla":x.funcionarios[0].rg_uf ?? ''});
                    setRgsegvia(x.funcionarios[0].rg_segunda_via);
                    setCnhuf({"sigla": x.funcionarios[0].cnh_uf  ?? ''});
                    setCpuf({"sigla": x.funcionarios[0].conselho_profissional_uf  ?? ''});
                    setNatmunicipio({
                        "nome": x.funcionarios[0].naturalidade ?? ''
                    });
                    setNatuf(x.funcionarios[0].uf ?? '');
                    setTipologradouro({
                        "tipo_logradouro": x.funcionarios[0].id_tipo_logradouro,
                        "descricao": x.funcionarios[0].descricao_tipo_logradouro ?? ''
                    });
                    setCategoriacnh(x.funcionarios[0].cnh_categoria);
                    setTitulomunicipio({
                        "nome": x.funcionarios[0].titulo_eleitor_municipio ?? ''
                    });
                    setGrauinstrucao({
                        "id_grau_instrucao": x.funcionarios[0].id_grau_instrucao,
                        "descricao": x.funcionarios[0].descricao_grau_instrucao ?? ''
                    });
                    setTiposervidor({
                        "id_tipo_servidor": x.funcionarios[0].id_tipo_servidor,
                        "descricao": x.funcionarios[0].descricao_tipo_servidor ?? ''
                    });
                    setEndmunicipio({
                        "nome": x.funcionarios[0].municipio ?? ''
                    });
                    setAtivo(x.funcionarios[0].ativo);

                    setAtivo_historico(x.funcionarios[0].ativo);

                    setChefeimediato({
                        "nome": x.funcionarios[0].nome_responsavel_lotacao ?? '',
                        "id_funcionario": x.funcionarios[0].id_funcionario_responsavel_lotacao,
                        "matricula": x.funcionarios[0].matricula_responsavel_lotacao ?? ''
                    });
                    setLotacao({
                        "descricao": x.funcionarios[0].descricao_lotacao ?? '',
                        "id_lotacao": x.funcionarios[0].id_lotacao
                    });
                    setCategoriacnh({"nome": x.funcionarios[0].cnh_categoria ?? ''})
                    setIngresso(x.funcionarios[0].ingresso ?? '');
                    setFeriasreferencia(x.funcionarios[0].ferias_referencia ?? '');
                    setConjugue(x.funcionarios[0].nome_conjuge ?? '');
                    setAnotacoes(x.funcionarios[0].anotacoes)
                    setcargo({
                        "id_cargo":x.funcionarios[0].id_cargo, 
                        "descricao":x.funcionarios[0].descricao_cargo ?? ''
                    });
                    setClasse({
                        "id_classe":x.funcionarios[0].id_classe,
                        "descricao":x.funcionarios[0].descricao_classe?? ''
                    });

                    setGrupo_sanguineo({"nome": x.funcionarios[0].grupo_sanguineo ?? ''});
                    setFator_rh({"nome": x.funcionarios[0].fator_rh ?? ''});
                    setPis(x.funcionarios[0].pis ?? '');
                    setCaminho_foto(x.funcionarios[0].foto);
                    setCaminho_assinatura(x.funcionarios[0].assinatura);
                    setInicio_atividades(x.funcionarios[0].data_inicio_atividades ?? '');
                    setFuncoes(x.funcionarios[0].funcoes);

                    setArquivos(x.funcionarios[0].arquivos);
                }else{
                    Alertas.erro("Sem registro para o servidor informado");
                }
            }else{
                Alertas.erro(x.motivo);
            }
        })

    }, [id]);


    function  reloadAnotacoes() {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            "id_funcionario": Number(id),
            "tipo_listagem": "COMPLETA"
        }
        api.post('/rh/servidor/',data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === "S"){
                setAnotacoes(x.funcionarios[0].anotacoes)
            }
        })
    }

    function  reloadFuncoes() {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            "id_funcionario": Number(id),
            "tipo_listagem": "COMPLETA"
        }
        api.post('/rh/servidor/',data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === "S"){
                setFuncoes(x.funcionarios[0].funcoes)
            }
        })
    }

    function  reloadArquivos() {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            "id_funcionario": Number(id),
            "tipo_listagem": "COMPLETA"
        }
        api.post('/rh/servidor/',data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === "S"){
                setArquivos(x.funcionarios[0].arquivos)
            }
        })
    }

    function  reloadSalvarDocumento() {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            "id_funcionario": Number(id),
            "tipo_listagem": "COMPLETA"
        }
        api.post('/rh/servidor/',data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === "S"){
                setDocumentosdiversos(x.funcionarios[0].documentos_diversos)
            }
        })
    }

    

    const showSuccess = () => {
        toast.current.show({severity:'success', summary: 'Sucesso', detail:'Imegens atualizadas', life: 3000});
    }

    const showEmail = () => {
        toast.current.show({severity:'success', summary: 'Sucesso', detail:'Um email de desativação foi enviado para o DTI', life: 3000});
    }

    const showError = () => {
        toast.current.show({severity:'error', summary: 'Erro', detail:'Erro no upload da imagem', life: 3000});
    }

    function nomeImagem(rotulo, imagem) {
        if(imagem){
            let nome = imagem.name;
            let arr = nome.split('.');
            let tipo = arr[arr.length - 1];
            let d = new Date();
            let tempo = d.toISOString().split('.')[0];
            return `${rotulo}_${cpf}_${tempo}.${tipo}`;
        }
        return null;
    }

    function uploadImegens(){ //faz o upload das imagens
        setObrigatorio(false);
        if(matricula && nome && cpf){    
            setSubmit(true);        
            let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
            // FOTO SERVIDOR
            if(foto || assinatura){
                let nome_foto = nomeImagem("foto", foto);
                let nome_assinatura = nomeImagem("assinatura", assinatura)
                const formData = new FormData();
                formData.append("imagem_foto", foto);
                formData.append("imagem_assinatura", assinatura);
                formData.append("nome_foto", nome_foto);
                formData.append("nome_assinatura", nome_assinatura);
                api.post('rh/upload_imagem_servidor/', formData, config)
                .then((res)=>{
                    if(res.data === 200){
                        showSuccess();
                        cadastrar(nome_foto, nome_assinatura); // depois cadastra o servidor
                    }else{
                        showError();
                        cadastrar('');  // se nao coseguir salvar a foto salva só o servidor
                    }
                    setSubmit(false);
                }).catch((err)=>{
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `${err}`,
                        confirmButtonText: 'fechar',
                    })
                    setSubmit(false);
                })
            }else{
                cadastrar('');  // salvar sem foto
            }
        }else{
            Swal.fire({
            icon:'warning',
            title:'Atenção',
            text: 'Preencha os campos obrigatórios',
            confirmButtonText: 'ok',
            }).then(() => {
                setObrigatorio(true);
            })
            setSubmit(false);
        }
    }

    function enviarEmaileDesativa() {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let d = new Date()
        let data_envio = `${d.getDate()}/${d.getMonth() + 1}/${d.getFullYear()} as ${d.toLocaleTimeString()}`
        let data = {
            cpf: cpf,
            nome: nome,
            matricula: matricula,
            tipo: 'Membro ou Servidor',
            cargo: cargo? cargo.descricao: "NÃO REGISTRADO",
            info: `Desativado por ${sessionStorage.getItem("nome")} em ${data_envio}`
        }
        api.post('/core/enviar_email_desativacao', data, config)
        .then((res)=>{
            if(res.data === 200) console.warn("Email enviado para desativação");
        })
        api.post('/core/desativar_usuario/', {cpf:cpf.replace('.','').replace('.','').replace('-','')}, config)
        .then((res)=>{
            if(res.data === 200) console.warn("Desativado no SIP");
        })
    }


    function cadastrar(nome_foto, nome_assinatura) {   
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            aplicacao: "SGI",
            id_usuario: getUserId(),
            id_funcionario: Number(id), 
            ativo: ativo,
            bairro: bairro.toUpperCase() || null,
            cep: cep || null,
            cnh: cnh || null,
            cnh_categoria: categoriacnh? categoriacnh.nome: null,
            cnh_expedicao: cnh_expedicao || null,
            cnh_expedicao_primeira: cnh_expedicao_primeira || null,
            cnh_uf: cnhuf ? cnhuf.sigla.toUpperCase() : null,
            cnh_validade: cnh_validade || null,
            complemento: complemento.toUpperCase() || null,
            conselho_profissional: conselho_profissional || null,
            conselho_profissional_expedicao: conselho_profissional_expedicao || null,
            conselho_profissional_orgao_emissor: conselho_profissional_orgao_emissor.toUpperCase() || null,
            conselho_profissional_uf: cpuf? cpuf.sigla.toUpperCase() : null,
            conselho_profissional_validade: conselho_profissional_validade || null,
            cpf: cpf || null,
            data_inicio_atividades: inicio_atividades || null,
            email_institucional: email_institucional || null,
            email_pessoal: email_pessoal || null,
            fator_rh: fator_rh?fator_rh.nome:null,
            ferias_referencia: ferias_referencia || null,
            grupo_sanguineo: grupo_sanguineo?grupo_sanguineo.nome:null,
            id_cargo: cargo? cargo.id_cargo: null,
            id_classe: classe? classe.id_classe: null,//classesss
            id_chefe_imediato: chefe_imediato? chefe_imediato.id_funcionario: null,
            id_estado_civil: ecivil?ecivil.id_estado_civil: null,
            id_grau_instrucao: grauinstrucao? grauinstrucao.id_grau_instrucao: null,
            id_lotacao: lotacao? lotacao.id_lotacao: null,
            id_tipo_logradouro: tipologradouro?tipologradouro.tipo_logradouro: null,
            id_tipo_servidor: tiposervidor?tiposervidor.id_tipo_servidor: null,
            ingresso: ingresso || null,
            logradouro: logradouro.toUpperCase() || null,
            matricula: matricula.toUpperCase(),
            municipio: endmunicipio? endmunicipio.nome.toUpperCase(): null,
            nascimento: nascimento || null,
            naturalidade: natmunicipio? natmunicipio.nome.toUpperCase(): null,
            nome: nome.toUpperCase() || null,
            nome_conjuge: nome_conjuge.toUpperCase() || null,
            nome_contato_emergencia: nome_contato_emergencia.toUpperCase() || null,
            nome_mae: nome_mae.toUpperCase() || null,
            nome_pai: nome_pai.toUpperCase() || null,
            nome_social: nome_social.toUpperCase() || null,
            numero: numero || null,
            pis: pis || null,
            possui_uniao_estavel: uniao,
            reservista: reservista || null,
            reservista_classe: reservista_classe.toUpperCase() || null,
            rg: rg || null,
            rg_expedicao: rg_expedicao || null,
            rg_orgao: rg_orgao.toUpperCase() || null,
            rg_segunda_via: rgsegvia,
            rg_uf: rguf? rguf.sigla.toUpperCase() : null,
            sexo: sexo? sexo.code.toUpperCase() : null,
            telefone_elternativo: telefone_elternativo || null,
            telefone_emergencia: telefone_emergencia || null,
            telefone_principal: telefone_principal || null,
            titulo_eleitor: titulo_eleitor || null,
            titulo_eleitor_municipio: titulomunicipio? titulomunicipio.nome.toUpperCase(): null,
            titulo_eleitor_secao: titulo_eleitor_secao || null,
            titulo_eleitor_zona: titulo_eleitor_zona || null,
            uf: natuf || null,
            eh_estagiario: false,

            imagem_caminho_foto: foto? nome_foto: caminho_foto,
            imagem_caminho_assinatura: assinatura? nome_assinatura: caminho_assinatura
        }

        api.post('/rh/cadastro_servidor/', data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === "S"){ // se retorna sucesso da procedure de cadastro
                //envia email caso dasativado
                if(ativo_historico === true && ativo === false){ enviarEmaileDesativa(); }
                Swal.fire({
                icon:'success',
                title:"Sucesso",
                text: 'Servidor atualizado',
                confirmButtonText: 'ok',
                }).then((result) => {
                    if (result.isConfirmed) {
                        navigate('/rh/servidores');
                    } 
                })
                setSubmit(false);
            }else{
                Swal.fire({
                icon:'error',
                title:"Erro",
                text: `${x.motivo}`,
                confirmButtonText: 'fechar',
                })
                setSubmit(false);
            }
        })
    }

    function selecaoLotacaoChefe(l) {
        setChefeimediato({
            "nome": l.nome_funcionario,
            "id_funcionario": l.id_funcionario_responsavel,
            "matricula": l.matricula
        });
    }

    return ( 
        <div className='view'>
            <div className='view-body'>
            {carregar?
            <>
            <div className="header">
                <BreadCrumb model={items} home={home}/>
                <h6 className="titulo-header">Editar Membro/Servidor</h6>
            </div>
            <Toast ref={toast}/>
            <TabView className="cadastro-painel">
            <TabPanel header="Dados Pessoais"> 
                <DadosPessoaisServidor
                    ativos={ativos}
                    editar={true}
                    sexo={{get: sexo, set: setSexo}}
                    uniao={{get: uniao, set: setUniao }}
                    ecivil={{get: ecivil, set: setEcivil}}
                    rguf={{get: rguf, set: setRguf}}
                    rgsegvia={{get: rgsegvia, set: setRgsegvia}}
                    cnhuf={{get: cnhuf, set: setCnhuf}}
                    cpuf={{get: cpuf, set: setCpuf}}
                    natmunicipio={{get: natmunicipio, set: setNatmunicipio}}
                    natuf={{get: natuf, set: setNatuf}}
                    tipologradouro={{get: tipologradouro, set: setTipologradouro}}
                    categoriacnh={{get: categoriacnh, set: setCategoriacnh}}
                    titulomunicipio={{get: titulomunicipio, set: setTitulomunicipio}}
                    endmunicipio={{get: endmunicipio, set: setEndmunicipio}}
                    bairro={{get: bairro , set: setbairro}}
                    cep={{get: cep , set: setcep}}
                    cnh={{get: cnh, set: setcnh}}
                    cnh_expedicao={{get: cnh_expedicao, set: setcnh_expedicao}}
                    cnh_expedicao_primeira={{get: cnh_expedicao_primeira, set: setcnh_expedicao_primeira}}
                    cnh_validade={{get: cnh_validade, set: setcnh_validade}}
                    complemento={{get: complemento, set: setcomplemento}}
                    conselho_profissional={{get: conselho_profissional, set: setconselho_profissional}}
                    conselho_profissional_expedicao={{get: conselho_profissional_expedicao, set: setconselho_profissional_expedicao}}
                    conselho_profissional_orgao_emissor={{get: conselho_profissional_orgao_emissor, set: setconselho_profissional_orgao_emissor}}
                    conselho_profissional_validade={{get: conselho_profissional_validade, set: setconselho_profissional_validade}}
                    cpf={{get: cpf, set: setcpf}}
                    email_institucional={{get: email_institucional, set: setemail_institucional}}
                    email_pessoal={{get: email_pessoal, set: setemail_pessoal}}
                    endereco={{get: endereco, set: setendereco}}
                    logradouro={{get: logradouro, set: setlogradouro}}
                    nascimento={{get: nascimento, set: setnascimento}}
                    matricula={{get: matricula, set: setmatricula}}
                    nome={{get: nome, set: setnome}}
                    nome_contato_emergencia={{get: nome_contato_emergencia, set: setnome_contato_emergencia}}
                    nome_mae={{get: nome_mae, set: setnome_mae}}
                    nome_conjuge={{get: nome_conjuge, set: setConjugue}}
                    nome_pai={{get: nome_pai, set: setnome_pai}}
                    nome_social={{get: nome_social, set: setnome_social}}
                    numero={{get: numero, set: setnumero}}
                    reservista={{get: reservista, set: setreservista}}
                    reservista_classe={{get: reservista_classe, set: setreservista_classe}}
                    rg={{get: rg, set: setrg}}
                    rg_expedicao={{get: rg_expedicao, set: setrg_expedicao}}
                    rg_orgao={{get: rg_orgao, set: setrg_orgao}}
                    telefone_elternativo={{get: telefone_elternativo, set: settelefone_elternativo}}
                    telefone_emergencia={{get: telefone_emergencia, set: settelefone_emergencia}}
                    telefone_principal={{get: telefone_principal, set: settelefone_principal}}
                    titulo_eleitor={{get: titulo_eleitor, set: settitulo_eleitor}}
                    titulo_eleitor_secao={{get: titulo_eleitor_secao, set: settitulo_eleitor_secao}}
                    titulo_eleitor_zona={{get: titulo_eleitor_zona, set: settitulo_eleitor_zona}}
                    obrigatorio={{get: obrigatorio, set: setObrigatorio}}
                    ecivils={ecivils}
                    tiposlogradouro={tiposlogradouro}
                    pis={{get: pis, set: setPis}}
                    grupo_sanguineo={{get: grupo_sanguineo, set: setGrupo_sanguineo}}
                    fator_rh={{get: fator_rh, set: setFator_rh}}
                    foto={{get: foto, set: setFoto}}
                    assinatura={{get: assinatura, set: setAssinatura}}
                    caminho_foto={{get: caminho_foto, set: setCaminho_foto}}
                    caminho_assinatura={{get: caminho_assinatura, set: setCaminho_assinatura}}
                />
            </TabPanel>
            <TabPanel header="Dados Funcionais">
                <DadosFuncionaisServidor
                    ativos={ativos}
                    grauinstrucao={{get: grauinstrucao, set: setGrauinstrucao}}
                    tiposervidor={{get: tiposervidor, set: setTiposervidor}}
                    ativo={{get: ativo, set: setAtivo}}
                    chefe_imediato={{get: chefe_imediato, set: setChefeimediato}}
                    lotacao={{get: lotacao,set: setLotacao}}
                    ingresso={{get: ingresso,set: setIngresso}}
                    ferias_referencia={{get: ferias_referencia,set: setFeriasreferencia}}
                    cargo={{get: cargo,set: setcargo}}
                    cargos={cargos}
                    edicao={true}
                    grauinstrucoes={grauInstrucoes}
                    tiposservidor={tiposservidor}
                    selecaoLotacaoChefe={selecaoLotacaoChefe}
                    funcoes={{get: funcoes,set: setFuncoes}}
                    inicio_atividades={{get: inicio_atividades, set: setInicio_atividades}}
                    classes={classes}
                    classe={{get: classe,set: setClasse}}
                />
            </TabPanel>
            <TabPanel header="Anotações">
                <AnotacoesServidor
                    ativos={ativos}
                    tipo_anotacao={tipoanotacao}
                    tipo_documento={tipodocumento}
                    id_funcionario={Number(id)}
                    anotacoes={{get: anotacoes, set: setAnotacoes}}
                    reloadAnotacoes={reloadAnotacoes}
                />
            </TabPanel>
            <TabPanel header="Documentos Diversos">
               <DocumentosDiversos
                    ativos={ativos} 
                    tipos_documento_diversos={tipos_documento_diversos}
                    id_funcionario={Number(id)}
                    documentos_diversos={{get: documentos_diversos, set: setDocumentosdiversos}}
                    reloadSalvarDocumento={reloadSalvarDocumento}
                />
            </TabPanel>
            <TabPanel header="Arquivos Anexos">
                <AnexosServidor
                    ativos={ativos}
                    id_funcionario={Number(id)}
                    cpf={{get: cpf, set: setcpf}}
                    arquivos={{get: arquivos, set: setArquivos}}
                    reloadArquivos={reloadArquivos}
                />
            </TabPanel>
            <TabPanel header="Funções">
                <FuncoesServidor
                ativos={ativos}
                id_funcionario={Number(id)}
                funcoes={{get: funcoes, set: setFuncoes}}
                reloadFuncoes={reloadFuncoes}
                ></FuncoesServidor>
            </TabPanel>
            
            </TabView>
            <div className='rodape-cadastro-servidor'>
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    className="btn-2" 
                    onClick={()=>navigate('/rh/servidores')}
                    style={{marginRight: '10px'}}
                />
                {
                    submit?
                        <Button 
                            label="Salvar" 
                            icon="pi pi-check" 
                            className="btn-1" 
                            loading
                        />
                    :
                        <Button 
                            label="Salvar" 
                            icon="pi pi-check" 
                            className="btn-1" 
                            onClick={uploadImegens}
                            disabled={!ativos}
                        />
                }
            </div>
            </>
            :
            <div className='loading-pagina'>
                <div className="loading-pagina" ><ProgressSpinner/>Carregando...</div>
            </div>
            }
            </div> 
        </div>
    );
}

export default EditarServidor;