import React, { useState } from 'react';
import '../style.scss';
import ListarAnotacoes from '../../../../components/listarAnotacoes';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { Dropdown } from 'primereact/dropdown';
import { Dialog } from 'primereact/dialog';
import { InputTextarea } from 'primereact/inputtextarea';
import { Calendar } from 'primereact/calendar';
import SeletorData from '../../../../components/SeletorData';
import Swal from 'sweetalert2';
import { getToken, getUserId } from '../../../../config/auth';
import api from '../../../../config/api';


function AnotacoesServidor(props) {
    const [modalcadastro, setmodalcadastro] = useState(false);
    const [modalEditar, setmodaleditar] = useState(false);
    const [anotacao, setAnotacao] = useState(null);
    const [anotacaodoc, setAnotacaodoc] = useState(null);
    const [resumo, setResumo] = useState('');
    const [datadiario, setDatadiario] = useState('');
    const [unidadedoc, setUnidadedoc] = useState('');
    const [descricao, setDescricao] = useState('');
    const [obrigatorio, setobrigatorio] = useState(false);

    const [numerodiario, setNumerodiario] = useState('');
    const [numerodocumento, setNumerodocumento] = useState('');
    const [anodiario, setAnodiario] = useState('');
    const [anodocumento, setAnodocumento] = useState('');

    const [id_anotacao, setId_anotacao] = useState(null);


    //tirar as aspas
    function tratamentoDescricao(desc) {
        let x = desc.replace(/(\r\n|\n|\r)/gm, " ");
        return x;
    }

    function adicionarAnotacao() {
        if(anotacao &&  anotacaodoc && datadiario){
            let nadiario = null;
            let nadocumento = null;

            if(numerodiario && anodiario){
                nadiario = `${numerodiario}/${anodiario.getFullYear()}`; 
            }

            if(numerodocumento && anodocumento){
                nadocumento = `${anodocumento.getFullYear()}/${numerodocumento}`;
            }

            let config = {headers: {"Authorization": `Bearer ${getToken()}`}}

            let data = {
                aplicacao: "SGI",
                id_usuario: getUserId(),
                data: datadiario?datadiario:null,
                descricao: descricao?tratamentoDescricao(descricao):"",
                descricao_tipo_anotacao: anotacao?anotacao.descricao:null,
                descricao_tipo_documento: anotacaodoc?anotacaodoc.descricao:"",
                diario_oficial_numero_ano: nadiario,
                documento_numero_ano: nadocumento,
                documento_unidade: unidadedoc,
                id_funcionario: props.id_funcionario,
                id_tipo_anotacao: anotacao?anotacao.id_tipo_anotacao:null,
                id_tipo_documento: anotacaodoc?anotacaodoc.id_tipo_documento:null,
                resumo: resumo
            }

            api.post('/rh/cadastro_anotacao/',data, config)
            .then((res)=>{
                let x = JSON.parse(res.data);
                if(x.sucesso === "S"){
                    setmodalcadastro(false);
                    Swal.fire({
                        icon: 'success',
                        title:"Sucesso",
                        text: `Anotação registrada`,
                        confirmButtonText: 'fechar',
                    })
                    liparDados();
                    props.reloadAnotacoes();
                }else{
                    setmodalcadastro(false);
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `${x.motivo}`,
                        confirmButtonText: 'fechar',
                    })
                }
            })
        }else{
            setobrigatorio(true);
        }
    }

    function atualizarAnotacao() {
        if(anotacao &&  anotacaodoc && datadiario){

            let nadiario = null;
            let nadocumento = null;

            if(numerodiario && anodiario){
                nadiario = `${numerodiario}/${anodiario.getFullYear()}`; 
            }

            if(numerodocumento && anodocumento){
                nadocumento = `${anodocumento.getFullYear()}/${numerodocumento}`;
            }

            let config = {headers: {"Authorization": `Bearer ${getToken()}`}}

            let data = {
                aplicacao: "SGI",
                id_usuario: getUserId(),
                id_funcionario_anotacoes: Number(id_anotacao),
                data: datadiario?datadiario:null,
                descricao: descricao?tratamentoDescricao(descricao):"",
                descricao_tipo_anotacao: anotacao?anotacao.descricao:null,
                descricao_tipo_documento: anotacaodoc?anotacaodoc.descricao:"",
                diario_oficial_numero_ano: nadiario,
                documento_numero_ano: nadocumento,
                documento_unidade: unidadedoc,
                id_tipo_anotacao: anotacao?anotacao.id_tipo_anotacao:null,
                id_tipo_documento: anotacaodoc?anotacaodoc.id_tipo_documento:null
            }

            api.post('/rh/cadastro_anotacao/',data, config)
            .then((res)=>{
                let x = JSON.parse(res.data);
                if(x.sucesso === "S"){
                    setmodaleditar(false);
                    Swal.fire({
                        icon: 'success',
                        title:"Sucesso",
                        text: `Anotação atualizada`,
                        confirmButtonText: 'fechar',
                    })
                    liparDados();
                    props.reloadAnotacoes();
                }else{
                    setmodaleditar(false);
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `${x.motivo}`,
                        confirmButtonText: 'fechar',
                    })
                }
            })
        }else{
            setobrigatorio(true);
        }
    }

    //EXCLUIR Anotação


    function excluir(obj) {

       Swal.fire({
           icon: 'warning',
           text: 'Excluir  anotação?',
           showCancelButton: true,
           confirmButtonText: 'Sim',
           cancelButtonText: 'Não',
           reverseButtons: true
       }).then((result) => {
           if (result.isConfirmed) { 
               let config = { headers: { "Authorization": `Bearer ${getToken()}` } }
               let data = {
                    'aplicacao': "SGI",
                    'id_usuario': getUserId(),
                    'id_funcionario_anotacoes': obj.id_funcionario_anotacoes,
                    'excluir':true,
               }
               api.post('rh/cadastro_anotacao/', data, config)
                   .then((res) => {
                       let x = JSON.parse(res.data);
                       if (x.sucesso === "S") {
                           Swal.fire({
                               icon: 'success',
                               title: "Sucesso",
                               text: 'Anotação excluída',
                               confirmButtonText: 'ok',
                           }).then(() => {
                               props.reloadAnotacoes();
                           })
                       } else {
                           Swal.fire({
                               icon: 'error',
                               title: "Erro",
                               text: `Erro ao excluir a anotação`,
                               confirmButtonText: 'fechar',
                           })
                       }
                   }).catch((err) => {
                       Swal.fire({
                           icon: 'error',
                           title: "Erro",
                           text: `${err}`,
                           confirmButtonText: 'fechar',
                       })
                   })
           }
       })
   }


    function seleciona(obj) {
        
        if(obj.diario_oficial_numero_ano){
            let anoDiario = new Date(obj.diario_oficial_numero_ano.split('/')[1], '1','1');
            setNumerodiario(obj.diario_oficial_numero_ano.split('/')[0]);
            setAnodiario(anoDiario);
        }

        if(obj.documento_numero_ano){
            let anoDocumento = new Date(obj.documento_numero_ano.split('/')[0], '1','1');
            setNumerodocumento(obj.documento_numero_ano.split('/')[1]);
            setAnodocumento(anoDocumento);
        }

        setId_anotacao(obj.id_funcionario_anotacoes);
        setDescricao(obj.descricao);
        setDatadiario(obj.data);
        setAnotacao({"id_tipo_anotacao": obj.id_tipo_anotacao, "descricao": obj.descricao_tipo_anotacao});
        setAnotacaodoc({"id_tipo_documento":obj.id_tipo_documento, "descricao":obj.descricao_tipo_documento});
        setUnidadedoc(obj.documento_unidade);
        setmodaleditar(true);
    }

    const modalCadastroFooter = () => {
        return (
            <div>
                {obrigatorio?<p className='mensagem-erro'>Informe os campos obrigatórios</p>:null}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setmodalcadastro(false);
                        setobrigatorio(false);
                        liparDados();
                    }} 
                    className="btn-2"
                />
                <Button 
                    label="Salvar" 
                    icon="pi pi-check" 
                    onClick={() => adicionarAnotacao()} 
                    autoFocus 
                    className='btn-1'
                />
            </div>
        );
    }

    const modalEditarFooter = () => {
        return (
            <div>
                {obrigatorio?<p className='mensagem-erro'>Informe os campos obrigatórios</p>:null}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setmodaleditar(false);
                        setobrigatorio(false);
                        liparDados();
                    }} 
                    className="btn-2" 
                />
                <Button 
                    label="Salvar" 
                    icon="pi pi-check" 
                    onClick={() => atualizarAnotacao()} 
                    autoFocus 
                    className="btn-1"/>
            </div>
        );
    }

    function liparDados(){
        setResumo('');
        setDatadiario('');
        setUnidadedoc('');
        setDescricao('');
        setNumerodiario('');
        setAnodiario('');
        setNumerodocumento('');
        setAnodocumento('');
    }

    function novaAnotacao() {

        let y = props.tipo_anotacao.find(y => y.descricao === "PORTARIA");
        if(y)setAnotacao(y);

        let x = props.tipo_documento.find(x => x.descricao === "PORTARIA");
        if(x)setAnotacaodoc(x);

        liparDados();
        setmodalcadastro(true);
    }

    return ( 
        <div>
            <br/>
            <div className='painel-anotacoes'>
                <div>
                    <Button
                        className="btn-1" 
                        label="Nova Anotação" 
                        icon="pi pi-plus" 
                        onClick={() => novaAnotacao()} 
                    />
                </div>
                <Dialog 
                    header="Nova Anotação"
                    visible={modalcadastro}
                    style={{ width: '50%' }}
                    footer={modalCadastroFooter}
                    onHide={
                        () =>{
                            setmodalcadastro(false);
                            setobrigatorio(false);
                            liparDados();
                        }
                    }
                >
                    <span className='field grupo-input-label'>
                        Tipo da Anotação *
                        <Dropdown 
                            value={anotacao} 
                            options={props.tipo_anotacao} 
                            onChange={(e)=>setAnotacao(e.value)} 
                            optionLabel="descricao" 
                            className='input-select p-inputtext-sm'
                            style={{height: "40px"}}
                        />
                    </span>
                    <span className="field grupo-input-label">
                        <span className="field input-anotacoes-num">
                            Número Diário Oficial
                            <input
                                type="number"
                                value={numerodiario}
                                className="block input-numeros"
                                onChange={(e)=>setNumerodiario(e.target.value)}
                                style={{height: "40px"}}
                            />
                        </span>
                        <span className="field input-anotacoes-ano">
                            Ano Diário Oficial
                            <Calendar 
                                id="yearpicker"
                                value={anodiario}
                                onChange={(e) => setAnodiario(e.value)}
                                view="year" 
                                dateFormat="yy"
                                className='imput-ano-anotacao'
                            />
                        </span>
                    </span>
                    <span className="field grupo-input-label">
                        Data Diário Oficial *
                        <div className="seletor-de-data">
                        <SeletorData
                            get={datadiario}
                            set={setDatadiario}
                        />
                        </div>
                    </span>
                    <span className='grupo-input-label'>
                        Tipo do Documento *
                        <Dropdown 
                            value={anotacaodoc} 
                            options={props.tipo_documento} 
                            onChange={(e)=>setAnotacaodoc(e.value)} 
                            optionLabel="descricao" 
                            className='input-select p-inputtext-sm'
                            style={{height: "40px"}}
                        />
                    </span>
                    <span className="field grupo-input-label">
                        <span className="field input-anotacoes-num">
                            Número Documento
                            {/* <InputText
                                value={numerodocumento}
                                className="block"
                                onChange={(e)=>setNumerodocumento(e.target.value)}
                                style={{height: "40px"}}
                            /> */}
                            <input
                                type="number"
                                value={numerodocumento}
                                className="block input-numeros"
                                onChange={(e)=>setNumerodocumento(e.target.value)}
                                style={{height: "40px"}}
                            />
                        </span>
                        <span className="field input-anotacoes-ano">
                            Ano Documento
                            <Calendar 
                                id="yearpicker"
                                value={anodocumento}
                                onChange={(e) => setAnodocumento(e.value)}
                                view="year" 
                                dateFormat="yy"
                                className='imput-ano-anotacao'
                            />
                        </span>
                    </span>

                    <span className="field grupo-input-label">
                        Unidade Documento
                        <InputText 
                            className="block"
                            onChange={(e)=>setUnidadedoc(e.target.value)}
                            style={{height: "40px"}}
                        />
                    </span>
                    <span className="grupo-input-label">
                        Descrição
                        <InputTextarea 
                            rows={5} cols={30} 
                            className="resumo-anotacoes"
                            onChange={(e)=>setDescricao(e.target.value)}
                            style={{height: "40px"}}
                        />
                    </span>
                    
                </Dialog>

                {/*para editar*/}

                <Dialog 
                    header="Editar Anotação"
                    visible={modalEditar}
                    style={{ width: '50%' }}
                    footer={modalEditarFooter}
                    onHide={
                        () =>{
                            setmodaleditar(false);
                            setobrigatorio(false);
                            liparDados();
                        }
                    }
                >
                    <span className='grupo-input-label'>
                        Tipo da Anotação *
                        <Dropdown 
                            value={anotacao} 
                            options={props.tipo_anotacao} 
                            onChange={(e)=>setAnotacao(e.value)} 
                            optionLabel="descricao" 
                            className='input-select p-inputtext-sm'
                            style={{height: "40px"}}
                        />
                    </span>
                    <span className="field grupo-input-label">
                        <span className="field input-anotacoes-num">
                            Número Diário Oficial
                            <input
                                type="number"
                                value={numerodiario}
                                className="block input-numeros"
                                onChange={(e)=>setNumerodiario(e.target.value)}
                                style={{height: "40px"}}
                            />
                        </span>
                        <span className="field input-anotacoes-ano">
                            Ano Diário Oficial
                            <Calendar 
                                id="yearpicker"
                                value={anodiario}
                                onChange={(e) => setAnodiario(e.value)}
                                view="year" 
                                dateFormat="yy"
                                className='imput-ano-anotacao'
                            />
                        </span>
                    </span>
                    <span className="field grupo-input-label">
                        Data Diário Oficial *
                        <div className="seletor-de-data">
                        <SeletorData
                            get={datadiario}
                            set={setDatadiario}
                        />
                        </div>
                    </span>
                    <span className='grupo-input-label'>
                        Tipo do Documento *
                        <Dropdown 
                            value={anotacaodoc} 
                            options={props.tipo_documento} 
                            onChange={(e)=>setAnotacaodoc(e.value)} 
                            optionLabel="descricao" 
                            className='input-select p-inputtext-sm'
                            style={{height: "40px"}}
                        />
                    </span>
                    <span className="field grupo-input-label">
                        <span className="field input-anotacoes-num">
                            Número Documento
                            {/* <InputText
                                value={numerodocumento}
                                className="block"
                                onChange={(e)=>setNumerodocumento(e.target.value)}
                                style={{height: "40px"}}
                            /> */}
                            <input
                                type="number"
                                value={numerodocumento}
                                className="block input-numeros"
                                onChange={(e)=>setNumerodocumento(e.target.value)}
                                style={{height: "40px"}}
                            />
                        </span>
                        <span className="field input-anotacoes-ano">
                            Ano Documento
                            <Calendar 
                                id="yearpicker"
                                value={anodocumento}
                                onChange={(e) => setAnodocumento(e.value)}
                                view="year" 
                                dateFormat="yy"
                                className='imput-ano-anotacao'
                            />
                        </span>
                    </span>
                    <span className="field grupo-input-label">
                        Unidade Documento
                        <InputText
                            value={unidadedoc} 
                            className="block"
                            onChange={(e)=>setUnidadedoc(e.target.value)}
                            style={{height: "40px"}}
                        />
                    </span>
                    <span className="grupo-input-label">
                        Descrição
                        <InputTextarea
                            value={descricao} 
                            rows={5} cols={30} 
                            className="resumo-anotacoes"
                            onChange={(e)=>setDescricao(e.target.value)}
                            style={{height: "40px"}}
                        />
                    </span>
                    
                </Dialog>

                <ListarAnotacoes 
                    anotacoes={props.anotacoes.get}
                    seleciona={seleciona}
                    excluir={excluir}
                    ativos={props.ativos}
                />
            </div>
        </div>
    );
}

export default AnotacoesServidor;

