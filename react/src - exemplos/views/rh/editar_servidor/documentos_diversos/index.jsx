
import ListaDocumentosDiversos from "../../../../components/lista_documentos_diversos";
import React, { useState } from 'react';
import '../style.scss';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { Dropdown } from 'primereact/dropdown';
import { Dialog } from 'primereact/dialog';
import SeletorData from "../../../../components/SeletorData";
import { InputTextarea } from 'primereact/inputtextarea';
import Swal from 'sweetalert2';
import { getToken, getUserId } from "../../../../config/auth";
import api from "../../../../config/api";

function DocumentosDiversos(props) {

    const [numerodocumento, setNumerodocumento] = useState('');
    const [descricao, setDescricao] = useState('');
    const [modalcadastro, setmodalcadastro] = useState(false);
    const [modalEditar, setmodaleditar] = useState(false);
    const [obrigatorio, setobrigatorio] = useState(false);

    const [data_doc, setData_doc] = useState(false);
    const [tipo_documento_diversos, setTipo_documentos_diversos] = useState(null);
    const [id_funcionario_documento_diversos, SetId_funcionario_documento_diversos] = useState(null);


    //tirar as aspas
    function tratamentoDescricao(desc) {
        let x = desc.replace(/(\r\n|\n|\r)/gm, " ");
        return x;
    }

    function adicionarDocumento() {
        if (numerodocumento && tipo_documento_diversos ) {
           

            let config = { headers: { "Authorization": `Bearer ${getToken()}` } }

            let data = {
                aplicacao: "SGI",
                id_usuario: getUserId(), 
                descricao: descricao ? tratamentoDescricao(descricao) : "",
                id_funcionario: props.id_funcionario,
                id_tipo_documento_diversos: tipo_documento_diversos ? tipo_documento_diversos.id_tipo_documento_diversos : null,
                numero: numerodocumento,
                data: data_doc
            }

            api.post('/rh/salvar_funcionario_documento_diversos/', data, config)
                .then((res) => {
                    let x = JSON.parse(res.data);
                    if (x.sucesso === "S") {
                        setmodalcadastro(false);
                        Swal.fire({
                            icon: 'success',
                            title: "Sucesso",
                            text: `Documento registrado`,
                            confirmButtonText: 'fechar',
                        })
                        limparDados();
                        props.reloadSalvarDocumento();
                        setobrigatorio(false);
                    } else {
                        setmodalcadastro(false);
                        Swal.fire({
                            icon: 'error',
                            title: "Erro",
                            text: `${x.motivo}`,
                            confirmButtonText: 'fechar',
                        })
                    }
                })
        } else {
            setobrigatorio(true);
        }
    }

    function atualizarDocumento() {
        if (numerodocumento && tipo_documento_diversos ) {

            let config = { headers: { "Authorization": `Bearer ${getToken()}` } }

            let data = {
                aplicacao: "SGI",
                id_usuario: getUserId(), 
                id_funcionario_documento_diversos: Number(id_funcionario_documento_diversos),
                descricao: descricao ? tratamentoDescricao(descricao) : "",
                id_funcionario: props.id_funcionario,
                id_tipo_documento_diversos: tipo_documento_diversos ? tipo_documento_diversos.id_tipo_documento_diversos : null,
                numero: numerodocumento,
                data: data_doc
            }

            api.post('/rh/salvar_funcionario_documento_diversos/', data, config)
                .then((res) => {
                    let x = JSON.parse(res.data);
                    if (x.sucesso === "S") {
                        setmodaleditar(false);
                        Swal.fire({
                            icon: 'success',
                            title: "Sucesso",
                            text: `Documento atualizado`,
                            confirmButtonText: 'fechar',
                        })
                        limparDados();
                        props.reloadSalvarDocumento();
                        setobrigatorio(false);
                    } else {
                        setmodaleditar(false);
                        Swal.fire({
                            icon: 'error',
                            title: "Erro",
                            text: `${x.motivo}`,
                            confirmButtonText: 'fechar',
                        })
                    }
                })
        } else {
            setobrigatorio(true);
        }
    }

    //Excluir documento

    function excluir(obj) {
        Swal.fire({
            icon: 'warning',
            text: 'Excluir  documento?',
            showCancelButton: true,
            confirmButtonText: 'Sim',
            cancelButtonText: 'Não',
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed) {
                let config = { headers: { "Authorization": `Bearer ${getToken()}` } }
                let data = {
                    "aplicacao": "SGI",
                    "id_usuario": getUserId(), 
                    'id_funcionario_documento_diversos': obj.id_funcionario_documento_diversos,
                    'id_tipo_documento_diversos': obj.id_tipo_documento_diversos,
                    'excluir':true,
                } 
                api.post('rh/salvar_funcionario_documento_diversos/', data, config)
                    .then((res) => {
                        let x = JSON.parse(res.data);
                        if (x.sucesso === "S") {
                            Swal.fire({
                                icon: 'success',
                                title: "Sucesso",
                                text: 'Documento excluído',
                                confirmButtonText: 'ok',
                            }).then(() => {
                                 props.reloadSalvarDocumento();
                            })
                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: "Erro",
                                text: `Erro ao excluir o documento`,
                                confirmButtonText: 'fechar',
                            })
                        }
                    }).catch((err) => {
                        Swal.fire({
                            icon: 'error',
                            title: "Erro",
                            text: `${err}`,
                            confirmButtonText: 'fechar',
                        })
                    })
            }
        })
    }

    function seleciona(obj) {
        setNumerodocumento(obj.numero);
        setDescricao(obj.descricao);
        setmodaleditar(true);
        SetId_funcionario_documento_diversos(obj.id_funcionario_documento_diversos);
        setData_doc(obj.data);
        setTipo_documentos_diversos({"id_tipo_documento_diversos": obj.id_tipo_documento_diversos, "descricao": obj.tipo_documento_diversos_descricao });
    }

    const modalCadastroFooter = () => {
        return (
            <div>
                {obrigatorio ? <p className="mensagem-erro">Informe os campos obrigatórios</p> : null}
                <Button
                    label="Cancelar"
                    icon="pi pi-times"
                    onClick={() => {
                        setmodalcadastro(false);
                        setobrigatorio(false);
                        limparDados();
                    }}
                    className="btn-2" />
                <Button
                    label="Salvar"
                    icon="pi pi-check"
                    onClick={() => adicionarDocumento()}
                    autoFocus
                    className="btn-1" />
            </div>
        );
    }

    const modalEditarFooter = () => {
        return (
            <div>
                {obrigatorio ? <p style={{ color: "#f00" }}>Informe os campos obrigatórios</p> : null}
                <Button
                    label="Cancelar"
                    icon="pi pi-times"
                    onClick={() => {
                        setmodaleditar(false);
                        setobrigatorio(false);
                        limparDados();
                    }}
                    className="btn-2" />
                <Button
                    label="Salvar"
                    icon="pi pi-check"
                    onClick={() => atualizarDocumento()}
                    className="btn-1"
                />
            </div>
        );
    }

    function limparDados(){
        setDescricao('');
        setNumerodocumento('');
        setTipo_documentos_diversos(null);
        setData_doc(null);
    }

    function novoDocumento() {
        limparDados();
        setmodalcadastro(true);
    }

    return (
        <div>
            <br />
            <div className='painel-anotacoes'>
                <div>
                    <Button
                        className="btn-1"
                        label="Novo Documento"
                        icon="pi pi-plus"
                        onClick={() => novoDocumento()}
                    />
                </div>
                <Dialog
                    header="Novo Documento"
                    visible={modalcadastro}
                    style={{ width: '50%' }}
                    footer={modalCadastroFooter}
                    onHide={
                        () => {
                            setmodalcadastro(false);
                            setobrigatorio(false);
                            limparDados();
                        }
                    }
                >


                    <span className='field grupo-input-label'>
                        Tipo do Documento *
                        <Dropdown
                            value={tipo_documento_diversos}
                            options={props.tipos_documento_diversos}
                            onChange={(e) => setTipo_documentos_diversos(e.value)}
                            optionLabel="descricao"
                            className='input-select p-inputtext-sm'
                            style={{ height: "40px" }}

                        />
                    </span>
                    <span className="field grupo-input-label">
                        Data
                        <div className="seletor-de-data">
                        <SeletorData
                            get={data_doc}
                            set={setData_doc}
                        />
                        </div>
                    </span>
                    <span className="field grupo-input-label">
                        Número *
                        <InputText            
                            value={numerodocumento}
                            className="block"
                            onChange={(e) => setNumerodocumento(e.target.value)}
                            style={{height: "40px"}}
                        />
                    </span>
                    <span className="field grupo-input-label">
                        Descrição
                        <InputTextarea
                            rows={5} cols={30}
                            className="resumo-anotacoes"
                            onChange={(e) => setDescricao(e.target.value)}
                            style={{ height: "40px" }}
                        />
                    </span>

                </Dialog>

                {/*para editar*/}

                <Dialog
                    header="Editar Documento"
                    visible={modalEditar}
                    style={{ width: '50%' }}
                    footer={modalEditarFooter}
                    onHide={
                        () => {
                            setmodaleditar(false);
                            setobrigatorio(false);
                            limparDados();
                        }
                    }
                >

                    <span className='grupo-input-label'>
                        Tipo do Documento *
                        <Dropdown
                            value={tipo_documento_diversos}
                            options={props.tipos_documento_diversos}
                            onChange={(e) => setTipo_documentos_diversos(e.value)}
                            optionLabel="descricao"
                            className='input-select p-inputtext-sm'
                            style={{ height: "40px" }}

                        />
                    </span>
                    <span className="field grupo-input-label">
                        Data
                        <div className="seletor-de-data">
                        <SeletorData
                            get={data_doc}
                            set={setData_doc}
                        />
                        </div>
                    </span>
                    <span className="field grupo-input-label">
                        <span className="field input-anotacoes-num">
                            Número *
                            <InputText            
                            value={numerodocumento}
                            className="block"
                            onChange={(e) => setNumerodocumento(e.target.value)}
                            />
                        </span>
                    </span>
                    <span className="grupo-input-label">
                        Descrição
                        <InputTextarea
                            value={descricao}
                            rows={5} cols={30}
                            className="resumo-anotacoes"
                            onChange={(e) => setDescricao(e.target.value)}
                            style={{ height: "40px" }}
                        />

                    </span>

                </Dialog>

                <ListaDocumentosDiversos 
                    documentos_diversos={props.documentos_diversos.get}
                    seleciona={seleciona}
                    excluir={excluir}
                    ativos={props.ativos}
                />
            </div>
        </div>
    );
}

export default DocumentosDiversos;

