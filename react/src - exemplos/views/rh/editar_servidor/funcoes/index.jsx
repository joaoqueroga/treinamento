import React, { useState } from 'react';
import '../style.scss';
import ListarFuncoesServidor from '../../../../components/listaFuncoesServidor';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import SeletorData from '../../../../components/SeletorData';
import SeletorFuncoes from '../../../../components/seletorFuncoes';
import Swal from 'sweetalert2';
import { getToken, getUserId } from '../../../../config/auth';
import api from '../../../../config/api';


function FuncoesServidor(props) {
    const [modalcadastro, setmodalcadastro] = useState(false);
    const [modalEditar, setmodaleditar] = useState(false);
    const [funcao, setFuncao] = useState(null);
    const [funcionarioFuncao, setFuncionarioFuncao] = useState(null);
    const [dataInicial, setDataInicial] = useState('');
    const [dataFinal, setDataFinal] = useState('');
    const [obrigatorio, setobrigatorio] = useState(false);

    function adicionarFuncao() {
        if(funcao && dataInicial){

            let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
            let data = {
                "aplicacao": "SGI",
                "id_usuario": getUserId(),
                "id_funcionario": props.id_funcionario,
                "funcoes": [{"id_funcao": funcao?funcao.id_funcao:null, "data_inicio": dataInicial || "", "data_fim": dataFinal}]
            }
            api.post('/rh/cadastro_funcao_servidor/',data, config)
            .then((res)=>{
                let x = JSON.parse(res.data);
                if(x.sucesso === "S"){
                    setmodalcadastro(false);
                    Swal.fire({
                        icon: 'success',
                        title:"Sucesso",
                        text: `Função registrada`,
                        confirmButtonText: 'fechar',
                    })
                    setFuncao(null);
                    setDataInicial('');
                    setDataFinal('');
                    props.reloadFuncoes();
                }else{
                    setmodalcadastro(false);
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `${x.motivo}`,
                        confirmButtonText: 'fechar',
                    })
                }
            })
        }else{
            setobrigatorio(true);
        }
    }

    function atualizarFuncao() {
        if(funcao && dataInicial){

            let config = {headers: {"Authorization": `Bearer ${getToken()}`}}

            let data = {
                "aplicacao": "SGI",
                "id_usuario": getUserId(),
                "id_funcionario": props.id_funcionario,
                "funcoes": [{"id_funcionario_funcao": funcionarioFuncao,"id_funcao": funcao?funcao.id_funcao:null, "data_inicio": dataInicial || "", "data_fim": dataFinal}]
            }

            api.post('/rh/cadastro_funcao_servidor/',data, config)
            .then((res)=>{
                let x = JSON.parse(res.data);
                if(x.sucesso === "S"){
                    setmodaleditar(false);
                    Swal.fire({
                        icon: 'success',
                        title:"Sucesso",
                        text: `Função atualizada`,
                        confirmButtonText: 'fechar',
                    })
                    props.reloadFuncoes();
                }else{
                    setmodaleditar(false);
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `${x.motivo}`,
                        confirmButtonText: 'fechar',
                    })
                }
            })
        }else{
            setobrigatorio(true);
        }
    }

    function seleciona(obj) {
        setFuncao({
            id_funcao : obj.id_funcao,
            descricao : obj.descricao
        });
        setFuncionarioFuncao(obj.id_funcionario_funcao);
        setDataInicial(obj.data_inicio);
        setDataFinal(obj.data_fim);
        setmodaleditar(true);
    }

    const modalCadastroFooter = () => {
        return (
            <div>
                {obrigatorio?<p style={{color: "#f00"}}>Informe os campos obrigatórios</p>:null}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setmodalcadastro(false);
                        setobrigatorio(false);
                    }} 
                    className="p-button-text" />
                <Button 
                    label="Salvar" 
                    icon="pi pi-check" 
                    onClick={() => adicionarFuncao()} 
                    autoFocus 
                />
            </div>
        );
    }

    const modalEditarFooter = () => {
        return (
            <div>
                {obrigatorio?<p style={{color: "#f00"}}>Informe os campos obrigatórios</p>:null}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setmodaleditar(false);
                        setobrigatorio(false);
                    }} 
                    className="p-button-text" />
                <Button 
                    label="Salvar" 
                    icon="pi pi-check" 
                    onClick={() => atualizarFuncao()} 
                    autoFocus 
                />
            </div>
        );
    }

    function novaFuncao() {
        setFuncao(null);
        setDataInicial('');
        setDataFinal('');
        setmodalcadastro(true);
    }

    return ( 
        <div>
            <br/>
            <div className='painel-funcoes'>
                <div>
                    <Button
                        className="btn-1" 
                        label="Nova Função" 
                        icon="pi pi-plus" 
                        onClick={() => novaFuncao()} 
                    />
                </div>
                <Dialog 
                    header="Nova Função"
                    visible={modalcadastro}
                    style={{ width: '50%' }}
                    footer={modalCadastroFooter}
                    onHide={
                        () =>{
                            setmodalcadastro(false);
                            setobrigatorio(false);
                        }
                    }
                >
                    <span className='grupo-input-label-2cols'>
                        <span className='grupo-input-label' style={{width: "100%"}}>
                            Função a ser atribuída*
                            <span className='grupo-input-label'>
                                <SeletorFuncoes
                                    get={funcao}
                                    set={setFuncao}
                                />
                            </span>
                        </span>
                    </span>

                    <span className="field grupo-input-label">
                        Data Inicial*
                        <div className="seletor-de-data">
                        <SeletorData
                            get={dataInicial}
                            set={setDataInicial}
                        />
                        </div>
                    </span>

                    <span className="field grupo-input-label">
                        Data Final
                        <div className="seletor-de-data">
                        <SeletorData
                            get={dataFinal}
                            set={setDataFinal}
                        />
                        </div>
                    </span>       
                </Dialog>

                {/*para editar*/}

                <Dialog 
                    header="Editar Função"
                    visible={modalEditar}
                    style={{ width: '50%' }}
                    footer={modalEditarFooter}
                    onHide={
                        () =>{
                            setmodaleditar(false);
                            setobrigatorio(false);
                        }
                    }
                >
                    <span className='grupo-input-label-2cols'>
                        <span className='grupo-input-label' style={{width: "100%"}}>
                            Função atribuída
                            <span className='grupo-input-label'>
                                {/* <SeletorFuncoes
                                    get={funcao}
                                    set={setFuncao}
                                /> */}
                                <InputText
                                    value={funcao?funcao.descricao:""}
                                    disabled
                                    style={{height: "40px"}}
                                />
                            </span>
                        </span>
                    </span>

                    <span className="field grupo-input-label">
                        Data Inicial
                        <div className="seletor-de-data">
                        {/* <SeletorData
                            get={dataInicial}
                            set={setDataInicial}
                        /> */}
                            <InputText
                                value={dataInicial}
                                disabled
                                style={{height: "40px", width: "100%"}}
                            />
                        </div>
                    </span>

                    <span className="field grupo-input-label">
                        Data Final
                        <div className="seletor-de-data">
                        <SeletorData
                            get={dataFinal}
                            set={setDataFinal}
                        />
                        </div>
                    </span>
                </Dialog>

                <ListarFuncoesServidor 
                    funcoes={props.funcoes.get}
                    seleciona={seleciona}
                    ativos={props.ativos}
                />
            </div>
        </div>
    );
}

export default FuncoesServidor;

