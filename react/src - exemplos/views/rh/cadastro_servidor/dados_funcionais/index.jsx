import React from 'react';
import '../style.scss';
import SeletorData from '../../../../components/SeletorData';
import { SelectButton } from 'primereact/selectbutton';
import SeletorServidor from '../../../../components/SeletorServidor';
import SeletorLotacao from '../../../../components/SeletorLotacao';
import { Dropdown } from 'primereact/dropdown';
import Swal from 'sweetalert2';

function DadosFuncionaisServidor(props) {
    

    const opcoes =[{name:'Não',value: false},{name:'Sim', value: true}];
    
    // function removerFuncao(f) {
    //     let aux = props.funcoes.get;
    //     let index = aux.indexOf(f);
    //     if (index > -1) {
    //         aux.splice(index, 1);
    //     }
    //     props.funcoes.set([...aux]);
    // }

    function desativarUsuario(v){
        if(v !== null){
            Swal.fire({
                showCancelButton: true,
                confirmButtonText: 'Confirmar',
                cancelButtonText: 'Cancelar',
                title: 'Cuidado',
                icon: 'warning',
                reverseButtons: true,
                html:
                    '<p><b>Desativar um servidor pode ocasionar várias ações</b> </p>' +
                    '<br/>' +
                    '<ul class="texto_alerta_desativar"> ' +
                    '<li> Os acessos para alguns sistemas serão retirados.</li>' +
                    '<li> Após a desativação não será possível reativar via sistema.</li>' +
                    '</ul>' +
                    '<br/>' +
                    '<p>Todas as modificação só serão aplicadas após salvar o formulário</p>'
            }).then((result) => {
                if (result.isConfirmed) {
                    props.ativo.set(v);
                }
            })
        }
    }

    return ( 
        
        <div className="form-cadastro-servidor form-servidor-dados-funcionais">
            <div className='painel-cadastro-servidor'>
                <div className='header-grid'>
                    <span className='grupo-input-label'>
                        Grau de Instrução 
                        <Dropdown 
                            value={props.grauinstrucao.get} 
                            options={props.grauinstrucoes} 
                            onChange={(e)=>props.grauinstrucao.set(e.value)} 
                            optionLabel="descricao" 
                            className='input-select p-inputtext-sm'
                            style={{height: "40px"}}
                        />
                    </span>

                    <span className='grupo-input-label'>
                        Data de Nomeação 
                        <SeletorData
                            get={props.ingresso.get}
                            set={props.ingresso.set}
                        />
                    </span>

                    <span className='grupo-input-label'>
                        Data de Entrada em Exercício 
                        <SeletorData
                            get={props.inicio_atividades.get}
                            set={props.inicio_atividades.set}   
                        />
                    </span>

                    <span className='grupo-input-label select-ativo'>
                        Servidor Ativo? 
                        <SelectButton
                            className="p-button-sm" 
                            value={props.ativo.get}
                            optionLabel="name"
                            options={opcoes}
                            // onChange={(e) => props.ativo.set(e.value)}
                            onChange={(e) => desativarUsuario(e.value)}
                        />
                    </span>


                </div>
                <span className='grupo-input-label'>
                    Data de Referência Férias
                    <SeletorData
                        get={props.ferias_referencia.get}
                        set={props.ferias_referencia.set}
                    />
                </span>

                <span className='grupo-input-label'>
                    Tipo de Servidor 
                    <Dropdown 
                        value={props.tiposervidor.get} 
                        options={props.tiposservidor} 
                        onChange={(e)=>props.tiposervidor.set(e.value)} 
                        optionLabel="descricao" 
                        className='input-select p-inputtext-sm'
                        style={{height: "40px"}}
                    />
                </span>
                <span className='grupo-input-label'>
                    Lotação
                    <SeletorLotacao
                        get={props.lotacao.get}
                        set={props.lotacao.set}
                        lotacao_chefe={props.selecaoLotacaoChefe}
                    />
                </span>
                <span className='grupo-input-label'>
                    Responsável Pela Lotação
                    <SeletorServidor
                        get={props.chefe_imediato.get}
                        set={props.chefe_imediato.set}
                        chefe={true}
                    />
                </span>
                <span className='grupo-input-label'>
                    Cargo 
                    <Dropdown 
                        value={props.cargo.get}
                        options={props.cargos} 
                        onChange={(e)=>props.cargo.set(e.value)} 
                        optionLabel="descricao" 
                        className='input-select p-inputtext-sm'
                        style={{height: "40px"}}
                    />
                </span>
                <span className='grupo-input-label'>
                    Classe 
                    <Dropdown 
                        value={props.classe.get }
                        options={props.classes}
                        onChange={(e)=>props.classe.set(e.value)} 
                        optionLabel="descricao" 
                        className='input-select p-inputtext-sm'
                        style={{height: "40px"}}
                    />
                </span>
                <span className='grupo-input-label'>
                    
                    
                </span>

                {/* <span className='grupo-input-label-2cols'>
                    <span className='grupo-input-label' style={{width: "33%", marginRight: "10px"}}>
                    Selecionar Funções 
                    <SeletorFuncoes set={props.funcoes.set} get={props.funcoes.get}/>
                    </span>

                    <span className='grupo-input-label' style={{width: "67%"}}>
                    
                    </span>
                </span> */}
                {/* <span className='grupo-input-label-2cols'>
                    <span className='grupo-input-label' style={{width: "100%"}}>
                        Funções atribuídas
                        <ListarFuncoes/>
                        {
                            props.funcoes.get.map((f, id)=>{
                                return(
                                    <span key={id} className='item-funcao-servidor'>
                                        <p>{f.descricao}</p>
                                        <span>
                                            Inicio
                                            <SeletorData
                                                get={data_inicio}
                                                set={setData_inicio}
                                            />
                                        </span>
                                        <span>
                                            Fim
                                            <SeletorData
                                                get={data_fim}
                                                set={setData_fim}
                                            />
                                        </span>
                                        <button onClick={()=>removerFuncao(f)}>remover</button>
                                    </span>
                                )
                            })
                        }
                    </span>
                </span> */}

            </div>
            <br/>
        </div>
    );
}

export default DadosFuncionaisServidor;