import React from 'react';
import '../style.scss';

import { BiCloudUpload } from "react-icons/bi";
import { InputText } from 'primereact/inputtext';
import { Dropdown } from 'primereact/dropdown';
import { InputMask } from 'primereact/inputmask';
import { SelectButton } from 'primereact/selectbutton';
import foto_padrao from '../../../../images/default.jpg';
import assinatura_padrao from '../../../../images/default_assinatura.png';

import SeletorData from '../../../../components/SeletorData';
import SeletorMunicipio from '../../../../components/SeletorMunicipio';

//auxs
import sexos from '../../../../jsons/sexos.json';
import ufs from '../../../../jsons/estados.json';
import grupossanguineos from '../../../../jsons/gruposanguineos.json';
import catcnh from '../../../../jsons/categoriacnh.json';
import { urlFiles } from '../../../../config/config';


function DadosPessoaisServidor(props) {

    const sexoChange = (e) => {
        props.sexo.set(e.value);
    }
    const ecivilChange = (e) => {
        props.ecivil.set(e.value);
    }

    const fatorRH = [
        {
            "nome": "POSITIVO"
        },
        {
            "nome": "NEGATIVO"
        }
    ]
    const opcoes =[{name:'Não',value: false},{name:'Sim', value: true}];
   
    return ( 

        <div className="form-cadastro-servidor">
            <div className='painel-cadastro-servidor'>
            <div></div>
                

                <span className='grupo-input-foto'>
                    {
                        props.editar?
                            props.foto.get?
                                <img src={URL.createObjectURL(props.foto.get)} alt="foto" className="foto-servidor"/>
                            :
                                props.caminho_foto.get?
                                    <img src={`${urlFiles}${props.caminho_foto.get}`} alt="foto" className="foto-servidor"/>
                                :
                                    <img src={foto_padrao} alt="foto" className="foto-servidor"/>
                        :
                            props.foto.get?
                                <img src={URL.createObjectURL(props.foto.get)} alt="foto" className="foto-servidor"/>
                            :
                                <img src={foto_padrao} alt="foto" className="foto-servidor"/>
                    }
                    
                    <label className='label-file'>
                        <BiCloudUpload size={20}/>
                        <input
                            type="file"
                            id="image" 
                            className='input-files' 
                            accept='image/*' 
                            onChange={e=>props.foto.set(e.target.files[0])}
                            disabled={!props.ativos}
                        />
                    </label>
                </span>
                <span className='grupo-input-mat grupo-input-label'>
                    <label>Matrícula *</label>
                    <InputMask 
                        style={props.obrigatorio.get?{borderColor:"#DE4B24"}:{}}
                        type="text" 
                        className="p-inputtext-sm input-mat texto-maiusculo"
                        mask="999.999-9 a"
                        value={props.matricula.get} 
                        onChange={(e) => props.matricula.set(e.target.value)} 
                        disabled={!props.ativos}
                    />
                </span>
                <span className='grupo-input-foto'>
                    {
                        props.editar?
                            props.assinatura.get?
                                <img src={URL.createObjectURL(props.assinatura.get)} alt="assinatura" className="ass-servidor"/>
                            :
                                props.caminho_assinatura.get?
                                    <img src={`${urlFiles}${props.caminho_assinatura.get}`} alt="assinatura" className="ass-servidor"/>
                                :
                                    <img src={assinatura_padrao} alt="assinatura" className="ass-servidor ass-default-servidor"/>
                        :
                            props.assinatura.get?
                                <img src={URL.createObjectURL(props.assinatura.get)} alt="assinatura" className="ass-servidor"/>
                            :
                                <img src={assinatura_padrao} alt="assinatura" className="ass-servidor ass-default-servidor"/>
                    }
                    <label className='label-file'>
                        <BiCloudUpload size={20}/>
                        <input
                            value="" 
                            type="file" 
                            className='input-files' 
                            accept='image/*'
                            onChange={e=>props.assinatura.set(e.target.files[0])}
                            disabled={!props.ativos}
                        />
                    </label>
                </span>


            </div>
            <h4 className='rotulo-formulario'>Dados Pessoais</h4>
            <div className='painel-cadastro-servidor'>
                <span className='grupo-input-label'>
                    <label>Nome *</label>
                    <InputText 
                        type="text"
                        style={props.obrigatorio.get?{borderColor:"#DE4B24", height: "40px"}:{height: "40px"}} 
                        className="p-inputtext-sm texto-maiusculo"
                        value={props.nome.get} 
                        onChange={(e) => props.nome.set(e.target.value)}
                        disabled={!props.ativos}
                    />
                </span>
                <span className='grupo-input-label'>
                    <label>Nome Social (Opcional)</label>
                    <InputText 
                        type="text" 
                        className="p-inputtext-sm texto-maiusculo"
                        value={props.nome_social.get} 
                        onChange={(e) => props.nome_social.set(e.target.value)}
                        style={{height: "40px"}}
                        disabled={!props.ativos}
                    />
                </span>
                <span className='grupo-input-label'>
                    <label>Nome da Mãe </label>
                    <InputText 
                        type="text"
                        className="p-inputtext-sm texto-maiusculo"
                        value={props.nome_mae.get} 
                        onChange={(e) => props.nome_mae.set(e.target.value)}
                        style={{height: "40px"}}
                        disabled={!props.ativos}
                    />
                </span>
                <span className='grupo-input-label'>
                    <label>Nome do Pai</label>
                    <InputText 
                        type="text" 
                        className="p-inputtext-sm texto-maiusculo"
                        value={props.nome_pai.get} 
                        onChange={(e) => props.nome_pai.set(e.target.value)}
                        style={{height: "40px"}}
                        disabled={!props.ativos}
                    />
                </span>
                <span className='grupo-input-label'>
                    <label>Data de Nascimento</label>
                    <SeletorData
                        get={props.nascimento.get}
                        set={props.nascimento.set}
                    />
                </span>
                <span className='grupo-input-label'>
                    <label>Naturalidade</label>
                    <div id='naturalidade-input'>
                        <div className="input-mu-naturalidade">
                        <SeletorMunicipio
                            get={props.natmunicipio.get}
                            set={props.natmunicipio.set}
                            setUf={props.natuf.set}
                        />
                        </div>
                        <div className="input-uf-naturalidade">{props.natuf.get}</div>
                    </div>
                </span>
                
                <span className='grupo-input-label grupo-input-label-2cols'>
                    <div className='grupo-input-label-grad'>
                        <label>Sexo </label>
                        <Dropdown 
                            value={props.sexo.get} 
                            options={sexos} 
                            onChange={sexoChange} 
                            optionLabel="code" 
                            className='input-select p-inputtext-sm'
                            style={{height: "40px"}}
                            disabled={!props.ativos}
                        />
                        
                    </div>
                    <div className='grupo-input-label-grad'>
                        <label>Possui União Estavel?</label>
                        <SelectButton
                            className="p-button-sm" 
                            value={props.uniao.get}
                            optionLabel="name"
                            options={opcoes}
                            onChange={(e) => props.uniao.set(e.value)}
                            disabled={!props.ativos}
                        />
                        
                    </div>
                </span>

                <span className='grupo-input-label'>
                    <label>Estado Civil </label>
                    <Dropdown 
                        value={props.ecivil.get} 
                        options={props.ecivils} 
                        onChange={ecivilChange} 
                        optionLabel="descricao" 
                        className='input-select p-inputtext-sm'
                        style={{height: "40px"}}
                        disabled={!props.ativos}
                    />
                </span>
                <span className='grupo-input-label'>
                    <label>Nome Cônjuge </label>
                    <InputText 
                        type="text" 
                        className="p-inputtext-sm texto-maiusculo"
                        value={props.nome_conjuge.get} 
                        onChange={(e) => props.nome_conjuge.set(e.target.value)}
                        style={{height: "40px"}}
                        disabled={!props.ativos}
                    />
                </span>

                <span className='grupo-input-label grupo-input-label-2cols'>
                    <div className='grupo-input-label-grad'>
                        <label>Grupo Sanguíneo </label>
                        <Dropdown 
                            value={props.grupo_sanguineo.get} 
                            options={grupossanguineos} 
                            onChange={(e)=>props.grupo_sanguineo.set(e.value)} 
                            optionLabel="nome" 
                            className='input-select p-inputtext-sm'
                            style={{height: "40px"}}
                            disabled={!props.ativos}
                        />                        
                    </div>
                    <div className='grupo-input-label-grad'>
                        <label>Fator RH </label>
                        <Dropdown 
                            value={props.fator_rh.get} 
                            options={fatorRH} 
                            onChange={(e)=>props.fator_rh.set(e.value)} 
                            optionLabel="nome" 
                            className='input-select p-inputtext-sm'
                            style={{height: "40px"}}
                            disabled={!props.ativos}
                        />
                    </div>

                </span>

                <span className='grupo-input-label'>
                </span>
            </div>
            <h4 className='rotulo-formulario'>Endereço</h4>
            <div className='painel-cadastro-servidor'>
                <span className='grupo-input-label'>
                    <label>Tipo Logradouro </label>
                    <Dropdown 
                        value={props.tipologradouro.get} 
                        options={props.tiposlogradouro} 
                        onChange={(e)=>props.tipologradouro.set(e.value)} 
                        optionLabel="descricao" 
                        className='input-select p-inputtext-sm'
                        style={{height: "40px"}}
                        disabled={!props.ativos}
                    />
                </span>
                <span className='grupo-input-label'>
                    <label>Logradouro </label>
                    <InputText 
                        type="text" 
                        className="p-inputtext-sm texto-maiusculo"
                        value={props.logradouro.get} 
                        onChange={(e) => props.logradouro.set(e.target.value)}
                        style={{height: "40px"}}
                        disabled={!props.ativos}
                    />
                </span>
                <span className='grupo-input-label'>
                    <label>Número</label>
                    <InputText 
                        type="text" 
                        className="p-inputtext-sm texto-maiusculo"
                        value={props.numero.get} 
                        onChange={(e) => props.numero.set(e.target.value)}
                        style={{height: "40px"}}
                        disabled={!props.ativos}
                    />
                </span>

                <span className='grupo-input-label'>
                     <label>Bairro</label>
                    <InputText 
                        type="text" 
                        className="p-inputtext-sm texto-maiusculo"
                        value={props.bairro.get} 
                        onChange={(e) => props.bairro.set(e.target.value)}
                        style={{height: "40px"}}
                        disabled={!props.ativos}
                    />
                </span>
                <span className='grupo-input-label'>
                    <label>Complemento</label>
                    <InputText 
                        type="text" 
                        className="p-inputtext-sm texto-maiusculo"
                        value={props.complemento.get} 
                        onChange={(e) => props.complemento.set(e.target.value)}
                        style={{height: "40px"}}
                        disabled={!props.ativos}
                    />
                </span>
                <span className='grupo-input-label grupo-input-label-2cols'>
                    <div className='grupo-input-label-grad'>
                        <label>CEP</label>
                        <InputMask
                            mask='99999-999' 
                            type="text" 
                            className="p-inputtext-sm"
                            value={props.cep.get} 
                            onChange={(e) => props.cep.set(e.target.value)}
                            style={{height: "40px", width: "100%"}}
                            disabled={!props.ativos}
                        />
                    </div>
                    <div className='grupo-input-label-grad'>
                        <label>Município</label>
                        <SeletorMunicipio
                            get={props.endmunicipio.get}
                            set={props.endmunicipio.set}
                        />
                    </div>
                </span>

            </div>
            <h4 className='rotulo-formulario'>Contatos</h4>
            <div className='painel-cadastro-servidor'>
                <span className='grupo-input-label'>
                    <label>Email Institucional </label>
                    <InputText 
                        type="text" 
                        className="p-inputtext-sm"
                        value={props.email_institucional.get} 
                        onChange={(e) => props.email_institucional.set(e.target.value)}
                        style={{height: "40px"}}
                        disabled={!props.ativos}
                    />
                </span>
                <span className='grupo-input-label'>
                    <label>Email Pessoal </label>
                    <InputText 
                        type="text" 
                        className="p-inputtext"
                        value={props.email_pessoal.get} 
                        onChange={(e) => props.email_pessoal.set(e.target.value)}
                        style={{height: "40px"}}
                        disabled={!props.ativos}
                    />
                </span>
                <span className='grupo-input-label'>
                    <label>Telefone Principal</label>
                    <InputText 
                        type="text" 
                        className="p-inputtext-sm"
                        value={props.telefone_principal.get} 
                        onChange={(e) => props.telefone_principal.set(e.target.value)}
                        style={{height: "40px"}}
                        disabled={!props.ativos}
                    />
                </span>

                <span className='grupo-input-label'>
                    <label>Telefone Alternativo</label> 
                    <InputText 
                        type="text" 
                        className="p-inputtext-sm"
                        value={props.telefone_elternativo.get} 
                        onChange={(e) => props.telefone_elternativo.set(e.target.value)}
                        style={{height: "40px"}}
                        disabled={!props.ativos}
                    />
                </span>
                <span className='grupo-input-label'>
                    <label>Telefone de Emergência </label>
                    <InputText 
                        type="text" 
                        className="p-inputtext-sm"
                        value={props.telefone_emergencia.get} 
                        onChange={(e) => props.telefone_emergencia.set(e.target.value)}
                        style={{height: "40px"}}
                        disabled={!props.ativos}
                    />
                </span>
                <span className='grupo-input-label'>
                    <label>Nome do Contato de Emergência</label>
                    <InputText 
                        type="text" 
                        className="p-inputtext-sm texto-maiusculo"
                        value={props.nome_contato_emergencia.get} 
                        onChange={(e) => props.nome_contato_emergencia.set(e.target.value)}
                        style={{height: "40px"}}
                        disabled={!props.ativos}
                    />
                </span>
            </div>
            <h4 className='rotulo-formulario'>Documentos</h4>
            <div className='painel-cadastro-servidor'>

                <span className='grupo-input-label'>
                    <label>CPF *</label>
                    <InputMask
                        mask='999.999.999-99' 
                        type="text" 
                        className="p-inputtext-sm"
                        value={props.cpf.get} 
                        onChange={(e) => props.cpf.set(e.target.value)}
                        style={props.obrigatorio.get?{borderColor:"#DE4B24", height: "40px"}:{height: "40px"}} 
                        disabled={!props.ativos}
                    />
                </span>
                <span className='grupo-input-label grupo-input-label-2cols'>
                    <div className="grupo-input-label-grad">
                        <label>RG</label>
                        <InputText 
                            type="text" 
                            className="p-inputtext-sm texto-maiusculo"
                            value={props.rg.get} 
                            onChange={(e) => props.rg.set(e.target.value)}
                            style={{height: "40px", width:'100%'}}
                            disabled={!props.ativos}
                        />
                    </div>
                    <div className="grupo-input-label-grad">
                        <label>UF</label>
                        <Dropdown 
                            value={props.rguf.get} 
                            options={ufs} 
                            onChange={(e)=>props.rguf.set(e.value)} 
                            optionLabel="sigla" 
                            className='input-select p-inputtext-sm'
                            style={{height: "40px"}}
                            disabled={!props.ativos}
                        />
                    </div>
                </span>

                <span className='grupo-input-label grupo-input-label-2cols'>
                    <div className="grupo-input-label-grad">
                        <label>RG Data Expedição</label>
                        <SeletorData
                            get={props.rg_expedicao.get}
                            set={props.rg_expedicao.set}
                        />
                    </div>
                    <div className="grupo-input-label-grad">
                        <label>RG Segunda Via?</label>
                            <SelectButton
                                className="p-button-sm" 
                                value={props.rgsegvia.get}
                                optionLabel="name"
                                options={opcoes}
                                onChange={(e) => props.rgsegvia.set(e.value)}
                                disabled={!props.ativos}
                            />
                        
                    </div>
                </span>

                <span className='grupo-input-label'>
                    <label>RG Orgão </label>
                    <InputText 
                        type="text" 
                        className="p-inputtext-sm texto-maiusculo"
                        value={props.rg_orgao.get} 
                        onChange={(e) => props.rg_orgao.set(e.target.value)}
                        style={{height: "40px"}}
                        disabled={!props.ativos}
                    />
                </span>
                <span className='grupo-input-label'>
                    <label>Título de Eleitor </label>
                    <InputText 
                        type="text" 
                        className="p-inputtext-sm"
                        value={props.titulo_eleitor.get} 
                        onChange={(e) => props.titulo_eleitor.set(e.target.value)}
                        style={{height: "40px"}}
                        disabled={!props.ativos}
                    />
                </span>
                <span className='grupo-input-label'>
                    <label>Zona do Título</label>
                    <InputText 
                        type="text" 
                        className="p-inputtext-sm texto-maiusculo"
                        value={props.titulo_eleitor_zona.get} 
                        onChange={(e) => props.titulo_eleitor_zona.set(e.target.value)}
                        style={{height: "40px"}}
                        disabled={!props.ativos}
                    />
                </span>
                <span className='grupo-input-label'>
                    <label>Seção do Título</label>
                    <InputText 
                        type="text" 
                        className="p-inputtext-sm texto-maiusculo"
                        value={props.titulo_eleitor_secao.get} 
                        onChange={(e) => props.titulo_eleitor_secao.set(e.target.value)}
                        style={{height: "40px"}}
                        disabled={!props.ativos}
                    />
                </span>

                <span className='grupo-input-label'>
                    <label>Município do Título</label>
                    <SeletorMunicipio
                        get={props.titulomunicipio.get}
                        set={props.titulomunicipio.set}
                        
                    />
                </span>
                <span className='grupo-input-label'>
                    <label>Reservista</label>
                    <InputText 
                        type="text" 
                        className="p-inputtext-sm"
                        value={props.reservista.get} 
                        onChange={(e) => props.reservista.set(e.target.value)}
                        style={{height: "40px"}}
                        disabled={!props.ativos}
                    />
                </span>
                <span className='grupo-input-label'>
                    <label>Classe Reservista</label>
                    <InputText 
                        type="text" 
                        className="p-inputtext-sm texto-maiusculo"
                        value={props.reservista_classe.get} 
                        onChange={(e) => props.reservista_classe.set(e.target.value)}
                        style={{height: "40px"}}
                        disabled={!props.ativos}
                    />
                </span>

                <span className='grupo-input-label grupo-input-label-2cols'>
                    <div className="grupo-input-label-grad">
                        <label>CNH</label>
                        <InputText 
                            type="text" 
                            className="p-inputtext-sm"
                            value={props.cnh.get} 
                            onChange={(e) => props.cnh.set(e.target.value)}
                            style={{height: "40px"}}
                            disabled={!props.ativos}
                        />
                    </div>
                    <div className="grupo-input-label-grad">
                        <label>Categoria</label>
                        <Dropdown 
                            value={props.categoriacnh.get} 
                            options={catcnh} 
                            onChange={(e)=>props.categoriacnh.set(e.value)} 
                            optionLabel="nome" 
                            className='input-select p-inputtext-sm'
                            style={{height: "40px"}}
                            disabled={!props.ativos}
                        />
                    </div>
                </span>
                <span className='grupo-input-label grupo-input-label-2cols'>
                    <div className="grupo-input-label-grad">
                        <label>Data de Expedição CNH</label>
                        <SeletorData
                            get={props.cnh_expedicao.get}
                            set={props.cnh_expedicao.set}
                        />
                    </div>
                    <div className="grupo-input-label-grad">
                        <label>Data de Validade CNH</label>
                        <SeletorData
                            get={props.cnh_validade.get}
                            set={props.cnh_validade.set}
                        />
                    </div>
                </span>

                <span className='grupo-input-label'>
                    <label>Data da Primeira CNH</label>
                    <SeletorData
                        get={props.cnh_expedicao_primeira.get}
                        set={props.cnh_expedicao_primeira.set}
                    />
                </span>
                <span className='grupo-input-label'>
                    <label>CNH UF</label>
                    <Dropdown 
                        value={props.cnhuf.get} 
                        options={ufs} 
                        onChange={(e)=>props.cnhuf.set(e.value)} 
                        optionLabel="sigla" 
                        className='input-select p-inputtext-sm'
                        style={{height: "40px"}}
                        disabled={!props.ativos}
                    />
                </span>

                <span className='grupo-input-label grupo-input-label-2cols'>
                    <div className='grupo-input-label-grad'>
                        <label>Registro Conselho Profissional </label>
                        <InputText 
                            type="text" 
                            className="p-inputtext-sm texto-maiusculo"
                            value={props.conselho_profissional.get} 
                            onChange={(e) => props.conselho_profissional.set(e.target.value)}
                            style={{height: "40px"}}
                            disabled={!props.ativos}
                        />
                    </div>
                    <div className='grupo-input-label-grad'>
                        <label>UF</label>
                        <Dropdown 
                            value={props.cpuf.get} 
                            options={ufs} 
                            onChange={(e)=>props.cpuf.set(e.value)} 
                            optionLabel="sigla" 
                            className='input-select p-inputtext-sm'
                            style={{height: "40px"}}
                            disabled={!props.ativos}
                        />
                    </div>
                </span>
                <span className='grupo-input-label'>
                    <label>Órgão Emissor Conselho Profissional</label>
                    <InputText 
                        type="text" 
                        className="p-inputtext-sm texto-maiusculo"
                        value={props.conselho_profissional_orgao_emissor.get} 
                        onChange={(e) => props.conselho_profissional_orgao_emissor.set(e.target.value)}
                        style={{height: "40px"}}
                        disabled={!props.ativos}
                    />
                </span>
                <span className='grupo-input-label'>
                    <label>Data de Expedição Conselho Profissional</label>
                    <SeletorData
                        get={props.conselho_profissional_expedicao.get}
                        set={props.conselho_profissional_expedicao.set}
                    />
                </span>

                <span className='grupo-input-label'>
                    <label>Data de Validade Conselho Profissional</label>
                    <SeletorData
                        get={props.conselho_profissional_validade.get}
                        set={props.conselho_profissional_validade.set}
                    />
                </span>
                <span className='grupo-input-label'>
                    <label>PIS/PASEP</label>
                    <InputText 
                        type="text" 
                        className="p-inputtext-sm"
                        value={props.pis.get} 
                        onChange={(e) => props.pis.set(e.target.value)}
                        style={{height: "40px"}}
                        disabled={!props.ativos}
                    />
                </span>
            </div>
            <br/>
        </div>
    );
}

export default DadosPessoaisServidor;