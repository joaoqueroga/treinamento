import React, {useState, useEffect} from "react";
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { FilterMatchMode } from 'primereact/api';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import SeletorServidor from "../../../components/SeletorServidor";
import SeletorPredio from "../../../components/SeletorPredios";
import SeletorLotacao from "../../../components/SeletorLotacao";
import Swal from 'sweetalert2';
import { ProgressSpinner } from 'primereact/progressspinner';
import { BreadCrumb } from 'primereact/breadcrumb';

import api from "../../../config/api";
import { getToken, getUserId } from "../../../config/auth";

function Lotacao() {

    const items = [
        { label: 'Recursos Humanos' },
        { label: 'Lotações' }
    ];
    const home = { icon: 'pi pi-home', url: '/inicio' }
    const [lotacoes, setLotacoes] = useState([]);
    const [modallotacao, setModallotacao] = useState(false);
    const [obrigatorio, setobrigatorio] = useState(false);

    const [modallotacaoeditar, setModallotacaoEditar] = useState(false);
    const [obrigatorioeditar, setobrigatorioEditar] = useState(false);

    const [nome, setNome] = useState('');
    const [sigla, setSigla] = useState('');
    const [responsavel, setResponsavel] = useState(null);
    const [predio, setPredio] = useState(null);
    const [lotacaosup, setLotacaosup] = useState(null);

    const [loading, setLoading] = useState(true);

    const [id, setId] = useState(null);

    useEffect(() => {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId()
        }
        api.post('/rh/lotacoes/',data , config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === 'S'){
                setLotacoes(x.lotacao);
                setLoading(false);
            }else{
                Swal.fire({
                    icon:'error',
                    title:"Erro",
                    text: `${x.motivo}`,
                    confirmButtonText: 'fechar',
                })
            }
        })
    }, []);

    const irBodyTemplate = (rowData) => {
        return(
            <span>
            <Button 
                onClick={()=>seleciona(rowData)}
                icon="pi pi-pencil"
                className="btn-green"
                title="Editar unidade"
            />
            </span>
        )                                    
    }

    const [filters] = useState({
        'sigla': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'descricao': { value: null, matchMode: FilterMatchMode.CONTAINS }
    });

    function seleciona(obj) {
        setId(obj.id_lotacao);
        setNome(obj.descricao)
        setSigla(obj.sigla)
        setResponsavel({"nome": obj.nome_funcionario, "id_funcionario": obj.id_funcionario_responsavel, "matricula": obj.matricula})
        setPredio({"nome": obj.nome_predio, "id_predio": obj.id_predio })
        setLotacaosup({"descricao": obj.lotacao_superior, "id_lotacao": obj.id_lotacao_superior})
        setModallotacaoEditar(true);
    }

    function liparDados() {
        setPredio(null);
        setResponsavel(null);
        setNome("");
        setSigla("");
        setLotacaosup(null);
    }

    function reloadLotacao() {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId()
        }
        api.post('/rh/lotacoes/',data , config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === 'S'){
                setLotacoes(x.lotacao);
            }
        })
    }

    function salvarLotacao() {
        if(nome && responsavel && predio){
            let dados = {
                aplicacao: "SGI",
                id_usuario: getUserId(),
                descricao: nome.toUpperCase(),
                id_funcionario_responsavel: responsavel?responsavel.id_funcionario:"",
                id_lotacao_superior: lotacaosup?lotacaosup.id_lotacao:null,
                id_predio: predio?predio.id_predio:null,
                lotacao_superior: lotacaosup?lotacaosup.descricao:"",
                matricula: "",
                nome_funcionario: responsavel?responsavel.nome:"",
                nome_predio: predio?predio.nome:null,
                sigla: sigla?sigla.toUpperCase():""
            }
            let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
            api.post('/rh/cadastro_lotacao/',dados, config)
            .then((res)=>{
                let x = JSON.parse(res.data);
                if(x.sucesso === "S"){
                    setModallotacao(false);
                    Swal.fire({
                        icon: 'success',
                        title:"Sucesso",
                        text: `Lotação registrada`,
                        confirmButtonText: 'fechar',
                    })
                    reloadLotacao();
                    liparDados();
                }else{
                    setModallotacao(false);
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `${x.motivo}`,
                        confirmButtonText: 'fechar',
                    })
                }
            })

            liparDados();
            setModallotacao(false);
        }else{
            setobrigatorio(true);
        }
    }

    function editarLotacao() {
        if(nome && responsavel && predio){
            let dados = {
                aplicacao: "SGI",
                id_usuario: getUserId(),
                id_lotacao: id,
                descricao: nome.toUpperCase(),
                id_funcionario_responsavel: responsavel?responsavel.id_funcionario:"",
                id_lotacao_superior: lotacaosup?lotacaosup.id_lotacao:null,
                id_predio: predio?predio.id_predio:null,
                lotacao_superior: lotacaosup?lotacaosup.descricao:"",
                matricula: "",
                nome_funcionario: responsavel?responsavel.nome:"",
                nome_predio: predio?predio.nome:null,
                sigla: sigla?sigla.toUpperCase():""
            }

            let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
            api.post('/rh/cadastro_lotacao/',dados, config)
            .then((res)=>{
                let x = JSON.parse(res.data);
                if(x.sucesso === "S"){
                    setModallotacao(false);
                    Swal.fire({
                        icon: 'success',
                        title:"Sucesso",
                        text: `Lotação atualizada`,
                        confirmButtonText: 'fechar',
                    })
                    reloadLotacao();
                    liparDados();
                }else{
                    setModallotacao(false);
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `${x.motivo}`,
                        confirmButtonText: 'fechar',
                    })
                }
            })

            liparDados();
            setModallotacaoEditar(false);
        }else{
            setobrigatorioEditar(true);
        }
    }

    const modalCadastroFooter = () => {
        return (
            <div>
                {obrigatorio?<p><small className="p-error">Informe os capos obrigatórios (*).</small></p>:null}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setModallotacao(false);
                        setobrigatorio(false);
                    }} 
                    className="btn-2" />
                <Button 
                    label="Salvar" 
                    icon="pi pi-check" 
                    onClick={()=>{
                        salvarLotacao();
                    }} 
                    className="btn-1"
                />
            </div>
        );
    }

    const modalEditarFooter = () => {
        return (
            <div>
                {obrigatorioeditar?<p><small className="p-error">Informe os capos obrigatórios (*).</small></p>:null}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setModallotacaoEditar(false);
                        setobrigatorioEditar(false);
                    }} 
                    className="btn-2" />
                <Button 
                    label="Salvar" 
                    icon="pi pi-check" 
                    onClick={()=>{
                        editarLotacao();
                    }} 
                    className="btn-1"
                />
            </div>
        );
    }

    return (
        <div className='view'>
        <div className="view-body">
        {
        loading?
        <div className="loading-pagina" ><ProgressSpinner/>Carregando...</div>
        :
        <div>
            <div className="header">
                <BreadCrumb model={items} home={home}/>
                <h6 className="titulo-header">Lotações</h6>
                <Button 
                    label="Nova Unidade" 
                    icon="pi pi-plus" 
                    className="btn-1"
                    onClick={()=>{setModallotacao(true); liparDados()}}
                />
            </div>
            <div className="top-box">
                <h1>Lotações</h1>
            </div>
            <div className="lotacao-body">
                <div className="dados-lotacoes">

                <DataTable
                    value={lotacoes}
                    size="small"
                    className="tabela-servidores"
                    dataKey="id"
                    filters={filters}
                    filterDisplay="row"
                    emptyMessage="Nada encontrado"
                    scrollable
                    scrollHeight="73vh"
                    selectionMode="single"
                >
                    <Column
                        header="Sigla"
                        field="sigla"
                        filter 
                        filterPlaceholder="Buscar" 
                        style={{ flexGrow: 0, flexBasis: '20%' }}
                        showFilterMenu={false}
                    />
                    <Column
                        header="Nome"
                        field="descricao"
                        filter 
                        filterPlaceholder="Buscar" 
                        style={{ flexGrow: 0, flexBasis: '70%' }}
                        showFilterMenu={false}
                    />
                    <Column 
                        field="botao" 
                        header="Ação" 
                        body={irBodyTemplate} 
                        className="col-centralizado" 
                        style={{ flexGrow: 0, flexBasis: '10%' }}
                    />

                </DataTable>

                </div>

                {/* Para Cadastrar*/}
                <Dialog 
                    header="Nova Unidade de Lotação"
                    visible={modallotacao}
                    style={{ width: '50%' }}
                    footer={modalCadastroFooter}
                    onHide={
                        () =>{
                            setModallotacao(false);
                            setobrigatorio(false);
                        }
                    }
                >
                    <span className="field grupo-input-label">
                        <label>Nome * </label>
                        <InputText 
                            value={nome}
                            onChange={(e)=>setNome(e.target.value)}
                        />
                    </span>
                    <span className="field grupo-input-label">
                        <label>Sigla </label>
                        <InputText 
                            value={sigla}
                            onChange={(e)=>setSigla(e.target.value)}
                        />
                    </span>
                    <span className='field'>
                        <label>Responsável * </label>
                        <SeletorServidor
                            get={responsavel}
                            set={setResponsavel}
                        />
                    </span>
                    <span className='field'>
                        <label>Prédio * </label>
                        <SeletorPredio
                            get={predio}
                            set={setPredio}
                        />
                    </span>
                    <span className='field'>
                        <label>Lotação Superior </label>
                        <SeletorLotacao
                            get={lotacaosup}
                            set={setLotacaosup}
                        />
                    </span>
                </Dialog>

                {/* Para Ver e Editar*/}
                <Dialog 
                    header="Unidade de Lotação"
                    visible={modallotacaoeditar}
                    style={{ width: '50%' }}
                    footer={modalEditarFooter}
                    onHide={
                        () =>{
                            setModallotacaoEditar(false);
                            setobrigatorioEditar(false);
                        }
                    }
                >
                    <span className="field grupo-input-label">
                        <label>Nome * </label>
                        <InputText 
                            value={nome}
                            onChange={(e)=>setNome(e.target.value)}
                        />
                    </span>
                    <span className="field grupo-input-label">
                        <label>Sigla</label>
                        <InputText 
                            value={sigla}
                            onChange={(e)=>setSigla(e.target.value)}
                        />
                    </span>
                    <span className='field'>
                        <label>Responsável *</label>
                        <SeletorServidor
                            get={responsavel}
                            set={setResponsavel}
                        />
                    </span>
                    <span className='field'>
                        <label>Prédio *</label>
                        <SeletorPredio
                            get={predio}
                            set={setPredio}
                        />
                    </span>
                    <span className='field'>
                        <label>Lotação Superior </label>
                        <SeletorLotacao
                            get={lotacaosup}
                            set={setLotacaosup}
                        />
                    </span>
                </Dialog>
                <br/>
            </div>
        </div>
        }
        </div>
        </div>
    );
}

export default Lotacao;