import React, {useState, useEffect} from "react";
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import Swal from 'sweetalert2';
import { ProgressSpinner } from 'primereact/progressspinner';
import { BreadCrumb } from 'primereact/breadcrumb';

import api from "../../../../config/api";
import { getToken, getUserId } from "../../../../config/auth";

function ClassesServidor(props) {

    const objeto_nome = "Classe"

    const items = [
        { label: 'Recursos Humanos' },
        { label: 'Parâmetros' },
        { label: objeto_nome }
    ];
    const home = { icon: 'pi pi-home', url: '/inicio' }

    const [dados, setDados] = useState([]);
    const [modal, setmodal] = useState(false);
    const [obrigatorio, setobrigatorio] = useState(false);

    const [modaleditar, setmodalEditar] = useState(false);
    const [obrigatorioeditar, setobrigatorioEditar] = useState(false);

    const [loading, setLoading] = useState(true);

    const [descricao, setdescricao] = useState('');
    const [id, setId] = useState(null);


    useEffect(() => {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data_aux = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
        }
        api.post('/rh/listar_classe/',data_aux , config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            try {
                setDados(x);
                setLoading(false);
            } catch (error) {
                Swal.fire({
                    icon:'error',
                    title:"Erro",
                    text: `${error}`,
                    confirmButtonText: 'fechar',
                })
            }
        })
    }, []);
    const irBodyTemplate = (rowData) => {
        return(
            <span>
            <Button 
                onClick={()=>seleciona(rowData)}
                icon="pi pi-pencil"
                className="btn-green"
                title="Editar unidade"
            />
            </span>
        )                                    
    }
    function seleciona(obj) {
        setId(obj.id_classe);
        setdescricao(obj.descricao);
        setmodalEditar(true);
    }

    function liparDados() {
        setId("");
        setdescricao("");
    }

    function reload() {
        
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data_aux = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
        }
        api.get('/rh/listar_classe/',data_aux , config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            try {
                setDados(x);
                setLoading(false);
            } catch (error) {
                Swal.fire({
                    icon:'error',
                    title:"Erro",
                    text: `${error}`,
                    confirmButtonText: 'fechar',
                })
            }
        })
    };

    function salvar() {
        if(descricao){
            let dados = {
                "aplicacao": "SGI",
                "id_usuario": getUserId(), 
                "descricao": descricao.toUpperCase(),
            }
            let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
            api.post('/rh/salvar_classe/',dados, config)
            .then((res)=>{
                let x = JSON.parse(res.data);
                if(x.sucesso === "S"){
                    setmodal(false);
                    Swal.fire({
                        icon: 'success',
                        title:"Sucesso",
                        text: `${objeto_nome} Cadastrada`,
                        confirmButtonText: 'fechar',
                    })
                    reload();
                    liparDados();
                }else{
                    setmodal(false);
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `${x.motivo}`,
                        confirmButtonText: 'fechar',
                    })
                }
            })

            liparDados();
            setmodal(false);
        }else{
            setobrigatorio(true);
        }
    }

    function atualizar() {
        if(descricao){
            let dados = {
                "aplicacao": "SGI",
                "id_usuario": getUserId(), 
                "id_classe": id,
                "descricao": descricao.toUpperCase()
            }

            let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
            api.post('/rh/salvar_classe/',dados, config)
            .then((res)=>{
                let x = JSON.parse(res.data);
                if(x.sucesso === "S"){
                    setmodal(false);
                    Swal.fire({
                        icon: 'success',
                        title:"Sucesso",
                        text: `${objeto_nome} atualizada`,
                        confirmButtonText: 'fechar',
                    })
                    reload();
                    liparDados();
                }else{
                    setmodal(false);
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `${x.motivo}`,
                        confirmButtonText: 'fechar',
                    })
                }
            })
            liparDados();
            setmodalEditar(false);
        }else{
            setobrigatorioEditar(true);
        }
    }

    const modalCadastroFooter = () => {
        return (
            <div>
                {obrigatorio?<p><small className="p-error">Informe os capos obrigatórios (*).</small></p>:null}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setmodal(false);
                        setobrigatorio(false);
                    }} 
                    className="btn-2" />
                <Button 
                    label="Salvar" 
                    icon="pi pi-check" 
                    onClick={()=>{
                        salvar();
                    }} 
                    className="btn-1"
                />
            </div>
        );
    }

    const modalEditarFooter = () => {
        return (
            <div>
                {obrigatorioeditar?<p><small className="p-error">Informe os capos obrigatórios (*).</small></p>:null}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setmodalEditar(false);
                        setobrigatorioEditar(false);
                    }} 
                    className="btn-2" />
                <Button 
                    label="Salvar" 
                    icon="pi pi-check" 
                    onClick={()=>{
                        atualizar();
                    }} 
                    className="btn-1"
                />
            </div>
        );
    }

    return (
        <div className='view'>
        <div className="view-body">
        {
        loading?
        <div className="loading-pagina" ><ProgressSpinner/>Carregando...</div>
        :
        <div>
            <div className="header">
                <BreadCrumb model={items} home={home}/>
                <h6 className="titulo-header">{objeto_nome}</h6>
                <Button 
                    label={`Nova ${objeto_nome}`} 
                    icon="pi pi-plus" 
                    className="btn-1"
                    onClick={()=>{setmodal(true); liparDados()}}
                />
            </div>
            <div className="lotacao-body">
                <div className="dados-lotacoes">

                <DataTable
                    value={dados}
                    size="small"
                    className="tabela-servidores"
                    dataKey="id"
                    emptyMessage="Nada encontrado"
                    scrollable
                    scrollHeight="77vh"
                    selectionMode="single"
                >
                    <Column
                        header="Id"
                        field="id_classe"
                        style={{ flexGrow: 0, flexBasis: '20%' }}
                    />
                    <Column
                        header="Descrição"
                        field="descricao"
                        style={{ flexGrow: 0, flexBasis: '70%' }}
                    />
                    <Column 
                        field="botao" 
                        header="Ação" 
                        body={irBodyTemplate} 
                        className="col-acao" 
                        style={{ flexGrow: 0, flexBasis: '10%' }}
                    />
                </DataTable>

                </div>

                {/* Para Cadastrar*/}
                <Dialog 
                    header={`Nova ${objeto_nome}`}
                    visible={modal}
                    style={{ width: '50%' }}
                    footer={modalCadastroFooter}
                    onHide={
                        () =>{
                            setmodal(false);
                            setobrigatorio(false);
                        }
                    }
                >
                    <span className="field grupo-input-label">
                        <label>Descrição *</label>
                        <InputText 
                            className="block texto-maiusculo"
                            style={{height: "40px"}}
                            value={descricao}
                            onChange={(e)=>setdescricao(e.target.value)}
                        />
                    </span>
                </Dialog>

                {/* Para Ver e Editar*/}
                <Dialog 
                    header={`Editar ${objeto_nome}`}
                    visible={modaleditar}
                    style={{ width: '50%' }}
                    footer={modalEditarFooter}
                    onHide={
                        () =>{
                            setmodalEditar(false);
                            setobrigatorioEditar(false);
                        }
                    }
                >
                    <span className="field grupo-input-label">
                        <label>Descrição *</label>
                        <InputText 
                            className="block texto-maiusculo"
                            style={{height: "40px"}}
                            value={descricao}
                            onChange={(e)=>setdescricao(e.target.value)}
                        />
                    </span>
                </Dialog>
                <br/>
            </div>
        </div>
        }
        </div>
    </div>
    );
}

export default ClassesServidor;