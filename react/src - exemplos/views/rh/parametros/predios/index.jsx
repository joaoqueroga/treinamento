import React, {useState, useEffect} from "react";
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { FilterMatchMode } from 'primereact/api';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import Swal from 'sweetalert2';
import { ProgressSpinner } from 'primereact/progressspinner';
import { BreadCrumb } from 'primereact/breadcrumb';

import api from "../../../../config/api";
import { getToken, getUserId } from "../../../../config/auth";

function RhPredios() {

    const items = [
        { label: 'Recursos Humanos' },
        { label: 'Parâmetros' },
        { label: 'Prédios' }
    ];
    const home = { icon: 'pi pi-home', url: '/inicio' }

    const [dados, setDados] = useState([]);
    const [modal, setmodal] = useState(false);
    const [obrigatorio, setobrigatorio] = useState(false);

    const [modaleditar, setmodalEditar] = useState(false);
    const [obrigatorioeditar, setobrigatorioEditar] = useState(false);

    const [loading, setLoading] = useState(true);

    // dados do objeto
    const [id, setId] = useState(null);
    const [comarca, setComarca] = useState('');
    const [endereco, setEndereco] = useState('');
    const [nome, setNome] = useState('');

    useEffect(() => {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId()
        }
        api.post('/rh/predios/', data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === 'S'){
                setDados(x.predio);
                setLoading(false);
            }else{
                Swal.fire({
                    icon:'error',
                    title:"Erro",
                    text: `${x.motivo}`,
                    confirmButtonText: 'fechar',
                })
            }
        })
    }, []);

    const irBodyTemplate = (rowData) => {
        return(
            <span>
            <Button 
                onClick={()=>seleciona(rowData)}
                icon="pi pi-pencil"
                className="btn-green"
                title="Editar função"
            />
            </span>
        )                                    
    }
    const [filters] = useState({
        'comarca': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'nome': { value: null, matchMode: FilterMatchMode.CONTAINS }
    });

    function seleciona(obj) {
        setId(obj.id_predio);
        setComarca(obj.comarca);
        setEndereco(obj.endereco);
        setNome(obj.nome);
        setmodalEditar(true);
    }

    function liparDados() {
        setId("");
        setComarca("");
        setNome("");
        setEndereco("");
    }

    function reload() {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId()
        }
        api.post('/rh/predios/',data , config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === 'S'){
                setDados(x.predio);
            }
        })
    }

    function salvar() {
        if(nome && comarca && endereco){
            let dados = {
                "aplicacao": "SGI",
                "id_usuario": getUserId(),
                "nome": nome.toUpperCase(),
                "endereco": endereco.toUpperCase(),
                "comarca": comarca.toUpperCase()
            }
            let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
            api.post('/rh/cadastro_predio/',dados, config)
            .then((res)=>{
                let x = JSON.parse(res.data);
                if(x.sucesso === "S"){
                    setmodal(false);
                    Swal.fire({
                        icon: 'success',
                        title:"Sucesso",
                        text: `Prédio Cadastrado`,
                        confirmButtonText: 'fechar',
                    })
                    reload();
                    liparDados();
                }else{
                    setmodal(false);
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `${x.motivo}`,
                        confirmButtonText: 'fechar',
                    })
                }
            })

            liparDados();
            setmodal(false);
        }else{
            setobrigatorio(true);
        }
    }

    function atualizar() {
        if(nome && comarca && endereco){
            let dados = {
                "aplicacao": "SGI",
                "id_usuario": getUserId(),
                "id_predio": id,
                "nome": nome.toUpperCase(),
                "endereco": endereco.toUpperCase(),
                "comarca": comarca.toUpperCase()
            }

            let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
            api.post('/rh/cadastro_predio/',dados, config)
            .then((res)=>{
                let x = JSON.parse(res.data);
                if(x.sucesso === "S"){
                    setmodal(false);
                    Swal.fire({
                        icon: 'success',
                        title:"Sucesso",
                        text: `Prédio atualizado`,
                        confirmButtonText: 'fechar',
                    })
                    reload();
                    liparDados();
                }else{
                    setmodal(false);
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `${x.motivo}`,
                        confirmButtonText: 'fechar',
                    })
                }
            })

            liparDados();
            setmodalEditar(false);
        }else{
            setobrigatorioEditar(true);
        }
    }

    const modalCadastroFooter = () => {
        return (
            <div>
                {obrigatorio?<p><small className="p-error">Informe os capos obrigatórios (*).</small></p>:null}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setmodal(false);
                        setobrigatorio(false);
                    }} 
                    className="btn-2" />
                <Button 
                    label="Salvar" 
                    icon="pi pi-check" 
                    onClick={()=>{
                        salvar();
                    }} 
                    className="btn-1"
                />
            </div>
        );
    }

    const modalEditarFooter = () => {
        return (
            <div>
                {obrigatorioeditar?<p><small className="p-error">Informe os capos obrigatórios (*).</small></p>:null}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setmodalEditar(false);
                        setobrigatorioEditar(false);
                    }} 
                    className="btn-2" />
                <Button 
                    label="Salvar" 
                    icon="pi pi-check" 
                    onClick={()=>{
                        atualizar();
                    }} 
                    className="btn-1"
                />
            </div>
        );
    }

    return (
        <div className='view'>
        <div className="view-body">
        {
        loading?
        <div className="loading-pagina" ><ProgressSpinner/>Carregando...</div>
        :
        <div>
            <div className="header">
                <BreadCrumb model={items} home={home}/>
                <h6 className="titulo-header">Prédios</h6>
                <Button 
                    label="Novo Prédio" 
                    icon="pi pi-plus" 
                    className="btn-1"
                    onClick={()=>{setmodal(true); liparDados()}}
                />
            </div>
            <div className="top-box">
                <h1>Prédios</h1>
            </div>
            <div className="lotacao-body">
                <div className="dados-lotacoes">

                <DataTable
                    value={dados}
                    size="small"
                    className="tabela-servidores"
                    dataKey="id"
                    filters={filters}
                    filterDisplay="row"
                    emptyMessage="Nada encontrado"
                    scrollable
                    scrollHeight="73vh"
                    selectionMode="single"
                >
                    <Column
                        header="Comarca"
                        field="comarca"
                        filter 
                        filterPlaceholder="Buscar" 
                        style={{ flexGrow: 0, flexBasis: '30%' }}
                        showFilterMenu={false}
                    />
                    <Column
                        header="Prédio"
                        field="nome"
                        filter 
                        filterPlaceholder="Buscar" 
                        style={{ flexGrow: 0, flexBasis: '70%' }}
                        showFilterMenu={false}
                    />
                    <Column 
                        field="botao" 
                        header="Ação" 
                        body={irBodyTemplate} 
                        className="col-centralizado" 
                        style={{ flexGrow: 0, flexBasis: '10%' }}
                    />
                </DataTable>

                </div>

                {/* Para Cadastrar*/}
                <Dialog 
                    header="Novo prédio"
                    visible={modal}
                    style={{ width: '50%' }}
                    footer={modalCadastroFooter}
                    onHide={
                        () =>{
                            setmodal(false);
                            setobrigatorio(false);
                        }
                    }
                >
                    <span className="field grupo-input-label">
                        <label>Nome *</label>
                        <InputText 
                            value={nome}
                            onChange={(e)=>setNome(e.target.value)}
                        />
                    </span>
                    <span className="field grupo-input-label">
                        <label>Comarca *</label>
                        <InputText 
                            value={comarca}
                            onChange={(e)=>setComarca(e.target.value)}
                        />
                    </span>
                    <span className="field grupo-input-label">
                        <label>Endereço *</label>
                        <InputText 
                            value={endereco}
                            onChange={(e)=>setEndereco(e.target.value)}
                        />
                    </span>
                </Dialog>

                {/* Para Ver e Editar*/}
                <Dialog 
                    header="Editar prédio"
                    visible={modaleditar}
                    style={{ width: '50%' }}
                    footer={modalEditarFooter}
                    onHide={
                        () =>{
                            setmodalEditar(false);
                            setobrigatorioEditar(false);
                        }
                    }
                >
                    <span className="field grupo-input-label">
                        <label>Nome *</label>
                        <InputText 
                            value={nome}
                            onChange={(e)=>setNome(e.target.value)}
                        />
                    </span>
                    <span className="field grupo-input-label">
                        <label>Comarca *</label>
                        <InputText 
                            value={comarca}
                            onChange={(e)=>setComarca(e.target.value)}
                        />
                    </span>
                    <span className="field grupo-input-label">
                        <label>Endereço *</label>
                        <InputText 
                            value={endereco}
                            onChange={(e)=>setEndereco(e.target.value)}
                        />
                    </span>
                </Dialog>
                <br/>
            </div>
        </div>
        }
        </div>
    </div>
    );
}

export default RhPredios;