import React, {useState, useEffect} from "react";
import './style.scss';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { FilterMatchMode } from 'primereact/api';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import Swal from 'sweetalert2';
import { ProgressSpinner } from 'primereact/progressspinner';
import { BreadCrumb } from 'primereact/breadcrumb';

import api from "../../../../config/api";
import { getToken, getUserId } from "../../../../config/auth";

function RhFuncoes() {

    const items = [
        { label: 'Recursos Humanos' },
        { label: 'Parâmetros' },
        { label: 'Funções' }
    ];
    const home = { icon: 'pi pi-home', url: '/inicio' }
    const [dados, setDados] = useState([]);
    const [modal, setmodal] = useState(false);
    const [obrigatorio, setobrigatorio] = useState(false);

    const [modaleditar, setmodalEditar] = useState(false);
    const [obrigatorioeditar, setobrigatorioEditar] = useState(false);

    const [loading, setLoading] = useState(true);

    const [descricao, setdescricao] = useState('');
    const [id, setId] = useState(null);

    useEffect(() => {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId()
        }
        api.post('/rh/funcoes/',data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === 'S'){
                setDados(x.funcao);
                setLoading(false);
            }else{
                Swal.fire({
                    icon:'error',
                    title:"Erro",
                    text: `${x.motivo}`,
                    confirmButtonText: 'fechar',
                })
            }
        })
    }, []);

    const irBodyTemplate = (rowData) => {
        return(
            <span>
            <Button 
                onClick={()=>seleciona(rowData)}
                icon="pi pi-pencil"
                className="btn-green"
                title="Editar função"
            />
            </span>
        )                                    
    }
    const [filters] = useState({
        'id_funcao': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'descricao': { value: null, matchMode: FilterMatchMode.CONTAINS }
    });

    function seleciona(obj) {
        setId(obj.id_funcao);
        setdescricao(obj.descricao);
        setmodalEditar(true);
    }

    function liparDados() {
        setId("");
        setdescricao("");
    }

    function reload() {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId()
        }
        api.post('/rh/funcoes/',data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === 'S'){
                setDados(x.funcao);
            }
        })
    }

    function salvar() {
        if(descricao){
            let dados = {
                "aplicacao": "SGI",
                "id_usuario": getUserId(),
                "descricao": descricao.toUpperCase()
            }
            let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
            api.post('/rh/cadastro_funcao/',dados, config)
            .then((res)=>{
                let x = JSON.parse(res.data);
                if(x.sucesso === "S"){
                    setmodal(false);
                    Swal.fire({
                        icon: 'success',
                        title:"Sucesso",
                        text: `Função Cadastrada`,
                        confirmButtonText: 'fechar',
                    })
                    reload();
                    liparDados();
                }else{
                    setmodal(false);
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `${x.motivo}`,
                        confirmButtonText: 'fechar',
                    })
                }
            })

            liparDados();
            setmodal(false);
        }else{
            setobrigatorio(true);
        }
    }

    function atualizar() {
        if(descricao){
            let dados = {
                "aplicacao": "SGI",
                "id_usuario": getUserId(),
                "id_funcao": id,
                "descricao": descricao.toUpperCase()
            }

            let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
            api.post('/rh/cadastro_funcao/',dados, config)
            .then((res)=>{
                let x = JSON.parse(res.data);
                if(x.sucesso === "S"){
                    setmodal(false);
                    Swal.fire({
                        icon: 'success',
                        title:"Sucesso",
                        text: `Função atualizada`,
                        confirmButtonText: 'fechar',
                    })
                    reload();
                    liparDados();
                }else{
                    setmodal(false);
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `${x.motivo}`,
                        confirmButtonText: 'fechar',
                    })
                }
            })

            liparDados();
            setmodalEditar(false);
        }else{
            setobrigatorioEditar(true);
        }
    }

    const modalCadastroFooter = () => {
        return (
            <div>
                {obrigatorio?<p><small className="p-error">Informe os capos obrigatórios (*).</small></p>:null}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setmodal(false);
                        setobrigatorio(false);
                    }} 
                    className="btn-2" />
                <Button 
                    label="Salvar" 
                    icon="pi pi-check" 
                    onClick={()=>{
                        salvar();
                    }} 
                    className="btn-1"
                />
            </div>
        );
    }

    const modalEditarFooter = () => {
        return (
            <div>
                {obrigatorioeditar?<p><small className="p-error">Informe os capos obrigatórios (*).</small></p>:null}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setmodalEditar(false);
                        setobrigatorioEditar(false);
                    }} 
                    className="btn-2" />
                <Button 
                    label="Salvar" 
                    icon="pi pi-check" 
                    onClick={()=>{
                        atualizar();
                    }} 
                    className="btn-1"
                />
            </div>
        );
    }

    return (
        <div className='view'>
        <div className="view-body">
        {
        loading?
        <div className="loading-pagina" ><ProgressSpinner/>Carregando...</div>
        :
        <div>
            <div className="header">
                <BreadCrumb model={items} home={home}/>
                <h6 className="titulo-header">Funções</h6>
                <Button 
                    label="Nova Função" 
                    icon="pi pi-plus" 
                    className="btn-1"
                    onClick={()=>{setmodal(true); liparDados()}}
                />
            </div>
            <div className="top-box">
                <h1>Funções</h1>
            </div>
            <div className="lotacao-body">
                <div className="dados-lotacoes">
                <DataTable
                    value={dados}
                    size="small"
                    className="tabela-servidores"
                    dataKey="id"
                    filters={filters}
                    filterDisplay="row"
                    emptyMessage="Nada encontrado"
                    scrollable
                    scrollHeight="73vh"
                    selectionMode="single"
                >
                    <Column
                        header="Id"
                        field="id_funcao"
                        filter 
                        filterPlaceholder="Buscar" 
                        style={{ flexGrow: 0, flexBasis: '20%' }}
                        showFilterMenu={false}
                    />
                    <Column
                        header="Descrição"
                        field="descricao"
                        filter 
                        filterPlaceholder="Buscar" 
                        style={{ flexGrow: 0, flexBasis: '80%' }}
                        showFilterMenu={false}
                    />
                    <Column 
                        field="botao" 
                        header="Ação" 
                        body={irBodyTemplate} 
                        className="col-centralizado" 
                        style={{ flexGrow: 0, flexBasis: '10%' }}
                    />
                </DataTable>

                </div>

                {/* Para Cadastrar*/}
                <Dialog 
                    header="Nova Função"
                    visible={modal}
                    style={{ width: '50%' }}
                    footer={modalCadastroFooter}
                    onHide={
                        () =>{
                            setmodal(false);
                            setobrigatorio(false);
                        }
                    }
                >
                    <div className="field grupo-input-label">
                        <label>Descrição *</label>
                        <InputText 
                            value={descricao}
                            onChange={(e)=>setdescricao(e.target.value)}
                        />
                    </div>          
                </Dialog>

                {/* Para Ver e Editar*/}
                <Dialog 
                    header="Ediar Função"
                    visible={modaleditar}
                    style={{ width: '50%' }}
                    footer={modalEditarFooter}
                    onHide={
                        () =>{
                            setmodalEditar(false);
                            setobrigatorioEditar(false);
                        }
                    }
                >
                <div className="field grupo-input-label">
                    <label>Descrição *</label>
                    <InputText 
                        value={descricao}
                        onChange={(e)=>setdescricao(e.target.value)}
                    />
                </div>

                </Dialog>
                <br/>
            </div>
        </div>
        }
        </div>
    </div>
    );
}

export default RhFuncoes;