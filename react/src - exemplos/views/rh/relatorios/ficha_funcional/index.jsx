import React, {useState} from "react";
import '../style.scss';
import { Dialog } from 'primereact/dialog';
import { Button } from 'primereact/button';
import { InputSwitch } from 'primereact/inputswitch';
import jsPDF from 'jspdf';
import autoTable from 'jspdf-autotable';
import logo from '../../../../images/logo_h.png'
import TelaSpinner from "../../../../components/spinner";
import api from "../../../../config/api";
import { getToken, getUserId } from "../../../../config/auth";
import SeletorServidor from "../../../../components/SeletorServidor";

function RelatorioFichaFuncional() {

    const [servidor, setServidor] = useState(null);
    const [display, setDisplay] = useState(false);
    const [displayPDF, setDisplayPDF] = useState(false);
    const [checkAnotacoes, setCheckAnotacoes] = useState(true);
    const [obrigatorio, setobrigatorio] = useState(false);

    const user = sessionStorage.getItem("nome");

    function baixarpdf(id) {
        setDisplayPDF(true);
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            "id_funcionario": Number(id),
            "tipo_listagem": "COMPLETA"
        }
        api.post('/rh/servidor/',data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === "S"){
                let f = x.funcionarios[0];
                let a = x.funcionarios[0].anotacoes;
                let d = x.funcionarios[0].documentos_diversos;
                let historico = x.funcionarios[0].anotacoes.length > 0? x.funcionarios[0].anotacoes[0].data_inclusao: "SEM REGISTROS"
                setDisplayPDF(false);
                generatePDF(f,a,d, historico);
            }
        })
    }

    function coluna( x, y, tam, rotulo, texto, doc) {
        doc.setFont('helvetica', 'bold');
        doc.text(x, y, `${rotulo}:`);
        doc.setFont('helvetica', 'normal');
        doc.text(x+tam, y, `${texto?texto:""}`);
    }

    function rotulo(y, texto, doc){
        let acrecimo = 8;
        doc.setFont('helvetica', 'bold');
        doc.text( 40, y+acrecimo, `${texto}`);
        y+=(5+acrecimo);
        doc.line(40,y,800,y);
        return y + 5;
    }

    function generatePDF  (s, anotacoes, d, historico){
        
        let data = new Date();
        let dia_x = data.getDate() < 10 ? `0${data.getDate()}` : data.getDate();
        let mes_x = (data.getMonth()+1) < 10 ? `0${data.getMonth()+1}` : data.getMonth()+1
        let dia = `${dia_x}/${mes_x}/${data.getFullYear()}`;
        let hora = data.toLocaleTimeString();

       
        let doc = new jsPDF('l', 'pt');
        doc.addImage(logo, 'PNG', 40, 20, 155, 30 );
        
        doc.setFont('helvetica', 'bold');
        doc.setFontSize(18);
        doc.setTextColor(20,71,88);
        doc.text(637, 40, 'FICHA FUNCIONAL');

        doc.setLineWidth(3.0); 
        doc.setTextColor(0,0,0);

        doc.setFont('helvetica', 'bold');
        doc.setFontSize(18);
        doc.text(40, 85, `${s.nome}`);

        doc.setFontSize(20);
        doc.text(665, 80, `${s.matricula}`);

        doc.line(40,90,800,90);
        doc.rect(640, 55, 160, 35);

        doc.setFontSize(12);

        //coluna 1: x=50 , coluna 2 x=400

        let y = 110;
        let espaco = 18;

        coluna(50, y, 80, "Nome Social", s.nome_social ,doc);
        coluna(400, y, 30, "Mãe", s.nome_mae ,doc);

        y+=espaco; //pula a linha

        coluna(50, y,  80, "Nascimento", s.nascimento ,doc);
        coluna(400, y, 28, "Pai", s.nome_pai ,doc);

        y+=espaco;
        coluna(50, y,  83, "Naturalidade", `${s.naturalidade}-${s.uf}` ,doc);
        coluna(400, y, 35, "Sexo", s.sexo ,doc);

        y+=espaco;
        coluna(50, y,  81, "Estado Civil", s.descricao_estado_civil ,doc);
        coluna(400, y, 55, "Cônjuge", s.nome_conjuge ,doc);

        y+=espaco;
        coluna(50, y,  140, "Possui União Estável?", s.possui_uniao_estavel?"Sim":"Não" ,doc);
        coluna(400, y,  100, "Tipo Sanguíneo", `${s.grupo_sanguineo?s.grupo_sanguineo:''} ${s.fator_rh?s.fator_rh:''}` ,doc);


        y+=espaco;
        y = rotulo(y, 'DADOS FUNCIONAIS', doc);

        y+=espaco;
        coluna(50, y,  60, "Lotação", s.descricao_lotacao ,doc);
        y+=espaco;
        coluna(50, y,  160, "Responsável Pela Lotação", s.nome_responsavel_lotacao ,doc);
        y+=espaco;
        coluna(50, y,  50, "Cargo", s.descricao_cargo ,doc);
        y+=espaco;
        coluna(50, y,  100, "Servidor Ativo?", s.ativo?"Sim":"Não" ,doc);
        y+=espaco;
        coluna(50, y,  115, "Grau de Instrução", s.descricao_grau_instrucao ,doc);
        coluna(400, y, 110, "Tipo de Servidor", s.descricao_tipo_servidor ,doc);
        y+=espaco;
        coluna(50, y,  180, "Data de Entrada em Exercício", s.data_inicio_atividades ,doc);
        coluna(400, y, 135, "Data referência férias", s.ferias_referencia ,doc);
        y+=espaco;
        coluna(400, y,  120, "Data de Nomeação", s.ingresso ,doc);
        


        y+=espaco;
        y = rotulo(y, 'CONTATOS', doc);

        y+=espaco;
        coluna(50, y,  120, "Email Institucional", s.email_institucional ,doc);
        coluna(400, y, 90, "Email Pessoal", s.email_pessoal ,doc);
        y+=espaco;
        coluna(50, y,  118, "Telefone Principal", s.telefone_principal ,doc);
        coluna(400, y, 125, "Telefone Alternativo", s.telefone_elternativo ,doc);
        y+=espaco;
        coluna(50, y,  150, "Telefone de Emergência", s.telefone_emergencia ,doc);
        coluna(400, y, 180, "Nome Contato de Emergência", s.nome_contato_emergencia ,doc);

        y+=espaco;
        y = rotulo(y, 'ENDEREÇO', doc);
        y+=espaco;
        coluna(50, y,  80, "Logradouro", s.logradouro ,doc);
        y+=espaco;
        coluna(50, y,   35, "Tipo", s.descricao_tipo_logradouro ,doc);
        coluna(400, y,  90, "Complemento", s.complemento ,doc);
        y+=espaco;
        coluna(50, y,   55, "Número", s.numero ,doc);
        coluna(400, y,  45, "Bairro", s.bairro ,doc);
        y+=espaco;
        coluna(50, y,   35, "CEP", s.cep ,doc);
        coluna(400, y,   70, "Município", s.municipio ,doc);
        


        doc.addPage();
        y = 40;


        y+=espaco;
        y = rotulo(y, 'DOCUMENTOS', doc);
        y+=espaco;
        coluna(50, y,   34, "CPF", s.cpf ,doc);
        coluna(400, y,  28, "RG", s.rg ,doc);
        y+=espaco;
        coluna(50, y,  88, "RG Expedição", s.rg_expedicao ,doc);
        coluna(400, y,  50, "RG UF", s.rg_uf ,doc);
        y+=espaco;
        coluna(50, y,   65, "RG Orgão", s.rg_orgao ,doc);
        coluna(400, y,  110, "RG Segunda Via?", s.rg_segunda_via?"Sim":"Não" ,doc);
        y+=espaco;
        coluna(50, y,  100, "Título de Eleitor", s.titulo_eleitor ,doc);
        coluna(400, y,  90, "Zona do Título", s.titulo_eleitor_zona ,doc);
        y+=espaco;
        coluna(50, y,   100, "Seção do Título", s.titulo_eleitor_secao ,doc);
        coluna(400, y,  120, "Município do Título", s.titulo_eleitor_municipio ,doc);
        y+=espaco;
        coluna(50, y,   70, "Reservista", s.reservista ,doc);
        coluna(400, y,  110, "Classe Reservista", s.reservista_classe ,doc);
        y+=espaco;
        coluna(50, y,  33, "CNH", s.cnh ,doc);
        coluna(400, y,  95, "Categoria CNH", s.cnh_categoria ,doc);
        y+=espaco;
        coluna(50, y,   90, "Validade CNH", s.cnh_validade ,doc);
        coluna(400, y,  90, "Primeira CNH", s.cnh_expedicao_primeira ,doc);
        y+=espaco;
        coluna(50, y,  100, "Expedição CNH", s.cnh_expedicao ,doc);
        coluna(400, y,  55, "CNH UF", s.cnh_uf ,doc);
        y+=espaco;
        coluna(50, y,   190, "Registro Conselho Profissional", s.conselho_profissional ,doc);
        coluna(400, y,  190, "Emissor Conselho Profissional", s.conselho_profissional_orgao_emissor ,doc);
        y+=espaco;
        coluna(50, y,   200, "Expedição Conselho Profissional", s.conselho_profissional_expedicao ,doc);
        coluna(400, y,  190, "Validade Conselho Profissional", s.conselho_profissional_validade ,doc);
        y+=espaco;
        coluna(50, y,  155, "Conselho Profissional UF", s.conselho_profissional_uf ,doc);
        coluna(400, y,  70, "PIS/PASEP", s.pis ,doc);

        if(checkAnotacoes){
            y+=espaco;
            autoTable(doc, {
                startY: y,
                styles: {fontSize: 15, cellPadding: 5, halign: 'justify' , textColor: "#000", lineColor:"#000", fillColor:"#fff"},
                head: [['ANOTAÇÕES FUNCIONAIS']],
            })

            autoTable(doc, ({
                body: anotacoes,
                styles: {fontSize: 11, cellPadding: 10, halign: 'justify' , textColor: "#000", lineColor:"#000", fillColor:"#fff"},
                columns: [
                    { header: 'TIPO', dataKey: 'descricao_tipo_documento' },
                    { header: 'NÚMERO', dataKey: 'documento_numero_ano' },
                    { header: 'DATA', dataKey: 'data' },
                    { header: 'DESCRIÇÃO', dataKey: 'descricao' },
                ],
            }))
            autoTable(doc, {
                styles: {fontSize: 15, cellPadding: 5, halign: 'justify' , textColor: "#000", lineColor:"#000", fillColor:"#fff"},
                head: [['DOCUMENTOS DIVERSOS']],
            })
            autoTable(doc, ({
                body: d,
                styles: {fontSize: 11, cellPadding: 10, halign: 'justify' , textColor: "#000", lineColor:"#000", fillColor:"#fff"},
                columns: [
                    { header: 'TIPO', dataKey: 'tipo_documento_diversos_descricao' },
                    { header: 'NÚMERO', dataKey: 'numero' },
                    { header: 'DATA', dataKey: 'data' },
                    { header: 'DESCRIÇÃO', dataKey: 'descricao' },
                ],
            }))
        }

        
        //rodapé da pagina
        let pageCount = doc.internal.getNumberOfPages()
        doc.setFontSize(7)
        doc.setFont('helvetica', 'bold');
        for (var i = 1; i <= pageCount; i++) {
            doc.setPage(i)
            doc.text(`ÚLTIMO REGISTRO DE ANOTAÇÕES E DOCUMENTOS DIVERSOS: ${historico}`, doc.internal.pageSize.width / 2, 560, { align: 'center'});
            doc.text('DEFENSORIA PÚBLICA DO ESTADO DO AMAZONAS - AV. ANDRE ARAUJO, 679, ALEIXO  CNPJ: 19.421.427/0001-91', doc.internal.pageSize.width / 2, 570, { align: 'center'});
            doc.text(`Emitido por ${user} em ${dia} ${hora} - Página  ${String(i)} de ${String(pageCount)}`, doc.internal.pageSize.width / 2, 580, { align: 'center'});
        }

        //doc.save(`${s.nome} ${new Date().toLocaleDateString()} ${new Date().toLocaleTimeString()}.pdf`)
        window.open(doc.output('bloburl', { filename: `ficha_funcional.pdf` }), '_blank');
    } 

    // ---------

    function gerar(){
        if(servidor !== null){
            setDisplay(false);
            setobrigatorio(false);
            baixarpdf(servidor.id_funcionario);
            setServidor(null);
        }else{
            setobrigatorio(true);
        }
    }

    const modalFooter = () => {
        return (
            <div>
                {obrigatorio?<p><small className="p-error">Informe os capos obrigatórios (*).</small></p>:null}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setDisplay(false);
                        setobrigatorio(false);
                    }} 
                    className="btn-2" />
                <Button 
                    label="Gerar Relatório" 
                    icon="pi pi-check" 
                    onClick={()=>{
                        gerar();
                    }} 
                    className="btn-1"
                />
            </div>
        );
    }

    return (
        <div>
            <Button 
                label="Ficha funcional" 
                icon="pi pi-file-pdf" 
                className="btn-2"
                onClick={()=>setDisplay(true)}
            />
            <Dialog 
                header="Ficha funcional"
                visible={display}
                style={{ width: '50%' }}
                footer={modalFooter}
                onHide={
                    () =>{
                        setDisplay(false);
                        setobrigatorio(false);
                    }
                }
            >
                <span>
                    <label>Servidor *</label>
                    <SeletorServidor
                        get={servidor}
                        set={setServidor}
                    />
                </span>
                <span>
                    <label>Com anotações? </label>
                    <span className='input-toggle-rotulo'>
                        <InputSwitch
                            checked={checkAnotacoes}
                            onChange={(e) => setCheckAnotacoes(e.value)} 
                        />
                        <p>{checkAnotacoes?"Sim":"Não"}</p>
                    </span>
                </span>
            </Dialog>

            <Dialog
                visible={displayPDF} 
                style={{ width: '50vw' }}
                closable={false}
            >
                <TelaSpinner tamanho={100} texto={"Gerando Documento"}/>
            </Dialog>
        </div>
    );
}

export default RelatorioFichaFuncional;