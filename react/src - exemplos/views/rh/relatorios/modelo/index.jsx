import React, {useState} from "react";
import './style.scss';
import { Dialog } from 'primereact/dialog';
import { Button } from 'primereact/button';
import jsPDF from 'jspdf';
import logo from '../../../../images/logo_h.png'
import TelaSpinner from "../../../../components/spinner";

function RelatorioModelo() {
    const [display_pdf, setdisplay_pdf] = useState(false);
    const user = sessionStorage.getItem("nome");

    function baixar_pdf() {
        setdisplay_pdf(true);
        //buscar os dados em uma procedure e passar para gerar_pdf();
        gerar_pdf();
    }

    function cabecalho(doc, x_titulo, titulo) {
        doc.addImage(logo, 'PNG', 40, 20, 155, 30 );
        doc.setFont('helvetica', 'bold');
        doc.setFontSize(18);
        doc.setTextColor(20,71,88);
        doc.text(x_titulo, 40, titulo);
    }

    function rodape(doc) {
        let data = new Date();
        let dia_x = data.getDate() < 10 ? `0${data.getDate()}` : data.getDate();
        let mes_x = (data.getMonth()+1) < 10 ? `0${data.getMonth()+1}` : data.getMonth()+1
        let dia = `${dia_x}/${mes_x}/${data.getFullYear()}`;
        let hora = data.toLocaleTimeString();

        let pageCount = doc.internal.getNumberOfPages();
        doc.setFontSize(7)
        doc.setFont('helvetica', 'bold');
        for (var i = 1; i <= pageCount; i++) {
            doc.setPage(i)
            doc.text('DEFENSORIA PÚBLICA DO ESTADO DO AMAZONAS - AV. ANDRE ARAUJO, 679, ALEIXO  CNPJ: 19.421.427/0001-91', doc.internal.pageSize.width / 2, 570, { align: 'center'});
            doc.text(`Emitido por ${user} em ${dia} ${hora} - Página  ${String(i)} de ${String(pageCount)}`, doc.internal.pageSize.width / 2, 580, { align: 'center'});
        }
    }

    function rotulo(doc, texto) {
        doc.autoTable({ startY: doc.autoTable.previous.finalY+5, styles: {fontStyle : 'bold'},  theme: 'striped' , body: [[texto]]})
    }

    function tabela_campos(doc,dados) {
        doc.autoTable({ startY: doc.autoTable.previous.finalY+5 , styles: { cellPadding: 2}, theme: 'plain', body: dados})
    }

    function gerar_pdf  (){
        let doc = new jsPDF('l', 'pt');
        cabecalho(doc, 580, "MODELO DE RELATÓRIO")

        //rotulo inicio do relatorio para referencia
        doc.autoTable({ startY: 60, styles: {fontStyle : 'bold'},  body:[['DADOS PESSOAIS']]})
        tabela_campos(doc, [
            ['NOME: joão', 'IDADE: 12'],
            ['NOME: Francisco pereira da silva', `IDADE: ${32}`],
        ])

        rotulo(doc, 'DADOS PESSOAIS');
        tabela_campos(doc, [
            ['NOME: joão', 'IDADE: 12'],
            ['DOCUMENTO: Francisco pereira da silva', `IDADE: ${32}`],
        ])

        rodape(doc);
        window.open(doc.output('bloburl', { filename: `ficha_funcional.pdf` }), '_blank');
        setdisplay_pdf(false);
    } 

    return (
        <div>
            <Button 
                label="Modelo" 
                icon="pi pi-file-pdf" 
                className="btn-2"
                onClick={()=>baixar_pdf()}
            />
            <Dialog
                visible={display_pdf} 
                style={{ width: '50vw' }}
                closable={false}
            >
                <TelaSpinner tamanho={100} texto={"Gerando Documento"}/>
            </Dialog>
        </div>
    );
}

export default RelatorioModelo;