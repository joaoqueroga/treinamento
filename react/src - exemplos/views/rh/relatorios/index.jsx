import React from "react";
import './style.scss';
import RelatorioModelo from "./modelo";
import RelatorioModeloFicha from "./modelo/modelo_ficha";
import RelatorioFichaFuncional from "./ficha_funcional";
import RelatorioServidoresLotacao from "./servidores_lotacao";
import { BreadCrumb } from 'primereact/breadcrumb';

function RelatoriosRh() {

    const items = [
        { label: 'Recursos Humanos' },
        { label: 'Relatórios' }
    ];
    const home = { icon: 'pi pi-home', url: '/inicio' }

    return (
        <div className='view'>
            <div className="view-body">
                <div className="header">
                    <BreadCrumb model={items} home={home}/>
                    <h6 className="titulo-header">Relatórios</h6>
                </div>
                <div className="top-box">
                    <h1>Relatórios</h1>
                </div>
                <div className="relatorios-main">
                    {/* <RelatorioModeloFicha/> */}
                    <RelatorioFichaFuncional/>
                    <RelatorioServidoresLotacao/>
                </div>
            </div>
        </div>
    );
}

export default RelatoriosRh;