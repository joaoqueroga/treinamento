import React, { useState, useEffect, useRef } from 'react';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { BreadCrumb } from 'primereact/breadcrumb';
import { FileUpload } from 'primereact/fileupload';
import { Badge } from 'primereact/badge';
import { Button } from 'primereact/button'
import { Tooltip } from 'primereact/tooltip';
import { Tag } from 'primereact/tag';
import { InputText } from 'primereact/inputtext';
import Swal from 'sweetalert2';
import descPlanilhas from './descricaoPlanilhas';

import api from '../../../config/api';
import { getToken } from '../../../config/auth';
import { getUserId } from '../../../config/auth';

import '../../../index.scss';
import './style.scss';

function MeritocraciaImportarPlanilhas() {
  const fileUPload = useRef(null);
  const [planilhas, setPlanilhas] = useState([]);
  const [situacaoPlanilhas, setSituacaoPlanilhas] = useState([]);
  const [planilhaSelecionada, setPlanilhaSelecionada] = useState('');
  const [coordenacao, setCoordenacao] = useState('');
  const itensBreadcrumbs = [{ label: 'Meritocracia' }, { label: 'Importar planilha de dados gerais' }];
  const home = { icon: 'pi pi-home', url: '/inicio' };
  const urlBackend = api.defaults.baseURL;


  function carregaDadosTabela() {
    api.get(`/meritocracia/listar-situacao-atual`)
      .then((response) => {
        let vSituacaoPlanilhas = response.data[0];
        setSituacaoPlanilhas(vSituacaoPlanilhas);
        const vPlanilhas = [];
        descPlanilhas.forEach(
          array => vPlanilhas.push({ ...array, "status": vSituacaoPlanilhas[array.keyPlan] })
        );
        setPlanilhas(vPlanilhas);
      })
      .catch((err) => { Swal.fire("Erro de conexão", `Houve um erro na conexão com o servidor. Por favor, tente mais tarde. motivo: ${err}`, "error"); });
  }


  function readFileAsync(file) {
    return new Promise((resolve, reject) => {
      let reader = new FileReader();
      reader.onload = () => {
        resolve(reader.result);
      }
      reader.onerror = reject;
      reader.readAsText(file);
    })
  }

  async function importarDados(files, rowData) {
    let response;
    let conteudoArq1, conteudoArq2; // Arq2 só é utilizado para planilha de 'Avaliação da chefia imediata'
    const extensoesValidas = ['txt', 'csv'];
    const extensao = files[0].name.split('.').pop();
    setPlanilhaSelecionada(rowData);

    if (!extensoesValidas.includes(extensao)) {
      Swal.fire('Falha na importação', 'Formato de arquio inválido. Selecione arquivos no formato .cvs ou .txt.', "error");
      return;
    }

    try {
      conteudoArq1 = await readFileAsync(files[0]);
      conteudoArq1 = conteudoArq1.trim();
      if (planilhaSelecionada.keyPlan === 'avaliacao_chefia_imediata') {
        if (files.length !== 2) {
          Swal.fire("Falha na importação", "Para Avaliação da Chefia Imediata são necessários dois arquivos. Verifique se foram selecionados dois arquivos.", "error");
          return;
        }
        conteudoArq2 = await readFileAsync(files[1]);
        conteudoArq2 = conteudoArq2.trim();
      }
    } catch (err) {
      Swal.fire("Falha na importação", 'Houve um erro ao carregar o arquivo.', "error");
      return;
    }

    if ((conteudoArq1 === '') || ((planilhaSelecionada.keyPlan === 'avaliacao_chefia_imediata') && (conteudoArq2 === ''))) {
      Swal.fire("Falha na importação", "O arquivo selecionado está vazio. Verifique.", "error");
      return;
    }

    Swal.fire({
      title: "Enviando dados",
      text: "Por favor aguarde",
      allowOutsideClick: false,
      didOpen: () => Swal.showLoading()
    })

    try {
      if (planilhaSelecionada.keyPlan === 'avaliacao_chefia_imediata')
        response = await api.post(`meritocracia/avaliacao-chefia-imediata/`, { texto1: conteudoArq1.trim(), texto2: conteudoArq2.trim() });
      else if (planilhaSelecionada.keyPlan === 'reunioes')
        response = await api.post(`meritocracia/reunioes/`, { texto: conteudoArq1.trim(), titulo: coordenacao });
      else
        response = await api.post(`meritocracia/${planilhaSelecionada.requestUrl}`, { texto: conteudoArq1 });

      Swal.close();

      if (response.data.sucesso === "S") {
        Swal.fire("Dados importados", "Os dados do arquivo foram importados com sucesso.", "success");
        carregaDadosTabela();
      }
      else
        Swal.fire("Falha na importação", `${response.data.motivo}`, "error");
    } catch (e) {
      Swal.fire("Falha na importação", `Houve um erro na conexão com o banco de dados. Por favor, tente mais tarde.<br>Detalhes: ${e.message}`, "error");
    }
  }

  const chooseOptions = { title: 'escolher', icon: 'pi pi-fw pi-file', iconOnly: true, className: 'custom-choose-btn p-button-rounded p-button-outlined', style: { height: '35px', width: '35px', marginTop: '5px', marginLeft: '10px' } };
  const uploadOptions = { icon: 'pi pi-fw pi-cloud-upload', iconOnly: true, className: 'custom-upload-btn p-button-success p-button-rounded p-button-outlined', style: { height: '35px', width: '35px' } };
  const cancelOptions = { icon: 'pi pi-fw pi-times', iconOnly: true, className: 'custom-cancel-btn p-button-danger  p-button-rounded p-button-outlined', style: { height: '35px', width: '35px' } };

  const itemTemplate = (file, props) => {
    return (
      <div className="flex align-items-center flex-wrap" style={{ display: 'flex', margin: '0px' }}>
        <div className="flex align-items-center">
          <span className="flex flex-column text-left ml-3">{file.name}</span>
        </div>
        <Tag value={props.formatSize} severity="warning" style={{ marginLeft: '2rem' }} />
        <Button
          visible={planilhaSelecionada.keyPlan === 'avaliacao_chefia_imediata'}
          className="p-button-outlined p-button-rounded p-button-danger ml-auto"
          style={{ margin: '200px', width: '22px', height: '10px' }}
          type="button"
          title='excluir' icon="pi pi-times"
          onClick={props.onRemove}
        />
        {
          planilhaSelecionada.keyPlan === 'reunioes'
            ? <InputText
              value={coordenacao}
              onChange={(e) => setCoordenacao(e.target.value)}
              name='coordenacao' id='coordenacao'
              placeholder='Insira a coordenação aqui'
              style={{ margin: '5px 0px 0px 0px', width: '100%' }}
            />
            : null
        }
      </div>
    );
  };

  function renderDatatableButtons(rowData) {
    const typePlan = rowData.keyPlan;
    return (
      <div>
        <Tooltip target=".custom-choose-btn" content="selecionar arquivo" position="bottom" style={{ fontSize: '13.5px' }} />
        <Tooltip target=".custom-upload-btn" content="enviar arquivo" position="bottom" style={{ fontSize: '13.5px' }} showOnDisabled={true} />
        <Tooltip target=".custom-cancel-btn" content="limpar seleção" position="bottom" style={{ fontSize: '13.5px' }} showOnDisabled={true} />
        <FileUpload
          id='up'
          ref={fileUPload}
          mode="advanced"
          url={`${urlBackend}/meritocracia/${rowData.requestUrl}`}
          accept="text/*"
          maxFileSize={10000000000}
          onUpload={e => importarDados(e.files, rowData)}
          chooseOptions={chooseOptions}
          uploadOptions={uploadOptions}
          cancelOptions={cancelOptions}
          multiple={typePlan === 'avaliacao_chefia_imediata'}
          headerStyle={{ padding: '0px 0px 5px 0px', margin: '0px', border: 'none', background: 'none' }}
          contentStyle={{ padding: '0px', marginTop: '0px', border: 'none', background: 'none' }}
          itemTemplate={itemTemplate}
          onSelect={() => setPlanilhaSelecionada(rowData)}
          onClear={() => setCoordenacao('')}
        />
      </div>)
  }

  function formataStatusPlanilha(rowData) {
    if (rowData.status)
      return <Badge style={{ background: 'rgb(8, 150, 8)' }} value="importada"></Badge>;
    else
      return <Badge style={{ background: 'rgb(150, 8, 8)' }} value="não importada"></Badge>;
  }

  function formataNomePlanilha(rowData) {
    if (rowData.keyPlan === 'avaliacao_chefia_imediata')
      return (
        <div style={{ margin: '9px 0px' }}>
          <p style={{ margin: '0px' }}>Avaliação da chefia imediata</p>
          <p style={{ fontSize: '11.5px' }}>são necessários dois arquivos</p>
        </div>
      );
    else
      return rowData.namePlan;
  }

  async function consolidarPlanilhas() {
    Swal.fire({
      title: 'Deseja prosseguir ?',
      text: 'Caso prossiga, os dados da planilha consolidada serão substituídos.',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'prosseguir',
      cancelButtonText: 'cancelar',
      cancelButtonColor: '#d33',
      confirmButtonColor: '#3085d6',
    })
      .then((result) => {
        if (result.isConfirmed) {
          Swal.fire({
            title: "Processando mapa",
            text: "Por favor aguarde...",
            allowOutsideClick: false,
            didOpen: async () => {
              try {
                const response = await api.post(`/meritocracia/processar-mapa/`);
                Swal.close()
                if (response.data.sucesso === 'S')
                  Swal.fire('Dados consolidados', 'Os dados das planilhas foram consolidados com sucesso.', 'success');
                else
                  Swal.fire('Falha na consolidação dos dados', `Houve um erro na consolidação dos dados.<br>Detalhes: ${response.data.motivo}`, "error");
              }
              catch (e) {
                Swal.fire('Falha na conexão', 'Não foi possível realizar a conexão com o servidor. Por favor, tente mais tarde.', "error");
              }
              Swal.showLoading();
              try {
                const response = await api.post(`/meritocracia/processar-mapa/`);
                Swal.close()
                if (response.data.sucesso === 'S')
                  Swal.fire('Dados consolidados', 'Os dados das planilhas foram consolidados com sucesso.', 'success');
                else
                  Swal.fire('Falha na consolidação dos dados', `Houve um erro na consolidação dos dados.<br>Detalhes: ${response.data.motivo}`, "error");
              }
              catch (e) {
                Swal.fire('Falha na conexão', 'Não foi possível realizar a conexão com o servidor. Por favor, tente mais tarde.', "error");
              }
            }
          })
        }
      });
  }

  useEffect(() => {
    carregaDadosTabela();
  }, []);

  return (
    <div className='view'>
      <div className='view-body'>
        <div className="header" >
          <BreadCrumb model={itensBreadcrumbs} home={home} />
          <h6 className="titulo-header">Importar dados de Meritocracia</h6>
        </div>
        <div className='meritocracia-table'>
          
          <DataTable
            value={planilhas}
          >
            <Column body={formataNomePlanilha} header='Planilha' bodyStyle={{ width: '400px' }} />
            <Column header='Status' body={formataStatusPlanilha} bodyStyle={{}} />
            <Column body={renderDatatableButtons} header="Ações" bodyStyle={{ width: '350px' }} />
          </DataTable>
        <div style={{ width: '91%', textAlign: 'left' }}>
          <Button label='Consolidar planilhas' icon="pi pi-file-excel" iconPos='right' onClick={consolidarPlanilhas} className="btn-1" />
        </div>
        </div>
      </div>

    </div>
  );
}
export default MeritocraciaImportarPlanilhas;
