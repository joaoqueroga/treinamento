import React, { useState, useEffect, useRef } from 'react';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { BreadCrumb } from 'primereact/breadcrumb';
import { Badge } from 'primereact/badge';
import { Button } from 'primereact/button'
import descPlanilhas from './descricaoPlanilhas';
import { BiCloudUpload } from "react-icons/bi";
import { ProgressSpinner } from 'primereact/progressspinner';
import { Toast } from 'primereact/toast';

import { Dialog } from 'primereact/dialog';
import api from '../../../config/api';
import { getToken } from '../../../config/auth';
import { getUserId } from '../../../config/auth';
import Alertas from '../../../utils/alertas';
import { useNavigate } from 'react-router-dom';

import * as XLSX from 'xlsx';

import '../../../index.scss';
import './style.scss';

function MeritocraciaImportarPlanilhas() {
  const navigate = useNavigate();

  const toast = useRef(null);
  
  const itensBreadcrumbs = [{ label: 'Meritocracia' }, { label: 'Importar planilha de dados gerais' }];
  const home = { icon: 'pi pi-home', url: '/inicio' };
  const urlBackend = api.defaults.baseURL;


  const [reload, setreload] = useState(false);

  const [obrigatorio, setObrigatorio] = useState(false);
  const [textoimportacao, setTextoimportacao] = useState('');
  const [modalupload, setModalupload] = useState(false);
  const [modalverdados, setModalverdados] = useState(false);
  const [situacaoPlanilhas, setSituacaoPlanilhas] = useState([]);
  const [planilhas, setPlanilhas] = useState([]);
  const [arquivo, setArquivo] = useState(null);
  const [arquivo2, setArquivo2] = useState(null);
  const [tipo_importacao, setTipo_importacao] = useState('');
  const [url_importacao, seturl_importacao] = useState('');
  const [carregar, setcarregar] = useState(false);
  const [processar, setprocessar] = useState(false);
  const [array_procedure, setArray_procedure] = useState([]);
  const [array_procedure2, setArray_procedure2] = useState([]);
  const [novo_envio, setNovo_envio] = useState(false);

  const [arquivo_duplo, setArquivo_duplo] = useState(false);


  function carregaDadosTabela() {
    let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
    let data = {
      "aplicacao":"SGI",
      "id_usuario":getUserId()
    }
    api.post(`/meritocracia/listar-situacao-atual/`, data, config)
      .then((res) => {
        try {
          let x = JSON.parse(res.data);
          let vSituacaoPlanilhas = x[0];
          setSituacaoPlanilhas(vSituacaoPlanilhas);
          const vPlanilhas = [];
          descPlanilhas.forEach(
            array => vPlanilhas.push({ ...array, "status": vSituacaoPlanilhas[array.keyPlan], "pode_importar": false })
          );

          let ordem_planilhas = [4,5,6,1,2,3,0,7,8,9] /// configuração - index da ordem de upload das planilhas
          
          let planilhas_ordem = [0,0,0,0,0,0,0,0,0,0];
          for(let i = 0; i < ordem_planilhas.length; i++){ // distribuir na ordem
            planilhas_ordem[ordem_planilhas[i]] = vPlanilhas[i];
          }


          planilhas_ordem[0].pode_importar = true;
          for(let i = 1; i < planilhas_ordem.length; i++){
            if(planilhas_ordem[i-1].status){
              planilhas_ordem[i].pode_importar = true;
            }
          }
          
          setPlanilhas(planilhas_ordem);
        } catch (error) {
          Alertas.erro(`ERRO: ${error}`);
        }
      }).catch((err) => {
        Alertas.erro(`ERRO: ${err}`);
      });
  }

  useEffect(() => {
    carregaDadosTabela();
  }, [reload]);

  const showWarn = (msg) => {
    toast.current.show({severity:'warn', summary: 'Atenção', detail: msg, life: 5000});
  }


  function abrirImportacao(data) {
    if(data.keyPlan === 'cursos'){
      setNovo_envio(true);
    }
    setTipo_importacao(data.keyPlan);
    setTextoimportacao(data.namePlan);
    seturl_importacao(data.requestUrl);
    if(data.keyPlan === 'avaliacao_chefia_imediata'){
      setArquivo_duplo(true);
    }
    setModalupload(true)
  }

  function renderDatatableButtons(rowData){
    return(
        <span>
        <Button 
            onClick={()=>{abrirImportacao(rowData)}}
            icon="pi pi-cloud-upload"
            className="btn-darkblue"
            title="Importação"
            disabled={!rowData.pode_importar}
        />
        </span>
    )                             
  }

  function liparDados() {
    setObrigatorio(false);
    setArquivo_duplo(false);
    seturl_importacao('');
    setTipo_importacao('');
    setNovo_envio(false);
    setArquivo(null);
    setArquivo2(null);
  }

  // 7 colunas NOME	- CARGO	- MATRICULA	- DATA	- EVENTO -	DESCRIÇÃO DO EVENTO -	ATUAÇÃO
  function extrair_cursos(dados) {
    return new Promise( 
        resolve =>{
          let aux_array_procedure = [];
          for(let i = 0; i < dados.length; i ++){
            let array_linha = dados[i];
            if(array_linha.length === 7){ // se a quantidade de colunas correspondem a estrutura esperada
              let item = {
                "nome": array_linha[0] || null,
                "cargo": array_linha[1] || null,
                "matricula": array_linha[2] || null,
                "data": array_linha[3] || null,
                "evento": array_linha[4] || null,
                "descricao_evento": array_linha[5] || null,
                "atuacao": array_linha[6] || null
              }
              aux_array_procedure.push(item);
            }else{
              showWarn("A planilha não está uniforme na quantidade de colunas, algumas linhas serão ignoradas.")
            }
          }
          resolve (aux_array_procedure);
        }
    )
  }

  function extrair(dados, qtd_colunas) {
    return new Promise( 
        resolve =>{
          let string_procedure = '';
          for(let i = 0; i < dados.length; i ++){
            let array_linha = dados[i];
            if(array_linha.length === qtd_colunas){ // se a quantidade de colunas correspondem a estrutura esperada
              array_linha.forEach((item)=>{
                let a = item.replaceAll(";","")
                            .replaceAll("“","")
                            .replaceAll("”","")
                            .replaceAll("'","")
                            .replaceAll("’","");
                string_procedure += `${a};`;
              })
            }else{
              showWarn("A planilha não está uniforme na quantidade de colunas, algumas linhas serão ignoradas.")
            }
          }
          let resultado = string_procedure.substring(0, string_procedure.length - 1);
          resolve (resultado);
        }
    )
  }

  

  async function importarXlsx(file, qual_arquivo){
    setprocessar(true);
    if(qual_arquivo === 1){
      setArquivo2(file);
    }else{
      setArquivo(file);
    }
    const reader = new FileReader();
    reader.onload = (e) => {
      const data = new Uint8Array(e.target.result);
      const workbook = XLSX.read(data, { type: 'array' });
      const sheetName = workbook.SheetNames[0];
      const worksheet = workbook.Sheets[sheetName];
      const csvData = XLSX.utils.sheet_to_csv(worksheet, { FS: '¨' });
      const lines = csvData.split('\n');

      // converte para arra e tira as linhas em branco
      const jsonData = lines
        .map((line) => line.split('¨'))
        .filter((line) => line.some((cell) => cell.trim() !== ''));
    
      jsonData.shift(); /// remove cabecalho

      if(tipo_importacao === 'cursos'){
        extrair_cursos(jsonData).then((res)=>{
          setprocessar(false)
          setArray_procedure(res);
        })
      }else if(tipo_importacao === 'sr'){
        extrair(jsonData, 25).then((res)=>{
          setprocessar(false)
          setArray_procedure(res);
        })
      }else if(tipo_importacao === 'gestao_pessoas'){
        extrair(jsonData, 7).then((res)=>{
          setprocessar(false)
          setArray_procedure(res);
        })
      }else if(tipo_importacao === 'programas'){
        extrair(jsonData, 5).then((res)=>{
          setprocessar(false)
          setArray_procedure(res);
        })
      }else if(tipo_importacao === 'depara_indicador'){
        extrair(jsonData, 6).then((res)=>{
          setprocessar(false)
          setArray_procedure(res);
        })
      }else if(tipo_importacao === 'depara_orgao'){
        extrair(jsonData, 6).then((res)=>{
          setprocessar(false)
          setArray_procedure(res);
        })
      }else if(tipo_importacao === 'metas_para_sistema'){
        extrair(jsonData, 5).then((res)=>{
          setprocessar(false)
          setArray_procedure(res);
        })
      }else if(tipo_importacao === 'funcionario_unidade'){
        extrair(jsonData, 3).then((res)=>{
          setprocessar(false)
          setArray_procedure(res);
        })
      }else if(tipo_importacao === 'reunioes'){
        extrair(jsonData, 6).then((res)=>{
          setprocessar(false)
          setArray_procedure(res);
        })
      }else if(tipo_importacao === 'avaliacao_chefia_imediata'){

        if(qual_arquivo === 0){ //planilha 1
          extrair(jsonData, 4).then((res)=>{
            setprocessar(false)
            setArray_procedure(res);
          })
        }else if(qual_arquivo === 1){ //planilha 2
          extrair(jsonData, 27).then((res)=>{
            setprocessar(false)
            setArray_procedure2(res);
          })
        }
      }      
    };

    reader.readAsArrayBuffer(file);
  }

  function enviarPlanilha() {
    if(arquivo){
      if(array_procedure.length > 0){
      setcarregar(true);
      let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
      let data = {
        "aplicacao":"SGI",
        "id_usuario":getUserId(),
        "planilha": array_procedure
      }
      api.post(`/meritocracia/${url_importacao}`, data, config)
        .then((res) => {
          try {
            let x = JSON.parse(res.data);
            if(x.sucesso == "S"){
              Alertas.sucesso("Planilha importada");
              setModalupload(false);
              liparDados();
              setreload(!reload);
            }else{
              Alertas.erro(x.motivo);
            }
          } catch (error) {
            Alertas.erro(`ERRO: ${error}`);
          }
          setcarregar(false);
        }).catch((err) => {
          setcarregar(false);
          Alertas.erro(`ERRO: ${err}`);
        });
      }else{
        Alertas.erro(`A planilha não gerou dados para a importação.`);
      }
    }else{
      setObrigatorio(true);
    }
  }

  function enviarPlanilhaAntiga() {
    if(arquivo){
      if(array_procedure.length > 0){
      setcarregar(true);
      let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
      let data = {
        "aplicacao":"SGI",
        "id_usuario":getUserId(),
        "texto": array_procedure,
        "titulo": ""
      }
      api.post(`/meritocracia/${url_importacao}`, data, config)
        .then((res) => {
          try {
            let x = JSON.parse(res.data);
            if(x.Sucesso == "S"){
              Alertas.sucesso("Planilha importada");
              setModalupload(false);
              liparDados();
              setreload(!reload);
            }else{
              Alertas.erro(x.Motivo);
            }
          } catch (error) {
            Alertas.erro(`ERRO: ${error}`);
          }
          setcarregar(false);
        }).catch((err) => {
          setcarregar(false);
          Alertas.erro(`ERRO: ${err}`);
        });
      }else{
        Alertas.erro(`A planilha não gerou dados para a importação.`);
      }
    }else{
      setObrigatorio(true);
    }
  }

  function enviarPlanilhaDupla() {
    if(arquivo && arquivo2){
      if(array_procedure.length > 0 && array_procedure2.length > 0){
      setcarregar(true);
      let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
      let data = {
        "aplicacao":"SGI",
        "id_usuario":getUserId(),
        "texto1": array_procedure,
        "texto2": array_procedure2,
      }
      api.post(`/meritocracia/${url_importacao}`, data, config)
        .then((res) => {
          try {
            let x = JSON.parse(res.data);
            if(x.Sucesso == "S"){
              Alertas.sucesso("Planilha importada");
              setModalupload(false);
              liparDados();
              setreload(!reload);
            }else{
              Alertas.erro(x.Motivo);
            }
          } catch (error) {
            Alertas.erro(`ERRO: ${error}`);
          }
          setcarregar(false);
        }).catch((err) => {
          setcarregar(false);
          Alertas.erro(`ERRO: ${err}`);
        });
      }else{
        Alertas.erro(`A planilha não gerou dados para a importação.`);
      }
    }else{
      setObrigatorio(true);
    }
  }

  const renderFooterimportacao = () => {
    return (
        <div>
            {obrigatorio?<p><small className="p-error">Obrigatório anexar todos os arquivos</small></p>:null}
            {
              arquivo?
              <Button className='btn-2' label="Ver" icon="pi pi-eye" onClick={() =>{setModalverdados(true)}}/>
              :null
            }
            {
              novo_envio?
              <Button className='btn-1' label="Enviar" icon="pi pi-cloud-upload" onClick={() =>{enviarPlanilha()}}/>
              :
                arquivo_duplo?
                  <Button className='btn-1' label="Enviar Arquivos" icon="pi pi-cloud-upload" onClick={() =>{enviarPlanilhaDupla()}}/>
                :
                  <Button className='btn-1' label="Enviar" icon="pi pi-cloud-upload" onClick={() =>{enviarPlanilhaAntiga()}}/>
            }
            
        </div>
    );
  }

  function formataStatusPlanilha(rowData) {
    if (rowData.status)
      return <Badge style={{ background: 'rgb(8, 150, 8)' }} value="importada"></Badge>;
    else
      return <Badge style={{ background: 'rgb(150, 8, 8)' }} value="não importada"></Badge>;
  }

  function formataNomePlanilha(rowData) {
    if (rowData.keyPlan === 'avaliacao_chefia_imediata')
      return (
        <div style={{ margin: '9px 0px' }}>
          <p style={{ margin: '0px' }}>Avaliação da chefia imediata</p>
          <p style={{ fontSize: '11.5px' }}>são necessários dois arquivos</p>
        </div>
      );
    else
      return rowData.namePlan;
  }
  
  return (
    <div className='view'>
      <div className='view-body'>
        <div className="header" >
          <BreadCrumb model={itensBreadcrumbs} home={home} />
          <h6 className="titulo-header">Importar dados de Meritocracia</h6>
        </div>
        <Toast ref={toast}/>
        <div className='meritocracia-table'>
          <DataTable
            value={planilhas}
          >
            <Column body={formataNomePlanilha} header='Planilhas' style={{ flexGrow: 0, flexBasis: '40%' }}/>
            <Column header='Status' body={formataStatusPlanilha} style={{ flexGrow: 0, flexBasis: '40%' }}/>
            {/* <Column header='Data do ultimo envio' field="data" style={{ flexGrow: 0, flexBasis: '20%' }}/> */}
            <Column body={renderDatatableButtons} header="Ação" style={{ flexGrow: 0, flexBasis: '20%' }}/>
          </DataTable>

          <Button 
            label='Voltar' 
            icon="pi pi-arrow-left" 
            iconPos='left' 
            onClick={()=>navigate("/meritocracia/trimestres")} 
            className="btn-1" 
          />
        
          <Dialog 
            header={`Carregar planilha - ${textoimportacao}`} 
            visible={modalupload} 
            style={{ width: '50vw' }} 
            onHide={()=>{setModalupload(false); liparDados()}}
            footer={renderFooterimportacao}
          >
            <div id='importar-csv-meritocracia'>
            <label className='label-file-servidor ' style={{height: '40px'}}>
              <BiCloudUpload size={20}/>
              <input
                  type="file" 
                  className='input-files' 
                  accept='.xlsx'
                  onChange={(e)=>{importarXlsx(e.target.files[0], 0)}}
              />
            </label>
            <span>
              {arquivo?arquivo.name:"Nenhum arquivo selecionado"}
            </span>
            </div>
            <br />
            {
              arquivo_duplo?
              <div>
                <div id='importar-csv-meritocracia'>
                <label className='label-file-servidor ' style={{height: '40px'}}>
                  <BiCloudUpload size={20}/>
                  <input
                      type="file" 
                      className='input-files' 
                      accept='.xlsx'
                      onChange={(e)=>{importarXlsx(e.target.files[0], 1)}}
                  />
                </label>
                <span>
                  {arquivo2?arquivo2.name:"Nenhum arquivo selecionado"}
                </span>
                </div>
              </div>
              :null
            }
          </Dialog>

          <Dialog 
            header={`Planilha de ${textoimportacao}`} 
            visible={modalverdados} 
            style={{ width: '90vw' }} 
            onHide={() => setModalverdados(false)}
          >
            {
              tipo_importacao === "cursos"?
              <DataTable
                value={array_procedure} 
                responsiveLayout="scroll"
                scrollable 
                scrollHeight="60vh"
                paginator
                paginatorTemplate="CurrentPageReport FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink"
                currentPageReportTemplate="{last} de {totalRecords}" 
                rows={10}
                size="small"
                className='tabela-ver-dados-meritocracia'
              >
                <Column 
                    field="nome" 
                    header="Nome"
                />
                <Column 
                    field="cargo" 
                    header="Cargo"
                />
                <Column 
                    field="matricula" 
                    header="Matrícula"
                />
                <Column 
                    field="data" 
                    header="Data"
                />
                <Column 
                    field="evento" 
                    header="Evento"
                />
                <Column 
                    field="descricao_evento" 
                    header="Descrição"
                />
                <Column 
                    field="atuacao" 
                    header="Atuação"
                />
              </DataTable>
              :null
            }
          </Dialog>

          <Dialog 
            visible={carregar} 
            style={{ width: '40vw' }}
            closable={false}
          >
          <div id='modal-spinner-importacao'>
            <ProgressSpinner size={12} />
            <p>{`Importando dados de ${textoimportacao}`}</p>
          </div>
          </Dialog>

          <Dialog 
            visible={processar} 
            style={{ width: '40vw' }}
            closable={false}
          >
          <div id='modal-spinner-importacao'>
            <ProgressSpinner size={12} />
            <p>{`Processando dados ...`}</p>
          </div>
          </Dialog>

          {/*
          <div style={{ width: '91%', textAlign: 'left' }}>
            <Button label='Consolidar planilhas' icon="pi pi-file-excel" iconPos='right' onClick={consolidarPlanilhas} className="btn-1" />
          </div> */}
        </div>
      </div>

    </div>
  );
}
export default MeritocraciaImportarPlanilhas;
