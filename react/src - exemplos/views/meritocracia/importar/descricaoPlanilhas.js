const descPlanilhas = [
  { keyPlan: 'cursos',                    namePlan: "Cursos",                       requestUrl: "cursos/"},
  { keyPlan: 'gestao_pessoas',            namePlan: "Gestão de Pessoas",            requestUrl: "gestao-pessoas/"},
  { keyPlan: 'programas',                 namePlan: "Programas",                    requestUrl: "programas/"},
  { keyPlan: 'depara_indicador',          namePlan: "De para indicador",            requestUrl: "depara-indicador/"},
  { keyPlan: 'depara_orgao',              namePlan: "De para orgão",                requestUrl: "depara-orgao/"},
  { keyPlan: 'metas_para_sistema',        namePlan: "Metas para sistema",           requestUrl: "metas-para-sistema/"},
  { keyPlan: 'funcionario_unidade',       namePlan: "Funcionário unidade",          requestUrl: "funcionario-unidade/"},
  { keyPlan: 'sr',                        namePlan: "SR",                           requestUrl: "sr/"},
  { keyPlan: 'reunioes',                  namePlan: "Reuniões",                     requestUrl: "reunioes/"},
  { keyPlan: 'avaliacao_chefia_imediata', namePlan: "Avaliação de chefia imediata", requestUrl: "avaliacao-chefia-imediata/"},    
];

export default descPlanilhas;