import React, { useState, useEffect, useRef } from 'react';
import { Button } from 'primereact/button';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { InputText } from 'primereact/inputtext';
import { FilterMatchMode } from 'primereact/api';
import { Dialog } from 'primereact/dialog';
import { SelectButton } from 'primereact/selectbutton';
import { Dropdown } from 'primereact/dropdown';
import Swal from 'sweetalert2';
import { useNavigate } from 'react-router-dom';
import { UnidadesService } from '../service/Unidades';
import { Tag } from 'primereact/tag';
import { Tooltip } from 'primereact/tooltip';
import TelaSpinner from "../../../../components/spinner";

import api from '../../../../config/api';


function TabelaUnidades(props) {
  const navigate = useNavigate();
  const [editarRegistro, setEditarRegistro] = useState(false);
  const [FuncionarioSelecionado, setFuncionarioSelecionado] = useState([]);
  const [removeLoading, setRemoveLoading] = useState(false);
  const [mapa, setMapa] = useState([]);

  const opcoes = [{ name: 'Sim', value: true }, { name: 'Não', value: false }];
  const dt = useRef(null);


  function showDialog(name, data) {
    if (name === "editarRegistro") {
      setFuncionarioSelecionado(data);
      setEditarRegistro(true);
    }

  }

  function renderDialogFooter(name) {
    if (name === "editarRegistro") {
      return (
        <div>
          <Button label='Cancelar' onClick={() => setEditarRegistro(false)} className="btn-2" />
          <Button label='Salvar' icon="pi pi-check-square" iconPos='right' className="btn-1" />
        </div>
      );
    }
  }

  const [globalFilterValue1, setGlobalFilterValue1] = useState('');
  const [filters1, setFilters1] = useState(null);

  const onGlobalFilterChange1 = (e) => {
    const value = e.target.value;
    let _filters1 = { ...filters1 };
    filters1['global'].value = value;
    setFilters1(_filters1);
    setGlobalFilterValue1(value);
  }

  const initFilters = () => {
    setFilters1({
      'global': { value: null, matchMode: FilterMatchMode.CONTAINS },
    });
    setGlobalFilterValue1('');
  }

  useEffect(() => initFilters(), []);

  useEffect(() => {
    UnidadesService.getUnidade().then((data) => {setMapa(data); setRemoveLoading(true);});
  }, []);

  const renderHeaderMapa = () => {
    return (
      <div className="table-header">
        <span className="p-input-icon-left">
          <i className="pi pi-search" />
          <InputText value={globalFilterValue1} onChange={onGlobalFilterChange1} placeholder="Pesquisar" />
        </span>
        {/* <Button className="btn-comum-1" label='Cadastrar nova vaga' onClick={() => showDialog("criarNovoRegistro", null)} /> */}
      </div>
    )
  }
  const headerMapa = renderHeaderMapa();

  function renderDatatableButtons(rowData) {
    return (
      <div>
        <Button icon="pi pi-pencil" className="p-button-raised p-button-success p-button-text" title='Alterar dados do servidor' onClick={() => showDialog("editarRegistro", rowData)} style={{ width: '35px', height: '35px', borderRadius: '50%' }} />
        <Button icon="pi pi-times" className="p-button-raised p-button-danger p-button-text" title='Excluir dados do servidor' onClick={() => Swal.fire("Exclusão de dados do usuário", "A opção de exclusão foi desabilitada.", "error")} style={{ width: '35px', height: '35px', borderRadius: '50%' }} />
      </div>);
  }

  function exportCSV() {
    try {
      dt.current.exportCSV();
      Swal.fire("Exportação de dados", "Os dados foram exportados no formato .csv com sucesso.", "success");
    } catch (e) {
      Swal.fire("Exportação de dados", `Não foi possível exportar os dados. Motivo: ${e.message}`, "error");
    }
  }

  const [expandedRows, setExpandedRows] = useState(null);
  const allowExpansion = (rowData) => {
    return rowData.orders.length > 0;
  };
  
  const statusUnidade = (rowData) => {
    switch (rowData.status) {
      case 'ELEGIVEL':
        return <Tag value='Elegível' className={`tag-aprovado`} style={{width: '5rem', height: '1.3rem'}} ></Tag>;
      case 'INELEGIVEL':
        return <Tag value='Inelegível' className={`tag-indeferido`} style={{width: '5rem', height: '1.3rem'}}></Tag>;       
    }
  }

  const resultUnidade = (rowData) => {
    return (rowData.resultado_pct+'%')
  }

  const diasDesc = (rowData) => {
    switch (rowData.dias_desc) {
      case 0:
        return "--";
      default:
        return rowData.dias_desc;
    }
  }

  const metaDef = (rowData) => {
    return (rowData.meta+'%')
  }

  const resultMeta = (rowData) => {
    return (rowData.meta_ind_pct+'%')
  }

  const rowExpansionTemplate = (data) => {
    let dados = [data];
    return (
      <DataTable value={dados}>
        <Column header="Tipo"    field="tipo" ></Column>
        <Column header="Matrícula"    field="matricula" ></Column>
        <Column header="% Realizado"    field="realizado_pct" ></Column>
        {/* <Column header="1º Mês"     field='mes_1'   className="col-centralizado"></Column>
        <Column header="2º Mês"     field='mes_2'   className="col-centralizado"></Column>
        <Column header="3º Mês"     field='mes_3'   className="col-centralizado"></Column>
        <Column header="Meta Definida"    field='meta'        body={metaDef}   className="col-centralizado"></Column>
        <Column header="Dias Desconto"    field='dias_desc'   body={diasDesc}   className="col-centralizado"></Column>
        <Column header="Meta Descontada"  field='meta_desc'   className="col-centralizado"></Column>
        <Column header="Real Trimestre"   field='trimestre'   className="col-centralizado"></Column>
        <Column header="% Meta (Por indicador)"   field='meta_ind_pct'   body={resultMeta}      className="col-centralizado"></Column> */}
      </DataTable>
    );
  };

  const paginatorLeft = <Button 
    label='Voltar' 
    icon="pi pi-arrow-left" 
    iconPos='left' 
    onClick={()=>navigate("/meritocracia/trimestres")} 
    className="btn-1" 
  />

  const paginatorRight = <Button onClick={()=>{}} icon="pi pi-download" className="btn-1" label="Exportar mapa" title="Exportar mapa" disabled/>

  const tabMapa =
    <div>
      <DataTable
        paginator
        rows={10} 
        paginatorLeft={paginatorLeft}
        paginatorRight={paginatorRight}
        value={props.unidades}
        header={headerMapa}
        filterDisplay='menu'
        filters={filters1}
        expandedRows={expandedRows} 
        onRowToggle={(e) => {
          setExpandedRows(e.data)
        }}
        rowExpansionTemplate={rowExpansionTemplate}
        emptyMessage='Nenhum registro foi encontrado.'
        size='small'
        sortField='nome'
        sortOrder={3}
        ref={dt}
        id='meritocracia_unidade'
      >
        <Column expander={allowExpansion} />
        <Column
          field='unidade'
          header='Unidades'
          sortable
        />
         <Column 
          field='' 
          header='Meta Unidade' 
          body={statusUnidade}
          className="col-centralizado"  
        />
        {/*
        <Column 
          field='resultado_pct' 
          header='Resultado'
          body={resultUnidade}
          className="col-centralizado"  
        />*/}
        <Column 
          style={{ flexGrow: 1, flexBasis: '100px' }} 
          body={renderDatatableButtons}
          className="col-centralizado" 
          header="Ações"
        /> 
      </DataTable>

    </div>
  ;

  return (
    <div>
      {removeLoading ?<>{tabMapa} </> : <TelaSpinner tamanho={30} texto={"Carregando dados. Aguarde..."} />}

    </div>
  );
}

export default TabelaUnidades;
