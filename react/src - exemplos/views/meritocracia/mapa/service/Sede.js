export const SedeService = {
    getSedeData() {
        return [
            {
                "id": 1,
                "tipo":     "sede_administrativa",
                "matricula":  'SEDE_ADMINISTRATIVA',
                "nome":        "CERIMONIAL",
                "status":   "ELEGIVEL",
                "resultado_pct":    111.13,
                "indicadores":[
                    {
                        'indicador':"% SATISFAÇÃO COM CERIMONIAL (AVALIADO POR MEMBROS QUE PARTICIPAM DE EVENTOS)",
                        'sentido':  true,
                        'meta':     73,
                        'unidade_medida':   '%',
                        'periodicidade':    'MENSAL',
                        'mes_1':        100,
                        'mes_2':        100,
                        'mes_3':        100,
                        'media_acum':   100,
                        'meta_ind_pct': 180
                    },
                    {
                        'indicador':    "% DE CONFORMIDADE COM OS REGISTROS DE DADOS",
                        'sentido':  false,
                        'meta':     73,
                        'unidade_medida':   '%',
                        'periodicidade':    'MENSAL',
                        'mes_1':        100,
                        'mes_2':        100,
                        'mes_3':        100,
                        'media_acum':   100,
                        'meta_ind_pct': 180
                    },
                ]
            },
            {
                "id": 1,
                "tipo":     "sede_administrativa",
                "matricula":  'SEDE_ADMINISTRATIVA',
                "nome":        "ATENDIMENTO CRIMINAL",
                "status":   "INELEGIVEL",
                "resultado_pct":    111.13,
                "indicadores":[
                    {
                        'indicador':"% SATISFAÇÃO COM CERIMONIAL (AVALIADO POR MEMBROS QUE PARTICIPAM DE EVENTOS)",
                        'sentido':  true,
                        'meta':     73,
                        'unidade_medida':   '%',
                        'periodicidade':    'MENSAL',
                        'mes_1':        100,
                        'mes_2':        100,
                        'mes_3':        100,
                        'media_acum':   100,
                        'meta_ind_pct': 180
                    },
                    {
                        'indicador':    "% DE CONFORMIDADE COM OS REGISTROS DE DADOS",
                        'sentido':  false,
                        'meta':     73,
                        'unidade_medida':   '%',
                        'periodicidade':    'MENSAL',
                        'mes_1':        100,
                        'mes_2':        100,
                        'mes_3':        100,
                        'media_acum':   100,
                        'meta_ind_pct': 180
                    },
                ]
            },
        ]
   
    },

    getSede() {
        return Promise.resolve(this.getSedeData());
    },
};
