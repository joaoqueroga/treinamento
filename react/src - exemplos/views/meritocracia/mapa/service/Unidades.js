export const UnidadesService = {
    getUnidadesData() {
        return [
            {
                "id": 1,
                "tipo":     "unidade",
                "matricula":  'CIVEL_INICIAL',
                "nome":        "2ª DP DE 1ª INST. CÍVEL",
                "status":   "INELEGIVEL",
                "resultado_pct":    155.13,
                "indicadores":[
                    {
                        'indicador':    "ATENDIMENTO NOVO",
                        'mes_1':        31,
                        'mes_2':        32,
                        'mes_3':        71,
                        'meta':        73,
                        'dias_desc':    0,
                        'meta_desc':    73,
                        'meta_ind_pct':    230,
                        'trimestre':    134
                    },
                    {
                        'indicador':    "ATOS JUDICIAIS",
                        'mes_1':        10,
                        'mes_2':        16,
                        'mes_3':        9,
                        'meta':        27,
                        'dias_desc':    0,
                        'meta_desc':    27, 
                        'meta_ind_pct':    180,
                        'trimestre':    35
                    },
                ]
            },
            {
                "id": 2,
                "tipo":     "unidade",
                "matricula":  'FAMILIA_INICIAL',
                "nome":        "2ª DP DE 1ª INST. CÍVEL",
                "status":   "ELEGIVEL",
                "resultado_pct":    155.13,
                "indicadores":[
                    {
                        'indicador':    "ATENDIMENTO NOVO",
                        'mes_1':        31,
                        'mes_2':        32,
                        'mes_3':        71,
                        'meta':        73,
                        'dias_desc':    0,
                        'meta_desc':    73,
                        'meta_ind_pct':    230,
                        'trimestre':    134
                    },
                    {
                        'indicador':    "ATOS JUDICIAIS",
                        'mes_1':        10,
                        'mes_2':        16,
                        'mes_3':        9,
                        'meta':        27,
                        'dias_desc':    0,
                        'meta_desc':    27, 
                        'meta_ind_pct':    180,
                        'trimestre':    35
                    },
                ]
            },
            {
                "id": 3,
                "tipo":     "unidade",
                "matricula":  'CRIMINAL_INICIAL',
                "nome":        "2ª DP DE 1ª INST. CÍVEL",
                "status":   "ELEGIVEL",
                "resultado_pct":    155.13,
                "indicadores":[
                    {
                        'indicador':    "ATENDIMENTO NOVO",
                        'mes_1':        31,
                        'mes_2':        32,
                        'mes_3':        71,
                        'meta':        73,
                        'dias_desc':    0,
                        'meta_desc':    73,
                        'meta_ind_pct':    230,
                        'trimestre':    134
                    },
                    {
                        'indicador':    "ATOS JUDICIAIS",
                        'mes_1':        10,
                        'mes_2':        16,
                        'mes_3':        9,
                        'meta':        27,
                        'dias_desc':    0,
                        'meta_desc':    27, 
                        'meta_ind_pct':    180,
                        'trimestre':    35
                    },
                ]
            },
            {
                "id": 4,
                "tipo":     "unidade",
                "matricula":  'CRIMINAL_INICIAL',
                "nome":        "2ª DP DE 1ª INST. CÍVEL",
                "status":   "INELEGIVEL",
                "resultado_pct":    155.13,
                "indicadores":[
                    {
                        'indicador':    "ATENDIMENTO NOVO",
                        'mes_1':        31,
                        'mes_2':        32,
                        'mes_3':        71,
                        'meta':        73,
                        'dias_desc':    0,
                        'meta_desc':    73,
                        'meta_ind_pct':    230,
                        'trimestre':    134
                    },
                    {
                        'indicador':    "ATOS JUDICIAIS",
                        'mes_1':        10,
                        'mes_2':        16,
                        'mes_3':        9,
                        'meta':        27,
                        'dias_desc':    0,
                        'meta_desc':    27, 
                        'meta_ind_pct':    180,
                        'trimestre':    35
                    },
                ]
            },
            {
                "id": 4,
                "tipo":     "unidade",
                "matricula":  'CIVEL_INICIAL',
                "nome":        "2ª DP DE 1ª INST. CÍVEL",
                "status":   "ELEGIVEL",
                "resultado_pct":    155.13,
                "indicadores":[
                    {
                        'indicador':    "ATENDIMENTO NOVO",
                        'mes_1':        31,
                        'mes_2':        32,
                        'mes_3':        71,
                        'meta':        73,
                        'dias_desc':    0,
                        'meta_desc':    73,
                        'meta_ind_pct':    230,
                        'trimestre':    134
                    },
                    {
                        'indicador':    "ATOS JUDICIAIS",
                        'mes_1':        10,
                        'mes_2':        16,
                        'mes_3':        9,
                        'meta':        27,
                        'dias_desc':    0,
                        'meta_desc':    27, 
                        'meta_ind_pct':    180,
                        'trimestre':    35
                    },
                ]
            },
        ]
   
    },

    getUnidade() {
        return Promise.resolve(this.getUnidadesData());
    },
};
