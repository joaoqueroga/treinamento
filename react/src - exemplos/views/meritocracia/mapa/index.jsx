import {useNavigate, useParams, useLocation} from 'react-router-dom';
import React, { useState, useEffect, useRef } from 'react';
import { BreadCrumb } from 'primereact/breadcrumb';
import { TabView, TabPanel } from 'primereact/tabview';
import Swal from 'sweetalert2';

import '../../../index.scss';
import './style.scss';
import api from '../../../config/api';
import { getUserId } from '../../../config/auth';
import { getToken } from '../../../config/auth';

import TabelaServidores from './tabela_servidores';
import TabelaMembros from './tabela_membros';
import TabelaUnidades from './tabela_unidades';
import TabelaSede from './tabela_sede';

function MeritocraciaMapa() {

  const { id } = useParams();
  const itensBreadcrumbs = [{ label: 'Meritocracia' }, { label: 'Trimestres' }, {label: id}];
  const home = { icon: 'pi pi-home', url: '/inicio' };

  const [membros, setMembros] = useState([]);
  const [servidores, setServidores] = useState([]);
  const [unidades, setUnidades] = useState([]);
  const [sede, setSede] = useState([]);

  const [removeLoadingMembros, setRemoveLoadingMembros] = useState(false);
  const [removeLoadingServidores, setRemoveLoadingServidores] = useState(false);
  const [removeLoadingUnidades, setRemoveLoadingUnidades] = useState(false);
  const [removeLoadingSede, setRemoveLoadingSede] = useState(false);
  const [reload, setReload] = useState(false);

  // LISTA MAPA 
  useEffect(() => {
    let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
    let data = {
      "aplicacao":"SGI",
      "id_usuario":getUserId(),
      "id_trimestre": parseInt(id)
    }
    api.post(`/meritocracia/listar-mapa/`, data, config)
      .then(response => {   
        getMembros(response.data.json_membros);
        getServidores(response.data.json_servidores);
        setUnidades(response.data.json_unidades);
      })
      .catch((err) => { Swal.fire("Erro de conexão", `Houve um erro na conexão com o servidor. Por favor, tente mais tarde. Motivo: ${err}`, "error"); });
  }, [reload]);

  function getMembros(json_membros) {
    let id = 0;
    let id_unidades = 0;
    let id_indicadores = 0;
    let index_colapse = 0;
    let dados_copactados = [];
    for(let i = 0; i < json_membros.length ; i++){
      if(i > 0){ // se não for o primeiro elemento

        if(json_membros[i].nome_funcionario === dados_copactados[index_colapse].nome_funcionario){ // se for o mesmo funcionario do ultimo elemento dos compactos 
          let index_colapse_uni = 0; // auxiliar para verificar se é a mesma unidade

          for (let j = 0; j < dados_copactados[index_colapse].unidades.length; j++) { // percorrer unidades já cadastradas
            if(json_membros[i].orgao === dados_copactados[index_colapse].unidades[j].nome){ // verificar se já foi cadastrado uma unidade igual

              if(!dados_copactados[index_colapse].unidades[j].indicadores.find(e => e.indicador == `${json_membros[i].indicador}`)){// verifica se o indicador ja foi cadastrado
                index_colapse_uni ++; // indicar que teve uma unidade igual
                dados_copactados[index_colapse].unidades[j].indicadores.push(
                  {
                    'id':id_indicadores,
                    'indicador':        json_membros[i].indicador,
                    'dias_desconto':    json_membros[i].dias_desconto,
                    'dias_uteis':       json_membros[i].dias_uteis,
                    'meta':             json_membros[i].meta,
                    'realizado_pct':    json_membros[i].realizado_pct,
                    'realizado':        json_membros[i].realizado,
                  },
                );
              }
              break; // para de percorrer o array de unidades
            }
          }
          if(index_colapse_uni === 0) { // verifica se não teve uma unidade igual
            dados_copactados[index_colapse].unidades.push(
              {
                "id_unidade": id_unidades,
                "nome": json_membros[i].orgao,
                "media_pct": 150, /// colocar o valor da procedure aqui
                'indicadores': [
                  {
                  'id':id_indicadores,
                  'indicador':        json_membros[i].indicador,
                  'dias_desconto':    json_membros[i].dias_desconto,
                  'dias_uteis':       json_membros[i].dias_uteis,
                  'meta':             json_membros[i].meta,
                  'realizado_pct':    json_membros[i].realizado_pct,
                  'realizado':        json_membros[i].realizado,
                  },
                ]
              }
            );
          }
        }else{
          // é um novo funcionario
          dados_copactados.push(
            {
              'id': id,
              'nome_funcionario': json_membros[i].nome_funcionario,
              'matricula':        json_membros[i].matricula,
              'avaliacao_chefia': json_membros[i].avaliacao_chefia,
              'cursos':           json_membros[i].cursos,
              'gestao_pessoas':   json_membros[i].gestao_pessoas,
              'tipo':             json_membros[i].tipo,
              'programas':        json_membros[i].programas,
              'reunioes':         json_membros[i].reunioes,
              'status':           json_membros[i].status,
              "id_trimestre":       json_membros[i].id_trimestre,
              "id_trimestre_mapa":  json_membros[i].id_trimestre_mapa,
              'unidades': [
                {
                  "id_unidade": id_unidades,
                  "nome": json_membros[i].orgao,
                  "media_pct": 150, /// colocar o valor da procedure aqui
                  'indicadores': [
                    {
                    'id':id_indicadores,
                    'indicador':        json_membros[i].indicador,
                    'dias_desconto':    json_membros[i].dias_desconto,
                    'dias_uteis':       json_membros[i].dias_uteis,
                    'meta':             json_membros[i].meta,
                    'realizado_pct':    json_membros[i].realizado_pct,
                    'realizado':        json_membros[i].realizado,
                    },
                  ]
                }
              ],
            }
          )
          index_colapse = dados_copactados.length - 1;  // o indece do colapse sempre será o ultimo elemento do array
        }
      }else{ // se for o primeiro elemento
        dados_copactados.push(
          {
            'id': id,
            'nome_funcionario': json_membros[i].nome_funcionario,
            'matricula':        json_membros[i].matricula,
            'avaliacao_chefia': json_membros[i].avaliacao_chefia,
            'cursos':           json_membros[i].cursos,
            'gestao_pessoas':   json_membros[i].gestao_pessoas,
            'tipo':             json_membros[i].tipo,
            'programas':        json_membros[i].programas,
            'reunioes':         json_membros[i].reunioes,
            'status':           json_membros[i].status,
            "id_trimestre":       json_membros[i].id_trimestre,
            "id_trimestre_mapa":  json_membros[i].id_trimestre_mapa,
            'unidades': [
              {
                "id_unidade": id_unidades,
                "nome": json_membros[i].orgao,
                "media_pct": 150, /// colocar o valor da procedure aqui
                'indicadores': [
                  {
                  'id':id_indicadores,
                  'indicador':        json_membros[i].indicador,
                  'dias_desconto':    json_membros[i].dias_desconto,
                  'dias_uteis':       json_membros[i].dias_uteis,
                  'meta':             json_membros[i].meta,
                  'realizado_pct':    json_membros[i].realizado_pct,
                  'realizado':        json_membros[i].realizado,
                  },
                ]
              }
            ],
          }
        )
        index_colapse = dados_copactados.length - 1;  // o indece do colapse sempre será o ultimo elemento do array
      }
      id++;
      id_unidades ++;
      id_indicadores++;
    }

    setMembros(dados_copactados);
    setRemoveLoadingMembros(true);
  }

  function getServidores(json_servidores) {
    let id = 0;
    let id_unidades = 0;
    let id_indicadores = 0;
    let index_colapse = 0;
    let dados_copactados = [];
    for(let i = 0; i < json_servidores.length ; i++){
      if(i > 0){ // se não for o primeiro elemento

        if(json_servidores[i].nome_funcionario === dados_copactados[index_colapse].nome_funcionario){ // se for o mesmo funcionario do ultimo elemento dos compactos 
          let index_colapse_uni = 0; // auxiliar para verificar se é a mesma unidade

          for (let j = 0; j < dados_copactados[index_colapse].unidades.length; j++) { // percorrer unidades já cadastradas
            if(json_servidores[i].orgao === dados_copactados[index_colapse].unidades[j].nome){ // verificar se já foi cadastrado uma unidade igual

              if(!dados_copactados[index_colapse].unidades[j].indicadores.find(e => e.indicador == `${json_servidores[i].indicador}`)){// verifica se o indicador ja foi cadastrado
                index_colapse_uni ++; // indicar que teve uma unidade igual
                dados_copactados[index_colapse].unidades[j].indicadores.push(
                  {
                    'id':id_indicadores,
                    'indicador':        json_servidores[i].indicador,
                    'dias_desconto':    json_servidores[i].dias_desconto,
                    'dias_uteis':       json_servidores[i].dias_uteis,
                    'meta':             json_servidores[i].meta,
                    'realizado_pct':    json_servidores[i].realizado_pct,
                    'realizado':        json_servidores[i].realizado,
                  },
                );
              }
              break; // para de percorrer o array de unidades
            }
          }
          if(index_colapse_uni === 0) { // verifica se não teve uma unidade igual
            dados_copactados[index_colapse].unidades.push(
              {
                "id_unidade": id_unidades,
                "nome": json_servidores[i].orgao,
                "media_pct": 150, /// colocar o valor da procedure aqui
                'indicadores': [
                  {
                  'id':id_indicadores,
                  'indicador':        json_servidores[i].indicador,
                  'dias_desconto':    json_servidores[i].dias_desconto,
                  'dias_uteis':       json_servidores[i].dias_uteis,
                  'meta':             json_servidores[i].meta,
                  'realizado_pct':    json_servidores[i].realizado_pct,
                  'realizado':        json_servidores[i].realizado,
                  },
                ]
              }
            );
          }
        }else{
          // é um novo funcionario
          dados_copactados.push(
            {
              'id': id,
              'nome_funcionario': json_servidores[i].nome_funcionario,
              'matricula':        json_servidores[i].matricula,
              'avaliacao_chefia': json_servidores[i].avaliacao_chefia,
              'cursos':           json_servidores[i].cursos,
              'gestao_pessoas':   json_servidores[i].gestao_pessoas,
              'tipo':             json_servidores[i].tipo,
              'programas':        json_servidores[i].programas,
              'reunioes':         json_servidores[i].reunioes,
              'status':           json_servidores[i].status,
              "id_trimestre":       json_servidores[i].id_trimestre,
              "id_trimestre_mapa":  json_servidores[i].id_trimestre_mapa,
              'unidades': [
                {
                  "id_unidade": id_unidades,
                  "nome": json_servidores[i].orgao,
                  "media_pct": 150, /// colocar o valor da procedure aqui
                  'indicadores': [
                    {
                    'id':id_indicadores,
                    'indicador':        json_servidores[i].indicador,
                    'dias_desconto':    json_servidores[i].dias_desconto,
                    'dias_uteis':       json_servidores[i].dias_uteis,
                    'meta':             json_servidores[i].meta,
                    'realizado_pct':    json_servidores[i].realizado_pct,
                    'realizado':        json_servidores[i].realizado,
                    },
                  ]
                }
              ],
            }
          )
          index_colapse = dados_copactados.length - 1;  // o indece do colapse sempre será o ultimo elemento do array
        }
      }else{ // se for o primeiro elemento
        dados_copactados.push(
          {
            'id': id,
            'nome_funcionario': json_servidores[i].nome_funcionario,
            'matricula':        json_servidores[i].matricula,
            'avaliacao_chefia': json_servidores[i].avaliacao_chefia,
            'cursos':           json_servidores[i].cursos,
            'gestao_pessoas':   json_servidores[i].gestao_pessoas,
            'tipo':             json_servidores[i].tipo,
            'programas':        json_servidores[i].programas,
            'reunioes':         json_servidores[i].reunioes,
            'status':           json_servidores[i].status,
            "id_trimestre":       json_servidores[i].id_trimestre,
            "id_trimestre_mapa":  json_servidores[i].id_trimestre_mapa,
            'unidades': [
              {
                "id_unidade": id_unidades,
                "nome": json_servidores[i].orgao,
                "media_pct": 150, /// colocar o valor da procedure aqui
                'indicadores': [
                  {
                  'id':id_indicadores,
                  'indicador':        json_servidores[i].indicador,
                  'dias_desconto':    json_servidores[i].dias_desconto,
                  'dias_uteis':       json_servidores[i].dias_uteis,
                  'meta':             json_servidores[i].meta,
                  'realizado_pct':    json_servidores[i].realizado_pct,
                  'realizado':        json_servidores[i].realizado,
                  },
                ]
              }
            ],
          }
        )
        index_colapse = dados_copactados.length - 1;  // o indece do colapse sempre será o ultimo elemento do array
      }
      id++;
      id_unidades ++;
      id_indicadores++;
    }
    setServidores(dados_copactados);
    setRemoveLoadingServidores(true);
  }

  return (
    <div className='view'>
      <div className="view-body" >
        <div className="header">
          <BreadCrumb model={itensBreadcrumbs} home={home} />
          <h6 className="titulo-header">{id}</h6>
        </div>
        <TabView>
          <TabPanel header="Membros">
            <TabelaMembros membros={membros} removeLoading={removeLoadingMembros} reload={reload} setReload={setReload} />
          </TabPanel>
          <TabPanel header="Servidores">
            <TabelaServidores servidores={servidores} removeLoading={removeLoadingServidores} reload={reload} setReload={setReload}/>
          </TabPanel>
          {/*<TabPanel header="Unidades">
              <TabelaUnidades unidades={unidades} removeLoading={removeLoadingServidores} reload={reload} setReload={setReload}/>
          </TabPanel>
           <TabPanel header="Sede Administrativa">
              <TabelaSede/>
          </TabPanel> */}
        </TabView>
      </div>
    </div>
  );
}

export default MeritocraciaMapa;
