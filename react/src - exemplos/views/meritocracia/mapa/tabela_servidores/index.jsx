import React, { useState, useEffect, useContext, useRef } from 'react';
import { Button } from 'primereact/button';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { InputText } from 'primereact/inputtext';
import { FilterMatchMode } from 'primereact/api';
import Swal from 'sweetalert2';
import { useNavigate } from 'react-router-dom';
import { Tag } from 'primereact/tag';
import { Tooltip } from 'primereact/tooltip';
import TelaSpinner from "../../../../components/spinner";

function TabelaServidores(state) {
  const navigate = useNavigate();
  const [mapa, setMapa] = useState([]);
  const dt = useRef(null);

  const [globalFilterValue1, setGlobalFilterValue1] = useState('');
  const [filters1, setFilters1] = useState(null);

  const onGlobalFilterChange1 = (e) => {
    const value = e.target.value;
    let _filters1 = { ...filters1 };
    filters1['global'].value = value;
    setFilters1(_filters1);
    setGlobalFilterValue1(value);
  }

  const initFilters = () => {
    setFilters1({
      'global': { value: null, matchMode: FilterMatchMode.CONTAINS },
    });
    setGlobalFilterValue1('');
  }

  useEffect(() => initFilters(), []);

  useEffect(() => {
    setMapa(state.servidores);
  }, [state.servidores]);

  const renderHeaderMapa = () => {
    return (
      <div className="table-header">
        <span className="p-input-icon-left">
          <i className="pi pi-search" />
          <InputText value={globalFilterValue1} onChange={onGlobalFilterChange1} placeholder="Pesquisar" />
        </span>
      </div>
    )
  }
  const headerMapa = renderHeaderMapa();

  function exportCSV() {
    try {
      dt.current.exportCSV();
      Swal.fire("Exportação de dados", "Os dados foram exportados no formato .csv com sucesso.", "success");
    } catch (e) {
      Swal.fire("Exportação de dados", `Não foi possível exportar os dados. Motivo: ${e.message}`, "error");
    }
  }

  const [expandedRows, setExpandedRows] = useState(null);
  const [expandedRowsEx, setExpandedRowsEx] = useState(null);

  const allowExpansion = (rowData) => {
    return rowData.orders.length > 0;
  };

  const tagStatus = (rowData) => {
    if (rowData.status === 'ELEGIVEL') {
      return <Tag value='Elegível' className="tag-aprovado" style={{width: '5rem', height: '1.3rem'}}></Tag>;
    }else{
      return <Tag value='Inelegível' className="tag-indeferido" style={{width: '5rem', height: '1.3rem'}}></Tag>;
    }
  }

  const tagReunioes = (rowData) => {
    if (rowData.reunioes == true) {
      return <><Tooltip target='.tag-merito-reunioes' content='Reuniões'/><Tag /* value='Elegível' */ className="tag-aprovado tag-merito-reunioes"      style={{width: '1.5rem', height: '1.5rem'}}><i className="pi pi-check" style={{ fontSize: '0.7rem' }}></i></Tag></>;
    }else{
      return <><Tooltip target='.tag-merito-reunioes' content='Reuniões'/><Tag /* value='Inelegível' */ className="tag-indeferido tag-merito-reunioes"  style={{width: '1.5rem', height: '1.5rem'}}><i className="pi pi-times" style={{ fontSize: '0.7rem' }}></i></Tag></>;
    }
  }
  
  const tagCursos = (rowData) => {
    if (rowData.cursos == true) {
      return <><Tooltip target='.tag-merito-cursos' content='ESUDPAM'/><Tag className="tag-aprovado tag-merito-cursos"      style={{width: '1.5rem', height: '1.5rem'}}><i className="pi pi-check" style={{ fontSize: '0.7rem' }}></i></Tag> </>;
    }else{
      return <><Tooltip target='.tag-merito-cursos' content='ESUDPAM' /><Tag className="tag-indeferido  tag-merito-cursos"  style={{width: '1.5rem', height: '1.5rem'}}><i className="pi pi-times" style={{ fontSize: '0.7rem' }}></i></Tag> </>;
    }
  }
  const tagAvChefia =  (rowData) =>{
    switch (rowData.avaliacao_chefia) {
      case true:
        return <><Tooltip target='.tag-merito-avaliacao_chefia' content='Avaliação chefia' /> <Tag className='tag-aprovado tag-merito-avaliacao_chefia'   style={{width: '1.5rem', height: '1.5rem'}}><i className="pi pi-check" style={{ fontSize: '0.7rem' }}></i></Tag></>;
      case false:
        return <><Tooltip target='.tag-merito-avaliacao_chefia' content='Avaliação chefia' /> <Tag className='tag-indeferido tag-merito-avaliacao_chefia' style={{width: '1.5rem', height: '1.5rem'}}><i className="pi pi-times" style={{ fontSize: '0.7rem' }}></i></Tag></>;       
      case null:
        return <><Tooltip target='.tag-merito-avaliacao_chefia' content='Avaliação chefia' /> <Tag className='tag-cancelado tag-merito-avaliacao_chefia'  style={{width: '1.5rem', height: '1.5rem'}}><i className="pi pi-times" style={{ fontSize: '0.7rem' }}></i></Tag></>;       
    }
  }

  const tagProgramas = (rowData) => {
    if (rowData.programas == true) {
      return <><Tooltip target='.tag-merito-programas' content='Programas' /><Tag className="tag-aprovado tag-merito-programas"     style={{width: '1.5rem', height: '1.5rem'}}><i className="pi pi-check" style={{ fontSize: '0.7rem' }}></i></Tag></> ;
    }else{
      return <><Tooltip target='.tag-merito-programas' content='Programas' /><Tag className="tag-indeferido tag-merito-programas"   style={{width: '1.5rem', height: '1.5rem'}}><i className="pi pi-times" style={{ fontSize: '0.7rem' }}></i></Tag></>;
    }
  }
  const tagGestaoPessoas = (rowData) => {
    if (rowData.gestao_pessoas == true) {
      return <><Tooltip target='.tag-merito-gestao_pessoas' content='Gestão de Pssoas' /><Tag className="tag-aprovado tag-merito-gestao_pessoas"    style={{width: '1.5rem', height: '1.5rem'}}><i className="pi pi-check" style={{ fontSize: '0.7rem' }}></i></Tag></>;
    }else{
      return <><Tooltip target='.tag-merito-gestao_pessoas' content='Gestão de Pssoas' /><Tag className="tag-indeferido tag-merito-gestao_pessoas"  style={{width: '1.5rem', height: '1.5rem'}}><i className="pi pi-times" style={{ fontSize: '0.7rem' }}></i></Tag></>;
    }
  }

  const RealizadoPct = (rowData) => {
    return (rowData.realizado_pct + ' %');
  }

  //TABELA 3 - INDICADORES
  const rowExpansionTemplateEx = (data) => {
    let dados = data.indicadores;
  return (
      <DataTable value={dados}>
        <Column header="indicador"      field="indicador"></Column>
        <Column header="Dias Úteis"     field="dias_uteis"    className="col-centralizado"></Column>
        <Column header="Dias Desconto"  field="dias_desconto" className="col-centralizado"></Column>
        <Column header="Meta"           field="meta"          className="col-centralizado"></Column>
        <Column header="Realizado"      field="realizado"     className="col-centralizado"></Column>
        <Column header="% Realizado"    body={RealizadoPct}   className="col-centralizado"></Column>
      </DataTable>
    );
  };

  //TABELA 2 - LOTAÇÕES
  const rowExpansionTemplate = (data) => {
    let dados = data.unidades;
    return (
      <DataTable 
        value={dados}
        expandedRows={expandedRowsEx} 
        onRowToggle={(e) => {
          setExpandedRowsEx(e.data)
        }}
        rowExpansionTemplate={rowExpansionTemplateEx}
      >
        <Column expander={allowExpansion} />
        <Column header="Lotação"    field="nome"></Column>
      </DataTable>
    );
  };

  const paginatorLeft = <Button 
    label='Voltar' 
    icon="pi pi-arrow-left" 
    iconPos='left' 
    onClick={()=>navigate("/meritocracia/trimestres")} 
    className="btn-1" 
  />

  const paginatorRight = <Button onClick={()=>{}} icon="pi pi-download" className="btn-1" label="Exportar mapa" title="Exportar mapa" disabled/>

  const tabMapa = //TABELA 1 - FUNCIONÁRIOS
    <div>
      <DataTable
        paginator
        rows={10}
        paginatorLeft={paginatorLeft}
        paginatorRight={paginatorRight}
        value={mapa}
        header={headerMapa}
        filterDisplay='menu'
        filters={filters1}
        expandedRows={expandedRows} 
        onRowToggle={(e) => {
          setExpandedRows(e.data)
        }}
        rowExpansionTemplate={rowExpansionTemplate}
        emptyMessage='Nenhum registro foi encontrado.'
        size='small'
        sortField='nome_funcionario'
        sortOrder={3}
        ref={dt}
        id='meritocracia_servidor'
      >
        <Column expander={allowExpansion} />
        <Column
          field='matricula'
          header='Matrícula'
        />
        <Column
          field='nome_funcionario'
          header='Nome'
          sortable
        />
        <Column header="CRIT I"    field='cursos'    body={tagCursos}      className="col-centralizado"></Column>
        <Column header="CRIT II"    field='avaliacao_chefia'  body={tagAvChefia} className="col-centralizado"></Column>
        <Column header="CRIT IV"    field='gestao_pessoas'    body={tagGestaoPessoas}   className="col-centralizado"></Column>
        <Column header="Resultado"  field='status'    body={tagStatus}   className="col-centralizado" sortable></Column>
      </DataTable>

    </div>

  return (
    <div>
      {state.removeLoading ? tabMapa : <TelaSpinner tamanho={30} texto={"Carregando dados. Aguarde..."} />}
    </div>
  );
}

export default TabelaServidores;
