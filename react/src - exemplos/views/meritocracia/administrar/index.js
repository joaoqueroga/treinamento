import React, { useState, useContext } from 'react';
import { Button } from 'primereact/button';
import { BreadCrumb } from 'primereact/breadcrumb';
import { Checkbox } from 'primereact/checkbox';
import Swal from 'sweetalert2';

import api from '../../../config/api';

import '../../../index.scss'
import './style.scss';


function MeritocraciaAdministrar() {
  const [trimestre, setTrimestre] = useState("Trimestre 2023/01");
  const [planilhas, setPlanilhas] = useState({
    cursos:             {nome: "Cursos",                       requestUrl: "cursos/"                                       },
    reunioes:           {nome: "Reuniões",                     requestUrl: "reunioes/",                 status: "enviada" },
    gestaoPessoas:      {nome: "Gestão de Pessoas",            requestUrl: "gestao-pessoas/"                               },
    programas:          {nome: "Programas",                    requestUrl: "programas/",                status: "enviada" },
    avaliacaoChefia:    {nome: "Avaliação de chefia imediata", requestUrl: "avaliacao-chefia-imediata/"                    },
    deParaIndicador:    {nome: "De para indicador",            requestUrl: "depara-indicador/"                             },
    deParaOrgao:        {nome: "De para orgão",                requestUrl: "depara-orgao/"                                 },
    metas:              {nome: "Metas para sistema",           requestUrl: "metas-para-sistema/"                           },
    funcionarioUnidade: {nome: "Funcionário-unidade",          requestUrl: "funcionario-unidade/",      status: "enviada" },
    SR:                 {nome: "SR",                           requestUrl: "sr/" },
  });

  const itensBreadcrumbs = [
    { label: 'Meritocracia' },
    { label: 'Administrar' }
  ];
  const home = { icon: 'pi pi-home', url: '/inicio' };

  function fetchDados() {

  }

  async function processarMapa() {
    alert("Processar mapa");
  }

  return (
    <div className='view'>
      <div className='view-body'>
        <div className="header">
          <BreadCrumb model={itensBreadcrumbs} home={home} />
        </div>
        <div className='meritocracia-administrar-body'>

          <div className='titulo-trimestre'>
            <h2>Planilhas enviadas</h2>
            <span>{trimestre}</span>
          </div>
          <ul className='lista-planilhas'>
            {
              Object.keys(planilhas).map((key, index) => (
                <li key={key}>
                  <div className={`checkbox-planilha ${planilhas[key].status ? "checkbox-planilha-enviada" : "checkbox-planilha-pendente"}`}>
                    <span>{planilhas[key].nome}</span>
                    {/* <Checkbox checked={true} /> */}
                    <Checkbox checked={planilhas[key].status ?true:false} />
                  </div>
                </li>
              ))
            }

          </ul>

          <Button label='Processar Mapa' onClick={processarMapa} />
        </div>
      </div>
    </div>
  );
}

export default MeritocraciaAdministrar;
