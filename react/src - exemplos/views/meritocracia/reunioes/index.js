import React, { useState, useContext } from 'react';

import { InputText } from 'primereact/inputtext';
import { InputSwitch } from 'primereact/inputswitch';
import { Button } from 'primereact/button';
import { InputTextarea } from 'primereact/inputtextarea';
import { BreadCrumb } from 'primereact/breadcrumb';
import Swal from 'sweetalert2';

import { ApiContext } from '../../../context/store';

import api from '../../../config/api';

import '../../../index.scss'
import './style.scss';


function MeritocraciaReunioes() {
  const { showError } = useContext(ApiContext);

  const itensBreadcrumbs = [
    { label: 'Meritocracia' },
    { label: 'Importar Reuniões' }
  ];
  const home = { icon: 'pi pi-home', url: '/inicio' };

  const [texto, setTexto] = useState("");
  const [titulo, setTitulo] = useState("");
  const [habilitarEdicao, setHabilitarEdicao] = useState(true);
  const [erro, setErro] = useState({
    mensagem: null,
    style: { visibility: "hidden" }
  }); // Se arquivo inválido


  function handleFile(event) {
    Swal.fire({
      title: "Enviando dados",
      text: "Por favor aguarde",
      allowOutsideClick: false,
      didOpen: () => Swal.showLoading()
    });
    const extensoesValidas = ['txt', 'csv'];
    if (event.target.files.length) { // se existem arquivos selecionados
      const extensao = event.target.files[0].name.split('.').pop();
      if (extensoesValidas.includes(extensao)) { // realizar a leitura caso o arquivo possua extensão válida
        const reader = new FileReader();
        reader.readAsText(event.target.files[0]);

        reader.onloadend = function () {
          let resultado = reader.result;
          resultado = resultado.trim(); // remover espaços em branco e newlines no início e no fim do arquivo
          setTexto(resultado);
          setHabilitarEdicao(false);
          setErro({ mensagem: null });
        }
      }
      else
        setErro({
          mensagem: "Por favor, insira um arquivo válido",
          style: { visibility: "visible" }
        });
      Swal.close();
    }
  }

  async function importarDados() {
    let response;
    if (texto === "") 
      return showError("Erro", "O campo está vazio");
    else if (titulo === "")
      return showError("Erro", "O campo título está vazio");

    Swal.fire({
      title: "Enviando dados",
      text: "Por favor aguarde",
      allowOutsideClick: false,
      didOpen: () => Swal.showLoading()
    });

    response = await api.post(`meritocracia/reunioes/`, { texto: texto.trim(), titulo });

    Swal.close();

    if (response.data.Sucesso === "S")
      Swal.fire("Dados importados", "Dados importados com sucesso no sistema", "success");
    else {
      Swal.fire("Falha na importação", `Não foi possível importar os dados no sistema. Motivo: ${response.data.Motivo}`, "error");
      console.error(response.data);
    }
  }

  return (
    <div className='view'>
      <div className='view-body'>
        <div className="header">
          <BreadCrumb model={itensBreadcrumbs} home={home} />
        </div>
        <div className='meritocracia-reunioes-body'>
          <h2>Importar Reuniões</h2>

          <InputTextarea
            value={texto}
            id='input-texto'
            readOnly={!habilitarEdicao}
            disabled={!habilitarEdicao}
            onChange={(e) => setTexto(e.target.value)}
            placeholder='Selecione um arquivo ou insira o seu conteúdo aqui'
          />
          <span className='mensagem-erro' style={erro.style}>{erro.mensagem}</span>
          <div className='meritocracia-elementos'>
            <input type={'file'} onInput={handleFile} />
            <div className='input-toggle-rotulo' style={{ alignSelf: 'flex-start' }}>
              <p>Edição manual</p>
              <InputSwitch
                tooltip='Habilitar edição de texto'
                tooltipOptions={{ position: 'bottom' }}
                checked={habilitarEdicao}
                onChange={e => setHabilitarEdicao(e.value)}
              />
              <p>{habilitarEdicao ?
                "Sim" : "Não"}</p>
            </div>
          </div>
          <InputText
            value={titulo}
            id='input-titulo'
            onChange={(e) => setTitulo(e.target.value)}
            placeholder='Insira aqui o título'
          />

          <Button className="btn-1" label='Importar dados' onClick={importarDados} />
        </div>
      </div>
    </div>
  );
}

export default MeritocraciaReunioes;
