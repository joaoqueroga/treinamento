import React, { useState, useContext } from 'react';

import { InputSwitch } from 'primereact/inputswitch';
import { Button } from 'primereact/button';
import { InputTextarea } from 'primereact/inputtextarea';
import { BreadCrumb } from 'primereact/breadcrumb';
import Swal from 'sweetalert2';

import { ApiContext } from '../../../context/store';

import api from '../../../config/api';

import '../../../index.scss'
import './style.scss';


function MeritocraciaAvaliacaoChefia() {
  const { showError } = useContext(ApiContext);

  const itensBreadcrumbs = [
    { label: 'Meritocracia' },
    { label: 'Importar Avaliação de Chefia' }
  ];
  const home = { icon: 'pi pi-home', url: '/inicio' };

  const [texto1, setTexto1] = useState("");
  const [texto2, setTexto2] = useState("");
  const [habilitarEdicao, setHabilitarEdicao] = useState(true);
  const [erro, setErro] = useState({
    mensagem: null,
    style: { visibility: "hidden" }
  }); // Se arquivo inválido


  function handleFile(event) {
    Swal.fire({
      title: "Enviando dados",
      text: "Por favor aguarde",
      allowOutsideClick: false,
      didOpen: () => Swal.showLoading()
    });
    const extensoesValidas = ['txt', 'csv'];
    if (event.target.files.length) { // se existem arquivos selecionados
      const extensao = event.target.files[0].name.split('.').pop();
      if (extensoesValidas.includes(extensao)) { // realizar a leitura caso o arquivo possua extensão válida
        const reader = new FileReader();
        reader.readAsText(event.target.files[0]);

        reader.onloadend = function () {
          let resultado = reader.result;
          resultado = resultado.trim(); // remover espaços em branco e newlines no início e no fim do arquivo
          if (event.target.id === 'input-texto1')
            setTexto1(resultado);
          else if (event.target.id === 'input-texto2')
            setTexto2(resultado);
          setHabilitarEdicao(false);
          setErro({ mensagem: null });
        }
      }
      else
        setErro({
          mensagem: "Por favor, insira um arquivo válido",
          style: { visibility: "visible" }
        });
      Swal.close();
    }
  }

  async function importarDados() {
    let response;
    if (texto1 === "" || texto2 === "") 
      return showError("Erro", "Existem campos vazios");

    Swal.fire({
      title: "Enviando dados",
      text: "Por favor aguarde",
      allowOutsideClick: false,
      didOpen: () => Swal.showLoading()
    });

    response = await api.post(`meritocracia/avaliacao-chefia-imediata/`, { texto1: texto1.trim(), texto2: texto2.trim() });

    Swal.close();

    if (response.data.Sucesso === "S")
      Swal.fire("Dados importados", "Dados importados com sucesso no sistema", "success");
    else {
      Swal.fire("Falha na importação", `Não foi possível importar os dados no sistema. Motivo: ${response.data.Motivo}`, "error");
      console.error(response.data);
    }
  }

  return (
    <div className='view'>
      <div className='view-body'>
        <div className="header">
          <BreadCrumb model={itensBreadcrumbs} home={home} />
        </div>
        <div className='meritocracia-avaliacao-body'>
          <h2>Importar Avaliação de Chefia Imediata</h2>

          <InputTextarea
            value={texto1}
            id='input-texto'
            readOnly={!habilitarEdicao}
            disabled={!habilitarEdicao}
            onChange={(e) => setTexto1(e.target.value)}
            placeholder='Planilha Gestores-Servidores. Selecione um arquivo ou insira o seu conteúdo aqui'
          />
          <span className='mensagem-erro' style={erro.style}>{erro.mensagem}</span>
          <div className='meritocracia-elementos'>
            <input id='input-texto1' type={'file'} onInput={handleFile} />
            <div className='input-toggle-rotulo' style={{ alignSelf: 'flex-start' }}>
              <p>Edição manual</p>
              <InputSwitch
                tooltip='Habilitar edição de texto'
                tooltipOptions={{ position: 'bottom' }}
                checked={habilitarEdicao}
                onChange={e => setHabilitarEdicao(e.value)}
              />
              <p>{habilitarEdicao ?
                "Sim" : "Não"}</p>
            </div>
          </div>


          <InputTextarea
            value={texto2}
            id='input-texto'
            readOnly={!habilitarEdicao}
            disabled={!habilitarEdicao}
            onChange={(e) => setTexto2(e.target.value)}
            placeholder='Planilha de Avaliações. Selecione um arquivo ou insira o seu conteúdo aqui'
          />
          <div className='meritocracia-elementos'>
            <input id='input-texto2' type={'file'} onInput={handleFile} />
            <div className='input-toggle-rotulo' style={{ alignSelf: 'flex-start' }}>
              <p>Edição manual</p>
              <InputSwitch
                tooltip='Habilitar edição de texto'
                tooltipOptions={{ position: 'bottom' }}
                checked={habilitarEdicao}
                onChange={e => setHabilitarEdicao(e.value)}
              />
              <p>{habilitarEdicao ?
                "Sim" : "Não"}</p>
            </div>
          </div>




          <Button className="btn-1" label='Importar dados' onClick={importarDados} />
        </div>
      </div>
    </div>
  );
}

export default MeritocraciaAvaliacaoChefia;
