import React, {useState, useEffect} from "react";
import { ProgressSpinner } from 'primereact/progressspinner';
import { BreadCrumb } from 'primereact/breadcrumb';
import { Button } from 'primereact/button';
import { FilterMatchMode } from 'primereact/api';
import { DataTable } from 'primereact/datatable';
import { Tag } from 'primereact/tag';
import { Column } from 'primereact/column';
import {useNavigate} from 'react-router-dom';
import Alertas from "../../../utils/alertas";
import api from '../../../config/api';
import { getToken } from '../../../config/auth';
import { getUserId } from '../../../config/auth';

function TrimestresMeritocracia() {
    const [loading, setLoading] = useState(true);
    const [reload, setReload] = useState(false);
    const navigate = useNavigate();
    const items = [
        { label: 'Meritocracia' },
        { label: 'Trimestres' }
    ];
    const home = { icon: 'pi pi-home', url: '/inicio' }
    const [trimestres, setTrimestres] = useState([]);


    useEffect(() => {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
        "aplicacao":"SGI",
        "id_usuario":getUserId()
        }
        api.post(`/meritocracia/listar_trimestres/`, data, config)
        .then((res) => {
            try {
            let x = JSON.parse(res.data);
            if(x.sucesso === 'S'){
                setTrimestres(x.trimestres);
                setLoading(false);
            }else{
                Alertas.erro(x.motivo);
            }
            } catch (error) {
                Alertas.erro(`ERRO: ${error}`);
            }
        }).catch((err) => {
            Alertas.erro(`ERRO: ${err}`);
        });
    }, [reload]);

    /// filtros e badge de status
    const [filters] = useState({
        'descricao': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'encerrado_em': { value: null, matchMode: FilterMatchMode.CONTAINS },
    });

    const statusColumnTemplate = (rowData) => {
        if (!rowData.encerrado_em) {
            return(
                <span>
                    <Tag value="ATUAL" className="tag-aprovado"></Tag>
                </span>
            )    
        } else{
            return(
                <span>
                    <Tag value="ENCERRADO" className="tag-cancelado"></Tag>
                </span>
            )
        }
    }

    const diasColumnTemplate = (rowData) => {
        if(rowData.encerrado_em !== null){
            let datas = rowData.descricao.split(' ');
            return(
                <span>
                    {
                        datas.map((d, index)=>{
                            return d !== '' ?<span key={index}><Tag value={d} className="tag-lista-dias"></Tag>{' '}</span>:null
                        })
                    }
                </span>
            )    
            
        }
    }

    function encerrar_trimestre() {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao":"SGI",
            "id_usuario":getUserId()
        }
        api.post(`/meritocracia/encerrar_trimestre/`, data, config)
        .then((res) => {
            try {
                let x = JSON.parse(res.data);
                if(x.sucesso === 'S'){
                    setReload(!reload);
                    Alertas.sucesso("Trimestre encerrado");
                }else{
                    Alertas.erro(x.motivo);
                }
            } catch (error) {
                Alertas.erro(`ERRO: ${error}`);
            }
        }).catch((err) => {
            Alertas.erro(`ERRO: ${err}`);
        });
    }

    //// template de colunas e rodapés de modais
    const irBodyTemplate = (rowData) => {
        if(!rowData.encerrado_em){
            return(
                <span>
                <Button 
                    onClick={()=>navigate(`/meritocracia/mapa/${rowData.id_trimestre}`)}
                    icon="pi pi-eye"
                    className="btn-darkblue"
                    title="Ver Levantamento"
                />
                <Button 
                    onClick={()=>navigate(`/meritocracia/importar/${rowData.id_trimestre}`)}
                    icon="pi pi-cloud-upload"
                    className="btn-darkblue"
                    title="Importação"
                />
                <Button 
                    onClick={()=>Alertas.confirmacao("Encerrar o trimestre?", encerrar_trimestre)}
                    icon="pi pi-check"
                    className="btn-green"
                    title="Encerrar"
                />
                </span>
            )
        }else{
            return(
                <span>
                <Button 
                    onClick={()=>navigate(`/meritocracia/mapa/${rowData.id_trimestre}`)}
                    icon="pi pi-eye"
                    className="btn-darkblue"
                    title="Ver Mapa"
                />
                </span>
            )
        }                                    
    }    

    return ( 
        <div className='view'>
        <div className="view-body">
        {
        loading?
        <div className="loading-pagina" ><ProgressSpinner/>Carregando...</div>
        :
        <div>
            <div className="header">
                <BreadCrumb model={items} home={home}/>
                <h6 className="titulo-header">Trimestres</h6>
            </div>
            <div>
                <DataTable
                    value={trimestres}
                    size="small"
                    className="tabela-servidores"
                    dataKey="id"
                    filters={filters}
                    filterDisplay="row"
                    emptyMessage="Nada encontrado"
                    scrollable
                    scrollHeight="73vh"
                    selectionMode="single"
                >
                    <Column
                        header="Descrição"
                        field="descricao"
                        filter 
                        filterPlaceholder="Buscar" 
                        style={{ flexGrow: 0, flexBasis: '40%' }}
                        showFilterMenu={false}
                    />
                    <Column
                        header="Encerrado em"
                        body={diasColumnTemplate}
                        filter 
                        filterPlaceholder="Buscar" 
                        style={{ flexGrow: 0, flexBasis: '40%' }}
                        showFilterMenu={false}
                    />
                    <Column
                        header="Status"
                        body={statusColumnTemplate}  
                        style={{ flexGrow: 0, flexBasis: '20%' }}
                    />
                    <Column 
                        field="botao" 
                        header="Ação" 
                        body={irBodyTemplate} 
                        className="col-centralizado" 
                        style={{ flexGrow: 0, flexBasis: '20%' }}
                    />
                </DataTable>
            </div>
        </div>
        }
        </div>
    </div>
    );
}

export default TrimestresMeritocracia;
