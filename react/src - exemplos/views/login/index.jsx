import React, {useState, useRef, useContext, useEffect} from 'react';
import './login.scss';
import { Card } from 'primereact/card';
import { Button } from 'primereact/button';
import { InputMask } from 'primereact/inputmask';
import { Password } from 'primereact/password';
import { Checkbox } from 'primereact/checkbox';
import logo from '../../images/logo_2.png'
import { useNavigate } from 'react-router-dom'
import { Toast } from 'primereact/toast';
import gradient1 from '../../components/navegacao/gradient1.svg';
import gradient2 from '../../components/navegacao/gradient2.svg';
import '../../components/navegacao_topo/navegacao_topo.scss';
import logoBranca from '../../images/Logo Horizontal Branca.png';
import GlobalStyle from '../../styles/global';

import usePersistedState from '../../utils/usePersistedState';
import { ThemeProvider } from 'styled-components';
import { light } from '../../styles/themes/light';

import { ApiContext } from '../../context/store';

import { login , logout} from '../../config/auth';
import api from '../../config/api';

function Login() {
    const [theme] = usePersistedState('theme', light);

    const { showError, setTemplate } = useContext(ApiContext);
    const navigate = useNavigate();
    const toast = useRef(null);

    useEffect(() => {
        logout();
        setTemplate(false);
    }, []);
    
    const [carregando, setCarregando] = useState(false);
    const [username, setUsername] = useState(null);
    const [password, setPassword] = useState(null);

    function flogin(){
        let path = '/core/login/';
        setCarregando(true);

        if(username && password){
            let u = username.replace('.','').replace('.','').replace('-','');
            api.post(path, {
                "username": u,
                "password": password,
            }).then((res)=>{
                setCarregando(false);
                if(res.data.autenticado){
                    setTemplate(true);
                    login(res.data.user);
                    sessionStorage.setItem("versao",res.data.versao);
                    navigate('/inicio');
                }else{
                    showError('Atenção',res.data.msg);
                }
            }).catch((err)=>{
                setCarregando(false);
                showError('Erro', 'Erro no login');
            })
        }else{
            setCarregando(false);
            showError('Atenção', 'Preencha todos os campos');
        }
    }


    const bindBotaoEnter = (event) => {
        if (event.key === "Enter")
          flogin();
      }


    return ( 
        <>
        <ThemeProvider theme={theme} >
        <GlobalStyle/>
        <div className="navegacao-topo">
            <div className='nav-topo-logo' onClick={()=>navigate('/inicio')}>
                <img className='logo-branca' alt='logo_branca' src={logoBranca} draggable={false}/>
            </div>
        </div>

        <div className="proid-login">
            <div className='navegacao'>
                <img className='gradient1' src={gradient1} alt="gradient" />
                <img className='gradient2' src={gradient2} alt="gradient" />
            </div>

            <Toast ref={toast}/>
            <Card className='card-login'>
                <div className='header-card-login'>
                    <img src={logo} alt="logo dpeam" className='image-logo-colorida logo-card-login'/>
                    <h3 id='nome-titulo-sigla'>
                        SGI
                    </h3>
                    <h6 id='nome-titulo-sistema' className='nome-sistema'>
                        Sistema de Gerenciamento Integrado
                    </h6>
                </div>
                <div className='body-card-login field'>

                    <div className="p-inputgroup">
                        <span className="p-inputgroup-addon">
                            <i className="pi pi-user"></i>
                        </span>
                        <InputMask onChange={(e)=>setUsername(e.target.value)} onKeyDownCapture={bindBotaoEnter} placeholder="CPF" className='input-cpf-text'  mask="999.999.999-99"/>
                    </div>

                    <div className="p-inputgroup">
                        <span className="p-inputgroup-addon">
                            <i className="pi pi-lock"></i>
                        </span>
                        <Password onChange={(e)=>setPassword(e.target.value)} onKeyDownCapture={bindBotaoEnter}  placeholder="SENHA" toggleMask className='input-cpf-text' feedback={false}/>
                    </div>
                    <div id='esqueceu-a-senha'>
                        <a href="https://ldapuserpwd.dpeam.com/" className="link" target="_blank">Esqueci minha senha</a>
                    </div>

                </div>
                <div className='footer-card-login'>
                    <Button onClick={flogin} label="Login" loadingIcon="pi pi-spin pi-spinner" className="btn-2" loading={carregando} />
                </div>
            </Card>
        </div>
        </ThemeProvider>
        </>
    );
}

export default Login;