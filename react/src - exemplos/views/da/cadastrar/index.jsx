import React, { useState, useEffect, useRef } from 'react';
import './style.scss';
import { TabView, TabPanel } from 'primereact/tabview';
import { Button } from 'primereact/button';
import Swal from 'sweetalert2';
import {useNavigate} from 'react-router-dom';
import { BreadCrumb } from 'primereact/breadcrumb';
import { Toast } from 'primereact/toast';

import DadosPessoaisColaborador from './dados_pessoais';
import DadosFuncionaisColaborador from  './dados_funcionais';

import api from '../../../config/api';
import { getToken, getUserId } from '../../../config/auth';


function CadastroColaborador() {

    const items = [
        { label: 'Cedidos e Tercerizados' },
        { label: 'Cadastro  Cedidos e Tercerizados' }
    ];
    const home = { icon: 'pi pi-home', url: '/inicio' }
    const navigate = useNavigate();
    const toast = useRef(null);
    const [submit, setSubmit] = useState(false);

    const [bairro, setbairro] = useState("");
    const [cep, setcep] = useState("");
    const [complemento, setcomplemento] = useState("");
    const [cpf, setcpf] = useState("");
    const [email, setemail] = useState("");
    const [logradouro, setlogradouro] = useState("");
    const [matricula, setmatricula] = useState("");
    const [endmunicipio, setEndmunicipio] = useState("");
    const [nascimento, setnascimento] = useState("");
    const [nome, setnome] = useState("");
    const [nome_social, setnome_social] = useState("");
    const [numero, setnumero] = useState("");
    const [rg, setrg] = useState("");
    const [tipologradouro , setTipologradouro] = useState(null);
    const [telefone, setTelefone] = useState('');
    const [grupo_sanguineo, setGrupo_sanguineo] = useState(null);
    const [fator_rh, setFator_rh] = useState(null);
    const [nome_contato_emergencia, setnome_contato_emergencia] = useState("");
    const [telefone_emergencia, settelefone_emergencia] = useState("");

    const [grauinstrucoes, setGrauinstrucoes] = useState([])
    const [tiposlogradouro, setTiposlogradouro] = useState([])

    const [banco, setBanco] = useState('');
    const [agencia, setAgencia] = useState('');
    const [conta, setConta] = useState('');

    const [obrigatorio, setObrigatorio] = useState(false);

    //imagens servidor
    const [foto, setFoto] = useState(null);
    const [caminho_foto, setCaminho_foto] = useState(null);
    const [tipo_servidor, setTiposervidor] = useState(null);

    const [grauinstrucao, setGrauinstrucao] = useState(null);
    const [bolsa, setBolsa] = useState('');
    const [atividades, setatividades] = useState('');
    const [inicio_contrato, setInicio_contrato] = useState('');
    const [fim_adtivo, setFim_adtivo] = useState('');
    const [desligamento, setDesligamento] = useState('');
    const [ferias_referencia, setFerias_referencia] = useState('');
    const [lotacao, setLotacao] = useState(null);
    const [chefe_imediato, setChefeimediato] = useState(null);
    const [supervisor, setSupervisor] = useState(null);
    const [formacao_supervisor, setFormacao_supervisor] = useState('');
    const [cargo_supervisor, setCargo_supervisor] = useState('');
    const [acessor_supervisor, setAcessor_supervisor] = useState(null);
    const [contato_supervisor, setContato_supervisor] = useState('');
    const [contato_acessor, setContato_acessor] = useState('');
    const [agente_integracao, setAgente_integracao] = useState(null);
    const [instituicao_ensino, setInstituicao_ensino] = useState(null);
    const [ativo, setAtivo] = useState(true); 

    const [cargos, setCargos] = useState([]);

    const [tipos_estagio, setTipos_estagio] = useState([]);
    const [tipo_estagio, setTipo_estagio] = useState(null);

    const [descricao_tipo_estagio, setDescricao_tipo_estagio] = useState('');
    const [selecao_lotacao, setSelecao_lotacao] = useState(false);

    const [tipo_conta, setTipo_conta] = useState('');
    const [valor_transporte, setValor_transporte] = useState('');
    const [curso, setCurso] = useState('');
    const [periodo, setPeriodo] = useState('');
    const [tipo_periodo, setTipo_periodo] = useState('');
    const [empresas, setEmpresas] = useState ([]);
    const [empresa, setEmpresa] = useState(null);
    const [Supervisor, setNomeSupervisor] = useState ("");
    const [Responsavel,setResponsavel] = useState("");
    const [Contato, setContato] = useState ("");




    useEffect(() => {
        setSubmit(false);
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        
        let data_aux = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
        }

        api.post('/rh/cargos/',data_aux, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            setCargos(x.cargo);
        })
        
        api.post('/rh/auxiliares/',data_aux ,config)
        .then((res)=>{
            let gi = JSON.parse(res.data.grau_instrucao);
            let tl = JSON.parse(res.data.tipo_logradouro);
            let ts = JSON.parse(res.data.tipo_servidor);
            // busca tipo de servidor estagiários
            let y = ts.tipo_servidor.find(y => (y.descricao === "Estagiário" || y.descricao === "Estagiario"));
            if(y){
                setTiposervidor(y);
            }else{
                console.warn("tipo de servidor Estagiário não existe na base de dados");
            }
            setGrauinstrucoes(gi.grau_instrucao);
            setTiposlogradouro(tl.tipo_logradouro);
        })

        api.post('/da/listar_empresa/',data_aux, config)
        .then((res)=>{
           
            let x = JSON.parse(res.data);
            console.log(x);
            setEmpresas(x.empresa);
        })



    }, []);

    function limparvalores() {
        setbairro("");
        setcep("");
        setcomplemento("");
        setcpf("");
        setemail("");
        setlogradouro("");
        setnascimento("");
        setmatricula("");
        setnome("");
        setnome_social("");
        setnumero("");
        setrg("");
        setTelefone('');
        setTipologradouro("");
        setEndmunicipio("");
        setGrupo_sanguineo(null);
        setFator_rh(null);
        setFoto(null);
        setCaminho_foto(null);
        setBanco('');
        setAgencia('');
        setConta('');

        setGrauinstrucao("");
        setBolsa('');
        setatividades('');
        setInicio_contrato('');
        setFim_adtivo('');
        setDesligamento('');
        setFerias_referencia('');
        setLotacao(null);
        setChefeimediato(null);
        setSupervisor(null);
        setFormacao_supervisor('');
        setCargo_supervisor('');
        setAcessor_supervisor(null);
        setContato_supervisor('');
        setContato_acessor('');
        setAgente_integracao(null);
        setInstituicao_ensino(null);
        setAtivo(true);
        setnome_contato_emergencia("");
        settelefone_emergencia("");
        setTipo_estagio(null);
        setTipo_conta('');
        setValor_transporte('');
        setCurso('');
        setPeriodo('');
        setTipo_periodo('');
        setNomeSupervisor('');
        setResponsavel('');
        setContato('');

    }

    const showSuccess = () => {
        toast.current.show({severity:'success', summary: 'Sucesso', detail:'Imegens atualizadas', life: 3000});
    }

    const showError = () => {
        toast.current.show({severity:'error', summary: 'Erro', detail:'Erro no upload da imagem', life: 3000});
    }

    function nomeFoto(f) {
        if(f){
            let nome = f.name;
            let arr = nome.split('.');
            let tipo = arr[arr.length - 1];
            let d = new Date();
            let tempo = d.toISOString().split('.')[0];
            return `foto_${cpf}_${tempo}.${tipo}`;
        }
        return null;
    }

    function uploadImegens(){ //faz o upload das imagens
        setObrigatorio(false);
        if(matricula && nome && cpf && tipo_estagio){
            setSubmit(true);            
            let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
            // FOTO SERVIDOR
            if(foto){
                let nome_foto = nomeFoto(foto);
                const formData = new FormData();
                formData.append("imagem_foto", foto);
                formData.append("nome_foto", nome_foto);
                api.post('rh/upload_imagem_servidor/', formData, config)
                .then((res)=>{
                    if(res.data === 200){
                        showSuccess();
                        cadastrar(nome_foto); // depois cadastra o servidor
                    }else{
                        showError();
                        cadastrar('');  // se nao coseguir salvar a foto salva só o servidor
                    }
                    setSubmit(false);
                }).catch((err)=>{
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `${err}`,
                        confirmButtonText: 'fechar',
                    })
                    setSubmit(false);
                })
            }else{
                cadastrar('');  // salvar sem foto
            }
        }else{
            Swal.fire({
            icon:'warning',
            title:'Atenção',
            text: 'Preencha os campos obrigatórios',
            confirmButtonText: 'ok',
            }).then(() => {
                setObrigatorio(true);
            })
            setSubmit(false);
        }
    }

    function cadastrar(nome_foto) {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            aplicacao: "SGI",
            id_usuario: getUserId(),
            ativo: ativo,
            bairro: bairro.toUpperCase() || null,
            cep: cep || null,
            complemento: complemento.toUpperCase() || null,
            cpf: cpf || null,
            email_pessoal: email || null,
            logradouro: logradouro.toUpperCase() || null,
            nascimento: nascimento || null,
            matricula: matricula.toUpperCase() || null,
            municipio: endmunicipio? endmunicipio.nome : null,
            nome: nome.toUpperCase() || null,
            nome_social: nome_social.toUpperCase() || null,
            nome_contato_emergencia: nome_contato_emergencia.toUpperCase() || null,
            numero: numero || null,
            rg: rg || null,
            id_tipo_logradouro: tipologradouro?tipologradouro.tipo_logradouro: null,
            telefone_principal: telefone || null,
            telefone_emergencia: telefone_emergencia || null,
            grupo_sanguineo: grupo_sanguineo ? grupo_sanguineo.nome : null,
            fator_rh: fator_rh ? fator_rh.nome : null,
            imagem_caminho_foto: foto ? nome_foto: caminho_foto,
            agencia: agencia || null,
            nome_banco: banco.toUpperCase() || null,
            conta: conta || null,
            id_lotacao: lotacao? lotacao.id_lotacao: null,
            id_chefe_imediato: chefe_imediato? chefe_imediato.id_funcionario: null,
            id_grau_instrucao: grauinstrucao? grauinstrucao.id_grau_instrucao: null,
            atividade_estagiario: atividades || null,
            data_inicio_estagio: inicio_contrato || null,
	        data_termo_aditivo: fim_adtivo || null,
	        data_fim_estagio: desligamento || null,
            ferias_referencia: ferias_referencia || null,
            id_funcionario_supervisor: supervisor?supervisor.id_funcionario:null,
            id_funcionario_assessor_supervisor: acessor_supervisor?acessor_supervisor.id_funcionario:null,
            id_instituicao_ensino: instituicao_ensino?instituicao_ensino.id_instituicao_ensino:null,
            id_agente_integracao: agente_integracao?agente_integracao.id_agente_integracao:null,
            valor_bolsa: bolsa || null,
            id_tipo_servidor: tipo_servidor? tipo_servidor.id_tipo_servidor: null,
            eh_estagiario: true,
            id_tipo_estagio: tipo_estagio?tipo_estagio.id_tipo_estagio: null,
            

            tipo_conta: tipo_conta,
            valor_vale_transporte: valor_transporte,
            curso: curso,
            periodo: periodo,
            tipo_periodo: tipo_periodo,
            id_empresa: empresa? empresa.id_empresa: null,
            supervisor:Supervisor.toUpperCase() || null,
            supervisor:Responsavel.toUpperCase() || null,
            contato:Contato || null,
            


        }
        
        api.post('/estagio/cadastro_estagiario/', data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === "S"){ // se retorna sucesso da procedure de cadastro
                Swal.fire({
                    icon:'success',
                    title:"Sucesso",
                    text: 'Estagiário cadastrado',
                    showCancelButton: true,
                    confirmButtonText: 'fechar',
                    cancelButtonText: `novo`,
                    reverseButtons: true
                }).then((result) => {
                    if (result.isConfirmed) {
                        navigate('/estagio/listar');
                    } else{
                        setSubmit(false);
                        limparvalores();
                    }
                })
                setSubmit(false);
            }else{
                Swal.fire({
                icon:'error',
                title:"Erro",
                text: `${x.motivo}`,
                confirmButtonText: 'fechar',
                })
                setSubmit(false);
            }
        }).catch((err)=>{
            Swal.fire({
                icon:'error',
                title:"Erro",
                text: `${err}`,
                confirmButtonText: 'fechar',
            })
        })
    }

    function selecaoLotacaoChefe(l) {
        setTipos_estagio(l.vagas_estagio)
        setChefeimediato({
            "nome": l.nome_funcionario,
            "id_funcionario": l.id_funcionario_responsavel,
            "matricula": l.matricula
        });
        setTipo_estagio(null);
        setSelecao_lotacao(true);
    }

    function dadosSupervisor(s) {
        setFormacao_supervisor(s.descricao_grau_instrucao);
        setCargo_supervisor(s.descricao_cargo);
        setContato_supervisor(s.telefone_principal);
    }

    function dadosAcessorSupervisor(s) {
        setContato_acessor(s.telefone_principal);
    }

    function selecionaTipoEstagio(t){
        setTipo_estagio(t);
        setBolsa(t.valor_bolsa);
        setValor_transporte(t.valor_vale_transporte)
    }

    return ( 
    <div className='view'>
        <div className='view-body'>
            <div className="header">
                <BreadCrumb model={items} home={home}/>
                <h6 className="titulo-header">Cadastro de Estagiário</h6>
            </div>
            <Toast ref={toast}/>
            <TabView className="cadastro-painel">
            <TabPanel header="Dados Pessoais"> 
                <DadosPessoaisColaborador
                    ativos={true}
                    editar={false}
                    tipologradouro={{get: tipologradouro, set: setTipologradouro}}
                    endmunicipio={{get: endmunicipio, set: setEndmunicipio}}
                    bairro={{get: bairro , set: setbairro}}
                    cep={{get: cep , set: setcep}}
                    complemento={{get: complemento, set: setcomplemento}}
                    cpf={{get: cpf, set: setcpf}}
                    email={{get: email, set: setemail}}
                    logradouro={{get: logradouro, set: setlogradouro}}
                    nascimento={{get: nascimento, set: setnascimento}}
                    matricula={{get: matricula, set: setmatricula}}
                    nome={{get: nome, set: setnome}}
                    nome_social={{get: nome_social, set: setnome_social}}
                    numero={{get: numero, set: setnumero}}
                    rg={{get: rg, set: setrg}}
                    obrigatorio={{get: obrigatorio, set: setObrigatorio}}
                    tiposlogradouro={tiposlogradouro}
                    grupo_sanguineo={{get: grupo_sanguineo, set: setGrupo_sanguineo}}
                    fator_rh={{get: fator_rh, set: setFator_rh}}
                    foto={{get: foto, set: setFoto}}
                    telefone={{get: telefone, set: setTelefone}}
                    caminho_foto={{get: caminho_foto, set: setCaminho_foto}}
                    banco={{get: banco, set: setBanco}}
                    agencia={{get: agencia, set: setAgencia}}
                    conta={{get: conta, set: setConta}}
                    nome_contato_emergencia={{get: nome_contato_emergencia, set: setnome_contato_emergencia}}
                    telefone_emergencia={{get: telefone_emergencia, set: settelefone_emergencia}}
                    tipo_conta={{get:tipo_conta, set:setTipo_conta}}
                />
            </TabPanel>
            <TabPanel header="Dados Funcionais">
                <DadosFuncionaisColaborador
                    ativos={true}
                    editar={false}
                    obrigatorio={{get: obrigatorio, set: setObrigatorio}}
                    grauinstrucao={{get: grauinstrucao, set: setGrauinstrucao}}
                    ativo={{get: ativo, set: setAtivo}}
                    chefe_imediato={{get: chefe_imediato, set: setChefeimediato}}
                    lotacao={{get: lotacao,set: setLotacao}}
                    grauinstrucoes={grauinstrucoes}
                    selecaoLotacaoChefe={selecaoLotacaoChefe}
                    bolsa={{get: bolsa, set: setBolsa }}
                    atividades={{get: atividades, set: setatividades}}
                    inicio_contrato={{get: inicio_contrato, set: setInicio_contrato}}
                    fim_adtivo={{get: fim_adtivo, set: setFim_adtivo}}
                    desligamento={{get: desligamento, set: setDesligamento}}
                    ferias_referencia={{get: ferias_referencia, set: setFerias_referencia}}
                    supervisor={{get: supervisor, set: setSupervisor}}
                    formacao_supervisor={{get: formacao_supervisor, set: setFormacao_supervisor}}
                    cargo_supervisor={{get: cargo_supervisor, set: setCargo_supervisor}}
                    acessor_supervisor={{get: acessor_supervisor, set: setAcessor_supervisor}}
                    contato_supervisor={{get: contato_supervisor, set: setContato_supervisor}}
                    contato_acessor={{get: contato_acessor, set: setContato_acessor}}
                    agente_integracao={{get: agente_integracao, set: setAgente_integracao}}
                    instituicao_ensino={{get: instituicao_ensino, set: setInstituicao_ensino}}
                    dadosSupervisor={dadosSupervisor}
                    dadosAcessorSupervisor={dadosAcessorSupervisor}
                    cargos={cargos}
                    tipos_estagio={{get: tipos_estagio, set: setTipos_estagio}}
                    tipo_estagio={{get: tipo_estagio, set: setTipo_estagio}}
                    selecionaTipoEstagio={selecionaTipoEstagio}
                    selecao_lotacao={selecao_lotacao}
                    descricao_tipo_estagio={descricao_tipo_estagio}
                    valor_transporte={{get: valor_transporte, set:setValor_transporte}}
                    curso={{get: curso, set: setCurso}}
                    periodo={{get: periodo, set: setPeriodo}}
                    tipo_periodo={{get: tipo_periodo, set: setTipo_periodo}}
                    endmunicipio={{get: endmunicipio, set: setEndmunicipio}}
                    empresas={empresas}
                    empresa={{get: empresa,set: setEmpresa}}

                />
            </TabPanel>
            </TabView>
            <div className='rodape-cadastro-servidor'>
                <Button label="Cancelar" icon="pi pi-times" className="btn-2" onClick={()=>navigate('/inicio')}/>
                {
                    submit?
                        <Button 
                            label="Salvar" 
                            icon="pi pi-check" 
                            className="btn-1" 
                            loading
                        />
                    :
                        <Button 
                            label="Salvar" 
                            icon="pi pi-check" 
                            className="btn-1" 
                            onClick={uploadImegens}
                        />
                }
            </div>
        </div> 
    </div>
    );
}

export default CadastroColaborador;