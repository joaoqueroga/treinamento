import React, { useState } from 'react';
import '../style.scss';
import SeletorData from '../../../../components/SeletorData';
import { SelectButton } from 'primereact/selectbutton';
import SeletorServidor from '../../../../components/SeletorServidor';
import SeletorLotacao from '../../../../components/SeletorLotacao';
import { InputText } from 'primereact/inputtext';
import { InputTextarea } from 'primereact/inputtextarea';
import { Dropdown } from 'primereact/dropdown';
import Swal from 'sweetalert2';
import SeletorMunicipio from '../../../../components/SeletorMunicipio';
import TipoVinculoDropdown from '../../dropdow';
import AmazonasDropdown from '../../dropdowMunicipios';




function DadosFuncionaisColaborador(props) {

    const opcoes = [{ name: 'Não', value: false }, { name: 'Sim', value: true }];

    function desativarUsuario(v) {
        if (v !== null) {
            Swal.fire({
                showCancelButton: true,
                confirmButtonText: 'Confirmar',
                cancelButtonText: 'Cancelar',
                title: 'Cuidado',
                icon: 'warning',
                reverseButtons: true,
                html:
                    '<p><b>Desativar um servidor pode ocasionar várias ações</b> </p>' +
                    '<br/>' +
                    '<ul class="texto_alerta_desativar"> ' +
                    '<li> Os acessos para alguns sistemas serão retirados.</li>' +
                    '<li> Após a desativação não será possível reativar via sistema.</li>' +
                    '</ul>' +
                    '<br/>' +
                    '<p>Todas as modificação só serão aplicadas após salvar o formulário</p>'
            }).then((result) => {
                if (result.isConfirmed) {
                    props.ativo.set(v);
                }
            })
        }
    }
    const handleTipoVinculoChange = (value) => {
       
        
      };

    return (

        <div className="form-cadastro-servidor form-servidor-dados-funcionais">
            <div className='painel-cadastro-servidor'>
                <span className='grupo-input-label'>
                    Lotação *
                    <SeletorLotacao
                        get={props.lotacao.get}
                        set={props.lotacao.set}
                        lotacao_chefe={props.selecaoLotacaoChefe}
                        disabled={!props.ativos}
                    />
                </span>
                <span className='grupo-input-label'>
                    Responsável pela Lotação
                    <SeletorServidor
                        get={props.chefe_imediato.get}
                        set={props.chefe_imediato.set}
                        chefe={true}

                    />
                </span>


                <span className='grupo-input-label'>
                    <label> Tipo de Vínculo</label>
                    <TipoVinculoDropdown onChange={handleTipoVinculoChange} />
                </span>

                <span className='grupo-input-label grupo-input-label-2cols'>
                    <div className='grupo-input-label-grad'>
                        Data do início do contrato
                        <SeletorData
                            get={props.inicio_contrato.get}
                            set={props.inicio_contrato.set}
                        />
                    </div>
                    {/* span para adição de um novo iput */}
                    <span>
                        <span className='grupo-input-label-grad'>
                            Data de desligamento
                            <SeletorData
                                get={props.desligamento.get}
                                set={props.desligamento.set}
                            />
                        </span>
                    </span>
                </span>

                <span className='grupo-input-label'>
                    Supervisor
                    <InputText
                        style={{ width: '100%', height: '40px' }}
                        type='text'
                        // value={props.nomeSupervisor.get}
                        onChange={(e) => props.nomeSupervisor.set(e.target.value)}

                    />
                </span>
                <span className='grupo-input-label grupo-input-label-2cols'>
                    <div className='grupo-input-label-grad' style={{ width: "100%" }}>
                        Responsável
                        <InputText
                            className='input-select p-inputtext-sm'
                            style={{ height: "40px" }}
                        />
                    </div>
                </span>

                <span className='grupo-input-label'>
                    Contato
                    <InputText
                        type="text" ach
                        className="p-inputtext-sm"
                        style={{ height: "40px" }}
                    />
                </span>

                <span className='grupo-input-label' >
                    Prefeitura
                    <div className='grupo-input-label-grad' style={{ width: '100%' }}>
                        <AmazonasDropdown/>
                    </div>
                </span>

                <span className='grupo-input-label'>
                    Empresa
                    <Dropdown
                        value={props.empresa.get}
                        options={props.empresas}
                        onChange={(e) => props.empresa.set(e.value)}
                        optionLabel="nome"
                        className='input-select p-inputtext-sm'
                        style={{ height: "40px" }}
                    />
                </span>

                <span className='grupo-input-label'>
                    Ativiades
                    <div className='grupo-input-label' style={{ width: '100%', height: '60px' }}>
                        <InputTextarea
                        type="text"
                        className="p-inputtext-sm"
                        value={props.atividades.get}
                        onChange={(e) => props.atividades.set(e.target.value)}
                        rows={2}
                        disabled={!props.ativos}
                        />
                    </div>
                </span>

                <span className='grupo-input-label'>

                    <span className='grupo-input-label select-estagio-ativo'>
                        Funcionário Ativo?
                        <SelectButton
                            className="p-button-sm"
                            value={props.ativo.get}
                            optionLabel="name"
                            options={opcoes}
                            onChange={(e) => desativarUsuario(e.value)}
                            disabled={!props.ativos}
                        />
                    </span>

                </span>


            </div>
            <br />
        </div>
    );
}

export default DadosFuncionaisColaborador;