import React, { useState, useEffect, useRef } from 'react';
import './style.scss';
import { TabView, TabPanel } from 'primereact/tabview';
import { Button } from 'primereact/button';
import Swal from 'sweetalert2';
import {useNavigate, useParams, useLocation} from 'react-router-dom';
import { BreadCrumb } from 'primereact/breadcrumb';
import { Toast } from 'primereact/toast';
import { ProgressSpinner } from 'primereact/progressspinner';

import DadosPessoaisEstagiario from '../cadastrar/dados_pessoais';
import DadosFuncionaisEstagiario from  '../cadastrar/dados_funcionais';
import AnotacoesEstagiario from './anotacoes';
import AnexosEstagiario from './anexos';
import ContratosEstagio from './contratos';

import api from '../../../config/api';
import { getToken , getUserId} from '../../../config/auth';


function EditarColaborador() {

    const { id } = useParams();
    const {state} = useLocation();
    const {ativos} = state;

    const items = [
        { label: 'Coordenação de Estágio' },
        { label: 'Estagiários', url: '/estagio/listar' },
        { label: 'Editar Estagiário' }
    ];
    const home = { icon: 'pi pi-home', url: '/inicio' }
    const navigate = useNavigate();
    const toast = useRef(null);
    const [submit, setSubmit] = useState(false);
    const [bairro, setbairro] = useState("");
    const [cep, setcep] = useState("");
    const [complemento, setcomplemento] = useState("");
    const [cpf, setcpf] = useState("");
    const [email, setemail] = useState("");
    const [logradouro, setlogradouro] = useState("");
    const [matricula, setmatricula] = useState("");
    const [endmunicipio, setEndmunicipio] = useState("");
    const [nascimento, setnascimento] = useState("");
    const [nome, setnome] = useState("");
    const [nome_social, setnome_social] = useState("");
    const [numero, setnumero] = useState("");
    const [rg, setrg] = useState("");
    const [tipologradouro , setTipologradouro] = useState(null);
    const [telefone, setTelefone] = useState('');
    const [grupo_sanguineo, setGrupo_sanguineo] = useState(null);
    const [fator_rh, setFator_rh] = useState(null);

    const [grauinstrucoes, setGrauinstrucoes] = useState([])
    const [tiposlogradouro, setTiposlogradouro] = useState([])

    const [banco, setBanco] = useState('');
    const [agencia, setAgencia] = useState('');
    const [conta, setConta] = useState('');

    const [obrigatorio, setObrigatorio] = useState(false);

    //imagens servidor
    const [foto, setFoto] = useState(null);
    const [caminho_foto, setCaminho_foto] = useState(null);
    const [tipo_servidor, setTiposervidor] = useState(null);

    const [grauinstrucao, setGrauinstrucao] = useState(null);
    const [bolsa, setBolsa] = useState('');
    const [atividades, setatividades] = useState('');
    const [inicio_contrato, setInicio_contrato] = useState('');
    const [fim_adtivo, setFim_adtivo] = useState('');
    const [desligamento, setDesligamento] = useState('');
    const [ferias_referencia, setFerias_referencia] = useState('');
    const [lotacao, setLotacao] = useState(null);
    const [chefe_imediato, setChefeimediato] = useState(null);
    const [supervisor, setSupervisor] = useState(null);
    const [formacao_supervisor, setFormacao_supervisor] = useState('');
    const [acessor_supervisor, setAcessor_supervisor] = useState(null);
    const [contato_supervisor, setContato_supervisor] = useState('');
    const [contato_acessor, setContato_acessor] = useState('');
    const [agente_integracao, setAgente_integracao] = useState(null);
    const [instituicao_ensino, setInstituicao_ensino] = useState(null);
    const [ativo, setAtivo] = useState(true); 

    const [cargos, setCargos] = useState([]);

    const [carregar, setCarregar] = useState(false);

    const [anotacoes, setAnotacoes] = useState([]);
    const [arquivos, setArquivos] = useState([]);
    const [tipoanotacao, settipoanotacao] = useState([]);

    const [nome_contato_emergencia, setnome_contato_emergencia] = useState("");
    const [telefone_emergencia, settelefone_emergencia] = useState("");

    const [tipos_estagio, setTipos_estagio] = useState([]);
    const [tipo_estagio, setTipo_estagio] = useState(null);
    const [descricao_tipo_estagio, setDescricao_tipo_estagio] = useState('');

    const [selecao_lotacao, setSelecao_lotacao] = useState(false);
    const [contratos, setContratos] = useState([]);

    const [tipo_conta, setTipo_conta] = useState('');
    const [valor_transporte, setValor_transporte] = useState('');
    const [curso, setCurso] = useState('');
    const [periodo, setPeriodo] = useState('');
    const [tipo_periodo, setTipo_periodo] = useState('');
    const [cargo_supervisor, setCargo_supervisor] = useState('');
    const [ativo_historico, setAtivo_historico] = useState(false); 


    useEffect(() => {
        setSubmit(false);
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        api.get('/rh/cargos/', config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            setCargos(x.cargo);
        })
        api.get('/rh/auxiliares/', config)
        .then((res)=>{
            
            let gi = JSON.parse(res.data.grau_instrucao);
            let tl = JSON.parse(res.data.tipo_logradouro);
            let ts = JSON.parse(res.data.tipo_servidor);
            let tpanot = JSON.parse(res.data.tipo_anotacao);
            setGrauinstrucoes(gi.grau_instrucao);
            setTiposlogradouro(tl.tipo_logradouro);
            setTiposervidor(ts.tipo_servidor[2]);
            settipoanotacao(tpanot.tipo_anotacao);
        })

        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            "id_funcionario": Number(id),
            "tipo_listagem": "COMPLETA",
            "ativo": ativos
        }
        api.post('/estagio/estagiarios/',data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === "S"){
                setCarregar(true);
                setbairro(x.funcionarios[0].bairro ?? '');
                setcep(x.funcionarios[0].cep ?? '');
                setcomplemento(x.funcionarios[0].complemento ?? '');
                setcpf(x.funcionarios[0].cpf);
                setemail(x.funcionarios[0].email_pessoal ?? '');
                setlogradouro(x.funcionarios[0].logradouro ?? '');
                setnascimento(x.funcionarios[0].nascimento ?? '');
                setmatricula(x.funcionarios[0].matricula);
                setnome(x.funcionarios[0].nome ?? '');
                setnome_social(x.funcionarios[0].nome_social ?? '');
                setnumero(x.funcionarios[0].numero ?? '');
                setrg(x.funcionarios[0].rg ?? '');
                setTelefone(x.funcionarios[0].telefone_principal ?? '');
                setTipologradouro({
                    "tipo_logradouro": x.funcionarios[0].id_tipo_logradouro,
                    "descricao": x.funcionarios[0].descricao_tipo_logradouro
                });
                setEndmunicipio(x.funcionarios[0].municipio ?? '');
                setGrupo_sanguineo({"nome": x.funcionarios[0].grupo_sanguineo});
                setFator_rh({"nome": x.funcionarios[0].fator_rh});
                setCaminho_foto(x.funcionarios[0].foto);
                setBanco(x.funcionarios[0].nome_banco ?? '');
                setAgencia(x.funcionarios[0].agencia ?? '');
                setConta(x.funcionarios[0].conta ?? '');

                setGrauinstrucao({
                    "id_grau_instrucao": x.funcionarios[0].id_grau_instrucao,
                    "descricao": x.funcionarios[0].descricao_grau_instrucao
                });
                setBolsa(x.funcionarios[0].valor_bolsa ?? '');
                setatividades(x.funcionarios[0].atividade_estagiario ?? '');
                setInicio_contrato(x.funcionarios[0].data_inicio_estagio ?? '');
                setFim_adtivo(x.funcionarios[0].data_termo_aditivo ?? '');
                setDesligamento(x.funcionarios[0].data_fim_estagio ?? '');
                setFerias_referencia(x.funcionarios[0].ferias_referencia ?? '');
                setLotacao({
                    "descricao": x.funcionarios[0].descricao_lotacao ?? '',
                    "id_lotacao": x.funcionarios[0].id_lotacao
                });
                setChefeimediato({
                    "nome": x.funcionarios[0].nome_responsavel_lotacao ?? '',
                    "id_funcionario": x.funcionarios[0].id_funcionario_responsavel_lotacao,
                    "matricula": x.funcionarios[0].matricula_responsavel_lotacao ?? ''
                });
                setAtivo(x.funcionarios[0].ativo);
                setSupervisor({
                    "nome": x.funcionarios[0].nome_supervisor ?? '',
                    "id_funcionario": x.funcionarios[0].id_funcionario_supervisor
                });
                setFormacao_supervisor(x.funcionarios[0].descricao_grau_instrucao_supervisor ?? '');
                
                setAcessor_supervisor({
                    "nome": x.funcionarios[0].nome_assessor_supervisor ?? '',
                    "id_funcionario": x.funcionarios[0].id_funcionario_assessor_supervisor
                });
                setContato_supervisor(x.funcionarios[0].telefone_supervisor ?? '');

                setAgente_integracao({
                    "id_agente_integracao": x.funcionarios[0].id_agente_integracao,
                    "nome": x.funcionarios[0].descricao_agente_integracao ?? ''
                });
                setInstituicao_ensino({
                    "id_instituicao_ensino": x.funcionarios[0].id_instituicao_ensino,
                    "nome":  x.funcionarios[0].descricao_instituicao_ensino ?? ''
                });
                setAnotacoes(x.funcionarios[0].anotacoes);
                setArquivos(x.funcionarios[0].arquivos);
                setContratos(x.funcionarios[0].contratos);
                setCargo_supervisor('');
                setContato_acessor( x.funcionarios[0].telefone_assessor ?? '');
                setnome_contato_emergencia(x.funcionarios[0].nome_contato_emergencia ?? '');
                settelefone_emergencia(x.funcionarios[0].telefone_emergencia ?? '');
                setTipo_estagio(x.funcionarios[0].tipo_estagio);
                setDescricao_tipo_estagio(x.funcionarios[0].tipo_estagio.descricao ?? '');

                setTipo_conta(x.funcionarios[0].tipo_conta ?? '');
                setValor_transporte(x.funcionarios[0].tipo_estagio.valor_vale_transporte ?? '');
                setCurso(x.funcionarios[0].curso ?? '');
                setPeriodo(x.funcionarios[0].periodo ?? '');

                setTipo_periodo(x.funcionarios[0].tipo_periodo ?? '');
                setCargo_supervisor(x.funcionarios[0].supervisor.descricao_cargo ?? '');
                setAtivo_historico(x.funcionarios[0].ativo);
            }
        })

    }, [id]);

    function  reloadAnotacoes() {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {"id_funcionario": Number(id), "tipo_listagem": "COMPLETA"}
        api.post('/rh/servidor/',data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === "S"){
                setAnotacoes(x.funcionarios[0].anotacoes)
            }
        })
    }

    function  reloadArquivos() {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {"id_funcionario": Number(id), "tipo_listagem": "COMPLETA"}
        api.post('/rh/servidor/',data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === "S"){
                setArquivos(x.funcionarios[0].arquivos)
            }
        })
    }

    function  reloadContratos() {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {"id_funcionario": Number(id), "tipo_listagem": "COMPLETA"}
        api.post('/rh/servidor/',data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === "S"){
                setContratos(x.funcionarios[0].contratos)
            }
        })
    }

    const showSuccess = () => {
        toast.current.show({severity:'success', summary: 'Sucesso', detail:'Imegens atualizadas', life: 3000});
    }

    const showError = () => {
        toast.current.show({severity:'error', summary: 'Erro', detail:'Erro no upload da imagem', life: 3000});
    }

    function nomeFoto(f) {
        if(f){
            let nome = f.name;
            let arr = nome.split('.');
            let tipo = arr[arr.length - 1];
            let d = new Date();
            let tempo = d.toISOString().split('.')[0];
            return `foto_${cpf}_${tempo}.${tipo}`;
        }
        return null;
    }

    function uploadImegens(){ //faz o upload das imagens
        setObrigatorio(false);
        if(matricula && nome && cpf && tipo_estagio){
            setSubmit(true);            
            let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
            // FOTO SERVIDOR
            if(foto){
                let nome_foto = nomeFoto(foto);
                const formData = new FormData();
                formData.append("imagem_foto", foto);
                formData.append("nome_foto", nome_foto);
                api.post('rh/upload_imagem_servidor/', formData, config)
                .then((res)=>{
                    if(res.data === 200){
                        showSuccess();
                        cadastrar(nome_foto); // depois cadastra o servidor
                    }else{
                        showError();
                        cadastrar('');  // se nao coseguir salvar a foto salva só o servidor
                    }
                    setSubmit(false);
                }).catch((err)=>{
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `${err}`,
                        confirmButtonText: 'fechar',
                    })
                    setSubmit(false);
                })
            }else{
                cadastrar('');  // salvar sem foto
            }
        }else{
            Swal.fire({
            icon:'warning',
            title:'Atenção',
            text: 'Preencha os campos obrigatórios',
            confirmButtonText: 'ok',
            }).then(() => {
                setObrigatorio(true);
            })
        }
    }

    function enviarEmaileDesativa() {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            cpf: cpf,
            nome: nome,
            matricula: matricula,
            tipo: 'Estagiário'
        }
        api.post('/core/enviar_email_desativacao', data, config)
        .then((res)=>{
            if(res.data === 200) console.warn("Email enviado para desativação");
        })
        api.post('/core/desativar_usuario/', {cpf:cpf.replace('.','').replace('.','').replace('-','')}, config)
        .then((res)=>{
            if(res.data === 200) console.warn("Desativado no SIP");
        })
    }

    function cadastrar(nome_foto) {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            aplicacao: "SGI",
            id_usuario: getUserId(),
            id_funcionario: Number(id),
            ativo: ativo,
            bairro: bairro.toUpperCase() || null,
            cep: cep || null,
            complemento: complemento.toUpperCase() || null,
            cpf: cpf || null,
            email_pessoal: email || null,
            logradouro: logradouro.toUpperCase() || null,
            nascimento: nascimento || null,
            matricula: matricula.toUpperCase() || null,
            municipio: endmunicipio? endmunicipio.nome : null,
            nome: nome.toUpperCase() || null,
            nome_social: nome_social.toUpperCase() || null,
            nome_contato_emergencia: nome_contato_emergencia.toUpperCase() || null,
            numero: numero || null,
            rg: rg || null,
            id_tipo_logradouro: tipologradouro?tipologradouro.tipo_logradouro: null,
            telefone_principal: telefone || null,
            telefone_emergencia: telefone_emergencia || null,
            grupo_sanguineo: grupo_sanguineo ? grupo_sanguineo.nome : null,
            fator_rh: fator_rh ? fator_rh.nome : null,
            imagem_caminho_foto: foto ? nome_foto: caminho_foto,
            agencia: agencia || null,
            nome_banco: banco.toUpperCase() || null,
            conta: conta || null,
            id_lotacao: lotacao? lotacao.id_lotacao: null,
            id_chefe_imediato: chefe_imediato? chefe_imediato.id_funcionario: null,
            id_grau_instrucao: grauinstrucao? grauinstrucao.id_grau_instrucao: null,
            atividade_estagiario: atividades || null,
            data_inicio_estagio: inicio_contrato || null,
	        data_termo_aditivo: fim_adtivo || null,
	        data_fim_estagio: desligamento || null,
            ferias_referencia: ferias_referencia || null,
            id_instituicao_ensino: instituicao_ensino?instituicao_ensino.id_instituicao_ensino:null,
            id_agente_integracao: agente_integracao?agente_integracao.id_agente_integracao:null,
            valor_bolsa: bolsa || null,
            id_tipo_servidor: tipo_servidor.id_tipo_servidor,
            id_funcionario_supervisor: supervisor?supervisor.id_funcionario:null,
            id_funcionario_assessor_supervisor: acessor_supervisor?acessor_supervisor.id_funcionario:null,
            eh_estagiario: true,
            id_tipo_estagio: tipo_estagio?tipo_estagio.id_tipo_estagio: null,

            tipo_conta: tipo_conta,
            valor_vale_transporte: valor_transporte,
            curso: curso,
            periodo: periodo,
            tipo_periodo: tipo_periodo

        }
        
        api.post('/estagio/cadastro_estagiario/', data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === "S"){ // se retorna sucesso da procedure de cadastro
                //envia email caso dasativado
                if(ativo_historico === true && ativo === false){ enviarEmaileDesativa(); }
                Swal.fire({
                    icon:'success',
                    title:"Sucesso",
                    text: 'Estagiário atualizado',
                    confirmButtonText: 'ok',
                }).then((result) => {
                    if (result.isConfirmed) {
                        navigate('/estagio/listar');
                    }
                })
                setSubmit(false);
            }else{
                Swal.fire({
                icon:'error',
                title:"Erro",
                text: `${x.motivo}`,
                confirmButtonText: 'fechar',
                })
                setSubmit(false);
            }
        }).catch((err)=>{
            Swal.fire({
                icon:'error',
                title:"Erro",
                text: `${err}`,
                confirmButtonText: 'fechar',
            })
            setSubmit(false);
        })
    }

    function selecaoLotacaoChefe(l) {
        setTipos_estagio(l.vagas_estagio)
        setChefeimediato({
            "nome": l.nome_funcionario,
            "id_funcionario": l.id_funcionario_responsavel,
            "matricula": l.matricula
        });
        setTipo_estagio(null);
        setSelecao_lotacao(true);
    }

    function dadosSupervisor(s) {
        setFormacao_supervisor(s.descricao_grau_instrucao);
        setCargo_supervisor(s.descricao_cargo);
        setContato_supervisor(s.telefone_principal);
    }

    function dadosAcessorSupervisor(s) {
        setContato_acessor(s.telefone_principal);
    }

    function selecionaTipoEstagio(t){
        setTipo_estagio(t);
        setBolsa(t.valor_bolsa);
        setValor_transporte(t.valor_vale_transporte)
    }

    return ( 
    <div className='view'>
        <div className='view-body'>
            {
                carregar?
            <>
            <div className="header">
                <BreadCrumb model={items} home={home}/>
                <h6 className="titulo-header">Cadastro de Estagiário</h6>
            </div>
            <Toast ref={toast}/>
            
            <TabView className="cadastro-painel">
            <TabPanel header="Dados Pessoais"> 
                <DadosPessoaisEstagiario
                    editar={true}
                    tipologradouro={{get: tipologradouro, set: setTipologradouro}}
                    endmunicipio={{get: endmunicipio, set: setEndmunicipio}}
                    bairro={{get: bairro , set: setbairro}}
                    cep={{get: cep , set: setcep}}
                    complemento={{get: complemento, set: setcomplemento}}
                    cpf={{get: cpf, set: setcpf}}
                    email={{get: email, set: setemail}}
                    logradouro={{get: logradouro, set: setlogradouro}}
                    nascimento={{get: nascimento, set: setnascimento}}
                    matricula={{get: matricula, set: setmatricula}}
                    nome={{get: nome, set: setnome}}
                    nome_social={{get: nome_social, set: setnome_social}}
                    numero={{get: numero, set: setnumero}}
                    rg={{get: rg, set: setrg}}
                    obrigatorio={{get: obrigatorio, set: setObrigatorio}}
                    tiposlogradouro={tiposlogradouro}
                    grupo_sanguineo={{get: grupo_sanguineo, set: setGrupo_sanguineo}}
                    fator_rh={{get: fator_rh, set: setFator_rh}}
                    foto={{get: foto, set: setFoto}}
                    telefone={{get: telefone, set: setTelefone}}
                    caminho_foto={{get: caminho_foto, set: setCaminho_foto}}
                    banco={{get: banco, set: setBanco}}
                    agencia={{get: agencia, set: setAgencia}}
                    conta={{get: conta, set: setConta}}
                    nome_contato_emergencia={{get: nome_contato_emergencia, set: setnome_contato_emergencia}}
                    telefone_emergencia={{get: telefone_emergencia, set: settelefone_emergencia}}
                    tipo_conta={{get:tipo_conta, set:setTipo_conta}}
                    ativos={ativos}
                />
            </TabPanel>
            <TabPanel header="Dados Funcionais">
                <DadosFuncionaisEstagiario
                    editar={false}
                    obrigatorio={{get: obrigatorio, set: setObrigatorio}}
                    grauinstrucao={{get: grauinstrucao, set: setGrauinstrucao}}
                    ativo={{get: ativo, set: setAtivo}}
                    chefe_imediato={{get: chefe_imediato, set: setChefeimediato}}
                    lotacao={{get: lotacao,set: setLotacao}}
                    grauinstrucoes={grauinstrucoes}
                    selecaoLotacaoChefe={selecaoLotacaoChefe}
                    bolsa={{get: bolsa, set: setBolsa }}
                    atividades={{get: atividades, set: setatividades}}
                    inicio_contrato={{get: inicio_contrato, set: setInicio_contrato}}
                    fim_adtivo={{get: fim_adtivo, set: setFim_adtivo}}
                    desligamento={{get: desligamento, set: setDesligamento}}
                    ferias_referencia={{get: ferias_referencia, set: setFerias_referencia}}
                    supervisor={{get: supervisor, set: setSupervisor}}
                    formacao_supervisor={{get: formacao_supervisor, set: setFormacao_supervisor}}
                    cargo_supervisor={{get: cargo_supervisor, set: setCargo_supervisor}}
                    acessor_supervisor={{get: acessor_supervisor, set: setAcessor_supervisor}}
                    contato_supervisor={{get: contato_supervisor, set: setContato_supervisor}}
                    contato_acessor={{get: contato_acessor, set: setContato_acessor}}
                    agente_integracao={{get: agente_integracao, set: setAgente_integracao}}
                    instituicao_ensino={{get: instituicao_ensino, set: setInstituicao_ensino}}
                    dadosSupervisor={dadosSupervisor}
                    dadosAcessorSupervisor={dadosAcessorSupervisor}
                    cargos={cargos}
                    tipos_estagio={{get: tipos_estagio, set: setTipos_estagio}}
                    tipo_estagio={{get: tipo_estagio, set: setTipo_estagio}}
                    selecionaTipoEstagio={selecionaTipoEstagio}
                    selecao_lotacao={selecao_lotacao}
                    descricao_tipo_estagio={descricao_tipo_estagio}
                    valor_transporte={{get: valor_transporte, set:setValor_transporte}}
                    curso={{get: curso, set: setCurso}}
                    periodo={{get: periodo, set: setPeriodo}}
                    tipo_periodo={{get: tipo_periodo, set: setTipo_periodo}}
                    ativos={ativos}
                />
            </TabPanel>
            <TabPanel header="Anotações">
                <AnotacoesEstagiario
                    tipo_anotacao={tipoanotacao}
                    id_funcionario={Number(id)}
                    anotacoes={{get: anotacoes, set: setAnotacoes}}
                    reloadAnotacoes={reloadAnotacoes}
                    ativos={ativos}
                />
            </TabPanel>
            <TabPanel header="Contratos">
                <ContratosEstagio
                    id_funcionario={Number(id)}
                    contratos={{get: contratos, set: setContratos}}
                    anotacoes={{get: anotacoes, set: setAnotacoes}}
                    reloadContratos={reloadContratos}
                    ativos={ativos}
                />
            </TabPanel>
            <TabPanel header="Arquivos Anexos">
                <AnexosEstagiario
                    id_funcionario={Number(id)}
                    cpf={{get: cpf, set: setcpf}}
                    arquivos={{get: arquivos, set: setArquivos}}
                    reloadArquivos={reloadArquivos}
                    ativos={ativos}
                />
            </TabPanel>
            </TabView>

            <div className='rodape-cadastro-servidor'>
                <Button label="Cancelar" icon="pi pi-times" className="btn-2" onClick={()=>navigate('/inicio')}/>
                {
                    submit?
                        <Button 
                            label="Salvar" 
                            icon="pi pi-check" 
                            className="btn-1" 
                            loading
                        />
                    :
                        <Button 
                            label="Salvar" 
                            icon="pi pi-check" 
                            className="btn-1" 
                            onClick={uploadImegens}
                            disabled={!ativos}
                        />
                }
            </div>
            </>
            :
            <div className='loading-pagina'>
                <div className="loading-pagina" ><ProgressSpinner/>Carregando...</div>
            </div>
            }
        </div> 
    </div>
    );
}

export default EditarColaborador;