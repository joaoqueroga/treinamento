import React, {useState} from "react";
import ListarContratosEstagio from "../../../../components/listarContratosEstagio";
import { Button } from 'primereact/button';
import { Dropdown } from 'primereact/dropdown';
import { Dialog } from 'primereact/dialog';
import SeletorContratoEstagio from "../../../../components/SeletorContratoEstagio";
import SeletorData from "../../../../components/SeletorData";
import Swal from 'sweetalert2';
import { getToken, getUserId } from '../../../../config/auth';
import api from '../../../../config/api';
import { InputText } from "primereact/inputtext";
import { BiCloudUpload } from "react-icons/bi";

function ContratosEstagio(props) {

    const FileDownload = require('js-file-download');

    const [modalcadastro, setmodalcadastro] = useState(false);
    const [modalEditar, setmodaleditar] = useState(false);
    const [obrigatorio, setobrigatorio] = useState(false);
    const [tipo, setTipo] = useState('');
    const [inicio, setInicio] = useState('');
    const [fim, setFim] = useState('');
    const [arquivo, setArquivo] = useState(null);
    const [adtivo, setAdtivo] = useState(null);
    const [id_contrato, setId_contrato] = useState(null);
    const [id_aditivo, setId_adtivo] = useState(null);

    const [id_tipo, setid_tipo] = useState(1);

    const tipos = [
        {
            "id": 1,
            "descricao": 'PRINCIPAL'
        },
        {
            "id": 2,
            "descricao": 'ADITIVO'
        }
    ]

    function limparDados() {
        setTipo(null);
        setInicio('');
        setFim('');
        setArquivo(null);
        setAdtivo(null);
        setId_contrato(null);
        setid_tipo(1);
    }

    ///upload PDF

    function nomeArquivo(f) {
        let nome = f.name;
        let arr = nome.split('.');
        let tipo = arr[arr.length - 1];
        let d = new Date();
        let tempo = d.toISOString().split('.')[0];
        return `contrato_${props.id_funcionario}_${tempo}.${tipo}`;
    }


    function uploadFile(tipo) { //tipo 1: adicionar 2: editar
        if(arquivo){
            let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
            let nome = nomeArquivo(arquivo);

            const formData = new FormData();
            formData.append("arquivo", arquivo);
            formData.append("nome", nome);

            api.post('core/upload_pdf', formData, config)
            .then((res)=>{
                if(res.data === 200){
                    if(tipo === 1)
                        adicionar(nome);
                    else
                        atualizar(nome);
                }else{
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `Erro ao salvar o arquivo`,
                        confirmButtonText: 'fechar',
                    })
                }
            }).catch((err)=>{
                Swal.fire({
                    icon:'error',
                    title:"Erro",
                    text: `${err}`,
                    confirmButtonText: 'fechar',
                })
            })
        }else{
            if(tipo === 1)
                adicionar(null);
            else
                atualizar(null);
        }
    }

    function download_pdf(nome_arquivo) {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}, responseType: 'blob'}
        let data = {
            nome: nome_arquivo
        }
        api.post('core/download_pdf', data, config)
        .then((res)=>{
            FileDownload(res.data, `${nome_arquivo}.pdf`);
        }).catch((err)=>{
            let msg = "Falha no download";
            if(err.response.status === 401){
                msg = "Não autorizado";
            }
            Swal.fire({
                icon:'error',
                title:"Erro",
                text: `${msg}`,
                confirmButtonText: 'fechar',
            })
        })
    }

    ///upload PDF fim

    function adicionar(nome_arquivo) {
        if(tipo && inicio){
           
            let config = {headers: {"Authorization": `Bearer ${getToken()}`}}

            let data = {
                aplicacao: "SGI",
                id_usuario: getUserId(),
                id_funcionario: props.id_funcionario,
                data_fim: fim || null,
                data_inicio: inicio || null,
                documento: nome_arquivo,
                excluir: false,
                id_contrato_aditivo: adtivo?adtivo.id_contrato:null
            }

            api.post('/estagio/salvar_funcionario_contratos/', data, config)
            .then((res)=>{
                let x = JSON.parse(res.data);
                if(x.sucesso === "S"){
                    setmodalcadastro(false);
                    Swal.fire({
                        icon: 'success',
                        title:"Sucesso",
                        text: `Contrato registrado`,
                        confirmButtonText: 'fechar',
                    })
                    limparDados();
                    props.reloadContratos();
                }else{
                    setmodalcadastro(false);
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `${x.motivo}`,
                        confirmButtonText: 'fechar',
                    })
                }
            })

        }else{
            setobrigatorio(true);
        }
    }

    function excluir(obj) {

        Swal.fire({
            icon:'warning',
            text: 'Excluir o contrato?',
            showCancelButton: true,
            confirmButtonText: 'sim',
            cancelButtonText: 'não',
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed) {

                let config = {headers: {"Authorization": `Bearer ${getToken()}`}}

                let data = {
                    aplicacao: "SGI",
                    id_usuario: getUserId(),
                    excluir: true,
                    id_contrato: obj.id_contrato
                }

                api.post('/estagio/salvar_funcionario_contratos/', data, config)
                .then((res)=>{
                    let x = JSON.parse(res.data);
                    if(x.sucesso === "S"){
                        setmodalcadastro(false);
                        Swal.fire({
                            icon: 'success',
                            title:"Sucesso",
                            text: `Contrato excluído`,
                            confirmButtonText: 'fechar',
                        })
                        limparDados();
                        props.reloadContratos();
                    }else{
                        setmodalcadastro(false);
                        Swal.fire({
                            icon:'error',
                            title:"Erro",
                            text: `${x.motivo}`,
                            confirmButtonText: 'fechar',
                        })
                    }
                })

            }
        })
    }


    function atualizar(nome_arquivo) {
        if(tipo && inicio){

            let config = {headers: {"Authorization": `Bearer ${getToken()}`}}

            let data = {
                aplicacao: "SGI",
                id_usuario: getUserId(),
                id_funcionario: props.id_funcionario,
                data_fim: fim || null,
                data_inicio: inicio || null,
                documento: nome_arquivo,
                excluir: false,
                id_contrato_aditivo: id_aditivo || null,
                id_contrato: id_contrato
            }

            api.post('/estagio/salvar_funcionario_contratos/',data, config)
            .then((res)=>{
                let x = JSON.parse(res.data);
                if(x.sucesso === "S"){
                    setmodaleditar(false);
                    Swal.fire({
                        icon: 'success',
                        title:"Sucesso",
                        text: `Contrato atualizado`,
                        confirmButtonText: 'fechar',
                    })
                    limparDados();
                    props.reloadContratos();
                }else{
                    setmodaleditar(false);
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `${x.motivo}`,
                        confirmButtonText: 'fechar',
                    })
                }
            })
        }else{
            setobrigatorio(true);
        }
    }

    function seleciona(obj) {
        setInicio(obj.data_inicio);
        setFim(obj.data_fim);
        if(obj.id_contrato_aditivo){
            setTipo({id: 2, descricao: "ADITIVO"});
            setid_tipo(2);
            setId_adtivo(obj.id_contrato_aditivo);
        }else{
            setTipo({id: 1, descricao: "PRINCIPAL"});
            setid_tipo(1);
            setId_adtivo(null);
        }
        setId_contrato(obj.id_contrato);
        if(obj.documento){
            setArquivo({"name": obj.documento});
        }else{
            setArquivo(null);
        }
        setmodaleditar(true);
    }

    const modalCadastroFooter = () => {
        return (
            <div>
                {obrigatorio?<p className='mensagem-erro'>Informe os campos obrigatórios</p>:null}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setmodalcadastro(false);
                        setobrigatorio(false);
                        limparDados();
                    }} 
                    className="btn-2" />
                <Button 
                    label="Salvar" 
                    icon="pi pi-check" 
                    onClick={() => uploadFile(1)} 
                    autoFocus 
                    className="btn-1"/>
            </div>
        );
    }

    const modalEditarFooter = () => {
        return (
            <div>
                {obrigatorio?<p className='mensagem-erro'>Informe os campos obrigatórios</p>:null}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setmodaleditar(false);
                        setobrigatorio(false);
                        limparDados();
                    }} 
                    className="btn-2" />
                <Button 
                    label="Salvar" 
                    icon="pi pi-check" 
                    onClick={() => uploadFile(2)}  
                    autoFocus 
                    className="btn-1"
                />
            </div>
        );
    }

    function novoContrato() {
        setmodalcadastro(true);
    }

    function selecionaTipo(value) {
        setTipo(value);
        setid_tipo(value.id);
    }

    return (
        <div>
            <br/>
            <div className='painel-contratos'>
                <div>
                    <Button
                        className="btn-1" 
                        label="Novo Contrato" 
                        icon="pi pi-plus" 
                        onClick={() => novoContrato()} 
                    />
                </div>
                <Dialog 
                    header="Novo Contrato de Estágio"
                    visible={modalcadastro}
                    style={{ width: '50%' }}
                    footer={modalCadastroFooter}
                    onHide={
                        () =>{
                            setmodalcadastro(false);
                            setobrigatorio(false);
                            limparDados();
                        }
                    }
                >
                    <span className='grupo-input-label'>
                        Tipo do Contrato *
                        <Dropdown 
                            value={tipo} 
                            options={tipos} 
                            onChange={(e)=>selecionaTipo(e.value)} 
                            optionLabel="descricao" 
                            className='input-select p-inputtext-sm'
                            style={{height: "40px"}}
                        />
                    </span>

                    {
                        id_tipo === 2?
                        <span className='grupo-input-label'>
                            Aditivo do Contrato *
                            <SeletorContratoEstagio 
                                contratos={props.contratos.get}
                                get={adtivo}
                                set={setAdtivo}
                            />
                        </span>
                        :null
                    }
                    <span className='grupo-input-label'>
                        Data de Início do Contrato *
                        <SeletorData
                            get={inicio}
                            set={setInicio}
                        />
                    </span>

                    <span className='grupo-input-label'>
                        Data de Fim do Contrato
                        <SeletorData
                            get={fim}
                            set={setFim}
                        />
                    </span>

                    <span className='field input-anotacoes'>
                    Arquivo PDF 
                    <span className='input-arquivo-servidor'>
                        <label className='label-file-servidor'>
                            <BiCloudUpload size={20}/>
                            <input
                                value="" 
                                type="file" 
                                className='input-files' 
                                accept='application/pdf'
                                onChange={e=>setArquivo(e.target.files[0])}
                            />
                        </label>
                        <p>
                            {arquivo?arquivo.name:"Nenhum arquivo selecionado"}
                        </p>
                    </span>
                    </span> 
                    
                </Dialog>

                {/*para editar*/}

                <Dialog 
                    header="Editar Contrato de Estágio"
                    visible={modalEditar}
                    style={{ width: '50%' }}
                    footer={modalEditarFooter}
                    onHide={
                        () =>{
                            setmodaleditar(false);
                            setobrigatorio(false);
                            limparDados();
                        }
                    }
                >
                    <span className='grupo-input-label'>
                        Tipo do Contrato *
                         <InputText
                            value={tipo?tipo.descricao:""}
                            disabled
                            style={{height: "40px", width: '100%'}}
                        />
                    </span>

                    {
                        id_tipo === 2?
                        <span className='grupo-input-label'>
                            Aditivo do Contrato *
                            <InputText
                                value={id_aditivo}
                                disabled
                                style={{height: "40px", width: '100%'}}
                            />
                        </span>
                        :null
                    }
                    <span className='grupo-input-label'>
                        Data de Início do Contrato *
                        <SeletorData
                            get={inicio}
                            set={setInicio}
                        />
                    </span>

                    <span className='grupo-input-label'>
                        Data de Fim do Contrato
                        <SeletorData
                            get={fim}
                            set={setFim}
                        />
                    </span>

                    <span className='field input-anotacoes'>
                    Arquivo PDF 
                    <span className='input-arquivo-servidor'>
                        <label className='label-file-servidor'>
                            <BiCloudUpload size={20}/>
                            <input
                                value="" 
                                type="file" 
                                className='input-files' 
                                accept='application/pdf'
                                onChange={e=>setArquivo(e.target.files[0])}
                            />
                        </label>
                        <p>
                            {arquivo?arquivo.name:"Nenhum arquivo selecionado"}
                        </p>
                    </span>
                    </span>
                    
                </Dialog>

                <ListarContratosEstagio 
                    contratos={props.contratos.get}
                    seleciona={seleciona}
                    baixarPdf={download_pdf}
                    excluir={excluir}
                    ativos={props.ativos}
                />
            </div>
        </div> 
    );
}

export default ContratosEstagio;