import React, { useState } from 'react';
import '../style.scss';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { BiCloudUpload } from "react-icons/bi";
import { Dropdown } from 'primereact/dropdown';
import Swal from 'sweetalert2';
import api from '../../../../config/api';
import { getToken, getUserId } from '../../../../config/auth';
import { BsFileEarmarkPdfFill } from "react-icons/bs";
import { Accordion, AccordionTab } from 'primereact/accordion';

function AnexosEstagiario(props) {
    const FileDownload = require('js-file-download');

    const [file, setFile] = useState(null);
    const [descricao, setDescricao] = useState('');
    const [grupo, setGrupo] = useState(null);
    const [modalcadastro, setmodalcadastro] = useState(false);
    const [obrigatorio, setobrigatorio] = useState(false);

    const grupos = [
        {
            "id": 1,
            "descricao": 'Documentos pessoais'
        },
        {
            "id": 2,
            "descricao": 'Frequências'
        },
        {
            "id": 3,
            "descricao": 'Férias'
        },
        {
            "id": 4,
            "descricao": 'Documentos diversos'
        }
    ]

    function nomeArquivo(f) {
        let nome = f.name;
        let arr = nome.split('.');
        let tipo = arr[arr.length - 1];
        let d = new Date();
        let tempo = d.toISOString().split('.')[0];
        return `arquivo_${props.cpf.get}_${tempo}.${tipo}`;
    }

    function cadastrarArquivo(nome_arquivo){

        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            aplicacao: "SGI",
            id_usuario: getUserId(),
            id_funcionario: props.id_funcionario,
            descricao: descricao.toUpperCase(),
            caminho: nome_arquivo,
            grupo: grupo.id
        }
        api.post('rh/cadastro_arquivo_servidor/', data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === "S"){
                setmodalcadastro(false);
                setobrigatorio(false);
                Swal.fire({
                icon:'success',
                title:"Sucesso",
                text: 'Upload de documento concluído',
                confirmButtonText: 'ok',
                }).then(() => {
                    setDescricao('');
                    setFile(null);
                    setGrupo(null);
                    props.reloadArquivos()
                })
            }else{
                Swal.fire({
                icon:'error',
                title:"Erro",
                text: `${x.motivo}`,
                confirmButtonText: 'fechar',
                })
            }
        }).catch((err)=>{
            Swal.fire({
                icon:'error',
                title:"Erro",
                text: `${err}`,
                confirmButtonText: 'fechar',
            })
        })
    }

    function uploadFile() {
        if(file && descricao && grupo){
            let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
            let nome = nomeArquivo(file);

            const formData = new FormData();
            formData.append("arquivo", file);
            formData.append("nome", nome);

            api.post('core/upload_pdf', formData, config)
            .then((res)=>{
                if(res.data === 200){
                    cadastrarArquivo(nome);
                }else{
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `Erro ao salvar o arquivo`,
                        confirmButtonText: 'fechar',
                    })
                }
            }).catch((err)=>{
                Swal.fire({
                    icon:'error',
                    title:"Erro",
                    text: `${err}`,
                    confirmButtonText: 'fechar',
                })
            })
        }else{
            setobrigatorio(true);
        }
    }

    function download_pdf(nome_arquivo) {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}, responseType: 'blob'}
        let data = {
            nome: nome_arquivo
        }
        api.post('core/download_pdf', data, config)
        .then((res)=>{
            FileDownload(res.data, `${nome_arquivo}.pdf`);
        }).catch((err)=>{
            let msg = "Falha no download";
            if(err.response.status === 401){
                msg = "Não autorizado";
            }
            Swal.fire({
                icon:'error',
                title:"Erro",
                text: `${msg}`,
                confirmButtonText: 'fechar',
            })
        })
    }
    
    const modalCadastroFooter = () => {
        return (
            <div>
                {obrigatorio?<p style={{color: "#f00"}}>Informe os campos obrigatórios</p>:null}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setmodalcadastro(false);
                        setobrigatorio(false);
                    }} 
                    className="btn-2" />
                <Button 
                    label="Salvar" 
                    icon="pi pi-check" 
                    onClick={() => uploadFile()} 
                    autoFocus 
                    className="btn-1"
                />
            </div>
        );
    }

    function excluir(id) {
        Swal.fire({
            icon:'warning',
            text: 'Excluir o arquivo em anexo?',
            showCancelButton: true,
            confirmButtonText: 'sim',
            cancelButtonText: 'não',
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed) {
                let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
                let data = {
                    aplicacao: "SGI",
                    id_usuario: getUserId(),
                    id_funcionario_arquivos: id,
                }
                api.post('rh/excluir_arquivo_servidor/', data, config)
                .then((res)=>{
                    let x = JSON.parse(res.data);
                    if(x.sucesso === "S"){
                        Swal.fire({
                            icon:'success',
                            title:"Sucesso",
                            text: 'Documento excluído',
                            confirmButtonText: 'ok',
                        }).then(() => {
                            props.reloadArquivos()
                        })
                    }else{
                        Swal.fire({
                            icon:'error',
                            title:"Erro",
                            text: `Erro ao excluir o arquivo`,
                            confirmButtonText: 'fechar',
                        })
                    }
                }).catch((err)=>{
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `${err}`,
                        confirmButtonText: 'fechar',
                    })
                })
            }
        })
    }


    return (
        <div className='painel-anotacoes'>
            <br/>
            <div>
                <Button
                    className="btn-1" 
                    label="Enviar Arquivo" 
                    icon="pi pi-upload" 
                    onClick={() =>setmodalcadastro(true)} 
                />
            </div>
            <Dialog 
                header="Seleção de Arquivo"
                visible={modalcadastro}
                style={{ width: '50%' }}
                footer={modalCadastroFooter}
                onHide={
                    () =>{
                        setmodalcadastro(false);
                        setobrigatorio(false);
                    }
                }
            >
                <span className="field input-anotacoes">
                    Descrição do arquivo *
                    <InputText 
                        className="block"
                        onChange={(e)=>setDescricao(e.target.value)}
                        style={{height: "40px"}}
                    />
                </span>
                <span className='field input-anotacoes'>
                    Grupo do Arquivo *
                    <Dropdown 
                        value={grupo} 
                        options={grupos} 
                        onChange={(e)=>setGrupo(e.value)} 
                        optionLabel="descricao" 
                        className='input-select p-inputtext-sm'
                        style={{height: "40px"}}
                    />
                </span> 
                <span className='field input-anotacoes'>
                    Arquivo PDF *
                    <span className='input-arquivo-servidor'>
                        <label className='label-file-servidor'>
                            <BiCloudUpload size={20}/>
                            <input
                                value="" 
                                type="file" 
                                className='input-files' 
                                accept='application/pdf'
                                onChange={e=>setFile(e.target.files[0])}
                            />
                        </label>
                        <p>
                            {file?file.name:"Nenhum arquivo selecionado"}
                        </p>
                    </span>
                </span>
            </Dialog>
            <br/>
            <Accordion multiple style={{width: "100%"}}>
                <AccordionTab header={grupos[0].descricao}>
                {
                    props.arquivos.get.map((a, id)=>{
                        return(
                            Number(a.grupo) === grupos[0].id?
                                <div key={id} className="documento-item-estagio">
                                    <p  
                                        className='link-arquivo-servidor'
                                    >
                                        <BsFileEarmarkPdfFill size={25} style={{color:"red"}}/>
                                        {" "}
                                        {a.descricao}
                                    </p>
                                    <span>
                                        <Button
                                            icon="pi pi-download"
                                            className="btn-darkblue"
                                            onClick={()=>download_pdf(a.caminho)}
                                        />
                                        <Button
                                            icon="pi pi-trash"
                                            className="btn-red"
                                            onClick={()=>excluir(a.id_funcionario_arquivos)}
                                            disabled={!props.ativos}
                                        />
                                    </span>
                                </div>
                            :
                                null
                        )
                    })
                }
                </AccordionTab>
                <AccordionTab header={grupos[1].descricao}>
                {
                    props.arquivos.get.map((a, id)=>{
                        return(
                            Number(a.grupo) === grupos[1].id?
                                <div key={id} className="documento-item-estagio">
                                    <p  
                                        className='link-arquivo-servidor'
                                    >
                                        <BsFileEarmarkPdfFill size={25} style={{color:"red"}}/>
                                        {" "}
                                        {a.descricao}
                                    </p>
                                    <span>
                                        <Button
                                            icon="pi pi-download"
                                            className="btn-darkblue"
                                            onClick={()=>download_pdf(a.caminho)}
                                        />
                                        <Button
                                            icon="pi pi-trash"
                                            className="btn-red"
                                            onClick={()=>excluir(a.id_funcionario_arquivos)}
                                            disabled={!props.ativos}
                                        />
                                    </span>
                                </div>
                            :
                                null
                        )
                    })
                }
                </AccordionTab>
                <AccordionTab header={grupos[2].descricao}>
                {
                    props.arquivos.get.map((a, id)=>{
                        return(
                            Number(a.grupo) === grupos[2].id?
                                <div key={id} className="documento-item-estagio">
                                    <p  
                                        className='link-arquivo-servidor'
                                    >
                                        <BsFileEarmarkPdfFill size={25} style={{color:"red"}}/>
                                        {" "}
                                        {a.descricao}
                                    </p>
                                    <span>
                                        <Button
                                            icon="pi pi-download"
                                            className="btn-darkblue"
                                            onClick={()=>download_pdf(a.caminho)}
                                        />
                                        <Button
                                            icon="pi pi-trash"
                                            className="btn-red"
                                            onClick={()=>excluir(a.id_funcionario_arquivos)}
                                            disabled={!props.ativos}
                                        />
                                    </span>
                                </div>
                            :
                                null
                        )
                    })
                }
                </AccordionTab>
                <AccordionTab header={grupos[3].descricao}>
                {
                    props.arquivos.get.map((a, id)=>{
                        return(
                            Number(a.grupo) === grupos[3].id?
                                <div key={id} className="documento-item-estagio">
                                    <p  
                                        className='link-arquivo-servidor'
                                    >
                                        <BsFileEarmarkPdfFill size={25} style={{color:"red"}}/>
                                        {" "}
                                        {a.descricao}
                                    </p>
                                    <span>
                                        <Button
                                            icon="pi pi-download"
                                            className="btn-darkblue"
                                            onClick={()=>download_pdf(a.caminho)}
                                        />
                                        <Button
                                            icon="pi pi-trash"
                                            className="btn-red"
                                            onClick={()=>excluir(a.id_funcionario_arquivos)}
                                            disabled={!props.ativos}
                                        />
                                    </span>
                                </div>
                            :
                                null
                        )
                    })
                }
                </AccordionTab>
            </Accordion>
            <br/>
            
        </div>
    );
}

export default AnexosEstagiario;