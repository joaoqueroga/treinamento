import React, { useState, useEffect } from "react";
// import './style.scss';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { FilterMatchMode } from 'primereact/api';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import Swal from 'sweetalert2';
import { ProgressSpinner } from 'primereact/progressspinner';
import { BreadCrumb } from 'primereact/breadcrumb';
import { InputMask } from 'primereact/inputmask';

import api from "../../../config/api";
import { getToken, getUserId } from "../../../config/auth";


function Empresas(props) {

    const items = [
        { label: 'Cedidos e Tercerizados' },
        { label: 'Parâmetros' },
        { label: 'Empresa' }
    ];
    const home = { icon: 'pi pi-home', url: '/inicio' }
    const [dados, setDados] = useState([]);
    const [modal, setmodal] = useState(false);
    const [obrigatorio, setobrigatorio] = useState(false);

    const [modaleditar, setmodalEditar] = useState(false);
    const [obrigatorioeditar, setobrigatorioEditar] = useState(false);

    const [loading, setLoading] = useState(true);

    const [nome, setNome] = useState('');
    const [endereco, setEndereco] = useState('');
    const [id, setId] = useState(null);
    const [cnpj, setCnpj] = useState('');

    useEffect(() => {
        let config = { headers: { "Authorization": `Bearer ${getToken()}` } }
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
        }
        api.post('/da/listar_empresa/', data, config)
            .then((res) => {
                let x = JSON.parse(res.data);
                if (x.sucesso === 'S') {
                    setDados(x.empresa);
                    setLoading(false);
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: "Erro",
                        text: `${x.motivo}`,
                        confirmButtonText: 'fechar',
                    })
                }
            })
    }, []);

    const irBodyTemplate = (rowData) => {
        return (
            <span>
                <Button
                    onClick={() => seleciona(rowData)}
                    icon="pi pi-pencil"
                    className="btn-green"
                    title="Editar função"
                />

            </span>
        )
    }

    //Botão para exclusão 

    const excluirBodyTemplate = (rowData) => {
        console.log(rowData);
        return (
            <span>
                <Button
                    onClick={() => excluirEmpresa(rowData.id_empresa)}
                    icon="pi pi-trash"
                    className="btn-red"
                    title="Excluir empresa"
                />
            </span>
        );
    };

    const [filters] = useState({
        'nome': { value: null, matchMode: FilterMatchMode.CONTAINS },
    });

    function seleciona(obj) {
        setId(obj.id_empresa);
        setNome(obj.nome);
        setEndereco(obj.endereco);
        setmodalEditar(true);
        setCnpj(obj.cnpj);
    }


    function liparDados() {
        setNome("");
        setEndereco("");
        setCnpj("");
    }

    function reload() {
        let config = { headers: { "Authorization": `Bearer ${getToken()}` } }
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
        }
        api.post('/da/listar_empresa/', data, config)
            .then((res) => {
                let x = JSON.parse(res.data);
                if (x.sucesso === 'S') {
                    setDados(x.empresa);
                }
            })
    }

    function salvar() {
        if (nome && endereco) {
            let dados = {
                nome: nome.toUpperCase(),
                endereco: endereco.toUpperCase(),
                cnpj: cnpj,
                excluir: false,
            };

            let config = {
                headers: { "Authorization": `Bearer ${getToken()}` }
            };

            let data = {
                aplicacao: "SGI",
                id_usuario: getUserId()
            };

            api.post('/da/salvar_empresa/', { ...dados, ...data }, config)
                .then((res) => {
                    let x = JSON.parse(res.data);
                    if (x.sucesso === "S") {
                        setmodal(false);
                        Swal.fire({
                            icon: 'success',
                            title: "Sucesso",
                            text: "Empresa Cadastrada",
                            confirmButtonText: 'fechar',
                        });
                        reload();
                        liparDados();
                    } else {
                        setmodal(false);
                        Swal.fire({
                            icon: 'error',
                            title: "Erro",
                            text: `${x.motivo}`,
                            confirmButtonText: 'fechar',
                        });
                    }
                });

            liparDados();
            setmodal(false);
        } else {
            setobrigatorio(true);
        }
    }


 

    function atualizar() {
        console.log('nome:', nome);
        console.log('endereco:', endereco);
        console.log('cnpj:', cnpj);
        if (nome && endereco) {
            let dados = {
                id_empresa: id,
                nome: nome.toUpperCase(),
                endereco: endereco.toUpperCase(),
                cnpj: cnpj,
                excluir: false,
            }
            let token = getToken();
            let config = { headers: { "Authorization": `Bearer ${token}` } }
            let data = {
                ...dados,
                aplicacao: "SGI",
                id_usuario: getUserId(),
                
            };
            api.post('/da/salvar_empresa/', data, config)
                .then((res) => {
                    let x = JSON.parse(res.data);
                    if (x.sucesso === "S") {
                        setmodal(false);
                        Swal.fire({
                            icon: 'success',
                            title: "Sucesso",
                            text: `Empresa Atualizada`,
                            confirmButtonText: 'fechar',
                        });
                        reload();
                        liparDados();
                    } else {
                        setmodal(false);
                        Swal.fire({
                            icon: 'error',
                            title: "Erro",
                            text: `${x.motivo}`,
                            confirmButtonText: 'fechar',
                        });
                    }
                })

            liparDados();
            setmodalEditar(false);
        } else {
            setobrigatorioEditar(true);
        }
    }



        //Função para excluir empresa 

        function excluirEmpresa(empresaId) {
            console.log(empresaId);
            console.log('to sendo chamda');
            let token = getToken();
            let config = { headers: { "Authorization": `Bearer ${token}` } };
            let data = {
              aplicacao: "SGI",
              id_usuario: getUserId(),
              id_empresa: empresaId,
              excluir: true // Indica que é uma operação de exclusão
            };
          
            api.post('/da/salvar_empresa/', data, config)
              .then((res) => {
                console.log('to chegando aqui');
                let response = JSON.parse(res.data);
                if (response.sucesso === "S") {
                  reload();
                  Swal.fire({
                    icon: 'success',
                    title: "Sucesso",
                    text: "Empresa excluída",
                    confirmButtonText: 'Fechar',
                  });
                } else {
                  Swal.fire({
                    icon: 'error',
                    title: "Erro",
                    text: "Falha ao excluir a empresa",
                    confirmButtonText: 'Fechar',
                  });
                }
              })
              .catch((error) => {
                console.error('Erro na exclusão:', error);
              });
          }



    const modalCadastroFooter = () => {
        return (
            <div>
                {obrigatorio ? <p><small className="p-error">Informe os capos obrigatórios (*).</small></p> : null}
                <Button
                    label="Cancelar"
                    icon="pi pi-times"
                    onClick={() => {
                        setmodal(false);
                        setobrigatorio(false);
                    }}
                    className="btn-2" />
                <Button
                    label="Salvar"
                    icon="pi pi-check"
                    onClick={() => {
                        salvar();
                    }}
                    className="btn-1"
                />
            </div>
        );
    }

    const modalEditarFooter = () => {
        return (
            <div>
                {obrigatorioeditar ? <p><small className="p-error">Informe os capos obrigatórios (*).</small></p> : null}
                <Button
                    label="Cancelar"
                    icon="pi pi-times"
                    onClick={() => {
                        setmodalEditar(false);
                        setobrigatorioEditar(false);
                    }}
                    className="btn-2" />
                <Button
                    label="Salvar"
                    icon="pi pi-check"
                    onClick={() => {
                        atualizar();
                    }}
                    className="btn-1"
                />
            </div>
        );
    }

    return (
        <div className='view'>
            <div className="view-body">
                {
                    loading ?
                        <div className="loading-pagina" ><ProgressSpinner />Carregando...</div>
                        :
                        <div>
                            <div className="header">
                                <BreadCrumb model={items} home={home} />
                                <h6 className="titulo-header">Empresa</h6>
                                <Button
                                    label="Nova Empresa"
                                    icon="pi pi-plus"
                                    className="btn-1"
                                    onClick={() => { setmodal(true); liparDados() }}
                                />
                            </div>
                            <div className="top-box">
                                <h1>Empresas</h1>
                            </div>
                            <div className="lotacao-body">
                                <div className="dados-lotacoes">
                                    <DataTable
                                        value={dados}
                                        size="small"
                                        className="tabela-servidores"
                                        dataKey="id"
                                        filters={filters}
                                        filterDisplay="row"
                                        emptyMessage="Nada encontrado"
                                        scrollable
                                        scrollHeight="73vh"
                                        selectionMode="single"
                                    >
                                        <Column
                                            header="Nome"
                                            field="nome"
                                            filter
                                            filterPlaceholder="Buscar"
                                            style={{ flexGrow: 0, flexBasis: '80%' }}
                                            showFilterMenu={false}
                                        />
                                        <Column
                                            field="botao"
                                            header="Ação"
                                            body={irBodyTemplate}
                                            className="col-centralizado"
                                            style={{ flexGrow: 0, flexBasis: '20%' }}
                                            
                                        />
                                        <Column
                                            field="botao"
                                            header="Ação"
                                            body={excluirBodyTemplate}
                                            className="col-centralizado"
                                            style={{ flexGrow: 0, flexBasis: '20%' }}
                                        />
                                    </DataTable>

                                </div>

                                {/* Para Cadastrar*/}
                                <Dialog
                                    header="Nova Empresa"
                                    visible={modal}
                                    style={{ width: '50%' }}
                                    footer={modalCadastroFooter}
                                    onHide={
                                        () => {
                                            setmodal(false);
                                            setobrigatorio(false);
                                        }
                                    }
                                >
                                    <div className="field grupo-input-label">
                                        <label>Nome *</label>
                                        <InputText
                                            className="texto-maiusculo"
                                            value={nome}
                                            onChange={(e) => setNome(e.target.value)}
                                        />
                                    </div>
                                    <div className="field grupo-input-label">
                                        <label>Endereço *</label>
                                        <InputText
                                            className="texto-maiusculo"
                                            value={endereco}
                                            onChange={(e) => setEndereco(e.target.value)}
                                        />
                                    </div>
                                    <div className="field grupo-input-label">
                                        <label>CNPJ</label>
                                        <InputMask
                                            value={cnpj}
                                            mask="99.999.999/9999-99"
                                            onChange={(e) => setCnpj(e.target.value)}
                                        />
                                    </div>
                                </Dialog>

                                {/* Para Ver e Editar*/}
                                <Dialog
                                    header="Ediar Empresa"
                                    visible={modaleditar}
                                    style={{ width: '50%' }}
                                    footer={modalEditarFooter}
                                    onHide={
                                        () => {
                                            setmodalEditar(false);
                                            setobrigatorioEditar(false);
                                        }
                                    }
                                >
                                    <div className="field grupo-input-label">
                                        <label>Nome *</label>
                                        <InputText
                                            className="texto-maiusculo"
                                            value={nome}
                                            onChange={(e) => setNome(e.target.value)}
                                        />
                                    </div>
                                    <div className="field grupo-input-label">
                                        <label>Endereço *</label>
                                        <InputText
                                            value={endereco}
                                            className="texto-maiusculo"
                                            onChange={(e) => setEndereco(e.target.value)}
                                        />
                                    </div>
                                    <div className="field grupo-input-label">
                                        <label>CNPJ</label>
                                        <InputMask
                                            value={cnpj}
                                            mask="99.999.999/9999-99"
                                            onChange={(e) => setCnpj(e.target.value)}
                                        />
                                    </div>
                                </Dialog>
                                <br />
                            </div>
                        </div>
                }
            </div>
        </div>
    );
}

export default Empresas;