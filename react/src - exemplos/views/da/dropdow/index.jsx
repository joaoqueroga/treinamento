import React, { useState } from 'react';
import { Dropdown } from 'primereact/dropdown';

const TipoVinculoDropdown = ({ onChange }) => {
  const [selectedOption, setSelectedOption] = useState(null);

  const handleOptionChange = (e) => {
    setSelectedOption(e.value);
    if (onChange) {
      onChange(e.value);
    }
  };

  const options = [
    { label: 'Cedido', value: 'cedidos' },
    { label: 'Terceirizado', value: 'terceirizados' }
  ];
  const dropdownStyle = {
    color: 'darkslategray' // Defina a cor de fonte desejada aqui
  };

  return (
    <Dropdown
      value={selectedOption}
      options={options}
      onChange={handleOptionChange}
      placeholder="Selecione o vínculo"
      style={dropdownStyle}
      
    />
  );
};

export default TipoVinculoDropdown;
