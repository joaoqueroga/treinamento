import React, { useState } from 'react';
import { Dropdown } from 'primereact/dropdown';

const AmazonasDropdown = () => {
  const [selectedState, setSelectedState] = useState(null);

  const handleStateChange = (e) => {
    setSelectedState(e.value);
  };

  const amazonasStates = [
    'Manaus',
    'Parintins',
    'Itacoatiara',
    'Manacapuru',
    'Coari',
    'Tefé',
    'Tabatinga',
    'São Gabriel da Cachoeira',
    'Maués',
    'Manicoré',
    'Humaitá',
    'Iranduba',
    'Autazes',
    'Benjamin Constant',
    'Borba',
    'Careiro',
    'Rio Preto da Eva',
    'Presidente Figueiredo',
    'Boca do Acre',
    'Barcelos',
    'Eirunepé',
    'Codajás',
    'Carauari',
    'Careiro da Várzea',
    'Nova Olinda do Norte',
    'Bom Jesus do Itabapoana',
    'Urucurituba',
    'Santo Antônio do Içá',
    'Silves',
    'Novo Airão',
    'Barreirinha',
    'Tonantins',
    'Manaquiri',
    'Santa Isabel do Rio Negro',
    'Anamã',
    'Anori',
    'Beruri',
    'São Paulo de Olivença',
    'Amaturá',
    'Uarini',
    'Atalaia do Norte',
    'Envira',
    'Fonte Boa',
    'Novo Aripuanã',
    'Guajará',
    'Jutaí',
    'Canutama',
    'Japurá',
    'Juruá',
    'Pauini',
    'Itapiranga',
    'Tapauá',
    'Boca do Acre',
    'Felipe Acre',
    'Nova Olinda do Norte',
    'Lábrea',
    'Ipixuna',
    'Codajás',
    'Urucurituba',
    'Urucará',
    'Itamarati',
    'Eirunepé',
    'Nhamundá',
    'Fonte Boa',
    'Tefé',
    'Amaturá',
    'Barreirinha',
    'Careiro',
    'Alvarães',
    'Coari',
    'Novo Aripuanã',
    'Beruri',
    'Itapiranga',
    'Parintins',
    'Tapauá',
    'Juruá',
    'São Gabriel da Cachoeira',
    'Silves',
    'Jutaí',
    'Maués',
    'São Paulo de Olivença',
    'Manacapuru',
    'Pauini',
    'Santa Isabel do Rio Negro',
    'Borba',
    'Codajás',
    'Santo Antônio do Içá',
    'Anamã',
    'Tonantins',
    'Boca do Acre',
    'Tefé',
    'Manaquiri',
    'Boca do Acre',
    'Anamã',
    'Anori',
    'Beruri',
    'São Paulo de Olivença',
    'Amaturá',
    'Uarini',
    'Atalaia do Norte',
    'Envira',
    'Fonte Boa',
    'Novo Aripuanã',
    'Guajará',
    'Jutaí',
    'Canutama',
    'Japurá',
    'Juruá',
    'Pauini',
    'Itapiranga',
    'Tapauá'
  ];

  const stateOptions = amazonasStates.map((state) => ({
    label: state,
    value: state
  }));

  return (
    <Dropdown
      value={selectedState}
      options={stateOptions}
      onChange={handleStateChange}
      placeholder="Selecione um estado"
      style={{width: '100%'}}
    />
  );
};

export default AmazonasDropdown;