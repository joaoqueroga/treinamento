import React, {useState, useEffect} from "react";
import './style.scss';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { InputNumber } from 'primereact/inputnumber';
import { FilterMatchMode } from 'primereact/api';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import Swal from 'sweetalert2';
import { ProgressSpinner } from 'primereact/progressspinner';
import { BreadCrumb } from 'primereact/breadcrumb';
import SeletorTipoEstagio from "../../../components/seletorTipoEstagio";

import api from "../../../config/api";
import { getToken, getUserId } from "../../../config/auth";

function LotacaoEstagio() {

    const items = [
        { label: 'Coordenação de Estágio' },
        { label: 'Vagas por Lotação' }
    ];
    const home = { icon: 'pi pi-home', url: '/inicio' }
    const [lotacoes, setLotacoes] = useState([]);
    const [modallotacaoeditar, setModallotacaoEditar] = useState(false);
    const [obrigatorioeditar, setobrigatorioEditar] = useState(false);
    const [nome, setNome] = useState('');
    const [sigla, setSigla] = useState('');
    const [tipoestagio, setTipoestagio] = useState(null);
    const [vagas, setvagas] = useState(0);
    const [vagas_estagio, setVagas_estagio] = useState([]);
    const [loading, setLoading] = useState(true);
    const [edicao, setedicao] = useState(false);
    const [index_edicao, setIndex_edicao] = useState(0);

    const [id, setId] = useState(null);

    useEffect(() => {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId()
        }
        api.post('/rh/lotacoes/',data , config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === 'S'){
                setLotacoes(x.lotacao);
                setLoading(false);
            }else{
                Swal.fire({
                    icon:'error',
                    title:"Erro",
                    text: `${x.motivo}`,
                    confirmButtonText: 'fechar',
                })
            }
        })
    }, []);

    const irBodyTemplate = (rowData) => {
        return(
            <span>
                <Button 
                    onClick={()=>seleciona(rowData)}
                    icon="pi pi-pencil"
                    className="btn-green"
                    title="Editar unidade"
                />
            </span>
        )                                    
    }

    const [filters] = useState({
        'sigla': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'descricao': { value: null, matchMode: FilterMatchMode.CONTAINS }
    });

    function seleciona(obj) {
        setId(obj.id_lotacao);
        setNome(obj.descricao);
        setSigla(obj.sigla);
        setVagas_estagio(obj.vagas_estagio);
        setModallotacaoEditar(true);
    }

    function liparDados() {
        setTipoestagio(null);
        setvagas(0);
        setedicao(false);
    }

    function reloadLotacao() {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId()
        }
        api.post('/rh/lotacoes/',data , config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === 'S'){
                setLotacoes(x.lotacao);
            }
        })
    }

    function editarLotacao() {
        if(nome){
            let dados = {
                aplicacao: "SGI",
                id_usuario: getUserId(),
                id_lotacao: id,
                vagas_estagio: vagas_estagio,
                excluir: false
            }

            let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
            api.post('/rh/cadastro_lotacao/',dados, config)
            .then((res)=>{
                let x = JSON.parse(res.data);
                if(x.sucesso === "S"){
                    // setModallotacao(false);
                    Swal.fire({
                        icon: 'success',
                        title:"Sucesso",
                        text: `Lotação atualizada`,
                        confirmButtonText: 'fechar',
                    })
                    reloadLotacao();
                    liparDados();
                }else{
                    // setModallotacao(false);
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `${x.motivo}`,
                        confirmButtonText: 'fechar',
                    })
                }
            })

            liparDados();
            setModallotacaoEditar(false);
        }else{
            setobrigatorioEditar(true);
        }
    }


    const modalEditarFooter = () => {
        return (
            <div>
                {obrigatorioeditar?<p><small className="p-error">Informe os capos obrigatórios (*).</small></p>:null}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setModallotacaoEditar(false);
                        setobrigatorioEditar(false);
                        liparDados();
                    }} 
                    className="btn-2" />
                <Button 
                    label="Salvar" 
                    icon="pi pi-check" 
                    onClick={()=>{
                        editarLotacao();
                    }} 
                    className="btn-1"
                />
            </div>
        );
    }

    function adicionarVaga() {
        if(tipoestagio && vagas){
            let aux = {
                id_tipo_estagio: tipoestagio.id_tipo_estagio,
                vagas_total: vagas,
                vagas_ocupadas: 0,
                tipo_estagio_descricao: tipoestagio.tipo_estagio_descricao,
                excluir: false
            };
            setVagas_estagio(prevState => [...prevState, aux]);
            setTipoestagio(null);
            setvagas(0);
        }else{
            Swal.fire({
                icon: 'warning',
                title:"Atenção",
                text: `Preencha todos os campos`,
                confirmButtonText: 'fechar',
            })
        }
    }

    function selecionaEdicao(obj) {
        setedicao(true);
        setvagas(obj.vagas_total)
        setTipoestagio(
            {
                "tipo_estagio_descricao" : obj.tipo_estagio_descricao,
                "id_tipo_estagio": obj.id_tipo_estagio,
                "vagas_ocupadas": obj.vagas_ocupadas,
                "vagas_total": obj.vagas_total,
                "excluir": obj.excluir
            }
        )
    }

    function atualizarObjeto() {
        let aux = vagas_estagio;
        aux[index_edicao].tipo_estagio_descricao = tipoestagio.tipo_estagio_descricao;
        aux[index_edicao].vagas_total = vagas;       
        setVagas_estagio([...aux]);
        setedicao(false);
        liparDados();
    }

  
    function excluirVaga(index){
        Swal.fire({
            showCancelButton: true,
            confirmButtonText: 'Sim',
            cancelButtonText: 'Não',
            icon: 'question',
            reverseButtons: true,
            text: "Escluir tipo de estágio?"
        }).then((result) => {
            if (result.isConfirmed) {
                let aux = vagas_estagio;
                aux[index].excluir = true;
                setVagas_estagio([...aux]);
                setedicao(false);
                liparDados();
            }
        })
    }
  

    return (
        <div className='view'>
        <div className="view-body">
        {
        loading?
        <div className="loading-pagina" ><ProgressSpinner/>Carregando...</div>
        :
        <div>
            <div className="header">
                <BreadCrumb model={items} home={home}/>
                <h6 className="titulo-header">Vagas por Lotação</h6>
            </div>
            <div className="top-box">
                <h1>Vagas por Lotação</h1>
            </div>

            <div className="lotacao-body">
                <div className="dados-lotacoes">

                <DataTable
                    value={lotacoes}
                    size="small"
                    className="tabela-servidores"
                    dataKey="id"
                    filters={filters}
                    filterDisplay="row"
                    emptyMessage="Nada encontrado"
                    scrollable
                    scrollHeight="73vh"
                    selectionMode="single"
                >
                    <Column
                        header="Sigla"
                        field="sigla"
                        filter 
                        filterPlaceholder="Buscar" 
                        style={{ flexGrow: 0, flexBasis: '20%' }}
                        showFilterMenu={false}
                    />
                    <Column
                        header="Nome"
                        field="descricao"
                        filter 
                        filterPlaceholder="Buscar" 
                        style={{ flexGrow: 0, flexBasis: '70%' }}
                        showFilterMenu={false}
                    />
                    <Column 
                        field="botao" 
                        header="Ação" 
                        body={irBodyTemplate} 
                        className="col-centralizado" 
                        style={{ flexGrow: 0, flexBasis: '10%' }}
                    />

                </DataTable>

                </div>

                {/* Para Ver e Editar*/}
                <Dialog 
                    header="Definir vagas por unidade de lotação"
                    visible={modallotacaoeditar}
                    style={{ width: '70%' }}
                    footer={modalEditarFooter}
                    onHide={
                        () =>{
                            setModallotacaoEditar(false);
                            setobrigatorioEditar(false);
                            liparDados();
                        }
                    }
                >
                    <span className="field grupo-input-label">
                        <label>Nome da unidade</label>
                        <InputText 
                            value={nome}
                            disabled
                        />
                    </span>
                    <span className="field grupo-input-label">
                        <label>Sigla da unidade</label>
                        <InputText 
                            value={sigla}
                            disabled
                        />
                    </span>

                    <br />
                   
                    <span className='field-triplo'>
                        <div style={{ width: '50%' }}>
                            <label>Seleção de tipo de estágio </label>
                            <SeletorTipoEstagio
                                get={tipoestagio}
                                set={setTipoestagio}
                            />
                        </div>
                        <div style={{ width: '35%' }}>
                            <span className="field field-num-vagas">
                                <label>Vagas</label>
                                <InputNumber
                                    inputId="minmax-buttons" 
                                    value={vagas} 
                                    onValueChange={(e)=>setvagas(e.target.value)} 
                                    mode="decimal" 
                                    showButtons 
                                    min={0} 
                                    max={100} 
                                />
                            </span>
                        </div>
                        <div style={{ width: '15%' }}>
                            <span className="field grupo-input-label">
                                {
                                    edicao?
                                        <Button 
                                            label="Atualizar" 
                                            style={{border:"none !important"}}
                                            icon="pi pi-sync" 
                                            onClick={()=>{
                                                atualizarObjeto();
                                            }} 
                                            className="btn-2"
                                        />
                                    :
                                        <Button 
                                            label="Adicionar" 
                                            style={{border:"none !important"}}
                                            icon="pi pi-plus" 
                                            onClick={()=>{
                                                adicionarVaga();
                                            }} 
                                            className="btn-1"
                                        />
                                }
                            </span>
                        </div>
                    </span>
                    <br />
                    <div className="lista-tipos-estagio">
                        {
                            vagas_estagio.map((e, index)=>{
                                if(e.excluir === false){
                                    return(
                                        <div key={index} className="vaga_estagio">
                                            <span id="vaga-estagio-descricao">
                                                {e.tipo_estagio_descricao}
                                            </span>
                                            <span id="vaga-estagio-vagas">
                                                {e.vagas_total} Vagas
                                            </span>
                                            <span id="vaga-estagio-vagas">
                                                {e.vagas_ocupadas} Ocupadas
                                            </span>
                                            <span id="vaga-estagio-botoes">
                                                <Button 
                                                    onClick={()=>{ selecionaEdicao(e); setIndex_edicao(index) }}
                                                    icon="pi pi-pencil"
                                                    className="btn-green"
                                                    title="Editar"
                                                />
                                                <Button 
                                                    onClick={()=>excluirVaga(index)}
                                                    icon="pi pi-trash"
                                                    className="btn-red"
                                                    title="Remover"
                                                />
                                            </span>
                                            
                                        </div>
                                    )
                                }
                            })
                        }
                    </div>
                </Dialog>
                <br/>
            </div>
        </div>
        }
        </div>
        </div>
    );
}

export default LotacaoEstagio;