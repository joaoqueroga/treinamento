import React from 'react';
import '../style.scss';

import { BiCloudUpload } from "react-icons/bi";
import { InputText } from 'primereact/inputtext';
import { Dropdown } from 'primereact/dropdown';
import { InputMask } from 'primereact/inputmask';
import foto_padrao from '../../../../images/default.jpg';
import SeletorData from '../../../../components/SeletorData';
import SeletorMunicipio from '../../../../components/SeletorMunicipio';
//auxs
import grupossanguineos from '../../../../jsons/gruposanguineos.json';
import { urlFiles } from '../../../../config/config';


function DadosPessoaisEstagiario(props) {
  
    const fatorRH = [
        {
            "nome": "POSITIVO"
        },
        {
            "nome": "NEGATIVO"
        }
    ]
  
    return ( 

        <div className="form-cadastro-servidor">
            <div className='painel-cadastro-servidor'>
                <span className='grupo-input-mat grupo-input-label'>
                    Número do Termo de Contrato de Estágio*
                    <InputText 
                        style={props.obrigatorio.get?{borderColor:"#DE4B24"}:{}}
                        type="text" 
                        className="p-inputtext-sm input-mat"
                        value={props.matricula.get} 
                        onChange={(e) => props.matricula.set(e.target.value)} 
                        disabled={!props.ativos}
                    />
                </span>
                <span className='grupo-input-foto'>
                    {
                        props.editar?
                            props.foto.get?
                                <img src={URL.createObjectURL(props.foto.get)} alt="foto" className="foto-servidor"/>
                            :
                                props.caminho_foto.get?
                                    <img src={`${urlFiles}${props.caminho_foto.get}`} alt="foto" className="foto-servidor"/>
                                :
                                    <img src={foto_padrao} alt="foto" className="foto-servidor"/>
                        :
                            props.foto.get?
                                <img src={URL.createObjectURL(props.foto.get)} alt="foto" className="foto-servidor"/>
                            :
                                <img src={foto_padrao} alt="foto" className="foto-servidor"/>
                                
                    }
                    
                    <label className='label-file'>
                        <BiCloudUpload size={20}/>
                        <input
                            type="file"
                            id="image" 
                            className='input-files' 
                            accept='image/*' 
                            onChange={e=>props.foto.set(e.target.files[0])}
                            disabled={!props.ativos}
                        />
                    </label>
                </span>
                
            </div>
            <h4 className='rotulo-formulario'>Dados Pessoais</h4>
            <div className='painel-cadastro-servidor'>
                <span className='grupo-input-label'>
                    Nome *
                    <InputText 
                        type="text"
                        style={props.obrigatorio.get?{borderColor:"#DE4B24", height: "40px"}:{height: "40px"}} 
                        className="p-inputtext-sm texto-maiusculo"
                        value={props.nome.get} 
                        onChange={(e) => props.nome.set(e.target.value)}
                        disabled={!props.ativos}
                    />
                </span>
                <span className='grupo-input-label'>
                    Nome Social (Opcional)
                    <InputText 
                        type="text" 
                        className="p-inputtext-sm texto-maiusculo"
                        value={props.nome_social.get} 
                        onChange={(e) => props.nome_social.set(e.target.value)}
                        style={{height: "40px"}}
                        disabled={!props.ativos}
                    />
                </span>
                
                <span className='grupo-input-label'>
                    Data de Nascimento
                    <SeletorData
                        get={props.nascimento.get}
                        set={props.nascimento.set}
                    />
                </span>
                <span className='grupo-input-label grupo-input-label-2cols'>
                    <div className='grupo-input-label-grad'>
                        Grupo Sanguíneo
                        <Dropdown 
                            value={props.grupo_sanguineo.get} 
                            options={grupossanguineos} 
                            onChange={(e)=>props.grupo_sanguineo.set(e.value)} 
                            optionLabel="nome" 
                            className='input-select p-inputtext-sm'
                            style={{height: "40px"}}
                            disabled={!props.ativos}
                        />                        
                    </div>
                    <div className='grupo-input-label-grad'>
                        Fator RH
                        <Dropdown 
                            value={props.fator_rh.get} 
                            options={fatorRH} 
                            onChange={(e)=>props.fator_rh.set(e.value)} 
                            optionLabel="nome" 
                            className='input-select p-inputtext-sm'
                            style={{height: "40px"}}
                            disabled={!props.ativos}
                        />
                    </div>

                </span>
            </div>

            <h4 className='rotulo-formulario'>Endereço</h4>
            <div className='painel-cadastro-servidor'>
                <span className='grupo-input-label'>
                    Tipo Logradouro 
                    <Dropdown 
                        value={props.tipologradouro.get} 
                        options={props.tiposlogradouro} 
                        onChange={(e)=>props.tipologradouro.set(e.value)} 
                        optionLabel="descricao" 
                        className='input-select p-inputtext-sm'
                        style={{height: "40px"}}
                        disabled={!props.ativos}
                    />
                </span>
                <span className='grupo-input-label'>
                    Logradouro 
                    <InputText 
                        type="text" 
                        className="p-inputtext-sm texto-maiusculo"
                        value={props.logradouro.get} 
                        onChange={(e) => props.logradouro.set(e.target.value)}
                        style={{height: "40px"}}
                        disabled={!props.ativos}
                    />
                </span>
                <span className='grupo-input-label'>
                    Número
                    <InputText 
                        type="text" 
                        className="p-inputtext-sm"
                        value={props.numero.get} 
                        onChange={(e) => props.numero.set(e.target.value)}
                        style={{height: "40px"}}
                        disabled={!props.ativos}
                    />
                </span>

                <span className='grupo-input-label'>
                    Bairro 
                    <InputText 
                        type="text" 
                        className="p-inputtext-sm texto-maiusculo"
                        value={props.bairro.get} 
                        onChange={(e) => props.bairro.set(e.target.value)}
                        style={{height: "40px"}}
                        disabled={!props.ativos}
                    />
                </span>
                <span className='grupo-input-label'>
                    Complemento
                    <InputText 
                        type="text" 
                        className="p-inputtext-sm texto-maiusculo"
                        value={props.complemento.get} 
                        onChange={(e) => props.complemento.set(e.target.value)}
                        style={{height: "40px"}}
                        disabled={!props.ativos}
                    />
                </span>
                <span className='grupo-input-label grupo-input-label-2cols'>
                    <div className='grupo-input-label-grad'>
                        CEP
                        <InputMask
                            mask='99999-999' 
                            type="text" 
                            className="p-inputtext-sm"
                            value={props.cep.get} 
                            onChange={(e) => props.cep.set(e.target.value)}
                            style={{height: "40px", width: "100%"}}
                            disabled={!props.ativos}
                        />
                    </div>
                    <div className='grupo-input-label-grad'>
                        Município
                        <SeletorMunicipio
                            get={props.endmunicipio.get}
                            set={props.endmunicipio.set}                          
                        />
                    </div>
                </span>
            </div>
            <h4 className='rotulo-formulario'>Contatos</h4>
            <div className='painel-cadastro-servidor'>
                <span className='grupo-input-label'>
                    Email 
                    <InputText 
                        type="text" 
                        className="p-inputtext-sm"
                        value={props.email.get} 
                        onChange={(e) => props.email.set(e.target.value)}
                        style={{height: "40px"}}
                        disabled={!props.ativos}
                    />
                </span>
                <span className='grupo-input-label'>
                    Telefone Principal
                    <InputText 
                        type="text" 
                        className="p-inputtext-sm"
                        value={props.telefone.get} 
                        onChange={(e) => props.telefone.set(e.target.value)}
                        style={{height: "40px"}}
                        disabled={!props.ativos}
                    />
                </span>
                <span className='grupo-input-label'>
                    <label>Telefone de Emergência </label>
                    <InputText 
                        type="text" 
                        className="p-inputtext-sm"
                        value={props.telefone_emergencia.get} 
                        onChange={(e) => props.telefone_emergencia.set(e.target.value)}
                        style={{height: "40px"}}
                        disabled={!props.ativos}
                    />
                </span>
                <span className='grupo-input-label'>
                    <label>Nome do Contato de Emergência</label>
                    <InputText 
                        type="text" 
                        className="p-inputtext-sm texto-maiusculo"
                        value={props.nome_contato_emergencia.get} 
                        onChange={(e) => props.nome_contato_emergencia.set(e.target.value)}
                        style={{height: "40px"}}
                        disabled={!props.ativos}
                    />
                </span>
            </div>
            <h4 className='rotulo-formulario'>Documentos</h4>
            <div className='painel-cadastro-servidor'>

                <span className='grupo-input-label'>
                    CPF *
                    <InputMask
                        mask='999.999.999-99' 
                        type="text" 
                        className="p-inputtext-sm"
                        value={props.cpf.get} 
                        onChange={(e) => props.cpf.set(e.target.value)}
                        style={props.obrigatorio.get?{borderColor:"#DE4B24", height: "40px"}:{height: "40px"}} 
                        disabled={!props.ativos}
                    />
                </span>
                <span className='grupo-input-label'>
                    RG 
                    <InputText 
                        type="text" 
                        className="p-inputtext-sm"
                        value={props.rg.get} 
                        onChange={(e) => props.rg.set(e.target.value)}
                        style={{height: "40px"}}
                        disabled={!props.ativos}
                    />
                </span>
            </div>
            <h4 className='rotulo-formulario'>Dados Bancários</h4>
            <div className='painel-cadastro-servidor'>
                <span className='grupo-input-label'>
                    Banco
                    <InputText 
                        type="text" 
                        className="p-inputtext-sm texto-maiusculo"
                        style={{height: "40px"}}
                        value={props.banco.get} 
                        onChange={(e) => props.banco.set(e.target.value)}
                        disabled={!props.ativos}
                    />
                </span>
                <span className='grupo-input-label'>
                    Agência
                    <InputText 
                        type="text" 
                        className="p-inputtext-sm"
                        style={{height: "40px"}}
                        value={props.agencia.get} 
                        onChange={(e) => props.agencia.set(e.target.value)}
                        disabled={!props.ativos}
                    />
                </span>
                <span className='grupo-input-label'>
                    Tipo da Conta
                    <InputText 
                        type="text" 
                        className="p-inputtext-sm texto-maiusculo"
                        style={{height: "40px"}}
                        value={props.tipo_conta.get} 
                        onChange={(e) => props.tipo_conta.set(e.target.value)}
                        disabled={!props.ativos}
                    />
                </span>
                <span className='grupo-input-label'>
                    Conta
                    <InputText 
                        type="text" 
                        className="p-inputtext-sm"
                        style={{height: "40px"}}
                        value={props.conta.get} 
                        onChange={(e) => props.conta.set(e.target.value)}
                        disabled={!props.ativos}
                    />
                </span>
            </div>
            <br/>
        </div>
    );
}

export default DadosPessoaisEstagiario;