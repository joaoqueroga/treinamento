import React from 'react';
import '../style.scss';
import SeletorData from '../../../../components/SeletorData';
import { SelectButton } from 'primereact/selectbutton';
import SeletorServidor from '../../../../components/SeletorServidor';
import SeletorLotacao from '../../../../components/SeletorLotacao';
import { InputText } from 'primereact/inputtext';
import { InputTextarea } from 'primereact/inputtextarea';
import { Dropdown } from 'primereact/dropdown';
import Swal from 'sweetalert2';

import SeletorAgenteIntegracao from '../../../../components/seletorAgenteIntegracao';
import SeletorInstituicaoEnsino from '../../../../components/seletorInstituicaoEnsino';

function DadosFuncionaisEstagiario(props) {

    const opcoes = [{ name: 'Não', value: false }, { name: 'Sim', value: true }];

    function desativarUsuario(v) {
        if (v !== null) {
            Swal.fire({
                showCancelButton: true,
                confirmButtonText: 'Confirmar',
                cancelButtonText: 'Cancelar',
                title: 'Cuidado',
                icon: 'warning',
                reverseButtons: true,
                html:
                    '<p><b>Desativar um servidor pode ocasionar várias ações</b> </p>' +
                    '<br/>' +
                    '<ul class="texto_alerta_desativar"> ' +
                    '<li> Os acessos para alguns sistemas serão retirados.</li>' +
                    '<li> Após a desativação não será possível reativar via sistema.</li>' +
                    '</ul>' +
                    '<br/>' +
                    '<p>Todas as modificação só serão aplicadas após salvar o formulário</p>'
            }).then((result) => {
                if (result.isConfirmed) {
                    props.ativo.set(v);
                }
            })
        }
    }

    return (

        <div className="form-cadastro-servidor form-servidor-dados-funcionais">
            <div className='painel-cadastro-servidor'>
                <span className='grupo-input-label'>
                    Lotação *
                    <SeletorLotacao
                        get={props.lotacao.get}
                        set={props.lotacao.set}
                        lotacao_chefe={props.selecaoLotacaoChefe}
                        disabled={!props.ativos}
                    />
                </span>
                <span className='grupo-input-label'>
                    Responsável pela Lotação
                    <SeletorServidor
                        get={props.chefe_imediato.get}
                        set={props.chefe_imediato.set}
                        chefe={true}
                        
                    />
                </span>


                <span className='grupo-input-label'>
                    <label> Tipo de Estágio * <i>(Requer uma Lotação)</i></label>
                    {
                        !props.selecao_lotacao ?
                            <InputText
                                style={props.obrigatorio.get ? { borderColor: "#DE4B24", height: "40px" } : { height: "40px" }}
                                className="p-inputtext-sm"
                                value={props.descricao_tipo_estagio}
                                disable
                                disabled={!props.ativos}
                            />
                            :
                            <Dropdown
                                style={props.obrigatorio.get ? { borderColor: "#DE4B24", height: "40px" } : { height: "40px" }}
                                value={props.tipo_estagio.get}
                                options={props.tipos_estagio.get}
                                onChange={(e) => props.selecionaTipoEstagio(e.value)}
                                optionLabel="tipo_estagio_descricao"
                                className='input-select p-inputtext-sm input-'
                                disabled={!props.ativos}
                            />
                    }
                </span>
                <span className='grupo-input-label grupo-input-label-2cols'>
                    <span className='grupo-input-label-grad'>
                        <label style={{ margin: '0px', display: 'block', width: '100%' }}>Valor da Bolsa</label>
                        <div className="p-inputgroup">
                            <span className="p-inputgroup-addon" style={{ fontSize: '14px', borderRight: '1px solid #ced4da' }}>R$</span>
                            <InputText
                                type="text"
                                className="p-inputtext-sm"
                                style={{ height: "40px" }}
                                value={props.bolsa.get}
                                disabled
                                onChange={(e) => props.bolsa.set(e.target.value)}
                            />
                        </div>
                    </span>
                    <span className='grupo-input-label-grad'>
                        <label style={{ margin: '0px', display: 'block', width: '100%' }}>Valor do Vale Transporte</label>
                        <div className="p-inputgroup">
                            <span className="p-inputgroup-addon" style={{ fontSize: '14px', borderRight: '1px solid #ced4da' }}>R$</span>
                            <InputText
                                type="text"
                                className="p-inputtext-sm"
                                style={{ height: "40px" }}
                                value={props.valor_transporte.get}
                                disabled
                                onChange={(e) => props.valor_transporte.set(e.target.value)}
                            />
                        </div>
                    </span>
                </span>

                <span className='grupo-input-label grupo-input-label-2cols'>
                    <div className='grupo-input-label-grad'>
                        Data do início do contrato
                        <SeletorData
                            get={props.inicio_contrato.get}
                            set={props.inicio_contrato.set}
                        />
                    </div>
                    {/* span para adição de um novo iput */}
                    <span>
                    
                    </span>
                </span>

                <span className='grupo-input-label grupo-input-label-2cols'>
                    <span className='grupo-input-label-grad'>
                        Data de Referência de férias
                        <SeletorData
                            get={props.ferias_referencia.get}
                            set={props.ferias_referencia.set}
                        />
                    </span>
                    <span className='grupo-input-label-grad'>
                        Data de desligamento
                        <SeletorData
                            get={props.desligamento.get}
                            set={props.desligamento.set}
                        />
                    </span>
                </span>

                <span className='grupo-input-label'>
                    Supervisor
                    <SeletorServidor
                        get={props.supervisor.get}
                        set={props.supervisor.set}
                        funcao={props.dadosSupervisor}
                        disabled={!props.ativos}
                    />
                </span>
                <span className='grupo-input-label grupo-input-label-2cols'>
                    <div className='grupo-input-label-grad' style={{ width: "50%" }}>
                        Formação do Supervisor
                        <InputText
                            value={props.formacao_supervisor.get}
                            disabled={true}
                            className='input-select p-inputtext-sm'
                            style={{ height: "40px" }}
                        />
                    </div>
                    <div className='grupo-input-label-grad' style={{ width: "50%" }}>
                        Contato do Supervisor
                        <InputText
                            type="text"
                            className="p-inputtext-sm"
                            style={{ height: "40px", width: '100%' }}
                            value={props.contato_supervisor.get}
                            disabled={true}
                        />
                    </div>
                </span>

                <span className='grupo-input-label'>
                    Cargo do Supervisor
                    <InputText
                        value={props.cargo_supervisor.get}
                        disabled={true}
                        className='input-select p-inputtext-sm'
                        style={{ height: "40px" }}
                    />
                </span>
                <span className='grupo-input-label'></span>
                <span className='grupo-input-label'>
                    Assessor do Supervisor
                    <SeletorServidor
                        get={props.acessor_supervisor.get}
                        set={props.acessor_supervisor.set}
                        funcao={props.dadosAcessorSupervisor}
                    />
                </span>
                <span className='grupo-input-label'>
                    Contato do Assessor do Supervisor
                    <InputText
                        type="text"
                        className="p-inputtext-sm"
                        style={{ height: "40px" }}
                        value={props.contato_acessor.get}
                        disabled={true}
                    />
                </span>

                <span className='grupo-input-label'>
                    Agente de Integração
                    <div className='seletor-3col'>
                        <SeletorAgenteIntegracao
                            get={props.agente_integracao.get}
                            set={props.agente_integracao.set}
                        />
                    </div>
                </span>

                <span className='grupo-input-label'>
                    Instituição de Ensino
                    <div className='seletor-3col'>
                        <SeletorInstituicaoEnsino
                            get={props.instituicao_ensino.get}
                            set={props.instituicao_ensino.set}
                        />
                    </div>
                </span>

                <span className='grupo-input-label' style={{ marginTop: '-14px' }}>
                    Curso
                    <InputText
                        type="text"
                        className="p-inputtext-sm texto-maiusculo"
                        style={{ height: "40px" }}
                        value={props.curso.get}
                        onChange={(e) => props.curso.set(e.target.value)}
                        disabled={!props.ativos}
                    />
                </span>

                <span className='grupo-input-label grupo-input-label-2cols'>
                    <div className='grupo-input-label' style={{ width: '50%' }}>
                        Período
                        <InputText
                            type="text"
                            className="p-inputtext-sm"
                            style={{ height: "40px" }}
                            value={props.periodo.get}
                            onChange={(e) => props.periodo.set(e.target.value)}
                            disabled={!props.ativos}
                        />
                    </div>
                    <div className='grupo-input-label' style={{ width: '50%' }}>
                        Tipo do Período
                        <InputText
                            type="text"
                            className="p-inputtext-sm texto-maiusculo"
                            style={{ height: "40px" }}
                            value={props.tipo_periodo.get}
                            onChange={(e) => props.tipo_periodo.set(e.target.value)}
                            disabled={!props.ativos}
                        />
                    </div>
                </span>

                <span className='grupo-input-label'>
                    Atividades do estagiário
                    <InputTextarea
                        type="text"
                        className="p-inputtext-sm"
                        value={props.atividades.get}
                        onChange={(e) => props.atividades.set(e.target.value)}
                        rows={2}
                        disabled={!props.ativos}
                    />
                </span>

                <span className='grupo-input-label'>

                    <span className='grupo-input-label select-estagio-ativo'>
                        Estagiário Ativo?
                        <SelectButton
                            className="p-button-sm"
                            value={props.ativo.get}
                            optionLabel="name"
                            options={opcoes}
                            onChange={(e) => desativarUsuario(e.value)}
                            disabled={!props.ativos}
                        />
                    </span>

                </span>


            </div>
            <br />
        </div>
    );
}

export default DadosFuncionaisEstagiario;