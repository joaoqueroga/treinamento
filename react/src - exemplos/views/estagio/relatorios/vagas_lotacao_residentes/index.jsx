import React, {useState} from "react";
import '../style.scss';
import { Dialog } from 'primereact/dialog';
import { Button } from 'primereact/button';
import jsPDF from 'jspdf';
import autoTable from 'jspdf-autotable';
import logo from '../../../../images/logo_h.png'
import TelaSpinner from "../../../../components/spinner";
import Swal from 'sweetalert2';

import api from "../../../../config/api";
import { getToken, getUserId } from "../../../../config/auth";

function RelatorioVagasPorlotacaoResidentes() {


    const [displayPDF, setDisplayPDF] = useState(false);

    //gerar o PDF

    const user = sessionStorage.getItem("nome");

    function baixarpdf() {

        
        setDisplayPDF(true);
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            "tipo_listagem": "RESIDENTE"
        }
        api.post('/estagio/rel_lotacao_estagio_vagas/',data, config)
        .then((res)=>{
            try {
                let x = JSON.parse(res.data);
                let total = x.vagas_total;
                let ocupadas = x.vagas_ocupadas;
                let disponiveis = x.vagas_disponiveis;
                generatePDF(x.lista[0], total, ocupadas, disponiveis);
            } catch (error) {
                setDisplayPDF(false);
                Swal.fire({
                    icon:'error',
                    title:"Erro",
                    text: `${error}`,
                    confirmButtonText: 'fechar',
                })
            }
        })
    }

    function coluna( x, y, tam, rotulo, texto, doc) {
        doc.setFont('helvetica', 'bold');
        doc.text(x, y, `${rotulo}:`);
        doc.setFont('helvetica', 'normal');
        doc.text(x+tam, y, `${texto?texto:""}`);
    }

    function generatePDF (d, total, ocupadas, disponiveis){

        let data = new Date();
        let dia = `${data.getDate()}/${data.getMonth()+1}/${data.getFullYear()}`;
        let hora = data.toLocaleTimeString();

       
        let doc = new jsPDF('l', 'pt');
        doc.addImage(logo, 'PNG', 40, 20, 155, 30 );
        
        doc.setFont('helvetica', 'bold');
        doc.setFontSize(18);
        doc.setTextColor(20,71,88);
        doc.text(450, 40, 'VAGAS DE ESTAGIÁRIOS RESIDENTES');

        //coluna 1: x=50 , coluna 2 x=400

        let y = 60;
        let espaco = 18;

        doc.setFontSize(12);
        doc.setTextColor(0,0,0);

        if(d.length > 0){

            y+=espaco;
            coluna(40, y,  115, "Total de vagas", total, doc);
            y+=espaco;
            coluna(40, y,  115, "Vagas ocupadas", ocupadas, doc);
            y+=espaco;
            coluna(40, y,  115, "Vagas disponíveis", disponiveis, doc);

            y+=espaco;

            autoTable(doc, ({
                startY: y,
                body: d,
                theme: 'striped',
                styles: {
                    fontSize: 9,
                    cellPadding: 2,
                    halign: 'left',
                    valign: 'middle',
                    textColor: "#000",
                    lineColor:"#000",
                    fillColor:"#fff"
                },
                columns: [
                    { header: 'Unidade de Lotação', dataKey: 'descricao_lotacao' },
                    { header: 'TOTAL disponíveis', dataKey: 'TOTAL - DISPONIVEL' },
                    { header: 'TOTAL ocupadas', dataKey: 'TOTAL - OCUPADAS' },
                    { header: 'TOTAL vagas', dataKey: 'TOTAL' },
                ],
            }))
        }else{
            y+=espaco;
            coluna(40, y,  0, "", "Sem dados", doc);
        }

        
        //rodapé da pagina
        let pageCount = doc.internal.getNumberOfPages()
        doc.setFontSize(8)
        for (var i = 1; i <= pageCount; i++) {
            doc.setPage(i)
            doc.text('DEFENSORIA PÚBLICA DO ESTADO DO AMAZONAS - AV. ANDRE ARAUJO, 679, ALEIXO  CNPJ: 19.421.427/0001-91', doc.internal.pageSize.width / 2, 570, {
            align: 'center'});
            doc.text(`Emitido por ${user} em ${dia}  ${hora}` + ' - Página ' + String(i) + ' de ' + String(pageCount), doc.internal.pageSize.width / 2, 580, {
            align: 'center'});
        }

        setDisplayPDF(false);
        //doc.save(`${s.nome} ${new Date().toLocaleDateString()} ${new Date().toLocaleTimeString()}.pdf`)
        window.open(doc.output('bloburl', { filename: `ficha_funcional.pdf` }), '_blank');
    } 


    return (
        <div>
            <Button 
                label="Relatório de Vagas Residentes" 
                icon="pi pi-file-pdf" 
                className="btn-2"
                onClick={()=>baixarpdf()}
            />

            
            <Dialog
                visible={displayPDF} 
                style={{ width: '50vw' }}
                closable={false}
            >
                <TelaSpinner tamanho={100} texto={"Gerando Documento"}/>
            </Dialog>
        </div>
    );
}

export default RelatorioVagasPorlotacaoResidentes;




