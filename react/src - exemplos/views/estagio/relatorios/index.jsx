import React from "react";
import './style.scss';
import RelatorioFichaFuncionalEstagio from "./ficha_funcional";
import RelatorioVagasLotacao from "./vagas_lotacoes";
import RelatorioEstagiariosLotacao from "./estagiarios_lotacao";
import { BreadCrumb } from 'primereact/breadcrumb';
import RelatorioVagasPorlotacaoResidentes from "./vagas_lotacao_residentes";


function RelatoriosEstagio() {

    const items = [
        { label: 'Coordenação de Estágio' },
        { label: 'Relatórios' }
    ];
    const home = { icon: 'pi pi-home', url: '/inicio' }

    return (
        <div className='view'>
            <div className="view-body">
                <div className="header">
                    <BreadCrumb model={items} home={home}/>
                    <h6 className="titulo-header">Relatórios</h6>
                </div>
                <div className="top-box">
                    <h1>Relatórios</h1>
                </div>
                <div className="relatorios-main">
                    <RelatorioFichaFuncionalEstagio/>
                    <RelatorioVagasLotacao/>
                    <RelatorioVagasPorlotacaoResidentes/>
                    <RelatorioEstagiariosLotacao/>
                </div>
            </div>
        </div>
    );
}

export default RelatoriosEstagio;