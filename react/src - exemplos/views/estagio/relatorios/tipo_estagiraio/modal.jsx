import React, { useState } from "react";
import '../style.scss';
import { Dialog } from 'primereact/dialog';
import { Button } from 'primereact/button';
import jsPDF from 'jspdf';
import autoTable from 'jspdf-autotable';
import logo from '../../../../images/logo_h.png'
import TelaSpinner from "../../../../components/spinner";

import api from "../../../../config/api";
import { getToken, getUserId } from "../../../../config/auth";
import ListarTipoEstagiario from ".";




function RelatorioEstagiariosLotacao() {

  const [lotacao, setLotacao] = useState(null);
  const [display, setDisplay] = useState(false);
  const [displayPDF, setDisplayPDF] = useState(false);
  const [obrigatorio, setobrigatorio] = useState(false);
  const [ModalDisplay, setModalDisplay] = useState(false);


  //gerar o PDF

  const user = sessionStorage.getItem("nome");

  function baixarpdf(id) {
    setDisplayPDF(true);
    let config = { headers: { "Authorization": `Bearer ${getToken()}` } }
    let data = {
      "aplicacao": "SGI",
      "id_usuario": getUserId(),
      "id_lotacao": Number(id),
      "tipo_listagem": "PARCIAL",
      "eh_estagiario": true,
      "ativo": true
    }
    api.post('/rh/relatorio_servidores_lotacao/', data, config)
      .then((res) => {
        let x = JSON.parse(res.data);
        if (x.sucesso === "S") {
          let lotacao = '';
          let sigla = '';
          let chefe = '';
          let dados = [];
          x.lotacao.map((d) => {
            let aux = [d.matricula, d.nome, d.tipo_estagio.descricao || '']
            dados.push(aux);
            if (d.descricao_lotacao) lotacao = d.descricao_lotacao;
            if (d.sigla_lotacao) sigla = d.sigla_lotacao;
            if (d.nome_chefe_imediato) chefe = d.nome_chefe_imediato;
          })

          setDisplayPDF(false);
          generatePDF(dados, lotacao, sigla, chefe);

        }
      })
  }

  function coluna(x, y, tam, rotulo, texto, doc) {
    doc.setFont('helvetica', 'bold');
    doc.text(x, y, `${rotulo}:`);
    doc.setFont('helvetica', 'normal');
    doc.text(x + tam, y, `${texto ? texto : ""}`);
  }

  function generatePDF(d, lotacao, sigla, chefe) {

    let data = new Date();
    let dia = `${data.getDate()}/${data.getMonth() + 1}/${data.getFullYear()}`;
    let hora = data.toLocaleTimeString();


    let doc = new jsPDF('l', 'pt');
    doc.addImage(logo, 'PNG', 40, 20, 155, 30);

    doc.setFont('helvetica', 'bold');
    doc.setFontSize(18);
    doc.setTextColor(20, 71, 88);
    doc.text(545, 40, 'ESTAGIÁRIOS POR LOTAÇÃO');

    //coluna 1: x=50 , coluna 2 x=400

    let y = 60;
    let espaco = 18;

    doc.setFontSize(12);
    doc.setTextColor(0, 0, 0);

    if (d.length > 0) {

      y += espaco;
      coluna(40, y, 55, "Lotação", lotacao, doc);
      y += espaco;
      coluna(40, y, 35, "Sigla", sigla, doc);
      y += espaco;
      coluna(40, y, 80, "Responsável", chefe, doc);

      y += espaco;
      autoTable(doc, ({
        startY: y,
        theme: 'striped',
        styles: {
          fontSize: 9,
          cellPadding: 2,
          halign: 'left',
          valign: 'middle',
          textColor: "#000",
          lineColor: "#000",
          fillColor: "#fff"
        },
        head: [["Matrícula", "Nome", "Tipo do estágio"]],
        body: d
      }))
    } else {
      y += espaco;
      coluna(40, y, 0, "", "Sem dados", doc);
    }


    //rodapé da pagina
    let pageCount = doc.internal.getNumberOfPages()
    doc.setFontSize(8)
    for (var i = 1; i <= pageCount; i++) {
      doc.setPage(i)
      doc.text('DEFENSORIA PÚBLICA DO ESTADO DO AMAZONAS - AV. ANDRE ARAUJO, 679, ALEIXO  CNPJ: 19.421.427/0001-91', doc.internal.pageSize.width / 2, 570, {
        align: 'center'
      });
      doc.text(`Emitido por ${user} em ${dia}  ${hora}` + ' - Página ' + String(i) + ' de ' + String(pageCount), doc.internal.pageSize.width / 2, 580, {
        align: 'center'
      });
    }

    //doc.save(`${s.nome} ${new Date().toLocaleDateString()} ${new Date().toLocaleTimeString()}.pdf`)
    window.open(doc.output('bloburl', { filename: `ficha_funcional.pdf` }), '_blank');
  }

  // ---------

  function gerar() {
    if (lotacao !== null) {
      setDisplay(false);
      setobrigatorio(false);
      baixarpdf(lotacao.id_lotacao);
      setLotacao(null);
    } else {
      setobrigatorio(true);
    }
  }

  const ModalFooterEstagiario = () => {
    return (
      <div>
        {obrigatorio ? <p><small className="p-error">Informe os capos obrigatórios (*).</small></p> : null}
        <Button
          label="Cancelar"
          icon="pi pi-times"
          onClick={() => {
            setModalDisplay(false);
            setobrigatorio(false);
          }}
          className="btn-2" />
        <Button
          label="Gerar Relatório"
          icon="pi pi-check"
          onClick={() => {
            gerar();
          }}
          className="btn-1"
        />
      </div>
    );
  }

  return (
    <div>
      <Button
        label="Tipo de Estagiário"
        icon="pi pi-file-pdf"
        className="btn-2"
        onClick={() => setModalDisplay(true)}
      />
      <Dialog
        // header="Estagiários por Lotação"
        visible={ModalDisplay}
        style={{ width: '50%' }}
        footer={ModalFooterEstagiario}
        onHide={
          () => {
            setModalDisplay(false);
            setobrigatorio(false);
          }
        }
      >
        <ListarTipoEstagiario />
      </Dialog>

      <Dialog
        visible={displayPDF}
        style={{ width: '50vw' }}
        closable={false}
      >
        <TelaSpinner tamanho={100} texto={"Gerando Documento"} />
      </Dialog>
    </div>
  );
}

export default RelatorioEstagiariosLotacao;