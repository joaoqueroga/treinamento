import React, {useState, useEffect} from "react";
// import './style.scss';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import Swal from 'sweetalert2';

import api from "../../../../config/api";
import { getToken, getUserId } from "../../../../config/auth";
import { Checkbox } from 'primereact/checkbox';

function ListarTipoEstagiario() {

 
    const [dados, setDados] = useState([]);
    const [loading, setLoading] = useState(true);
    const [selectedItems, setSelectedItems] = useState([]);


    useEffect(() => {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId()
        }
        api.post('/estagio/tipos_estagiario/',data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.length > 0){
                setDados(x);
                setLoading(false);
            }else{
                Swal.fire({
                    icon:'info',
                    title:"Informação",
                    text: `Sem dados`,
                    confirmButtonText: 'fechar',
                })
                setLoading(false);
            }
        })
    }, []);




    const handleItemCheckboxChange = (event, rowData) => {
        const { checked } = event.target;
        if (checked) {
            setSelectedItems((prevSelectedItems) => [...prevSelectedItems, rowData]);
        } else {
            setSelectedItems((prevSelectedItems) =>
                prevSelectedItems.filter((item) => item !== rowData)
            );
        } 
    };


    // const [filters] = useState({
    //     'tipo_estagio_descricao': { value: null, matchMode: FilterMatchMode.CONTAINS },
    //     'valor_bolsa': { value: null, matchMode: FilterMatchMode.CONTAINS },
    // });

    // function seleciona(obj) {
    //     setId(obj.id_tipo_estagio);
    //     setDescricao(obj.tipo_estagio_descricao);
    //     setBolsa(obj.valor_bolsa);
    //     setVale_transporte(obj.valor_vale_transporte);
    //     setmodalEditar(true);
    // }


    return (
        <div className="view-body">
        <div>
            <div className="top-box">
                <h1>Tipos de Estágio</h1>
            </div>
            <div className="lotacao-body">
                <div className="dados-lotacoes">
                    <DataTable
                        value={dados}
                        className="tabela-servidores no-select"
                        dataKey="id"
                        emptyMessage="Nada encontrado"
                        scrollable
                        scrollHeight="73vh"
                    >
                        <Column
                            headerStyle={{ width: '3rem' }}
                            body={(rowData) => (
                                <Checkbox
                                    checked={selectedItems.includes(rowData)}
                                    onChange={(e) => {handleItemCheckboxChange(e, rowData); }}
                                />
                            )}
                        />
                        <Column
                            header="Descrição"
                            field="tipo_estagio_descricao"
                            style={{ flexGrow: 0, flexBasis: '60%' }}
                        />
                        <Column
                            header="Bolsa"
                            field="valor_bolsa"
                            style={{ flexGrow: 0, flexBasis: '20%' }}
                        />
                    </DataTable>
                </div>
            </div>
        </div>
    </div>

       
    );
}

export default ListarTipoEstagiario;