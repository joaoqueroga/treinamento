import React, {useState} from "react";
import '../style.scss';
import { Dialog } from 'primereact/dialog';
import { Button } from 'primereact/button';
import { InputSwitch } from 'primereact/inputswitch';
import jsPDF from 'jspdf';
import autoTable from 'jspdf-autotable';
import logo from '../../../../images/logo_h.png'
import TelaSpinner from "../../../../components/spinner";
import api from "../../../../config/api";
import { getToken, getUserId } from "../../../../config/auth";
import SeletorEstagiario from "../../../../components/SeletorEstagiario";

function RelatorioFichaFuncionalEstagio() {

    const [servidor, setServidor] = useState(null);
    const [display, setDisplay] = useState(false);
    const [displayPDF, setDisplayPDF] = useState(false);
    const [checkAnotacoes, setCheckAnotacoes] = useState(true);
    const [obrigatorio, setobrigatorio] = useState(false);

    const user = sessionStorage.getItem("nome");

    function baixarpdf(id) {
        setDisplayPDF(true);
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            "id_funcionario": Number(id),
            "tipo_listagem": "COMPLETA"
        }
        api.post('/estagio/estagiarios/',data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === "S"){
                let f = x.funcionarios[0];
                let a = x.funcionarios[0].anotacoes;
                let d = x.funcionarios[0].documentos_diversos;
                let historico = x.funcionarios[0].anotacoes.length > 0? x.funcionarios[0].anotacoes[0].data_inclusao: "SEM REGISTROS"
                setDisplayPDF(false);
                generatePDF(f,a,d, historico);
            }
        })
    }

    function coluna( x, y, tam, rotulo, texto, doc) {
        doc.setFont('helvetica', 'bold');
        doc.text(x, y, `${rotulo}:`);
        doc.setFont('helvetica', 'normal');
        doc.text(x+tam, y, `${texto?texto:""}`);
    }

    function rotulo(y, texto, doc){
        let acrecimo = 8;
        doc.setFont('helvetica', 'bold');
        doc.text( 40, y+acrecimo, `${texto}`);
        y+=(5+acrecimo);
        doc.line(40,y,800,y);
        return y + 5;
    }

    function generatePDF (s, anotacoes, d, historico){

        let data = new Date();
        let dia_x = data.getDate() < 10 ? `0${data.getDate()}` : data.getDate();
        let mes_x = (data.getMonth()+1) < 10 ? `0${data.getMonth()+1}` : data.getMonth()+1
        let dia = `${dia_x}/${mes_x}/${data.getFullYear()}`;
        let hora = data.toLocaleTimeString();

       
        let doc = new jsPDF('l', 'pt');
        doc.addImage(logo, 'PNG', 40, 20, 155, 30 );
        
        doc.setFont('helvetica', 'bold');
        doc.setFontSize(18);
        doc.setTextColor(20,71,88);
        doc.text(600, 40, 'FICHA DO ESTAGIÁRIO');

        doc.setLineWidth(3.0); 
        doc.setTextColor(0,0,0);

        doc.setFont('helvetica', 'bold');
        doc.setFontSize(18);
        doc.text(40, 85, `${s.nome}`);

        doc.line(40,90,800,90);
        doc.setFontSize(12);

        //coluna 1: x=50 , coluna 2 x=400

        let y = 110;
        let espaco = 18;

        coluna(50, y, 248, "Número do Termo de Contrato de Estágio", s.matricula ,doc);

        y+=espaco; //pula a linha

        coluna(50, y, 80, "Nome Social", s.nome_social ,doc);
        
        y+=espaco; //pula a linha

        coluna(50, y,  100, "Tipo Sanguíneo", `${s.grupo_sanguineo?s.grupo_sanguineo:''} ${s.fator_rh?s.fator_rh:''}` ,doc);
        coluna(400, y,  80, "Nascimento", s.nascimento ,doc);


        y+=espaco;
        y = rotulo(y, 'DADOS FUNCIONAIS', doc);

        y+=espaco;
        coluna(50, y,  60, "Lotação", s.descricao_lotacao ,doc);
        y+=espaco;
        coluna(50, y,  100, "Tipo de Estágio", s.tipo_estagio.descricao || '' ,doc);
        y+=espaco;
        coluna(50, y,  100, "Valor da Bolsa", `R$ ${s.tipo_estagio.valor_bolsa}`|| '' ,doc);
        y+=espaco;
        coluna(50, y,  100, "Vale Transporte", `R$ ${s.tipo_estagio.valor_vale_transporte}`|| '' ,doc);

        y+=espaco;
        coluna(50, y,  160, "Responsável Pela Lotação", s.nome_responsavel_lotacao ,doc);

        y+=espaco;
        coluna(50, y,  75, "Supervisor", s.nome_supervisor ,doc);
        
        y+=espaco;
        coluna(50, y,  150, "Formação do Supervisor", s.descricao_grau_instrucao_supervisor ,doc);
        coluna(400, y, 140, "Contato do Supervisor", s.telefone_supervisor ,doc);

        y+=espaco;
        coluna(50, y,  150, "Assessor do Supervisor", s.nome_assessor_supervisor ,doc);
        y+=espaco;
        coluna(50, y,  215, "Contato do Assessor do Supervisor", s.telefone_assessor ,doc);

        coluna(400, y, 140, "Data Referência Férias", s.ferias_referencia ,doc);

        y+=espaco;
        coluna(50, y,  135, "Agente de Integração", s.descricao_agente_integracao ,doc);
        y+=espaco;
        coluna(50, y,  135, "Instituição de Ensino", s.descricao_instituicao_ensino ,doc);
        y+=espaco;
        coluna(50, y,  110, "Estagiário Ativo?", s.ativo?"Sim":"Não" ,doc);
        coluna(400, y, 135, "Data de Desligamento", s.data_fim_estagio ,doc);
        y+=espaco;
        coluna(50, y,  135, "Instituição de Ensino", s.descricao_instituicao_ensino ,doc);
        
        doc.addPage();
        y = 40;


        y+=espaco;
        y = rotulo(y, 'CONTATOS', doc);

        y+=espaco;
        coluna(50, y,  40, "Email", s.email_pessoal ,doc);
        coluna(400, y,  110, "Telefone Principal", s.telefone_principal ,doc);

        y+=espaco;
        coluna(50, y,  150, "Telefone de Emergência", s.telefone_emergencia ,doc);
        y+=espaco;
        coluna(50, y,  200, "Nome do Contato de Emergência", s.nome_contato_emergencia ,doc);
    

        y+=espaco;

        y = rotulo(y, 'ENDEREÇO', doc);
        y+=espaco;
        coluna(50, y,   35, "Tipo", s.descricao_tipo_logradouro ,doc);
        coluna(400, y,  80, "Logradouro", s.logradouro ,doc);
        y+=espaco;
        coluna(50, y,   55, "Número", s.numero ,doc);
        coluna(400, y,  45, "Bairro", s.bairro ,doc);
        y+=espaco;
        coluna(50, y,   35, "CEP", s.cep ,doc);
        coluna(400, y,   70, "Município", s.municipio ,doc);
        y+=espaco;
        coluna(50, y,  90, "Complemento", s.complemento ,doc);
        
        y+=espaco;
        y = rotulo(y, 'DOCUMENTOS', doc);
        y+=espaco;
        coluna(50, y,   34, "CPF", s.cpf ,doc);
        coluna(400, y,  28, "RG", s.rg ,doc);
        
        y+=espaco;
        y = rotulo(y, 'DADOS BANCÁRIOS', doc);

        y+=espaco;
        coluna(50, y,   45, "Banco", s.nome_banco ,doc);
        
        y+=espaco;
        coluna(50, y,   45, "Conta", s.conta ,doc);
        coluna(400, y,  60, "Agência", s.agencia ,doc);

        

        y+=espaco;
        if(checkAnotacoes){
            y+=espaco;
            y = rotulo(y, 'ANOTAÇÕES', doc);
            
            autoTable(doc, ({
                startY: y,
                body: anotacoes,
                theme: 'grid',
                styles: {fontSize: 12, cellPadding: 5, halign: 'left' , textColor: "#000", lineColor:"#000", fillColor:"#fff"},
                columns: [
                    { header: 'TIPO', dataKey: 'descricao_tipo_anotacao' },
                    { header: 'DESCRIÇÃO', dataKey: 'descricao' },
                ],
            }))
        }

        //rodapé da pagina
        let pageCount = doc.internal.getNumberOfPages()
        doc.setFontSize(8)
        for (var i = 1; i <= pageCount; i++) {
            doc.setPage(i)
            doc.text(`ÚLTIMO REGISTRO DE ANOTAÇÕES E DOCUMENTOS DIVERSOS: ${historico}`, doc.internal.pageSize.width / 2, 560, { align: 'center'});
            doc.text('DEFENSORIA PÚBLICA DO ESTADO DO AMAZONAS - AV. ANDRE ARAUJO, 679, ALEIXO  CNPJ: 19.421.427/0001-91', doc.internal.pageSize.width / 2, 570, {align: 'center'});
            doc.text(`Emitido por ${user} em ${dia} ${hora} - Página  ${String(i)} de ${String(pageCount)}`, doc.internal.pageSize.width / 2, 580, {align: 'center'});
        }

        //doc.save(`${s.nome} ${new Date().toLocaleDateString()} ${new Date().toLocaleTimeString()}.pdf`)
        window.open(doc.output('bloburl', { filename: `ficha_funcional.pdf` }), '_blank');
    } 

    // ---------

    function gerar(){
        if(servidor !== null){
            setDisplay(false);
            setobrigatorio(false);
            baixarpdf(servidor.id_funcionario);
            setServidor(null);
        }else{
            setobrigatorio(true);
        }
    }

    const modalFooter = () => {
        return (
            <div>
                {obrigatorio?<p><small className="p-error">Informe os capos obrigatórios (*).</small></p>:null}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setDisplay(false);
                        setobrigatorio(false);
                    }} 
                    className="btn-2" />
                <Button 
                    label="Gerar Relatório" 
                    icon="pi pi-check" 
                    onClick={()=>{
                        gerar();
                    }} 
                    className="btn-1"
                />
            </div>
        );
    }

    return (
        <div>
            <Button 
                label="Ficha do Estagiário" 
                icon="pi pi-file-pdf" 
                className="btn-2"
                onClick={()=>setDisplay(true)}
            />
            <Dialog 
                header="Ficha do Estagiário"
                visible={display}
                style={{ width: '50%' }}
                footer={modalFooter}
                onHide={
                    () =>{
                        setDisplay(false);
                        setobrigatorio(false);
                    }
                }
            >
                <span>
                    <label>Estagiário *</label>
                    <SeletorEstagiario
                        get={servidor}
                        set={setServidor}
                    />
                </span>
                <span>
                    <label>Com anotações? </label>
                    <span className='input-toggle-rotulo'>
                        <InputSwitch
                            checked={checkAnotacoes}
                            onChange={(e) => setCheckAnotacoes(e.value)} 
                        />
                        <p>{checkAnotacoes?"Sim":"Não"}</p>
                    </span>
                </span>
            </Dialog>

            <Dialog
                visible={displayPDF} 
                style={{ width: '50vw' }}
                closable={false}
            >
                <TelaSpinner tamanho={100} texto={"Gerando Documento"}/>
            </Dialog>
        </div>
    );
}

export default RelatorioFichaFuncionalEstagio;