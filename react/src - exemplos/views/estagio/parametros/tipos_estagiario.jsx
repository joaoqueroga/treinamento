import React, {useState, useEffect} from "react";
import './style.scss';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { FilterMatchMode } from 'primereact/api';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import Swal from 'sweetalert2';
import { ProgressSpinner } from 'primereact/progressspinner';
import { BreadCrumb } from 'primereact/breadcrumb';

import api from "../../../config/api";
import { getToken, getUserId } from "../../../config/auth";

function TiposEstagiario() {

    const items = [
        { label: 'Coordenação de Estágio' },
        { label: 'Parâmetros' },
        { label: 'Tipos de Estágio' }
    ];
    const home = { icon: 'pi pi-home', url: '/inicio' }
    const [dados, setDados] = useState([]);
    const [modal, setmodal] = useState(false);
    const [obrigatorio, setobrigatorio] = useState(false);

    const [modaleditar, setmodalEditar] = useState(false);
    const [obrigatorioeditar, setobrigatorioEditar] = useState(false);

    const [loading, setLoading] = useState(true);

    const [descricao, setDescricao] = useState('');
    const [bolsa, setBolsa] = useState('');
    const [vale_transporte, setVale_transporte] = useState('');
    const [id, setId] = useState(null);

    useEffect(() => {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId()
        }
        api.post('/estagio/tipos_estagiario/',data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.length > 0){
                setDados(x);
                setLoading(false);
            }else{
                Swal.fire({
                    icon:'info',
                    title:"Informação",
                    text: `Sem dados`,
                    confirmButtonText: 'fechar',
                })
                setLoading(false);
            }
        })
    }, []);

    const irBodyTemplate = (rowData) => {
        return(
            <span>
            <Button 
                onClick={()=>seleciona(rowData)}
                icon="pi pi-pencil"
                className="btn-green"
                title="Editar"
            />
            </span>
        )                                    
    }
    const [filters] = useState({
        'tipo_estagio_descricao': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'valor_bolsa': { value: null, matchMode: FilterMatchMode.CONTAINS },
    });

    function seleciona(obj) {
        setId(obj.id_tipo_estagio);
        setDescricao(obj.tipo_estagio_descricao);
        setBolsa(obj.valor_bolsa);
        setVale_transporte(obj.valor_vale_transporte);
        setmodalEditar(true);
    }

    function liparDados() {
        setId("");
        setDescricao("");
        setBolsa("");
        setVale_transporte("");
    }

    function reload() {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId()
        }
        api.post('/estagio/tipos_estagiario/',data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.length > 0){
                setDados(x);
            }
        })
    }

    function salvar() {
        if(descricao && bolsa){
            let dados = {
                aplicacao: "SGI",
                id_usuario: getUserId(),
                descricao: descricao.toUpperCase(),
                valor_bolsa: bolsa.toUpperCase(),
                valor_vale_transporte: vale_transporte.toUpperCase(),
            }
            let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
            api.post('/estagio/cadastro_tipo_estagiario/',dados, config)
            .then((res)=>{
                let x = JSON.parse(res.data);
                if(x.sucesso === "S"){
                    setmodal(false);
                    Swal.fire({
                        icon: 'success',
                        title:"Sucesso",
                        text: `Tipo de Estágio Cadastrado`,
                        confirmButtonText: 'fechar',
                    })
                    reload();
                    liparDados();
                }else{
                    setmodal(false);
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `${x.motivo}`,
                        confirmButtonText: 'fechar',
                    })
                }
            })

            liparDados();
            setmodal(false);
        }else{
            setobrigatorio(true);
        }
    }

    function atualizar() {
        if(descricao && bolsa){
            let dados = {
                aplicacao: "SGI",
                id_usuario: getUserId(),
                id_tipo_estagio: id,
                descricao: descricao.toUpperCase(),
                valor_bolsa: bolsa.toUpperCase(),
                valor_vale_transporte: vale_transporte.toUpperCase(),
            }
            let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
            api.post('/estagio/cadastro_tipo_estagiario/',dados, config)
            .then((res)=>{
                let x = JSON.parse(res.data);
                if(x.sucesso === "S"){
                    setmodal(false);
                    Swal.fire({
                        icon: 'success',
                        title:"Sucesso",
                        text: `Tipo de Estágio Atualizado`,
                        confirmButtonText: 'fechar',
                    })
                    reload();
                    liparDados();
                }else{
                    setmodal(false);
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `${x.motivo}`,
                        confirmButtonText: 'fechar',
                    })
                }
            })

            liparDados();
            setmodalEditar(false);
        }else{
            setobrigatorioEditar(true);
        }
    }

    const modalCadastroFooter = () => {
        return (
            <div>
                {obrigatorio?<p><small className="p-error">Informe os capos obrigatórios (*).</small></p>:null}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setmodal(false);
                        setobrigatorio(false);
                    }} 
                    className="btn-2" />
                <Button 
                    label="Salvar" 
                    icon="pi pi-check" 
                    onClick={()=>{
                        salvar();
                    }} 
                    className="btn-1"
                />
            </div>
        );
    }

    const modalEditarFooter = () => {
        return (
            <div>
                {obrigatorioeditar?<p><small className="p-error">Informe os capos obrigatórios (*).</small></p>:null}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setmodalEditar(false);
                        setobrigatorioEditar(false);
                    }} 
                    className="btn-2" />
                <Button 
                    label="Salvar" 
                    icon="pi pi-check" 
                    onClick={()=>{
                        atualizar();
                    }} 
                    className="btn-1"
                />
            </div>
        );
    }

    return (
        <div className='view'>
        <div className="view-body">
        {
        loading?
        <div className="loading-pagina" ><ProgressSpinner/>Carregando...</div>
        :
        <div>
            <div className="header">
                <BreadCrumb model={items} home={home}/>
                <h6 className="titulo-header">Tipos de Estágio</h6>
                <Button 
                    label="Novo Tipo de Estágio" 
                    icon="pi pi-plus" 
                    className="btn-1"
                    onClick={()=>{setmodal(true); liparDados()}}
                />
            </div>
            <div className="top-box">
                <h1>Tipos de Estágio</h1>
            </div>
            <div className="lotacao-body">
                <div className="dados-lotacoes">
                <DataTable
                    value={dados}
                    size="small"
                    className="tabela-servidores"
                    dataKey="id"
                    filters={filters}
                    filterDisplay="row"
                    emptyMessage="Nada encontrado"
                    scrollable
                    scrollHeight="73vh"
                    selectionMode="single"
                >
                    <Column
                        header="Descrição"
                        field="tipo_estagio_descricao"
                        filter 
                        filterPlaceholder="Buscar" 
                        style={{ flexGrow: 0, flexBasis: '60%' }}
                        showFilterMenu={false}
                    />
                    <Column
                        header="Bolsa"
                        field="valor_bolsa"
                        filter 
                        filterPlaceholder="Buscar" 
                        style={{ flexGrow: 0, flexBasis: '20%' }}
                        showFilterMenu={false}
                    />
                    <Column 
                        field="botao" 
                        header="Ação" 
                        body={irBodyTemplate} 
                        className="col-centralizado" 
                        style={{ flexGrow: 0, flexBasis: '20%' }}
                        showFilterMenu={false}
                    />
                </DataTable>

                </div>

                {/* Para Cadastrar*/}
                <Dialog 
                    header="Novo Tipo de Estágio"
                    visible={modal}
                    style={{ width: '50%' }}
                    footer={modalCadastroFooter}
                    onHide={
                        () =>{
                            setmodal(false);
                            setobrigatorio(false);
                        }
                    }
                >
                    <div className="field grupo-input-label">
                        <label>Descrição <i>(Ex: MÉDIO - 3H)</i> *</label>
                        <InputText
                            className="texto-maiusculo" 
                            value={descricao}
                            onChange={(e)=>setDescricao(e.target.value)}
                        />
                    </div>
                    <div className="field grupo-input-label">
                        <label>Valor da bolsa *</label>
                        <InputText
                            className="texto-maiusculo" 
                            value={bolsa}
                            onChange={(e)=>setBolsa(e.target.value)}
                        />
                    </div>     
                    <div className="field grupo-input-label">
                        <label>Valor do Vale Transporte</label>
                        <InputText
                            className="texto-maiusculo" 
                            value={vale_transporte}
                            onChange={(e)=>setVale_transporte(e.target.value)}
                        />
                    </div>  
                </Dialog>

                {/* Para Ver e Editar*/}
                <Dialog 
                    header="Ediar Tipo de Estágio"
                    visible={modaleditar}
                    style={{ width: '50%' }}
                    footer={modalEditarFooter}
                    onHide={
                        () =>{
                            setmodalEditar(false);
                            setobrigatorioEditar(false);
                        }
                    }
                >
                    <div className="field grupo-input-label">
                        <label>Descrição <i>(Ex: MÉDIO - 3H)</i> *</label>
                        <InputText
                            className="texto-maiusculo" 
                            value={descricao}
                            onChange={(e)=>setDescricao(e.target.value)}
                        />
                    </div>
                    <div className="field grupo-input-label">
                        <label>Valor da bolsa *</label>
                        <InputText
                            className="texto-maiusculo" 
                            value={bolsa}
                            onChange={(e)=>setBolsa(e.target.value)}
                        />
                    </div>
                    <div className="field grupo-input-label">
                        <label>Valor do Vale Transporte</label>
                        <InputText
                            className="texto-maiusculo" 
                            value={vale_transporte}
                            onChange={(e)=>setVale_transporte(e.target.value)}
                        />
                    </div> 
                </Dialog>
                <br/>
            </div>
        </div>
        }
        </div>
    </div>
    );
}

export default TiposEstagiario;