import React, {useState, useEffect} from "react";
import {useParams, useNavigate} from 'react-router-dom';
import '../style.scss';
import Swal from 'sweetalert2';
import { ProgressSpinner } from 'primereact/progressspinner';
import { BreadCrumb } from 'primereact/breadcrumb';
import { InputSwitch } from 'primereact/inputswitch';
import { BiCloudUpload } from "react-icons/bi";
import { Button } from 'primereact/button';
import { Dropdown } from 'primereact/dropdown';
import { InputTextarea } from 'primereact/inputtextarea';
import { InputText } from 'primereact/inputtext';
import { Dialog } from 'primereact/dialog';
import { FilterMatchMode } from 'primereact/api';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';

import api from "../../../../config/api";
import { getToken, getUserId } from "../../../../config/auth";


function ProcessoSeletivo() {
    const { id } = useParams();
    const navigate = useNavigate();

    const lista_status = [
        { label: '-' },
        { label: 'Contratação efetivada' },
        { label: 'Desistência' },
        { label: 'Fim de fila' },
        { label: 'Aguardando' }
    ];

    const [filters] = useState({
        'colocacao': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'nome': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'status': { value: null, matchMode: FilterMatchMode.CONTAINS }
    });

    const separadores = [
        {label: 'Ponto e vírgula', value: ';'},
        {label: 'Vírgula', value: ','},
    ]
    const [separador, setSeparador] = useState(';');

    const home = { icon: 'pi pi-home', url: '/inicio' }

    const [loading, setLoading] = useState(true);
    const [importar, setImportar] = useState(false);

    const [csv, setCsv] = useState(null);
    const [texto, setTexto] = useState('');
    const [linhas, setLinhas] = useState([]);

    const [processo, setProcesso] = useState([]);
    const [descProcesso, setDescProcesso] = useState(id);

    const items = [
        { label: 'Coordenação de Estágio' },
        { label: 'Processos Seletivos' , url: '/estagio/processos_seletivos'},
        { label: descProcesso },
    ];

    const [dialog, setDialog] = useState(false);

    // campos edição candidato

    const [nome, setNome] = useState('');
    const [email, setEmail] = useState('');
    const [telefone, setTelefone] = useState('');
    const [status, setStatus] = useState('');
    const [observacao, setObservacao] = useState('');
    const [id_candidato, setId_candidato] = useState(0);

    useEffect(() => {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            "id_processo_seletivo": id
        }
        api.post('/estagio/processo_seletivo/', data , config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x){
                setProcesso(x[0]);
                setDescProcesso(x[0].descricao);
                setLoading(false);
            }else{
                Swal.fire({
                    icon:'error',
                    title:"Erro",
                    text: `Erro na requisição de dados`,
                    confirmButtonText: 'fechar',
                })
            }
        })
    }, []);

    function atualizarProcesso(){
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId(),
            "id_processo_seletivo": id
        }
        api.post('/estagio/processo_seletivo/', data , config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            setProcesso(x[0]);
        })
    }


    function importarCsv(file){
        if(separador){
            setCsv(file);
            const reader = new FileReader();
            reader.readAsText(file);
            reader.onloadend = function () {
                let resultado = reader.result;

                let rows = resultado.trim().split(/\r?\n|\r/); 
                let rows_tabela = resultado.trim().split(/\r?\n|\r/); 

                /// remove cabecalho do CSV
                rows.shift();
                let texto_formatado_procedure = '';
                for(let i = 0; i < rows.length; i ++){
                    texto_formatado_procedure += rows[i];
                    if(i < rows.length - 1) texto_formatado_procedure += ';'; // se nao estiver na ultima linha adiciona ';'
                }
               
                /// cria os dados para a tabela da interface
                for(let i = 0; i < rows_tabela.length; i ++){
                    rows_tabela[i] = rows_tabela[i].split(separador);
                }

                setLinhas(rows_tabela);
                setTexto(texto_formatado_procedure);
            }
        }else{
            Swal.fire({
                icon:'warning',
                title:"Atenção",
                text: `Selecione um separador para a visualização do CSV`,
                confirmButtonText: 'fechar',
            })
        }
    }

    function uploadCsv(){
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            aplicacao: "SGI",
            id_usuario: getUserId(),
            id_processo_seletivo: Number(id),
            arquivo_csv: texto
        }
        api.post('/estagio/importar_csv_processo/', data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === 'S'){
                Swal.fire({
                    icon:'success',
                    title:"Sucesso",
                    text: `Importação concluída`,
                    confirmButtonText: 'fechar',
                }).then(() => {
                    atualizarProcesso();
                    setImportar(false);
                })
            }else{
                Swal.fire({
                    icon:'error',
                    title:"Erro",
                    text: `${x.motivo}`,
                    confirmButtonText: 'fechar',
                })
            }
        })
    }

    function selecionaCandidatoEdicao(c){
        let label = c.status.trim();
        setId_candidato(Number(c.id_processo_seletivo_candidato));
        setDialog(true);
        setNome(c.nome || '');
        setEmail(c.email || '');
        setTelefone(c.telefone || '');
        setObservacao(c.observacao || '');
        setStatus({ "label": label });

    }

    function salvarcandidato(){
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            aplicacao: "SGI",
            id_usuario: getUserId(),
            id_processo_seletivo_candidato: id_candidato,
            status: status ? status.label : null,
            email: email || null,
            telefone_principal: telefone || null,
            observacao: observacao || null,
            nome: nome || null
        }
        api.post('/estagio/salvar_candidato/', data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === 'S'){
                Swal.fire({
                    icon:'success',
                    title:"Sucesso",
                    text: `Candidato atualizado`,
                    confirmButtonText: 'fechar',
                }).then(() => {
                    setDialog(false);
                    atualizarProcesso();
                })
            }else{
                Swal.fire({
                    icon:'error',
                    title:"Erro",
                    text: `${x.motivo}`,
                    confirmButtonText: 'fechar',
                })
            }
        })
    }

    const irBodyTemplate = (rowData) => {
        return(
            <span>
            <Button 
                onClick={()=>selecionaCandidatoEdicao(rowData)}
                icon="pi pi-pencil"
                className="btn-green"
                title="Editar função"
            />
            </span>
        )                                    
    }

    const renderFooter = () => {
        return (
            <div>
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() => setDialog(false)} 
                    className="btn-2"
                />
                <Button 
                    label="Salvar" 
                    icon="pi pi-save" 
                    onClick={salvarcandidato} 
                    className="btn-1"
                />
            </div>
        );
    }

    return ( 
        <div className='view'>
        <div className="view-body">
        {
        loading?
        <div className="loading-pagina" ><ProgressSpinner/>Carregando...</div>
        :
        <div>
            <div className="header">
                <BreadCrumb model={items} home={home}/>
                <h5 className="titulo-header">Processos Seletivos</h5>
                <span className="opcao-processo-seletivo">
                    {importar?'Importação':'Edição'}
                    <InputSwitch checked={importar} onChange={(e) => setImportar(e.value)} className="switch-modo-processo"/>
                </span>
            </div>
            <div>
                {
                importar?
                    <div className="importar-csv-processo">
                        <span className='input-arquivo-servidor'>
                            <Dropdown 
                                value={separador} 
                                options={separadores} 
                                onChange={(e)=>setSeparador(e.value)} 
                                optionLabel="label" 
                                placeholder="Separador do CSV"
                                style={{height: '40px', width: '300px'}}
                                className='dropdow-separador-csv' 
                            />

                            <label className='label-file-servidor' style={{height: '40px'}}>
                                <BiCloudUpload size={20}/>
                                <input
                                    value="" 
                                    type="file" 
                                    className='input-files' 
                                    accept='.csv'
                                    onChange={(e)=>importarCsv(e.target.files[0])}
                                />
                            </label>
                            <p>
                                {csv?csv.name:"Nenhum arquivo selecionado"}
                            </p>
                        </span>

                        <div className="csv-table">
                            <table>
                                <tbody>
                                {
                                    linhas.map((l, index)=>{
                                        return(
                                            <tr key={index}> 
                                                { 
                                                    l.map((c, index)=>{
                                                        return(
                                                            <th key={index} className="celula-tabela-csv"> { c }</th> 
                                                        )
                                                    }) 
                                                }
                                            </tr>
                                        )
                                    })
                                }
                                </tbody>
                            </table>
                        </div>
                        <div className="rodape-exportacao">
                            <Button 
                                label="Importar dados" 
                                icon="pi pi-check" 
                                className="btn-1"
                                onClick={uploadCsv}
                            />
                        </div>
                    </div>
                :
                    <div className="editar-csv-processo">
                        {
                            processo.candidatos.length > 0?
                                <DataTable
                                    value={processo.candidatos}
                                    size="small"
                                    className="tabela-servidores"
                                    dataKey="id"
                                    filters={filters}
                                    filterDisplay="row"
                                    emptyMessage="Nada encontrado"
                                    scrollable
                                    scrollHeight="73vh"
                                    selectionMode="single"
                                >
                                    <Column
                                        header="#"
                                        field="colocacao"
                                        //filter 
                                        style={{ flexGrow: 0, flexBasis: '5%' }}
                                    />
                                    <Column
                                        header="Nome"
                                        field="nome"
                                        filter 
                                        style={{ flexGrow: 0, flexBasis: '40%' }}
                                        showFilterMenu={false}
                                    />
                                    <Column
                                        header="Status"
                                        field="status"
                                        filter  
                                        style={{ flexGrow: 0, flexBasis: '20%' }}
                                        showFilterMenu={false}
                                    />
                                    <Column
                                        header="Email"
                                        field="email"
                                        style={{ flexGrow: 0, flexBasis: '20%' }}
                                    />
                                    <Column
                                        header="Telefone"
                                        field="telefone"
                                        style={{ flexGrow: 0, flexBasis: '15%' }}
                                    />
                                    <Column 
                                        field="botao" 
                                        header="Ação" 
                                        body={irBodyTemplate} 
                                        className="col-centralizado" 
                                        style={{ flexGrow: 0, flexBasis: '10%' }}
                                    />
                                </DataTable>
                            : <div id="texto-processo-vazio">Sem Registros de Candidados</div>
                        }

                        <Dialog 
                            header="Atualizar Candidato" 
                            visible={dialog} 
                            style={{ width: '70vw' }} 
                            footer={renderFooter} 
                            onHide={() => setDialog(false)}
                        >
                            <div className="field grupo-input-label">
                                <label>Nome</label>
                                <InputText 
                                    value={nome} 
                                    onChange={(e) => setNome(e.target.value)} 
                                />
                            </div>
                            <div className="field grupo-input-label">
                                <label>Email</label>
                                <InputText 
                                    value={email} 
                                    onChange={(e) => setEmail(e.target.value)} 
                                />
                            </div>
                            <div className="field grupo-input-label">
                                <label>Telefone</label>
                                <InputText 
                                    value={telefone} 
                                    onChange={(e) => setTelefone(e.target.value)} 
                                />
                            </div>
                            <div className="field grupo-input-label">
                                <label>Status</label>
                                <Dropdown 
                                    value={status} 
                                    options={lista_status} 
                                    onChange={(e)=>setStatus(e.value)} 
                                    optionLabel="label"
                                    style={{height: '40px', margin: '0'}}
                                    className='dropdow-separador-csv'
                                    placeholder={status.label}
                                />
                            </div>
                            <div className="field grupo-input-label">
                                <label>Observação</label>
                                <InputTextarea 
                                    value={observacao} 
                                    onChange={(e) => setObservacao(e.target.value)} 
                                />
                            </div>
                        </Dialog>
                    </div>
                }
            </div>
        </div>
        }
        </div>
    </div>
    );
}

export default ProcessoSeletivo;