import React, {useState, useEffect} from "react";
import './style.scss';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { FilterMatchMode } from 'primereact/api';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import Swal from 'sweetalert2';
import { ProgressSpinner } from 'primereact/progressspinner';
import { BreadCrumb } from 'primereact/breadcrumb';
import SeletorData from "../../../components/SeletorData";
import {useNavigate} from 'react-router-dom';
import { Tag } from 'primereact/tag';

import api from "../../../config/api";
import { getToken, getUserId } from "../../../config/auth";

import Alertas from "../../../utils/alertas";

function ProcessosSeletivosEstagio() {

    const items = [
        { label: 'Coordenação de Estágio' },
        { label: 'Processos Seletivos' }
    ];
    const navigate = useNavigate();

    const home = { icon: 'pi pi-home', url: '/inicio' }
    const [dados, setDados] = useState([]);
    const [modal, setmodal] = useState(false);
    const [obrigatorio, setobrigatorio] = useState(false);

    const [modaleditar, setmodalEditar] = useState(false);
    const [obrigatorioeditar, setobrigatorioEditar] = useState(false);

    const [loading, setLoading] = useState(true);

    const [descricao, setdescricao] = useState('');
    const [dataInicial, setDataInicial] = useState('');
    const [dataFinal, setDataFinal] = useState('');
    const [id, setId] = useState(null);

    useEffect(() => {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId()
        }
        api.post('/estagio/processos_seletivos/',data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x){
                setDados(x);
                setLoading(false);
            }else{
                Swal.fire({
                    icon:'error',
                    title:"Erro",
                    text: `${x.motivo}`,
                    confirmButtonText: 'fechar',
                })
            }
        })
    }, []);

    function excluir(i) {
        let dados = {
            aplicacao: "SGI",
            id_usuario: getUserId(),
            id_processo_seletivo: i.id_processo_seletivo,
            excluir: true
        }
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        api.post('/estagio/salvar_processo_seletivo/',dados, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === "S"){
                Alertas.sucesso("Processo seletivo excluído")
                reload();
            }else{
                Alertas.erro(x.motivo);
            }
        })
    }

    const irBodyTemplate = (rowData) => {
        return(
            <span>
                <Button 
                onClick={()=>navigate(`/estagio/processo_seletivo/${rowData.id_processo_seletivo}`)}
                icon="pi pi-eye"
                className="btn-darkblue"
                title="Ir"
                />
                <Button 
                    onClick={()=>seleciona(rowData)}
                    icon="pi pi-pencil"
                    className="btn-green"
                    title="Editar"
                />
                <Button 
                    onClick={()=>Alertas.confirmacao("Excluir processo seletivo?", ()=>excluir(rowData))}
                    icon="pi pi-trash"
                    className="btn-red"
                    title="Excluir"
                />
            </span>
        )                                    
    }
    const [filters] = useState({
        'descricao': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'data_inicio': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'data_fim': { value: null, matchMode: FilterMatchMode.CONTAINS }
    });

    function seleciona(obj) {
        setId(obj.id_processo_seletivo);
        setdescricao(obj.descricao);
        setDataInicial(obj.data_inicio);
        setDataFinal(obj.data_fim)
        setmodalEditar(true);
    }

    function liparDados() {
        setId("");
        setdescricao("");
        setDataInicial('');
        setDataFinal('');
    }

    function reload() {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId()
        }
        api.post('/estagio/processos_seletivos/',data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            setDados(x);
        })
    }

    function salvar() {
        if(descricao){
            let dados = {
                aplicacao: "SGI",
                id_usuario: getUserId(),
                descricao: descricao.toUpperCase(),
                data_inicio: dataInicial || null,
                data_fim: dataFinal || null,
                excluir: false
            }
            let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
            api.post('/estagio/salvar_processo_seletivo/',dados, config)
            .then((res)=>{
                let x = JSON.parse(res.data);
                if(x.sucesso === "S"){
                    setmodal(false);
                    Swal.fire({
                        icon: 'success',
                        title:"Sucesso",
                        text: `Cadastrado`,
                        confirmButtonText: 'fechar',
                    })
                    reload();
                    liparDados();
                }else{
                    setmodal(false);
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `${x.motivo}`,
                        confirmButtonText: 'fechar',
                    })
                }
            })

            liparDados();
            setmodal(false);
        }else{
            setobrigatorio(true);
        }
    }

    function atualizar() {
        if(descricao){
            let dados = {
                aplicacao: "SGI",
                id_usuario: getUserId(),
                id_processo_seletivo: id,
                descricao: descricao.toUpperCase(),
                data_inicio: dataInicial || null,
                data_fim: dataFinal || null
            }

            let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
            api.post('/estagio/salvar_processo_seletivo/',dados, config)
            .then((res)=>{
                let x = JSON.parse(res.data);
                if(x.sucesso === "S"){
                    setmodal(false);
                    Swal.fire({
                        icon: 'success',
                        title:"Sucesso",
                        text: `Atualizado`,
                        confirmButtonText: 'fechar',
                    })
                    reload();
                    liparDados();
                }else{
                    setmodal(false);
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `${x.motivo}`,
                        confirmButtonText: 'fechar',
                    })
                }
            })

            liparDados();
            setmodalEditar(false);
        }else{
            setobrigatorioEditar(true);
        }
    }

    const modalCadastroFooter = () => {
        return (
            <div>
                {obrigatorio?<p><small className="p-error">Informe os capos obrigatórios (*).</small></p>:null}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setmodal(false);
                        setobrigatorio(false);
                    }} 
                    className="btn-2" />
                <Button 
                    label="Salvar" 
                    icon="pi pi-check" 
                    onClick={()=>{
                        salvar();
                    }} 
                    className="btn-1"
                />
            </div>
        );
    }

    const modalEditarFooter = () => {
        return (
            <div>
                {obrigatorioeditar?<p><small className="p-error">Informe os capos obrigatórios (*).</small></p>:null}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setmodalEditar(false);
                        setobrigatorioEditar(false);
                    }} 
                    className="btn-2" />
                <Button 
                    label="Salvar" 
                    icon="pi pi-check" 
                    onClick={()=>{
                        atualizar();
                    }} 
                    className="btn-1"
                />
            </div>
        );
    }

    const diasInicialTemplate = (rowData) => {
        return <span><Tag value={rowData.data_inicio} className="tag-lista-dias"></Tag></span>;
    }

    const diasFinalTemplate = (rowData) => {
        return <span><Tag value={rowData.data_fim} className="tag-lista-dias"></Tag></span>;
    }

    return (
        <div className='view'>
        <div className="view-body">
        {
        loading?
        <div className="loading-pagina" ><ProgressSpinner/>Carregando...</div>
        :
        <div>
            <div className="header">
                <BreadCrumb model={items} home={home}/>
                <h6 className="titulo-header">Processos Seletivos</h6>
                <Button 
                    label="Novo Processo" 
                    icon="pi pi-plus" 
                    className="btn-1"
                    onClick={()=>{setmodal(true); liparDados()}}
                />
            </div>
            <div className="top-box">
                <h1>Processos Seletivos</h1>
            </div>
            <div className="lotacao-body">
                <div className="dados-lotacoes">
                <DataTable
                    value={dados}
                    size="small"
                    className="tabela-servidores"
                    dataKey="id"
                    filters={filters}
                    filterDisplay="row"
                    emptyMessage="Nada encontrado"
                    scrollable
                    scrollHeight="73vh"
                    selectionMode="single"
                >
                    <Column
                        header="Descrição"
                        field="descricao"
                        filter 
                        filterPlaceholder="Buscar"
                        showFilterMenu = {false}  
                    />
                    <Column
                        header="Início"
                        field="data_inicio"
                        body={diasInicialTemplate}
                        filter 
                        filterPlaceholder="Buscar" 
                        showFilterMenu = {false} 
                        className="col-centralizado"
                        style={{ flexGrow: 0, flexBasis: '15%' }}
                    />
                    <Column
                        header="Fim"
                        field="data_fim"
                        body={diasFinalTemplate}
                        filter 
                        filterPlaceholder="Buscar" 
                        showFilterMenu = {false} 
                        className="col-centralizado"
                        style={{ flexGrow: 0, flexBasis: '15%' }}
                    />
                    <Column 
                        field="botao" 
                        header="Ação" 
                        body={irBodyTemplate} 
                        className="col-centralizado" 
                        style={{ flexGrow: 0, flexBasis: '20%' }}
                    />
                </DataTable>

                </div>

                {/* Para Cadastrar*/}
                <Dialog 
                    header="Novo Processo Seletivo"
                    visible={modal}
                    style={{ width: '50%' }}
                    footer={modalCadastroFooter}
                    onHide={
                        () =>{
                            setmodal(false);
                            setobrigatorio(false);
                        }
                    }
                >
                    <div className="field grupo-input-label">
                        <label>Descrição *</label>
                        <InputText 
                            value={descricao}
                            onChange={(e)=>setdescricao(e.target.value)}
                            className="texto-maiusculo"
                        />
                    </div>
                    <span className="field grupo-input-label">
                        Data Inicial
                        <div className="seletor-de-data">
                        <SeletorData
                            get={dataInicial}
                            set={setDataInicial}
                        />
                        </div>
                    </span>
                    <span className="field grupo-input-label">
                        Data Final
                        <div className="seletor-de-data">
                        <SeletorData
                            get={dataFinal}
                            set={setDataFinal}
                        />
                        </div>
                    </span>             
                </Dialog>

                {/* Para Ver e Editar*/}
                <Dialog 
                    header="Ediar Processo Seletivo"
                    visible={modaleditar}
                    style={{ width: '50%' }}
                    footer={modalEditarFooter}
                    onHide={
                        () =>{
                            setmodalEditar(false);
                            setobrigatorioEditar(false);
                        }
                    }
                >
                    <div className="field grupo-input-label">
                        <label>Descrição *</label>
                        <InputText 
                            value={descricao}
                            onChange={(e)=>setdescricao(e.target.value)}
                            className="texto-maiusculo"
                        />
                    </div>
                    <span className="field grupo-input-label">
                        Data Inicial
                        <div className="seletor-de-data">
                        <SeletorData
                            get={dataInicial}
                            set={setDataInicial}
                        />
                        </div>
                    </span>
                    <span className="field grupo-input-label">
                        Data Final
                        <div className="seletor-de-data">
                        <SeletorData
                            get={dataFinal}
                            set={setDataFinal}
                        />
                        </div>
                    </span>
                </Dialog>
                <br/>
            </div>
        </div>
        }
        </div>
    </div>
    );
}

export default ProcessosSeletivosEstagio;