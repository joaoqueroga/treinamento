import React, { useState } from 'react';
import '../style.scss';
import ListarAnotacoesEstagio from '../../../../components/listarAnotacoesEstagio';
import { Button } from 'primereact/button';
import { Dropdown } from 'primereact/dropdown';
import { Dialog } from 'primereact/dialog';
import { InputTextarea } from 'primereact/inputtextarea';
import Swal from 'sweetalert2';
import { getToken, getUserId } from '../../../../config/auth';
import api from '../../../../config/api';


function AnotacoesEstagiario(props) {
    const [modalcadastro, setmodalcadastro] = useState(false);
    const [modalEditar, setmodaleditar] = useState(false);
    const [anotacao, setAnotacao] = useState(null);
    const [descricao, setDescricao] = useState('');
    const [obrigatorio, setobrigatorio] = useState(false);
    const [id_anotacao, setId_anotacao] = useState(null);


    //tirar as aspas
    function tratamentoDescricao(desc) {
        let x = desc.replace(/(\r\n|\n|\r)/gm, " ");
        return x;
    }

    function adicionarAnotacao() {
        if(anotacao){
           
            let config = {headers: {"Authorization": `Bearer ${getToken()}`}}

            let data = {
                aplicacao: "SGI",
                id_usuario: getUserId(),
                id_funcionario: props.id_funcionario,
                descricao: descricao?tratamentoDescricao(descricao):"",
                descricao_tipo_anotacao: anotacao?anotacao.descricao:null,
                id_tipo_anotacao: anotacao?anotacao.id_tipo_anotacao:null,
            }

            api.post('/rh/cadastro_anotacao/',data, config)
            .then((res)=>{
                let x = JSON.parse(res.data);
                if(x.sucesso === "S"){
                    setmodalcadastro(false);
                    Swal.fire({
                        icon: 'success',
                        title:"Sucesso",
                        text: `Anotação registrada`,
                        confirmButtonText: 'fechar',
                    })
                    setDescricao('');
                    setAnotacao(null);
                    props.reloadAnotacoes();
                }else{
                    setmodalcadastro(false);
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `${x.motivo}`,
                        confirmButtonText: 'fechar',
                    })
                }
            })
        }else{
            setobrigatorio(true);
        }
    }

    function atualizarAnotacao() {
        if(anotacao){


            let config = {headers: {"Authorization": `Bearer ${getToken()}`}}

            let data = {
                aplicacao: "SGI",
                id_usuario: getUserId(),
                id_funcionario_anotacoes: Number(id_anotacao),
                descricao: descricao?tratamentoDescricao(descricao):"",
                descricao_tipo_anotacao: anotacao?anotacao.descricao:null,
                id_tipo_anotacao: anotacao?anotacao.id_tipo_anotacao:null,
            }

            api.post('/rh/cadastro_anotacao/',data, config)
            .then((res)=>{
                let x = JSON.parse(res.data);
                if(x.sucesso === "S"){
                    setmodaleditar(false);
                    Swal.fire({
                        icon: 'success',
                        title:"Sucesso",
                        text: `Anotação atualizada`,
                        confirmButtonText: 'fechar',
                    })
                    props.reloadAnotacoes();
                }else{
                    setmodaleditar(false);
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `${x.motivo}`,
                        confirmButtonText: 'fechar',
                    })
                }
            })
        }else{
            setobrigatorio(true);
        }
    }

     //EXCLUIR Anotação


     function excluir(obj) {

        Swal.fire({
            icon: 'warning',
            text: 'Excluir  anotação?',
            showCancelButton: true,
            confirmButtonText: 'Sim',
            cancelButtonText: 'Não',
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed) { 
                let config = { headers: { "Authorization": `Bearer ${getToken()}` } }
                let data = {
                    'aplicacao': "SGI",
                    'id_usuario': getUserId(),
                    'id_funcionario_anotacoes': obj.id_funcionario_anotacoes,
                    'excluir':true,
                }
                api.post('rh/cadastro_anotacao/', data, config)
                    .then((res) => {
                        let x = JSON.parse(res.data);
                        if (x.sucesso === "S") {
                            Swal.fire({
                                icon: 'success',
                                title: "Sucesso",
                                text: 'Anotação excluída',
                                confirmButtonText: 'ok',
                            }).then(() => {
                                props.reloadAnotacoes();
                            })
                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: "Erro",
                                text: `Erro ao excluir a anotação`,
                                confirmButtonText: 'fechar',
                            })
                        }
                    }).catch((err) => {
                        Swal.fire({
                            icon: 'error',
                            title: "Erro",
                            text: `${err}`,
                            confirmButtonText: 'fechar',
                        })
                    })
            }
        })
    }

    function seleciona(obj) {
        setId_anotacao(obj.id_funcionario_anotacoes)
        setDescricao(obj.descricao);
        setAnotacao({"id_tipo_anotacao": obj.id_tipo_anotacao, "descricao": obj.descricao_tipo_anotacao});
        setmodaleditar(true);
    }

    const modalCadastroFooter = () => {
        return (
            <div>
                {obrigatorio?<p className='mensagem-erro'>Informe os campos obrigatórios</p>:null}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setmodalcadastro(false);
                        setobrigatorio(false);
                    }} 
                    className="btn-2" />
                <Button 
                    label="Salvar" 
                    icon="pi pi-check" 
                    onClick={() => adicionarAnotacao()} 
                    autoFocus 
                    className="btn-1" />
            </div>
        );
    }

    const modalEditarFooter = () => {
        return (
            <div>
                {obrigatorio?<p className='mensagem-erro'>Informe os campos obrigatórios</p>:null}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setmodaleditar(false);
                        setobrigatorio(false);
                    }} 
                    className="btn-2" />
                <Button 
                    label="Salvar" 
                    icon="pi pi-check" 
                    onClick={() => atualizarAnotacao()} 
                    autoFocus 
                    className="btn-1" />
            </div>
        );
    }

    function novaAnotacao() {
        setAnotacao(null);
        setDescricao('');
        setmodalcadastro(true);
    }

    return ( 
        <div>
            <br/>
            <div className='painel-anotacoes'>
                <div>
                    <Button
                        className="btn-1" 
                        label="Nova Anotação" 
                        icon="pi pi-plus" 
                        onClick={() => novaAnotacao()} 
                    />
                </div>
                <Dialog 
                    header="Nova Anotação"
                    visible={modalcadastro}
                    style={{ width: '50%' }}
                    footer={modalCadastroFooter}
                    onHide={
                        () =>{
                            setmodalcadastro(false);
                            setobrigatorio(false);
                        }
                    }
                >
                    <span className='field grupo-input-label'>
                        Tipo da Anotação *
                        <Dropdown 
                            value={anotacao} 
                            options={props.tipo_anotacao} 
                            onChange={(e)=>setAnotacao(e.value)} 
                            optionLabel="descricao" 
                            className='input-select p-inputtext-sm'
                            style={{height: "40px"}}
                        />
                    </span>
                    <span className="field grupo-input-label">
                        Descrição
                        <InputTextarea 
                            rows={5} cols={30} 
                            className="resumo-anotacoes"
                            onChange={(e)=>setDescricao(e.target.value)}
                            style={{height: "40px"}}
                        />
                    </span>
                    
                </Dialog>

                {/*para editar*/}

                <Dialog 
                    header="Editar Anotação"
                    visible={modalEditar}
                    style={{ width: '50%' }}
                    footer={modalEditarFooter}
                    onHide={
                        () =>{
                            setmodaleditar(false);
                            setobrigatorio(false);
                        }
                    }
                >
                    <span className='field grupo-input-label'>
                        Tipo da Anotação *
                        <Dropdown 
                            value={anotacao} 
                            options={props.tipo_anotacao} 
                            onChange={(e)=>setAnotacao(e.value)} 
                            optionLabel="descricao" 
                            className='input-select p-inputtext-sm'
                            style={{height: "40px"}}
                        />
                    </span>
                    <span className="field grupo-input-label">
                        Descrição
                        <InputTextarea
                            value={descricao} 
                            rows={5} cols={30} 
                            className="resumo-anotacoes"
                            onChange={(e)=>setDescricao(e.target.value)}
                            style={{height: "40px"}}
                        />
                    </span>
                    
                </Dialog>

                <ListarAnotacoesEstagio 
                    anotacoes={props.anotacoes.get} 
                    seleciona={seleciona} 
                    excluir={excluir}
                    ativos={props.ativos}
                />
            </div>
        </div>
    );
}

export default AnotacoesEstagiario;

