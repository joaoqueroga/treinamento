import React, {useState, useEffect} from "react";
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { FilterMatchMode } from 'primereact/api';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import Swal from 'sweetalert2';
import { ProgressSpinner } from 'primereact/progressspinner';
import { BreadCrumb } from 'primereact/breadcrumb';

import api from "../../../../config/api";
import { getToken, getUserId } from "../../../../config/auth";
import { Checkbox } from 'primereact/checkbox';

import SeletorLotacao from '../../../../components/SeletorLotacao';
import SeletorServidor from "../../../../components/SeletorServidor";

function AprovacaoListaServidores() {

    const items = [
        { label: 'Administrador' },
        { label: 'Aprovadores de Férias e Folgas' }
    ];

    const [filters] = useState({
        'descricao_lotacao_ferias': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'descricao_lotacao_folga': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'nome': { value: null, matchMode: FilterMatchMode.CONTAINS }
    });

    const home = { icon: 'pi pi-home', url: '/inicio' }

    const [dados, setDados] = useState([]);
    const [obrigatorio, setobrigatorio] = useState(false);

    const [obrigatorioeditar, setobrigatorioEditar] = useState(false);

    const [loading, setLoading] = useState(true);

    // dados do objeto
    const [id, setId] = useState(null);
    const [nome, setNome] = useState('');


    const [dialog, setDialog] = useState(false);
    const [dialog_editar, setDialog_editar] = useState(false);

    const [check_ferias, setCheck_ferias] = useState(false);
    const [check_folgas, setCheck_folgas] = useState(false);

    const [lotacao_ferias, setLotacao_ferias] = useState(null);
    const [lotacao_folgas, setLotacao_folgas] = useState(null);
    const [servidor, setServidor] = useState(null);


    useEffect(() => {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId()
        }
        api.post('/core/listar_aprovadores_ferias', data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            setDados(x);
            setLoading(false);
        })
    }, []);

    const irBodyTemplate = (rowData) => {
        return(
            <span className='botoes-acao'>
            <Button 
                onClick={()=>selecionaAprovador(rowData)}
                icon="pi pi-cog"
                className="btn-darkblue"
                title="Configurar"
            />
            <Button 
                onClick={()=>excluir(rowData)}
                icon="pi pi-trash"
                className="btn-red"
                title="Excluir"
            />
            </span>
        )                                    
    }

    function liparDados() {
        setId("");
        setNome("");
        setServidor(null);
        setLotacao_ferias(null);
        setLotacao_folgas(null);
        setCheck_ferias(false);
        setCheck_folgas(false);
    }

    function reload() {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId()
        }
        api.post('/core/listar_aprovadores_ferias',data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            setDados(x);
        })
    }

    function set_check_ferias(value) {
        setCheck_ferias(value);
        if(!value) setLotacao_ferias(null);
        setobrigatorio(false);
    }

    function set_check_folgas(value) {
        setCheck_folgas(value);
        if(!value) setLotacao_folgas(null);
        setobrigatorio(false);
    }

    function salvar() {
        if(servidor){    
            let data = {
                "aplicacao": "SGI",
                "id_usuario": getUserId(),
                "aprova_ferias": check_ferias,
                "aprova_folga": check_folgas,
                "id_lotacao_ferias": lotacao_ferias? lotacao_ferias.id_lotacao : null,
                "id_lotacao_folga": lotacao_folgas ? lotacao_folgas.id_lotacao : null,
                "id_funcionario" : servidor.id_funcionario,
                "excluir": false
            }
            
            let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
            api.post('/core/salvar_aprovadores_ferias',data, config)
            .then((res)=>{
                let x = JSON.parse(res.data);
                if(x.sucesso === "S"){
                    setDialog(false);
                    Swal.fire({
                        icon: 'success',
                        title:"Sucesso",
                        text: `Aprovador Cadastrado`,
                        confirmButtonText: 'fechar',
                    })
                    reload();
                    liparDados();
                }else{
                    setDialog(false);
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `${x.motivo}`,
                        confirmButtonText: 'fechar',
                    })
                }
            }).catch((error)=>{
                setobrigatorio(false);
                Swal.fire({
                    icon:'error',
                    title:"Erro",
                    text: `${error}`,
                    confirmButtonText: 'fechar',
                })
            })

            liparDados();
            setDialog(false);
        }else{
            setobrigatorio(true);
        }
    }

    function atualizar() {
        if(servidor){    
            let data = {
                "aplicacao": "SGI",
                "id_usuario": getUserId(),
                "aprova_ferias": check_ferias,
                "aprova_folga": check_folgas,
                "id_lotacao_ferias": lotacao_ferias? lotacao_ferias.id_lotacao : null,
                "id_lotacao_folga": lotacao_folgas ? lotacao_folgas.id_lotacao : null,
                "id_funcionario" : servidor.id_funcionario,
                "excluir": false
            }
            
            let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
            api.post('/core/salvar_aprovadores_ferias',data, config)
            .then((res)=>{
                let x = JSON.parse(res.data);
                if(x.sucesso === "S"){
                    setDialog(false);
                    Swal.fire({
                        icon: 'success',
                        title:"Sucesso",
                        text: `Aprovador Atualizado`,
                        confirmButtonText: 'fechar',
                    })
                    reload();
                    liparDados();
                }else{
                    setDialog_editar(false);
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `${x.motivo}`,
                        confirmButtonText: 'fechar',
                    })
                }
            }).catch((error)=>{
                setobrigatorio(false);
                Swal.fire({
                    icon:'error',
                    title:"Erro",
                    text: `${error}`,
                    confirmButtonText: 'fechar',
                })
            })

            liparDados();
            setDialog_editar(false);
        }else{
            setobrigatorio(true);
        }
    }

    function excluir(obj) {

        Swal.fire({
            icon:'warning',
            text: 'Remover o aprovador',
            showCancelButton: true,
            confirmButtonText: 'sim',
            cancelButtonText: 'não',
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed) {
                let data = {
                    "aplicacao": "SGI",
                    "id_usuario": getUserId(),
                    "id_funcionario" : obj.id_funcionario,
                    "excluir": true
                }
                let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
                api.post('/core/salvar_aprovadores_ferias',data, config)
                .then((res)=>{
                    let x = JSON.parse(res.data);
                    if(x.sucesso === "S"){
                        setDialog(false);
                        Swal.fire({
                            icon: 'success',
                            title:"Sucesso",
                            text: `Aprovador removido`,
                            confirmButtonText: 'fechar',
                        })
                        reload();
                        liparDados();
                    }else{
                        setDialog_editar(false);
                        Swal.fire({
                            icon:'error',
                            title:"Erro",
                            text: `${x.motivo}`,
                            confirmButtonText: 'fechar',
                        })
                    }
                }).catch((error)=>{
                    setobrigatorio(false);
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `${error}`,
                        confirmButtonText: 'fechar',
                    })
                })
            }
        })
    }

    const modalCadastroFooter = () => {
        return (
            <div>
                {obrigatorio?<p><small className="p-error">Informe os capos obrigatórios (*).</small></p>:null}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setDialog(false);
                        setobrigatorio(false);
                        liparDados();
                    }} 
                    className="btn-2" />
                <Button 
                    label="Salvar" 
                    icon="pi pi-check" 
                    onClick={()=>{
                        salvar();
                    }} 
                    className="btn-1"
                />
            </div>
        );
    }

    const modalEditarFooter = () => {
        return (
            <div>
                {obrigatorioeditar?<p><small className="p-error">Informe os capos obrigatórios (*).</small></p>:null}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setDialog_editar(false);
                        setobrigatorioEditar(false);
                        liparDados();
                    }} 
                    className="btn-2" />
                <Button 
                    label="Salvar" 
                    icon="pi pi-check" 
                    onClick={()=>{
                        atualizar();
                    }} 
                    className="btn-1"
                />
            </div>
        );
    }

    function selecionaAprovador(aprovador) {
        setDialog_editar(true);
        setNome(aprovador.nome);
        setCheck_ferias(aprovador.aprova_ferias);
        setCheck_folgas(aprovador.aprova_folga);
        setLotacao_ferias({"id_lotacao": aprovador.id_lotacao_ferias,  "descricao": aprovador.descricao_lotacao_ferias});
        setLotacao_folgas({"id_lotacao": aprovador.id_lotacao_folga,  "descricao": aprovador.descricao_lotacao_folga});
        setServidor({"id_funcionario": aprovador.id_funcionario, "nome": aprovador.nome})
    }

    return (
        <div className='view'>
        <div className="view-body">
        {
        loading?
        <div className="loading-pagina" ><ProgressSpinner/>Carregando...</div>
        :
        <div>
            <div className="header">
                <BreadCrumb model={items} home={home}/>
                <h6 className="titulo-header">Aprovadores de Férias e Folgas</h6>
                <Button 
                    label="Novo Aprovador" 
                    icon="pi pi-plus" 
                    className="btn-1"
                    onClick={()=>{setDialog(true); liparDados()}}
                />
            </div>
            <div className="lotacao-body">
                <div className="dados-lotacoes">

                <DataTable
                    value={dados}
                    size="small"
                    className="tabela-servidores"
                    dataKey="id"
                    filters={filters}
                    filterDisplay="row"
                    emptyMessage="Nada encontrado"
                    scrollable
                    scrollHeight="73vh"
                    selectionMode="single"
                >
                    <Column
                        header="Nome"
                        field="nome"
                        filter 
                        filterPlaceholder="Buscar" 
                        style={{ flexGrow: 0, flexBasis: '30%' }}
                        showFilterMenu={false}
                    />
                    <Column
                        header="Lotação de Férias"
                        field="descricao_lotacao_ferias"
                        filter 
                        filterPlaceholder="Buscar" 
                        style={{ flexGrow: 0, flexBasis: '30%' }}
                        showFilterMenu={false}
                    />
                    <Column
                        header="Lotação de Folgas"
                        field="descricao_lotacao_folga"
                        filter 
                        filterPlaceholder="Buscar" 
                        style={{ flexGrow: 0, flexBasis: '30%' }}
                        showFilterMenu={false}
                    />
                    <Column 
                        field="botao" 
                        header="Ação" 
                        body={irBodyTemplate} 
                        className="col-centralizado" 
                        style={{ flexGrow: 0, flexBasis: '10%' }}
                    />
                </DataTable>

                </div>

                {/* Para Cadastrar*/}
                <Dialog 
                    header="Novo Aprovador"
                    visible={dialog}
                    style={{ width: '50%' }}
                    footer={modalCadastroFooter}
                    onHide={
                        () =>{
                            setDialog(false);
                            setobrigatorio(false);
                            liparDados();
                        }
                    }
                >
                    <span className="field grupo-input-label">
                        <label>Membro ou Servidor *</label>
                        <SeletorServidor get={servidor} set={setServidor}/>
                    </span>
                    <span className="field grupo-input-label">
                        <div className="field-checkbox">
                            <Checkbox 
                                inputId="ferias-aprovador" 
                                checked={check_ferias} 
                                onChange={e => set_check_ferias(e.checked)} 
                            />
                            <label htmlFor="ferias-aprovador" style={{marginLeft: "10px"}}>Aprovador de Férias</label>
                        </div>
                    </span>
                    {check_ferias?
                    <span className="field grupo-input-label">
                        <label>Lotação Para a Aprovação de Férias</label>
                        <SeletorLotacao get={lotacao_ferias} set={setLotacao_ferias}/>
                    </span>
                    :null
                    }
                    <span className="field grupo-input-label">
                        <div className="field-checkbox">
                            <Checkbox 
                                inputId="folgas-aprovador" 
                                checked={check_folgas} 
                                onChange={e => set_check_folgas(e.checked)} 
                            />
                            <label htmlFor="folgas-aprovador" style={{marginLeft: "10px"}}>Aprovador de Folgas</label>
                        </div>
                    </span>
                    { check_folgas?
                    <span className="field grupo-input-label">
                        <label>Lotação Para a Aprovação de Folgas</label>
                        <SeletorLotacao get={lotacao_folgas} set={setLotacao_folgas}/>
                    </span>
                    :null
                    }
                    <span className="field grupo-input-label">
                        <p><i> <b> Importante: </b> Caso seja marcado como aprovador, se não definir a Lotação este poderá aprovar para qualquer Unidade de Lotação. </i></p>
                    </span>
                </Dialog>

                {/* dialog para configuracao */}

                <Dialog 
                    header="Configurar Aprovador" 
                    visible={dialog_editar} 
                    style={{ width: '50vw' }} 
                    footer={modalEditarFooter} 
                    onHide={() => {
                        setDialog_editar(false);
                        setobrigatorio(false);
                        liparDados();
                        }
                    }
                >
                    <span className="field grupo-input-label">
                        <label>Membro ou Servidor *</label>
                        <InputText 
                            value={nome}
                            disabled
                        />
                    </span>
                    <span className="field grupo-input-label">
                        <div className="field-checkbox">
                            <Checkbox 
                                inputId="ferias-aprovador" 
                                checked={check_ferias} 
                                onChange={e => set_check_ferias(e.checked)} 
                            />
                            <label htmlFor="ferias-aprovador" style={{marginLeft: "10px"}}>Aprovador de Férias</label>
                        </div>
                    </span>
                    {check_ferias?
                    <span className="field grupo-input-label">
                        <label>Lotação Para a Aprovação de Férias</label>
                        <SeletorLotacao get={lotacao_ferias} set={setLotacao_ferias}/>
                    </span>
                    :null
                    }
                    <span className="field grupo-input-label">
                        <div className="field-checkbox">
                            <Checkbox 
                                inputId="folgas-aprovador" 
                                checked={check_folgas} 
                                onChange={e => set_check_folgas(e.checked)} 
                            />
                            <label htmlFor="folgas-aprovador" style={{marginLeft: "10px"}}>Aprovador de Folgas</label>
                        </div>
                    </span>
                    { check_folgas?
                    <span className="field grupo-input-label">
                        <label>Lotação Para a Aprovação de Folgas</label>
                        <SeletorLotacao get={lotacao_folgas} set={setLotacao_folgas}/>
                    </span>
                    :null
                    }
                    <span className="field grupo-input-label">
                        <p><i> <b> Importante: </b> Caso seja marcado como aprovador, se não definir a Lotação este poderá aprovar para qualquer Unidade de Lotação. </i></p>
                    </span>
                </Dialog>

                <br/>
            </div>
        </div>
        }
        </div>
    </div>
    );
}

export default AprovacaoListaServidores;