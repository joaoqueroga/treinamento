import React, { useState } from 'react';
// import './style.scss';
import { Button } from 'primereact/button';
import api from '../../../../../config/api';
import { getToken } from '../../../../../config/auth';
import { InputText } from 'primereact/inputtext';
import Swal from 'sweetalert2';
import { Dialog } from 'primereact/dialog';
import SeletorGrupo from '../../../../../components/seletorGrupo';
import ListarGruposUsuario from '../../../../../components/listarGruposUsuario';

function GruposUsuario(props) {
    const [modalcadastro, setmodalcadastro] = useState(false);
    const [modalEditar, setmodaleditar] = useState(false);
    const [grupo, setGrupo] = useState(null);
    const [obrigatorio, setobrigatorio] = useState(false);

    function adicionarGrupo() {
        if(grupo){
            let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
            let data = {
                "id_usuario": props.id_usuario,
                "grupos": [{"id_grupo": grupo?grupo.id_grupo:null}]
            }
            api.post('/painel_gerenciamento/salvar_atualizar_usuario/', data, config)
            .then((res)=>{
                let x = JSON.parse(res.data);
                if(x.sucessoo === 'S'){
                    setmodalcadastro(false);
                    Swal.fire({
                        icon: 'success',
                        title:"Sucesso",
                        text: `Grupo adicionado ao usuário`,
                        confirmButtonText: 'fechar',
                    })
                    setGrupo(null);
                    props.setreload(!props.reload);
                }else{
                    setmodalcadastro(false);
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `${x.motivo}`,
                        confirmButtonText: 'fechar',
                    })
                }
            }).catch((err)=>{
                Swal.fire({
                    icon:'error',
                    title:"Erro",
                    text: `${err}`,
                    confirmButtonText: 'fechar',
                })
            })
        }else{
            setobrigatorio(true);
        }
    }

    // function atualizarGrupo() {
    //     if(grupo){
    //         let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
    //         let data = {
    //             "id_usuario": props.id_usuario,
    //             "grupos": [{"id_grupo": grupo?grupo.id_grupo:null, "excluir": true}]
    //         }
    //         api.post('/painel_gerenciamento/salvar_atualizar_usuario/', data, config)
    //         .then((res)=>{
    //             let x = JSON.parse(res.data);
    //             if(x.sucessoo === 'S'){
    //                 setmodaleditar(false);
    //                 Swal.fire({
    //                     icon: 'success',
    //                     title:"Sucesso",
    //                     text: `Grupo atualizados ao usuário`,
    //                     confirmButtonText: 'fechar',
    //                 })
    //                 setGrupo(null);
    //                 props.reloadGrupos();
    //             }else{
    //                 setmodalcadastro(false);
    //                 Swal.fire({
    //                     icon:'error',
    //                     title:"Erro",
    //                     text: `${x.motivo}`,
    //                     confirmButtonText: 'fechar',
    //                 })
    //             }
    //         })
    //     }else{
    //         setobrigatorio(true);
    //     }
    // }

    function excluir_grupo_usuario(obj) {
        Swal.fire({
            icon:'warning',
            text: 'Retirar o grupo do usuário?',
            showCancelButton: true,
            confirmButtonText: 'Sim',
            cancelButtonText: 'Não',
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed) {
                let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
                let data = {
                    "id_usuario": props.id_usuario,
                    "grupos": [{"id_grupo": obj?obj.id_grupo:null, "excluir": true}]
                }
                api.post('/painel_gerenciamento/salvar_atualizar_usuario/', data, config)
                .then((res)=>{
                    let x = JSON.parse(res.data);
                    if(x.sucessoo === 'S'){
                        setmodaleditar(false);
                        Swal.fire({
                            icon: 'success',
                            title:"Sucesso",
                            text: `Grupo retirado do usuário`,
                            confirmButtonText: 'fechar',
                        })
                        setGrupo(null);
                        props.reloadGrupos();
                    }else{
                        setmodalcadastro(false);
                        Swal.fire({
                            icon:'error',
                            title:"Erro",
                            text: `${x.motivo}`,
                            confirmButtonText: 'fechar',
                        })
                    }
                }).catch((err)=>{
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `${err}`,
                        confirmButtonText: 'fechar',
                    })
                })
            }
        })
    }

    function seleciona(obj) {
        setGrupo({
            id_grupo : obj.id_grupo,
            descricao : obj.descricao
        });
        setmodaleditar(true);
    }

    const modalCadastroFooter = () => {
        return (
            <div>
                {obrigatorio?<p style={{color: "#f00"}}>Informe os campos obrigatórios</p>:null}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setmodalcadastro(false);
                        setobrigatorio(false);
                    }} 
                    className="btn-2" />
                <Button 
                    label="Salvar" 
                    icon="pi pi-check" 
                    onClick={() => adicionarGrupo()} 
                    autoFocus 
                    className="btn-1"
                />
            </div>
        );
    }

    const modalEditarFooter = () => {
        return (
            <div>
                {obrigatorio?<p style={{color: "#f00"}}>Informe os campos obrigatórios</p>:null}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setmodaleditar(false);
                        setobrigatorio(false);
                    }} 
                    className="btn-2" />
                {/* <Button 
                    label="Salvar" 
                    icon="pi pi-check" 
                    onClick={() => atualizarGrupo()} 
                    autoFocus 
                    className="btn-1"
                /> */}
            </div>
        );
    }

    function novoGrupo() {
        setGrupo(null);
        setmodalcadastro(true);
    }

    return ( 
        <div>
            <br/>
            <div className='painel-grupos'>
                <div>
                    <Button
                        className="btn-1" 
                        label="Novo Grupo" 
                        icon="pi pi-plus" 
                        onClick={() => novoGrupo()} 
                    />
                </div>
                <Dialog 
                    header="Novo Grupo"
                    visible={modalcadastro}
                    style={{ width: '50%' }}
                    footer={modalCadastroFooter}
                    onHide={
                        () =>{
                            setmodalcadastro(false);
                            setobrigatorio(false);
                        }
                    }
                >
                    <span className='grupo-input-label-2cols'>
                        <span className='grupo-input-label' style={{width: "100%"}}>
                            Grupo a ser atribuído*
                            <span className='grupo-input-label'>
                                <SeletorGrupo
                                    get={grupo}
                                    set={setGrupo}
                                />
                            </span>
                        </span>
                    </span>    
                </Dialog>

                {/*para editar*/}

                <Dialog 
                    header="Editar Grupo"
                    visible={modalEditar}
                    style={{ width: '50%' }}
                    footer={modalEditarFooter}
                    onHide={
                        () =>{
                            setmodaleditar(false);
                            setobrigatorio(false);
                        }
                    }
                >
                    <span className='grupo-input-label-2cols'>
                        <span className='grupo-input-label' style={{width: "100%"}}>
                            Grupo atribuído
                            <span className='grupo-input-label'>
                                <InputText
                                    value={grupo?grupo.descricao:""}
                                    disabled
                                    style={{height: "40px"}}
                                />
                            </span>
                        </span>
                    </span>

                </Dialog>

                <ListarGruposUsuario grupo={props.grupo} excluir={excluir_grupo_usuario} seleciona={seleciona}/>
            </div>
        </div>
    );
}

export default GruposUsuario;