// import './style.scss';
import { useEffect, useState } from 'react';
import { Checkbox } from 'primereact/checkbox';
import Swal from 'sweetalert2';
import api from '../../../../../config/api';
import { getToken } from '../../../../../config/auth';
import { Tag } from 'primereact/tag';


export default function UsuarioArvoreRecursos(props){

    function atualizar_arvore(obj) {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "id_usuario": 1,
            "id_recurso": obj.id_recurso,
            "checked": !obj.checked
        }
        // api.post('/painel_gerenciamento/atualizar_arvore_recursos/',data, config)
        // .then((res)=>{
        //     let x = JSON.parse(res.data);
        //     if(x.sucesso === "S"){
        //         Swal.fire({
        //             icon: 'success',
        //             title:"Sucesso",
        //             text: `Árvore do grupo alterada`,
        //             confirmButtonText: 'fechar',
        //         })
        //         props.setRecursosTabs([...x.json]);
        //     }else{
        //         Swal.fire({
        //             icon:'error',
        //             title:"Erro",
        //             text: `${x.motivo}`,
        //             confirmButtonText: 'fechar',
        //         })
        //     }
        // })
    }
    
    return(
        <div className='montagem-menus' style={{height: "65vh"}}>
        <div className='menus-arvore'>
            <div>
                <div>
                    {
                        // cria o menu raiz
                        props.recursosTabs.map((item, i)=>{
                            return(
                                <div key={i} style={{marginLeft: "50px"}}>
                                    
                                    <span className='item-de-recursos'>
                                        <span className='item-de-recursos-conteudos'>
                                            <span>
                                                <i className={`pi ${item.icone}`}></i>
                                                {' '}{item.rotulo}
                                            </span>
                                            <span>
                                                <Tag
                                                    className="mr-2"
                                                    icon={item.eh_menu?'pi pi-list':'pi pi-desktop'}
                                                    value={item.eh_menu?'Menu':'Tela'}
                                                    severity={item.eh_menu?'info':'warning'}
                                                ></Tag>
                                            </span>
                                            <span>
                                                <Checkbox
                                                    className='icone-opcao-recurso'  
                                                    inputId="binary" 
                                                    checked={item.ativo}
                                                    // onChange={()=>atualizar_arvore(item)}
                                                    disabled
                                                />
                                            </span>
                                        </span>
                                    </span>
                                    <div style={{marginLeft: "50px"}}>
                                        {
                                            item.menus_1.map((item, j)=>{
                                                return(
                                                    <div key={j}>
                                                        <span className='item-de-recursos'>
                                                        <span className='item-de-recursos-conteudos'>
                                                            <span>
                                                                <i className={`pi ${item.icone}`}></i>
                                                                {' '}{item.rotulo}
                                                            </span>
                                                            <span>
                                                                <Tag
                                                                    className="mr-2"
                                                                    icon={item.eh_menu?'pi pi-list':'pi pi-desktop'}
                                                                    value={item.eh_menu?'Menu':'Tela'}
                                                                    severity={item.eh_menu?'info':'warning'}
                                                                ></Tag>
                                                            </span>
                                                            <span>
                                                            <Checkbox 
                                                                className='icone-opcao-recurso' inputId="binary" 
                                                                checked={item.ativo}
                                                                // onChange={()=>atualizar_arvore(item)}
                                                                disabled
                                                            />
                                                                </span>
                                                            </span>
                                                        </span>
                                                        <div style={{marginLeft: "100px"}}>
                                                            {
                                                                item.menus_2.map((item, k)=>{
                                                                    return(
                                                                        <div key={k}>
                                                                            <span className='item-de-recursos'>
                                                                            <span className='item-de-recursos-conteudos'>
                                                                                <span>
                                                                                    <i className={`pi ${item.icone}`}></i>
                                                                                    {' '}{item.rotulo}
                                                                                </span>
                                                                                <span>
                                                                                    <Tag
                                                                                        className="mr-2"
                                                                                        icon={item.eh_menu?'pi pi-list':'pi pi-desktop'}
                                                                                        value={item.eh_menu?'Menu':'Tela'}
                                                                                        severity={item.eh_menu?'info':'warning'}
                                                                                    ></Tag>
                                                                                </span>
                                                                                <span>
                                                                                <Checkbox
                                                                                    className='icone-opcao-recurso'  
                                                                                    inputId="binary"
                                                                                    checked={item.ativo}
                                                                                    // onChange={()=>atualizar_arvore(item)}
                                                                                    disabled
                                                                                />
                                                                                </span>
                                                                            </span>
                                                                            </span>
                                                                        </div>
                                                                    )
                                                                })
                                                            }
                                                        </div>
                                                    </div>
                                                )
                                            })
                                        }
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
                
            </div>
        </div>
        </div>
    )
}