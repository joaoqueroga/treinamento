import React, { useState, useEffect, useRef } from 'react';
// import './style.scss';
import { TabView, TabPanel } from 'primereact/tabview';
import { Button } from 'primereact/button';
import Swal from 'sweetalert2';
import {useNavigate, useParams} from 'react-router-dom';
import { ProgressSpinner } from 'primereact/progressspinner';
import { BreadCrumb } from 'primereact/breadcrumb';
import api from '../../../../../config/api';
import { getToken } from '../../../../../config/auth';
import { Toast } from 'primereact/toast';
import { InputText } from 'primereact/inputtext';
import GruposUsuario from '../grupos_usuario';
import UsuarioArvoreRecursos from '../arvore_recurso_usuario';

function InformacoesUsuario() {

    const items = [
        { label: 'Administrador' },
        { label: 'Painel de Gerenciamento' , url: '/admin/painel_gerenciamento'},
        { label: 'Gerenciamento de Usuários'}
    ];
    const home = { icon: 'pi pi-home', url: '/inicio' }

    const [reload, setReload] = useState(false);
    const navigate = useNavigate();
    const { id } = useParams();
    const toast = useRef(null);
    
    const [carregar, setCarregar] = useState(true);

    const [nome, setNome] = useState("");
    const [cpf, setCPF] = useState("");
    const [grupos, setGrupos] = useState([]);
    const [recursos, setRecursosTabs] = useState([]);

    useEffect(() => {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}};
        let data = {"id_usuario": Number(id)};
        api.post('/painel_gerenciamento/listar_usuarios/', data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(Array.isArray(x)){
                setNome(x[0].nome);
                setCPF(x[0].cpf?x[0].cpf:"");
                setGrupos(x[0].grupo);
                api.post('/painel_gerenciamento/listar_arvore_usuario/', data, config)
                .then((res)=>{
                    let y = JSON.parse(res.data);
                    if(Array.isArray(y)){
                        setRecursosTabs(y);
                    }else{
                        Swal.fire({
                            icon:'error',
                            title:"Erro",
                            text: `${y.motivo}`,
                            confirmButtonText: 'fechar',
                        })
                    }
                }).catch((err)=>{
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `${err}`,
                        confirmButtonText: 'fechar',
                    })
                })
            }else{
                Swal.fire({
                    icon:'error',
                    title:"Erro",
                    text: `${x.motivo}`,
                    confirmButtonText: 'fechar',
                })
            }
        }).catch((err)=>{
            Swal.fire({
                icon:'error',
                title:"Erro",
                text: `${err}`,
                confirmButtonText: 'fechar',
            })
        })

    },[reload, id]);

    function  reloadGrupos() {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {"id_usuario": Number(id)}
        api.post('/painel_gerenciamento/listar_usuarios/',data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            setGrupos(x[0].grupo);
        })
    }

    return ( 
        <div className='view'>
            <div className='view-body'>
            {carregar?
            <>
            <div className="header">
                <BreadCrumb model={items} home={home}/>
                <h6 className="titulo-header">Editar Usuário</h6>
            </div>
            <Toast ref={toast}/>
            <TabView className="cadastro-painel">
            <TabPanel header="Dados Usuário">
                <span className="grupo-input-label">
                    <label>Nome usuário</label>
                        <InputText
                            value={nome}
                            disabled
                            style={{height: "40px", width: "100%"}}
                        />
                </span>
                <span className="grupo-input-label">
                    <label>CPF usuário</label>
                        <InputText
                            value={cpf}
                            disabled
                            style={{height: "40px", width: "100%"}}
                        />
                </span>
            </TabPanel>
            <TabPanel header="Grupos">
                <GruposUsuario
                    grupo={grupos}
                    id_usuario={id}
                    reloadGrupos={reloadGrupos}
                    reload={reload}
                    setreload={setReload}
                >
                </GruposUsuario>
            </TabPanel>

            <TabPanel header="Recursos Disponíveis">
                <UsuarioArvoreRecursos
                    recursosTabs={recursos}
                    setRecursosTabs={setRecursosTabs}
                    id_usuario={id}
                    reload={reload}
                    setreload={setReload}
                >
                </UsuarioArvoreRecursos>
            </TabPanel>

            
            </TabView>
            <div className='rodape-cadastro-servidor'>
                <Button label="Voltar" icon="pi pi-replay" className="btn-2" onClick={()=>navigate('/admin/painel_gerenciamento/')}/>
                {/* <Button label="Salvar" icon="pi pi-check" className="btn-1"/> */}
            </div>
            </>
            :
            <div className='loading-pagina'>
                <div className="loading-pagina" ><ProgressSpinner/>Carregando...</div>
            </div>
            }
            </div> 
        </div>
    );
}

export default InformacoesUsuario;