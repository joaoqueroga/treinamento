import React, { useState } from 'react';
import {useNavigate} from 'react-router-dom';
// import './style.scss';
import { FilterMatchMode } from 'primereact/api';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';

function GerenciarUsuarios(props) {

    const navigate = useNavigate();

    const [filters] = useState({
        'nome': { value: null, matchMode: FilterMatchMode.CONTAINS },
    });

    const irBodyTemplate = (rowData) => {
        return(
            <span className='botoes-acao'>
            <Button 
                onClick={()=>navigate(`/admin/painel_gerenciamento/gerenciar_usuario/${rowData.id_usuario}`)}
                icon="pi pi-pencil"
                className="btn-green"
                title="Editar usuário"
            />
            </span>
        )                                    
    }

    return ( 
        <div className='view' style={{height: "73vh"}}>
            <div>
                <DataTable
                    value={props.usuarios}
                    size="small"
                    className="tabela-servidores"
                    dataKey="id"
                    emptyMessage="..."
                    scrollable
                    scrollHeight="70vh"
                    selectionMode="single"
                    filters={filters}
                    filterDisplay="row"
                >
                    <Column 
                        field="id_usuario"
                        header="ID_Funcionario"
                        style={{ flexGrow: 0, flexBasis: '17%' }}
                    />
                    <Column
                        field="nome" 
                        header="Nome"
                        filter 
                        filterPlaceholder="Buscar" 
                        style={{ flexGrow: 0, flexBasis: '73%' }}
                        showFilterMenu={false}
                    />
                    <Column 
                        field="botao" 
                        header="Ação" 
                        body={irBodyTemplate} 
                        className="col-centralizado" 
                        style={{ flexGrow: 0, flexBasis: '20%' }}
                    />
                </DataTable>
            </div>
        </div>
    );
}

export default GerenciarUsuarios;