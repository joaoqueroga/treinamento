import React, { useState, useEffect, useRef } from 'react';
import { TabView, TabPanel } from 'primereact/tabview';
import Swal from 'sweetalert2';
import {useNavigate, useParams} from 'react-router-dom';
import { ProgressSpinner } from 'primereact/progressspinner';
import { BreadCrumb } from 'primereact/breadcrumb';
import api from '../../../config/api';
import { getToken, getUserId } from '../../../config/auth';
import { Toast } from 'primereact/toast';
import GerenciarGrupos from './gerenciar_grupos';
import GerenciarUsuarios from './gerenciar_usuarios';
import GerenciarArvoreRecursos from './gerenciar_arvore_recursos';

function PainelGerenciamento() {

    const items = [
        { label: 'Administrador' },
        { label: 'Painel de Gerenciamento' }
    ];
    const home = { icon: 'pi pi-home', url: '/inicio' }

    const toast = useRef(null);
    
    const [carregar, setCarregar] = useState(true);

    const [usuarios, setUsuarios] = useState([]);

    const [gruposTabs, setGruposTabs] = useState([]);

    const [recursosTabs, setRecursosTabs] = useState([]);

    useEffect(() => {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        // let data = {"id_funcionario": Number(id)}
        api.get('/painel_gerenciamento/listar_usuarios/', config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(Array.isArray(x)){
                setUsuarios(x);
            }else{
                Swal.fire({
                    icon:'error',
                    title:"Erro",
                    // text: `${x.motivo}`,
                    text: 'Erro na busca da lista de usuarios',
                    confirmButtonText: 'fechar',
                })
            }
        }).catch((err)=>{
            Swal.fire({
                icon:'error',
                title:"Erro",
                text: `${err}`,
                confirmButtonText: 'fechar',
            })
        })

        let data_grupo = {"id_usuario": getUserId()}

        api.post('/painel_gerenciamento/listar_grupos/', data_grupo, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(Array.isArray(x)){
                setGruposTabs(x);
            }else{
                Swal.fire({
                    icon:'error',
                    title:"Erro",
                    // text: `${x.motivo}`,
                    text: 'Erro na busca da lista de grupos',
                    confirmButtonText: 'fechar',
                })
            }
        }).catch((err)=>{
            Swal.fire({
                icon:'error',
                title:"Erro",
                text: `${err}`,
                confirmButtonText: 'fechar',
            })
        })

        api.post('/painel_gerenciamento/buscar_arvore_recursos/', data_grupo, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(Array.isArray(x)){
                setRecursosTabs(x);
            }else{
                Swal.fire({
                    icon:'error',
                    title:"Erro",
                    // text: `${x.motivo}`,
                    text: 'Erro na busca da árvore de recursos',
                    confirmButtonText: 'fechar',
                })
            }
        }).catch((err)=>{
            Swal.fire({
                icon:'error',
                title:"Erro",
                text: `${err}`,
                confirmButtonText: 'fechar',
            })
        })

    },[]);

    function  reloadRecursos() {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {"id_usuario": getUserId()}
        api.post('/painel_gerenciamento/buscar_arvore_recursos/',data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            setRecursosTabs(x);
        })
    }

    function reloadGrupos() {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {"id_usuario": getUserId()}
        api.post('/painel_gerenciamento/listar_grupos/',data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            setGruposTabs(x);
        })
    }

    return ( 
        <div className='view'>
            <div className='view-body'>
            {carregar?
            <>
            <div className="header">
                <BreadCrumb model={items} home={home}/>
                <h6 className="titulo-header">Painel de Gerenciamento</h6>
            </div>
            <Toast ref={toast}/>
            <TabView className="painel-gerenciamento">
            <TabPanel header="Usuários">
                <GerenciarUsuarios
                    usuarios={usuarios}
                    recursosTabs={recursosTabs}
                    setRecursosTabs={setRecursosTabs}
                    reloadRecursos={reloadRecursos}
                ></GerenciarUsuarios>
            </TabPanel>
            <TabPanel header="Grupos">
                <GerenciarGrupos
                    gruposTabs={gruposTabs}
                    reloadGrupos={reloadGrupos}
                ></GerenciarGrupos>
            </TabPanel>
            <TabPanel header="Recursos">
                <GerenciarArvoreRecursos
                    recursosTabs={recursosTabs}
                    setRecursosTabs={setRecursosTabs}
                    reloadRecursos={reloadRecursos}
                >
                </GerenciarArvoreRecursos>
            </TabPanel>
            
            </TabView>
            </>
            :
            <div className='loading-pagina'>
                <div className="loading-pagina" ><ProgressSpinner/>Carregando...</div>
            </div>
            }
            </div> 
        </div>
    );
}

export default PainelGerenciamento;