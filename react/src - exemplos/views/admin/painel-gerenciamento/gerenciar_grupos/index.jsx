import React, { useState } from 'react';
import {useNavigate} from 'react-router-dom';
// import './style.scss';
import { ProgressSpinner } from 'primereact/progressspinner';
import { FilterMatchMode } from 'primereact/api';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import Swal from 'sweetalert2';
import api from '../../../../config/api';
import { getToken, getUserId } from '../../../../config/auth';

function GerenciarGrupos(props) {

    const navigate = useNavigate();

    const [loading, setLoading] = useState(false);

    const [modalcadastro, setmodalcadastro] = useState(false);
    const [modaldeletar, setmodaldeletar] = useState(false);

    const [filters] = useState({
        'descricao': { value: null, matchMode: FilterMatchMode.CONTAINS },
    });
    
    const[nome, setNome] = useState('');
    const[grupoDeletar, setgrupoDeletar] = useState(0);

    function adicionarGrupo(){
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "id_usuario": getUserId(),
            "descricao": nome,
        }
        api.post('/painel_gerenciamento/salvar_grupo/',data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucessoo === "S"){
                Swal.fire({
                    icon: 'success',
                    title:"Sucesso",
                    text: `Grupo criado com sucesso`,
                    confirmButtonText: 'fechar',
                })
                setmodalcadastro(false);
                setNome('');
                props.reloadGrupos();
            }else{
                Swal.fire({
                    icon:'error',
                    title:"Erro",
                    text: `${x.motivo}`,
                    confirmButtonText: 'fechar',
                })
            }
        })
    }

    function excluirGrupo (){
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "id_usuario": getUserId(),
            "id_grupo": grupoDeletar,
            "excluir": true
        }
        setmodaldeletar(false);
        // api.post('/painel_gerenciamento/salvar_grupo/',data, config)
        // .then((res)=>{
        //     let x = JSON.parse(res.data);
        //     if(x.sucessoo === "S"){
        //         Swal.fire({
        //             icon: 'success',
        //             title:"Sucesso",
        //             text: `Grupo excluido com sucesso`,
        //             confirmButtonText: 'fechar',
        //         })
        //         setmodalcadastro(false);
        //         setNome('');
        //         props.reloadGrupos();
        //     }else{
        //         Swal.fire({
        //             icon:'error',
        //             title:"Erro",
        //             text: `${x.motivo}`,
        //             confirmButtonText: 'fechar',
        //         })
        //     }
        // })
    }

    const irBodyTemplate = (rowData) => {
        return(
            <span className='botoes-acao'>
            <Button 
                onClick={()=>navigate(`/admin/painel_gerenciamento/gerenciar_grupo/${rowData.id_grupo}`)}
                icon="pi pi-pencil"
                className="btn-green"
                title="Editar grupo"
            />

            <Button
                onClick={() => ativarModalDeletar(rowData)}
                icon="pi pi-trash"
                className="btn-red"
                title="Excluir"
            />
            </span>
        )                                    
    }

    const modalCadastroFooter = () => {
        return (
            <div>
                {/* {obrigatorio?<p style={{color: "#f00"}}>Informe os campos obrigatórios</p>:null} */}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setmodalcadastro(false);
                        // setobrigatorio(false);
                    }} 
                    className="p-button-text" />
                <Button 
                    label="Salvar" 
                    icon="pi pi-check" 
                    onClick={() => adicionarGrupo()} 
                    autoFocus 
                />
            </div>
        );
    }

    const modalDeletarFooter = () => {
        return (
            <div>
                {/* {obrigatorio?<p style={{color: "#f00"}}>Informe os campos obrigatórios</p>:null} */}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setmodaldeletar(false);
                        // setobrigatorio(false);
                    }} 
                    className="p-button-text" />
                <Button 
                    label="Deletar" 
                    icon="pi pi-trash" 
                    onClick={() => excluirGrupo()} 
                    autoFocus 
                />
            </div>
        );
    }

    function novoGrupo() {
        setmodalcadastro(true);
    }

    function ativarModalDeletar(obj) {
        setgrupoDeletar(obj.id_grupo);
        setmodaldeletar(true);
    }

    return ( 
        <div className='view' style={{height: "75vh"}}>
            <div>
                <Button
                    className="btn-1" 
                    label="Novo Grupo" 
                    icon="pi pi-plus" 
                    onClick={() => novoGrupo()} 
                />
            </div>
            {/* criar grupo */}
            <Dialog 
                header="Novo Grupo"
                visible={modalcadastro}
                style={{ width: '50%' }}
                footer={modalCadastroFooter}
                onHide={
                    () =>{
                        setmodalcadastro(false);
                        // setobrigatorio(false);
                    }
                }
            >
                <span className='grupo-input-label-2cols'>
                    <span className='grupo-input-label' style={{width: "100%"}}>
                        Nome do Grupo*
                        <span className='grupo-input-label'>
                        <InputText 
                            value={nome} 
                            onChange={(e) => setNome(e.target.value)}
                            style={{width: "100%"}} 
                        />
                        </span>
                    </span>
                </span>    
            </Dialog>

            {/* deletar grupo */}
            <Dialog 
                header="Novo Grupo"
                visible={modaldeletar}
                style={{ width: '50%' }}
                footer={modalDeletarFooter}
                onHide={
                    () =>{
                        setmodaldeletar(false);
                        // setobrigatorio(false);
                    }
                }
            >
                <span className='grupo-input-label-2cols'>
                    Deseja deletar o grupo selecionado?
                </span>    
            </Dialog>


            <div>
            {
            loading?
            <div className="loading-pagina" ><ProgressSpinner/>Carregando...</div>
            :
            <div>
                <div>
                    <DataTable
                        value={props.gruposTabs}
                        size="small"
                        className="tabela-grupos"
                        dataKey="id"
                        emptyMessage="..."
                        scrollable
                        scrollHeight="65vh"
                        selectionMode="single"
                        filters={filters}
                        filterDisplay="row"
                    >
                        <Column 
                            field="id_grupo"
                            header="ID_Grupo"
                            style={{ flexGrow: 0, flexBasis: '17%' }}
                        />
                        <Column
                            field="descricao" 
                            header="Nome" 
                            filter 
                            filterPlaceholder="Buscar" 
                            style={{ flexGrow: 0, flexBasis: '73%' }}
                            showFilterMenu={false}
                        />
                        <Column 
                            field="botao" 
                            header="Ação" 
                            body={irBodyTemplate} 
                            className="col-centralizado" 
                            style={{ flexGrow: 0, flexBasis: '20%' }}
                        />
                    </DataTable>
                </div>

            </div>
            }
            </div>
        </div>
    );
}

export default GerenciarGrupos;