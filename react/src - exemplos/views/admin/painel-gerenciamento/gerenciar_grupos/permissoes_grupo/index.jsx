import React, { useState, useEffect } from 'react';
import { ProgressSpinner } from 'primereact/progressspinner';
import { Button } from 'primereact/button';
import api from '../../../../../config/api';
import { getToken } from '../../../../../config/auth';
import { InputText } from 'primereact/inputtext';
import Swal from 'sweetalert2';
import { Dialog } from 'primereact/dialog';
import ListarPermissoesGrupo from '../../../../../components/listarPermissoesGrupo';
import SeletorRecursos from '../../../../../components/SeletorRecursos';

function PermissoesGrupo(props) {
    const [modalcadastro, setmodalcadastro] = useState(false);
    const [modalEditar, setmodaleditar] = useState(false);
    const [permissao, setPermissao] = useState(null);
    const [obrigatorio, setobrigatorio] = useState(false);

    function adicionarPermissao() {
        if(permissao){
            let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
            let data = {
                "id_usuario": 1,
                "id_grupo": props.id_grupo,
                "id_recurso": permissao.id_recurso,
            }
            api.post('/painel_gerenciamento/salvar_atualizar_grupo/', data, config)
            .then((res)=>{
                let x = JSON.parse(res.data);
                if(x.sucessoo === 'S'){
                    setmodalcadastro(false);
                    Swal.fire({
                        icon: 'success',
                        title:"Sucesso",
                        text: `Permissão/recurso adicionado ao grupo`,
                        confirmButtonText: 'fechar',
                    })
                    setPermissao(null);
                    props.reloadPermissoes();
                }else{
                    setmodalcadastro(false);
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `${x.motivo}`,
                        confirmButtonText: 'fechar',
                    })
                }
            }).catch((err)=>{
                Swal.fire({
                    icon:'error',
                    title:"Erro",
                    text: `${err}`,
                    confirmButtonText: 'fechar',
                })
            })
        }else{
            setobrigatorio(true);
        }
    }

    function excluir_permissao_grupo(obj) {
        Swal.fire({
            icon:'warning',
            text: 'Retirar a permissão do grupo?',
            showCancelButton: true,
            confirmButtonText: 'Sim',
            cancelButtonText: 'Não',
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed) {
                let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
                let data = {
                    "id_usuario": 1,
                    "id_grupo": props.id_grupo,
                    "id_recurso": obj.id_recurso,
                    "excluir": true
                }
                api.post('/painel_gerenciamento/salvar_atualizar_grupo/', data, config)
                .then((res)=>{
                    let x = JSON.parse(res.data);
                    if(x.sucessoo === 'S'){
                        setmodaleditar(false);
                        Swal.fire({
                            icon: 'success',
                            title:"Sucesso",
                            text: `Recurso retirado do grupo`,
                            confirmButtonText: 'fechar',
                        })
                        setPermissao(null);
                        props.reloadPermissoes();
                    }else{
                        setmodalcadastro(false);
                        Swal.fire({
                            icon:'error',
                            title:"Erro",
                            text: `${x.motivo}`,
                            confirmButtonText: 'fechar',
                        })
                    }
                }).catch((err)=>{
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `${err}`,
                        confirmButtonText: 'fechar',
                    })
                })
            }
        })
    }

    function atualizarPermissao() {
        if(permissao){

        }else{
            setobrigatorio(true);
        }
    }

    function seleciona(obj) {
        setPermissao(obj)
        setmodaleditar(true);
    }

    const modalCadastroFooter = () => {
        return (
            <div>
                {obrigatorio?<p style={{color: "#f00"}}>Informe os campos obrigatórios</p>:null}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setmodalcadastro(false);
                        setobrigatorio(false);
                    }} 
                    className="btn-2" />
                <Button 
                    label="Salvar" 
                    icon="pi pi-check" 
                    onClick={() => adicionarPermissao()} 
                    autoFocus 
                    className="btn-1"
                />
            </div>
        );
    }

    const modalEditarFooter = () => {
        return (
            <div>
                {obrigatorio?<p style={{color: "#f00"}}>Informe os campos obrigatórios</p>:null}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setmodaleditar(false);
                        setobrigatorio(false);
                    }} 
                    className="btn-2" />
                <Button 
                    label="Salvar" 
                    icon="pi pi-check" 
                    onClick={() => atualizarPermissao()} 
                    autoFocus 
                    className="btn-2"
                />
            </div>
        );
    }

    function novaPermissao() {
        setPermissao(null);
        setmodalcadastro(true);
    }

    return ( 
        <div>
            <br/>
            <div className='painel-grupo-permissao'>
                <div>
                    <Button
                        className="btn-1" 
                        label="Nova Permissão/Recurso" 
                        icon="pi pi-plus" 
                        onClick={() => novaPermissao()} 
                    />
                </div>
                <Dialog 
                    header="Novo Permissão/Recurso do Grupo"
                    visible={modalcadastro}
                    style={{ width: '50%' }}
                    footer={modalCadastroFooter}
                    onHide={
                        () =>{
                            setmodalcadastro(false);
                            setobrigatorio(false);
                        }
                    }
                >
                    <span className='grupo-permissao-input-label-2cols'>
                        <span className='grupo-permissao-input-label' style={{width: "100%"}}>
                            Permissão a ser atribuído*
                            <span className='grupo-permissao-input-label'>
                                <SeletorRecursos
                                    get={permissao}
                                    set={setPermissao}
                                ></SeletorRecursos>
                            </span>
                        </span>
                    </span>     
                </Dialog>

                {/*para editar*/}

                <Dialog 
                    header="Editar Permissão/Recurso do Grupo"
                    visible={modalEditar}
                    style={{ width: '50%' }}
                    footer={modalEditarFooter}
                    onHide={
                        () =>{
                            setmodaleditar(false);
                            setobrigatorio(false);
                        }
                    }
                >
                    <span className='grupo-permissao-input-label-2cols'>
                        <span className='grupo-permissao-input-label' style={{width: "100%"}}>
                            Permissão atribuída
                            <span className='grupo-permissao-input-label'>
                                <InputText
                                    value={permissao?permissao.descricao_recurso:""}
                                    disabled
                                    style={{height: "40px"}}
                                />
                            </span>
                        </span>
                    </span>
                </Dialog>

                <ListarPermissoesGrupo permissoes={props.permissoes} excluir={excluir_permissao_grupo} seleciona={seleciona}/>
            </div>
        </div>
    );
}

export default PermissoesGrupo;