import React, { useState, useEffect, useRef } from 'react';
// import './style.scss';
import { TabView, TabPanel } from 'primereact/tabview';
import { Button } from 'primereact/button';
import Swal from 'sweetalert2';
import {useNavigate, useParams} from 'react-router-dom';
import { ProgressSpinner } from 'primereact/progressspinner';
import { BreadCrumb } from 'primereact/breadcrumb';
import api from '../../../../../config/api';
import { getToken, getUserId } from '../../../../../config/auth';
import { Toast } from 'primereact/toast';
import { InputText } from 'primereact/inputtext';
import PermissoesGrupo from '../permissoes_grupo';
import GrupoArvoreRecursos from '../permissoes_arvore_grupo';
import Alertas from '../../../../../utils/alertas';

function InformacoesGrupo() {

    const items = [
        { label: 'Administrador' },
        { label: 'Painel de Gerenciamento' , url: '/admin/painel_gerenciamento'},
        { label: 'Gerenciamento de Grupo'}
    ];
    const home = { icon: 'pi pi-home', url: '/inicio' }

    const [reload, setReload] = useState(false);

    const navigate = useNavigate();
    const { id } = useParams();
    const toast = useRef(null);
    
    const [carregar, setCarregar] = useState(true);

    const [nomeGrupo, setNomeGrupo] = useState("");
    const [idGrupo, setIdGrupo] = useState(0);
    const [permissoes, setPermissoes] = useState([]);

    useEffect(() => {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data_grupo = {"id_usuario": getUserId(), "id_grupo": id}

        api.post('/painel_gerenciamento/listar_grupos/', data_grupo, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(Array.isArray(x)){
                setNomeGrupo(x[0].descricao);
                setIdGrupo(x[0].id_grupo);
                let data_grupo_recurso = {"id_usuario": getUserId(), "id_grupo": x[0].id_grupo}
                api.post('/painel_gerenciamento/listar_arvore_grupo/', data_grupo_recurso, config)
                .then((res)=>{
                    let y = JSON.parse(res.data);
                    if(Array.isArray(y)){
                        setPermissoes(y);
                    }else{
                        Swal.fire({
                            icon:'error',
                            title:"Erro",
                            text: `${y.motivo}`,
                            confirmButtonText: 'fechar',
                        })
                    }
                }).catch((err)=>{
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `${err}`,
                        confirmButtonText: 'fechar',
                    })
                })
            }else{
                Swal.fire({
                    icon:'error',
                    title:"Erro",
                    text: `${x.motivo}`,
                    confirmButtonText: 'fechar',
                })
            }
        }).catch((err)=>{
            Swal.fire({
                icon:'error',
                title:"Erro",
                text: `${err}`,
                confirmButtonText: 'fechar',
            })   
        })

    },[reload, id]);

    function atualizarGrupo () {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data_grupo = {"id_usuario": getUserId(), "id_grupo": id, "descricao": nomeGrupo}
        api.post('/painel_gerenciamento/salvar_grupo/', data_grupo, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucessoo === 'S'){
                Alertas.toast_sucesso("Nome do grupo atualizado");
                setReload(!reload);
            }else{
                Swal.fire({
                    icon:'error',
                    title:"Erro",
                    text: `${x.motivo}`,
                    confirmButtonText: 'fechar',
                })
            }
        }).catch((err)=>{
            Swal.fire({
                icon:'error',
                title:"Erro",
                text: `${err}`,
                confirmButtonText: 'fechar',
            })   
        })
    }

    function  reloadPermissoes() {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {"id_usuario": getUserId(), "id_grupo": id}
        api.post('/painel_gerenciamento/listar_recurso_grupo/',data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            setPermissoes(x);
        })
    }
    
    return ( 
        <div className='view' >
            <div className='view-body'>
            {carregar?
            <>
            <div className="header">
                <BreadCrumb model={items} home={home}/>
                <h6 className="titulo-header">Editar Grupo</h6>
            </div>
            <Toast ref={toast}/>
            <TabView className="cadastro-painel">
            <TabPanel header="Dados Grupo">
                <span className="grupo-input-label">
                    <label>Título Grupo</label>
                        <InputText
                            value={nomeGrupo}
                            onChange={(e) => setNomeGrupo(e.target.value)}
                            style={{height: "40px", width: "100%"}}
                        />
                </span>
                <span>
                    <Button
                        label="Salvar" 
                        icon="pi pi-check" 
                        onClick={() => atualizarGrupo()} 
                        autoFocus 
                        className="btn-1"
                    />
                </span>
            </TabPanel>
            {/* <TabPanel header="Permissões/Recursos">
                <PermissoesGrupo
                    permissoes={permissoes}
                    reloadPermissoes={reloadPermissoes}
                    id_grupo={idGrupo}
                >
                </PermissoesGrupo>
            </TabPanel> */}
            <TabPanel header="Árvore Recursos">
                <GrupoArvoreRecursos
                    recursosTabs={permissoes}
                    setRecursosTabs={setPermissoes}
                    reloadPermissoes={reloadPermissoes}
                    id_grupo={idGrupo}
                    reload={reload}
                    setreload={setReload}
                >
                </GrupoArvoreRecursos>
            </TabPanel>

            
            </TabView>
            <div className='rodape-cadastro-servidor'>
                <Button label="Voltar" icon="pi pi-replay" className="btn-2" onClick={()=>navigate('/admin/painel_gerenciamento/')}/>
                {/* <Button label="Salvar" icon="pi pi-check" className="btn-1"/> */}
            </div>
            </>
            :
            <div className='loading-pagina'>
                <div className="loading-pagina" ><ProgressSpinner/>Carregando...</div>
            </div>
            }
            </div> 
        </div>
    );
}

export default InformacoesGrupo;