import './style.scss';
import { useState } from 'react';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { InputText } from 'primereact/inputtext';
import { Checkbox } from 'primereact/checkbox';
import Swal from 'sweetalert2';
import api from '../../../../config/api';
import { getToken, getUserId } from '../../../../config/auth';
import { BsFillTrashFill, BsPencilSquare  } from "react-icons/bs";


export default function GerenciarArvoreRecursos(props){

    const [modalCadastro, setmodalcadastro] = useState(false);
    const [modalEditar, setmodaleditar] = useState(false);

    const [rotulo, setRotulo] = useState('');
    const [icone, setIcone] = useState('');
    const [rota, setRota] = useState('');
    const [id, setId] = useState(0);

    const [eh_menu, setEh_menu] = useState(false);

    //controle do nivel do item e o index do pai
    
    const [nivel, setNivel] = useState(0);
    
    const [index_1, setIndex_1] = useState(0);
    const [index_2, setIndex_2] = useState(0);

    function limparDados() {
        setRotulo('');
        setIcone('');
        setRota('');
        setEh_menu(false);
    }

    function atualizar_arvore(x) {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        // if(x.id){}
        let data = {
            "id_usuario": getUserId(),
            "rotulo": x.rotulo,
            "icone": x.icone,
            "rota": x.rota || '',
            "eh_menu": x.eh_menu,
            "id_recurso_pai": x.id_recurso_pai,
        }
        api.post('/painel_gerenciamento/atualizar_arvore_recursos/',data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === "S"){
                Swal.fire({
                    icon: 'success',
                    title:"Sucesso",
                    text: `Recurso registrado na árvore`,
                    confirmButtonText: 'fechar',
                })
                props.setRecursosTabs([...x.json]);
            }else{
                Swal.fire({
                    icon:'error',
                    title:"Erro",
                    text: `${x.motivo}`,
                    confirmButtonText: 'fechar',
                })
            }
        })
    }

    function atualizar_no_arvore(){
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "id_usuario": getUserId(),
            "id_recurso": id,
            "rotulo": rotulo,
            "icone": icone,
            "rota": rota || '',
            "eh_menu": eh_menu
        }
        api.post('/painel_gerenciamento/atualizar_arvore_recursos/',data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === "S"){
                Swal.fire({
                    icon: 'success',
                    title:"Sucesso",
                    text: `Recurso alterado na árvore`,
                    confirmButtonText: 'fechar',
                })
                setmodaleditar(false);
                props.setRecursosTabs([...x.json]);
            }else{
                Swal.fire({
                    icon:'error',
                    title:"Erro",
                    text: `${x.motivo}`,
                    confirmButtonText: 'fechar',
                })
            }
        })
    }

    function remover_no_arvore(x){
        Swal.fire({
            icon:'warning',
            text: 'Deseja retirar o recurso da árvore?',
            showCancelButton: true,
            confirmButtonText: 'Sim',
            cancelButtonText: 'Não',
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed) {
                let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
                let data = {
                    "id_usuario": getUserId(),
                    "id_recurso": x.id_recurso,
                    "excluir": true
                }
                api.post('/painel_gerenciamento/atualizar_arvore_recursos/', data, config)
                .then((res)=>{
                    let x = JSON.parse(res.data);
                    if(x.sucesso === 'S'){
                        Swal.fire({
                            icon: 'success',
                            title:"Sucesso",
                            text: `Recurso retirado da árvore`,
                            confirmButtonText: 'fechar',
                        })
                        props.setRecursosTabs([...x.json]);
                    }else{
                        Swal.fire({
                            icon:'error',
                            title:"Erro",
                            text: `${x.motivo}`,
                            confirmButtonText: 'fechar',
                        })
                    }
                }).catch((err)=>{
                    Swal.fire({
                        icon:'error',
                        title:"Erro",
                        text: `${err}`,
                        confirmButtonText: 'fechar',
                    })
                })
            }
        })
    }

    function adicionarMenu() {

        if(nivel === 1){ // cadastra na raiz
            let obj = {
                "id_recurso_pai": 0, 
                "rotulo": rotulo,
                "icone": icone,
                "rota": rota,
                "eh_menu": eh_menu
            }
            atualizar_arvore(obj);
        }else if(nivel === 2){ /// cadastra em um submenu 2
            let obj = {
                "id_recurso_pai": index_1,
                "rotulo": rotulo,
                "icone": icone,
                "rota": rota,
                "eh_menu": eh_menu
            }
            atualizar_arvore(obj);
        }
        else if(nivel === 3){ /// cadastra em um submenu 3
            let obj = {
                "id_recurso_pai": index_2,
                "rotulo": rotulo,
                "icone": icone,
                "eh_menu": eh_menu,
                "rota": rota,
            }
            atualizar_arvore(obj);
        }
        limparDados();
        setmodalcadastro(false);
    }

    function painel_cadastro(nivel) { // abre o painel e define onde colocar
        setNivel(nivel);
        setmodalcadastro(true);
    }

    const renderFooter = () => {
        return (
            <div>
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() => {
                        setmodalcadastro(false);
                        limparDados();
                    }} 
                    className="btn-2" 
                />
                <Button 
                    label="Adicionar" 
                    icon="pi pi-check" 
                    onClick={() => adicionarMenu()} 
                    autoFocus 
                    className="btn-1"
                />
            </div>
        );
    }

    const modalEditarFooter = () => {
        return (
            <div>
                {/* {obrigatorio?<p style={{color: "#f00"}}>Informe os campos obrigatórios</p>:null} */}
                <Button 
                    label="Cancelar" 
                    icon="pi pi-times" 
                    onClick={() =>{
                        setmodaleditar(false);
                        limparDados();
                    }} 
                    className="btn-2" />
                <Button 
                    label="Salvar" 
                    icon="pi pi-check" 
                    onClick={() => atualizar_no_arvore()} 
                    autoFocus 
                    className="btn-1"
                />
            </div>
        );
    }

    function seleciona(obj) {
        setId(obj.id_recurso);
        setRotulo(obj.rotulo||'');
        setIcone(obj.icone||'');
        setRota(obj.rota||'');
        setEh_menu(obj.eh_menu);
        setmodaleditar(true);
    }
    
    return(
        <div className='montagem-menus'>
        <div className='menus-arvore'>
            <div>
                <button 
                    className="botao-adicionar-recurso" 
                    onClick={()=>{
                        setIndex_1(null)
                        setNivel(1);
                        setmodalcadastro(true);
                    }} // adiciona no nivel 1 sem passar index
                > + Recurso principal</button>
                <div>
                    {
                        // cria o menu raiz
                        props.recursosTabs.map((item, i)=>{
                            return(
                                <div key={i} style={{marginLeft: "50px"}}>
                                    
                                    <span className='item-de-recursos'>
                                        <span className='item-de-recursos-conteudos'>
                                            <span>
                                                <i className={`pi ${item.icone}`}></i>
                                                {' '}{item.rotulo}
                                            </span>
                                            <span>
                                                <Checkbox
                                                    className='icone-opcao-recurso'  
                                                    inputId="binary" 
                                                    checked={item.eh_menu} 
                                                    disabled
                                                />
                                                <BsPencilSquare
                                                    className='icone-opcao-recurso'  
                                                    onClick={()=>{seleciona(item)}}
                                                />
                                                <BsFillTrashFill
                                                    className='icone-opcao-recurso' 
                                                    onClick={()=>{remover_no_arvore(item)}}
                                                />
                                            </span>
                                        </span>
                                    </span>
                                    <button 
                                        className="botao-adicionar-recurso"
                                        onClick={()=>{
                                            setIndex_1(item.id_recurso)
                                            setNivel(2);
                                            setmodalcadastro(true);
                                        }} // adiciona no index passando o nivel
                                    >+</button>
                                    <div style={{marginLeft: "50px"}}>
                                        {
                                            item.menus_1.map((item, j)=>{
                                                return(
                                                    <div key={j}>
                                                        <span className='item-de-recursos'>
                                                        <span className='item-de-recursos-conteudos'>
                                                            <span>
                                                                <i className={`pi ${item.icone}`}></i>
                                                                {' '}{item.rotulo}
                                                            </span>
                                                            <span>
                                                            <Checkbox 
                                                                className='icone-opcao-recurso' inputId="binary" 
                                                                checked={item.eh_menu} 
                                                                disabled
                                                            />
                                                            <BsPencilSquare
                                                                className='icone-opcao-recurso' 
                                                                onClick={()=>{seleciona(item)}}
                                                            />
                                                            <BsFillTrashFill
                                                                className='icone-opcao-recurso'  
                                                                onClick={()=>{remover_no_arvore(item)}}
                                                            />
                                                                </span>
                                                            </span>
                                                        </span>
                                                        <button 
                                                            className="botao-adicionar-recurso"
                                                            onClick={()=>{
                                                                setNivel(3);
                                                                setIndex_1(i);
                                                                setIndex_2(item.id_recurso);
                                                                setmodalcadastro(true);
                                                            }} // adiciona no index passando o nivel
                                                        >+</button>
                                                        <div style={{marginLeft: "100px"}}>
                                                            {
                                                                item.menus_2.map((item, k)=>{
                                                                    return(
                                                                        <div key={k}>
                                                                            <span className='item-de-recursos'>
                                                                            <span className='item-de-recursos-conteudos'>
                                                                                <span>
                                                                                    <i className={`pi ${item.icone}`}></i>
                                                                                    {' '}{item.rotulo}
                                                                                </span>
                                                                                <span>
                                                                                <Checkbox
                                                                                    className='icone-opcao-recurso'  
                                                                                    inputId="binary"
                                                                                    checked={item.eh_menu} disabled
                                                                                />
                                                                                <BsPencilSquare
                                                                                    className='icone-opcao-recurso' 
                                                                                    onClick={()=>{seleciona(item)}}
                                                                                />
                                                                                <BsFillTrashFill
                                                                                    className='icone-opcao-recurso' 
                                                                                    onClick={()=>{remover_no_arvore(item)}}
                                                                                />
                                        
                                                                                </span>
                                                                            </span>
                                                                            </span>
                                                                        </div>
                                                                    )
                                                                })
                                                            }
                                                        </div>
                                                    </div>
                                                )
                                            })
                                        }
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
                
            </div>
            {/* DIALOG DE CADASTRO */}
            <Dialog 
                header="Cadastro de Recurso" 
                visible={modalCadastro} 
                style={{ width: '50vw' }} 
                footer={renderFooter()} 
                onHide={() => {setmodalcadastro(false); limparDados()}}
                >
                <span style={{width: "100%"}} >
                    Rótulo
                    <InputText 
                        value={rotulo} 
                        onChange={(e) => setRotulo(e.target.value)}
                        style={{width: "100%"}} 
                    />
                </span>
                <span style={{width: "100%"}} >
                    Ícone
                    <InputText 
                        value={icone} 
                        onChange={(e) => setIcone(e.target.value)}
                        style={{width: "100%"}} 
                    />
                </span>
                <span style={{width: "100%"}} >
                    Rota
                    <InputText 
                        value={rota} 
                        onChange={(e) => setRota(e.target.value)}
                        style={{width: "100%"}} 
                    />
                </span>
                <span style={{width: "100%"}} >
                    *Marque caso seja um recurso de menu*
                    {" "}
                    <Checkbox inputId="binary" checked={eh_menu} onChange={e => setEh_menu(e.checked)}/>
                </span>
            </Dialog>
            {/* DIALOG DE EDIÇÃO */}
            <Dialog 
                header="Edição de Recurso" 
                visible={modalEditar}
                style={{ width: '50vw' }} 
                footer={modalEditarFooter()} 
                onHide={() => {
                    setmodaleditar(false);
                    limparDados();
                }}
                >
                <span style={{width: "100%"}} >
                    Rótulo
                    <InputText 
                        value={rotulo} 
                        onChange={(e) => setRotulo(e.target.value)}
                        style={{width: "100%"}} 
                    />
                </span>
                <span style={{width: "100%"}} >
                    Ícone
                    <InputText 
                        value={icone} 
                        onChange={(e) => setIcone(e.target.value)}
                        style={{width: "100%"}} 
                    />
                </span>
                <span style={{width: "100%"}} >
                    Rota
                    <InputText 
                        value={rota} 
                        onChange={(e) => setRota(e.target.value)}
                        style={{width: "100%"}} 
                    />
                </span>
                <span style={{width: "100%"}} >
                    *Marque caso seja um recurso de menu*
                    {" "}
                    <Checkbox inputId="binary" checked={eh_menu} onChange={e => setEh_menu(e.checked)}/>
                </span>
            </Dialog>
        </div>
        </div>
    )
}