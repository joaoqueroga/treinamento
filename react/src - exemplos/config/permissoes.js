import PaginaNaoAutorizada from "../views/paginaNaoAutorizado";
import CryptoJS from 'crypto-js';
import keys from '../config/keys.json'

const Permissoes = ({children, path}) => {

    // entra no array de permissoes se tem a rota que tá acessando
    function verificaPermissao() {
        let array_crypto  = JSON.parse(sessionStorage.getItem("endpoints"));
        let array = [];
        array_crypto.map((i)=>{
            let bytes  = CryptoJS.AES.decrypt(i, keys.secret);
            let originalText = bytes.toString(CryptoJS.enc.Utf8);
            array.push(originalText);
        })
        return array.includes(path);       
    }

    // verifica se o usuario tem permissao para esta rota
    if(verificaPermissao()){
        return children;
    }else{
        return <PaginaNaoAutorizada/>
    }
}

export default Permissoes;