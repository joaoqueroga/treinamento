import keys from './keys.json';

var CryptoJS = require("crypto-js");

export var autenticado = () => {
    if(sessionStorage.getItem("ACCESS_TOKEN")){
        return true
    }else{
        return false
    }
};

export const getToken = () =>{
    return sessionStorage.getItem("ACCESS_TOKEN");
};

export const getUserId = () =>{
    let bytes  = CryptoJS.AES.decrypt(sessionStorage.getItem("id_usuario"), keys.secret);
    let originalText = bytes.toString(CryptoJS.enc.Utf8);
    return Number(originalText);
};

export const getEhDefensor = () =>{
    let bytes  = CryptoJS.AES.decrypt(sessionStorage.getItem("eh_defensor"), keys.secret);
    let originalText = bytes.toString(CryptoJS.enc.Utf8);
    return originalText;
};

export const getRefreshToken = () =>{
    return sessionStorage.getItem("REFRESH_TOKEN");
};

export const login = (user) => {

    let x = JSON.parse(user.acessos);

    let rotas = []
    // criptografa os endpoints disponiveis para acesso
    try {
        rotas.push(CryptoJS.AES.encrypt("/inicio", keys.secret ).toString());
        x.map((r)=>{
            if(r.rota != null)rotas.push(CryptoJS.AES.encrypt(r.rota, keys.secret ).toString());
            r.menus_1.map((s)=>{
                if(s.rota != null)rotas.push(CryptoJS.AES.encrypt(s.rota, keys.secret ).toString());
                s.menus_2.map((t)=>{
                    if(t.rota != null)rotas.push(CryptoJS.AES.encrypt(t.rota, keys.secret ).toString());
                })
            })
        })
    } catch (error) {
        console.warn("Erro na criptografia de endpoints do usuário")
    }

    sessionStorage.setItem("endpoints", JSON.stringify(rotas));
    sessionStorage.setItem("menu", user.acessos);

    sessionStorage.setItem("ACCESS_TOKEN", user.tokens.access);
    sessionStorage.setItem("REFRESH_TOKEN", user.tokens.refresh);
    sessionStorage.setItem('TEMPO_SESSAO', Number(user.tempo_sessao)*60);
    sessionStorage.setItem('nome', user.nome);
    sessionStorage.setItem('id_usuario',   CryptoJS.AES.encrypt(user.id_usuario.toString(), keys.secret ).toString());
    sessionStorage.setItem('cpf', user.cpf);
    sessionStorage.setItem('atividade', 300);
    sessionStorage.setItem('eh_defensor',   CryptoJS.AES.encrypt(user.eh_defensor?"S":"N", keys.secret ).toString());
};

export const logout = () => { 
    sessionStorage.removeItem("ACCESS_TOKEN");
    sessionStorage.removeItem("REFRESH_TOKEN");
    sessionStorage.removeItem('TEMPO_SESSAO');
    sessionStorage.removeItem('nome');
    sessionStorage.removeItem('id_usuario');
    sessionStorage.removeItem('cpf');
    sessionStorage.removeItem('versao');
    sessionStorage.removeItem('atividade');
    sessionStorage.removeItem('menu');
    sessionStorage.removeItem('endpoints');
};