import Swal from "sweetalert2";

class Alertas{

    static sucesso(msg) {
        Swal.fire({
            icon:'success',
            title:"Sucesso",
            text: msg,
            confirmButtonText: 'fechar',
        });
    }

    static erro(msg) {
        Swal.fire({
            icon:'error',
            title:"Erro",
            text: msg,
            confirmButtonText: 'fechar',
        });
    }

    static cuidado(msg) {
        Swal.fire({
            icon:'warning',
            title:"Cuidado",
            text: msg,
            confirmButtonText: 'fechar',
        });
    }

    static atencao(msg) {
        Swal.fire({
            icon:'warning',
            title:"Atenção",
            text: msg,
            confirmButtonText: 'fechar',
        });
    }

    static informacao(msg) {
        Swal.fire({
            icon:'info',
            title:"Informação",
            text: msg,
            confirmButtonText: 'fechar',
        });
    }

    static confirmacao(msg, funcao) {
        Swal.fire({
            icon:'question',
            text: msg,
            showCancelButton: true,
            confirmButtonText: 'sim',
            cancelButtonText: 'não',
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed) {
                funcao();
            }
        })
    }

    static sucesso_acao(msg, funcao) {
        Swal.fire({
            icon:'success',
            title:"Sucesso",
            text: msg,
            confirmButtonText: 'ok',
        }).then((result) => {
            if (result.isConfirmed) {
                funcao();
            }
        })
    }

    static toast_sucesso(msg){
        const Toast = Swal.mixin({
            toast: true,
            position: "top-end",
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
              toast.addEventListener('mouseenter', Swal.stopTimer)
              toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        })
          
        Toast.fire({
        icon: 'success',
        title: msg
        })
    }

    static toast_erro(msg){
        const Toast = Swal.mixin({
            toast: true,
            position: "top-end",
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
              toast.addEventListener('mouseenter', Swal.stopTimer)
              toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        })
          
        Toast.fire({
        icon: 'error',
        title: msg
        })
    }

    static toast_cuidado(msg){
        const Toast = Swal.mixin({
            toast: true,
            position: "top-end",
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
              toast.addEventListener('mouseenter', Swal.stopTimer)
              toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        })
          
        Toast.fire({
        icon: 'warning',
        title: msg
        })
    }

    static toast_informacao(msg){
        const Toast = Swal.mixin({
            toast: true,
            position: "top-end",
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
              toast.addEventListener('mouseenter', Swal.stopTimer)
              toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        })
          
        Toast.fire({
        icon: 'info',
        title: msg
        })
    }
}

export default Alertas;