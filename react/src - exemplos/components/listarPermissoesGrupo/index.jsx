import React, {useState, useEffect} from "react";
import { FilterMatchMode } from 'primereact/api';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';

function ListarPermissoesGrupo(props) {
    const [permissoes, setPermissoes] = useState([]);
    
    const [filters] = useState({
        'descricao_recurso': { value: null, matchMode: FilterMatchMode.CONTAINS },
    });

    useEffect(() => {
        setPermissoes(props.permissoes);
    }, [props.permissoes]);

    const irBodyTemplate = (rowData) => {
        return(
            <span>
            {/* <Button 
                onClick={()=>props.seleciona(rowData)}
                icon="pi pi-pencil"
                className="btn-green"
                title="Editar Grupo/Permissão"
            /> */}
            <Button
                icon="pi pi-trash"
                className="btn-red"
                onClick={()=>props.excluir(rowData)}
                style={{marginLeft:'5px'}} 
            />
            </span>
        )                                    
    }

    return (
        <div style={{width: "100%", marginTop: "10px"}}>
        <DataTable
            value={props.permissoes}
            size="small"
            dataKey="id"
            filters={filters}
            filterDisplay="row"
            scrollable
            scrollHeight="55vh"
            emptyMessage="Nada encontrado"
            selectionMode="single"
        >
            <Column
                field="id_recurso" 
                header="ID Recurso"
                style={{ flexGrow: 0, flexBasis: '15%' }}
            />
            <Column
                field="descricao_recurso" 
                header="Descrição" 
                filter 
                filterPlaceholder="Buscar" 
                style={{ flexGrow: 0, flexBasis: '70%' }}
                showFilterMenu={false}
            />
            <Column 
                field="botao" 
                header="Ação"
                body={irBodyTemplate} 
                className="col-centralizado" 
                style={{ flexGrow: 0, flexBasis: '15%' }}
            />
        </DataTable>
        </div>
    );
}

export default ListarPermissoesGrupo;