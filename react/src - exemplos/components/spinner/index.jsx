import React from "react";

import { ProgressSpinner } from 'primereact/progressspinner';
import './style.scss';

function TelaSpinner(props) {
    return ( 
        <div className="div-tela-spinner">
            {
                <ProgressSpinner size={props.tamanho} />
            }
            <p>{props.texto}</p>
        </div>
    );
}

export default TelaSpinner;