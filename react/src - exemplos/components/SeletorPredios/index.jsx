import React, {useState, useEffect} from "react";
import { FilterMatchMode } from 'primereact/api';
import { DataTable } from 'primereact/datatable';
import { InputText } from 'primereact/inputtext';
import { Dialog } from 'primereact/dialog';

import { Column } from 'primereact/column';
import api from "../../config/api";
import { getToken, getUserId } from "../../config/auth";
import TelaSpinnerComponente from "../spinner_componente";

function SeletorPredio(props) {

    const [predios, setPredios] = useState([]);
    const [display, setDisplay] = useState(false);

    const [erro, setErro] = useState(false);
    const [carregando, setCarregando] =  useState(true);

    const [filters] = useState({
        'nome': { value: null, matchMode: FilterMatchMode.CONTAINS }
    });

    function seleciona(obj) {
        props.set(obj);
        setDisplay(false);
    }

    useEffect(() => {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId()
        }
        api.post('/rh/predios/',data , config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === 'S'){
                setPredios(x.predio);
                setCarregando(false);
            }else{
               setErro(true);
            }
        })
        
    }, []);

    return (
        <div>
            <div className="p-inputgroup">
                <InputText 
                    type="text" 
                    value={props.get?props.get.nome:""}
                    onClick={()=>setDisplay(true)}
                />
                <span className="p-inputgroup-addon">
                    <i className="pi pi-map-marker"></i>
                </span>
            </div>
            <Dialog 
            header="Prédios"
            visible={display} 
            style={{ width: '50%', height:'100%'}}
            onHide={() => setDisplay(false)}
            >
                <div className='card-selecionar-chefe'>
                    <DataTable
                        value={predios}
                        size="small"
                        className="p-datatable-customers tabela-servidores"
                        dataKey="id"
                        filters={filters}
                        filterDisplay="row"
                        emptyMessage={carregando?"...":"Nada encontrado"}
                        scrollable
                        scrollHeight="80vh"
                        selectionMode="single"
                        onSelectionChange={e => seleciona(e.value)}
                    >
                        <Column
                            header="Nome"
                            field="nome"
                            filter 
                            filterPlaceholder="Buscar" 
                            style={{ flexGrow: 0, flexBasis: '100%' }}
                            showFilterMenu={false}
                        />
                    </DataTable>
                    {
                        carregando?<TelaSpinnerComponente texto="Buscando Prédios" />:null
                    }
                    {
                        erro?<p><small className="p-error">Erro ao carregar os dados</small> </p>:null
                    }
                </div>
            </Dialog>
        </div>
    );
}

export default SeletorPredio;