import React, {useState, useEffect} from "react";
import { FilterMatchMode } from 'primereact/api';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';

function ListarFuncoesServidor(props) {
    const [funcoes, setFuncoes] = useState([]);
    
    const [filters] = useState({
        'funcao_id': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'descricao': { value: null, matchMode: FilterMatchMode.CONTAINS },
    });

    useEffect(() => {
        setFuncoes(props.funcoes);
    }, [props.funcoes]);

    const irBodyTemplate = (rowData) => {
        return(
            <span>
            <Button 
                onClick={()=>props.seleciona(rowData)}
                icon="pi pi-pencil"
                className="btn-green"
                title="Editar Anotação"
                disabled={!props.ativos}
            />
            </span>
        )                                    
    }

    return (
        <div style={{width: "100%", marginTop: "10px"}}>
        <DataTable
            value={funcoes}
            size="small"
            dataKey="id"
            filters={filters}
            filterDisplay="row"
            scrollable
            scrollHeight="55vh"
            emptyMessage="Nada encontrado"
            selectionMode="single"
            showFilterMenu={false}
        >
            <Column
                field="descricao" 
                header="Descrição" 
                filter 
                filterPlaceholder="Buscar" 
                style={{ flexGrow: 0, flexBasis: '70%' }}
                showFilterMenu={false}
            />
            <Column
                field="data_inicio" 
                header="Data de Início" 
                style={{ flexGrow: 0, flexBasis: '10%' }}
            />
            <Column
                field="data_fim" 
                header="Data de Fim"
                style={{ flexGrow: 0, flexBasis: '10%' }}
            />
            <Column 
                field="botao" 
                header="Ação"
                body={irBodyTemplate} 
                className="col-centralizado" 
                style={{ flexGrow: 0, flexBasis: '10%' }}
            />
        </DataTable>
        </div>
    );
}

export default ListarFuncoesServidor;