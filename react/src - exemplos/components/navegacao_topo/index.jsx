import React, {useState, useEffect, useContext, useRef} from 'react';
import {useNavigate} from 'react-router-dom';
import { logout } from '../../config/auth';
import Offcanvas from 'react-bootstrap/Offcanvas';
import { Avatar } from 'primereact/avatar';
import { Button } from 'primereact/button';
import { Sidebar } from 'primereact/sidebar';
import { Column } from 'primereact/column';
import { DataTable } from 'primereact/datatable';
import Navegacao from '../navegacao';
import { RadioButton } from 'primereact/radiobutton';
import { ApiContext } from '../../context/store';
import { Badge } from 'primereact/badge';
import api from '../../config/api';
import { getToken, getUserId } from '../../config/auth';
import { OverlayPanel } from 'primereact/overlaypanel';
import { Skeleton } from 'primereact/skeleton';

import './navegacao_topo.scss';
import logoBranca from '../../images/Logo Horizontal Branca.png';

import { FaUserAlt, FaMoon } from "react-icons/fa";

function NavegacaoTopo(props) {
    const op = useRef(null);
    const navigate = useNavigate();
    const { ativo, setTemplate } = useContext(ApiContext);

    const themes = [{name: 'Claro', key: 'light'}, {name: 'Escuro', key: 'dark'}];
    const [selectedTheme, setSelectedTheme] = useState(JSON.parse(localStorage.getItem("theme")).title);

    const [visibleLeft, setVisibleLeft] = useState(false);
    const [show, setShow] = useState(false);
    const [notify, setNotify] = useState(false);

    const [time, settime] = useState("00:00")
    var timer = null;

    const handleClose = () => setShow(false);
    const handleShow = () => {
      setShow(true);
    }; 

    const [notificacoes, setNotificacoes] = useState([]);
    const [qtd_nao_lidas, setQtd_nao_lidas] = useState([]);


    useEffect(() => {
        get_notificacoes();
        timer = setInterval(() => {setTempoexpiracao()}, 1000);
        return () => clearInterval(timer);
    }, []);


    function get_notificacoes() {
        setNotify(false);
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {"aplicacao": "SGI", "id_usuario": getUserId()}

        api.post('/core/listar_notificacoes/', data, config)
        .then((res)=>{
            try {
                let x = JSON.parse(res.data);
                //setNotificacoes(x.notificacao);
                let nao_lidas = [];
                let count = 0;
                x.notificacao.forEach(n => { /// extrai a quantidade de nao lidas
                    if(n.lido_em === null){
                        nao_lidas.push(n);
                        count++;
                    }
                });
                setQtd_nao_lidas(count);
                setNotificacoes([...nao_lidas]);
                setNotify(true);

            } catch (error) {
                console.warn("Erro ao buscar as notificações para este usuário");
            }
        })
    }

    function handleLogout() {
        setTemplate(false);
        logout();
        navigate('/');
    }

    function formatahhmmss(s){
        function duas_casas(numero){
          if (numero <= 9){
            numero = "0"+numero;
          }
          return numero;
        }
        let minuto = duas_casas(Math.floor(s/60));
        let segundo = duas_casas((s%3600)%60);
        let formatado = minuto+":"+segundo;
        return formatado;
    }
    
    function setTempoexpiracao(){
        settime(formatahhmmss(Number(sessionStorage.getItem("TEMPO_SESSAO"))));
        sessionStorage.setItem("TEMPO_SESSAO", Number(sessionStorage.getItem("TEMPO_SESSAO"))-1);

        if(Number(sessionStorage.getItem("TEMPO_SESSAO")) < 0){ // se acabar o tempo da sessao 
            if(Number(sessionStorage.getItem("atividade")) > 0){ // se está ativo busca um token novo
                let data = {"refresh": sessionStorage.getItem("REFRESH_TOKEN")}
                api.post('/api/token/refresh/', data).then((res)=>{
                    sessionStorage.setItem("ACCESS_TOKEN", res.data.access);
                })
                sessionStorage.setItem("TEMPO_SESSAO", 3600) // define o novo tempo para a sessao
            }else{ // se está iniativo faz logout
                clearInterval(timer);
                logout();
                navigate('/');
            }
        }
    }

    const items = [
        {
            label:'Delete',
            icon:'pi pi-fw pi-trash'
        },
        {
            separator:true
        },
        {
            label:'Export',
            icon:'pi pi-fw pi-external-link'
        }
    ];

    const NotificacaoSinal = (rowData) => {
        switch (rowData.lido_em) {
            case null:
                return <div className='circulo-notificacao'></div>                                           
            default:
                return <div className='circulo-lido'></div>
        }
    }
    const NotificacaoIcon = (rowData) => {
        return(
            <div className=''>
                <Avatar icon="pi pi-wallet" size="large" shape="circle" style={{color: '#495057', backgroundColor: '#dee2e6'}}/>
            </div>
        )                                    
    }

    function marcar_lido(n) {
        
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI", 
            "id_usuario": getUserId(), 
            "notificacoes": [{"id_notificacao" : n.id_notificacao}] 
        }

        api.post('/core/notificacoes_marcar_lido/', data, config)
        .then((res)=>{
            try {
                let x = JSON.parse(res.data);
                if(x.sucesso === 'S'){
                    get_notificacoes();
                }else{
                    console.warn("Erro ao marcar como lida");
                }
            } catch (error) {
                console.warn("Erro ao marcar como lida");
            }
        })
    }

    const NotificacaoOpcoes = (rowData) => {
    
        return (<>
            <Button  
                icon="pi pi-check-square" 
                className="btn-opcoes" 
                title='Marcar como lido' 
                onClick={()=>marcar_lido(rowData)}
                aria-controls="popup_menu_left" 
                aria-haspopup 
            />
        </>);
    }
    const NotificacaoTemplate = (rowData) => {
        return(
            <div className='template-notoficacao-linha'>
                <p className='template-notoficacao-linha-tempo'>{`${rowData.data_fmtd} - ${rowData.tempo}`}</p>
                <p className='template-notoficacao-linha-titulo'><b>{rowData.titulo}</b></p>
                <p className='template-notoficacao-linha-texto'>{rowData.texto}</p>
            </div>
        )                                    
    }

    return ( 
      <>
        <div className="navegacao-topo">
            <div className='nav-topo-logo' onClick={()=>navigate('/inicio')}>
                <img className='logo-branca' src={logoBranca} draggable={false} alt="logo dpeam"/>
            </div>
            <Button id="btn-menu" className="menu-topo icone-topo-botoes" icon="pi pi-bars" onClick={(e) => setVisibleLeft(true)}/>
            
            <Sidebar className="sidebar" visible={visibleLeft} onHide={() => setVisibleLeft(false)}>
                <Navegacao/>
            </Sidebar>
            <p className='titulo-nav no-select'>Sistema de Gerenciamento Integrado</p>
            <div className='nav-topo-botoes'>
                <i className="pi pi-bell icone-topo-botoes p-overlay-badge" id='icone-notificacoes' onClick={(e) => op.current.toggle(e)} >
                    {
                        qtd_nao_lidas > 0?
                        <Badge value={qtd_nao_lidas < 9 ? qtd_nao_lidas : '9+'} severity="danger" className="badge-notificacao" ></Badge>
                        :null
                    }
                </i>
                <Button onClick={handleShow} icon="pi pi-user" className="icone-topo-botoes p-button-rounded p-button-outlined" aria-label="User" style={{color:'#ffffff96'}} />
                {/* CAIXA DE NOTIFICACAO */}
                <OverlayPanel 
                    ref={op}
                    id="overlay_panel"
                    className="overlaypanel-demo"
                    dismissable
                >
                    <div id='topo-panel-notificacoes'>
                        <h5>Notificações</h5>
                        <div>
                            <Button 
                                icon="pi pi-external-link"
                                className="btn-darkblue"
                                title='Minhas notificações'
                                onClick={()=> navigate('/notificacoes') }
                            />
                            <Button 
                                icon="pi pi-refresh"
                                className="btn-green"
                                title='Atualizar'
                                onClick={()=>get_notificacoes()}
                            />
                            
                        </div>
                    </div>

                    {
                        notify?

                        <DataTable
                            value={notificacoes}
                            selectionMode="single"
                            scrollable
                            scrollHeight="45vh"
                            emptyMessage="Sem novas notificações"
                        >
                            <Column body={NotificacaoSinal} style={{ flexGrow: 0, flexBasis: '1%', padding: 0 }}/>
                            <Column body={NotificacaoIcon} style={{ flexGrow: 0, flexBasis: '12%' }}/>
                            <Column body={NotificacaoTemplate}/>
                            <Column body={NotificacaoOpcoes} style={{ flexGrow: 0, flexBasis: '5%' }}/>
                        </DataTable>

                        :
                        <>
                        <div className="p-selectable-row" style={{margin: '16px 15px 0 20px',display: 'flex', justifyContent: 'space-between'}}>
                            <div  style={{display: 'flex'}}>
                                <Skeleton shape="circle" size="3rem" style={{alignSelf: 'center'}}></Skeleton>
                                <div style={{marginLeft:'10px'}}>
                                    <Skeleton className="mb-1" width="7rem"  height="10px"></Skeleton>
                                    <Skeleton className="mb-1" width="5rem" height="15px"></Skeleton>
                                    <Skeleton className="mb-1" width="13rem" height="15px"></Skeleton>
                                </div>
                            </div>
                            <div style={{alignSelf: 'center'}}>
                                <Skeleton size="1rem" borderRadius="3px"></Skeleton>
                            </div>
                        </div>
                        <hr style={{ color: '#8b8e90'}}/>

                        <div className="p-selectable-row" style={{margin: '16px 15px 0 20px',display: 'flex', justifyContent: 'space-between'}}>
                            <div  style={{display: 'flex'}}>
                                <Skeleton shape="circle" size="3rem" style={{alignSelf: 'center'}}></Skeleton>
                                <div style={{marginLeft:'10px'}}>
                                    <Skeleton className="mb-1" width="7rem"  height="10px"></Skeleton>
                                    <Skeleton className="mb-1" width="5rem" height="15px"></Skeleton>
                                    <Skeleton className="mb-1" width="13rem" height="15px"></Skeleton>
                                </div>
                            </div>
                            <div style={{alignSelf: 'center'}}>
                                <Skeleton size="1rem" borderRadius="3px"></Skeleton>
                            </div>
                        </div>
                        <hr style={{ color: '#8b8e90'}}/>

                        <div className="p-selectable-row" style={{margin: '16px 15px 0 20px',display: 'flex', justifyContent: 'space-between'}}>
                            <div  style={{display: 'flex'}}>
                                <Skeleton shape="circle" size="3rem" style={{alignSelf: 'center'}}></Skeleton>
                                <div style={{marginLeft:'10px'}}>
                                    <Skeleton className="mb-1" width="7rem"  height="10px"></Skeleton>
                                    <Skeleton className="mb-1" width="5rem" height="15px"></Skeleton>
                                    <Skeleton className="mb-1" width="13rem" height="15px"></Skeleton>
                                </div>
                            </div>
                            <div style={{alignSelf: 'center'}}>
                                <Skeleton size="1rem" borderRadius="3px"></Skeleton>
                            </div>
                        </div>
                        </>
                    }
 
                </OverlayPanel>
            </div>
        </div>

        <Offcanvas show={show} onHide={handleClose} placement="end">
          <Offcanvas.Header closeButton>
            <Offcanvas.Title>
              <div className="d-flex">
                <Avatar icon="pi pi-user" className="m-2" size="large" style={{ backgroundColor: '#104856', borderRadius: '50px', color: '#DADADA' }} />
                <div className="pt-1 name-user" >
                    {sessionStorage.getItem("nome")}
                </div>
              </div>
            </Offcanvas.Title>
          </Offcanvas.Header>
          <Offcanvas.Body style={{display: 'flex', flexDirection: 'column', alignItems: 'flex-start'}}>
            <p style={{margin: '0'}}>Temas</p>
            <div className="seletor-de-temas">
            {
            themes.map((theme) => {
                return (
                    <div key={theme.key} className="field-radiobutton">
                        <RadioButton
                            inputId={theme.key}
                            name="theme"
                            value={theme}
                            onChange={(e)=>{
                                props.switchTheme(e);
                                setSelectedTheme(e.value.key);
                            }}
                            checked={selectedTheme === theme.key}
                            style={{marginTop: "5px"}}
                        />
                        <label htmlFor={theme.key} style={{marginLeft: "10px"}}>{theme.name}</label>
                    </div>
                )
            })
            }
            </div>
            <div className='botao-sair-sistema'>
            <Button label="Sair" onClick={handleLogout} icon="pi pi-sign-out" iconPos="left" className="p-button-secondary p-button-text" />
            </div>
            <label className="versao no-select">
                <small>
                    SGI {sessionStorage.getItem("versao")} por <strong>DTI-DPEAM</strong>
                </small>
                <small className='visualizador-sessao'>
                    {ativo?<FaUserAlt size={10} style={{marginBottom : "3px"}}/>:<FaMoon size={10} style={{marginBottom : "3px"}}/>} {time}
                </small>
            </label>
          </Offcanvas.Body>
        </Offcanvas>


      </>
    );
}

export default NavegacaoTopo;