import React, {useState, useEffect} from "react";
import { FilterMatchMode } from 'primereact/api';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';

function ListarGruposUsuario(props) {
    const [grupos, setGrupos] = useState([]);
    
    const [filters] = useState({
        'descricao_grupo': { value: null, matchMode: FilterMatchMode.CONTAINS },
    });

    useEffect(() => {
        setGrupos(props.grupo);
    }, [props.grupo]);

    const irBodyTemplate = (rowData) => {
        return(
            <span>
            {/* <Button 
                onClick={()=>props.seleciona(rowData)}
                icon="pi pi-pencil"
                className="btn-green"
                title="Editar Grupo/Permissão"
            /> */}
            <Button
                icon="pi pi-trash"
                className="btn-red"
                onClick={()=>props.excluir(rowData)}
                style={{marginLeft:'5px'}} 
            />
            </span>
        )                                    
    }

    return (
        <div style={{width: "100%", marginTop: "10px"}}>
        <DataTable
            value={props.grupo}
            size="small"
            dataKey="id"
            filters={filters}
            filterDisplay="row"
            scrollable
            scrollHeight="55vh"
            emptyMessage="Nada encontrado"
            selectionMode="single"
            showFilterMenu={false}
        >
            <Column
                field="descricao_grupo" 
                header="Descrição"
                filter 
                filterPlaceholder="Buscar" 
                style={{ flexGrow: 0, flexBasis: '85%' }}
                showFilterMenu={false}
            />
            <Column 
                field="botao" 
                header="Ação"
                body={irBodyTemplate} 
                className="col-centralizado" 
                style={{ flexGrow: 0, flexBasis: '15%' }}
            />
        </DataTable>
        </div>
    );
}

export default ListarGruposUsuario;