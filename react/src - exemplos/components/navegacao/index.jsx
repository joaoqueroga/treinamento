import React, {useState, useEffect} from 'react';
import './navegacao.scss';
import { PanelMenu } from 'primereact/panelmenu';
import {useNavigate} from 'react-router-dom';
import Alertas from '../../utils/alertas';

function Navegacao() {
    const navigate = useNavigate();
    const [navegacao, setNavegacao] = useState([]);
    const [carregando, setCarregando] = useState(true);

    useEffect(() => { //decripta o grupo para ativar as permissoes
        let permissoes = JSON.parse(sessionStorage.getItem("menu"));
        montaMenu(permissoes);
    }, []);


    function montaMenu(json_menus) {
        let menus = [];
        try {
            json_menus.map((m)=>{
                if(m.eh_menu){ // monta menus que nao são funcionalidades
                    let obj = {
                        label: m.rotulo,
                        icon: `pi pi-fw ${m.icone}`,
                        command: m.rota !== '' && m.rota !== null ? ()=>{ navigate(m.rota) } : null,
                        items: m.menus_1.length > 0 ? montaSubmenus_1(m.menus_1) : null
                        // items: m.menus_1 ? montaSubmenus_1(m.menus_1) : null se vir null
                    }
                    menus.push(obj);
                }
            })
        } catch (error) {
            Alertas.erro(error);
        }
        setNavegacao(menus);
        setCarregando(false); //para mostrar apos carregar da procedure (a fezer)
    }

    function montaSubmenus_1(submenus) {
        let menus = [];
        try {
            submenus.map((m)=>{
                if(m.eh_menu){
                    let obj = {
                        label: m.rotulo,
                        icon: `pi pi-fw ${m.icone}`,
                        command: m.rota !== '' && m.rota !== null ? ()=>{ navigate(m.rota) } : null,
                        items: m.menus_2.length > 0 ? montaSubmenus_2(m.menus_2) : null
                        // items: m.menus_2 ? montaSubmenus_2(m.menus_2) : null
                    }
                    menus.push(obj);
                }
            })
        } catch (error) {
            Alertas.erro(error);
        }
        return menus;
    }

    function montaSubmenus_2(submenus) {
        let menus = [];
        try {
            submenus.map((m)=>{
                if(m.eh_menu){
                    let obj = {
                        label: m.rotulo,
                        icon: `pi pi-fw ${m.icone}`,
                        command: m.rota !== '' && m.rota !== null ? ()=>{ navigate(m.rota) } : null,
                    }
                    menus.push(obj);
                }
            })
        } catch (error) {
            Alertas.erro(error);
        }
        return menus;
    }

    return (     
        <div>
            {
                carregando
                ?
                <p>Carregando...</p>
                :
                <PanelMenu model={navegacao}/>        
            }
        </div>
    );
}

export default Navegacao;