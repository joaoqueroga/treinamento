import React, { useState, useEffect } from "react";
import { FilterMatchMode } from 'primereact/api';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';

function ListarAnotacoesEstagio(props) {
    const [anotacoes, setAnotacoes] = useState([]);

    const [filters] = useState({
        'descricao_tipo_anotacao': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'descricao': { value: null, matchMode: FilterMatchMode.CONTAINS },
    });

    useEffect(() => {
        setAnotacoes(props.anotacoes);
    }, [props.anotacoes]);

    const irBodyTemplate = (rowData) => {
        return (
            <span>
                <Button
                    onClick={() => props.seleciona(rowData)}
                    icon="pi pi-pencil"
                    className="btn-green"
                    title="Editar Anotação"
                    disabled={!props.ativos}
                />
                <Button
                    onClick={() => props.excluir(rowData)}
                    icon="pi pi-trash"
                    className="btn-red"
                    title="Excluir"
                    disabled={!props.ativos}
                />
            </span>
        )
    }

    return (
        <div style={{ width: "100%", marginTop: "10px" }}>
            <DataTable
                value={anotacoes}
                size="small"
                dataKey="id"
                filters={filters}
                filterDisplay="row"
                scrollable
                scrollHeight="55vh"
                emptyMessage="Nada encontrado"
                selectionMode="single"
                showFilterMenu={false}
            >
                <Column
                    field="descricao_tipo_anotacao"
                    header="Tipo"
                    filter
                    filterPlaceholder="Buscar"
                    style={{ flexGrow: 0, flexBasis: '20%' }}
                    showFilterMenu={false}
                />
                <Column
                    field="descricao"
                    header="Texto"
                    filter
                    filterPlaceholder="Buscar"
                    style={{ flexGrow: 0, flexBasis: '70%' }}
                    showFilterMenu={false}
                />
                <Column
                    field="botao"
                    header="Ação"
                    body={irBodyTemplate}
                    className="col-centralizado"
                    style={{ flexGrow: 0, flexBasis: '10%' }}
                />
            </DataTable>
        </div>
    );
}

export default ListarAnotacoesEstagio;