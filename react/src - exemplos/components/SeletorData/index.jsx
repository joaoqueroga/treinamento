import React, {useEffect, useState} from "react";
import { Calendar } from 'primereact/calendar';
import { addLocale } from 'primereact/api';

function SeletorData(props) {

    const [data, setData] = useState(null);

    useEffect(() => {
        if(props.get){
            let d = (props.get.substring(0,2));
            let m = Number(props.get.substring(3,5))-1;
            let a = (props.get.substring(6,10));
            let newdate = new Date(a,m,d)
            setData(newdate);
        }
    }, []);

    addLocale('pt', {
        firstDayOfWeek: 0,
        dayNames: ['domingo', 'segunda', 'terça', 'quarta', 'quinta', 'sexta', 'sábado'],
        dayNamesShort: ['dom', 'seg', 'ter', 'qua', 'qui', 'sex', 'sáb'],
        dayNamesMin: ['DOM', 'SEG', 'TER', 'QUA', 'QUI', 'SEX', 'SÁB'],
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        monthNamesShort: ['jan', 'fev', 'mar', 'abr', 'mai', 'jun', 'jul', 'ago', 'set', 'out', 'nov', 'dez'],
        today: 'Hoje',
        clear: 'Limpar'
    });

    function defineData(d) {
        if(d){
            let dia = d.getDate()<10?`0${d.getDate()}`:d.getDate();
            let mes = (d.getMonth()+1)<10?`0${(d.getMonth()+1)}`:(d.getMonth()+1);
            let ano = d.getFullYear();
            props.set(`${dia}/${mes}/${ano}`);
            setData(d);
        }
    }

    return (
        <Calendar 
            id="date" 
            name="date"
            value={data} 
            onChange={(e)=>defineData(e.value)}
            dateFormat="dd/mm/yy" 
            mask="99/99/9999"
            locale="pt"
            showButtonBar
            style={{height: "40px", width:"100%"}}
            showIcon
            onClearButtonClick={()=>props.set(null)}
        />
    );
}

export default SeletorData;