import React from "react";
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';

function ListarContratosEstagio(props) {

    const irBodyTemplate = (rowData) => {
        return(
            <span>
                <Button 
                    onClick={()=>props.seleciona(rowData)}
                    icon="pi pi-pencil"
                    className="btn-green"
                    title="Editar Contrato"
                    disabled={!props.ativos}
                />

                <Button 
                    onClick={()=>props.excluir(rowData)}
                    icon="pi pi-trash"
                    className="btn-red"
                    title="Excluir"
                    disabled={!props.ativos}
                />
            </span>
        )                                    
    }

    const irFileTemplate = (rowData) => {
        return(
            <span>
                {
                    rowData.documento?
                        <Button 
                            onClick={()=>props.baixarPdf(rowData.documento)}
                            icon="pi pi-file-pdf"
                            className="btn-red"
                            title="Arquivo"
                        />
                    :
                        <span></span>
                }
                
            </span>
        )                                    
    }

    return (
        <div style={{width: "100%", marginTop: "10px"}}>
        <DataTable
            value={props.contratos}
            size="small"
            dataKey="id"
            scrollable
            scrollHeight="55vh"
            emptyMessage="Nada encontrado"
            selectionMode="single"
        >
            <Column 
                field="id_contrato"
                header="Id"
                style={{ flexGrow: 0, flexBasis: '10%' }}
            />
            <Column 
                field="tipo"
                header="Tipo"
                style={{ flexGrow: 0, flexBasis: '20%' }}
            />
            <Column
                field="data_inicio" 
                header="Início" 
                style={{ flexGrow: 0, flexBasis: '20%' }}
            />
            <Column
                field="data_fim" 
                header="Fim"
                style={{ flexGrow: 0, flexBasis: '20%' }}
            />
            <Column
                field="id_contrato_aditivo" 
                header="Aditivo do"
                style={{ flexGrow: 0, flexBasis: '20%' }}
            />
            <Column
                field="data_fim" 
                header="Arquivo"
                body={irFileTemplate} 
                style={{ flexGrow: 0, flexBasis: '10%' }}
            />
            <Column 
                field="botao" 
                header="Ação"
                body={irBodyTemplate} 
                className="col-centralizado" 
                style={{ flexGrow: 0, flexBasis: '10%' }}
            />
        </DataTable>
        </div>
    );
}

export default ListarContratosEstagio;