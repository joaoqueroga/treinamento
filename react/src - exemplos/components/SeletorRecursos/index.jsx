import React, {useState, useEffect} from "react";
import { FilterMatchMode } from 'primereact/api';
import { DataTable } from 'primereact/datatable';
import { InputText } from 'primereact/inputtext';
import { Dialog } from 'primereact/dialog';
import Swal from 'sweetalert2';
import { Column } from 'primereact/column';
import api from "../../config/api";
import { getToken } from "../../config/auth";
import TelaSpinnerComponente from "../spinner_componente";

function SeletorRecursos(props) {

    const [recursos, setRecursos] = useState([]);
    const [display, setDisplay] = useState(false);

    const [erro, setErro] = useState(false);
    const [carregando, setCarregando] =  useState(true);

    const [filters] = useState({
        'descricao': { value: null, matchMode: FilterMatchMode.CONTAINS }
    });

    function seleciona(obj) {
        props.set(obj);
        setDisplay(false);
    }

    useEffect(() => {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data_usuario = {"id_usuario":1}
        api.post('/painel_gerenciamento/listar_recursos/', data_usuario, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(Array.isArray(x)){
                setRecursos(x);
                setCarregando(false);
            }else{
                Swal.fire({
                    icon:'error',
                    title:"Erro",
                    text: `${x.motivo}`,
                    confirmButtonText: 'fechar',
                })
            }
        }).catch((err)=>{
            Swal.fire({
                icon:'error',
                title:"Erro",
                text: `${err}`,
                confirmButtonText: 'fechar',
            })
        })
        
    }, []);

    return (
        <div>
            <div className="p-inputgroup">
                <InputText 
                    type="text" 
                    value={props.get?props.get.descricao:""}
                    onClick={()=>setDisplay(true)}
                />
                <span className="p-inputgroup-addon">
                    <i className="pi pi-map-marker"></i>
                </span>
            </div>
            <Dialog 
            header="Permissões/Recursos"
            visible={display} 
            style={{ width: '50%', height:'100%'}}
            onHide={() => setDisplay(false)}
            >
                <div className='card-selecionar-chefe'>
                    {
                    carregando?<TelaSpinnerComponente texto="Buscando Recursos" />:
                    <DataTable
                        value={recursos}
                        size="small"
                        className="p-datatable-customers tabela-servidores"
                        dataKey="id"
                        filters={filters}
                        filterDisplay="row"
                        emptyMessage={carregando?"...":"Nada encontrado"}
                        scrollable
                        scrollHeight="80vh"
                        selectionMode="single"
                        onSelectionChange={e => seleciona(e.value)}
                    >
                        <Column
                            header="Nome"
                            field="descricao"
                            filter 
                            filterPlaceholder="Buscar" 
                            style={{ flexGrow: 0, flexBasis: '100%' }}
                            showFilterMenu={false}
                        />
                    </DataTable>
                    }
                </div>
            </Dialog>
        </div>
    );
}

export default SeletorRecursos;