import React from "react";

import { ProgressSpinner } from 'primereact/progressspinner';
import './style.scss';

function TelaSpinnerComponente(props) {
    return ( 
        <div className="div-tela-spinner-componente">
            {
                <ProgressSpinner size={props.tamanho} />
            }
            <p>{props.texto}</p>
        </div>
    );
}

export default TelaSpinnerComponente;