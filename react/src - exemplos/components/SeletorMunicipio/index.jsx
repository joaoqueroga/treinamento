import React, {useState, useEffect} from "react";
import { AutoComplete } from 'primereact/autocomplete';

import ms from '../../jsons/municipios.json';

function SeletorMunicipio(props) {
    const [municipios, setmunicipios] = useState([]);
    const [filtroMunicipio, setfiltroMunicipio] = useState(null);
    
    useEffect(() => {
        setmunicipios(ms);
    }, []);

    //filtro select municipios
    const buscaMunicipio = (event) => {
        setTimeout(() => {
            let _filtroMunicipio;
            if (!event.query.trim().length) {
                _filtroMunicipio = [...municipios];
            }
            else {
                _filtroMunicipio = municipios.filter((mun) => {
                    return mun.nome.toLowerCase().startsWith(event.query.toLowerCase());
                });
            }
            setfiltroMunicipio(_filtroMunicipio);
        }, 250);
    }

    const itemTemplate = (item) => { //municipio
        return (
            <div className="country-item">
                <div>{item.nome} {item.microrregiao.mesorregiao.UF.sigla}</div>
            </div>
        );
    }

    const naturalidadeChange = (e) => {
        try {
            if(e.value.nome && props.setUf){
                props.setUf(e.value.microrregiao.mesorregiao.UF.sigla);
            }
        } catch (error) {
            console.warn("exceção tratada na seleção de município");
        }
        props.set(e.value)
    }

    return ( 
        <div>
            <AutoComplete 
                value={ props.get} 
                suggestions={filtroMunicipio} 
                completeMethod={buscaMunicipio} 
                field="nome" 
                dropdown 
                forceSelection 
                itemTemplate={itemTemplate} 
                onChange={naturalidadeChange} 
                aria-label="municipios" 
                className="p-inputtext-sm input-select-1"
                style={{height: "40px", width:"100%"}}
            />
        </div> 
    );
}

export default SeletorMunicipio;