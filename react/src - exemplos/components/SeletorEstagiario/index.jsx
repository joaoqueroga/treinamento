import React, {useState, useEffect} from "react";
import { FilterMatchMode } from 'primereact/api';
import { DataTable } from 'primereact/datatable';
import { InputText } from 'primereact/inputtext';
import { Dialog } from 'primereact/dialog';

import { Column } from 'primereact/column';
import api from "../../config/api";
import { getToken, getUserId } from "../../config/auth";
import TelaSpinnerComponente from "../spinner_componente";

function SeletorEstagiario(props) {

    const [servidores, setServidores] = useState([]);
    const [chefe_display, setChefe_display] = useState(false);
    const [carregando, setCarregando] =  useState(true);
    const [erro, setErro] = useState(false);

    const [filters] = useState({
        'nome': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'matricula': { value: null, matchMode: FilterMatchMode.CONTAINS },
    });

    function selecionaChefe(obj) {
        props.set(obj);
        setChefe_display(false);

        if(props.funcao){ //execulta uma função com base nos dados do servidor selecionado
            let data = {
                "aplicacao": "SGI",
                "id_usuario": getUserId(),
                "id_funcionario": Number(obj.id_funcionario),
                "tipo_listagem": "COMPLETA"
            }
            let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
            api.post('/rh/servidor/',data, config)
            .then((res)=>{
                let x = JSON.parse(res.data);
                if(x.sucesso === "S"){
                    props.funcao(x.funcionarios[0]);
                }
            })
        }
    }

    useEffect(() => {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {"aplicacao": "SGI", "id_usuario": getUserId() , "tipo_listagem": "PARCIAL", "eh_estagiario": true}
        api.post('/rh/servidores/', data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === 'S'){
                setServidores(x.funcionarios);
                setCarregando(false);
            }else{
               setErro(true);
            }
        })
    }, []);

    return (
        <div>
            <div className="p-inputgroup">
                <InputText 
                    type="text" 
                    value={props.get?props.get.nome:""}
                    onClick={()=>setChefe_display(true)}
                    disabled={props.chefe?true:false}
                />
                <span className="p-inputgroup-addon">
                    <i className="pi pi-user"></i>
                </span>
            </div>
            <Dialog 
            header="Servidores"
            visible={chefe_display} 
            style={{ width: '50%', height:'100%'}}
            onHide={() => setChefe_display(false)}
            >
                <div className='card-selecionar-chefe'>
                    <DataTable
                        value={servidores}
                        size="small"
                        className="p-datatable-customers tabela-servidores"
                        dataKey="id"
                        filters={filters}
                        filterDisplay="row"
                        emptyMessage={carregando?"...":"Nada encontrado"}
                        scrollable
                        scrollHeight="80vh"
                        selectionMode="single"
                        onSelectionChange={e => selecionaChefe(e.value)}
                    >
                        <Column 
                            field="matricula"
                            header="Matrícula"
                            filter
                            filterPlaceholder="Buscar"
                            style={{ flexGrow: 0, flexBasis: '30%' }}
                            showFilterMenu={false}
                        />
                        <Column
                            field="nome" 
                            header="Nome" 
                            filter 
                            filterPlaceholder="Buscar" 
                            style={{ flexGrow: 0, flexBasis: '70%' }}
                            showFilterMenu={false}
                        />
                    </DataTable>
                    {
                        carregando?<TelaSpinnerComponente texto="Buscando Servidores" />:null
                    }
                    {
                        erro?<p><small className="p-error">Erro ao carregar os dados</small> </p>:null
                    }
                </div>
            </Dialog>
        </div>
    );
}

export default SeletorEstagiario;