import React, {useState, useEffect} from "react";
import { FilterMatchMode } from 'primereact/api';
import { DataTable } from 'primereact/datatable';
import { InputText } from 'primereact/inputtext';
import { Dialog } from 'primereact/dialog';

import { Column } from 'primereact/column';
import api from "../../config/api";
import { getToken } from "../../config/auth";
import TelaSpinnerComponente from "../spinner_componente";

function SeletorDefensor(props) {

    const [defensores, setDefensores] = useState([]);
    const [defensorSelecionado,setDefensorSelecionado] = useState(props.get);
    const [mostrarDialogBuscador, setMostrarDialogBuscador] = useState(false);
    const [carregando, setCarregando] =  useState(true);
    const [erro, setErro] = useState(false);

    const [filters] = useState({
        'nome': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'matricula': { value: null, matchMode: FilterMatchMode.CONTAINS },
    });

    function selecionaDefensor(defensor) {
        setDefensorSelecionado(defensor);
        setMostrarDialogBuscador(false);   
        props.set(defensor);     
    }

    useEffect(() => {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {"eh_defensor":true,"tipo_listagem": "PARCIAL"}
        api.post('/rh/defensores/', data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.sucesso === 'S'){
                console.log("defensores: ",x.funcionarios);
                setDefensores(x.funcionarios);
                setCarregando(false);
            }else{
               setErro(true);
            }
        })
    }, []);

    return (
        <div>
            <div className="p-inputgroup">
                <InputText 
                    type="text" 
                    disabled={props.disabled?props.disabled:false}
                    value={defensorSelecionado?defensorSelecionado.nome:""}
                    onClick={()=>setMostrarDialogBuscador(true)}
                />
                <span className="p-inputgroup-addon">
                    <i className="pi pi-user"></i>
                </span>
            </div>
            <Dialog 
                header="Defensores"
                visible={mostrarDialogBuscador} 
                style={{ width: '50%', height:'100%'}}
                onHide={() => setMostrarDialogBuscador(false)}
                >
                <div className='card-selecionar-chefe'>
                    <DataTable
                        value={defensores}
                        size="small"
                        dataKey="id"
                        filters={filters}
                        filterDisplay="row"
                        emptyMessage={carregando?"...":"Nada encontrado"}
                        scrollable
                        scrollHeight="80vh"
                        selectionMode="single"
                        onSelectionChange={e => selecionaDefensor(e.value)}
                    >
                        <Column 
                            field="matricula"
                            header="Matrícula"
                            filter
                            filterPlaceholder="Buscar"
                            style={{ flexGrow: 0, flexBasis: '30%' }}
                            showFilterMenu={false}
                        />
                        <Column
                            field="nome" 
                            header="Nome" 
                            filter 
                            filterPlaceholder="Buscar" 
                            style={{ flexGrow: 0, flexBasis: '70%' }}
                            showFilterMenu={false}
                        />
                    </DataTable>
                    {
                        carregando?<TelaSpinnerComponente texto="Buscando Defensores" />:null
                    }
                    {
                        erro?<p><small className="p-error">Erro ao carregar os dados</small> </p>:null
                    }
                </div>
            </Dialog>
        </div>
    );
}

export default SeletorDefensor;