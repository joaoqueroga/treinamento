import React, {useState, useEffect} from "react";
import { FilterMatchMode } from 'primereact/api';
import { DataTable } from 'primereact/datatable';
import { InputText } from 'primereact/inputtext';
import { Dialog } from 'primereact/dialog';
import { Column } from 'primereact/column';
import api from "../../config/api";
import { getToken, getUserId } from "../../config/auth";
import TelaSpinnerComponente from "../spinner_componente";

function SeletorTipoEstagio(props) {

    const [dados, setDados] = useState([]);
    const [display, setDisplay] = useState(false);
    const [erro, setErro] = useState(false);
    const [carregando, setCarregando] =  useState(true);

    const [filters] = useState({
        'valor_bolsa': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'tipo_estagio_descricao': { value: null, matchMode: FilterMatchMode.CONTAINS }
    });

    function seleciona(obj) {
        props.set(obj);
        if(props.lotacao_chefe){ // se manda a props que define o chefe o resp da lotacao
            props.lotacao_chefe(obj)
        }
        setDisplay(false);
    }

    useEffect(() => {
        let config = {headers: {"Authorization": `Bearer ${getToken()}`}}
        let data = {
            "aplicacao": "SGI",
            "id_usuario": getUserId()
        }
        api.post('/estagio/tipos_estagiario/',data, config)
        .then((res)=>{
            let x = JSON.parse(res.data);
            if(x.length > 0){
                setDados(x);
                setCarregando(false);
            }else{
                setCarregando(false);
                setErro(true);
            }
        })
    }, []);

    return (
        <div>
            <div className="p-inputgroup">
                <InputText 
                    type="text" 
                    value={props.get?props.get.tipo_estagio_descricao:""}
                    onClick={()=>setDisplay(true)}
                />
                <span className="p-inputgroup-addon">
                    <i className="pi pi-briefcase"></i>
                </span>
            </div>

            <Dialog 
            header="Tipos de Estágio"
            visible={display} 
            style={{ width: '50%', height:'100%'}}
            onHide={() => setDisplay(false)}
            >
                <div className='card-selecionar-chefe'>
                    <DataTable
                        value={dados}
                        size="small"
                        className="p-datatable-customers tabela-servidores"
                        dataKey="id"
                        filters={filters}
                        filterDisplay="row"
                        emptyMessage={carregando?"...":"Nada encontrado"}
                        scrollable
                        scrollHeight="80vh"
                        selectionMode="single"
                        onSelectionChange={e => seleciona(e.value)}
                    >
                        <Column
                            header="Descrição"
                            field="tipo_estagio_descricao"
                            filter 
                            filterPlaceholder="Buscar" 
                            style={{ flexGrow: 0, flexBasis: '70%' }}
                            showFilterMenu={false}
                        />
                        <Column
                            header="Valor Bolsa"
                            field="valor_bolsa"
                            filter 
                            filterPlaceholder="Buscar" 
                            style={{ flexGrow: 0, flexBasis: '30%' }}
                            showFilterMenu={false}
                        />
                    </DataTable>
                    {
                        carregando?<TelaSpinnerComponente texto="Buscando Lotações" />:null
                    }
                    {
                        erro?<p><small className="p-error">Erro ao carregar os dados</small> </p>:null
                    }
                </div>
            </Dialog>
        </div>
    );
}

export default SeletorTipoEstagio;