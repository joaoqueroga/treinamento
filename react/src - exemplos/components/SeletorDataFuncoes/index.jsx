import React, {useEffect, useState} from "react";
import { Calendar } from 'primereact/calendar';
import { addLocale } from 'primereact/api';

function SeletorDataFuncoes(props) {

    const [data, setData] = useState(null);

    addLocale('pt', {
        firstDayOfWeek: 0,
        dayNames: ['domingo', 'segunda', 'terça', 'quarta', 'quinta', 'sexta', 'sábado'],
        dayNamesShort: ['dom', 'seg', 'ter', 'qua', 'qui', 'sex', 'sáb'],
        dayNamesMin: ['DOM', 'SEG', 'TER', 'QUA', 'QUI', 'SEX', 'SÁB'],
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        monthNamesShort: ['jan', 'fev', 'mar', 'abr', 'mai', 'jun', 'jul', 'ago', 'set', 'out', 'nov', 'dez'],
        today: 'Hoje',
        clear: 'Limpar'
    });

    function defineData(d) {
        if(d){
            let dia = d.getDate()<10?`0${d.getDate()}`:d.getDate();
            let mes = (d.getMonth()+1)<10?`0${(d.getMonth()+1)}`:(d.getMonth()+1);
            let ano = d.getFullYear();
            props.seleciona(`${dia}/${mes}/${ano}`, props.index);
            setData(d);
        }
    }

    return (
        <Calendar 
            id="date" 
            name="date"
            value={data} 
            onChange={(e)=>defineData(e.value)}
            dateFormat="dd/mm/yy" 
            mask="99/99/9999"
            locale="pt"
            showButtonBar
            style={{height: "40px", width:"100%"}}
            showIcon
            onClearButtonClick={()=>props.seleciona(null, props.index)}
        />
    );
}

export default SeletorDataFuncoes;