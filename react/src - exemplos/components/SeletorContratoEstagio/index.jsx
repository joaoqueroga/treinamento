import React, {useState} from "react";
import { DataTable } from 'primereact/datatable';
import { InputText } from 'primereact/inputtext';
import { Dialog } from 'primereact/dialog';
import { Column } from 'primereact/column';
import TelaSpinnerComponente from "../spinner_componente";

function SeletorContratoEstagio(props) {

    const [dados] = useState(props.contratos);

    const [display, setDisplay] = useState(false);
    const [erro, setErro] = useState(false);
    const [carregando, setCarregando] =  useState(false);

    function seleciona(obj) {
        props.set(obj);
        setDisplay(false);
    }

    return (
        <div>
            <div className="p-inputgroup">
                <InputText 
                    type="text" 
                    value={props.get?props.get.id_contrato:""}
                    onClick={()=>setDisplay(true)}
                />
                <span className="p-inputgroup-addon">
                    <i className="pi pi-briefcase"></i>
                </span>
            </div>

            <Dialog 
            header="Contratos"
            visible={display} 
            style={{ width: '50%', height:'100%'}}
            onHide={() => setDisplay(false)}
            >
                <div className='card-selecionar-chefe'>
                    <DataTable
                        value={dados}
                        size="small"
                        className="p-datatable-customers tabela-servidores"
                        dataKey="id"
                        emptyMessage={carregando?"...":"Nada encontrado"}
                        scrollable
                        scrollHeight="80vh"
                        selectionMode="single"
                        onSelectionChange={e => seleciona(e.value)}
                    >
                        <Column
                            header="Id"
                            field="id_contrato"
                            style={{ flexGrow: 0, flexBasis: '10%' }}
                        />
                        <Column
                            header="Tipo"
                            field="tipo"
                            style={{ flexGrow: 0, flexBasis: '45%' }}
                        />
                        <Column
                            header="Início"
                            field="data_inicio"
                            style={{ flexGrow: 0, flexBasis: '45%' }}
                        />
                    </DataTable>
                    {
                        carregando?<TelaSpinnerComponente texto="Buscando..." />:null
                    }
                    {
                        erro?<p><small className="p-error">Erro ao carregar os dados</small> </p>:null
                    }
                </div>
            </Dialog>
        </div>
    );
}

export default SeletorContratoEstagio;