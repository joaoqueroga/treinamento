import React, { useState, useEffect } from "react";
import { FilterMatchMode } from 'primereact/api';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Button } from 'primereact/button';



function ListaDocumentosDiversos(props) {
    const [documento_diverso, setDocumentosdiversos] = useState([]);


    const [filters] = useState({
        'numero': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'data': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'tipo_documento_diversos_descricao': { value: null, matchMode: FilterMatchMode.CONTAINS },
        'descricao': { value: null, matchMode: FilterMatchMode.CONTAINS },
    });

    useEffect(() => {
        setDocumentosdiversos(props.documentos_diversos);
    }, [props.documentos_diversos]);

    const irBodyTemplate = (rowData) => {
        return (
            <span>
                <Button
                    onClick={() => props.seleciona(rowData)}
                    icon="pi pi-pencil"
                    className="btn-green"
                    title="Editar Anotação"
                    disabled={!props.ativos}
                />

                <Button
                    onClick={() => props.excluir(rowData)}
                    icon="pi pi-trash"
                    className="btn-red"
                    title="Excluir"
                    disabled={!props.ativos}
                />
            </span>
        )
    }

    return (
        <div style={{ width: "100%", marginTop: "10px" }}>
            <DataTable
                value={documento_diverso}
                size="small"
                dataKey="id"
                filters={filters}
                filterDisplay="row"
                scrollable
                scrollHeight="60vh"
                emptyMessage="Nada encontrado"
                selectionMode="single"
            >
                <Column
                    field="numero"
                    header="Número"
                    filter
                    filterPlaceholder="Buscar"
                    style={{ flexGrow: 0, flexBasis: '15%' }}
                    showFilterMenu={false}
                />
                <Column
                    field="tipo_documento_diversos_descricao"
                    header="Tipo"
                    filter
                    filterPlaceholder="Buscar"
                    style={{ flexGrow: 0, flexBasis: '15%' }}
                    showFilterMenu={false}
                />
                <Column
                    field="data"
                    header="Data"
                    filter
                    filterPlaceholder="Buscar"
                    style={{ flexGrow: 0, flexBasis: '15%' }}
                    showFilterMenu={false}
                />
                <Column
                    field="descricao"
                    header="Descrição"
                    filter
                    filterPlaceholder="Buscar"
                    style={{ flexGrow: 0, flexBasis: '45%', textAlign: 'justify'}}
                    showFilterMenu={false}
                />
                <Column
                    field="botao"
                    header="Ação"
                    body={irBodyTemplate}
                    className="col-centralizado"
                    style={{ flexGrow: 0, flexBasis: '10%' }}
                />
 
            </DataTable>
        </div>
    );
}

export default ListaDocumentosDiversos;