import React, { createContext, useEffect, useRef, useState } from 'react';
import { Toast } from 'primereact/toast';

import api from '../config/api';
import './styles.scss'

//contexto
export const ApiContext = createContext();

//provedor
export default function ApiProvider({ children }) {
  const refHoraServidor = useRef();
  const count = useRef(0);
  const toast = useRef(null);
  const [horaAtual, setHoraAtual] = useState(new Date());
  const [token, setToken] = useState(null);

  const [ativo, setAtivo] = useState(true);

  //controle de apresentacao de template ou nao
  const [template, setTemplate] = useState(false);

  useEffect(() => {
    api.get("distribuicao-processos/horario/").then(
      async (response) => {
        refHoraServidor.current = response.data.horario;
        setHoraAtual(refHoraServidor.current);
      }
    );

    const interval = setInterval(() => {
      count.current += 1;
      const novaHora = new Date((refHoraServidor.current + count.current) * 1000);
      if (refHoraServidor)
        setHoraAtual(novaHora);
    }, 1000);

    return () => clearInterval(interval);
  }, [])

  ///para contabilizar a atividade do usuario afim de atualizar o token automatico
  useEffect(() => {
    if(sessionStorage.getItem("ACCESS_TOKEN")) setTemplate(true);
    
    const atividade = setInterval(()=>{
      sessionStorage.setItem("atividade", Number(sessionStorage.getItem("atividade"))-1);
      if(Number(sessionStorage.getItem("atividade")) <= 0){
        setAtivo(false);
      }
    }, 1000);

    return () => clearInterval(atividade);
  }, []);

  function iniciaAtividade() {
    setAtivo(true);
    sessionStorage.setItem("atividade", 300);
  }


  const showError = (titulo, mensagem) => {
    toast.current.show({ severity: 'error', summary: titulo, detail: mensagem, life: 3000 });
  }

  return (
    <ApiContext.Provider
      value={{
        token,
        setToken,
        showError,
        horaAtual,
        iniciaAtividade,
        ativo,
        template,
        setTemplate
      }}
    >
      <Toast ref={toast} />
      {children}
    </ApiContext.Provider>
  )
}

