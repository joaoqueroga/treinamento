import "styled-components";

declare module 'styled-components' {
    export interface DefaultTheme{
        title: string;

        colors:{
            cor_0 : string;
            cor_1 : string;
            cor_2 : string;
            cor_3 : string;
            cor_4 : string;
            cor_btn_branco: string,

            cor_name : string;

            cor_inside : string;
            cor_1_inside : string;
            cor_3_inside : string;
            cor_btn_branco_inside : string,

            cor_hover_menu : string;
            cor_focus_menu : string;
            cor_hover_1 : string;
            cor_hover_3 : string;
            cor_hover_4 : string;

            cor_disabled : string;
            cor_link : string;
            cor_erro : string;

            cor_placeholder : string,
            cor_legenda : string,

            transparent : string,

            cor_1_focus : string,
            cor_3_focus : string,
            cor_4_focus : string,
            cor_menu_focus : string,
            cor_nav_link_focus : string,
            
            cor_text : string,
            cor_menu : string,

            outline_none : string,
            border_none : string,

            img_dark : string,
            input_dark : string,
            input_dark_focus : string,

            datatable_dark : string,
            btn_cancelar : string,
            inputgroup_addon: string,

            cor_btn_bg: string,
            cor_btn_red: string,
            cor_btn_gren: string,
            cor_btn_darkblue: string,
            cor_data_desativada: string,

            cor_th_tabela: string,
            cor_nav_link: string,
            cor_notificacoes: string,

            
        }
    }
}