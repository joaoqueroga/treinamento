import {createGlobalStyle} from 'styled-components';

export default createGlobalStyle`

    body{
        background-color: ${props => props.theme.colors.cor_0} !important;
    }
    .tabela-servidores{
        border-top: 0.5px solid ${props => props.theme.colors.cor_0};
        border-bottom: 0.5px solid ${props => props.theme.colors.cor_0};
    }
    .titulo-user{
        color: #DADADA;
    }
    .name-user {
        color: ${props => props.theme.colors.cor_name};
    }

    .icone-topo-botoes {
        color: #ffffff96 !important;
    }
     .icone-topo-botoes:hover{
        transform: scale(1.1) !important;
    }


    .p-panelmenu .p-panelmenu-content .p-menuitem .p-menuitem-link:focus {
        span {
            color: ${props => props.theme.colors.cor_focus_menu} !important;
        }
    }
    .p-panelmenu .p-panelmenu-content .p-menuitem .p-menuitem-link:hover {
        span {
            color: ${props => props.theme.colors.cor_hover_menu} !important;
        }
    }
    .p-button-link{
        color: ${props => props.theme.colors.cor_1} !important;
    }
    .link{
        color: ${props => props.theme.colors.cor_link} !important;
    }
    .btn-1, .swal2-confirm, .p-inputnumber-button {
        background: ${props => props.theme.colors.cor_link} !important;
    }
    .p-inputnumber-button{
        box-shadow: 0px 4px 8px 2px rgb(0 0 0 / 25%);
        border: none !important;
    }

    .card-login{
        color: ${props => props.theme.colors.cor_2};
    }


    .titulo-dash, .titulo{
        color: ${props => props.theme.colors.cor_3};
    }
    .titulo{
        border-bottom: 2px solid ${props => props.theme.colors.cor_3};
    }
    .navegacao-topo, .titulo-user {
        background-color: ${props => props.theme.colors.cor_3};
    }
    .btn-2, .swal2-cancel, 
    .label-file-servidor {
        background: ${props => props.theme.colors.cor_3} !important;
    }
    .p-inputswitch-slider, .p-selectbutton .p-button[aria-pressed="true"]{
        background: ${props => props.theme.colors.cor_3} !important;
    }
    .p-inputswitch-slider, .p-selectbutton .p-button[aria-pressed="true"][aria-label="Sim"] {
        background: #0b6f40 !important;
    }
    .p-inputswitch-slider, .p-selectbutton .p-button[aria-pressed="true"][aria-label="Não"] {
        background: #6f2727 !important;
    }

    .p-inputswitch-slider, .p-selectbutton [aria-label="Sim"]:focus {
        box-shadow: 0 0 0 2px #ffffff, 0 0 0 4px ${props => props.theme.colors.cor_1_focus}, 0 1px 2px 0 black  !important;
    }
    .p-inputswitch-slider, .p-selectbutton [aria-label="Não"]:focus {
        box-shadow: 0 0 0 2px #ffffff, 0 0 0 4px ${props => props.theme.colors.cor_4_focus}, 0 1px 2px 0 black  !important;
    }


    .btn-3 {
        background: ${props => props.theme.colors.cor_4} !important;
    }

    .btn-branco{
        background: ${props => props.theme.colors.cor_btn_branco} !important;
        color: ${props => props.theme.colors.cor_btn_branco_inside} !important;
        &:hover{
            color: ${props => props.theme.colors.cor_1_inside} !important;
            background: ${props => props.theme.colors.cor_3} !important;
        }
    }

    .view-body, .menu, .card-login , .offcanvas , .p-sidebar, .p-tabview .p-tabview-panels,
    .p-dialog .p-dialog-header, .p-dialog .p-dialog-content, .p-dialog .p-dialog-footer,
    .p-datatable .p-datatable-tbody > tr , .p-overlaypanel, .p-paginator, .p-dropdown-panel{
        background:${props => props.theme.colors.cor_inside} !important;
        color: ${props => props.theme.colors.cor_text} !important ;
    }

    .p-datatable-thead tr th, .p-datatable .p-datatable-header  {
        background:${props => props.theme.colors.cor_th_tabela} !important;
        color: ${props => props.theme.colors.cor_text} !important ;
    }

    .btn-1, .btn-2, .btn-3, .swal2-confirm, .swal2-cancel,
    .label-file-servidor, .p-inputnumber-button {
        color: ${props => props.theme.colors.cor_1_inside} !important;
    }

    .btn-1:hover, .btn-3:hover, .btn-branco:hover, .swal2-confirm:hover, .p-button-link:hover, .login-link:hover,
    .p-inputnumber-button:hover {
        background-color: ${props => props.theme.colors.cor_3} !important;
    }
    .btn-2:hover, .swal2-cancel:hover, .p-datepicker-trigger:hover, 
    .label-file-servidor:hover {
        background-color: ${props => props.theme.colors.cor_1} !important;
    }

    .grupo-input-label{
        button:hover{
            background-color: ${props => props.theme.colors.cor_hover_3} !important;
        }
    }

    .btn-green:focus, .btn-1:focus, .btn-1:focus {
        outline: solid ${props => props.theme.colors.cor_1_focus} !important;
        border: solid ${props => props.theme.colors.cor_1_focus} !important;
        box-shadow: 0 0 0 1px ${props => props.theme.colors.cor_1_focus} !important;
    }
    .btn-darkblue:focus, .btn-2:focus, .btn-branco:focus, .btn-opcoes:focus {
        outline: solid ${props => props.theme.colors.cor_3_focus} !important;
        border: solid ${props => props.theme.colors.cor_3_focus} !important;
        box-shadow: 0 0 0 1px ${props => props.theme.colors.cor_3_focus} !important;
    }
    .btn-red:focus, .btn-3:focus {
        outline: solid ${props => props.theme.colors.cor_4_focus} !important;
        border: solid ${props => props.theme.colors.cor_4_focus} !important;
        box-shadow: 0 0 0 1px ${props => props.theme.colors.cor_4_focus} !important;
    }

    .btn-opcoes {
        background:${props => props.theme.colors.transparent} !important;
    }
    .btn-green, .btn-darkblue, .btn-red {
        background:${props => props.theme.colors.cor_btn_bg} !important;
    }
    .btn-green {
        color:${props => props.theme.colors.cor_btn_gren} !important;
        &:hover{
            background:${props => props.theme.colors.cor_btn_gren} !important;
            color: ${props => props.theme.colors.cor_btn_bg} !important;
        }
    }
    .btn-darkblue {
        color:${props => props.theme.colors.cor_btn_darkblue};
        &:hover{
            background:${props => props.theme.colors.cor_btn_darkblue} !important;
            color: ${props => props.theme.colors.cor_btn_bg} !important;
        }
    }
    .btn-opcoes {
        color:${props => props.theme.colors.cor_text};
        &:hover{
            color:${props => props.theme.colors.cor_text} !important;
        }
    }
    .btn-red {
        color:${props => props.theme.colors.cor_btn_red}!important;
        &:hover{
            background:${props => props.theme.colors.cor_btn_red} !important;
            color: ${props => props.theme.colors.cor_btn_bg} !important;
        }
    }

    btn-1:disabled:hover, .btn-2:disabled:hover, .btn-1:disabled:hover, .btn-2:disabled:hover, .btn-branco:disabled:hover,
    .btn-1:disabled, .btn-2:disabled, .btn-3:disabled, .btn-branco:disabled {
        background-color: ${props => props.theme.colors.cor_disabled} !important;
    }

    .login-link{
        color: ${props => props.theme.colors.cor_link};
    }

    .versao{
        color: ${props => props.theme.colors.cor_legenda};
    }
    .p-panelmenu .p-panelmenu-content .p-menuitem .p-menuitem-link:focus {
        outline: ${props => props.theme.colors.outline_none} !important;
        border: ${props => props.theme.colors.border_none} !important;
        box-shadow: 0 0 0 2px ${props => props.theme.colors.cor_menu_focus} !important;
    }
    .p-panelmenu .p-panelmenu-content .p-menuitem .p-menuitem-link:hover {
        background-color: ${props => props.theme.colors.cor_3} !important;
    }
    .p-datatable.p-datatable-selectable .p-datatable-tbody > tr.p-selectable-row:focus {
        outline: 0.15rem solid ${props => props.theme.colors.cor_3_focus} !important;
    }

    .p-panelmenu .p-panelmenu-header a:hover {
        color: ${props => props.theme.colors.cor_text} !important;
    }
    .p-fileupload-choose {
        color: ${props => props.theme.colors.cor_text} !important;
        background-color: ${props => props.theme.colors.transparent} !important;
        border-color: ${props => props.theme.colors.transparent} !important;
    }


    .p-panelmenu .p-panelmenu-content .p-menuitem .p-menuitem-link {
        span {
            color: ${props => props.theme.colors.cor_menu} !important;
        }
    }
    .p-panelmenu .p-panelmenu-header a{
        color: ${props => props.theme.colors.cor_menu} !important;
    }

    .grupo-input-label, .p-datepicker-trigger{
        color: ${props => props.theme.colors.cor_menu} !important;
    }
    .p-breadcrumb ul li a span,.p-breadcrumb ul li , .p-breadcrumb ul .p-menuitem-text {
        color: ${props => props.theme.colors.cor_menu} !important;
    }
    .dashboard{
        img{
            width: 100%;
            opacity: 40%;
        }
        h3{
            filter: ${props => props.theme.colors.img_dark};
        }
    }
    .image-logo-colorida{
        filter: ${props => props.theme.colors.img_dark};
    }

    .p-inputtext, .p-dropdown, .input-uf-naturalidade, .defensor-item {
        ${props => props.theme.colors.input_dark};
    }
    .p-inputtext:focus, .p-focus, .p-selectbutton .p-button[aria-pressed="false"]:hover {
        ${props => props.theme.colors.input_dark_focus};
    }


    .p-datatable .p-datatable-tbody > tr {
        ${props => props.theme.colors.datatable_dark};
    }
    .p-datatable .p-datatable-tbody > tr:hover {
        background-color: ${props => props.theme.colors.cor_th_tabela} !important;
    }

    .p-inputgroup-addon, .grupo-input-label button , .p-datepicker-trigger, 
    .p-selectbutton .p-button[aria-pressed="false"]
    { 
        ${props => props.theme.colors.inputgroup_addon} ;
    }

    .mensagem-erro {
        padding: 5px;
        font-size: 14px;
        color: ${props => props.theme.colors.cor_erro};
    }

    .p-checkbox .p-checkbox-box.p-highlight,
    .p-radiobutton .p-radiobutton-box.p-highlight
    {
        border-color: ${props => props.theme.colors.cor_1};
        background: ${props => props.theme.colors.cor_1};
    }
    .p-checkbox .p-checkbox-box,
    .p-radiobutton .p-radiobutton-box
    {
        background: ${props => props.theme.colors.cor_inside};
    }
    .p-checkbox:not(.p-checkbox-disabled) .p-checkbox-box:hover {
        border-color: ${props => props.theme.colors.cor_hover_1};
    }
    .p-checkbox .p-checkbox-box.p-highlight:not(.p-disabled):hover {
        border-color: ${props => props.theme.colors.cor_hover_3};
        background: ${props => props.theme.colors.cor_hover_3};
    }
    .p-dropdown:not(.p-disabled):hover {
        border-color: ${props => props.theme.colors.cor_1};
    }
    .p-dropdown:not(.p-disabled).p-focus {
        box-shadow: 0 0 0 0.2rem ${props => props.theme.colors.cor_1_focus} ;
        border-color: ${props => props.theme.colors.cor_1};
    }
    .p-dropdown-panel .p-dropdown-items .p-dropdown-item.p-highlight {
        color: ${props => props.theme.colors.cor_1};
        background: ${props => props.theme.colors.cor_1_focus};
    }
    .p-inputtext:enabled:hover {
        border-color: ${props => props.theme.colors.cor_1};
    }
    .p-inputtext:enabled:focus {
        box-shadow: 0 0 0 0.2rem ${props => props.theme.colors.cor_1_focus};
        border-color: ${props => props.theme.colors.cor_1};
    }
    ul.lista-sorteados {
        li {
            background-color: #eee ;

            &.suplente-item {
                background-color: ${props => props.theme.colors.cor_3_focus};
        
                &:hover {
                  background-color: ${props => props.theme.colors.cor_3};
                  color: ${props => props.theme.colors.cor_inside};
                }
              }
        
              &:hover {
                background-color: ${props => props.theme.colors.cor_1_focus};
              }
        
        }
    }
    .distribuicao-processos{
        .suplente-item {
            background-color: ${props => props.theme.colors.cor_3_focus} !important;//#4c4a31
            &:hover {
                background-color: ${props => props.theme.colors.cor_3} !important;
                color: ${props => props.theme.colors.cor_3_inside} !important;
            }
        }
    }
    .defensor-item{
        &:hover {
            background-color: ${props => props.theme.colors.cor_1_focus} !important;
          }      
    }
    .tag-dias, .tag-mes{
        background-color: ${props => props.theme.colors.cor_3} !important;
    }
    .p-slider .p-slider-range {
        background: ${props => props.theme.colors.cor_1};
    }
    
    .saldos-disponiveis{
        border: 1px solid ${props => props.theme.colors.cor_0};

        label{
            border-right: 1px solid ${props => props.theme.colors.cor_0};
        }

        .p-checkbox .p-checkbox-box.p-highlight: hover {
            border-color: ${props => props.theme.colors.cor_1};
        }

    }

    .saldos-disponiveis:hover{
        background-color:${props => props.theme.colors.cor_1} !important;
        border: 1px solid ${props => props.theme.colors.cor_1} !important;

        label{
            color:${props => props.theme.colors.cor_3_inside} !important;
            border-right: 1px solid ${props => props.theme.colors.cor_1} !important;
        }
        
        .p-checkbox .p-checkbox-box {
            border: 1px solid ${props => props.theme.colors.cor_1} !important;
        }

    }

    .saldo-selecionado {
        background-color:${props => props.theme.colors.cor_3} !important;
        border: 1px solid ${props => props.theme.colors.cor_3} !important;
        color:${props => props.theme.colors.cor_3_inside} !important;
        label{
            border-right: 1px solid ${props => props.theme.colors.cor_3} !important;
        }
        .p-checkbox .p-checkbox-box.p-highlight {
            border-color: ${props => props.theme.colors.cor_3} !important;
            background: ${props => props.theme.colors.cor_inside} !important;
            .p-checkbox-icon{
                color: ${props => props.theme.colors.cor_text} !important;
            }
        }
        .p-checkbox .p-checkbox-box.p-highlight:hover {
            color: ${props => props.theme.colors.cor_1} !important;
            border-color: ${props => props.theme.colors.cor_1} !important;
        }
    }
    .calendario-nav, .calendario-folgas-nav{
        background-color:${props => props.theme.colors.cor_3} !important;
        color:${props => props.theme.colors.cor_3_inside} !important;

    }
    .calendario-folgas-data button {
        background-color:${props => props.theme.colors.cor_inside} ;
        color:${props => props.theme.colors.cor_text} ;
    }
    .calendario-folgas-data button:disabled{
        color: ${props => props.theme.colors.cor_data_desativada} !important;
    }

    .mes-anterior-cal{
        color: ${props => props.theme.colors.cor_data_desativada} !important;
    }

    .cal-botao-clicado{
        background-color: ${props => props.theme.colors.cor_3} !important;
        color: ${props => props.theme.colors.cor_3_inside} !important;
    }    

    .p-slider .p-slider-handle{
        border: 1px solid  ${props => props.theme.colors.cor_1};
    }
    .p-slider .p-slider-handle:hover{
        background-color: ${props => props.theme.colors.cor_1};
    }

    .p-slider .p-slider-range{
        border-radius: 14px !important;
    }

    .p-slider:not(.p-disabled) .p-slider-handle:hover {
        background: ${props => props.theme.colors.cor_inside};
        border-color: ${props => props.theme.colors.cor_1};
    }

    .p-slider .p-slider-handle:focus {
        box-shadow: 0 0 0 0.2rem ${props => props.theme.colors.cor_1_focus};
    }

    .tag-respondida{
        background-color: ${props => props.theme.colors.cor_3} !important;
        font-size: 1rem;
    }

    .p-datatable .p-sortable-column.p-highlight:hover {
        background: #EEF2FF;
        color: #4338CA;
    }

    .p-tabview .p-tabview-nav li.p-highlight .p-tabview-nav-link{
        color: ${props => props.theme.colors.cor_nav_link} !important;
        border-color: ${props => props.theme.colors.cor_nav_link} !important;
    }

    .p-tabview .p-tabview-nav .p-tabview-ink-bar{
        background-color: ${props => props.theme.colors.cor_nav_link} !important;
    }

    .p-tabview .p-tabview-nav li .p-tabview-nav-link:not(.p-disabled):focus {
        box-shadow: inset 0 0 0 0.2rem ${props => props.theme.colors.cor_nav_link_focus} !important;
    }

    .dropdown_darkblue {
        background-color: ${props => props.theme.colors.cor_3} !important;
        .p-dropdown-label, .p-dropdown-trigger-icon {
            color: ${props => props.theme.colors.cor_3_inside} !important;
        }
    }
    .inputnumber_darkblue{
        .p-inputtext, .p-inputnumber-button{
            background-color: ${props => props.theme.colors.cor_3} !important;
            color: ${props => props.theme.colors.cor_3_inside} !important;
            border: none;
            box-shadow: none;
        }
    }
    a.link{
        color: ${props => props.theme.colors.cor_1} !important;
    }

    .p-radiobutton .p-radiobutton-box:not(.p-disabled):not(.p-highlight):hover {
        border-color: ${props => props.theme.colors.cor_1} !important;
    }
    .p-radiobutton .p-radiobutton-box:not(.p-disabled).p-focus {
        outline: 0 none;
        outline-offset: 0;
        box-shadow: 0 0 0 0.2rem ${props => props.theme.colors.cor_1_focus} !important;
        border-color: ${props => props.theme.colors.cor_1_focus} !important;
    }
    .p-radiobutton .p-radiobutton-box.p-highlight {
        border-color: ${props => props.theme.colors.cor_1};
        background: ${props => props.theme.colors.cor_1};
    }
    .p-radiobutton .p-radiobutton-box.p-highlight:not(.p-disabled):hover {
        border-color: ${props => props.theme.colors.cor_3};
        background: ${props => props.theme.colors.cor_3};
    }
    .tag-ferias{
        background-color: #d0ffd0c4;
        color: #59ba00;
    }
    .tag-pecunia{
        background-color: #fff5d0;
        color: #ba8400;
    }
    .tag-folgas{
        background-color: #d0f8ff;
        color: #006a6d;
    }
    .tag-processo-seletivo{
        background-color: #d0e2ff;
        color: #002d9c;
    }
    .tag-meritocracia{
        background-color: #ead0ff;
        color: #78009c;
    }

    .tag-aprovado, .tag-tramitando, .tag-cancelado, .tag-indeferido, .tag-lista-dias, .tag-alteracao{
        font-weight: 500 !important;
        font-size: 0.7rem !important;      
    }
    .tag-aprovado{
        background-color: #a7f0ba;
        color: #044317;
    }
    .tag-tramitando{
        background-color: #d0e2ff;
        color: #002d9c;
    }
    .tag-cancelado{
        background-color: #e5e0df;
        color: #3c3838;
    }
    .tag-indeferido{
        background-color: #ffd7d9;
        color: #750e13;
    }
    .tag-alteracao{
        background-color: #ffebb3;
        color: #75560e;
    }
    .tag-lista-dias{
        background-color: #dde1e6;
        color: #343a3f;
    }
    .p-datatable-row-expansion td{
        .p-datatable{
            margin: 0 2rem 0 2rem;
            .p-datatable-wrapper{
                height: auto !important;
            }
        }
        background:${props => props.theme.colors.cor_th_tabela} !important;
        .p-datatable-tbody tr td, .p-datatable-thead tr th {
            font-size: 0.9rem !important;
        }
    }
    .p-paginator .p-paginator-pages .p-paginator-page.p-highlight {
        background: ${props => props.theme.colors.cor_3};
        border-color: ${props => props.theme.colors.cor_3};
        color: ${props => props.theme.colors.cor_3_inside};
    }

    #tabela-notificacoes .p-selectable-row{
        background: ${props => props.theme.colors.cor_notificacoes} !important;
    }

    .p-overlaypanel .p-overlaypanel-close {
        background: #104856 !important;
    }
    .p-overlaypanel .p-overlaypanel-close:hover {
        background: #19985C !important;
    }



    
    
    

    

      
    
    


    
      
    
      
    

`;