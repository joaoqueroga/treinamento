export const dark = {
    title: 'dark',
  
    colors: {

      cor_text : '#EEEEEE',
      cor_menu : '#EEEEEE',

      cor_0 : '#232931',
      cor_1 : '#21583e',
      cor_2 : '#959595',
      cor_3 : '#104856',
      cor_4 : '#4c2b2b',
      cor_btn_branco: "#a3a3a7",
      cor_name : '#DADADA',

      cor_inside : '#393E46',
      cor_1_inside : '#EEEEEE',
      cor_3_inside : '#EEEEEE',
      cor_btn_branco_inside : '#104856',

      cor_hover_menu : '#DADADA',
      cor_focus_menu : '#EEEEEE',
      cor_hover_1 : '#194a33',
      cor_hover_3 : '#0d3641',
      cor_hover_4 : '#9e3737',

      cor_disabled : '#818ea1b5',
      cor_link : '#04A64B',
      cor_erro : '#EE5012',

      cor_placeholder : '#C4C4C4',
      cor_legenda : '#8F8F8F',
      cor_legenda_hover: '#727272',

      transparent : 'transparent',

      cor_1_focus: '#19985d28',
      cor_3_focus: '#0d586d3b',
      cor_4_focus: '#c13e3e4b',
      cor_menu_focus: '#eeeeee14',
      
      outline_none : 'none',
      border_none : 'none',

      btn_listas_bg : "#104856",
      btn_listas_color : "#DADADA",
      
      img_dark: "brightness(250%);opacity: 40%",
      input_dark: "background: #34383e !important; color: #EEEEEE; border: 1px solid #eeeeee14 !important",
      input_dark_focus : "outline: solid darkgrey !important",

      cor_btn_bg : "#EBEBEB",
      cor_btn_red: "#C13E3E",
      cor_btn_gren: "#19985C",
      cor_btn_darkblue: "#104856",

      datatable_dark : "color: #EEEEEE; td { border: 1px solid #dee2e630;border-width: 0 0 1px 0} ",

      btn_cancelar : "#6e737a",

      inputgroup_addon : "background: #34383e; color: #6c757d !important; border-left: 1px solid #eeeeee14; border-right: 1px solid #eeeeee14 !important; border-bottom: 1px solid #eeeeee14 !important; border-top: 1px solid #eeeeee14 !important",
      cor_data_desativada: "#7d7d7d",

      cor_th_tabela: "#474c54",
      cor_nav_link: '#EEEEEE',
      cor_nav_link_focus: '#ffffff1f',
      cor_notificacoes: '#fbf9ec3d',

             




      
    },
  };