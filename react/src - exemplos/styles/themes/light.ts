export const light = {
    title: 'light',
  
    colors: {

      cor_text : '#495057',
      cor_menu : '#6c757d',

      cor_0 : '#DADADA',
      cor_1 : '#19985C',
      cor_2 : '#959595',
      cor_3 : '#104856',
      cor_4 : '#9e3737',
      cor_btn_branco: "#DADADA",
      cor_name : '#0d3641',

      cor_inside : '#FFFFFF',
      cor_1_inside : '#EEEEEE',
      cor_3_inside : '#EEEEEE',
      cor_btn_branco_inside : '#104856',
      
      cor_hover_menu : '#FFFFFF',
      cor_focus_menu : '#104856',
      cor_hover_1 : '#0D7159',
      cor_hover_3 : '#0d3641',
      cor_hover_4 : '#9e3737',

      cor_disabled : '#818ea1b5',
      cor_link : '#04A64B',
      cor_erro : '#EE5012',

      cor_placeholder : '#C4C4C4',
      cor_legenda : '#8F8F8F',
      cor_legenda_hover: '#727272',

      transparent : 'transparent',

      cor_1_focus: '#19985d28',
      cor_3_focus: '#0d586d3b',
      cor_4_focus: '#c13e3e4b',
      cor_menu_focus: '#19985d28',

      outline_none : 'none',
      border_none : 'none',

      btn_cancelar : "#64748B",

      cor_btn_bg : "#EBEBEB",
      cor_btn_red: "#C13E3E",
      cor_btn_gren: "#19985C",
      cor_btn_darkblue: "#104856",
      cor_data_desativada: "#DADADA",

      cor_th_tabela: "#f8f9fa",
      cor_nav_link: '#19985C',
      cor_nav_link_focus: '#0d586d3b',
      cor_notificacoes: '#FBF9EC',

    },
  };