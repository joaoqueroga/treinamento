import React, {useContext} from 'react';
import './template.scss';
import Navegacao from '../components/navegacao';
import NavegacaoTopo from '../components/navegacao_topo';

import usePersistedState from '../utils/usePersistedState';
import GlobalStyle from '../styles/global';
import { ApiContext } from '../context/store';
import { ThemeProvider } from 'styled-components';
import { light } from '../styles/themes/light';
import { dark } from '../styles/themes/dark';


function Template(props) {
  const [theme, setTheme] = usePersistedState('theme', light);

  const {iniciaAtividade, template} = useContext(ApiContext); 
  const switchTheme = (e) =>{
    setTheme(e.value.key === 'light' ? light : dark);
  };

  return (
    <ThemeProvider theme={theme} >
      <GlobalStyle/>
      {
        template?
        <div>
          <div className="topo" onClick={iniciaAtividade}>
            <NavegacaoTopo switchTheme={switchTheme}/>
          </div>
          <div className="App" onClick={iniciaAtividade}>
              <div className="navegacao">
                <div className="menu">
                  <Navegacao />
                </div>
              </div>
            <div className="rotas">
              {props.children}
            </div>
          </div>
        </div>
      : 
        props.children
      }
    </ThemeProvider> 
  );
}

export default Template;