import { Route, Navigate } from 'react-router-dom';

import { autenticado } from '../config/auth';
import Permissoes from '../config/permissoes';

//views da aplicação
import ListaServidores from '../views/rh/lista_servidores';
import CadastroServidor from '../views/rh/cadastro_servidor';
import Servidor from '../views/rh/ver_servidor';
import Lotacoes from '../views/rh/lotacao';
import EditarServidor from '../views/rh/editar_servidor';
import RhFuncoes from '../views/rh/parametros/funcoes';
import RelatoriosRh from '../views/rh/relatorios';
import RhPredios from '../views/rh/parametros/predios';
import ClassesServidor from '../views/rh/parametros/classes-servidor/classes_servidor';
import ListaVerServidores from '../views/rh/ver_servidor/lista';

const ProtectedRoute = ({ children , path}) => {
    return autenticado() ?  
        <Permissoes path={path}>
             {children} 
        </Permissoes>
        : 
        <Navigate to="/" replace />;
};

//permissoes LDAP para acesso as ROTAS
const permissoes = ["admin", "rh"];

export default [
    <Route
        key="lista-servidores-ver"
        path="/rh/ver-servidores"
        element={
            <ProtectedRoute path="/rh/ver-servidores">
                <ListaVerServidores/>
            </ProtectedRoute>
        }
    />,
    <Route
        key="lista-servidores"
        path="/rh/servidores"
        element={
            <ProtectedRoute path="/rh/servidores">
                <ListaServidores/>
            </ProtectedRoute>
        }
    />,
    <Route
        key="cadastro-servidor"
        path="/rh/novo_servidor"
        element={
            <ProtectedRoute path="/rh/novo_servidor">
                <CadastroServidor/>
            </ProtectedRoute>
        }
    />,
    <Route
        key="ver-servidor"
        path="/rh/servidor/:id"
        element={
            <ProtectedRoute path="/rh/servidor/:id">
                <Servidor/>
            </ProtectedRoute>
        }
    />,
    <Route
        key="lotacoes"
        path="/rh/lotacoes"
        element={
            <ProtectedRoute path="/rh/lotacoes">
                <Lotacoes/>
            </ProtectedRoute>
        }
    />,
    <Route
        key="relatorios"
        path="/rh/relatorios"
        element={
            <ProtectedRoute path="/rh/relatorios">
                <RelatoriosRh/>
            </ProtectedRoute>
        }
    />,
    <Route
        key="editar-servidor"
        path="/rh/editar_servidor/:id"
        element={
            <ProtectedRoute path="/rh/editar_servidor/:id">
                <EditarServidor/>
            </ProtectedRoute>
        }
    />,
    <Route
        key="funcoes-servidores"
        path="/rh/funcoes"
        element={
            <ProtectedRoute path="/rh/funcoes">
                <RhFuncoes/>
            </ProtectedRoute>
        }
    />,
    <Route
        key="predios"
        path="/rh/predios"
        element={
            <ProtectedRoute path="/rh/predios">
                <RhPredios/>
            </ProtectedRoute>
        }
    />,
    <Route
        key="classes_servidor"
        path="/rh/classes_servidor"
        element={
            <ProtectedRoute path="/rh/classes_servidor">
                <ClassesServidor/>
            </ProtectedRoute>
        }
    />
];