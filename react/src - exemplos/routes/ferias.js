import { Route, Navigate } from 'react-router-dom';

import { autenticado } from '../config/auth';
import Permissoes from '../config/permissoes';

//views da aplicação
import FeriasAprovacao from '../views/modulo_ferias/ferias/aprovacao';
import FeriasSolicitacao from '../views/modulo_ferias/ferias/solicitacao';
import EscalaFerias from '../views/modulo_ferias/ferias/escala';
import PesquisaEscalaFeriasListagem from '../views/modulo_ferias/ferias/pesquisa_escala/pesquisa';
import PesquisaEscalaFeriasDetalhe from '../views/modulo_ferias/ferias/pesquisa_escala/detalhe';
import SolicitacoesFerias from '../views/modulo_ferias/ferias/solicitacoes';
import CreditarFolgas from '../views/modulo_ferias/folgas/creditar';
import CreditarFolgaListagem from '../views/modulo_ferias/folgas/creditar_listagem';
import VisualizarFeriasFolgasGeral from '../views/modulo_ferias/visualizacao_geral';
import SolicitarParaTerceiros from '../views/modulo_ferias/solicitacoes_terceiros';

const ProtectedRoute = ({ children , path}) => {
    return autenticado() ?  
        <Permissoes path={path}>
             {children} 
        </Permissoes>
        : 
        <Navigate to="/" replace />;
};

export default [
    <Route
        key="solicitar-ferias"
        path="/solicitacao_ferias"
        element={
            <ProtectedRoute path="/solicitacao_ferias">
                <FeriasSolicitacao/>
            </ProtectedRoute>
        }
    />,
    <Route
        key="aprovar-ferias"
        path="/ferias/aprovacao/"
        element={
            <ProtectedRoute path="/ferias/aprovacao">
                <FeriasAprovacao/>
            </ProtectedRoute>
        }
    />,

    <Route
        key="escala-de-ferias"
        path="/escala_ferias"
        element={
            <ProtectedRoute path="/escala_ferias">
                <EscalaFerias/>
            </ProtectedRoute>
        }
    />,
    <Route
    key="pesquisa-escala-ferias"
    path="/ferias/pesquisa_escala_ferias"
    element={
        <ProtectedRoute path="/ferias/pesquisa_escala_ferias">
            <PesquisaEscalaFeriasListagem/>
        </ProtectedRoute>
    }
    />,
    <Route
    key="gestao-escala-de-ferias-detalhe"
    path="/ferias/gestao_escala/:id"
    element={
        <ProtectedRoute path="/ferias/gestao_escala/:id">
            <PesquisaEscalaFeriasDetalhe/>
        </ProtectedRoute>
    }
    />,
    <Route
        key="solicitacoes-ferias"
        path="/solicitacoes/ferias"
        element={
            <ProtectedRoute path="/solicitacoes/ferias">
                <SolicitacoesFerias/>
            </ProtectedRoute>
    }/>,

    <Route
        key="creditar-folgas"
        path="/solicitacoes/creditar-folgas"
        element={
            <ProtectedRoute path="/solicitacoes/creditar-folgas">
                <CreditarFolgas/>
            </ProtectedRoute>
    }/>,

    <Route
        key="creditar-folgas-listagem"
        path="/solicitacoes/creditar-folgas-listagem"
        element={
            <ProtectedRoute path="/solicitacoes/creditar-folgas-listagem">
                <CreditarFolgaListagem/>
            </ProtectedRoute>
    }/>,

    <Route
        key="todas as solicitações"
        path="/ferias/todas_solicitacoes"
        element={
            <ProtectedRoute path="/ferias/todas_solicitacoes">
                <VisualizarFeriasFolgasGeral/>
            </ProtectedRoute>
    }/>,

    <Route
        key="solicitar para terceiros"
        path="/ferias/solicitacoes_outros"
        element={
            <ProtectedRoute path="/ferias/solicitacoes_outros">
                <SolicitarParaTerceiros/>
            </ProtectedRoute>
    }/>,
    
];