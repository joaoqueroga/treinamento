import { Route, Navigate } from 'react-router-dom';

import { autenticado } from '../config/auth';
import Permissoes from '../config/permissoes';

//views da aplicação


//folgas
import SolicitacoesTelaPrincipal from '../views/modulo_ferias/solicitacoes_gerais';

const ProtectedRoute = ({ children , path}) => {
    return autenticado() ?  
        <Permissoes path={path}>
             {children} 
        </Permissoes>
        : 
        <Navigate to="/" replace />;
};

export default [
    <Route
        key="folgas-solicitacao"
        path="/publico/solicitacoes"
        element={
            <ProtectedRoute path="/publico/solicitacoes">
                <SolicitacoesTelaPrincipal/>
            </ProtectedRoute>
        }
    />, 
];