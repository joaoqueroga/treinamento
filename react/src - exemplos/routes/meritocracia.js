import { Route, Navigate } from 'react-router-dom';

import { autenticado } from '../config/auth';
import Permissoes from '../config/permissoes';

//views da aplicação
import MeritocraciaMapa from '../views/meritocracia/mapa';
import MeritocraciaImportarPlanilhas from '../views/meritocracia/importar';
import TrimestresMeritocracia from '../views/meritocracia/lista_trimestres';

const ProtectedRoute = ({ children , path}) => {
    return autenticado() ?  
        <Permissoes path={path}>
             {children} 
        </Permissoes>
        : 
        <Navigate to="/" replace />;
};

export default [
    <Route
        key="trimestres"
        path="/meritocracia/trimestres"
        element={
            <ProtectedRoute path="/meritocracia/trimestres">
                    <TrimestresMeritocracia/>
            </ProtectedRoute>
        }
    />,
    <Route
        key="importar"
        path="/meritocracia/importar/:id"
        element={
            <ProtectedRoute path="/meritocracia/importar/:id">
                    <MeritocraciaImportarPlanilhas/>
            </ProtectedRoute>
        }
    />,
    <Route
        key="mapa"
        path="/meritocracia/mapa/:id"
        element={
            <ProtectedRoute path="/meritocracia/mapa/:id">
                    <MeritocraciaMapa/>
            </ProtectedRoute>
        }
    />,
];