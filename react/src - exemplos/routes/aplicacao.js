import { Route, Navigate } from 'react-router-dom';

import { autenticado } from '../config/auth';
import Permissoes from '../config/permissoes';

//views da aplicação
import Dashboard from '../views/dashboard';
import Login from '../views/login';
import Notificacoes from '../../src/views/notificacoes';
import LoginAdmin from '../views/login/admin_login';


const ProtectedRoute = ({ children , path}) => {
    return autenticado() ?  
        <Permissoes path={path}>
             {children} 
        </Permissoes>
        : 
        <Navigate to="/" replace />;
};

//permissoes LDAP para acesso as ROTAS
const permissoes = ["admin","publico", "rh", "meritrocacia" ,"distribuir_processos", "estagio", "notificacoes"];

export default [
    <Route
        key="pagina-de-login" 
        path='/' 
        element={
            <Login/>
        }
    />,

    <Route
        key="pagina-de-login-admin" 
        path='/adm' 
        element={
            <LoginAdmin/>
        }
    />,

    <Route
        key="pagina-inicial"
        path="/inicio"
        element={
            <ProtectedRoute path="/inicio">
                <Dashboard/>
            </ProtectedRoute>
        }
    />,

    <Route
    key="notificacoes"
    path="/notificacoes"
    element={
        <ProtectedRoute path="/notificacoes">
            <Notificacoes />
        </ProtectedRoute>
    }
/>
];