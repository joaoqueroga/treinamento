import { Route, Navigate } from 'react-router-dom';

import { autenticado } from '../config/auth';
import Permissoes from '../config/permissoes';

//views da aplicação
import ListaEstagiarios from '../views/estagio/listar';
import CadastroEstagiario from '../views/estagio/cadastrar';
import EstagioAgentesIntegracao from '../views/estagio/parametros/agentes_integracao';
import EstagioIstituicoesEnsino from '../views/estagio/parametros/instituicoes_ensino';
import EditarEstagiario from '../views/estagio/editar';
import RelatoriosEstagio from '../views/estagio/relatorios';
import LotacaoEstagio from '../views/estagio/lotacoes';
import TiposEstagiario from '../views/estagio/parametros/tipos_estagiario';
import ProcessosSeletivosEstagio from '../views/estagio/processos_seletivos';
import ProcessoSeletivo from '../views/estagio/processos_seletivos/processo';


const ProtectedRoute = ({ children, path }) => {
    return autenticado() ?
        <Permissoes path={path}>
            {children}
        </Permissoes>
        :
        <Navigate to="/" replace />;
};

//permissoes LDAP para acesso as ROTAS
const permissoes = ["admin", "estagio"];

export default [
    <Route
        key="cadastro-estagiario"
        path="/estagio/novo"
        element={
            <ProtectedRoute path="/estagio/novo">
                <CadastroEstagiario />
            </ProtectedRoute>
        }
    />,
    <Route
        key="listar-estagiarios"
        path="/estagio/listar"
        element={
            <ProtectedRoute path="/estagio/listar">
                <ListaEstagiarios />
            </ProtectedRoute>
        }
    />,
    <Route
        key="agentes-integracao"
        path="/estagio/agentes_integracao"
        element={
            <ProtectedRoute path="/estagio/agentes_integracao">
                <EstagioAgentesIntegracao />
            </ProtectedRoute>
        }
    />,
    <Route
        key="instituicoes-ensino"
        path="/estagio/instituicoes"
        element={
            <ProtectedRoute path="/estagio/instituicoes">
                <EstagioIstituicoesEnsino />
            </ProtectedRoute>
        }
    />,
    <Route
        key="tipos-estagiarios"
        path="/estagio/tipos_estagiario"
        element={
            <ProtectedRoute path="/estagio/tipos_estagiario">
                <TiposEstagiario />
            </ProtectedRoute>
        }
    />,
    <Route
        key="editar-estagiario"
        path="/estagio/editar/:id"
        element={
            <ProtectedRoute path="/estagio/editar/:id">
                <EditarEstagiario />
            </ProtectedRoute>
        }
    />,
    <Route
        key="relatorios-estagio"
        path="/estagio/relatorios"
        element={
            <ProtectedRoute path="/estagio/relatorios">
                <RelatoriosEstagio />
            </ProtectedRoute>
        }
    />,
    <Route
        key="vagas-lotacao"
        path="/estagio/lotacoes"
        element={
            <ProtectedRoute path="/estagio/lotacoes">
                <LotacaoEstagio />
            </ProtectedRoute>
        }
    />,
    <Route
        key="processos-seletivos"
        path="/estagio/processos_seletivos"
        element={
            <ProtectedRoute path="/estagio/processos_seletivos">
                <ProcessosSeletivosEstagio />
            </ProtectedRoute>
        }
    />,
    <Route
        key="processo-seletivo"
        path="/estagio/processo_seletivo/:id"
        element={
            <ProtectedRoute path="/estagio/processo_seletivo/:id">
                <ProcessoSeletivo />
            </ProtectedRoute>
        }
    />

];