import { Route, Navigate } from 'react-router-dom';

import { autenticado } from '../config/auth';
import Permissoes from '../config/permissoes';

//views da aplicação
import ParametrosEcivil from '../views/admin/parametros/estado_civil';
import ParametrosGrauInstrucao from '../views/admin/parametros/grau_instrucao';
import ParametrosTipoAnotacao from '../views/admin/parametros/tipo_anotacao';
import ParametrosTipoDocumento from '../views/admin/parametros/tipo_documento';
import ParametrosTipoLogradouro from '../views/admin/parametros/tipo_logradouro';
import PainelGerenciamento from '../views/admin/painel-gerenciamento/painel_gerenciamento';
import GerenciarUsuarios from '../views/admin/painel-gerenciamento/gerenciar_usuarios';
import InformacoesUsuario from '../views/admin/painel-gerenciamento/gerenciar_usuarios/informacoes_usuario';
import GerenciarGrupos from '../views/admin/painel-gerenciamento/gerenciar_grupos';
import InformacoesGrupo from '../views/admin/painel-gerenciamento/gerenciar_grupos/informacoes_grupo';
import AprovacaoListaServidores from '../views/admin/gerenciamento_servidores/aprovador_ferias';

const ProtectedRoute = ({ children , path}) => {
    return autenticado() ?  
        <Permissoes path={path}>
             {children} 
        </Permissoes>
        : 
        <Navigate to="/" replace />;
};

export default [
    <Route
        key="estado-civil"
        path="/admin/ecivil"
        element={
            <ProtectedRoute path="/admin/ecivil">
                <ParametrosEcivil/>
            </ProtectedRoute>
        }
    />,
    <Route
        key="grau-de-instrucao"
        path="/admin/grau_instrucao"
        element={
            <ProtectedRoute path="/admin/grau_instrucao">
                <ParametrosGrauInstrucao/>
            </ProtectedRoute>
        }
    />,
    <Route
        key="tipos-de-anotacao"
        path="/admin/tipo_anotacao"
        element={
            <ProtectedRoute path="/admin/tipo_anotacao">
                <ParametrosTipoAnotacao/>
            </ProtectedRoute>
        }
    />,
    <Route
        key="tipos-de-documento"
        path="/admin/tipo_documento"
        element={
            <ProtectedRoute path="/admin/tipo_documento">
                <ParametrosTipoDocumento/>
            </ProtectedRoute>
        }
    />,
    <Route
        key="tipos-de-logradouro"
        path="/admin/tipo_logradouro"
        element={
            <ProtectedRoute path="/admin/tipo_logradouro">
                <ParametrosTipoLogradouro/>
            </ProtectedRoute>
        }
    />,
    <Route
    key="painel-de-gerenciamento"
    path="/admin/painel_gerenciamento"
    element={
        <ProtectedRoute path="/admin/painel_gerenciamento">
            <PainelGerenciamento/>
        </ProtectedRoute>
    }
    />,
    <Route
    key="painel-de-gerenciamento-usuarios"
    path="/admin/painel_gerenciamento/gerenciar_usuarios"
    element={
        <ProtectedRoute path="/admin/painel_gerenciamento/gerenciar_usuarios">
            <GerenciarUsuarios/>
        </ProtectedRoute>
    }
    />,
    <Route
    key="painel-de-gerenciamento-usuario"
    path="/admin/painel_gerenciamento/gerenciar_usuario/:id"
    element={
        <ProtectedRoute path="/admin/painel_gerenciamento/gerenciar_usuario/:id">
            <InformacoesUsuario/>
        </ProtectedRoute>
    }
    />,
    <Route
    key="painel-de-gerenciamento-grupos"
    path="/admin/painel_gerenciamento/gerenciar_grupos"
    element={
        <ProtectedRoute path="/admin/painel_gerenciamento/gerenciar_grupos">
            <GerenciarGrupos/>
        </ProtectedRoute>
    }
    />,
    <Route
    key="painel-de-gerenciamento-grupo"
    path="/admin/painel_gerenciamento/gerenciar_grupo/:id"
    element={
        <ProtectedRoute path="/admin/painel_gerenciamento/gerenciar_grupo/:id">
            <InformacoesGrupo/>
        </ProtectedRoute>
    }/>,
    <Route
        key="aprovador-de-ferias"
        path="/admin/aprovadores_ferias"
        element={
            <ProtectedRoute path="/admin/aprovadores_ferias">
                <AprovacaoListaServidores/>
            </ProtectedRoute>
        }
    />
];