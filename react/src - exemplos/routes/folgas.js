import { Route, Navigate } from 'react-router-dom';

import { autenticado } from '../config/auth';
import Permissoes from '../config/permissoes';
import FolgasSolicitacao from '../views/modulo_ferias/folgas/solicitacao';
import CreditarFolgaSaldo from '../views/modulo_ferias/folgas/creditar_detalhe_saldo';
import CreditarFolgas from '../views/modulo_ferias/folgas/creditar';

const ProtectedRoute = ({ children , path}) => {
    return autenticado() ?  
        <Permissoes path={path}>
             {children} 
        </Permissoes>
        : 
        <Navigate to="/" replace />;
};

export default [

    <Route
        key="folgas-solicitacao"
        path="/folgas/solicitacao"
        element={
            <ProtectedRoute path="/folgas/solicitacao">
                <FolgasSolicitacao/>
            </ProtectedRoute>
        }
    />,
    <Route
        key="folgas-creditos"
        path="/folgas/creditos/:id_funcionario"
        element={
            <ProtectedRoute path="/folgas/creditos/:id_funcionario">
                <CreditarFolgaSaldo/>
            </ProtectedRoute>
        }
    />,
    <Route
        key="folgas-creditar"
        path="/folgas/creditar/:id_funcionario"
        element={
            <ProtectedRoute path="/folgas/creditar/:id_funcionario">
                <CreditarFolgas/>
            </ProtectedRoute>
        }
    />,
];