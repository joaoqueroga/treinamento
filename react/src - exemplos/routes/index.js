import {BrowserRouter, Route, Routes} from 'react-router-dom';

import Pagina404 from '../views/pagina404';

import rotas_aplicacao from './aplicacao';
import rotas_admin from './admin';
import rotas_rh from './rh';
import rotas_estagio from './estagio';
import rotas_ferias from './ferias';
import rotas_folgas from  './folgas';
import rotas_pecunia from  './pecunia';
import rotas_publicas from './publico';
import rotas_meritocracia from './meritocracia';
import rotas_distribuicao_processos from './distribuicao_processos';
import rotas_da from './da'

import Template from '../template';

function Rotas(){
    return(
        <BrowserRouter>
            <Template>
                <Routes>
                    {rotas_publicas}
                    {rotas_aplicacao}
                    {rotas_admin}
                    {rotas_rh}
                    {rotas_estagio}
                    {rotas_ferias}
                    {rotas_folgas}
                    {rotas_pecunia}
                    {rotas_meritocracia}
                    {rotas_distribuicao_processos}
                    {rotas_da}
                    <Route path='*' element={<Pagina404/>} />
                </Routes>
            </Template>
        </BrowserRouter>
    )
}

export default Rotas;