import { Route, Navigate } from 'react-router-dom';

import { autenticado } from '../config/auth';
import Permissoes from '../config/permissoes';

//views da aplicação

import ListaColaborador from '../views/da/listar';
import CadastroColaborador from '../views/da/cadastrar';
import EditarColaborador from '../views/da/editar';
import Empresas from '../views/da/paramentros';


const ProtectedRoute = ({ children, path }) => {
    return autenticado() ?
        <Permissoes path={path}>
            {children}
        </Permissoes>
        :
        <Navigate to="/" replace />;
};

export default [
    <Route
        key="cadastro-cedidos-terceirizados"
        path="/da/cadastro_cedidos_terceirizados"
        element={
            <ProtectedRoute path="/da/cadastro_cedidos_terceirizados">
                <CadastroColaborador/>
            </ProtectedRoute>
        }
    />,
    <Route
        key="listar-cedidos-terceirizados"
        path="/da/cedidos_terceirizados"
        element={
            <ProtectedRoute path="/da/cedidos_terceirizados">
                <ListaColaborador />
            </ProtectedRoute>
        }
    />,
    <Route
        key="editar-cedidos-terceirizados"
        path="/da/editar_cedidos_terceirizados/:id"
        element={
            <ProtectedRoute path="/da/editar_cedidos_terceirizados/:id">
                <EditarColaborador />
            </ProtectedRoute>
        }
    />,
    <Route
    key="empresas"
    path="/da/empresas/:id"
    element={
        <ProtectedRoute path="/da/empresas/:id">
            <Empresas/>
        </ProtectedRoute>
    }
/>,
];