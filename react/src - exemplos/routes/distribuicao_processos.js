import { Route, Navigate } from 'react-router-dom';

import { autenticado } from '../config/auth';
import Permissoes from '../config/permissoes';

//views da aplicação
import DistribuirProcessos from '../views/distribuicao-processos/distribuir-processos';
import RelatoriosDistribuicaoProcessos from '../views/distribuicao-processos/relatorios';

const ProtectedRoute = ({ children , path}) => {
    return autenticado() ?  
        <Permissoes path={path}>
             {children} 
        </Permissoes>
        : 
        <Navigate to="/" replace />;
};

//permissoes LDAP para acesso as ROTAS
const permissoes = ["admin", "distribuir_processos"];

export default [
    <Route
        key="distribuir-processos"
        path="/distribuicao-processos/distribuir"
        element={
            <ProtectedRoute path="/distribuicao-processos/distribuir">
                <DistribuirProcessos/>
            </ProtectedRoute>
        }
    />,
    <Route
        key="relatorios-distribuicao-processos"
        path="/distribuicao-processos/relatorios"
        element={
            <ProtectedRoute path="/distribuicao-processos/relatorios">
                <RelatoriosDistribuicaoProcessos/>
            </ProtectedRoute>
        }
    />
];