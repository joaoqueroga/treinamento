import { Route, Navigate } from 'react-router-dom';

import { autenticado } from '../config/auth';
import Permissoes from '../config/permissoes';

//views da aplicação
import PecuniaSolicitacao from '../views/modulo_ferias/pecunia/solicitacao';
import PesquisaPecuniaDetalhe from '../views/modulo_ferias/pecunia/pesquisa/Detalhes';
import PecuniaPesquisa from '../views/modulo_ferias/pecunia/pesquisa';
import SolicitacoesPecúnia from '../views/modulo_ferias/pecunia/solicitacoes';

const ProtectedRoute = ({ children , path}) => {
    return autenticado() ?  
        <Permissoes path={path}>
             {children} 
        </Permissoes>
        : 
        <Navigate to="/" replace />;
};

export default [
    <Route
        key="solicitar-pecunia"
        path="/solicitacao_pecunia"
        element={
            <ProtectedRoute path="/solicitacao_pecunia">
                <PecuniaSolicitacao/>
            </ProtectedRoute>
        }
    />,
    <Route
    key="pecunia-pesquisa"
    path="/pecunia/pesquisa"
    element={
        <ProtectedRoute path="/pecunia/pesquisa">
            <PecuniaPesquisa/>
        </ProtectedRoute>
    }
    />,
    <Route
    key="pecunia-pesquisa-detalhe"
    path="/pecunia/pesquisa/:id"
    element={
        <ProtectedRoute path="/pecunia/pesquisa/:id">
            <PesquisaPecuniaDetalhe/>
        </ProtectedRoute>
    }
    />,
    <Route
        key="solicitacoes-pecunia"
        path="/solicitacoes/pecunia"
        element={
            <ProtectedRoute path="/solicitacoes/pecunia">
                <SolicitacoesPecúnia/>
            </ProtectedRoute>
        }/>

];