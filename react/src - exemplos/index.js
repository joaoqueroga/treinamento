import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.scss';
import App from './App';
import ApiProvider from "./context/store";
import 'bootstrap/dist/css/bootstrap.min.css';

import "primereact/resources/themes/lara-light-indigo/theme.css";  //theme
import "primereact/resources/primereact.min.css";                  //core css
import "primeicons/primeicons.css";                                //icons

import Traducoes from "../src/config/primereact-ptbr.json";
import { locale, addLocale } from 'primereact/api';

addLocale('pt', Traducoes['pt']);
locale('pt');

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <ApiProvider>
      <App />
    </ApiProvider>
);


