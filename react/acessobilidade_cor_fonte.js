import React, {useState} from "react";
import "./App.scss";
import { FaUniversalAccess } from "react-icons/fa";



const App = () => {

  const [tema,setTema] = useState(0); //0 light 1 dark
  const [fontp,setFontp] = useState(0); 
  const [btacess,setbtAcess] = useState(false); 

  function assesibilidadeCor() {
    if(tema === 1){
      return {
        background: "#000",
        color:"#FFF",
      }
    }
    return null
  }

  function assesibilidadeFont(x) {
   
    return {
      fontSize: x+fontp
    }

  }

  return (
    <div className="assistente-voz-result">

        <span className="bts-acessibilidade-card">
          {btacess?
          <span className="botoes-de-acesibilidade">
            <button onClick={()=>setTema(0)}>normal</button>
            <button onClick={()=>setTema(1)}>contraste</button>
            <button onClick={()=>setFontp(fontp-1)}>A-</button>
            <button onClick={()=>setFontp(fontp+1)}>A+</button>
          </span>
          :null
          }
          <FaUniversalAccess className="bts-acessibilidade" onClick={()=>setbtAcess(!btacess)}/>
        </span>
        <div className="div-1" style={ assesibilidadeCor()}>
          <h2 >Container 01</h2>
          <p className="teste" style={ assesibilidadeFont(16)}>Sua demanda envolve ação contra pessoas físicas? Exemplos: Vizinhança (infiltrações, poda de árvores, aberturas irregulares de janelas, passagem forçada, muro divisório, dentre outras), acidente de trânsito, despejo, cobranças de valores, anulação de contratos e negociações particulares, danos morais, invesão de terreno com até um metro, queda de água da chuva no seu terreno, construção irregular de fossas, acidentes causados por animais, entre outras?</p>
        </div>
        <div className="div-2">
          <h2>Container 02</h2>
          <p>Sua demanda envolve ação contra pessoas físicas? Exemplos: Vizinhança (infiltrações, poda de árvores, aberturas irregulares de janelas, passagem forçada, muro divisório, dentre outras), acidente de trânsito, despejo, cobranças de valores, anulação de contratos e negociações particulares, danos morais, invesão de terreno com até um metro, queda de água da chuva no seu terreno, construção irregular de fossas, acidentes causados por animais, entre outras?</p>
        </div>

       

        
    </div>
  );
};

export default App;


/*

CSS desta aplicacao

.div-1{
  background-color: #19985C;
  color: #DADADA;

  width: 50%;
  padding: 20px;
  margin: 20px 0px 0px 20px;
}

.div-2{
  background-color: #104856;
  color: #DADADA;

  width: 50%;
  padding: 20px;
  margin: 20px 0px 0px 20px;
}

.bts-acessibilidade-card{
  display: flex;
  align-items: center;
  justify-content: flex-end;
}
.bts-acessibilidade{
  padding: 5px;
  color: blue;
  font-size: 50px;
  border-radius: 45px;
  box-shadow: 2px 2px 10px #ccc;
  margin-right: 20px;
}

.bts-acessibilidade:hover{
  background-color: #ccc;
}

.botoes-de-acesibilidade{
  background-color: #ccc;
  padding: 8px;
  border-radius: 45px;
  margin-right: 10px;
  box-shadow: 2px 2px 10px #ccc;
}

.botoes-de-acesibilidade>button{
  border-radius: 10px;
  padding: 5px;
  margin: 0px 2px 0px 2px;
  font-size: 15px;
  outline: none;
  border: none;
}

.botoes-de-acesibilidade>button:hover{
  background-color: rgb(147, 135, 135);
}


*/