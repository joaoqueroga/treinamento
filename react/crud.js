import React, { useState } from 'react';

export default function App(){
   
  const [nome, setNome] = useState('');
  const [nomes, setNomes] = useState([]);

  const [edit, setEdit] = useState(false);
  const [indexedit, setIndexedit] = useState(0);
  const [editname, setEditname] = useState('');

  function adicionar() {
      if(!nome){
         alert("Escreva um o nome");
      }else{
         let aux = nomes;
         aux.push(nome);
         setNome('');
         setNomes([...aux]);
      }
  }

  function excluir(item){
      let aux = nomes;
      let index = aux.indexOf(item);
      if (index > -1) {
         aux.splice(index, 1);
      }
      setNomes([...aux]);
  }

  function editar(item, index){
      setEdit(true)
      setEditname(item);
      setIndexedit(index);
   }

   function salvarEdicao(){
      let aux = nomes;
      aux[indexedit] = editname;
      setEdit(false)
      setNomes([...aux]);
   }


  return (
      <div>
        <p>Nomes</p>
        <input size={50} value={nome} onChange={(e)=>setNome(e.target.value)}/>
        <button onClick={adicionar}>adicionar</button>
        {
            edit?
            <div>
               <p>Atualizar nome</p>
               <input size={50} value={editname} onChange={(e)=>setEditname(e.target.value)}/>
               <button onClick={salvarEdicao}>salvar</button>
               <button onClick={()=>setEdit(false)}>cancelar</button>
            </div>
            :
            null
        }
        <hr/>
        {
           nomes.map((n, index)=>{
               return(
                  <div key={index}>  
                  {' '}<button onClick={()=>editar(n, index)}>editar</button> 
                  {' '}<button onClick={()=>excluir(n)}>excluir</button> 
                  {' '}- - - {n}
                  </div>
               )
           })
        }
      </div>
  )
}