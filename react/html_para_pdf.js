import { useRef } from "react";
import html2canvas from "html2canvas"; /// npm i html2canvas 
import { jsPDF } from "jspdf";   /// npm i jspdf

import "./App.css";

const App = () => {
  const inputRef = useRef(null);
  const printDocument = () => {
    html2canvas(inputRef.current).then((canvas) => {
      const imgData = canvas.toDataURL("image/png", 2.0);
      const pdf = new jsPDF('', 'pt', 'a4');
      pdf.addImage(imgData, 'JPEG', 0, 0, 595.28, 592.28/canvas.width * canvas.height );
      //pdf.save("download.pdf");
      window.open(pdf.output('bloburl', { filename: 'download.pdf' }), '_blank');
    });
  };

  function printToPDF_2() { /// com quebra de pagina
    html2canvas(inputRef.current).then((canvas)=>{
            var contentWidth = canvas.width;
            var contentHeight = canvas.height;
            var pageHeight = contentWidth / 592.28 * 841.89;
            var leftHeight = contentHeight;
            var position = 0;
            var imgWidth = 555.28;
            var imgHeight = 555.28 / contentWidth * contentHeight;
            var pageData = canvas.toDataURL('image/jpeg', 1.0);
            var pdf = new jsPDF('', 'pt', 'a4');

            if (leftHeight < pageHeight) { 
              pdf.addImage(pageData, 'JPEG', 20, 0, imgWidth, imgHeight); 
            } else {
              while (leftHeight > 0) {
                pdf.addImage(pageData, 'JPEG', 20, position, imgWidth, imgHeight)
                leftHeight -= pageHeight;
                position -= 841.89;
                    //Avoid adding blank pages
                if (leftHeight > 0) {
                    pdf.addPage();
                }
              }
            }
            pdf.save('download.pdf');
        });
}
  return (
    <>
      <div className="App">
        <h1>Hello CodeSandbox</h1>
        <h2>Start editing to see some magic happen!</h2>
        <div className="mb5">
          <button onClick={printDocument}>Print</button>
        </div>
        <div id="divToPrint" >
          <div ref={inputRef}>
            <p>Mussum Ipsum, cacilds vidis litro abertis. Cevadis im ampola pa arma uma pindureta.Nec orci ornare consequat. Praesent lacinia ultrices consectetur. Sed non ipsum felis.Interagi no mé, cursus quis, vehicula ac nisi.Sapien in monti palavris qui num significa nadis i pareci latim.</p>

            <p>
              Mussum Ipsum, cacilds vidis litro abertis. Cevadis im ampola pa arma uma pindureta.Nec orci ornare consequat. Praesent lacinia ultrices consectetur. Sed non ipsum felis.Interagi no mé, cursus quis, vehicula ac nisi.Sapien in monti palavris qui num significa nadis i pareci latim. teste
            </p>

            <table>
              <tr>
                <td>c1</td>
                <td>c2</td>
                <td>c3</td>
                <td>c4</td>
              </tr>
              <tr>
                <td>c1</td>
                <td>c2</td>
                <td>c3</td>
                <td>c4</td>
              </tr>
              <tr>
                <td>c1</td>
                <td>c2</td>
                <td>c3</td>
                <td>c4</td>
              </tr>
            </table>

            <p>Mussum Ipsum, cacilds vidis litro abertis. Cevadis im ampola pa arma uma pindureta.Nec orci ornare consequat. Praesent lacinia ultrices consectetur. Sed non ipsum felis.Interagi no mé, cursus quis, vehicula ac nisi.Sapien in monti palavris qui num significa nadis i pareci latim.</p>

            <p>
              Mussum Ipsum, cacilds vidis litro abertis. Cevadis im ampola pa arma uma pindureta.Nec orci ornare consequat. Praesent lacinia ultrices consectetur. Sed non ipsum felis.Interagi no mé, cursus quis, vehicula ac nisi.Sapien in monti palavris qui num significa nadis i pareci latim. teste
            </p>

            <p>Mussum Ipsum, cacilds vidis litro abertis. Cevadis im ampola pa arma uma pindureta.Nec orci ornare consequat. Praesent lacinia ultrices consectetur. Sed non ipsum felis.Interagi no mé, cursus quis, vehicula ac nisi.Sapien in monti palavris qui num significa nadis i pareci latim.</p>

            <p>
              Mussum Ipsum, cacilds vidis litro abertis. Cevadis im ampola pa arma uma pindureta.Nec orci ornare consequat. Praesent lacinia ultrices consectetur. Sed non ipsum felis.Interagi no mé, cursus quis, vehicula ac nisi.Sapien in monti palavris qui num significa nadis i pareci latim. teste
            </p>

            <p>Mussum Ipsum, cacilds vidis litro abertis. Cevadis im ampola pa arma uma pindureta.Nec orci ornare consequat. Praesent lacinia ultrices consectetur. Sed non ipsum felis.Interagi no mé, cursus quis, vehicula ac nisi.Sapien in monti palavris qui num significa nadis i pareci latim.</p>

            <p>
              Mussum Ipsum, cacilds vidis litro abertis. Cevadis im ampola pa arma uma pindureta.Nec orci ornare consequat. Praesent lacinia ultrices consectetur. Sed non ipsum felis.Interagi no mé, cursus quis, vehicula ac nisi.Sapien in monti palavris qui num significa nadis i pareci latim. teste
            </p>

            <table>
              <tr>
                <td>c1</td>
                <td>c2</td>
                <td>c3</td>
                <td>c4</td>
              </tr>
              <tr>
                <td>c1</td>
                <td>c2</td>
                <td>c3</td>
                <td>c4</td>
              </tr>
              <tr>
                <td>c1</td>
                <td>c2</td>
                <td>c3</td>
                <td>c4</td>
              </tr>
            </table>

            <p>Mussum Ipsum, cacilds vidis litro abertis. Cevadis im ampola pa arma uma pindureta.Nec orci ornare consequat. Praesent lacinia ultrices consectetur. Sed non ipsum felis.Interagi no mé, cursus quis, vehicula ac nisi.Sapien in monti palavris qui num significa nadis i pareci latim.</p>

            <p>
              Mussum Ipsum, cacilds vidis litro abertis. Cevadis im ampola pa arma uma pindureta.Nec orci ornare consequat. Praesent lacinia ultrices consectetur. Sed non ipsum felis.Interagi no mé, cursus quis, vehicula ac nisi.Sapien in monti palavris qui num significa nadis i pareci latim. teste
            </p>


           
          </div>
        </div>
      </div>
    </>
  );
};
export default App;
