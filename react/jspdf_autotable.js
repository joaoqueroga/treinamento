import React, {useState} from "react";
import { Dialog } from 'primereact/dialog';
import { Button } from 'primereact/button';
import {FaFileDownload} from "react-icons/fa";
import jsPDF from 'jspdf';
import autoTable from 'jspdf-autotable';
import logo from './logo_colorido_low.png';
import './comprovantepdf.scss'

function ComprovanteSolicitacao(props) {
    const [display_pdf, setdisplay_pdf] = useState(false);

    function baixar_pdf() {
        //setdisplay_pdf(true);
        //buscar os dados em uma procedure e passar para gerar_pdf();
        gerar_pdf();
    }

    function cabecalho(doc, x_titulo, titulo) {
        doc.addImage(logo, 'PNG', 40, 20, 155, 30 );
        doc.setFont('helvetica', 'bold');
        doc.setFontSize(18);
        doc.setTextColor(20,71,88);
        doc.text(x_titulo, 40, titulo);
    }

    function rodape(doc) {
        let data = new Date();
        let dia_x = data.getDate() < 10 ? `0${data.getDate()}` : data.getDate();
        let mes_x = (data.getMonth()+1) < 10 ? `0${data.getMonth()+1}` : data.getMonth()+1
        let dia = `${dia_x}/${mes_x}/${data.getFullYear()}`;
        let hora = data.toLocaleTimeString();

        let pageCount = doc.internal.getNumberOfPages();
        doc.setFontSize(7)
        doc.setFont('helvetica', 'bold');
        for (var i = 1; i <= pageCount; i++) {
            doc.setPage(i)
            doc.text('DEFENSORIA PÚBLICA DO ESTADO DO AMAZONAS - AV. ANDRE ARAUJO, 679, ALEIXO  CNPJ: 19.421.427/0001-91', doc.internal.pageSize.width / 2, 810, { align: 'center'});
            doc.text(`Emitido em ${dia} ${hora} - Página  ${String(i)} de ${String(pageCount)}`, doc.internal.pageSize.width / 2, 820, { align: 'center'});
        }
    }

    function rotulo_centro(doc, texto) {
        doc.autoTable({ startY: doc.autoTable.previous.finalY+5, styles: {fontStyle : 'bold', halign: 'center'},  theme: 'striped' , body: [[texto]]})
    }

    function tabela_campos(doc,dados) {
        doc.autoTable({ startY: doc.autoTable.previous.finalY+5 , styles: { cellPadding: 2}, theme: 'plain', body: dados})
    }
    function tabela_texto_centro(doc,dados) {
        doc.autoTable({ startY: doc.autoTable.previous.finalY+5 , styles: { cellPadding: 2, halign: 'center', textColor: '#665759'},theme: 'plain', body: dados})
    }
    function tabela_campos_grid(doc,dados) {
        doc.autoTable({ startY: doc.autoTable.previous.finalY+5 , styles: { cellPadding: 2}, theme: 'grid', body: dados})
    }
    function tabela_campos_quadro(doc,dados) {
        doc.autoTable({ startY: doc.autoTable.previous.finalY+5 , styles: { cellPadding: 10}, theme: 'grid', body: dados})
    }
    function linha(doc,dados) {
        doc.autoTable({ startY: doc.autoTable.previous.finalY , styles: { cellPadding: 2}, theme: 'plain', body: dados})
    }

    function gerar_pdf  (){
        let doc = new jsPDF('p', 'pt');
        cabecalho(doc, 322, "");

        autoTable(doc, { startY: 60,headStyles: { fillColor: [0,0,0] }, styles: {fontStyle : 'bold', halign: 'center'},  body:[[`COMPROVANTE DE AGENDAMENTO - ${props.agenda.tipo===1?'PRESENCIAL':"VIRTUAL"}`]]})

        let data_agendamento = `${props.agenda.data.getDate()<10?'0'+props.agenda.data.getDate():props.agenda.data.getDate()}/${(props.agenda.data.getMonth()+1)<10?'0'+(props.agenda.data.getMonth()+1):props.agenda.data.getMonth()+1}/${props.agenda.data.getFullYear()} - ${props.agenda.hora}`

        if(props.usuario.nome_social){
            tabela_campos_grid(doc, [
                ['Requerente', props.usuario.nome],
                ['Nome social', props.usuario.nome_social],
                ['CPF do requerente', props.usuario.cpf],
                ['Atendimento número', props.atendimento.numero],
                ['Agendado para', data_agendamento],
                ['Defensoria', props.atendimento.defensoria],
                ['Endereço', props.agenda.tipo===1?props.agenda.endereco:"Atendimento Virtual"],
                ['Área/Pedido', props.area_nome],
                ['Qualificação', props.agenda.qualificacao_texto],
            ])
        }else{
            tabela_campos_grid(doc, [
                ['Requerente', props.usuario.nome],
                ['CPF do requerente', props.usuario.cpf],
                ['Atendimento número', props.atendimento.numero],
                ['Agendado para', data_agendamento],
                ['Defensoria', props.atendimento.defensoria],
                ['Endereço', props.agenda.tipo===1?props.agenda.endereco:"Atendimento Virtual"],
                ['Área/Pedido', props.area_nome],
                ['Qualificação', props.agenda.qualificacao_texto],
            ])
        }

        if(props.agenda.tipo===0){
            rotulo_centro(doc, 'ATENÇÃO');
            tabela_campos_quadro(doc, [
                [`- O ATENDIMENTO VIRTUAL OCORRERÁ PELO APLICATIVO WHATSAPP. \n- CONFIRME A CONTA OFICIAL QUE PODE SER VERIFICADA POR UM  SELO AO LADO DO CONTATO. \n- A DPE ENVIARÁ MENSAGEM, AO SEU WHASTAPP, CONFIRMANDO O ATENDIMENTO. \n- PARA PROSSEGUIR COM O ATENDIMENTO É NECESSÁRIO CLICAR EM "CONTINUAR".`],
            ])
        }

        rotulo_centro(doc, 'DOCUMENTAÇÃO NECESSÁRIA');
        let docs = '';
        props.agenda.documentos.map((d, index)=>{
            docs += `- ${d}`;
            if(index < props.agenda.documentos.length-1) docs += '\n';
        })
        tabela_campos_quadro(doc, [
            [docs],
        ])

        rotulo_centro(doc, 'ATENÇÃO');
        tabela_campos_quadro(doc, [
            [`- NO DIA DO ATENDIMENTO É PRECISO ESTAR DE POSSE DA DOCUMENTAÇÃO NECESSÁRIA.\n- NO CASO FALTE ALGUM DOCUMENTO, NÃO SERÁ POSSÍVEL ATENDÊ-LO NA DATA AGENDADA.\n- AS FOTOS DOS DOCUMENTOS DEVEM SER, LEGÍVEIS, CENTRALIZADAS E NITÍDAS.`],
        ])

        tabela_campos(doc, [[]])
        tabela_texto_centro(doc, [['IMPORTANTE: Os serviços oferecidos pela Defensoria Pública são gratuitos em todas as etapas de um processo judicial. Logo, a Defensoria não cobra pelos serviços à população.']])

        rodape(doc);

        // doc.save(`Agendamento_${props.atendimento.numero}.pdf`)
        window.open(doc.output('bloburl', { filename: `comprovante_agendamento.pdf` }), '_blank');
        setdisplay_pdf(false);
    } 

    
    return (
        <div>
            <Button 
                onClick={()=>baixar_pdf()} 
                className="comprovante_botao_pdf"
            >
                Baixar Comprovante<FaFileDownload className="react-icon" size={20}/>
            </Button>
            <Dialog
                visible={display_pdf} 
                style={{ width: '50vw' }}
                closable={false}
            >
                Gerando comprovante...
            </Dialog>
        </div>
     );
}

export default ComprovanteSolicitacao;