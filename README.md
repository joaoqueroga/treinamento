# Treinamento - João Queroga

- Este repositório armazena códigos e trechos importantes

## React JS
- CRUD
- Baixar HTML para PDF e abrir em nova guia
- Baixar PDF de tabela automática (conteúdo dinamico)
- Converter fala para texto
- Converter texto para fala
- Acessibilidade cor e themas
- Acessibilidade tamanho da fonte

## Javascript
- Promisses
- Async await
- Spread
## Unity

## C#

## Java

## Python

## Django

## SQL

## Node JS
